﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body style="text-align: left; font-size: 11pt;">

        <div>
          Dear <xsl:value-of select="/EventEmail/MDSEvent/INSTL_SITE_POC_NME" />,
        </div>

        <br></br>

        <div>
          The new equipment (Device Serial # : <xsl:value-of select="/EventEmail/MDSEvent/DEV_SERIAL_NBR" />) for
          <xsl:value-of select="/EventEmail/MDSEvent/STREET_ADR" />, <xsl:value-of select="/EventEmail/MDSEvent/CTY_NME" />,
          <xsl:value-of select="/EventEmail/MDSEvent/STT_PRVN_NME" />, <xsl:value-of select="/EventEmail/MDSEvent/CTRY_RGN_NME" />,
          <xsl:value-of select="/EventEmail/MDSEvent/ZIP_CD" /> has been configured and shipped<xsl:if test="EventEmail/MDSEvent/ALT_SHIP_POC_NME[.!='']"> to <xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_POC_NME" />, <xsl:if test="EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME[.!='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/ALT_SHIP_STREET_ADR, ', ',EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTY_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_STT_PRVN_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTRY_RGN_NME, ', ',  EventEmail/MDSEvent/ALT_SHIP_ZIP_CD)" />
                </xsl:if>
                <xsl:if test="EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME[.='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/ALT_SHIP_STREET_ADR, ', ', EventEmail/MDSEvent/ALT_SHIP_CTY_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_STT_PRVN_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTRY_RGN_NME, ', ',  EventEmail/MDSEvent/ALT_SHIP_ZIP_CD)" />
                </xsl:if></xsl:if>.
          Shipment Carrier Name is <xsl:value-of select="/EventEmail/MDSEvent/SHIP_CXR_NME" />.
          You may track this shipment via tracking number <xsl:value-of select="/EventEmail/MDSEvent/SHIP_TRK_REFR_NBR" />.
        </div>

        <br></br>

        <div>
          Please ensure that the device is unboxed and plugged in by <xsl:value-of select="substring(/EventEmail/MDSEvent/STRT_TMST,1,10)" />.
          If you have any questions regarding shipment or activation of this equipment, please contact
          <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER" /> at <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_PHN" />
          or <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_EMAIL" />.
        </div>

        <br></br>

        <div>
         Thank you-
         <br></br>
         Sprint Managed Network Services
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>