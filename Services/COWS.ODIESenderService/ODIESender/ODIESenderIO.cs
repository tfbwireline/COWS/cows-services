﻿/*
Updated By:	Md. Mahmud Monir/Vn370313
Updated date:  03/03/2017
Description:	MDSInstall Activity Type     ADDed
                  new XElement("ServiceAssuranceContactHours", dtEvent.Rows[0]["ServiceAssuranceContactHours"]),
                    new XElement("ServiceAssuranceTimeZone", dtEvent.Rows[0]["ServiceAssuranceTimeZone"]),
*/

using IOManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;

namespace COWS.ODIESenderService
{
    public class ODIESenderIO
    {
        public int _iSleepTime = 0;

        private DataRow DR;
        private string XMLStr = string.Empty;
        private string REQ_ID = string.Empty;
        private string CSG_LVL_ID = string.Empty;
        private ODIESenderDB _SenderDB = new ODIESenderDB();

        public void GetODIERequests(ref MQC _mqClient)
        {
            try
            {
                DataSet DS = new DataSet();
                DataTable DT = new DataTable();
                DataTable DTDisc = new DataTable();

                DS = _SenderDB.GetODIERequests();
                if (DS != null & DS.Tables.Count > 0)
                {
                    DT = DS.Tables[0];
                    DTDisc = DS.Tables[1];

                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        int iCnt = DT.Rows.Count;
                        CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send Request(s) to ODIE", string.Empty);
                        for (int i = 0; i < iCnt; i++)
                        {
                            DR = DT.Rows[i];
                            XMLStr = string.Empty;
                            REQ_ID = DR["REQ_ID"].ToString();
                            CSG_LVL_ID = DR["CSG_LVL_ID"].ToString();
                            int iStat = 11;

                            try
                            {
                                switch (Convert.ToInt32(DR["ODIE_MSG_ID"].ToString()))
                                {
                                    case 1:
                                        XMLStr = GenerateNameRequest();
                                        break;

                                    case 2:
                                        XMLStr = GenerateInfoRequest();
                                        break;

                                    case 3:
                                        XMLStr = GenerateDCRequest();
                                        break;

                                    case 4:
                                        XMLStr = GenerateDiscoRequest(DTDisc);
                                        break;

                                    case 5:
                                        XMLStr = GenerateCPEInfoMsg();
                                        break;

                                    case 6:
                                        XMLStr = GenerateCPEStusMsg();
                                        break;

                                    case 7:
                                        XMLStr = GenerateCPTRequest();
                                        break;

                                    case 8:
                                        XMLStr = GenerateCPEPRCVMsg();
                                        break;

                                    case 9:
                                        XMLStr = GenerateCPEISMVMsg();
                                        break;

                                    case 10:
                                        XMLStr = GenerateODIECpeReuseMsg();
                                        break;
                                        
                                    default:
                                        break;
                                }

                                if (CSG_LVL_ID == "0")
                                    CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "ODIE Request XML is: ", XMLStr);
                                else
                                {
                                    try
                                    {
                                        _SenderDB.InsertCustomerSecuredLog(Convert.ToInt32(REQ_ID), 12, XMLStr);
                                    }
                                    catch (Exception e)
                                    {
                                        CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error inserting Log message into CUST_SCRD_LOG table for REQ_ID " + REQ_ID, e.Message);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                iStat = 12;
                                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error generating ODIE XML for REQ_ID " + REQ_ID, e.Message);
                            }

                            if (XMLStr.Length > 0)
                            {
                                try
                                {
                                    _mqClient.MQPut(XMLStr);
                                    CL.WriteLogFile("Request", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Request send to ODIE with Success, REQ_ID = " + REQ_ID, string.Empty);
                                }
                                catch (Exception e)
                                {
                                    CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "MQ Error putting XML in the ODIE Request Queue for REQ_ID " + REQ_ID, e.Message + e.StackTrace);
                                    iStat = 12;
                                }
                            }

                            if (_SenderDB.UpdateODIERequest(Convert.ToInt32(REQ_ID), iStat) == 0)
                            {
                                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error updating the status in the ODIE_REQ table for REQ_ID: " + REQ_ID, string.Empty);
                            }
                        }
                    }
                    else
                    {
                        if (DateTime.Now.Second % 30 == 0)
                        {
                            CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to ODIE", string.Empty);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Exception in GetODIERequests(ref MQC _mqClient)", e.Message + e.StackTrace);
            }
        }

        #region "Private Methods"

        private string GenerateNameRequest()
        {
            ODIECustomerNameRequest _Name = new ODIECustomerNameRequest();
            _Name.H1 = FixNull(DR["H1_CUST_ID"].ToString());
            _Name.TranID = REQ_ID;
            _Name.RequestDateTime = DateTime.Now;

            XmlSerializer _Serializer = new XmlSerializer(typeof(ODIECustomerNameRequest));
            return SerializeObject<ODIECustomerNameRequest>(Encoding.UTF8, _Name);
        }

        private string GenerateInfoRequest()
        {
            ODIECustomerInfoRequest _Info = new ODIECustomerInfoRequest();
            _Info.CustomerName = FixNull(DR["CUST_NME"].ToString());
            _Info.TranID = REQ_ID;
            _Info.CustID = FixNull(DR["ODIE_CUST_ID"].ToString());

            ODIECustomerInfoRequestCategoryType[] _catType;
            if (DR["ODIE_CUST_INFO_REQ_CAT_TYPE_ID"] != null && Convert.ToInt32(DR["ODIE_CUST_INFO_REQ_CAT_TYPE_ID"]) != 0)
            {
                int catTypeID = Convert.ToInt32(DR["ODIE_CUST_INFO_REQ_CAT_TYPE_ID"]);
                if (catTypeID == 1 || catTypeID == 2)
                    _catType = new ODIECustomerInfoRequestCategoryType[2];
                else
                    _catType = new ODIECustomerInfoRequestCategoryType[3];

                ApplyODIECustInfoCategoryTypes(ref _catType, DR);
                _Info.Category = _catType;
            }
            else
            {
                _Info.Category = null;
            }
            if (DR["ODIE_DEVICE_FILTER"] != null && DR["ODIE_DEVICE_FILTER"].ToString() != string.Empty)
            {
                List<string> lstDeviceFilter = new List<string>();

                if (DR["ODIE_DEVICE_FILTER"].ToString().IndexOf(',') > 0)
                {
                    foreach (string s in DR["ODIE_DEVICE_FILTER"].ToString().Split(','))
                    {
                        if (s != string.Empty)
                            lstDeviceFilter.Add(s);
                    }
                }
                else
                {
                    lstDeviceFilter.Add(DR["ODIE_DEVICE_FILTER"].ToString());
                }
                ODIECustomerInfoRequestDeviceFilter[] _devFilter = new ODIECustomerInfoRequestDeviceFilter[lstDeviceFilter.Count];
                int i = 0;
                foreach (string s in lstDeviceFilter)
                {
                    ODIECustomerInfoRequestDeviceFilter devFilter = new ODIECustomerInfoRequestDeviceFilter();
                    devFilter.Value = s;
                    _devFilter[i] = devFilter;
                    i = i + 1;
                }
                _Info.DeviceFilter = _devFilter;
            }
            else
            {
                _Info.DeviceFilter = null;
            }

            _Info.RequestDateTime = DateTime.Now;

            XmlSerializer _Serializer = new XmlSerializer(typeof(ODIECustomerInfoRequest));
            return SerializeObject<ODIECustomerInfoRequest>(Encoding.UTF8, _Info);
        }

        private string GenerateDCRequest()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtEvent = new DataTable();
                DataTable dtDevice = new DataTable();
                DataTable dtFTN = new DataTable();
                DataTable dtService = new DataTable();

                ds = _SenderDB.getDesignCompleteData(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    string _FTN = "";
                    string _ServiceTier = "";

                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        dtEvent = ds.Tables[0];
                    if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                        dtFTN = ds.Tables[1];
                    if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                        dtDevice = ds.Tables[2];
                    if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                        dtService = ds.Tables[3];

                    if ((dtEvent.Rows.Count>0) && (dtEvent.Rows[0]["ActivityType"].ToString() == "Install"))
                    {
                        if (dtFTN.Rows.Count > 0)
                        {
                            _FTN = dtFTN.Rows[0]["FTN"].ToString();
                            _ServiceTier = dtFTN.Rows[0]["Service_Tier"].ToString();
                        }

                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"]),
                                new XElement("H1", dtEvent.Rows[0]["H1"])
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device = new XElement("DeviceActivation",
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("EquipType", string.Empty),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("oneMB", string.Empty),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("FTN", _FTN),
                                    new XElement("Service_Tier", _ServiceTier),
                                    new XElement("H5_H6_Cust_ID", dtEvent.Rows[0]["H5_H6_Cust_ID"]),
                                    new XElement("Address", dtEvent.Rows[0]["Address"]),
                                    new XElement("Floor", dtEvent.Rows[0]["Floor"]),
                                    new XElement("City", dtEvent.Rows[0]["City"]),
                                    new XElement("State", dtEvent.Rows[0]["State"]),
                                    new XElement("ZipCode", dtEvent.Rows[0]["ZipCode"]),
                                    new XElement("Country", dtEvent.Rows[0]["Country"]),
                                    new XElement("ServiceAssurancePOCEmail", string.Empty),
                                    new XElement("ServiceAssuranceContactPhone", dtEvent.Rows[0]["ServiceAssuranceContactPhone"]),
                                    new XElement("ServiceAssuranceContactLastName", dtEvent.Rows[0]["ServiceAssuranceContactName"]),
                                    new XElement("ServiceAssuranceContactFirstName", ""),
                                    new XElement("ServiceAssuranceContactHours", dtEvent.Rows[0]["ServiceAssuranceContactHours"]),
                                    new XElement("ServiceAssuranceTimeZone", dtEvent.Rows[0]["ServiceAssuranceTimeZone"]),
                                    new XElement("Domestic_International_Flag", dtEvent.Rows[0]["Domestic_International_Flag"]),
                                    new XElement("Mach5_Flag", string.Empty) //Used for 8006
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && (dtEvent.Rows[0]["ActivityType"].ToString() == "MAC-Move"))
                    {
                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"]),
                                new XElement("H1", dtEvent.Rows[0]["H1"])
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device = new XElement("DeviceActivation",
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("EquipType", string.Empty),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("oneMB", string.Empty),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("H5_H6_Cust_ID", dtEvent.Rows[0]["H5_H6_Cust_ID"]),
                                    new XElement("Address", dtEvent.Rows[0]["Address"]),
                                    new XElement("Floor", dtEvent.Rows[0]["Floor"]),
                                    new XElement("City", dtEvent.Rows[0]["City"]),
                                    new XElement("State", dtEvent.Rows[0]["State"]),
                                    new XElement("ZipCode", dtEvent.Rows[0]["ZipCode"]),
                                    new XElement("Country", dtEvent.Rows[0]["Country"]),
                                    new XElement("ServiceAssurancePOCEmail", string.Empty),
                                    new XElement("ServiceAssuranceContactPhone", dtEvent.Rows[0]["ServiceAssuranceContactPhone"]),
                                    new XElement("ServiceAssuranceContactLastName", dtEvent.Rows[0]["ServiceAssuranceContactName"]),
                                    new XElement("ServiceAssuranceContactFirstName", ""),
                                    new XElement("ServiceAssuranceContactHours", dtEvent.Rows[0]["ServiceAssuranceContactHours"]),
                                    new XElement("ServiceAssuranceTimeZone", dtEvent.Rows[0]["ServiceAssuranceTimeZone"]),
                                    new XElement("Mach5_Flag", string.Empty) //Used for 8006
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && (dtEvent.Rows[0]["ActivityType"].ToString() == "MAC-Change"))
                    {
                        if (dtFTN.Rows.Count > 0)
                            _FTN = dtFTN.Rows[0]["FTN"].ToString();

                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                            new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                            new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                            new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"])
                            );

                        XElement _DeviceList = new XElement("DeviceActivationList");

                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device;

                                _Device = new XElement("DeviceActivation",
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("EquipType", string.Empty),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("FTN", _FTN),
                                    new XElement("H5_H6_Cust_ID", dtEvent.Rows[0]["H5_H6_Cust_ID"]),
                                    new XElement("Domestic_International_Flag", dtEvent.Rows[0]["Domestic_International_Flag"]),
                                    new XElement("Mach5_Flag", string.Empty) //Used for 8006
                                     );
                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);

                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && ((dtEvent.Rows[0]["ActivityType"].ToString() == "VoiceInstall") || (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSInstall")))
                    {
                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"].ToString()),
                                new XElement("H1", dtEvent.Rows[0]["H1"]),
                                new XElement("voice_data_flg", (dtEvent.Rows[0]["ActivityType"].ToString() == "VoiceInstall") ? "V" : "D")
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device = new XElement("DeviceActivation",
                                    new XElement("SiteID", dtEvent.Rows[0]["SITE_ID_TXT"]),
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("Service_Tier", dtDevice.Rows[i]["ServiceTier"]),
                                    new XElement("H5_H6_Cust_ID", ((dtDevice.Rows[i]["device_id"].ToString().Length > 0) ? dtDevice.Rows[i]["OdieH6"] : dtEvent.Rows[0]["H5_H6_Cust_ID"])),
                                    new XElement("Address", dtEvent.Rows[0]["Address"]),
                                    new XElement("Floor", dtEvent.Rows[0]["Floor"]),
                                    new XElement("City", dtEvent.Rows[0]["City"]),
                                    new XElement("State", dtEvent.Rows[0]["State"]),
                                    new XElement("ZipCode", dtEvent.Rows[0]["ZipCode"]),
                                    new XElement("Country", dtEvent.Rows[0]["Country"]),
                                    new XElement("ServiceAssuranceContactPhone", dtEvent.Rows[0]["ServiceAssuranceContactPhone"]),
                                    new XElement("ServiceAssuranceContactName", dtEvent.Rows[0]["ServiceAssuranceContactName"]),
                                    new XElement("ServiceAssuranceContactHours", dtEvent.Rows[0]["ServiceAssuranceContactHours"]),   //vn370313   0303217
                                    new XElement("ServiceAssuranceTimeZone", dtEvent.Rows[0]["ServiceAssuranceTimeZone"]),           //vn370313   0303217
                                    new XElement("Domestic_International_Flag", dtEvent.Rows[0]["Domestic_International_Flag"]),
                                    new XElement("CoverageBy", dtDevice.Rows[i]["CoverageBy"]),
                                    new XElement("SerialNumber", dtDevice.Rows[i]["SerialNumber"]),
                                    new XElement("ContractNumber", dtDevice.Rows[i]["ContractNumber"]),
                                    new XElement("ContractType", dtDevice.Rows[i]["ContractType"]),
                                    new XElement("VendorExpirationDate", dtDevice.Rows[i]["VendorExpirationDate"])
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && ((dtEvent.Rows[0]["ActivityType"].ToString() == "VoiceChange") || (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACChange")))
                    {
                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"].ToString()),
                                new XElement("voice_data_flg", (dtEvent.Rows[0]["ActivityType"].ToString() == "VoiceChange") ? "V" : "D")
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device = new XElement("DeviceActivation",
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Service_Tier", dtDevice.Rows[i]["ServiceTier"]),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"])
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        if (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACChange")
                            _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && ((dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACSwap") || (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACGuest")))
                    {
                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"].ToString()),
                                new XElement("voice_data_flg", "D")
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device;
                                if (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACGuest")
                                    _Device = new XElement("DeviceActivation",
                                    new XElement("SiteID", dtEvent.Rows[0]["SITE_ID_TXT"]),
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Service_Tier", dtDevice.Rows[i]["ServiceTier"]),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("H5_H6_Cust_ID", ((dtDevice.Rows[i]["OdieH6"].ToString().Length > 0) ? dtDevice.Rows[i]["OdieH6"] : dtEvent.Rows[0]["H5_H6_Cust_ID"])),
                                    new XElement("Domestic_International_Flag", dtEvent.Rows[0]["Domestic_International_Flag"]),
                                    new XElement("CoverageBy", dtDevice.Rows[i]["CoverageBy"]),
                                    new XElement("SerialNumber", dtDevice.Rows[i]["SerialNumber"]),
                                    new XElement("ContractNumber", dtDevice.Rows[i]["ContractNumber"]),
                                    new XElement("ContractType", dtDevice.Rows[i]["ContractType"]),
                                    new XElement("VendorExpirationDate", dtDevice.Rows[i]["VendorExpirationDate"])
                                    );
                                else
                                    _Device = new XElement("DeviceActivation",
                                    new XElement("SiteID", dtEvent.Rows[0]["SITE_ID_TXT"]),
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Service_Tier", dtDevice.Rows[i]["ServiceTier"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("H5_H6_Cust_ID", ((dtDevice.Rows[i]["OdieH6"].ToString().Length > 0) ? dtDevice.Rows[i]["OdieH6"] : dtEvent.Rows[0]["H5_H6_Cust_ID"])),
                                    new XElement("CoverageBy", dtDevice.Rows[i]["CoverageBy"]),
                                    new XElement("SerialNumber", dtDevice.Rows[i]["SerialNumber"]),
                                    new XElement("ContractNumber", dtDevice.Rows[i]["ContractNumber"]),
                                    new XElement("ContractType", dtDevice.Rows[i]["ContractType"]),
                                    new XElement("ServiceAssuranceContactHours", dtEvent.Rows[0]["ServiceAssuranceContactHours"]),
                                    new XElement("ServiceAssuranceTimeZone", dtEvent.Rows[0]["ServiceAssuranceTimeZone"]),
                                    new XElement("VendorExpirationDate", dtDevice.Rows[i]["VendorExpirationDate"])
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else if ((dtEvent.Rows.Count > 0) && (dtEvent.Rows[0]["ActivityType"].ToString() == "MDSMACMove"))
                    {
                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                                new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                                new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                                new XElement("ActivityType", dtEvent.Rows[0]["ActivityType"].ToString()),
                                new XElement("voice_data_flg", "D")
                                );

                        XElement _DeviceList = new XElement("DeviceActivationList");
                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device = new XElement("DeviceActivation",
                                    new XElement("SiteID", dtEvent.Rows[0]["SITE_ID_TXT"]),
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("H5_H6_Cust_ID", ((dtDevice.Rows[i]["device_id"].ToString().Length > 0) ? dtDevice.Rows[i]["OdieH6"] : dtEvent.Rows[0]["H5_H6_Cust_ID"])),
                                    new XElement("Address", dtEvent.Rows[0]["Address"]),
                                    new XElement("Floor", dtEvent.Rows[0]["Floor"]),
                                    new XElement("City", dtEvent.Rows[0]["City"]),
                                    new XElement("State", dtEvent.Rows[0]["State"]),
                                    new XElement("ZipCode", dtEvent.Rows[0]["ZipCode"]),
                                    new XElement("Country", dtEvent.Rows[0]["Country"]),
                                    new XElement("ServiceAssuranceContactPhone", dtEvent.Rows[0]["ServiceAssuranceContactPhone"]),
                                    new XElement("ServiceAssuranceContactName", dtEvent.Rows[0]["ServiceAssuranceContactName"]),
                                    new XElement("CoverageBy", dtDevice.Rows[i]["CoverageBy"]),
                                    new XElement("SerialNumber", dtDevice.Rows[i]["SerialNumber"]),
                                    new XElement("ContractNumber", dtDevice.Rows[i]["ContractNumber"]),
                                    new XElement("ContractType", dtDevice.Rows[i]["ContractType"]),
                                    new XElement("VendorExpirationDate", dtDevice.Rows[i]["VendorExpirationDate"])
                                    );

                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);
                        _DCMessage.Add(GetDCServiceXML(ref dtService));
                        return _DCMessage.ToString();
                    }
                    else// if (dtEvent.Rows[0]["ActivityType"].ToString() == "MAC-Change2")
                    {
                        if (dtFTN.Rows.Count > 0)
                        {
                            _FTN = dtFTN.Rows[0]["FTN"].ToString();
                            _ServiceTier = dtFTN.Rows[0]["Service_Tier"].ToString();
                        }

                        XMLHelper _XHelper = new XMLHelper();

                        XElement _DCMessage = new XElement("DesignComplete",
                            new XElement("TranID", dtEvent.Rows[0]["TranID"]),
                            new XElement("EventID", dtEvent.Rows[0]["EventID"]),
                            new XElement("ActivityType", "MAC-Change")
                            );

                        XElement _DeviceList = new XElement("DeviceActivationList");

                        if (dtDevice.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDevice.Rows.Count; i++)
                            {
                                XElement _Device;

                                _Device = new XElement("DeviceActivation",
                                    new XElement("device_id", dtDevice.Rows[i]["device_id"]),
                                    new XElement("Mach5DeviceID", dtDevice.Rows[i]["Mach5DeviceID"]),
                                    new XElement("Service_Tier", (dtEvent.Rows[0]["ActivityType"].ToString() == "MAC-Change2") ? _ServiceTier : dtDevice.Rows[i]["ServiceTier"]),
                                    new XElement("EquipType", string.Empty),
                                    new XElement("Redesign_Flag", dtDevice.Rows[i]["Redesign_Flag"]),
                                    new XElement("RedesignNumber", dtDevice.Rows[i]["RedesignNumber"]),
                                    new XElement("PrimaryVendorName", dtDevice.Rows[i]["PrimaryVendorName"]),
                                    new XElement("Equipment_Install_Date", dtDevice.Rows[i]["Equipment_Install_Date"]),
                                    new XElement("FTN", _FTN),
                                    new XElement("Mach5_Flag", string.Empty) //Used for 8006
                                     );
                                _DeviceList.Add(_Device);
                            }
                        }

                        _DCMessage.Add(_DeviceList);

                        return _DCMessage.ToString();
                    }
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "GenerateDCRequest() for REQ_ID " + REQ_ID, ex.Message + " ; " + ex.StackTrace);
                return string.Empty;
            }
        }

        private string GenerateDiscoRequest(DataTable _DTDisco)
        {
            DataRow[] _DRDisco = _DTDisco.Select("REQ_ID = " + REQ_ID);
            ODIEDiscoRequest _Disco = new ODIEDiscoRequest();
            _Disco.TranID = REQ_ID;
            _Disco.Req_Dt = DateTime.Now;

            int i = 0;
            int _RowCount = _DRDisco.Length;
            Devices[] _DeviceList = new Devices[_RowCount];

            foreach (DataRow _DR in _DRDisco)
            {
                Devices _Device = new Devices();
                _Device.Dev_Req_ID = _DR["ODIE_DISC_REQ_ID"].ToString();
                _Device.Device_Name = _DR["DEV_NME"].ToString();
                _DeviceList[i] = _Device;
                i += 1;
            }

            _Disco.DeviceList = _DeviceList;
            XmlSerializer _Serializer = new XmlSerializer(typeof(ODIEDiscoRequest));
            return SerializeObject<ODIEDiscoRequest>(Encoding.UTF8, _Disco);
        }

        private XElement GetDCServiceXML(ref DataTable dtIn)
        {
            XElement _ServiceTypeList = new XElement("ServiceTypeList");

            if (dtIn.Rows.Count > 0)
            {
                for (int i = 0; i < dtIn.Rows.Count; i++)
                {
                    XElement _Service;
                    _Service = new XElement("ServiceType",
                                new XElement("service_device_id", dtIn.Rows[i]["service_device_id"]),
                                new XElement("MACH5_H6_SRVC_ID", dtIn.Rows[i]["MACH5_H6_SRVC_ID"]),
                                new XElement("ServiceTypeSelected", dtIn.Rows[i]["ServiceType"]),
                                new XElement("ThirdPartyVendor", dtIn.Rows[i]["ThirdPartyVendor"]),
                                new XElement("ThirdPartyID", dtIn.Rows[i]["ThirdPartyID"]),
                                new XElement("ThirdPartyServiceLevel", dtIn.Rows[i]["ThirdPartyServiceLevel"]),
                                new XElement("ActivationDate", dtIn.Rows[i]["ActivationDate"]),
                                new XElement("Comments", dtIn.Rows[i]["Comments"].ToString().Replace("<br/>", "\u00A0"))
                                 );
                    _ServiceTypeList.Add(_Service);
                }
            }
            return _ServiceTypeList;
        }

        private string GenerateCPEInfoMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtCustomer = new DataTable();
                DataTable dtContact = new DataTable();
                DataTable dtOrderLevel = new DataTable();
                DataTable dtLineItem = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getODIECPEMsgInfo(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    dtCustomer = ds.Tables[1];
                    dtContact = ds.Tables[2];
                    dtOrderLevel = ds.Tables[3];
                    dtLineItem = ds.Tables[4];
                    dtNotes = ds.Tables[5];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrder",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(REQ_ID))
                            );
                    XElement _orderInfo = new XElement("OrderInformation",
                           new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                           new XElement("OrderType", dtOrder.Rows[0]["OrderType"]),
                           new XElement("OrderAction", dtOrder.Rows[0]["OrderAction"]),
                           new XElement("ProductType", dtOrder.Rows[0]["ProductType"]),
                           new XElement("DomesticInternationalFlag", dtOrder.Rows[0]["DomesticInternationalFlag"]),
                           new XElement("CSGLevel", dtOrder.Rows[0]["CSGLevel"]),
                           new XElement("ParentOrderID", dtOrder.Rows[0]["ParentOrderID"]),
                           new XElement("RelatedOrderID", dtOrder.Rows[0]["RelatedOrderID"]),
                           new XElement("CustomerCommitDate", dtOrder.Rows[0]["CustomerCommitDate"]),
                           new XElement("CustomerPremiseCurrentlyOccupiedFlag", dtOrder.Rows[0]["CustomerPremiseCurrentlyOccupiedFlag"]),
                           new XElement("SiteID", dtOrder.Rows[0]["SiteID"])
                           );
                    _cpeMessage.Add(_orderInfo);

                    XElement _cpeCustInfo = new XElement("CustomerInfo");

                    if (dtCustomer.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCustomer.Rows.Count; i++)
                        {
                            XElement _hierachyInfo = new XElement("CISHierarchyInfo",
                               new XElement("LevelType", dtCustomer.Rows[i]["LevelType"]),
                               new XElement("CustomerID", dtCustomer.Rows[i]["CustomerID"]),
                               new XElement("CustomerName", dtCustomer.Rows[i]["CustomerName"])
                               );

                            XElement _addressInfo = new XElement("Address",
                               new XElement("AddressLine1", dtCustomer.Rows[i]["AddressLine1"]),
                               new XElement("AddressLine2", dtCustomer.Rows[i]["AddressLine2"]),
                               new XElement("AddressLine3", dtCustomer.Rows[i]["AddressLine3"]),
                               new XElement("Suite", dtCustomer.Rows[i]["Suite"]),
                               new XElement("Building", dtCustomer.Rows[i]["Building"]),
                               new XElement("Floor", dtCustomer.Rows[i]["Floor"]),
                               new XElement("Room", dtCustomer.Rows[i]["Room"]),
                               new XElement("City", dtCustomer.Rows[i]["City"]),
                               new XElement("StateCode", dtCustomer.Rows[i]["StateCode"]),
                               new XElement("ZIPPostalCode", dtCustomer.Rows[i]["ZIPPostalCode"]),
                               new XElement("CntryCode", dtCustomer.Rows[i]["CntryCode"]),
                               new XElement("ProvinceMuncipality", dtCustomer.Rows[i]["ProvinceMuncipality"])
                               );

                            XElement _phoneInfo = new XElement("PhoneInfo",
                              new XElement("PhoneType", dtCustomer.Rows[i]["PhoneType"]),
                              new XElement("CountryCode", dtCustomer.Rows[i]["CountryCode"]),
                              new XElement("NPA", dtCustomer.Rows[i]["NPA"]),
                              new XElement("NXX", dtCustomer.Rows[i]["NXX"]),
                              new XElement("Station", dtCustomer.Rows[i]["Station"]),
                              new XElement("CityCode", dtCustomer.Rows[i]["CityCode"]),
                              new XElement("Number", dtCustomer.Rows[i]["Number"])
                              );

                            _hierachyInfo.Add(_addressInfo);
                            _hierachyInfo.Add(_phoneInfo);
                            _cpeCustInfo.Add(_hierachyInfo);
                        }
                    }

                    if (dtContact.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtContact.Rows.Count; i++)
                        {
                            XElement _contactInfo = new XElement("ContactInfo");
                            _contactInfo.Add(new XElement("Type", dtContact.Rows[i]["Type"]));

                            XElement _cntctNameInfo = new XElement("ContactNameInfo",
                             new XElement("FirstName", dtContact.Rows[i]["FirstName"]),
                             new XElement("LastName", dtContact.Rows[i]["LastName"]),
                             new XElement("Name", dtContact.Rows[i]["Name"]),
                             new XElement("EmailAddress", dtContact.Rows[i]["EmailAddress"]));

                            _contactInfo.Add(_cntctNameInfo);

                            _contactInfo.Add(
                               new XElement("AddressLine1", dtContact.Rows[i]["AddressLine1"]),
                               new XElement("AddressLine2", dtContact.Rows[i]["AddressLine2"]),
                               new XElement("City", dtContact.Rows[i]["City"]),
                               new XElement("StateCode", dtContact.Rows[i]["StateCode"]),
                               new XElement("ZIPPostalCode", dtContact.Rows[i]["ZIPPostalCode"]),
                               new XElement("CntryCode", dtContact.Rows[i]["CntryCode"]),
                               new XElement("ProvinceMuncipality", dtContact.Rows[i]["ProvinceMuncipality"])

                               );

                            XElement _cntctphone = new XElement("PhoneInfo",
                              new XElement("PhoneType", dtContact.Rows[i]["PhoneType"]),
                              new XElement("CountryCode", dtContact.Rows[i]["CountryCode"]),
                              new XElement("NPA", dtContact.Rows[i]["NPA"]),
                              new XElement("NXX", dtContact.Rows[i]["NXX"]),
                              new XElement("Station", dtContact.Rows[i]["Station"]),
                              new XElement("CityCode", dtContact.Rows[i]["CityCode"]),
                              new XElement("Number", dtContact.Rows[i]["Number"]),
                              new XElement("Extension", dtContact.Rows[i]["Extension"])
                              );

                            _contactInfo.Add(_cntctphone);
                            _cpeCustInfo.Add(_contactInfo);
                        }

                        _cpeMessage.Add(_cpeCustInfo);
                    }

                    XElement _itemlist = new XElement("CPEInformation");

                    XElement _cpeInfo = new XElement("CPEInfo",
                                new XElement("CPEOrderTypeCode", dtOrderLevel.Rows[0]["CPEOrderTypeCode"]),
                                new XElement("EquipmentOnlyFlagCode", dtOrderLevel.Rows[0]["EquipmentOnlyFlagCode"]),
                                new XElement("RecordsOnlyOrderFlagCode", dtOrderLevel.Rows[0]["RecordsOnlyOrderFlagCode"]),
                                new XElement("AccessProviderCode", dtOrderLevel.Rows[0]["AccessProviderCode"]),
                                new XElement("CPEPhoneNumber", dtOrderLevel.Rows[0]["CPEPhoneNumber"]),
                                new XElement("SprintTestPhoneNumber", dtOrderLevel.Rows[0]["SprintTestPhoneNumber"]),
                                new XElement("ECCKTIdentifier", dtOrderLevel.Rows[0]["ECCKTIdentifier"]),
                                new XElement("ManagedServicesChannelProgramCode", dtOrderLevel.Rows[0]["ManagedServicesChannelProgramCode"]),
                                new XElement("DeliveryDuties", dtOrderLevel.Rows[0]["DeliveryDuties"])
                                );

                    if (dtLineItem.Rows.Count > 0)
                    {
                        XElement _lineInfo = new XElement("CPEEquipItems");

                        for (int i = 0; i < dtLineItem.Rows.Count; i++)
                        {
                            XElement _lineitem = new XElement("CPEItem",
                                new XElement("ItemAction", dtLineItem.Rows[i]["ItemAction"]),
                                new XElement("EquipmentTypeCode", dtLineItem.Rows[i]["EquipmentTypeCode"]),
                                new XElement("MatCode", dtLineItem.Rows[i]["MatCode"]),
                                new XElement("Description", dtLineItem.Rows[i]["Description"]),
                                new XElement("Manufacturer", dtLineItem.Rows[i]["Manufacturer"]),
                                new XElement("DeviceID", dtLineItem.Rows[i]["DeviceID"]),
                                new XElement("ProjectID", dtLineItem.Rows[i]["ProjectID"]),
                                new XElement("Quantity", dtLineItem.Rows[i]["Quantity"]),
                                new XElement("ContractType", dtLineItem.Rows[i]["ContractType"]),
                                new XElement("ContractTerm", dtLineItem.Rows[i]["ContractTerm"]),
                                new XElement("InstallationFlagCode", dtLineItem.Rows[i]["InstallationFlagCode"]),
                                new XElement("MaintenanceFlagCode", dtLineItem.Rows[i]["MaintenanceFlagCode"]),
                                new XElement("FMSCktNbr", dtLineItem.Rows[i]["FMSCktNbr"]),
                                new XElement("CompFamily", dtLineItem.Rows[i]["CompFamily"]),
                                new XElement("ComponentID", dtLineItem.Rows[i]["ComponentID"]),
                                new XElement("Reuse", dtLineItem.Rows[i]["Reuse"])
                                );

                            _lineInfo.Add(_lineitem);
                        }
                        _cpeInfo.Add(_lineInfo);
                    }

                    _itemlist.Add(_cpeInfo);
                    _cpeMessage.Add(_itemlist);

                    XElement _noteslist = new XElement("Notes");

                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            XElement _note = new XElement("Note",
                                new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                                );

                            _noteslist.Add(_note);
                        }
                    }

                    _cpeMessage.Add(_noteslist);
                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPEStusMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtCmpntIds = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getODIECPEStus(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    dtCmpntIds = ds.Tables[1];
                    dtNotes = ds.Tables[2];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderStatus",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(REQ_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                            new XElement("H6_CustomerID", dtOrder.Rows[0]["H6_CustomerID"]),
                            new XElement("OrderStatus", dtOrder.Rows[0]["OrderStatus"]),
                            new XElement("StatusDate", dtOrder.Rows[0]["StatusDate"]),
                           new XElement("DeviceID", dtOrder.Rows[0]["DeviceID"])
                           );

                    XElement _idlist = new XElement("Components");

                    if (dtCmpntIds.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCmpntIds.Rows.Count; i++)
                        {
                            XElement _id = new XElement("Component",
                                new XElement("ComponentID", dtCmpntIds.Rows[i]["ComponentID"])
                                );

                            _idlist.Add(_id);
                        }
                    }

                    _cpeMessage.Add(_idlist);

                    XElement _noteslist = new XElement("Notes");

                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            XElement _note = new XElement("Note",
                                new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                                );

                            _noteslist.Add(_note);
                        }
                    }

                    _cpeMessage.Add(_noteslist);
                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPTRequest()
        {
            ODIEShortNameValidationRequest _Info = new ODIEShortNameValidationRequest();
            _Info.CustomerName = FixNull(DR["CUST_NME"].ToString());
            _Info.TranID = REQ_ID;
            _Info.H1 = FixNull(DR["H1_CUST_ID"].ToString());
            _Info.RequestDateTime = DateTime.Now;

            XmlSerializer _Serializer = new XmlSerializer(typeof(ODIEShortNameValidationRequest));
            return SerializeObject<ODIEShortNameValidationRequest>(Encoding.UTF8, _Info);
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPEPRCVMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getODIECPEPRCV(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    dtNotes = ds.Tables[1];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderMaint",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(REQ_ID)),
                            new XElement("Type", "PRCV"),
                            new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                           new XElement("StatusDate", dtOrder.Rows[0]["StatusDate"]),
                           new XElement("DeviceID", dtOrder.Rows[0]["DeviceID"]),
                           new XElement("ContractType", dtOrder.Rows[0]["CntrcType"]),
                           new XElement("ContractTerm", dtOrder.Rows[0]["CntrcTerm"])
                           );

                    XElement _noteslist = new XElement("Notes");

                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            XElement _note = new XElement("Note",
                                new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                                );

                            _noteslist.Add(_note);
                        }
                    }

                    _cpeMessage.Add(_noteslist);
                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPEISMVMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getODIECPEISMV(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    dtNotes = ds.Tables[1];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderMaint",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(REQ_ID)),
                            new XElement("Type", "ISMV"),
                            new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                           new XElement("StatusDate", dtOrder.Rows[0]["StatusDate"]),
                           new XElement("DeviceID", dtOrder.Rows[0]["DeviceID"]),
                           new XElement("Building", dtOrder.Rows[0]["Building"]),
                           new XElement("Floor", dtOrder.Rows[0]["Floor"]),
                           new XElement("Room", dtOrder.Rows[0]["Room"])
                           );

                    XElement _noteslist = new XElement("Notes");

                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            XElement _note = new XElement("Note",
                                new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                                );

                            _noteslist.Add(_note);
                        }
                    }

                    _cpeMessage.Add(_noteslist);
                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateODIECpeReuseMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                //DataTable dtNotes = new DataTable();

                ds = _SenderDB.getODIECpeReUse(Convert.ToInt32(REQ_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    //dtNotes = ds.Tables[1];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderMaint",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(REQ_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                            new XElement("Type", "REUS"),//changed from ISMV
                            new XElement("StatusDate", dtOrder.Rows[0]["StatusDate"]),
                            new XElement("DeviceID", dtOrder.Rows[0]["DeviceID"]),
                            new XElement("OldNUA", dtOrder.Rows[0]["OldNUA"]),
                            new XElement("CurrentNUA", dtOrder.Rows[0]["CurrentNUA"])
                           );

                    //XElement _noteslist = new XElement("Notes");

                    //if (dtNotes.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtNotes.Rows.Count; i++)
                    //    {
                    //        XElement _note = new XElement("Note",
                    //            new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                    //            );

                    //        _noteslist.Add(_note);
                    //    }
                    //}

                    //_cpeMessage.Add(_noteslist);
                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        private void ApplyODIECustInfoCategoryTypes(ref ODIECustomerInfoRequestCategoryType[] _catType, DataRow dr)
        {
            ODIECustomerInfoRequestCategoryType odieData = new ODIECustomerInfoRequestCategoryType();
            ODIECustomerInfoRequestCategoryType odieConv = new ODIECustomerInfoRequestCategoryType();
            ODIECustomerInfoRequestCategoryType odieVoice = new ODIECustomerInfoRequestCategoryType();
            switch (Convert.ToInt32(DR["ODIE_CUST_INFO_REQ_CAT_TYPE_ID"]))
            {
                case 1:
                    {
                        //Data & Converged

                        odieData.Value = "D";
                        odieConv.Value = "C";
                        _catType[0] = odieData;
                        _catType[1] = odieConv;
                        break;
                    }
                case 2:
                    {
                        //Voice & Converged
                        odieVoice.Value = "V";
                        odieConv.Value = "C";
                        _catType[0] = odieVoice;
                        _catType[1] = odieConv;

                        break;
                    }
                case 3:
                    {
                        //Data & Converged
                        odieData.Value = "D";
                        odieVoice.Value = "V";
                        odieConv.Value = "C";
                        _catType[0] = odieData;
                        _catType[1] = odieVoice;
                        _catType[2] = odieConv;
                        break;
                    }
                default:
                    {
                        //Data & Converged
                        _catType[0].Value = "D";
                        _catType[1].Value = "C";
                        break;
                    }
            }
        }

        #endregion "Private Methods"
    }
}