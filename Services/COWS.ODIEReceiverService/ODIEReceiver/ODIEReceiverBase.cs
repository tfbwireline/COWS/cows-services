﻿namespace COWS.ODIEReceiverService
{
    public class ODIEReceiverBase
    {
        private bool _bStopThread = false;

        public bool bStopThread
        {
            get { return _bStopThread; }
            set { _bStopThread = value; }
        }

        private bool _canBeStoppedNow = true;

        public bool CanBeStoppedNow
        {
            get { return _canBeStoppedNow; }
            set { _canBeStoppedNow = value; }
        }
    }
}