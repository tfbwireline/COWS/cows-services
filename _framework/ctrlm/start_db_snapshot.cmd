@echo off
REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave 
REM
REM Script to remotely run DB job to drop/recreate the snapshot of the 
REM Database for the readonly site and for backup purposes
REM
REM
REM
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%

CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%

REM
REM validate %COMPUTERNAME% matches local config
REM 

CALL "%APP_HOME%\Scripts\perform-host-check.cmd"
IF ERRORLEVEL 1 (
  ECHO ERROR: host validation failed
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" start_db_snapshot.cmd 3 "ERROR: host validation failed"
  EXIT /b 3
)



REM
REM Run the passed job on the database
REM

%WINDIR%\System32\WindowsPowerShell\v1.0\powershell.exe "%APP_HOME%"\Scripts\ExecSqlServerAgentJob.ps1 -ConnectString '' -JobName 'WSPS-COWS-CreateSnapshot'

IF ERRORLEVEL 1 (
  ECHO ERROR: ExecSqlServerAgentJob.ps1 failed with exit code %ERRORLEVEL%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" ExecSqlServerAgentJob.ps1 6 "ERROR: ExecSqlServerAgentJob.ps1 failed"
  EXIT /b 6
)