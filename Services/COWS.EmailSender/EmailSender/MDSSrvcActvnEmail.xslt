﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-WORD;table-layout: fixed;" cellpadding="1">
          <tr>
            <td>

              <span>
                ViaSat Managed Wi-Fi Services,<br></br><br></br>

                The following activity was successfully completed on <b><xsl:value-of select="/EventMail/ActvDt" /></b>.<br></br><br></br>

                Service Type:  <b>Guest Wi-Fi</b><br></br>
                3rd Party Vendor:  <b>ViaSat</b><br></br>
                3rd Party ID:   <b><xsl:value-of select="/EventMail/THIRD_PARTY_ID" /></b><br></br>

                Requestor Contact Name:  <b><xsl:value-of select="/EventMail/REQOR_NME" /></b><br></br>
                Requestor Contact Phone:  <b><xsl:value-of select="/EventMail/REQOR_PHN_NBR" /></b><br></br><br></br><br></br>

                <br></br>
                T-Mobile Managed Network Services
              </span>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>