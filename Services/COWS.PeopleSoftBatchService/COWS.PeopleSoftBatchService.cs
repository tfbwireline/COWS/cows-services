﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;

//using PeopleSoftHelper;
//using PeopleSoftHelper.DB;
using CL = LogManager.FileLogger;

namespace COWS.PeopleSoftBatchService
{
    /// <summary>
    /// Service that runs the COWS PeopleSoft batch process.
    /// </summary>
    public partial class PeopleSoftBatchService : ServiceBase
    {
        //private int batchIntervalMinutes;
        //private Task peoplesoftBatchTask;
        //private CancellationTokenSource cancellationTokenSource;

        private string _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["RequestSleepTime"]);
        private int _iExecMinuteTimer = Convert.ToInt32(ConfigurationManager.AppSettings["ExecMinuteTimer"]);
        private Thread _SenderThread = null;

        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private ASCIIEncoding encoding = new ASCIIEncoding();

        public PeopleSoftBatchService()
        {
            //{
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
            //}
        }

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new PeopleSoftBatchService() };
            ServiceBase.Run(ServicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.PeopleSoftBatchService is starting.", "");

                    _bActive = true;
                    _bStopThread = false;
                    _bCanBeStoppedNow = true;
                    _SenderThread = new Thread(new ThreadStart(this.SendRequests));
                    CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send SCM Requests)", string.Empty);
                    _SenderThread.Start();
                }
                catch (Exception e)
                {
                    try
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.PeopleSoftBatchService. Error: " + e.ToString(), string.Empty);
                    }
                    catch { }
                }
                CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.PeopleSoftBatchService", "COWS.PeopleSoftBatchService Started at :" + DateTime.Now.ToString());
            }
        }

        protected override void OnStop()
        {
            {
                try
                {
                    base.OnStop();
                    _bStopThread = true;
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.Mach5SenderService", string.Empty);

                    _SenderThread.Join(TimeSpan.FromSeconds(30));
                    if (!_bCanBeStoppedNow)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                    }

                    CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send SCM  Requests)", string.Empty);
                    _SenderThread.Abort();
                    _SenderThread.Join();

                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.PeopleSoftBatchService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                }
                catch (Exception e)
                {
                    try
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.PeopleSoftBatchService. Error: " + e.ToString(), string.Empty);
                    }
                    catch { }
                }
            }
        }

        private void SendRequests()
        {
            PeopleSoftBatchIO _oIO = new PeopleSoftBatchIO();

            while (!_bStopThread)
            {
                if ((_bActive))
                //&& (DateTime.Now.Minute == 38))
                //&& (DateTime.Now.Minute == _iExecMinuteTimer))
                //&& (DateTime.Now.Second > 50))
                {
                    _oIO.ProcessRequests();  // Sends the Material Req's to Peoplesoft

                    _oIO.ProcessReceipts();  // Sends the Equip Receipts to Peoplesoft

                    _oIO.GetPO_Nbrs();      //Reads Peoplesoft DB for Purchase Order Info.

                    _oIO.TransferFiles();  // Transfers files to PeopleSoft.

                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Not Time for Service to run", "");
                    Thread.Sleep(_iRequestSleepTime);
                }
            }
        }
    }
}