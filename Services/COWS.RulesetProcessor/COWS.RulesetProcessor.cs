﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWS.RulesetProcessor
{
    /// <summary>
    /// Service that runs the COWS PeopleSoft batch process.
    /// </summary>
    public partial class RulesetProcessor : ServiceBase
    {
        private string _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);
        private string[] _execAppListArr = ConfigurationManager.AppSettings["ExecAppList"].ToString().Split(new char[] { ',' });
        private string[] _execMinuteTimerArr = ConfigurationManager.AppSettings["ExecMinuteTimer"].ToString().Split(new char[] { ',' });
        private string[] _execHourTimerArr = ConfigurationManager.AppSettings["ExecHourTimer"].ToString().Split(new char[] { ',' });
        private string[] _execSecondTimerArr = ConfigurationManager.AppSettings["ExecSecondTimer"].ToString().Split(new char[] { ',' });
        private string[] _execActiveThreadArr = ConfigurationManager.AppSettings["ExecActiveThread"].ToString().Split(new char[] { ',' });
        private Thread _Sender30RThread = null;
        private Thread _SenderEDWThread = null;
        private Thread _BPMRedesignThread = null;

        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        
        public RulesetProcessor()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new RulesetProcessor() };
            ServiceBase.Run(ServicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.RulesetProcessor is starting.", "");

                    _bActive = true;
                    _bStopThread = false;
                    _bCanBeStoppedNow = true;
                    _Sender30RThread = new Thread(new ThreadStart(this.Send30RRequests));
                    CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send 30R Requests)", string.Empty);
                    _Sender30RThread.Start();
                    _SenderEDWThread = new Thread(new ThreadStart(this.SendEDWRequests));
                    CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send EDW Requests)", string.Empty);
                    _SenderEDWThread.Start();
                    // km967761 - 02/02/2021 - BPM Redesign
                    _BPMRedesignThread = new Thread(new ThreadStart(this.SendBPMRedesignRequests));
                    CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send BPM Redesign Requests)", string.Empty);
                    _BPMRedesignThread.Start();
                }
                catch (Exception e)
                {
                    try
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.RulesetProcessor. Error: " + e.ToString(), string.Empty);
                    }
                    catch { }
                }
                CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.RulesetProcessor", "COWS.RulesetProcessor Started at :" + DateTime.Now.ToString());
            }
        }

        protected override void OnStop()
        {
            {
                try
                {
                    base.OnStop();
                    _bStopThread = true;
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.RulesetProcessor", string.Empty);

                    //_Sender30RThread.Join(TimeSpan.FromSeconds(30));
                    //_SenderEDWThread.Join(TimeSpan.FromSeconds(30));
                    _BPMRedesignThread.Join(TimeSpan.FromSeconds(30));
                    if (!_bCanBeStoppedNow)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                    }

                    //CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send30RRequests)", string.Empty);
                    //CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (SendEDWRequests)", string.Empty);
                    CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (SendBPMRequests)", string.Empty);
                    //_Sender30RThread.Abort();
                    //_Sender30RThread.Join();
                    //_SenderEDWThread.Abort();
                    //_SenderEDWThread.Join();
                    _BPMRedesignThread.Abort();
                    _BPMRedesignThread.Join();

                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.RulesetProcessor Stopped at: " + DateTime.Now.ToString(), string.Empty);
                }
                catch (Exception e)
                {
                    try
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.RulesetProcessor. Error: " + e.ToString(), string.Empty);
                    }
                    catch { }
                }
            }
        }

        private void Send30RRequests()
        {
            try
            {
                RulesetProcessorIO _oIO = new RulesetProcessorIO();
                int _appIndx30R = Array.IndexOf(_execAppListArr, "30R");
                bool _willExecuteThread = bool.Parse(_execActiveThreadArr[_appIndx30R]);

                while (!_bStopThread && _willExecuteThread)
                {
                    if ((_bActive)
                        && (DateTime.Now.Minute == int.Parse(_execMinuteTimerArr[_appIndx30R]))
                        && (DateTime.Now.Hour == int.Parse(_execHourTimerArr[_appIndx30R]))
                        && (DateTime.Now.Second == int.Parse(_execSecondTimerArr[_appIndx30R]))
                       )
                    {
                        _oIO.Process30RRequests("30R");
                        Thread.Sleep(_iRequestSleepTime);
                    }
                    else
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Not Time for 30R RulesetProcessor service to run", "");
                        Thread.Sleep(_iRequestSleepTime);
                    }
                }
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "30R Thread encountered an error: ", e.Message + e.StackTrace);
            }
        }

        private void SendEDWRequests()
        {
            try
            {
                RulesetProcessorIO _oIO = new RulesetProcessorIO();
                int _appIndxEDW = Array.IndexOf(_execAppListArr, "EDW");
                bool _willExecuteThread = bool.Parse(_execActiveThreadArr[_appIndxEDW]);

                while (!_bStopThread && _willExecuteThread)
                {
                    if ((_bActive)
                        && (DateTime.Now.Minute == int.Parse(_execMinuteTimerArr[_appIndxEDW]))
                        && (DateTime.Now.Hour == int.Parse(_execHourTimerArr[_appIndxEDW]))
                        && (DateTime.Now.Second == int.Parse(_execSecondTimerArr[_appIndxEDW]))
                       )
                    {
                        _oIO.ProcessEDWRequests("EDW");
                        Thread.Sleep(_iRequestSleepTime);
                    }
                    else
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Not Time for EDW RulesetProcessor service to run", "");
                        Thread.Sleep(_iRequestSleepTime);
                    }
                }
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "EDW Thread encountered an error: ", e.Message + e.StackTrace);
            }
        }

        private void SendBPMRedesignRequests()
        {
            try
            {
                BPMRedesignIO _oIO = new BPMRedesignIO();
                int _appIndxBPM = Array.IndexOf(_execAppListArr, "BPM");
                bool _willExecuteThread = bool.Parse(_execActiveThreadArr[_appIndxBPM]);

                while (!_bStopThread && _willExecuteThread)
                {

                    if ((_bActive)
                        && (DateTime.Now.Minute % int.Parse(_execMinuteTimerArr[_appIndxBPM]) == 0)
                        && (DateTime.Now.Second == int.Parse(_execSecondTimerArr[_appIndxBPM])))
                    {
                        //CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "DateTime.Now.Minute: " + DateTime.Now.Minute, "");
                        //CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "DateTime.Now.Second: " + DateTime.Now.Second, "");
                        //CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Minute: " + int.Parse(_execMinuteTimerArr[_appIndxBPM]), "");
                        //CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Second: " + int.Parse(_execSecondTimerArr[_appIndxBPM]), "");
                        _oIO.ProcessBPMRedesigns();
                        Thread.Sleep(_iRequestSleepTime);
                    }
                    else
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Not Time for BPM Redesign service to run", "");
                        Thread.Sleep(_iRequestSleepTime);
                    }
                }
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "BPM Redesign Thread encountered an error: ", e.Message + e.StackTrace);
            }
        }
    }
}