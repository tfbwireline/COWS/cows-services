﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Order ID : </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Order_ID" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Product Type : </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Product_Type" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Order Type :  </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Order_Type" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Vendor :  </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Vendor_Name" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Customer Want Date :  </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/CWD" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Customer Commit Date : </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/CCD" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">H5 Account Number: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/H5_Account_Number" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site City: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Site_City" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site Country: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Site_Country" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">SOL: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/SOL" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">OE: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/OE" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">SOTS Number: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/SOTS_Number" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Bill Cycle Number: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Bill_Cycle_Number" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Comments: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Comments" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Created By: </td>
            <td width="80%">
              <xsl:value-of select="NCCOOrderEmail/Created_By" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>