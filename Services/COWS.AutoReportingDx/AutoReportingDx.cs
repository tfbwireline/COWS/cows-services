﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Permissions;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using Excel = Microsoft.Office.Interop.Excel;
using Marshal = System.Runtime.InteropServices.Marshal;
using EmailManager;

namespace COWS.AutoReportingDx
{
    public class AutoReportDx : ServiceBase
    {
        public System.Timers.Timer tmrAutoReportingDx;
        public AutoReportingDB aDB = new AutoReportingDB();
        public AutoReportingDx.Connect.ConnectDB connectDB = new Connect.ConnectDB();
        public AutoReportingDx.Encrypt.EncryptFile encryptMe = new Encrypt.EncryptFile();
        public DateTime ueLastMailDate = DateTime.Now.AddMinutes(-10), dHeartbeatLastCheck = DateTime.Now;
        public bool bWhere = false;
        public int nLogWidthL = 0, nLogWidthR = 0, nLogWidth = 0, nLogFileDelAfterDays = 0, nLogLocal = 0, nNoOfEmails = 2;
        public int nTimerCycle = 0, nHeartbeatCycle = 0, nDataCurrMins = 0, nSMTPPort = 0, nTimeout = 600;
        public string sLineBreak = "", sLineApp = "", sWinSvcHeader = "", sServiceName = "", sServiceDisplayName = "";
        public string sLogFileName = "", sLogFilePath = "", sLogFilePathAlt = "", exMessage = "", sComputerName = "";
        public string sTempMsg = "", sMessage = "", sLogMessage = "", sLogMessageAlt = "", sLogVariables = "", sFolderPathAltCfg = "", sFolderPathCfg="";
        public string sAppServer = "", sEnvironment = "", sRunDatabase = "", sSQLConn = "", sSQLOLEDB = "", sDataSource = "", sSQL = "", sSQLD = "";
        public string sFTPServer = "", sFTPName = "", sFTPFileName = "", sFTPPath = "", sFTPFilePath = "", sFTPSaveTo = "", sTempFilePath = "";
        public string sSMTPHost = "", sSMTPUID = "", sSMTPPassword = "", sSMTPDefaultTo = "", sSMTPDefaultFrom = "", sSMTPDefaultCC = "", sSMTPDefaultBCC = "";
        public string ueMailTo = "", ueMailFrom = "", ueMailCC = "", ueMailBCC = "", ueDescription = "", ueMailSubject = "", ueAlert = "";
        public int nAdmin = 0, nSystemOn = 0, nRunning = 0, nCount = 0, nCount2 = 0, nCont = 1, nRptCnt = 0, nReportID = 0, nTemplateID = 0, nRowCnt = 0;
        public int nQueueID = 0, nGetEmailDefaults = 0, iRow = 0, nDependent = 0, nFTPID = 0, exitCode = 1, nExcelPID = 0;
        public int nAlertID = 0, nAlertError = 0, nSendEmail = 0, nReportEnabled = 0, nTemplateEnabled = 0;
        public string sReportName = "", sGroupAlias = "", sFileFormatCD = "", sConstantName = "", sFileExtension = "";
        public string sValue = "", sHTML = "", sPass = "", sRunDate = "", sIntervalTime = "", sEnabled = "", sRunNow = "", sFTP = "";
        public string sDevEmailTo = "", sDevEmailFrom = "", sDevEmailCC = "", sDevEmailBCC = "";

        public string sObjectCD = "", sRangeFirst = "", sRangeLast = "", sRangeCD = "", sRangeStyle = "", sExecCall = "";
        public string sFormula = "", sCondition = "", sStyle = "", sObject = "", sLastCol = "", sLastCell = "";
        public int nSheetGroupID = 0, nDataGroupID = 0, nFormulaGroupID = 0, nConditionGroupID = 0, nStyleGroupID = 0, nFormatGroupID = 0;
        public int nVariance = 0, nExecOrder = 0, nLastCol = 0, nLastRow = 0, nFailed = 0, nPTSheetNumber = 0, nPTID = 0, nRefreshDays = -15;
        public string sSQLE = "";
        public string sPTName = "", sPTFieldName = "", sPTCaption = "", sPTFieldType = "", sPTFunction = "", sPTFunctionFieldName = "";

        //FileManager variables:
        public int nRefresh = -1;

        private static void Main(string[] args)
        {
            //try
            //{
            //   System.ServiceProcess.ServiceBase[] ServicesToRun;
            //   ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AutoReportDx() };
            //   System.Threading.Thread.Sleep(5000);
            //   System.ServiceProcess.ServiceBase.Run(ServicesToRun);
            //}
            try
            {
                if (Environment.UserInteractive)
                {
                    AutoReportDx consoleApp = new AutoReportDx();
                    consoleApp.OnStart(args);
                    Console.WriteLine("Press any key to stop program");
                    Console.Read();
                    consoleApp.OnStop();
                }
                else
                {
                    System.ServiceProcess.ServiceBase[] ServicesToRun;
                    ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AutoReportDx() };
                    System.Threading.Thread.Sleep(500);
                    System.ServiceProcess.ServiceBase.Run(ServicesToRun);
                }
            }
            catch (Exception ex)
            {
                string mexMessage = (Convert.ToString(ex.Message) != "") ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                string mSQL = "", mueDescription = "ERROR::Main()";
                mueDescription += (mexMessage == "") ? "<br><br>EX MESSAGE: NULL" : "<br><br>EX MESSAGE: " + mexMessage;
                mueDescription += (mSQL == "") ? "<br><br>LAST SQL: NULL<br><br>" : ("<br><br>LAST SQL: " + mSQL.Replace("'", "") + "<br><br>");
                string mueAlert = "WINDOWS SERVICE INTERRUPTION";
                string mHTML = string.Format("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>{0}", Environment.NewLine);
                mHTML += string.Format("<html xmlns='http://www.w3.org/1999/xhtml'>{0}", Environment.NewLine);
                mHTML += string.Format("<head><title>Report Automation Dx Alert</title></head>{0}", Environment.NewLine);
                mHTML += string.Format("<body><table cellpadding=0 cellspacing=0 width=700 style='border-top: #000000 1px solid; border-right: #000000 1px solid; border-bottom: #000000 1px solid; border-left: #000000 1px solid;'>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 0px; border: none; width: 100%; background-color: #000000; background-position-x: 100%; background-image: url(http://www.sprint.com/assets/images/common/nav/bg_header_gradient.gif); background-repeat: no-repeat;' height=80 colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=''><img src='http://www.sprint.com/assets/images/common/nav/logo_header_en.gif' border=0></a></td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr bgcolor=Gainsboro><td style='padding: 0; border-spacing: 0; width: 100%; border-top: none; border-right: none; border-bottom: none; border-left: none; background-image: url(http://www.sprint.com/assets/images/common/bg_grey_tile_v2.gif);' height=20 colspan=2></td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><th style='padding-left: 7px; border-spacing: 0; border: none; width: 100%; background-color: #59639c; font-family: Verdana; font-size: 9pt; color: white;' height='22' colspan=2 align=left>Report Automation Dx Alert</th></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 0px; width: 100%; height: 10px;' colspan=2></td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>Date:</td>{0}", Environment.NewLine);
                mHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + DateTime.Now.ToString("g") + "</td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>Alert:</td>{0}", Environment.NewLine);
                mHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;'>" + mueAlert + "</td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right valign=top>Errors:</td>{0}", Environment.NewLine);
                mHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;' valign=top>" + mueDescription + "</td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("<tr><td style='padding: 0px; width: 100%; background-color: #ffe100; height: 7px;' colspan=2></td></tr>{0}", Environment.NewLine);
                mHTML += string.Format("</table></body></html>{0}", Environment.NewLine);
                mueDescription = "";
                //ErrorLogMess(3, mHTML);
                string _FromEmail, _EmailAddress, _EmailCC, _Subject, _EmailBody;
                MailMessage meMail = new MailMessage();
                _FromEmail = ((Convert.ToString(ConfigurationManager.AppSettings["SMTPDefaultFrom"])).Replace(" ", "")).TrimEnd(';');
                //meMail.From = new MailAddress(sTemp);
                _EmailAddress = ((Convert.ToString(ConfigurationManager.AppSettings["SMTPDefaultTo"])).Replace(" ", "")).TrimEnd(';');
                //string[] toArray = sTemp.Split(';');
                //foreach (string toEmail in toArray) { emailTemp = toEmail; if (emailTemp != "") meMail.To.Add(new MailAddress(emailTemp)); }
                _EmailCC = ((Convert.ToString(ConfigurationManager.AppSettings["SMTPDefaultCC"])).Replace(" ", "")).TrimEnd(';');
                //if (sTemp != "")
                //{
                //    string[] ccArray = sTemp.Split(';');
                //    foreach (string ccEmail in ccArray) { emailTemp = ccEmail; if (emailTemp != "") meMail.CC.Add(new MailAddress(emailTemp)); }
                //}
                //sTemp = ((Convert.ToString(ConfigurationManager.AppSettings["SMTPDefaultBCC"])).Replace(" ", "")).TrimEnd(';');
                //if (sTemp != "")
                //{
                //    string[] bccArray = sTemp.Split(';');
                //    foreach (string bccEmail in bccArray) { emailTemp = bccEmail; if (emailTemp != "") meMail.Bcc.Add(new MailAddress(emailTemp)); }
                //}
                _Subject = "*** Report Automation Dx Alert - Windows Service Interruption ***";
                //meMail.Subject = sTemp;
                //meMail.Priority = MailPriority.High;
                _EmailBody = mHTML.ToString();
                //meMail.IsBodyHtml = true;

                //SmtpClient client = new SmtpClient();
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential((Convert.ToString(ConfigurationManager.AppSettings["SMTPUID"])), (Convert.ToString(ConfigurationManager.AppSettings["SMTPPassword"])));
                //client.Port = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
                //client.Host = Convert.ToString(ConfigurationManager.AppSettings["SMTPHost"]);
                //client.Send(meMail);

                //EmailHelper._SendSingleEmail(true, MailPriority.High, _FromEmail, _EmailAddress, _EmailCC, string.Empty, _Subject, _EmailBody);
            }
        }

        public AutoReportDx()
        {
            InitializeComponent();
            CanPauseAndContinue = true;
            CanShutdown = true;
            CanStop = true;
        }

        private void InitializeComponent()
        {
            ServiceName = "COWS.AutoReportingDx";
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                int nReturn = 0;
                nReturn = InitializeVariables();
                if (nReturn == 0)
                {
                    //GetEmailDefaults();
                    if (nGetEmailDefaults == 0)
                    {
                        tmrAutoReportingDx = new System.Timers.Timer(nTimerCycle);
                        tmrAutoReportingDx.Start();
                        ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "OnStart() - Windows Service Started[L][B]");
                        tmrAutoReportingDx.Elapsed += new ElapsedEventHandler(tmrAutoReportingDx_Elapsed);
                    }
                    else { nReturn = 1; }
                }
                if (nReturn == 1) { ErrorLogMess(3, "[D]" + PadIt("Error:", " ", 1, 0) + "InitializeVariables() - *** ERROR ***[L][B]"); }
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::OnStart()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (Convert.ToString(ex.Message) != "") ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Windows Service', 'OnStart()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::OnStart()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        protected override void OnPause()
        {
            sLogMessage = "[D]" + PadIt("Message:", " ", 1, 0) + "OnPause() - Windows Service Paused[L][B]";
            ErrorLogMess(3, sLogMessage);
            base.OnPause();
        }

        protected override void OnContinue()
        {
            sLogMessage = "[D]" + PadIt("Message:", " ", 1, 0) + "OnContinue() - Windows Service Resumed[L][B]";
            ErrorLogMess(3, sLogMessage);
            base.OnContinue();
        }

        protected override void OnShutdown()
        {
            sLogMessage = "[D]" + PadIt("Message:", " ", 1, 0) + "OnShutdown() - Windows Service Shutting Down[L][B]";
            ErrorLogMess(3, sLogMessage);
            base.OnShutdown();
        }

        protected override void Dispose(bool disposing)
        {
            sLogMessage = "[D]" + PadIt("Message:", " ", 1, 0) + "Dispose() - Windows Service Disposing[L][B]";
            ErrorLogMess(3, sLogMessage);
            base.Dispose(disposing);
        }

        protected override void OnStop()
        {
            try
            {
                sLogMessage = "[L][B][L][D]" + PadIt("Message:", " ", 1, 0) + "OnStop() - Windows Service Stopped[L]";
                tmrAutoReportingDx.Dispose();
                if (nExcelPID != 0)
                {
                    Process[] pProcess;
                    pProcess = Process.GetProcessesByName("EXCEL");
                    foreach (Process p in Process.GetProcessesByName("EXCEL"))
                    {
                        if (p.Id == nExcelPID)
                        {
                            p.Kill();
                            nExcelPID = 0;
                            break;
                        }
                    }
                    sLogMessage += PadIt("=== Excel Forced Shut Down - Service Stopping ===", " ", 2, 0) + "[L]";
                }
                sLogMessage += PadIt("=== AUTO REPORTING Dx WINDOWS SERVICE STOPPED ===", " ", 2, 0) + "[L][B]";
                ErrorLogMess(3, sLogMessage);
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::OnStop()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString() + "; " + ex.StackTrace) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Windows Service', 'OnStop()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::OnStop()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public int InitializeVariables()
        {
            try
            {
                RegistryPermission rp = new RegistryPermission(RegistryPermissionAccess.Read, "HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet001\\Control\\ComputerName\\ComputerName");
                RegistryKey regKey = Registry.LocalMachine;
                regKey = regKey.OpenSubKey(@"SYSTEM\ControlSet001\Control\ComputerName\ComputerName", false);
                sComputerName = (regKey.GetValue("ComputerName").ToString()).ToUpper();
                sComputerName = (sComputerName == "") ? "" : (sComputerName.Replace(@"\\", ""));
                string sComputerInit = "";
                if (sComputerName != "") { sComputerInit = sComputerName.Substring(0, 1); }
                if (sComputerInit == "W") { nLogLocal = 1; }
                nLogWidthL = 30;
                nLogWidth = Convert.ToInt16(ConfigurationManager.AppSettings["LogFileWidth"]);
                if (nLogWidth <= 120) { nLogWidth = 120; }
                nLogWidthR = nLogWidth - nLogWidthL;
                string sTemp = "";
                sLineBreak = PadIt(sLineBreak, "-", 0, 0);
                sWinSvcHeader = " WINDOWS SERVICE: REPORT AUTOMATION Dx (RAD) ";
                sWinSvcHeader = PadIt(sTemp, "*", 10, ((nLogWidth - sWinSvcHeader.Length) / 2)) + sWinSvcHeader;
                sWinSvcHeader = PadIt(sWinSvcHeader, "*", 20, (nLogWidth - sWinSvcHeader.Length));
                sLineApp = (sLineBreak + Environment.NewLine + sWinSvcHeader + Environment.NewLine + sLineBreak + Environment.NewLine);
                string sConfigFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
                sConfigFile = sConfigFile.Substring((sConfigFile.LastIndexOf("\\")) + 1);
                sLogFileName = ConfigurationManager.AppSettings["LogFileName"].ToString();
                if (sLogFileName == "") { throw new System.Exception(sConfigFile + "::Log File Name - NULL"); }
                sLogFilePath = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                sLogFilePathAlt = ConfigurationManager.AppSettings["LogFilePathAlt"].ToString();
                //if (nLogLocal == 1) { sLogFilePath = @"C:\Documents and Settings\All Users\Desktop\"; sLogFilePathAlt = ""; }
                if (sLogFilePath == "") { throw new System.Exception(sConfigFile + "::Log File Path - NULL"); }
                else if (sLogFilePath.Substring(1, 1) != ":") sLogFilePath = @"\\" + sLogFilePath;
                if (!Directory.Exists(sLogFilePath)) { throw new System.Exception(sConfigFile + "::Log File Path - Invalid: " + sLogFilePath); }
                if (sLogFilePathAlt.Length != 0)
                {
                    if (sLogFilePathAlt.Substring(1, 1) != ":") { sLogFilePathAlt = @"\\" + sLogFilePathAlt; }
                    if (!Directory.Exists(sLogFilePathAlt)) { throw new System.Exception(sConfigFile + "::Alt Log File Path - Invalid: " + sLogFilePathAlt); }
                }
                nLogFileDelAfterDays = Convert.ToInt32(ConfigurationManager.AppSettings["LogFileDelAfterDays"]);
                nLogFileDelAfterDays = (nLogFileDelAfterDays < 1) ? 1 : nLogFileDelAfterDays;
                sFolderPathCfg = ConfigurationManager.AppSettings["FolderPath"].ToString();
                sFolderPathAltCfg = ConfigurationManager.AppSettings["FolderPathAlt"].ToString();
                sSMTPHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                if (sSMTPHost == "") { throw new System.Exception(sConfigFile + "::SMTP Host - NULL"); }
                nSMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                if (nSMTPPort == 0) { throw new System.Exception(sConfigFile + "::SMTP Port - 0"); }
                sSMTPUID = ConfigurationManager.AppSettings["SMTPUID"].ToString();
                if (sSMTPUID == "") { throw new System.Exception(sConfigFile + "::SMTP UID - NULL"); }
                sSMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                if (sSMTPPassword == "") { throw new System.Exception(sConfigFile + "::SMTP Password - NULL"); }
                sSMTPDefaultTo = ((ConfigurationManager.AppSettings["SMTPDefaultTo"].ToString()).Replace(" ", "")).TrimEnd(';');
                if (sSMTPDefaultTo == "") { throw new System.Exception(sConfigFile + "::SMTP Default To - NULL"); }
                sSMTPDefaultFrom = ((ConfigurationManager.AppSettings["SMTPDefaultFrom"].ToString()).Replace(" ", "")).TrimEnd(';');
                if (sSMTPDefaultFrom == "") { throw new System.Exception(sConfigFile + "::SMTP Default From - NULL"); }
                sSMTPDefaultCC = ((ConfigurationManager.AppSettings["SMTPDefaultCC"].ToString()).Replace(" ", "")).TrimEnd(';');
                sSMTPDefaultBCC = ((ConfigurationManager.AppSettings["SMTPDefaultBCC"].ToString()).Replace(" ", "")).TrimEnd(';');
                string sAppType = "";
                int nFound = 0;
                string assemblyName = Convert.ToString(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                //  string assemblyName = "COWS.AutoReportingDx";
                ServiceController[] svcs = ServiceController.GetServices();
                foreach (ServiceController svc in svcs)
                {
                    if (svc.ServiceName == assemblyName)
                    {
                        sServiceName = svc.ServiceName.ToString();
                        sServiceDisplayName = svc.DisplayName.ToString();
                        sAppType = svc.ServiceType.ToString();
                        nFound = 1;
                        break;
                    }
                }
                if (nFound == 0)
                {
                    sServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
                    if (sServiceName == "") { throw new System.Exception(sConfigFile + "::Service Name - NULL"); }
                    sServiceDisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"].ToString();
                    if (sServiceDisplayName == "") { throw new System.Exception(sConfigFile + "::Service Display Name - NULL"); }
                    sAppType = "Windows Service";
                }
                string sEPath = Directory.GetCurrentDirectory();
                sTempFilePath = ConfigurationManager.AppSettings["TempFilePath"].ToString();
                if (sTempFilePath == "") { sTempFilePath = sEPath; }
                string sEFullName = "", sEDate = "", sAppName = "";
                sAppName = sServiceName + ".exe";
                string[] fileEntries = Directory.GetFiles(sEPath, (sServiceName), SearchOption.TopDirectoryOnly);
                foreach (string fileName in fileEntries)
                {
                    FileInfo fp = new FileInfo(fileName);
                    sEFullName = fp.FullName;
                    sEDate = (fp.LastWriteTime).ToString();
                }
                nTimerCycle = Convert.ToInt32(ConfigurationManager.AppSettings["TimerCycle"]);
                if (nTimerCycle < 1) { nTimerCycle = 1; }
                nHeartbeatCycle = Convert.ToInt32(ConfigurationManager.AppSettings["HeartbeatCycle"]);
                if (nHeartbeatCycle < 15) { nHeartbeatCycle = 15; }
                sAppServer = ConfigurationManager.AppSettings["AppFileServer"].ToString();
                if (sAppServer == "") { throw new System.Exception(sConfigFile + "::App File Server - NULL"); }
                else if (sAppServer.Substring(1, 1) != ":") sAppServer = @"\\" + sAppServer;
                sDataSource = ConfigurationManager.AppSettings["SQLServer"].ToString();
                if (sDataSource == "") { throw new System.Exception(sConfigFile + "::SQL Server - NULL"); }
                switch (sDataSource.Substring(0, 2))
                {
                    case "PL": { sRunDatabase = "P"; sEnvironment = "Production"; break; }
                    case "PV": { sRunDatabase = "P"; sEnvironment = "Production"; break; }
                    case "TD": { sRunDatabase = "O"; sEnvironment = "ORT"; break; }
                    case "TV": { sRunDatabase = "T"; sEnvironment = "Test"; break; }
                    case "DV": { sRunDatabase = "D"; sEnvironment = "Development"; break; }
                    default: { sRunDatabase = "L"; sEnvironment = "Local"; break; }
                }
                if (sComputerInit == "P" && sRunDatabase == "P") { nSendEmail = 1; }
                sDevEmailFrom = ConfigurationManager.AppSettings["DevEmailFrom"].ToString();
                if ((nSendEmail == 0) && (sDevEmailFrom == "")) { throw new System.Exception(sConfigFile + "::Developer e-Mail From - NULL"); }
                sDevEmailTo = ConfigurationManager.AppSettings["DevEmailTo"].ToString();
                if ((nSendEmail == 0) && (sDevEmailTo == "")) { throw new System.Exception(sConfigFile + "::Developer e-Mail To - NULL"); }
                sDevEmailCC = ConfigurationManager.AppSettings["DevEmailCC"].ToString();
                sDevEmailBCC = ConfigurationManager.AppSettings["DevEmailBCC"].ToString();
                sSQLConn = ConfigurationManager.AppSettings["SQLConn"].ToString();
                if (sSQLConn == "") { throw new System.Exception(sConfigFile + "::SQL Conn - NULL"); }
                else
                {
                    sSQL = "SELECT TOP 1 * FROM RAD_Reporting.rad.Report WITH (NOLOCK)";
                    try { SqlDataReader drTest = connectDB.GetDR(sSQL, sSQLConn, nTimeout); drTest.Close(); sSQL = ""; }
                    catch (Exception ex) { sSQL = ""; throw new System.Exception(sConfigFile + "::SQL Conn - Invalid: " + sSQLConn + "; Exception - " + ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace); }
                }
                sSQLOLEDB = ConfigurationManager.AppSettings["SQLOLEDB"].ToString();
                if (sSQLOLEDB == "") { throw new System.Exception(sConfigFile + "::SQL OLEDB - NULL"); }
                else
                {
                    OleDbConnection oleConn = new OleDbConnection(sSQLOLEDB);
                    try { oleConn.Open(); oleConn.Close(); }
                    catch (Exception ex) { throw new System.Exception(sConfigFile + "::SQL OLEDB - Invalid: " + sSQLOLEDB + "; Exception - " + ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace); }
                }
                nDataCurrMins = Convert.ToInt16(ConfigurationManager.AppSettings["DataCurrMins"]);
                if (nDataCurrMins == 0) nDataCurrMins = 1;
                sFTPServer = ConfigurationManager.AppSettings["FTPServer"].ToString();
                if (sFTPServer == "") { throw new System.Exception(sConfigFile + "::FTP Server - NULL"); }
                sFTPFilePath = ConfigurationManager.AppSettings["FTPFilePath"];
                if (sFTPFilePath == "") { throw new System.Exception(sConfigFile + "::FTP Files Path - NULL"); }
                else if (sFTPFilePath.Substring(1, 1) != ":") sFTPFilePath = @"\\" + sFTPFilePath;
                if (!Directory.Exists(sFTPFilePath)) { throw new System.Exception(sConfigFile + "::FTP Files Path - Invalid: " + sFTPFilePath); }
                if (ConfigurationManager.AppSettings["CmdTimeout"] != null)
                    nTimeout = Int32.Parse(ConfigurationManager.AppSettings["CmdTimeout"].ToString());
                sLogMessage = sLineApp + "[D]" + PadIt("Function:", " ", 1, 0) + "InitializeVariables()[L]";
                sLogMessage += "[L]APPLICATION:[L]";
                sLogMessage += PadIt("Display Name:", " ", 1, 1) + sServiceDisplayName + "[L]";
                sLogMessage += PadIt("Assembly:", " ", 1, 1) + sAppName + "[L]";
                sLogMessage += PadIt("Type:", " ", 1, 1) + sAppType + "[L]";
                sLogMessage += PadIt("Date/Version:", " ", 1, 1) + sEDate + "[L]";
                sLogMessage += WrapIt((PadIt("Location:", " ", 1, 1) + sEFullName), 2, 1) + "[L]";
                sLogMessage += PadIt("Server / Log To:", " ", 1, 1) + (sComputerName + " / " + ((nLogLocal.ToString()).Replace("0", "Server").Replace("1", "Local"))) + "[L]";
                sLogMessage += PadIt("*Timer Cycle:", " ", 1, 1) + nTimerCycle + " mins (min 1)[L]";
                sLogMessage += PadIt("*Heartbeat Cycle:", " ", 1, 1) + nHeartbeatCycle + " mins (min 15)[L]";
                sLogMessage += "[L]APP/FILE SERVER[L]";
                sLogMessage += PadIt("App/File Server:", " ", 1, 1) + sAppServer.Replace(@"\\", "") + "[L]";
                sLogMessage += PadIt("Log File Path:", " ", 1, 1) + sLogFilePath.Replace(@"\\", "") + "[L]";
                sLogMessage += PadIt("*Alt Log File Path:", " ", 1, 1) + sLogFilePathAlt.Replace(@"\\", "") + "[L]";
                sLogMessage += WrapIt((PadIt("Temp File Path:", " ", 1, 1) + sTempFilePath), 2, 1) + "[L]";
                sLogMessage += PadIt("Delete After Days:", " ", 1, 1) + nLogFileDelAfterDays + "[L]";
                sLogMessage += PadIt("Log File Width:", " ", 1, 1) + nLogWidth + " (min 120)[L]";
                sLogMessage += "[L]SQL SERVER[L]";
                sLogMessage += PadIt("SQL Server:", " ", 1, 1) + sDataSource + "[L]";
                sLogMessage += PadIt("Environment / RunDB:", " ", 1, 1) + sEnvironment + " / " + sRunDatabase + "[L]";
                sLogMessage += WrapIt((PadIt("SQL Connection:", " ", 1, 1) + sSQLConn), 2, 1) + "[L]";
                sLogMessage += WrapIt((PadIt("SQLOLEDB Connection:", " ", 1, 1) + sSQLOLEDB), 2, 1) + "[L]";
                sLogMessage += PadIt("Data Current Minutes:", " ", 1, 1) + nDataCurrMins + "[L]";
                sLogMessage += "[L]FTP[L]";
                sLogMessage += PadIt("FTP Server:", " ", 1, 1) + sFTPServer + "[L]";
                sLogMessage += PadIt("FTP File(s) Path:", " ", 1, 1) + sFTPFilePath.Replace(@"\\", "") + "[L]";
                sLogMessage += "[L]E-MAIL[L]";
                sLogMessage += PadIt("SMTP Host:", " ", 1, 1) + sSMTPHost + "[L]";
                sLogMessage += PadIt("SMTP Port:", " ", 1, 1) + nSMTPPort + "[L]";
                sLogMessage += PadIt("SMTP UID:", " ", 1, 1) + sSMTPUID + "[L]";
                sLogMessage += WrapIt((PadIt("SMTP Default To:", " ", 1, 1) + sSMTPDefaultTo), 2, 1) + "[L]";
                sLogMessage += WrapIt((PadIt("SMTP Default From:", " ", 1, 1) + sSMTPDefaultFrom), 2, 1) + "[L]";
                sLogMessage += WrapIt((PadIt("*SMTP Default CC:", " ", 1, 1) + sSMTPDefaultCC), 2, 1) + "[L]";
                sLogMessage += WrapIt((PadIt("*SMTP Default BCC:", " ", 1, 1) + sSMTPDefaultBCC), 2, 1) + "[L]";
                if (nSendEmail == 0)
                {
                    sLogMessage += "[L]DEVELOPER NOTIFICATION[L]";
                    sLogMessage += PadIt("Dev eMail To:", " ", 1, 1) + sDevEmailTo + "[L]";
                    sLogMessage += PadIt("Dev eMail From:", " ", 1, 1) + sDevEmailFrom + "[L]";
                    sLogMessage += PadIt("Dev eMail CC:", " ", 1, 1) + sDevEmailCC + "[L]";
                    sLogMessage += PadIt("Dev eMail BCC:", " ", 1, 1) + sDevEmailBCC + "[L]";
                }
                sLogMessage += "[L]* Optional field[L]** Requires \"YYYY\", \"MM\" and \"DD\" in the log file name (any format)[L]*** All e-mail fields: Separate multiple e-mail addresses and/or distribution lists with a semicolon[L][B][L]";
                nTimerCycle = (nLogLocal == 0) ? (nTimerCycle *= 600) : 500;
                sLogMessage = CleanIt(sLogMessage);
                sLogVariables = sLogMessage;
                ErrorLogMess(3, sLogMessage);
                return 0;
            }
            catch (Exception ex)
            {
                ErrorLogEx(3, ex);
                //string lines = "ex.Message = " + ex.Message + "\r\n";
                //lines += "ex.StackTrace = " + ex.StackTrace + "\r\n\r\n";
                //lines += "sLogMessage = " + sLogMessage + "\r\n";
                //System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Documents and Settings\All Users\Desktop\RADx-ERROR.log");
                //file.WriteLine(lines);
                //file.Close();
                return 1;
            }
        }

        public void LogHeartbeat()
        {
            try
            {
                DateTime dLogIfBefore = DateTime.Now.AddMinutes(-nHeartbeatCycle);
                if (dHeartbeatLastCheck < dLogIfBefore)
                {
                    dHeartbeatLastCheck = DateTime.Now;
                    sLogMessage = PadIt("Heartbeat:", " ", 1, 0) + String.Format("{0:MM/dd/yy h:mm:ss tt}", DateTime.Now) + "[L][B]";
                    ErrorLogMess(3, sLogMessage);
                }
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::LogHeartbeat()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'LogHeartbeat()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::LogHeartbeat()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public void ErrorLogMess(int nLogType, string sMsg)
        {
            try
            {
                string sErr = CleanIt(sMsg);
                ErrorLog(nLogType, sErr);
            }
            catch (Exception ex)
            {
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ErrorLogMess()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::ErrorLogMess()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public void ErrorLogEx(int nLogType, Exception objEx)
        {
            try
            {
                string sErr = "[D]";
                sErr += WrapIt((PadIt("Error:", " ", 1, 0) + (objEx.Message.ToString().Trim()).Replace(Environment.NewLine, " ")), 2, 1) + "[L]";
                if (nReportID != 0) { sErr += WrapIt((PadIt("Process:", " ", 1, 0) + ("Report ID# " + nReportID + " - " + sReportName)), 2, 1) + "[L]"; }
                if ((sSQL != "") && (objEx.Message.IndexOf(".config") == -1)) { sErr += WrapIt((PadIt("Last SQL:", " ", 1, 0) + (sSQL)), 2, 1) + "[L]"; }
                string exStk = (objEx.StackTrace.ToString().Trim()).Replace("[", "").Replace("]", "");
                exStk = (exStk.Replace(Environment.NewLine, " ")).Replace("    at", " at");
                sErr += WrapIt((PadIt("Stack Trace:", " ", 1, 0) + (exStk)), 2, 1) + "[L]";
                sErr = CleanIt(sErr);
                ErrorLog(nLogType, sErr);
            }
            catch (Exception ex)
            {
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ErrorLogEx()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::ErrorLogEx()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public void ErrorLog(int nLogType, string sMess)
        {
            try
            {
                DateTime dCurrDay = DateTime.Now;
                string tFile = sLogFileName.Replace("YYYY", dCurrDay.Year.ToString()).Replace("MM", (String.Format("{0:MM}", dCurrDay))).Replace("DD", (String.Format("{0:dd}", dCurrDay)));
                for (int i = 0; i < 2; i++)
                {
                    string sMessage = "", tPath = "";
                    switch (nLogType)
                    {
                        case 1: { tPath = sLogFilePath; tFile = (tPath + tFile); i++; break; }
                        case 2: { if (sLogFilePathAlt != "") { tPath = sLogFilePathAlt; tFile = (tPath + tFile); } i++; break; }
                        case 3:
                            {
                                if ((i == 0) && (sLogFilePath != "")) { tPath = sLogFilePath; tFile = (tPath + tFile); }
                                else if (i == 1) { if (sLogFilePathAlt == "") { i++; break; } else { tPath = sLogFilePathAlt; tFile = (tPath + tFile); } }
                                break;
                            }
                    }
                    if (i < 2)
                    {
                        if (Directory.Exists(tPath) == true)
                        {
                            if ((!File.Exists(tFile)) && (sLogVariables != "") && (sMess.IndexOf("APPLICATION:") == -1)) { sMessage = sLogVariables; }
                            else if ((File.Exists(tFile) && (sLogVariables != "") && (sMess.IndexOf("APPLICATION:") != -1))) { sMessage = Environment.NewLine + Environment.NewLine; }
                            sMessage += sMess;
                            FileStream fs = new FileStream(tFile, FileMode.Append);
                            StreamWriter sw = new StreamWriter(fs);
                            try { sw.WriteLine(sMessage); sw.Flush(); sw.Close(); fs.Close(); dHeartbeatLastCheck = DateTime.Now; }
                            catch (Exception ex)
                            {
                                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                                int k = 0;
                                if ((exMessage.IndexOf("not enough space") != -1) || ((exMessage.IndexOf("cannot access") != -1) && (exMessage.IndexOf(".log ") != -1)))
                                {
                                    exMessage += "; Pausing service for 15 minutes."; k = 1;
                                }
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ErrorLog()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                                if (k == 1)
                                {
                                    sw.Flush(); sw.Close(); fs.Close();
                                    dHeartbeatLastCheck = DateTime.Now;
                                    System.Threading.Thread.Sleep(900000);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                nCont = 0;
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    int i = 0;
                    if ((exMessage.IndexOf("not enough space") != -1) || ((exMessage.IndexOf("cannot access") != -1) && (exMessage.IndexOf(".log ") != -1)))
                    {
                        exMessage += "; Pausing service for 15 minutes."; i = 1;
                    }
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ErrorLog()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    if (i == 1) { System.Threading.Thread.Sleep(900000); }
                }
                catch
                {
                    ueDescription = "ERROR::ErrorLog()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public string PadIt(string sStr, string sChar, int nSect, int nVal)
        {
            int nPad = 2 * nVal;
            switch (nSect)
            {
                case 0: for (int i = sStr.Length; i < (nLogWidth); i++) { sStr += sChar; } break;
                case 1:
                    if (nVal == 0) { for (int i = sStr.Length; i < (nLogWidthL); i++) { sStr += sChar; } break; }
                    else
                    {
                        for (int i = 0; i < nPad; i++) { sStr = sChar + sStr; }
                        nPad = nLogWidthL - sStr.Length;
                        for (int i = 0; i < nPad; i++) { sStr += sChar; }
                        break;
                    }
                case 2: for (int i = 0; i < (nLogWidthL + nPad); i++) { sStr = sChar + sStr; } break;
                case 10: for (int i = 0; i < nVal; i++) { sStr = sChar + sStr; } break;
                case 20: for (int i = 0; i < nVal; i++) { sStr += sChar; } break;
            }
            return sStr;
        }

        public string WrapIt(string sStr, int nSect, int nInd)
        {
            string sRet = "";
            while (sStr.Length > (nLogWidth))
            {
                sRet += (sStr.Substring(0, nLogWidth) + "[L]");
                sStr = PadIt((sStr.Substring(nLogWidth)), " ", nSect, nInd);
            }
            sRet += sStr;
            return sRet;
        }

        public string CleanIt(string sStr)
        {
            string sRet = "";
            sRet = sStr.Replace("[D]", (PadIt("Date/Time:", " ", 1, 0) + String.Format("{0:MM/dd/yy h:mm:ss tt}", DateTime.Now) + Environment.NewLine));
            sRet = sRet.Replace("[L]", Environment.NewLine);
            sRet = sRet.Replace("[B]", sLineBreak);
            return sRet;
        }

        protected void GetEmailDefaults()
        {
            try
            {
                sSQL = "SELECT LTRIM(RTRIM(eMailFrom)) 'eMailFrom', 'eMailTo'=CASE WHEN LTRIM(RTRIM(eMailToFailure)) > '' " +
                   "THEN LTRIM(RTRIM(eMailToFailure)) ELSE LTRIM(RTRIM(eMailTo)) END, LTRIM(RTRIM(eMailCC)) 'eMailCC', LTRIM(RTRIM(eMailBCC)) " +
                   "'eMailBCC', LTRIM(RTRIM(eMailSubject)) 'eMailSubject' FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE AlertCode='U'";
                SqlDataReader drEmailDefaults = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                drEmailDefaults.Read();
                if (drEmailDefaults.HasRows)
                {
                    ueMailSubject = (Convert.ToString(drEmailDefaults["eMailSubject"]));
                    if (nSendEmail == 1)
                    {
                        ueMailFrom = ((Convert.ToString(drEmailDefaults["eMailFrom"])).Replace(" ", "")).TrimEnd(';');
                        ueMailTo = ((Convert.ToString(drEmailDefaults["eMailTo"])).Replace(" ", "")).TrimEnd(';');
                        ueMailCC = ((Convert.ToString(drEmailDefaults["eMailCC"])).Replace(" ", "")).TrimEnd(';');
                        ueMailBCC = ((Convert.ToString(drEmailDefaults["eMailBCC"])).Replace(" ", "")).TrimEnd(';');
                    }
                    else
                    {
                        ueMailFrom = sDevEmailFrom;
                        ueMailTo = sDevEmailTo;
                        ueMailCC = sDevEmailCC;
                        ueMailBCC = sDevEmailBCC;
                    }
                }
                drEmailDefaults.Close();
                if (ueMailFrom == "" || ueMailTo == "" || (ueMailTo.ToLower()).IndexOf("sprint.com") == -1)
                {
                    nGetEmailDefaults = 1;
                    sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::GetEmailDefaults():Invalid Default e-Mail Field(s)[L]";
                    if (nSendEmail == 1) { sTempMsg += PadIt("Stop service, correct e-mail values in RAD_Reporting.rad.REPORTALERT_NEW (AlertCode='U') then restart.", " ", 2, 1) + "[L][B]"; }
                    else { sTempMsg += PadIt("Stop service, correct e-mail values in sDevEmail* variables then restart.", " ", 2, 1) + "[L][B]"; }
                    ErrorLogMess(3, sTempMsg);
                }
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::GetEmailDefaults()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'GetEmailDefaults()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::GetEmailDefaults()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        private void tmrAutoReportingDx_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (nGetEmailDefaults == 1) { tmrAutoReportingDx.Enabled = false; }
                else if (nRunning == 0)
                {
                    nRunning = 1;
                    tmrAutoReportingDx.Enabled = false;
                    //DataTable dtIfDecrypt = aDB.CheckIfDecryptWorking();
                    //if ((dtIfDecrypt == null) || (dtIfDecrypt.Rows.Count <= 0) || (dtIfDecrypt.Rows[0]["cust_nme"].ToString().Equals(string.Empty)))
                    //{
                    //    while (nNoOfEmails > 0)
                    //    {
                    //        sTempMsg = "Reporting COWS Database is missing the certificate/key, Please contact the DBA to run the script. ";
                    //        sTempMsg = sTempMsg + ((dtIfDecrypt == null) ? "DataTable is null" : ((dtIfDecrypt.Rows.Count <= 0) ? "DataTable doesnt have any rows" : "cust_nme is empty"));
                    //        ErrorLogMess(3, sTempMsg);
                    //        EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, "Reporting COWS Database is missing the certificate/key", "Reporting COWS Database is missing the certificate/key, Please contact the DBA to run the script.");
                    //        nNoOfEmails--;
                    //    }
                    //    nNoOfEmails = 2;
                    //}
                    //else
                    //{
                        //Run all reports at 5.35am, changed 'cows file manager/refresh web files' schedule from 6.30a to 6.00a
                        DateTime today = DateTime.Today;
                        DateTime dt = new DateTime(today.Year, today.Month, today.Day, 5, 35, 0);
                        TimeSpan ts = DateTime.Now - dt;
                        if ((ts.TotalMinutes > 0) && (ts.TotalMinutes < 30))
                            RunReports();
                        else
                        {
                            //Atleast one report to run
                            sSQL = "SELECT TOP 1 QUEUEID FROM rad.REPORTQUEUE_NEW WITH (NOLOCK) " +
                            "WHERE STARTTIME IS NULL AND ENDTIME IS NULL AND ISNULL(STATUS,'')='' " +
                            "AND CONVERT(DATE,RUNDATE)=CONVERT(DATE,GETDATE()) AND REPORTID!=0 ORDER BY 1 DESC;";
                            aDB.setCommandText(sSQL);
                            DataTable dtRptToRun = aDB.getDataTable();
                            if ((dtRptToRun!=null) && (dtRptToRun.Rows.Count>0))
                                RunReports();
                        }
                    //}
                    //DataTable dtRptDBStatus = aDB.GetRptDBStatus();
                    //if ((dtRptDBStatus != null) && (dtRptDBStatus.Rows.Count > 0)) //Run Reports only when COWS db is restored/replicated and online
                    //{
                    //    if (dtRptDBStatus.Rows[0]["Report"].ToString().Equals("UPD"))
                    //    {
                    //        DataTable dtIfDecrypt = aDB.CheckIfDecryptWorking();
                    //        if ((dtIfDecrypt == null) || (dtIfDecrypt.Rows.Count <= 0) || (dtIfDecrypt.Rows[0]["cust_nme"].ToString().Equals(string.Empty)))
                    //        {
                    //            while (nNoOfEmails > 0)
                    //            {
                    //                sTempMsg = "Reporting COWS Database is missing the certificate/key, Please contact the DBA to run the script. ";
                    //                sTempMsg = sTempMsg + ((dtIfDecrypt == null) ? "DataTable is null" : ((dtIfDecrypt.Rows.Count <= 0) ? "DataTable doesnt have any rows" : "cust_nme is empty"));
                    //                ErrorLogMess(3, sTempMsg);
                    //                EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, "Reporting COWS Database is missing the certificate/key", "Reporting COWS Database is missing the certificate/key, Please contact the DBA to run the script.");
                    //                nNoOfEmails--;
                    //            }
                    //        }
                    //        else
                    //            RunReports();
                    //    }
                    //    else
                    //    {
                    //        DateTime today = DateTime.Today;
                    //        DateTime dt = new DateTime(today.Year, today.Month, today.Day, 7, 30, 0);
                    //        TimeSpan ts = DateTime.Now - dt;
                    //        if ((ts.TotalMinutes > 0) && (ts.TotalMinutes < 90))
                    //        {
                    //            while (nNoOfEmails > 0)
                    //            {
                    //                sTempMsg = "Reporting COWS Database is out-of-date, Please contact the DBA to restore it from Primary.";
                    //                ErrorLogMess(3, sTempMsg);
                    //                EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, "Reporting COWS Database is out-of-date", "Reporting COWS Database is out-of-date, Please contact the DBA to restore it from Primary.");
                    //                nNoOfEmails--;
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    while (nNoOfEmails > 0)
                    //    {
                    //        sTempMsg = "Unable to get Reporting COWS Status, Please contact the DBA";
                    //        ErrorLogMess(3, sTempMsg);
                    //        EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, "Unable to get Reporting COWS Status", "Unable to get Reporting COWS Status, Please contact the DBA");
                    //        nNoOfEmails--;
                    //    }
                    //}
                    LogHeartbeat();
                    ProcessFiles();
                    LogHeartbeat();
                    tmrAutoReportingDx.Enabled = true;
                    nRunning = 0;
                }
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::tmrAutoReportingDx_Elapsed()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Windows Service', 'tmrAutoReportingDx_Elapsed()', 'ERROR: ex.Message=" + exMessage;
                    sSQLD += "; Timer will be stopped and restarted in 5 minutes to allow time for network or connectivity issues to be resolved.', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::tmrAutoReportingDx_Elapsed; Timer will be stopped and restarted in 5 minutes to allow time for network or connectivity issues to be resolved.";
                    SendAlertUnknown(exMessage);
                }
                finally { System.Threading.Thread.Sleep(300000); }
            }
        }

        private void CheckAlerts(string sAlertCode, int nCatch)
        {
            try
            {
                sSQL = "SELECT z.ID, z.OrderSeq, z.AlertCode, z.DataCheck, z.Cyclic FROM ( " +
                   "SELECT b.OrderSeq, b.AlertCode, b.Enabled, b.Complete, b.ID, b.Cyclic, CycleMeasure=COALESCE(b.CycleMeasure,''), CycleTime=COALESCE(b.CycleTime,''), " +
                   "Diff=CASE (b.Cyclic) WHEN 0 THEN 0 ELSE CASE (b.CycleMeasure) WHEN 'MI' THEN DATEDIFF(MI,COALESCE(b.LastCheck,DATEADD(MM,-61,GETDATE())),GETDATE()) " +
                   "WHEN 'HH' THEN DATEDIFF(HH,COALESCE(b.LastCheck,DATEADD(HH,-25,GETDATE())),GETDATE()) WHEN 'DD' " +
                   "THEN DATEDIFF(DD,COALESCE(b.LastCheck,DATEADD(DD,-2,GETDATE())),GETDATE()) ELSE 0 END END, " +
                   "b.IntervalTime, b.NotCompleteBy, b.Weekday, b.DayofMonth, b.LastCheck, b.DataCheck " +
                   "FROM RAD_Reporting.rad.ReportSystem a WITH (NOLOCK), RAD_Reporting.rad.REPORTALERT_NEW b WITH (NOLOCK) " +
                   "WHERE b.Enabled=1 AND b.Complete=0";
                switch (sAlertCode)
                {
                    case "": { break; }
                    case "Y": { sSQL += " AND b.AlertCode IN (SELECT DISTINCT AlertCode FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE Cyclic=1)"; break; }
                    case "R": { sSQL += " AND b.ReportID=" + nReportID; break; }
                    default: { sSQL += " AND b.AlertCode IN ('" + sAlertCode.Replace(",", "','") + "')"; break; }
                }
                sSQL += " AND (a.SysOnOff=1 OR COALESCE(b.NotCompleteBy,'') > '')" +
                   " AND (CAST(GETDATE() AS TIME(7)) > CAST(COALESCE(b.IntervalTime,'') AS TIME(7)))" +
                   " AND (CAST(GETDATE() AS TIME(7)) > CAST(COALESCE(b.NotCompleteBy,'') AS TIME(7)))" +
                   " AND (b.Weekday='0' OR CHARINDEX(CAST(DATEPART(WEEKDAY,GETDATE()) AS VARCHAR), b.Weekday,1) <> 0)" +
                   " AND (b.DayofMonth='0' OR CHARINDEX(CAST(DATEPART(DAY,GETDATE()) AS VARCHAR), b.DayofMonth,1) <> 0)) z ";
                if (nCatch == 0) sSQL += "WHERE (z.Cyclic=0 OR (z.Diff >= z.CycleTime)) AND z.AlertCode <> 'U' ";
                sSQL += "ORDER BY z.Cyclic DESC, z.OrderSeq";
                SqlDataReader drCheckAlerts = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drCheckAlerts.Read())
                {
                    int nAlertID = Convert.ToInt32(drCheckAlerts["ID"]);
                    string sAlert = Convert.ToString(drCheckAlerts["AlertCode"]);
                    string sDataCheck = Convert.ToString(drCheckAlerts["DataCheck"]);
                    int nCyclic = Convert.ToInt32(drCheckAlerts["Cyclic"]);
                    SqlDataReader drDataCheck = connectDB.GetDR(sDataCheck, sSQLConn, nTimeout);
                    drDataCheck.Read();
                    int nCheck = Convert.ToInt32(drDataCheck["CNT"]);
                    if (nCheck == 0)
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTALERT_NEW SET LastCheck=GETDATE()";
                        if ((nCyclic == 0) && (sAlert != "R") && (sAlert != "C")) sSQL += ", Complete=1";
                        sSQL += " WHERE ID=" + nAlertID;
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                    }
                    else SendAlert(nAlertID);
                    drDataCheck.Close();
                }
                drCheckAlerts.Close();
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::CheckAlerts()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckAlerts()', 'ERROR: ex.Message=" + exMessage + "', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::CheckAlerts()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        private void SendAlert(int nAlertID)
        {
            int nAlertReportID = 0, neMailAttachments = 0;
            double dAttSize = 0.0, dMaxSizeMbs = 0.0;
            string sAlertCode = "", sAlertName = "", sErrorOrComplete = "", sAlertIntervalTime = "", seMailFrom = "", seMailTo = "";
            string seMailCC = "", seMailBCC = "", seMailSubject = "", seMailDescription = "", sPostCheck = "", seMailPriority = "";
            string seMailDate = "", seMailCategoryLabel = "", seMailCategory = "", seMailDescriptionLabel = "", seMailTitle = "";
            string sTempFrom = "", sTempTo = "", sTempCC = "", sTempBCC = "";

            try
            {
                seMailDate = DateTime.Now.ToString("g");
                sSQL = "SELECT AlertCode, LTRIM(RTRIM(AlertName)) 'AlertName', ErrorOrComplete, ReportID, IntervalTime, LTRIM(RTRIM(eMailFrom)) 'eMailFrom', " +
                   "'eMailTo'=CASE WHEN LTRIM(RTRIM(eMailToFailure)) > '' THEN LTRIM(RTRIM(eMailToFailure)) ELSE LTRIM(RTRIM(eMailTo)) END, " +
                   "LTRIM(RTRIM(eMailCC)) 'eMailCC', LTRIM(RTRIM(eMailBCC)) 'eMailBCC', LTRIM(RTRIM(eMailSubject)) 'eMailSubject', eMailPriority, " +
                   "eMailAttachments, MaxSizeMbs, LTRIM(RTRIM(eMailDescription)) 'eMailDescription', LTRIM(RTRIM(PostCheck)) 'PostCheck' " +
                   "FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE ID=" + nAlertID;
                SqlDataReader drAlertData = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drAlertData.Read())
                {
                    sAlertCode = Convert.ToString(drAlertData["AlertCode"]);
                    sAlertName = Convert.ToString(drAlertData["AlertName"]);
                    sErrorOrComplete = Convert.ToString(drAlertData["ErrorOrComplete"]);
                    nAlertReportID = Convert.ToInt32(drAlertData["ReportID"]);
                    sAlertIntervalTime = Convert.ToString(drAlertData["IntervalTime"]);
                    seMailSubject = Convert.ToString(drAlertData["eMailSubject"]).Trim();
                    seMailFrom = ((Convert.ToString(drAlertData["eMailFrom"])).Replace(" ", "")).TrimEnd(';');
                    seMailTo = ((Convert.ToString(drAlertData["eMailTo"])).Replace(" ", "")).TrimEnd(';');
                    seMailCC = ((Convert.ToString(drAlertData["eMailCC"])).Replace(" ", "")).TrimEnd(';');
                    seMailBCC = ((Convert.ToString(drAlertData["eMailBCC"])).Replace(" ", "")).TrimEnd(';');
                    seMailPriority = Convert.ToString(drAlertData["eMailPriority"]).Trim();
                    neMailAttachments = Convert.ToInt16(drAlertData["eMailAttachments"]);
                    dMaxSizeMbs = Convert.ToDouble(drAlertData["MaxSizeMbs"]);
                    seMailDescription = Convert.ToString(drAlertData["eMailDescription"]).Trim();
                    sPostCheck = Convert.ToString(drAlertData["PostCheck"]).Trim();
                }
                drAlertData.Close();
                if (nSendEmail == 0) { sTempFrom = sDevEmailFrom; sTempTo = sDevEmailTo; sTempCC = sDevEmailCC; sTempBCC = sDevEmailBCC; }
                else { sTempFrom = seMailFrom; sTempTo = seMailTo; sTempCC = seMailCC; sTempBCC = seMailBCC; }
                if (nSendEmail == 0 || sComputerName.Substring(0, 1) != "P") { seMailSubject = "(TESTING) " + seMailSubject; }

                sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=Status + '; SMTP IN PROGRESS...' WHERE QueueID=" + nQueueID.ToString();
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                StringBuilder sbMailDescription = new StringBuilder();
                SqlDataReader drDescript = connectDB.GetDR(seMailDescription, sSQLConn, nTimeout);
                while (drDescript.Read()) { sbMailDescription.Append(Convert.ToString(drDescript["DESCRIPT"])); }
                drDescript.Close();

                if (neMailAttachments != 0)
                {
                    sSQL = "SELECT COALESCE(SUM(FileSize),0.00) AS 'AttSize' FROM RAD_Reporting.rad.ReportAttachment WITH (NOLOCK) WHERE Enabled=1 AND ID=" + nAlertID;
                    SqlDataReader drAttSize = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    while (drAttSize.Read())
                    {
                        dAttSize = Convert.ToDouble(drAttSize["AttSize"]);
                    }
                    drAttSize.Close();
                }
                if (dAttSize > dMaxSizeMbs)
                {
                    seMailTitle = "Report Automation Dx Alert";
                    seMailCategoryLabel = "Alert";
                    seMailCategory = sAlertName.ToUpper();
                    seMailDescriptionLabel = "Size Limit";
                    seMailSubject = seMailSubject.Replace("Notice", "Alert");
                    seMailSubject = "*** " + seMailSubject + " ***";
                    seMailSubject = seMailSubject.Replace("Complete ***", "SMTP Incomplete ***");
                    seMailPriority = "H";
                }
                else
                {
                    switch (seMailPriority)
                    {
                        case "H":
                            {
                                seMailTitle = "Report Automation Dx Alert";
                                seMailCategoryLabel = "Alert";
                                seMailCategory = sAlertName.ToUpper();
                                seMailDescriptionLabel = "Errors";
                                break;
                            }
                        case "N":
                            {
                                seMailTitle = (sErrorOrComplete == "E") ? "Report Automation Dx Warning" : "Report Automation Dx Notice";
                                seMailCategoryLabel = (sErrorOrComplete == "E") ? "Warning" : "Notice";
                                seMailCategory = sAlertName.ToUpper();
                                seMailDescriptionLabel = "Details";
                                break;
                            }
                        case "L":
                            {
                                seMailTitle = "Report Automation Dx Notice";
                                seMailCategoryLabel = "Notice";
                                seMailCategory = sAlertName.ToUpper();
                                seMailDescriptionLabel = "Details";
                                break;
                            }
                    }
                }

                sHTML = string.Format("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>{0}", Environment.NewLine);
                sHTML += string.Format("<html xmlns='http://www.w3.org/1999/xhtml'>{0}", Environment.NewLine);
                sHTML += string.Format("<head><title>" + seMailTitle + "</title></head>{0}", Environment.NewLine);
                sHTML += string.Format("<body><table cellpadding=0 cellspacing=0 width=700 style='border-top: #000000 1px solid; border-right: #000000 1px solid; border-bottom: #000000 1px solid; border-left: #000000 1px solid;'>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><td style='padding: 0px; border: none; width: 100%; background-color: #000000; background-position-x: 100%; background-image: url(http://www.sprint.com/assets/images/common/nav/bg_header_gradient.gif); background-repeat: no-repeat;' height=80 colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=''><img src='http://www.sprint.com/assets/images/common/nav/logo_header_en.gif' border=0></a></td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr bgcolor=Gainsboro><td style='padding: 0; border-spacing: 0; width: 100%; border-top: none; border-right: none; border-bottom: none; border-left: none; background-image: url(http://www.sprint.com/assets/images/common/bg_grey_tile_v2.gif);' height=20 colspan=2></td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><th style='padding-left: 7px; border-spacing: 0; border: none; width: 100%; background-color: #59639c; font-family: Verdana; font-size: 9pt; color: white;' height='22' colspan=2 align=left>" + seMailTitle + "</th></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><td style='padding: 0px; width: 100%; height: 10px;' colspan=2></td></tr>{0}", Environment.NewLine);
                if (nSendEmail == 0)
                {
                    sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>DEVELOPMENT:</td><td>&nbsp;</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail To:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + seMailTo + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail From:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + seMailFrom + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail CC:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + seMailCC + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding-bottom: 7px; border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail BCC:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding-left: 7px; padding-bottom: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + seMailBCC + "</td></tr>{0}", Environment.NewLine);
                }
                sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>Date:</td>{0}", Environment.NewLine);
                sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + seMailDate + "</td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>" + seMailCategoryLabel + ":</td>{0}", Environment.NewLine);
                sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;'>" + seMailCategory + "</td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right valign=top>" + seMailDescriptionLabel + ":</td>{0}", Environment.NewLine);
                sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;' valign=top>" + sbMailDescription + "</td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("<tr><td style='padding: 0px; width: 100%; background-color: #ffe100; height: 7px;' colspan=2></td></tr>{0}", Environment.NewLine);
                sHTML += string.Format("</table></body></html>{0}", Environment.NewLine);

                MailMessage eMail = new MailMessage();
                eMail.From = new MailAddress(sTempFrom);
                string emailTemp = "";
                string[] toArray = sTempTo.Split(';');
                foreach (string toEmail in toArray) { emailTemp = toEmail; if (emailTemp != "") eMail.To.Add(new MailAddress(emailTemp)); }
                if (sTempCC != "")
                {
                    string[] ccArray = sTempCC.Split(';');
                    foreach (string ccEmail in ccArray) { emailTemp = ccEmail; if (emailTemp != "") eMail.CC.Add(new MailAddress(emailTemp)); }
                }
                if (sTempBCC != "")
                {
                    string[] bccArray = sTempBCC.Split(';');
                    foreach (string bccEmail in bccArray) { emailTemp = bccEmail; if (emailTemp != "") eMail.Bcc.Add(new MailAddress(emailTemp)); }
                }
                switch (seMailPriority)
                {
                    case "H": { eMail.Priority = MailPriority.High; break; }
                    case "N": { eMail.Priority = MailPriority.Normal; break; }
                    case "L": { eMail.Priority = MailPriority.Low; break; }
                }
                eMail.Subject = seMailSubject;
                eMail.Body = sHTML.ToString();
                eMail.IsBodyHtml = true;

                //SmtpClient client = new SmtpClient();
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential(sSMTPUID, sSMTPPassword);
                //client.Port = nSMTPPort;
                //client.Host = sSMTPHost.ToString();
                //client.Send(eMail);
                //connectDB.ExecuteNonQuery(sPostCheck, sSQLConn, nTimeout);
                aDB.setCommandText(sPostCheck);
                aDB.execNoQuery();
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::SendAlert()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    if (neMailAttachments != 0)
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'SMTP IN PROGRESS...','SMTP FAILURE'), EndTime='" + DateTime.Now + "' WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                    }
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'SendAlert()', 'ERROR: ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::SendAlert()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        private void SendAlertUnknown(string sEx)
        {
            DateTime startTime = DateTime.Now;
            TimeSpan span = startTime.Subtract(ueLastMailDate);
            string sTempFrom = "", sTempTo = "", sTempCC = "", sTempBCC = "";

            try
            {
                if (span.Minutes > 5)
                {
                    ueDescription = (ueDescription == "") ? "ERROR::Unknown" : ueDescription;
                    ueDescription += (sEx == "") ? "<br><br>EX MESSAGE: NULL" : "<br><br>EX MESSAGE: " + sEx;
                    ueDescription += (sSQL == "") ? "<br><br>LAST SQL: N/A<br><br>" : ("<br><br>LAST SQL: " + sSQL.Replace("'", "") + "<br><br>");
                    if (ueDescription.IndexOf("tmrAutoReportingDx_Elapsed") != -1)
                    {
                        ueMailSubject = "*** Report Automation Dx Alert - Windows Service Interruption";
                        ueAlert = "WINDOWS SERVICE INTERRUPTION";
                    }
                    else
                    {
                        ueMailSubject = "*** Report Automation Dx Alert - Windows Service Unknown Error";
                        ueAlert = "WINDOWS SERVICE UNKNOWN ERROR";
                    }
                    ueMailSubject = ueMailSubject + ": " + sAppServer.Replace("\\", "") + " ***";

                    if (nSendEmail == 0) { sTempFrom = sDevEmailFrom; sTempTo = sDevEmailTo; sTempCC = sDevEmailCC; sTempBCC = sDevEmailBCC; }
                    else { sTempFrom = ueMailFrom; sTempTo = ueMailTo; sTempCC = ueMailCC; sTempBCC = ueMailBCC; }
                    if (nSendEmail == 0 || sComputerName.Substring(0, 1) != "P") { ueMailSubject = "(TESTING) " + ueMailSubject; }

                    sHTML = string.Format("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>{0}", Environment.NewLine);
                    sHTML += string.Format("<html xmlns='http://www.w3.org/1999/xhtml'>{0}", Environment.NewLine);
                    sHTML += string.Format("<head><title>Report Automation Dx Alert</title></head>{0}", Environment.NewLine);
                    sHTML += string.Format("<body><table cellpadding=0 cellspacing=0 width=700 style='border-top: #000000 1px solid; border-right: #000000 1px solid; border-bottom: #000000 1px solid; border-left: #000000 1px solid;'>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding: 0px; border: none; width: 100%; background-color: #000000; background-position-x: 100%; background-image: url(http://www.sprint.com/assets/images/common/nav/bg_header_gradient.gif); background-repeat: no-repeat;' height=80 colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=''><img src='http://www.sprint.com/assets/images/common/nav/logo_header_en.gif' border=0></a></td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr bgcolor=Gainsboro><td style='padding: 0; border-spacing: 0; width: 100%; border-top: none; border-right: none; border-bottom: none; border-left: none; background-image: url(http://www.sprint.com/assets/images/common/bg_grey_tile_v2.gif);' height=20 colspan=2></td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><th style='padding-left: 7px; border-spacing: 0; border: none; width: 100%; background-color: #59639c; font-family: Verdana; font-size: 9pt; color: white;' height='22' colspan=2 align=left>Report Automation Dx Alert</th></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding: 0px; width: 100%; height: 10px;' colspan=2></td></tr>{0}", Environment.NewLine);
                    if (nSendEmail == 0)
                    {
                        sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>DEVELOPMENT:</td><td>&nbsp;</td></tr>{0}", Environment.NewLine);
                        sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail To:</td>{0}", Environment.NewLine);
                        sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + ueMailTo + "</td></tr>{0}", Environment.NewLine);
                        sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail From:</td>{0}", Environment.NewLine);
                        sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + ueMailFrom + "</td></tr>{0}", Environment.NewLine);
                        sHTML += string.Format("<tr><td style='border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail CC:</td>{0}", Environment.NewLine);
                        sHTML += string.Format("<td style='padding-left: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + ueMailCC + "</td></tr>{0}", Environment.NewLine);
                        sHTML += string.Format("<tr><td style='padding-bottom: 7px; border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt;' align=right>e-Mail BCC:</td>{0}", Environment.NewLine);
                        sHTML += string.Format("<td style='padding-left: 7px; padding-bottom: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + ueMailBCC + "</td></tr>{0}", Environment.NewLine);
                    }
                    sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; width: 15%; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>Date:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; width: 85%; font-family: Verdana; font-size: 8pt;' align=left>" + DateTime.Now.ToString("g") + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right>Alert:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;'>" + ueAlert + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt; font-weight: bold;' align=right valign=top>Errors:</td>{0}", Environment.NewLine);
                    sHTML += string.Format("<td style='padding: 7px; border-spacing: 0; border: none; font-family: Verdana; font-size: 8pt;' valign=top>" + ueDescription + "</td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("<tr><td style='padding: 0px; width: 100%; background-color: #ffe100; height: 7px;' colspan=2></td></tr>{0}", Environment.NewLine);
                    sHTML += string.Format("</table></body></html>{0}", Environment.NewLine);
                    ueDescription = "";

                    MailMessage eMail = new MailMessage();
                    eMail.From = new MailAddress(sTempFrom);
                    string emailTemp = "";
                    string[] toArray = sTempTo.Split(';');
                    foreach (string toEmail in toArray) { emailTemp = toEmail; if (emailTemp != "") eMail.To.Add(new MailAddress(emailTemp)); }
                    if (sTempCC != "")
                    {
                        string[] ccArray = sTempCC.Split(';');
                        foreach (string ccEmail in ccArray) { emailTemp = ccEmail; if (emailTemp != "") eMail.CC.Add(new MailAddress(emailTemp)); }
                    }
                    if (sTempBCC != "")
                    {
                        string[] bccArray = sTempBCC.Split(';');
                        foreach (string bccEmail in bccArray) { emailTemp = bccEmail; if (emailTemp != "") eMail.Bcc.Add(new MailAddress(emailTemp)); }
                    }
                    eMail.Priority = MailPriority.High;
                    eMail.Subject = ueMailSubject;
                    eMail.Body = sHTML.ToString();
                    eMail.IsBodyHtml = true;

                    //SmtpClient client = new SmtpClient();
                    //client.UseDefaultCredentials = false;
                    //client.Credentials = new System.Net.NetworkCredential(sSMTPUID, sSMTPPassword);
                    //client.Port = nSMTPPort;
                    //client.Host = sSMTPHost.ToString();
                    //client.Send(eMail);
                    //ueLastMailDate = DateTime.Now;

                    //EmailHelper._SendSingleEmail(true, MailPriority.High, eMail.From, eMail.To, eMail.CC, string.Empty, _Subject, _EmailBody);
                }
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::SendAlertUnknown()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'SendAlertUnknown()', 'ERROR: ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch (Exception e)
                {
                    throw new System.Exception("SendAlertUnKnown::EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW" + "; Exception - " + e.Message + "; " + e.InnerException + "; " + e.StackTrace);
                }
            }
        }

        public static bool IsCritical(Exception ex)
        {
            if (ex is OutOfMemoryException) return true;
            if (ex is AppDomainUnloadedException) return true;
            if (ex is BadImageFormatException) return true;
            if (ex is CannotUnloadAppDomainException) return true;
            if (ex is ExecutionEngineException) return true;
            if (ex is InvalidProgramException) return true;
            if (ex is System.Threading.ThreadAbortException) return true;
            return false;
        }

        protected void CheckDependencies(int nReportID, int nQueueID, string sBatchID)
        {
            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='DEPENDENCY CHECK...' WHERE QueueID=" + nQueueID.ToString();
            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
            aDB.setCommandText(sSQL);
            aDB.execNoQuery();
            int i = 0, nAttempts = 0, nDependencyID = 0, nDone = 0, nError = 0, nLastRun = 0, nComplete = 0, nReportDepID = 0;
            string sDescShort = "";
            try
            {
                sSQL = "SELECT b.QueryCheck, b.DependencyID, b.DescriptionShort, b.Complete, b.Error, b.Attempts FROM RAD_Reporting.rad.ReportxDependency a WITH (NOLOCK) " +
                   "JOIN RAD_Reporting.rad.REPORTDEPENDENCY_NEW b WITH (NOLOCK) ON a.DependencyID=b.DependencyID WHERE ReportID=" + nReportID.ToString() + " ORDER BY RunOrder";
                SqlDataReader drDependencies = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drDependencies.Read())
                {
                    if ((nCont == 1) && (nDone == 0))
                    {
                        nError = Convert.ToInt16(drDependencies["Error"]);
                        nComplete = Convert.ToInt16(drDependencies["Complete"]);
                        nAttempts = Convert.ToInt16(drDependencies["Attempts"]);
                        nDependencyID = Convert.ToInt16(drDependencies["DependencyID"]);
                        sDescShort = Convert.ToString(drDependencies["DescriptionShort"]);
                        if (nError == 1)
                        {
                            nCont = 0; nDone = 1;
                            sTempMsg = PadIt(("ERROR: Dependency Failure-" + sDescShort + "; Existing Error"), " ", 2, 2);
                            ErrorLogMess(3, sTempMsg);
                            sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', 'ERROR: Dependency Failure-" + sDescShort + "; Existing Error', " + nReportID.ToString() + ", " + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQLD);
                            aDB.execNoQuery();
                            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='DEPENDENCY FAILURE-" + sDescShort.ToString() + "' WHERE QueueID=" + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQL);
                            aDB.execNoQuery();
                        }
                        if (nCont == 1)
                        {
                            if (nComplete == 1)
                            {
                                sTempMsg = PadIt(("Dependency Complete-" + sDescShort), " ", 2, 2);
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', 'Dependency Complete-" + sDescShort + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                            }
                            else
                            {
                                sSQL = "SELECT COALESCE(DATEDIFF(MI,MAX(EndTime),GETDATE()),0) 'Mins' FROM RAD_Reporting.rad.REPORTDEPLOG_NEW WITH (NOLOCK) WHERE DepID=" + nDependencyID + " AND Process='TOTAL'";
                                SqlDataReader drLastRun = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                if (drLastRun.HasRows)
                                {
                                    drLastRun.Read();
                                    nLastRun = Convert.ToInt16(drLastRun["Mins"]);
                                    drLastRun.Close();
                                }
                                if ((nLastRun < nDataCurrMins) && (nLastRun != 0))
                                {
                                    nComplete = 1;
                                    sSQL = "UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Complete=1 WHERE DependencyID=" + nDependencyID;
                                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                    aDB.setCommandText(sSQL);
                                    aDB.execNoQuery();
                                }
                                if (nComplete == 1)
                                {
                                    sTempMsg = PadIt(("Dependency Complete-" + sDescShort), " ", 2, 2);
                                    ErrorLogMess(3, sTempMsg);
                                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', 'Dependency Complete-" + sDescShort + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                    aDB.setCommandText(sSQLD);
                                    aDB.execNoQuery();
                                }
                                else
                                {
                                    string sSQLQ = drDependencies["QueryCheck"].ToString();
                                    if (sSQLQ.IndexOf("JOB_QUEUE") > 0)
                                    {
                                        DateTime dBatchID = Convert.ToDateTime(sBatchID);
                                        dBatchID = dBatchID.AddDays(-1);
                                        sBatchID = dBatchID.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                        sSQLQ = sSQLQ.Replace("[BATCH_ID]", sBatchID);
                                    }
                                    while ((i <= nAttempts) && (nCont == 1))
                                    {
                                        i = i + 1;
                                        int nSuccess = 0, nDefault = 0, nMaximum = 0;
                                        string sTimeout = "";
                                        sSQL = "SELECT 'Timeout'=(SELECT Value FROM RAD_Reporting.rad.ReportConfig WITH (NOLOCK) WHERE Environment='" + sRunDatabase +
                                           "' AND Component='Dependency' AND ComponentID=" + nDependencyID + " AND Ref1='CmdTimeout' AND Ref2=" + i + "), " +
                                           "'Default'=(SELECT Value FROM RAD_Reporting.rad.ReportConfig WITH (NOLOCK) WHERE Environment='" + sRunDatabase + "' AND " +
                                           "Component='Dependency' AND Ref1='CmdTimeout' AND Ref2='Default'), 'Maximum'=(SELECT Value FROM RAD_Reporting.rad.ReportConfig " +
                                           "WITH (NOLOCK) WHERE Environment='" + sRunDatabase + "' AND Component='Dependency' AND Ref1='CmdTimeout' AND Ref2='Maximum')";
                                        SqlDataReader drTimeout = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                        if (drTimeout.HasRows)
                                        {
                                            drTimeout.Read();
                                            sTimeout = Convert.ToString(drTimeout["Timeout"]);
                                            nDefault = Convert.ToInt32(drTimeout["Default"]);
                                            nMaximum = Convert.ToInt32(drTimeout["Maximum"]);
                                            drTimeout.Close();
                                        }
                                        if (nDefault == 0) nDefault = 600;
                                        if (sTimeout == "" || sTimeout == "0") sTimeout = Convert.ToString(nDefault);
                                        if ((Convert.ToInt32(sTimeout) > nMaximum) && (nMaximum != 0)) sTimeout = Convert.ToString(nMaximum);
                                        SqlDataReader drSvrTime = connectDB.GetDR("SELECT GETDATE()", sSQLConn, Convert.ToInt32(sTimeout));
                                        drSvrTime.Read();
                                        string sExecTime = drSvrTime[0].ToString();
                                        drSvrTime.Close();
                                        SqlDataReader drQuery = connectDB.GetDR(sSQLQ, sSQLConn, Convert.ToInt32(sTimeout));
                                        if (drQuery.HasRows)
                                        {
                                            drQuery.Read();
                                            nSuccess = Convert.ToInt32(drQuery[0]);
                                            drQuery.Close();
                                            if (nSuccess == 1)
                                            {
                                                sTempMsg = PadIt(("Dependency Complete-" + sDescShort + "; Attempt #" + i + " of " + nAttempts), " ", 2, 2);
                                                ErrorLogMess(3, sTempMsg);
                                                sSQL = "UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Complete=1, Error=0 WHERE DependencyID=" + nDependencyID;
                                                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                                aDB.setCommandText(sSQL);
                                                aDB.execNoQuery();
                                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', '";
                                                sSQLD += "Dependency Complete-" + sDescShort + "; Attempt #" + i + " of " + nAttempts;
                                                sSQLD += "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                                aDB.setCommandText(sSQLD);
                                                aDB.execNoQuery();
                                                i = nAttempts + 1;
                                            }
                                            else { nCont = 0; }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                drDependencies.Close();
                if (nCont == 1)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='DEPENDENCY COMPLETE' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                }
                else
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='DEPENDENCY FAILURE-" + sDescShort + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sTempMsg = PadIt(("ERROR: Dependency Failure-" + sDescShort + "; Attempt #" + i + " of " + nAttempts), " ", 2, 2);
                    ErrorLogMess(3, sTempMsg);
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Complete=0, Error=1 WHERE DependencyID=" + nDependencyID;
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQL = "SELECT TOP 1 ReportDepID, 'Timeout'=CASE WHEN EndTime IS NULL THEN 1 ELSE 0 END FROM RAD_Reporting.rad.REPORTDEPLOG_NEW WITH (NOLOCK) WHERE DepID=" + nDependencyID + " ORDER BY RunDate DESC, RunTime DESC, StepNo DESC";
                    SqlDataReader drTOUpdate = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    while (drTOUpdate.Read())
                    {
                        nReportDepID = Convert.ToInt32(drTOUpdate["ReportDepID"]);
                        nTimeout = Convert.ToInt16(drTOUpdate["Timeout"]);
                    }
                    drTOUpdate.Close();
                    if (nTimeout == 1)
                    {
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', 'Dependency Failure-" + sDescShort + "; ";
                        sSQLD += (i < nAttempts) ? "Timeout: " : "Final Timeout: ";
                        sSQLD += "Attempt #" + i + " of " + nAttempts + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQLD = "UPDATE RAD_Reporting.rad.REPORTDEPLOG_NEW SET StoredProc='";
                        if (i >= nAttempts) { sSQLD += "Final "; }
                        sSQLD += "Timeout: Attempt " + i + " of " + nAttempts + "', EndTime=GETDATE(), ExecTime=DATEDIFF(MI,StartTime,EndTime) WHERE ReportDepID=(" +
                           "SELECT COALESCE(MAX(ReportDepID),0) FROM RAD_Reporting.rad.REPORTDEPLOG_NEW WITH (NOLOCK) WHERE DepID=" + nDependencyID + ");";
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                    else
                    {
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', ";
                        sSQLD += "'ERROR: Dependency Failure-" + sDescShort + "; Attempt #" + (i + 1) + " of " + nAttempts + "; ex.Message=";
                        sSQLD += (exMessage == "") ? "NULL" : exMessage.ToString();
                        sSQLD += "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                nCont = 0; nDone = 1;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::CheckDependencies()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? (((ex.Message).Replace("'", "")).ToString()) : "NULL";
                try
                {
                    sSQLD = "UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Complete=0 WHERE DependencyID=" + nDependencyID;
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    if ((exMessage.ToLower()).IndexOf("timeout") != -1)
                    {
                        sSQLD = "SELECT TOP 1 ReportDepID FROM RAD_Reporting.rad.REPORTDEPLOG_NEW WITH (NOLOCK) WHERE DepID=" + nDependencyID + " ORDER BY RunDate DESC, RunTime DESC, StepNo DESC";
                        SqlDataReader drcTOUpdate = connectDB.GetDR(sSQLD, sSQLConn, nTimeout);
                        while (drcTOUpdate.Read()) { nReportDepID = Convert.ToInt32(drcTOUpdate["ReportDepID"]); }
                        drcTOUpdate.Close();
                        sSQLD = "UPDATE RAD_Reporting.rad.REPORTDEPLOG_NEW SET StoredProc='";
                        if (i >= nAttempts) { sSQLD += "Final "; }
                        sSQLD += "Timeout: Attempt " + i + " of " + nAttempts + "', EndTime=GETDATE(), ExecTime=DATEDIFF(MI,StartTime,EndTime) WHERE ReportDepID=" + nReportDepID;
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', ";
                        sSQLD += "'ERROR: Dependency Failure-" + sDescShort + "; Attempt #" + (i + 1) + " of " + nAttempts + "; ex.Message=";
                        sSQLD += (exMessage == "") ? "NULL" : exMessage.ToString();
                        sSQLD += "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                    else
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='DEPENDENCY FAILURE-ERROR:" + sDescShort + "' WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'CheckDependencies()', 'ERROR: ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                }
                catch
                {
                    ueDescription = "ERROR::CheckDependencies()";
                    SendAlertUnknown(exMessage);
                }
            }
            finally { CheckAlerts("", 0); }
        }

        protected void RunReports()
        {
            try
            {
                string sGetDate = ""; nCont = 1; nRptCnt = 0;
                sLogMessage = "";
                sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET StartTime=NULL, EndTime=NULL, Status='' WHERE (RunNow='Y') OR ((ReportID > 0) AND " +
                   "(DATEDIFF(MI,(CONVERT(VARCHAR(19),(CONVERT(VARCHAR(19),RunDate,121) + ' ' + LEFT(CONVERT(VARCHAR(8),RunTime),5)))), " +
                   "(CONVERT(VARCHAR(19),GETDATE(),121))) > 0) AND ((Status='' OR Status IS NULL OR Status LIKE '%...%') OR ((COALESCE(StartTime,'') > '') AND (COALESCE(EndTime,'')=''))));";
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                sSQL = "SELECT 'NeedToRun'=(SELECT COUNT(*) FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE (RunNow='Y') OR ((ReportID > 0) AND  " +
                   "((COALESCE(Status,'')='')))), 'NeedToRefresh'=(SELECT 'CNT'=CASE WHEN Complete=1 THEN 0 WHEN (CONVERT(VARCHAR(10),COALESCE(LastCheck,''),121) < " +
                   "CONVERT(VARCHAR(10),GETDATE(),121)) THEN 1 ELSE 0 END FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE AlertCode='Q');";
                SqlDataReader drCurrQueue = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                drCurrQueue.Read();
                nCount = Convert.ToInt32(drCurrQueue["NeedToRun"]);
                nCount2 = Convert.ToInt32(drCurrQueue["NeedToRefresh"]);

                drCurrQueue.Close();
                sSQL = "SELECT TOP 1 MAX(RunDate) as RunDate FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK)";
                SqlDataReader drRunDate = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                drRunDate.Read();
                sRunDate = ((drRunDate["RunDate"] != null) && (drRunDate["RunDate"] != DBNull.Value)) ? Convert.ToDateTime(drRunDate["RunDate"]).ToString("yyyy-MM-dd") : DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                drRunDate.Close();
                sGetDate = System.DateTime.Now.ToString("yyyy-MM-dd");
                if ((nCount == 0) && (nCount2 == 0) && (sRunDate != sGetDate))
                {
                    sSQL = "EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'Y','0'";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "RunReports() - Report Queue Refreshed[L][B]");
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', 'Report Queue Refreshed', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                //if (nCount > 0) {
                //sSQL = "SELECT COUNT(*) FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE (RunNow='Y') OR (COALESCE(StartTime,'')='' AND COALESCE(EndTime,'')='' " +
                //   "AND COALESCE(Status,'')='' AND (DATEDIFF(MI,(CONVERT(VARCHAR(16),(CONVERT(VARCHAR(10), RunDate,121) + ' ' + " +
                //   "LEFT(CONVERT(VARCHAR(8),RunTime),5)))), (CONVERT(VARCHAR(16),GETDATE(),121))) > 0));";
                //SqlDataReader drRun = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                //drRun.Read();
                //nCount=Convert.ToInt32(drRun[0]);
                //drRun.Close();
                if (nCount > 0)
                {
                    DateTime dStartTime = DateTime.Now;
                    sLogMessage = "[D]" + PadIt("Function:", " ", 1, 0) + "RunReports()[L]";
                    sLogMessage += PadIt(("******* REPORT COUNT IN CURRENT CYCLE: " + nCount.ToString() + " *******"), " ", 2, 0);
                    ErrorLogMess(3, sLogMessage);
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', '******* REPORT COUNT IN CURRENT CYCLE: " + nCount + " *******', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sSQL = "SELECT q.QueueID, q.ReportID, q.TemplateID, q.RunNow, s.SysOnOff, 'ReportEnabled'=r.Enabled, r.ReportName, q.IntervalID, q.FTP, " +
                          "'TemplateEnabled'=t.Enabled, q.GroupAlias, t.[Dependent], f.FileFormatCD, COALESCE(f.FileExtension,'') AS 'FileExtension', " +
                          "COALESCE(f.ConstantName,'') AS 'ConstantName', COALESCE(f.ConstantValue,0) AS 'ConstantValue', 'RunDate'=CONVERT(VARCHAR(10),q.RunDate,121) " +
                          "FROM RAD_Reporting.rad.REPORTQUEUE_NEW q WITH (NOLOCK) CROSS JOIN RAD_Reporting.rad.ReportSystem s WITH (NOLOCK) JOIN RAD_Reporting.rad.Report r " +
                          "WITH (NOLOCK) ON q.ReportID=r.ReportID JOIN RAD_Reporting.rad.ReportTemplate t WITH (NOLOCK) ON q.TemplateID=t.TemplateID " +
                          "JOIN RAD_Reporting.rad.Report_L_FileFormat f WITH (NOLOCK) ON t.FileFormatID=f.FileFormatID AND f.Enabled=1 " +
                          "WHERE (q.RunNow <> 'N') OR (q.RunDate < CONVERT(VARCHAR(10),GETDATE(),121) AND (q.Status='' OR q.Status IS NULL)) " +
                          "OR ((q.RunDate=CONVERT(VARCHAR(10),GETDATE(),121)) AND (q.RunTime < CONVERT(VARCHAR(10),GETDATE(),8)) AND (q.Status='' OR q.Status IS NULL)) " +
                          "ORDER BY q.RunNow DESC, s.SysOnOff, r.Enabled, t.Enabled, q.IntervalID, q.Priority";
                    SqlDataReader drRunNow = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    while (drRunNow.Read())
                    {
                        nCont = 1;
                        dStartTime = DateTime.Now;
                        nQueueID = Convert.ToInt32(drRunNow["QueueID"]);
                        nReportID = Convert.ToInt32(drRunNow["ReportID"]);
                        nTemplateID = Convert.ToInt32(drRunNow["TemplateID"]);
                        sRunNow = Convert.ToString(drRunNow["RunNow"]);
                        nSystemOn = Convert.ToInt16(drRunNow["SysOnOff"]);
                        nReportEnabled = Convert.ToInt16(drRunNow["ReportEnabled"]);
                        nTemplateEnabled = Convert.ToInt16(drRunNow["TemplateEnabled"]);
                        sReportName = Convert.ToString(drRunNow["ReportName"]);
                        sGroupAlias = Convert.ToString(drRunNow["GroupAlias"]);
                        nDependent = Convert.ToInt16(drRunNow["Dependent"]);
                        sFileFormatCD = Convert.ToString(drRunNow["FileFormatCD"]);
                        sFileExtension = Convert.ToString(drRunNow["FileExtension"]);
                        sConstantName = Convert.ToString(drRunNow["ConstantName"]);
                        sRunDate = Convert.ToString(drRunNow["RunDate"]);
                        sFTP = Convert.ToString(drRunNow["FTP"]);
                        nRptCnt++;
                        sLogMessage = WrapIt((PadIt(String.Format("{0:MM/dd/yy h:mm:ss tt}", DateTime.Now), " ", 1, 1) + ("#" + nRptCnt.ToString() + ": " + sReportName.ToUpper() + " (ID " + nReportID.ToString() + ")")), 2, 1);
                        ErrorLogMess(3, sLogMessage);
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', 'REPORT # " + nRptCnt + ": " + sReportName + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET StartTime=GETDATE() WHERE QueueID=" + nQueueID;
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sLogMessage = "";
                        if (nSystemOn == 0 && sRunNow == "N")
                        {
                            nCont = 0;
                            sLogMessage += "System Off";
                            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET SystemOn='N', [Status]='SYSTEM OFF', EndTime=GETDATE() WHERE QueueID=" + nQueueID;
                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQL);
                            aDB.execNoQuery();
                        }
                        if (nCont == 1 && sRunNow == "N")
                        {
                            if (nReportEnabled == 0 || nTemplateEnabled == 0)
                            {
                                nCont = 0;
                                if (sLogMessage != "")
                                {
                                    sLogMessage += "; ";
                                }
                                string sTempEnabled = "";
                                if (nReportEnabled == 0 && nTemplateEnabled == 0)
                                {
                                    sTempEnabled = "Report and Template Disabled";
                                }
                                else if (nReportEnabled == 0)
                                {
                                    sTempEnabled = "Report Disabled";
                                }
                                else
                                {
                                    sTempEnabled = "Template Disabled";
                                }
                                sLogMessage += sTempEnabled;
                                sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET SystemOn='Y', Enabled='N', [Status]='" + sTempEnabled.ToUpper() + "', EndTime=GETDATE() WHERE QueueID=" + nQueueID;
                                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQL);
                                aDB.execNoQuery();
                            }
                        }
                        if (nCont == 1)
                        {
                            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET SystemOn='Y', [Status]='IN PROGRESS...' WHERE QueueID=" + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQL);
                            aDB.execNoQuery();
                            if (nDependent == 1)
                            {
                                sRunDate = Convert.ToString(drRunNow["RunDate"]);
                                CheckDependencies(nReportID, nQueueID, sRunDate);
                                if (nCont == 0) { sLogMessage += "***** ERROR::Dependency Failure *****"; }
                            }
                            else
                            {
                                sTempMsg = (PadIt(("No Dependencies"), " ", 2, 2));
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', 'No Dependencies', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                            }
                            if ((nCont == 1) && (sFileFormatCD == "E"))
                            {
                                ExecuteExcel(nQueueID, nReportID, nTemplateID, sGroupAlias);
                                if (nCont == 0) { sLogMessage += "***** ERROR::Report Generation Failure *****"; }
                            }
                            if ((nCont == 1) && (sFTP == "Y"))
                            {
                                sSQL = "SELECT t.FTPID " +
                                   "FROM RAD_Reporting.rad.ReportTemplate t WITH (NOLOCK) " +
                                   "LEFT OUTER JOIN RAD_Reporting.rad.ReportFTP f WITH (NOLOCK) ON f.FTPID=t.FTPID WHERE t.TemplateID=" + nTemplateID.ToString();
                                SqlDataReader drFTP = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                drFTP.Read();
                                nFTPID = Convert.ToInt32(drFTP["FTPID"]);
                                sSQL = "SELECT FTPName, FTPFileName, FTPPath FROM RAD_Reporting.rad.ReportFTP WITH (NOLOCK) " +
                                   "WHERE FTPID=" + nFTPID.ToString();
                                SqlDataReader drFTPFields = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                while (drFTPFields.Read())
                                {
                                    sFTPName = Convert.ToString(drFTPFields["FTPName"]);
                                    sFTPFileName = Convert.ToString(drFTPFields["FTPFileName"]);
                                    sFTPPath = Convert.ToString(drFTPFields["FTPPath"]);
                                }
                                drFTPFields.Close();

                                FileTransfer(nFTPID, nQueueID, sRunDatabase);
                                if (nCont == 0) { sLogMessage += "***** ERROR::FTP Failure *****"; }
                                sSQLD = "EXEC Reporting.dbo.sp_ReportProcessLog 'Automation', 'RunReports()', 'File Transfer ";
                                sSQLD += (nCont == 0) ? "Failed" : "Complete";
                                sSQLD += "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                                drFTP.Close();
                            }
                        }
                        if (nCont == 1)
                        {
                            sLogMessage += ("Complete (" + String.Format("{0:MM/dd/yy h:mm:ss tt}", DateTime.Now) + ")");
                        }
                        if (sLogMessage == "")
                        {
                            sLogMessage += "***** ERROR::Report Failure *****";
                        }
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', '" + sLogMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sLogMessage = "Status: " + sLogMessage;
                        //sLogMessage=WrapIt((PadIt((String.Format("{0:MM/dd/yy h:mm:ss tt}", DateTime.Now) + " - " + sLogMessage)," ",2,2)),2,3);
                        sLogMessage = WrapIt((PadIt(sLogMessage, " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sLogMessage);
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET RunNow='N', EndTime='" + DateTime.Now + "' WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                    }
                    nReportID = 0;
                    nQueueID = 0;
                    drRunNow.Close();
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Complete=0 WHERE Reset=1; UPDATE RAD_Reporting.rad.REPORTDEPENDENCY_NEW SET Error=0 WHERE Error=1;";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sLogMessage = PadIt("******* CURRENT CYCLE EXECUTION COMPLETE *******", " ", 2, 0) + "[L][B]";
                    ErrorLogMess(3, sLogMessage);
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', '******* CURRENT CYCLE EXECUTION COMPLETE *******', null, null";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sTempMsg = "Reports successfully generated at - " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                    ErrorLogMess(3, sTempMsg);
                    EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, sTempMsg, sTempMsg);
                }
                //}
                CheckAlerts("", 0);
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::RunReports()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET RunNow='N', StartTime=GETDATE(), EndTime=GETDATE(), Status='RUN REPORT FAILURE-" + exMessage + "' WHERE QueueID=" + nQueueID;
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'RunReports()', 'ERROR: ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch
                {
                    ueDescription = "ERROR::RunReports()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        public void GetFirstLast()
        {
            int nTempVal = 0;
            string sTempVal = "";
            if ((sRangeFirst != "") && (sRangeFirst.IndexOf("Last") != -1))
            {
                if (sRangeFirst.IndexOf("Col") != -1)
                {
                    if (nVariance != 0)
                    {
                        nTempVal = nLastCol + nVariance;
                        sTempVal = GetLastColumnName(nTempVal);
                        sRangeFirst = sRangeFirst.Replace("LastCol", sTempVal);
                    }
                    else { sRangeFirst = sRangeFirst.Replace("LastCol", sLastCol); }
                }
                if (sRangeFirst.IndexOf("Row") != -1)
                {
                    if (nVariance != 0)
                    {
                        nTempVal = nLastRow + nVariance;
                        sRangeFirst = sRangeFirst.Replace("LastRow", nTempVal.ToString());
                    }
                    else { sRangeFirst = sRangeFirst.Replace("LastRow", nLastRow.ToString()); }
                }
                sRangeFirst = sRangeFirst.Replace("LastCell", sLastCell);
            }
            if ((sRangeLast != "") && (sRangeLast.IndexOf("Last") != -1))
            {
                if (sRangeLast.IndexOf("Col") != -1)
                {
                    if (nVariance != 0)
                    {
                        nTempVal = nLastCol + nVariance;
                        sTempVal = GetLastColumnName(nTempVal);
                        sRangeLast = sRangeLast.Replace("LastCol", sTempVal);
                    }
                    else { sRangeLast = sRangeLast.Replace("LastCol", sLastCol); }
                }
                if (sRangeLast.IndexOf("Row") != -1)
                {
                    if (nVariance != 0)
                    {
                        nTempVal = nLastRow + nVariance;
                        sRangeLast = sRangeLast.Replace("LastRow", nTempVal.ToString());
                    }
                    else { sRangeLast = sRangeLast.Replace("LastRow", nLastRow.ToString()); }
                }
                sRangeLast = sRangeLast.Replace("LastCell", sLastCell);
            }
        }

        private string GetLastColumnName(int lastColIndex)
        {
            string sLastCol = "";
            if (lastColIndex > 26)
            {
                char first = Convert.ToChar(64 + ((lastColIndex - 1) / 26));
                char second = Convert.ToChar(64 + (lastColIndex % 26 == 0 ? 26 : lastColIndex % 26));
                sLastCol = first.ToString() + second.ToString();
            }
            else sLastCol = Convert.ToChar(64 + lastColIndex).ToString();
            return sLastCol;
        }

        protected void ExecuteExcel(int nQueueID, int nReportID, int nTemplateID, string sGroupAlias)
        {
            string sFolderPathReport = "", sFolderPathReportFile = "", sFolderPathTemplate = "", sFolderPathTemplateFile = "", sFolderPathLegend = "", sFolderPathLegendFile = "";
            string sTemplateName = "", sLegendName = "", sSaveAs = "", sSaveAsExec = "", sTabName = "", sVariables = "", sTemp = "";
            int nCacheIndex = 0, nCacheNum = 0, nSheetNumber = 1, nSheetCount = 0, nTotalSheets = 0, nCreateNextSheet = 0, nLegendSheetCount = 0, nFileFormat = 0, nEncrypted = 0, nAttachmentID = 0;
            //----------------------------------------------------------------------------------------------------------------------------------------------------
            //public string sObjectCD="",sRangeFirst="",sRangeLast="",sRangeCD="",sRangeStyle="",sExecCall="";
            //public string sFormula="",sCondition="",sStyle="",sObject="",sLastCol="",sLastCell="";
            //public int nSheetGroupID=0, nDataGroupID=0, nFormulaGroupID=0, nConitionGroupID=0, nStyleGroupID=0, nFormatGroupID=0;
            //public int nVariance=0, nExecOrder=0, nLastCol=0, nLastRow=0, nFailed=0;

            try
            {
                nCont = 1;
                if (nDependent == 1)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=(Status + '; EXCEL IN PROGRESS...') WHERE QueueID=" + nQueueID.ToString();
                }
                else
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='EXCEL IN PROGRESS...' WHERE QueueID=" + nQueueID.ToString();
                }
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                //sSQL = "SELECT z.Code, 'Path'=REPLACE((z.Path + '\\' + z.Path2 + '\\' + z.Path3),'[GRP]','" + sGroupAlias + "') FROM (SELECT 'Path'=(SELECT value FROM RAD_Reporting.rad.REPORTSYSCONFIG_NEW " +
                //  "WHERE name=(SELECT srvname FROM master.dbo.sysservers WHERE srvid=0) AND Component='APPSVR'), 'Code'=f.FolderTypeCD, 'Path2'=f.FolderPath, " +
                //  "'Path3'=f.folderName FROM RAD_Reporting.rad.ReportFolder f WITH (NOLOCK) WHERE f.FolderTypeCD='T'" +
                //  " OR f.FolderID=(SELECT FolderID FROM RAD_Reporting.rad.ReportTemplate WHERE TemplateID=" + nTemplateID + ")) z;";

                // sSQL="SELECT z.Code, 'Path'=REPLACE((z.Path + z.path2),'[GRP]','" + sGroupAlias + "') + '\\' + z.folder FROM (SELECT 'Path'=(SELECT value FROM RAD_Reporting.rad.REPORTSYSCONFIG_NEW " +
                //   "WHERE name=(SELECT srvname FROM master.dbo.sysservers WHERE srvid=0) AND Component='APPSVR') + '\\', 'Code'=f.FolderTypeCD, 'Path2'=f.FolderPath, " +
                //   "'folder'=f.folderName FROM RAD_Reporting.rad.ReportFolder f WITH (NOLOCK) WHERE f.FolderTypeCD='T'" +
                //   " OR f.FolderID=(SELECT FolderID FROM RAD_Reporting.rad.ReportTemplate WHERE TemplateID=" + nTemplateID + ")) z;";

                sSQL = "SELECT z.Code, z.Path3, 'Path'=REPLACE((z.Path + '\\' + z.path2),'[GRP]','" + sGroupAlias + "') FROM (SELECT 'Path'=(SELECT value FROM RAD_Reporting.rad.REPORTSYSCONFIG_NEW WITH (NOLOCK) " +
                "WHERE name=(SELECT srvname FROM master.dbo.sysservers WHERE srvid=0) AND Component='APPSVR'), 'Code'=f.FolderTypeCD, 'Path2'=f.FolderPath, " +
                "'Path3'=f.folderName FROM RAD_Reporting.rad.ReportFolder f WITH (NOLOCK) WHERE f.FolderTypeCD='T'" +
                " OR f.FolderID=(SELECT FolderID FROM RAD_Reporting.rad.ReportTemplate WITH (NOLOCK) WHERE TemplateID=" + nTemplateID + ")) z;";

                SqlDataReader drFolders = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drFolders.Read())
                {
                    sTemp = Convert.ToString(drFolders["Code"]);
                    if (sTemp == "T")
                    {
                        sFolderPathTemplate = Convert.ToString(drFolders["Path"]);
                        sFolderPathLegend = Convert.ToString(drFolders["Path"]);
                    }
                    else { sFolderPathReport = Convert.ToString(drFolders["Path"]) + '\\' + Convert.ToString(drFolders["Path3"]); }
                }
                drFolders.Close();
                if (sFolderPathReport == "") { sTemp = "Report Folder Missing"; nCont = 0; }
                else if (sFolderPathTemplate == "") { sTemp = "Template Folder Missing"; nCont = 0; }
                else if (sFolderPathLegend == "") { sTemp = "Legend Folder Missing"; nCont = 0; }
                if (nCont == 0)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sTemp = "ERROR::" + sTemp;
                    sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                }
                sSQL = "SELECT COALESCE(d.ExecCall,'') AS 'ExecCall' FROM RAD_Reporting.rad.ReportTemplate t WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportSheet s WITH (NOLOCK) " +
                   "ON t.SheetGroupID=s.SheetGroupID LEFT OUTER JOIN RAD_Reporting.rad.ReportSheetData d WITH (NOLOCK) ON s.DataGroupID=d.DataGroupID LEFT OUTER JOIN " +
                   "RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON d.ObjectID=o.ObjectID WHERE t.SheetGroupID=s.SheetGroupID AND s.DataGroupID=d.DataGroupID AND " +
                   "d.ObjectID=0 AND t.TemplateID=" + nTemplateID.ToString() + " ORDER BY t.TemplateID, s.SheetNumber, d.ExecOrder";
                SqlDataReader drPreWorkCheck = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drPreWorkCheck.Read())
                {
                    sSQL = Convert.ToString(drPreWorkCheck["ExecCall"]);
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                }
                drPreWorkCheck.Close();
                sSQL = "SELECT t.TemplateName, COALESCE(t.SaveAs,'') AS 'SaveAs', COALESCE(t.SaveAsExec,'') AS 'SaveAsExec', t.Encrypted, t.AttachmentID, t.FTPID, " +
                   "'LegendName'=CASE WHEN t.LegendID=0 THEN '' ELSE l.LegendName END, 'LegendSheetCount'=CASE WHEN t.LegendID=0 THEN '0' ELSE l.SheetCount END, " +
                   "t.SheetGroupID, COALESCE(f.ConstantValue,0) AS 'FileFormatValue', f.FileExtension FROM RAD_Reporting.rad.ReportTemplate t WITH (NOLOCK) " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_FileFormat f WITH (NOLOCK) ON t.FileFormatID=f.FileFormatID " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.ReportLegend l (NOLOCK) ON l.LegendID=t.LegendID WHERE t.TemplateID=" + nTemplateID.ToString();
                SqlDataReader drReport = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drReport.Read())
                {
                    sTemplateName = Convert.ToString(drReport["TemplateName"]);
                    if (sTemplateName == "") { nCont = 0; sTemp = "Missing Template Name"; break; }
                    sSaveAs = Convert.ToString(drReport["SaveAs"]);
                    sSaveAsExec = Convert.ToString(drReport["SaveAsExec"]);
                    if (sSaveAs == "" && sSaveAsExec == "") { nCont = 0; sTemp = "Missing SaveAs Value"; break; }
                    nFileFormat = Convert.ToInt32(drReport["FileFormatValue"]);
                    if (nFileFormat == 0) { nCont = 0; sTemp = "Missing File Format"; break; }
                    else { sFileExtension = Convert.ToString(drReport["FileExtension"]); }
                    nEncrypted = Convert.ToInt16(drReport["Encrypted"]);
                    nAttachmentID = Convert.ToInt16(drReport["AttachmentID"]);
                    nFTPID = Convert.ToInt32(drReport["FTPID"]);
                    sLegendName = Convert.ToString(drReport["LegendName"]);
                    nLegendSheetCount = Convert.ToInt16(drReport["LegendSheetCount"]);
                    if ((nLegendSheetCount == 0 && sLegendName != "") || (nLegendSheetCount > 0 && sLegendName == "")) { nCont = 0; sTemp = "Missing Legend Name"; break; }
                    nSheetGroupID = Convert.ToInt16(drReport["SheetGroupID"]);
                }
                drReport.Close();
                if (nCont == 0)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sTemp = "ERROR::" + sTemp;
                    sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                }

                sTemp = "";
                sTempMsg = "";
                sFolderPathReport = ReplaceUNC(sFolderPathReport);
                if (!Directory.Exists(sFolderPathReport))
                {
                    try { Directory.CreateDirectory(sFolderPathReport); }
                    catch (Exception ex) { nCont = 0; sTemp = "Invalid Report Path" + "; Exception - " + ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace; }
                    if (nCont == 0)
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sTemp = "ERROR::" + sTemp;
                        sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                    }
                    else
                    {
                        sTemp = "Report Folder: Path=" + sFolderPathReport;
                        sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                }

                sTemp = "";
                sTempMsg = "";
                sFolderPathTemplateFile = (sFolderPathTemplate + "\\" + sTemplateName);
                sFolderPathTemplateFile = ReplaceUNC(sFolderPathTemplateFile);
                if (!File.Exists(sFolderPathTemplateFile)) { nCont = 0; sTemp = "Invalid Template Path"; }
                if (nCont == 0)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sTemp = "ERROR::" + sTemp;
                    sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                }
                else
                {
                    sTemp = "Template File: Path=" + sFolderPathTemplateFile;
                    sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }

                sTemp = "";
                sTempMsg = "";
                if (sLegendName != "")
                {
                    sFolderPathLegendFile = sFolderPathLegend + "\\" + sLegendName;
                    sFolderPathLegendFile = ReplaceUNC(sFolderPathLegendFile);
                    if (!File.Exists(sFolderPathLegendFile)) { nCont = 0; sTemp = "Invalid Legend Path"; }
                    if (nCont == 0)
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sTemp = "ERROR::" + sTemp;
                        sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                    }
                    else
                    {
                        sTemp = "Legend File: Path=" + sFolderPathLegendFile;
                        sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                    }
                }

                sTemp = "";
                sTempMsg = "";
                if (sSaveAsExec != "")
                {
                    try
                    {
                        SqlDataReader drSaveAsExec = connectDB.GetDR(sSaveAsExec, sSQLConn, nTimeout);
                        drSaveAsExec.Read();
                        if (drSaveAsExec.HasRows) { sSaveAs = Convert.ToString(drSaveAsExec[0]); }
                        else { nCont = 0; sTemp = "Invalid SaveAsExec Value"; }
                        drSaveAsExec.Close();
                    }
                    catch (Exception ex) { nCont = 0; sTemp = "Invalid SaveAsExec Value" + "; Exception - " + ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace; }
                }
                if (sSaveAs != "")
                {
                    sSQL = "DECLARE @ReturnValue VARCHAR(500); EXEC RAD_Reporting.rad.sp_ReportSaveAs " + nReportID.ToString() + ", 1, '', '" + sSaveAs + "', @ReturnValue OUTPUT; SELECT @ReturnValue;";
                    try
                    {
                        SqlDataReader drSaveAs = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                        drSaveAs.Read();
                        if (drSaveAs.HasRows) { sSaveAs = Convert.ToString(drSaveAs[0]); }
                        drSaveAs.Close();
                    }
                    catch (Exception ex) { nCont = 0; sTemp = "Invalid SaveAs Value" + "; Exception - " + ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace; }
                }
                else { nCont = 0; sTemp = "Invalid SaveAs Value"; }

                if (nCont == 0)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FAILURE-" + sTemp.ToUpper() + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR::" + sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    sTemp = "ERROR::" + sTemp;
                    sTempMsg = WrapIt((PadIt(sTemp, " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                }

                if (nCont == 1)
                {
                    nExcelPID = 0;
                    Process[] P0;
                    P0 = Process.GetProcessesByName("EXCEL");
                    Excel.Application oExcel = new Excel.Application();
                    if (oExcel == null) { throw new System.Exception("******* ALERT: UNABLE TO START EXCEL *******"); }
                    else
                    {
                        Process[] P1;
                        P1 = Process.GetProcessesByName("EXCEL");
                        for (int a = 0; a < P1.Count(); a++)
                        {
                            int nPID = P1[a].Id;
                            int match = 0;
                            for (int b = 0; b < P0.Count(); b++)
                            {
                                int nPID2 = P0[b].Id;
                                if (nPID == nPID2) { match = 1; break; }
                            }
                            if (match == 0) { nExcelPID = nPID; break; }
                        }
                    }
                    oExcel.Visible = true;
                    oExcel.DisplayAlerts = false;
                    oExcel.ErrorCheckingOptions.BackgroundChecking = false;
                    Excel.Workbooks oBooks = oExcel.Workbooks;
                    Excel.Workbook oBook = null;
                    oBook = oExcel.Workbooks.Open(sFolderPathTemplateFile, false, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                    Excel.Sheets oSheets = oBook.Worksheets;
                    Excel.Worksheet oSheet = oSheets.get_Item(nSheetNumber) as Excel.Worksheet;
                    ((Excel._Worksheet)oSheet).Activate();
                    Excel.Range oCells = (Excel.Range)oSheet.Cells;
                    Excel.Range oRange = (Excel.Range)oSheet.get_Range("A1", "A1");
                    Excel.Range oRange2 = (Excel.Range)oSheet.get_Range("A2", "A2");
                    Excel.Range oRange3 = (Excel.Range)oSheet.get_Range("A3", "A3");
                    oRange.Select();
                    nTotalSheets = oBook.Sheets.Count;
                    try
                    {
                        //sSQL = "SELECT DISTINCT s.SheetCount, s.SheetNumber, s.SheetOrder, s.CreateNextSheet, COALESCE(s.TabName,'') AS TabName, 'DataGroupID'=s.DataGroupID, " +
                        //   "'FormulaGroupID'=s.FormulaGroupID, 'ConditionGroupID'=s.ConditionGroupID, 'StyleGroupID'=s.StyleGroupID, 'FormatGroupID'=s.FormatGroupID " +
                        //   "FROM RAD_Reporting.rad.ReportSheet s WITH (NOLOCK) LEFT OUTER JOIN RAD_Reporting.rad.ReportSheetData d WITH (NOLOCK) ON s.DataGroupID=d.DataGroupID " +
                        //   "WHERE s.SheetGroupID=" + nSheetGroupID + " AND d.ObjectID > 0 ORDER BY s.SheetOrder";
                        //SqlDataReader drSheet = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                        DataTable dt1 = aDB.ExecuteExcelwithID(1, nSheetGroupID);
                        foreach (DataRow drSheet in dt1.Rows)
                        {
                            nSheetCount = Convert.ToInt16(drSheet["SheetCount"]);
                            nSheetNumber = Convert.ToInt16(drSheet["SheetNumber"]);
                            nCreateNextSheet = Convert.ToInt16(drSheet["CreateNextSheet"]);
                            sTabName = Convert.ToString(drSheet["TabName"]);
                            sVariables = "Sheet #" + nSheetNumber;
                            if (sTabName != "") sVariables += "; Tab=" + sTabName;
                            nDataGroupID = Convert.ToInt32(drSheet["DataGroupID"]);
                            nFormulaGroupID = Convert.ToInt32(drSheet["FormulaGroupID"]);
                            nConditionGroupID = Convert.ToInt32(drSheet["ConditionGroupID"]);
                            nStyleGroupID = Convert.ToInt32(drSheet["StyleGroupID"]);
                            nFormatGroupID = Convert.ToInt32(drSheet["FormatGroupID"]);

                            sTempMsg = WrapIt((PadIt((sVariables), " ", 2, 2)), 2, 3);
                            ErrorLogMess(3, sTempMsg);
                            sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sVariables + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQLD);
                            aDB.execNoQuery();

                            oSheet = (Excel.Worksheet)oSheets.get_Item(nSheetNumber);
                            if (nCreateNextSheet == 1) oSheet.Copy(Type.Missing, oBook.Sheets[nSheetNumber]);
                            oSheet = (Excel.Worksheet)oSheets.get_Item(nSheetNumber);
                            if (sTabName != "") oSheet.Name = sTabName;

                            //sSQL = "SELECT 'ObjectCD'=o.ObjectCD, COALESCE(d.RangeFirst,'') AS 'RangeFirst', COALESCE(d.RangeLast,'') AS 'RangeLast', d.Variance, d.ExecOrder, " +
                            //   "COALESCE(d.ExecCall,'') AS 'ExecCall' " +
                            //   "FROM RAD_Reporting.rad.ReportSheetData d WITH (NOLOCK) " +
                            //   "JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON d.ObjectID=o.ObjectID " +
                            //   "WHERE d.DataGroupID=" + nDataGroupID + " AND d.ObjectID > 0 " +
                            //   "ORDER BY d.ExecOrder";
                            //SqlDataReader drData = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                            DataTable dt2 = aDB.ExecuteExcelwithID(2, nDataGroupID);
                            foreach (DataRow drData in dt2.Rows)
                            {
                                sObjectCD = Convert.ToString(drData["ObjectCD"]);
                                sRangeFirst = Convert.ToString(drData["RangeFirst"]);
                                sRangeLast = Convert.ToString(drData["RangeLast"]);
                                nVariance = Convert.ToInt16(drData["Variance"]);
                                nExecOrder = Convert.ToInt16(drData["ExecOrder"]);
                                sExecCall = Convert.ToString(drData["ExecCall"]);
                                switch (sObjectCD)
                                {
                                    case "T":
                                        {
                                            OleDbConnection oleConn = new OleDbConnection(sSQLOLEDB);
                                            string sOLE = "OLEDB;" + sSQLOLEDB;
                                            sSQL = sExecCall.ToString();
                                            oleConn.Open();
                                            oSheet = (Excel.Worksheet)oSheets.get_Item(nSheetNumber);
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeFirst);
                                            Excel.QueryTable qt = oSheet.QueryTables.Add(sOLE, oDestination, sSQL);
                                            qt.Refresh(Type.Missing);
                                            nRowCnt = Convert.ToInt32(oSheet.UsedRange.Rows.Count);
                                            oleConn.Close();
                                            qt.Delete();
                                            break;
                                        }
                                    case "Q":
                                        {
                                            SqlDataReader drGetValue = connectDB.GetDR(sExecCall, sSQLConn, nTimeout);
                                            drGetValue.Read();
                                            if (drGetValue.HasRows == true)
                                            {
                                                sValue = Convert.ToString(drGetValue[0]);
                                                sValue = Convert.ToString(sValue).Replace("[LF]", Convert.ToChar(10).ToString());
                                            }
                                            else sValue = "0";
                                            drGetValue.Close();
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeFirst);
                                            oDestination.Value2 = (sValue);
                                            break;
                                        }
                                    case "P":
                                        {
                                            nCacheIndex = 1;
                                            string sOLE = "OLEDB;" + sSQLOLEDB;
                                            oSheet = (Excel.Worksheet)oSheets.get_Item(nSheetNumber);
                                            ((Excel._Worksheet)oSheet).Activate();
                                            Excel.PivotTable oPT = (Excel.PivotTable)oSheet.PivotTables(sRangeFirst);
                                            nCacheNum = oPT.CacheIndex;
                                            Excel.PivotCaches oPCs = oBook.PivotCaches();
                                            oPCs.Item(nCacheNum).Connection = sOLE.ToString();
                                            oPCs.Item(nCacheNum).CommandType = Excel.XlCmdType.xlCmdSql;
                                            oPCs.Item(nCacheNum).CommandText = sExecCall.ToString();
                                            oPT.RefreshTable();
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range("A1", "A1");
                                            string sA1 = oDestination.Value2.ToString();
                                            if (oPT.Value == sA1) { oPT.SelectionMode = (Excel.XlPTSelectionMode.xlLabelOnly); }
                                            break;
                                        }
                                    case "PD":
                                        {
                                            oSheet = (Excel.Worksheet)oSheets.get_Item(nSheetNumber);
                                            ((Excel._Worksheet)oSheet).Activate();
                                            Excel.PivotTable oPT = (Excel.PivotTable)oSheet.PivotTables(sRangeFirst);
                                            oPT.RefreshTable();
                                            break;
                                        }
                                    default:
                                        {
                                            OleDbConnection oleConn = new OleDbConnection(sSQLOLEDB);
                                            string sOLE = "OLEDB;" + sSQLOLEDB;
                                            sSQL = sExecCall.ToString();
                                            oleConn.Open();
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range("A1", "A1");
                                            oRange2 = oDestination.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                                            nLastRow = oRange2.Row;
                                            nLastCol = oRange2.Column;
                                            if ((nLastRow > 0) && (nLastCol > 0)) sLastCol = GetLastColumnName(nLastCol);
                                            sLastCell = sLastCol + nLastRow.ToString();
                                            GetFirstLast();
                                            //oDestination=(Excel.Range)oSheet.get_Range(sRangeFirst, sRangeLast);
                                            oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeFirst);
                                            //int i=0;
                                            SqlDataReader drCycle = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                            //while (drCycle.Read()) { i++;
                                            //   if (sObjectCD=="L") { Excel.Range cell=(Excel.Range)oRange2[i,1]; cell.Value2=Convert.ToString(drCycle[0]); }
                                            //   else { Excel.Range cell=(Excel.Range)oRange2[1,i]; cell.Value2=Convert.ToString(drCycle[0]); }
                                            //   if (i==oRange2.Count) { break; } }
                                            drCycle.Read();
                                            if (drCycle.HasRows == true)
                                            {
                                                sValue = Convert.ToString(drCycle[0]);
                                                sValue = Convert.ToString(sValue).Replace("[LF]", Convert.ToChar(10).ToString());
                                            }
                                            else sValue = "0";
                                            drCycle.Close();
                                            oDestination.Value2 = (sValue);
                                            break;
                                        }
                                }
                            }
                            //drData.Close();

                            //sSQL = "SELECT 'ObjectCD'=o.ObjectCD, COALESCE(f.RangeFirst,'') AS 'RangeFirst', COALESCE(f.RangeLast,'') AS 'RangeLast', f.Variance, f.Formula, " +
                            //   "f.ExecOrder FROM RAD_Reporting.rad.ReportSheetFormula f WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON f.ObjectID=o.ObjectID " +
                            //   "WHERE f.FormulaGroupID=" + nFormulaGroupID + " AND f.ObjectID > 0 ORDER BY f.ExecOrder";
                            //SqlDataReader drFormula = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                            DataTable dt3 = aDB.ExecuteExcelwithID(3, nFormulaGroupID);
                            foreach (DataRow drFormula in dt3.Rows)
                            {
                                sObjectCD = Convert.ToString(drFormula["ObjectCD"]);
                                sRangeFirst = Convert.ToString(drFormula["RangeFirst"]);
                                sRangeLast = Convert.ToString(drFormula["RangeLast"]);
                                nVariance = Convert.ToInt16(drFormula["Variance"]);
                                sFormula = Convert.ToString(drFormula["Formula"]);
                                nExecOrder = Convert.ToInt16(drFormula["ExecOrder"]);
                                switch (sObjectCD)
                                {
                                    case "S":
                                        {
                                            Excel.Range oFormula = (Excel.Range)oSheet.get_Range("A1", "A1");
                                            Excel.Range oFormula2 = oFormula.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                                            nLastRow = oFormula2.Row;
                                            nLastCol = oFormula2.Column;
                                            if ((nLastRow > 0) && (nLastCol > 0)) sLastCol = GetLastColumnName(nLastCol);
                                            sLastCell = sLastCol + nLastRow.ToString();
                                            oFormula = (Excel.Range)oSheet.get_Range("A1", sRangeLast);
                                            oFormula.Formula = sFormula.ToString();
                                            break;
                                        }
                                    case "R":
                                        {
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeLast);
                                            oRange.Formula = sFormula.ToString();
                                            break;
                                        }
                                    case "W":
                                        { //row
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, Type.Missing).EntireRow;
                                            oRange.Formula = sFormula.ToString();
                                            break;
                                        }
                                    case "E":
                                        { //cell
                                            Excel.Range oDestination = (Excel.Range)oSheet.get_Range(sRangeFirst, Type.Missing).EntireColumn;
                                            oRange.Formula = sFormula.ToString();
                                            break;
                                        }
                                }
                            }
                            //drFormula.Close();

                            ////CONDITIONAL FORMATTING (????)
                            //sSQL="SELECT 'ObjectCD'=o.ObjectCD, COALESCE(c.RangeFirst,'') AS 'RangeFirst', COALESCE(c.RangeLast,'') AS 'RangeLast', c.ExecOrder FROM " +
                            //      "RAD_Reporting.rad.ReportSheetCondition c WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON c.ObjectID=o.ObjectID WHERE " +
                            //      "c.ConditionGroupID=" + nConditionGroupID + " AND c.ObjectID > 0 ORDER BY c.ExecOrder";
                            //   SqlDataReader drCondition=connectDB.GetDR(sSQL,sSQLConn,nTimeout);
                            //   while (drCondition.Read()) {
                            //      sObjectCD=Convert.ToString(drCondition["ObjectCD"]);
                            //      sRangeFirst=Convert.ToString(drCondition["RangeFirst"]);
                            //      sRangeLast=Convert.ToString(drCondition["RangeLast"]);
                            //      sCondition=Convert.ToString(drCondition["Condition"]);
                            //      nExecOrder=Convert.ToInt32(drCondition["ExecOrder"]);
                            //      switch (sObjectCD) {
                            //         case "S": { break; }
                            //         case "R": { break; }
                            //         case "W": { break; }
                            //         case "L": { break; } } }
                            //   drCondition.Close();

                            // STYLES
                            if ((sFileFormatCD == "E") && (sFileExtension == ".xlsx") && (nStyleGroupID != 0))
                            {
                                //sSQL = "SELECT 'ObjectCD'=o.ObjectCD, COALESCE(s.RangeFirst,'') AS 'RangeFirst', COALESCE(s.RangeLast,'') AS 'RangeLast', s.Style, " +
                                //   "s.ExecOrder FROM RAD_Reporting.rad.ReportSheetStyle s WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON " +
                                //   "s.ObjectID=o.ObjectID WHERE s.StyleGroupID=" + nStyleGroupID + " AND s.StyleGroupID > 0 ORDER BY s.ExecOrder";
                                //SqlDataReader drStyle = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                DataTable dt4 = aDB.ExecuteExcelwithID(4, nStyleGroupID);
                                foreach (DataRow drStyle in dt4.Rows)
                                {
                                    sObjectCD = Convert.ToString(drStyle["ObjectCD"]);
                                    sRangeFirst = Convert.ToString(drStyle["RangeFirst"]);
                                    sRangeLast = Convert.ToString(drStyle["RangeLast"]);
                                    sStyle = Convert.ToString(drStyle["Style"]);
                                    nExecOrder = Convert.ToInt16(drStyle["ExecOrder"]);
                                    switch (sObjectCD)
                                    {
                                        case "T":
                                            {
                                                oRange = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeLast);
                                                if (sRangeLast == "") { oRange = (Excel.Range)oSheet.get_Range(sRangeFirst, "XFD1048576"); }
                                                oRange2 = oRange.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                                                nLastRow = oRange2.Row; nLastCol = oRange2.Column;
                                                if ((nLastRow > 0) && (nLastCol > 0)) sLastCol = GetLastColumnName(nLastCol);
                                                sLastCell = sLastCol + nLastRow.ToString();
                                                oRange3 = (Excel.Range)oSheet.get_Range(sRangeFirst, sLastCell);
                                                sTemp = "Dataset" + sRangeFirst;
                                                oSheet.ListObjects.Add(Excel.XlListObjectSourceType.xlSrcRange, oRange3, false, Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo, Type.Missing).Name = sTemp;
                                                oSheet.ListObjects[sTemp].TableStyle = sRangeStyle;
                                                break;
                                            }
                                        case "P":
                                            {
                                                Excel.PivotTable oPT = (Excel.PivotTable)oSheet.PivotTables(sRangeFirst);
                                                oPT.TableStyle = "PivotStyleLight9";
                                                //oBook.ActiveSheet.ActiveSheet.PivotTables
                                                ////Excel.PivotTable oPT=(Excel.PivotTable)oSheet.PivotTables(sRangeFirst);
                                                //Excel.PivotTables oPTs=(Excel.PivotTables)oBook.  _oSheet.PivotTables;
                                                //Excel.PivotTable oPT=(Excel.PivotTable)oSheet.PivotTables(sRangeFirst);
                                                //oPT.TableStyle
                                                ////ActiveSheet.PivotTables("PivotTable1").TableStyle2 = "PivotStyleLight9"
                                                //////IPivotTable pivotTable = sheet1.PivotTables.Add("PivotTable1", sheet1["A1"], cache);
                                                //////pivotTable.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark12;
                                                //Excel.PivotCaches oPTCs=oBook.PivotCaches();
                                                break;
                                            }
                                        case "E": { break; }
                                        case "H": { break; }
                                    }
                                }
                                //drStyle.Close();
                            }

                            if (nFormatGroupID != 0)
                            {
                                int nColWidth = 0, nRowHeight = 0, nAlignIndent = 0, nFontSize = 0, nFontColorID = 0, nCellPatternID = 0, nBorderLineStyleID = 0, nBorderWeightID = 0;
                                int nCellColorID = 0, nFormatID = 0, nFailed = 0, nAlignHorizontal = 0, nAlignVertical = 0, nBorderColorID = 0, nBorderColorIndex = 0, nAlignDegrees = 0;
                                string sAutoFilter = "", sColAutoFit = "", sRowAutoFit = "", sAlignTextWrap = "", sAlignTextShrink = "", sAlignTextMerge = "", sFontName = "", sFontStyleBold = "";
                                string sFontStyleItalic = "", sFontUnderline = "", sBorderEdgeLeft = "", sBorderEdgeRight = "", sBorderEdgeTop = "", sBorderEdgeBottom = "", sNumberFormat = "";
                                string sBorderInsideHorizontal = "", sBorderInsideVertical = "", sBorderDiagonalDown = "", sBorderDiagonalUp = "", sFontStrikethrough = "";

                                //sSQL = "SELECT o.ObjectCD, 'Object'=o.Description, rsf.execOrder, rsf.FormatID, COALESCE(rsf.RangeFirst,'') AS RangeFirst, COALESCE(rsf.RangeLast,'') AS RangeLast, " +
                                //   "rsf.Variance, rf.AutoFilter, rf.ColWidth, rf.ColAutoFit, rf.RowHeight, rf.RowAutoFit, lha.ConstantValue AS 'AlignHorizontal', lva.ConstantValue " +
                                //   "AS 'AlignVertical', rf.AlignIndent, rf.AlignTextWrap, rf.AlignTextShrink, rf.AlignTextMerge, rf.AlignDegrees, rf.FontName, rf.FontSize, rf.FontStyleBold, " +
                                //   "rf.FontStyleItalic, rf.FontUnderline, rf.FontColorID, rf.FontStrikethrough, rf.BorderEdgeLeft, rf.BorderEdgeRight, rf.BorderEdgeTop, rf.BorderEdgeBottom, " +
                                //   "rf.BorderInsideHorizontal, rf.BorderInsideVertical, rf.BorderDiagonalDown, rf.BorderDiagonalUp, lbls.ConstantValue AS 'BorderLineStyleID', rf.BorderColorID, " +
                                //   "COALESCE(lci.ExcelColorIndex,0) AS 'BorderColorIndex', lbw.ConstantValue AS 'BorderWeightID', rf.CellColorID, lcp.ConstantValue AS 'CellPatternID', lnf.NumberFormat " +
                                //   "FROM RAD_Reporting.rad.ReportSheetFormat rsf WITH (NOLOCK) " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.ReportFormat rf WITH (NOLOCK) ON rsf.FormatID=rf.FormatID " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lha WITH (NOLOCK) ON rf.AlignHorizontalID=lha.ID AND lha.xlHAlign='Y' " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lva WITH (NOLOCK) ON rf.AlignVerticalID=lva.ID AND lva.xlVAlign='Y' " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lbls WITH (NOLOCK) ON rf.BorderLineStyleID=lbls.ID AND lbls.xlLineStyle='Y' " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lbw WITH (NOLOCK) ON rf.BorderWeightID=lbw.ID AND lbw.xlBorderWeight='Y' " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lcp WITH (NOLOCK) ON rf.CellPatternID=lcp.ID AND lcp.xlPattern='Y' " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_NumberFormat lnf WITH (NOLOCK) ON rf.NumberFormatID=lnf.ID " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelColorIndex lci WITH (NOLOCK) ON rf.BorderColorID=lci.ID " +
                                //   "LEFT OUTER JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON rsf.ObjectID=o.ObjectID " +
                                //   "WHERE rsf.FormatGroupID=" + nFormatGroupID + " ORDER BY rsf.ExecOrder";
                                //SqlDataReader drFormat = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                DataTable dt5 = aDB.ExecuteExcelwithID(5, nFormatGroupID);
                                sVariables = sVariables + " Apply Formatting: ";
                                foreach (DataRow drFormat in dt5.Rows)
                                {
                                    sObjectCD = Convert.ToString(drFormat["ObjectCD"]);
                                    sObject = Convert.ToString(drFormat["Object"]);
                                    sRangeFirst = Convert.ToString(drFormat["RangeFirst"]);
                                    sRangeLast = Convert.ToString(drFormat["RangeLast"]);
                                    nVariance = Convert.ToInt32(drFormat["Variance"]);
                                    nExecOrder = Convert.ToInt16(drFormat["ExecOrder"]);
                                    nFormatID = Convert.ToInt32(drFormat["FormatID"]);
                                    sAutoFilter = Convert.ToString(drFormat["AutoFilter"]);
                                    nColWidth = Convert.ToInt16(drFormat["ColWidth"]);
                                    sColAutoFit = Convert.ToString(drFormat["ColAutoFit"]);
                                    nRowHeight = Convert.ToInt32(drFormat["RowHeight"]);
                                    sRowAutoFit = Convert.ToString(drFormat["RowAutoFit"]);
                                    nAlignHorizontal = Convert.ToInt32(drFormat["AlignHorizontal"]);
                                    nAlignVertical = Convert.ToInt32(drFormat["AlignVertical"]);
                                    nAlignIndent = Convert.ToInt16(drFormat["AlignIndent"]);
                                    sAlignTextWrap = Convert.ToString(drFormat["AlignTextWrap"]);
                                    sAlignTextShrink = Convert.ToString(drFormat["AlignTextShrink"]);
                                    sAlignTextMerge = Convert.ToString(drFormat["AlignTextMerge"]);
                                    nAlignDegrees = Convert.ToInt32(drFormat["AlignDegrees"]);
                                    sFontName = Convert.ToString(drFormat["FontName"]);
                                    nFontSize = Convert.ToInt32(drFormat["FontSize"]);
                                    sFontStyleBold = Convert.ToString(drFormat["FontStyleBold"]);
                                    sFontStyleItalic = Convert.ToString(drFormat["FontStyleItalic"]);
                                    sFontUnderline = Convert.ToString(drFormat["FontUnderline"]);
                                    nFontColorID = Convert.ToInt32(drFormat["FontColorID"]);
                                    sFontStrikethrough = Convert.ToString(drFormat["FontStrikethrough"]);
                                    nCellColorID = Convert.ToInt32(drFormat["CellColorID"]);
                                    nCellPatternID = Convert.ToInt32(drFormat["CellPatternID"]);
                                    sNumberFormat = Convert.ToString(drFormat["NumberFormat"]);
                                    nBorderLineStyleID = Convert.ToInt32(drFormat["BorderLineStyleID"]);
                                    nBorderColorID = Convert.ToInt32(drFormat["BorderColorID"]);

                                    string sObjArea = nFormatID.ToString() + "-" + sObject;
                                    if (sRangeFirst != "") sObjArea += " " + sRangeFirst;
                                    if (sRangeLast != "") sObjArea += ":" + sRangeLast;
                                    sObjArea += " Record Count: " + nRowCnt.ToString();
                                    sVariables = (sVariables == "Apply Formatting: ") ? (sVariables + sObjArea) : (sVariables + ", " + sObjArea);

                                    if (nBorderLineStyleID != 0)
                                    {
                                        sBorderEdgeLeft = Convert.ToString(drFormat["BorderEdgeLeft"]);
                                        sBorderEdgeRight = Convert.ToString(drFormat["BorderEdgeRight"]);
                                        sBorderEdgeTop = Convert.ToString(drFormat["BorderEdgeTop"]);
                                        sBorderEdgeBottom = Convert.ToString(drFormat["BorderEdgeBottom"]);
                                        sBorderInsideHorizontal = Convert.ToString(drFormat["BorderInsideHorizontal"]);
                                        sBorderInsideVertical = Convert.ToString(drFormat["BorderInsideVertical"]);
                                        sBorderDiagonalDown = Convert.ToString(drFormat["BorderDiagonalDown"]);
                                        sBorderDiagonalUp = Convert.ToString(drFormat["BorderDiagonalUp"]);
                                        nBorderWeightID = Convert.ToInt32(drFormat["BorderWeightID"]);
                                        nBorderColorIndex = Convert.ToInt32(drFormat["BorderColorIndex"]);
                                    }

                                    if (sFileExtension == ".xlsx") { oRange = (Excel.Range)oSheet.get_Range("A1", "XFD1048576"); }
                                    else { oRange = (Excel.Range)oSheet.get_Range("A1", "IV65536"); }
                                    oRange2 = oRange.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                                    nLastRow = oRange2.Row;
                                    nLastCol = oRange2.Column;
                                    if ((nLastRow > 0) && (nLastCol > 0)) sLastCol = GetLastColumnName(nLastCol);
                                    sLastCell = sLastCol + nLastRow.ToString();
                                    GetFirstLast();

                                    switch (sObjectCD)
                                    {
                                        case "S": { oRange = (Excel.Range)oSheet.get_Range("A1", sLastCell); break; }
                                        case "R": { oRange = (Excel.Range)oSheet.get_Range(sRangeFirst, sRangeLast); break; }
                                        case "W": { oRange = (Excel.Range)oSheet.get_Range(sRangeFirst, Type.Missing); break; }
                                        case "L": { oRange = (Excel.Range)oSheet.get_Range(sRangeFirst, Type.Missing); break; }
                                        default: { nFailed = 1; break; }
                                    }

                                    if (nFailed != 1)
                                    {
                                        if (sAutoFilter != "N") oRange.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
                                        if (nColWidth != 0) oRange.ColumnWidth = nColWidth;
                                        if (sColAutoFit != "N") oRange.Columns.AutoFit();
                                        if (nRowHeight != 0) oRange.RowHeight = nRowHeight;
                                        if (sRowAutoFit != "N") oRange.Rows.AutoFit();
                                        if (nAlignHorizontal != 0) oRange.HorizontalAlignment = nAlignHorizontal;
                                        if (nAlignVertical != 0) oRange.VerticalAlignment = nAlignVertical;
                                        if (nAlignIndent != 0) oRange.IndentLevel = nAlignIndent;
                                        if (sAlignTextWrap != "N") oRange.Cells.WrapText = true;
                                        if (sAlignTextShrink != "N") oRange.Cells.ShrinkToFit = true;
                                        if (sAlignTextMerge != "N") oRange.Cells.MergeCells = true;
                                        if (nAlignDegrees != 0) oRange.Cells.Orientation = nAlignDegrees;
                                        if (sFontName != "N") oRange.Font.Name = sFontName;
                                        if (nFontSize != 0) oRange.Font.Size = nFontSize;
                                        if (sFontStyleBold != "N") oRange.Font.Bold = true;
                                        if (sFontStyleItalic != "N") oRange.Font.Italic = true;
                                        if (sFontUnderline != "N") oRange.Font.Underline = true;
                                        if (nFontColorID != 0) oRange.Font.ColorIndex = nFontColorID;
                                        if (sFontStrikethrough != "N") oRange.Font.Strikethrough = true;
                                        if (nCellColorID != 0) oRange.Interior.ColorIndex = nCellColorID;
                                        if (nCellPatternID != 0) oRange.Interior.Pattern = nCellPatternID;
                                        if (sNumberFormat != "N") oRange.NumberFormat = sNumberFormat;

                                        if (nBorderLineStyleID != 0)
                                        {
                                            if (sBorderEdgeLeft == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = nBorderWeightID;
                                            }
                                            if (sBorderEdgeRight == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlEdgeRight].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = nBorderWeightID;
                                            }
                                            if (sBorderEdgeTop == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlEdgeTop].Weight = nBorderWeightID;
                                            }
                                            if (sBorderEdgeBottom == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = nBorderWeightID;
                                            }
                                            if (sBorderInsideHorizontal == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlInsideHorizontal].Weight = nBorderWeightID;
                                            }
                                            if (sBorderInsideVertical == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlInsideVertical].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlInsideVertical].Weight = nBorderWeightID;
                                            }
                                            if (sBorderDiagonalDown == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlDiagonalDown].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlDiagonalDown].Weight = nBorderWeightID;
                                            }
                                            if (sBorderDiagonalUp == "Y")
                                            {
                                                oRange.Borders[Excel.XlBordersIndex.xlDiagonalUp].LineStyle = nBorderLineStyleID;
                                                if (nBorderWeightID != 0) oRange.Borders[Excel.XlBordersIndex.xlDiagonalUp].Weight = nBorderWeightID;
                                            }
                                            if (nBorderColorID != 0) { oRange.Borders.ColorIndex = nBorderColorIndex; }
                                        }
                                    }
                                    else
                                    {
                                        sTempMsg = WrapIt((PadIt(("ERROR: Formatting Range NULL"), " ", 2, 2)), 2, 3);
                                        ErrorLogMess(3, sTempMsg);
                                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Formatting Range NULL', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                        aDB.setCommandText(sSQLD);
                                        aDB.execNoQuery();
                                    }
                                }
                                sVariables = sVariables + "; Exec SQL: " + sExecCall;
                                sTempMsg = WrapIt((PadIt((sVariables), " ", 2, 2)), 2, 3);
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sVariables + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                                //drFormat.Close();
                            }
                            oRange = (Excel.Range)oSheet.get_Range("A1", "A1");
                        }

                        if (nCacheIndex == 1)
                        {
                            //SqlConnection sqlConn = new SqlConnection(sSQLConn);
                            int nConnCount = oBook.PivotCaches().Count;
                            for (int i = 1; i < nConnCount + 1; i++)
                            {
                                oBook.PivotCaches().Item(i).Connection = "OLEDB;";
                                oBook.PivotCaches().Item(i).CommandType = Excel.XlCmdType.xlCmdDefault;
                                oBook.PivotCaches().Item(i).CommandText = "";
                            }
                        }

                        if (sLegendName != "")
                        {
                            sTempMsg = WrapIt((PadIt(("Legend Attached: Path=" + sFolderPathLegendFile), " ", 2, 2)), 2, 3);
                            ErrorLogMess(3, sTempMsg);
                            sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'Legend Required: Path=" + sFolderPathLegendFile + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQLD);
                            aDB.execNoQuery();
                            if (File.Exists(sFolderPathLegendFile))
                            {
                                Excel.Workbook oBookLegend = null;
                                oBookLegend = oExcel.Workbooks.Open(sFolderPathLegendFile, false, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                                Excel.Sheets oSheetsLegend = oBookLegend.Worksheets;
                                Excel.Worksheet oSheetLegend = (Excel.Worksheet)oSheetsLegend.get_Item(1);
                                for (int i = 1; i < nLegendSheetCount + 1; i++)
                                {
                                    oSheetLegend = (Excel.Worksheet)oSheetsLegend.get_Item(i);
                                    oSheetLegend.Copy(Type.Missing, oBook.Sheets[nSheetCount]);
                                    nSheetCount++;
                                }
                                oBookLegend.Close(Type.Missing, Type.Missing, Type.Missing);
                                if (oSheetsLegend != null) Marshal.ReleaseComObject(oSheetsLegend);
                                if (oBookLegend != null) Marshal.ReleaseComObject(oBookLegend);
                                oSheetsLegend = null;
                                oBookLegend = null;
                            }
                            else
                            {
                                nCont = 0;
                                sTempMsg = WrapIt((PadIt(("ERROR: Legend Missing-" + sLegendName.ToString()), " ", 2, 2)), 2, 3);
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Legend Missing-" + sLegendName.ToString() + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                                sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','LEGEND MISSING-" + sLegendName.ToString() + "') WHERE QueueID=" + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQL);
                                aDB.execNoQuery();
                            }
                        }

                        if (sFileFormatCD == "E")
                        {
                            string sTitle = "", sSubject = "", sAuthor = "", sManager = "", sCompany = "", sCategory = "", sKeywords = "", sComments = "";
                            //sSQL = "SELECT COALESCE(Title,'') AS Title, COALESCE(Subject,'') AS Subject, COALESCE(Author,'') AS Author, COALESCE(Manager,'') AS Manager, " +
                            //   "COALESCE(Company,'') AS Company, COALESCE(Category,'') AS Category, COALESCE(Keywords,'') AS Keywords, COALESCE(Comments,'') AS Comments " +
                            //   "FROM RAD_Reporting.rad.ReportProperties WITH (NOLOCK) WHERE ReportID=" + nReportID;
                            //SqlDataReader drProperties = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                            DataTable dt6 = aDB.ExecuteExcelwithID(6, nReportID);
                            sVariables = "Apply Formatting: ";
                            if (dt6.Rows.Count>0)
                            {
                                foreach (DataRow drProperties in dt6.Rows)
                                {
                                    sTitle = Convert.ToString(drProperties["Title"]);
                                    sSubject = Convert.ToString(drProperties["Subject"]);
                                    sAuthor = Convert.ToString(drProperties["Author"]);
                                    sManager = Convert.ToString(drProperties["Manager"]);
                                    sCompany = Convert.ToString(drProperties["Company"]);
                                    sKeywords = Convert.ToString(drProperties["Keywords"]);
                                    sComments = Convert.ToString(drProperties["Comments"]);
                                }
                            }
                            //drProperties.Close();
                            oBook.Title = sTitle;
                            oBook.Subject = sSubject;
                            oBook.Author = sAuthor;
                            object oBookBuiltInProps = oBook.BuiltinDocumentProperties;
                            Type typeBookBuiltInProps = oBookBuiltInProps.GetType();
                            object oBookProperties = typeBookBuiltInProps.InvokeMember("Item", BindingFlags.Default | BindingFlags.SetProperty,
                               null, oBookBuiltInProps, new object[] { "Manager", sManager });
                            oBookProperties = typeBookBuiltInProps.InvokeMember("Item", BindingFlags.Default | BindingFlags.SetProperty,
                               null, oBookBuiltInProps, new object[] { "Company", sCompany });
                            oBookProperties = typeBookBuiltInProps.InvokeMember("Item", BindingFlags.Default | BindingFlags.SetProperty,
                               null, oBookBuiltInProps, new object[] { "Category", sCategory });
                            oBookProperties = typeBookBuiltInProps.InvokeMember("Item", BindingFlags.Default | BindingFlags.SetProperty,
                               null, oBookBuiltInProps, new object[] { "Keywords", sKeywords });
                            oBookProperties = typeBookBuiltInProps.InvokeMember("Item", BindingFlags.Default | BindingFlags.SetProperty,
                               null, oBookBuiltInProps, new object[] { "Comments", sComments });
                        }

                        try
                        {
                            sFolderPathReportFile = sFolderPathReport + "\\" + sSaveAs + sFileExtension;
                            string sReport1 = "", sReport2 = "";
                            if (nEncrypted == 1)
                            {
                                sFolderPathReportFile = sFolderPathReport + "\\E-" + sSaveAs + sFileExtension;
                                sReport2 = sFolderPathReportFile;
                                sReport1 = sTempFilePath + "\\temp" + sFileExtension;
                            }
                            else { sReport1 = sFolderPathReportFile; }
                            Microsoft.Office.Interop.Excel.XlFileFormat oFileFormat = (Microsoft.Office.Interop.Excel.XlFileFormat)nFileFormat;
                            sReport1 = ReplaceUNC(sReport1);
                            sTempMsg = sTempMsg + "; File Name : " + sReport1 + "; Excel PID : " + nExcelPID.ToString();
                            // add saveas csv format - bts4947 02-23-2012
                            if (sFileExtension == ".csv")
                            {
                                oBook.SaveAs(sReport1, Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing,
                                   Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing,
                                   Type.Missing, Type.Missing, Type.Missing);
                            }
                            else
                            {
                                //File.Create(sReport1+".txt").Dispose();
                                string stfilenme = sSaveAs + sFileExtension;
                                oBook.SaveAs(sReport1, Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false,
                                   Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlLocalSessionChanges,
                                   false, Type.Missing, Type.Missing, false);
                            }
                            // end add code
                            //oBook.SaveAs(sReport1, Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                            ////Type.Missing, Type.Missing,
                            //false, false,
                            //Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlLocalSessionChanges, false, Type.Missing, Type.Missing, false);
                            if (nEncrypted == 1)
                            {
                                oBook.Close(Type.Missing, "temp.xls", Type.Missing);
                                if (nExcelPID != 0)
                                {
                                    Process[] pProcess;
                                    pProcess = Process.GetProcessesByName("EXCEL");
                                    foreach (Process p in Process.GetProcessesByName("EXCEL")) { if (p.Id == nExcelPID) { p.Kill(); nExcelPID = 0; break; } }
                                }
                                encryptMe.Encrypt(sReport1, sReport2, "password");
                                File.Delete(sReport1);
                            }
                            sTempMsg = sTempMsg + WrapIt((PadIt(("Report Group File Saved: Path=" + sFolderPathReportFile), " ", 2, 2)), 2, 3);
                            ErrorLogMess(3, sTempMsg);
                            sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'Report Group File Saved: Path=" + sFolderPathReportFile + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQLD);
                            aDB.execNoQuery();

                            if (nAttachmentID != 0)
                            {
                                string sTempFileName = "";
                                double dTempFileSize = 0.00;
                                DirectoryInfo dir = new DirectoryInfo(sFolderPathReport);
                                FileInfo[] files = dir.GetFiles();
                                string sAttachSize = "";
                                foreach (FileInfo file in files)
                                {
                                    sTempFileName = file.Name.ToString();
                                    if (sTempFileName == sSaveAs)
                                    {
                                        dTempFileSize = (file.Length / 1024) * .001;
                                        sAttachSize = String.Format("{0:###.00}", dTempFileSize);
                                        break;
                                    }
                                }
                                sSQL = "UPDATE RAD_Reporting.rad.ReportAttachment SET AttachmentName='" + sSaveAs.ToString() + "', AttachmentPath=' " +
                                   sFolderPathReport.ToString() + "\\', FileSize=" + sAttachSize + ", LastUpdate=GETDATE() WHERE AttachmentID=" + nAttachmentID;
                                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQL);
                                aDB.execNoQuery();
                                sTempMsg = sTempMsg + WrapIt((PadIt(("Notification Attachment Saved"), " ", 2, 2)), 2, 3);
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'Notification Attachment Saved', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                            }
                            if ((nFTPID != 0) && (nFTPID != 99))
                            {
                                string sSaveTo = sFTPFilePath + sFTPSaveTo;
                                sTemp = sSaveTo + @"\" + sFTPFileName;
                                if (sTemp != sFolderPathReportFile)
                                {
                                    // add saveas csv format - bts4947 02-23-2012
                                    if (sFileExtension == ".csv")
                                    {
                                        oBook.SaveAs(sReport1, Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing,
                                           Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing,
                                           Type.Missing, Type.Missing, Type.Missing);
                                    }
                                    else
                                    {
                                        oBook.SaveAs(sReport1, Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false,
                                           Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlLocalSessionChanges,
                                           false, Type.Missing, Type.Missing, false);
                                    }
                                    // end add code
                                    sTempMsg = sTempMsg + WrapIt((PadIt(("FTP File Saved: Path=" + @sTemp), " ", 2, 2)), 2, 3);
                                    ErrorLogMess(3, sTempMsg);
                                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'FTP File Saved: Path=" + @sTemp + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                    aDB.setCommandText(sSQLD);
                                    aDB.execNoQuery();
                                }
                            }
                            if (nEncrypted != 1)
                            {
                                oBook.Close(Type.Missing, Type.Missing, Type.Missing);
                                oExcel.Workbooks.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            nCont = 0;
                            if (nFormatGroupID != 0)
                            {
                                sTempMsg = sTempMsg + (PadIt((sVariables), " ", 2, 2));
                                ErrorLogMess(3, sTempMsg);
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', '" + sVariables + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                            }
                            exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                            sTempMsg = sTempMsg + (PadIt(("ERROR::ExecuteExcel():Excel Save"), " ", 2, 2));
                            ErrorLogMess(3, sTempMsg);
                            ErrorLogEx(3, ex);
                            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','EXCEL FAILURE-" + exMessage + "') WHERE QueueID=" + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQL);
                            aDB.execNoQuery();
                            sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Excel Save; ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQLD);
                            aDB.execNoQuery();
                            if (oExcel != null)
                            {
                                Process[] pProcess;
                                pProcess = Process.GetProcessesByName("EXCEL");
                                foreach (Process p in Process.GetProcessesByName("EXCEL")) { if (p.Id == nExcelPID) { p.Kill(); nExcelPID = 0; break; } }
                            }
                        }
                        //drSheet.Close();

                        if (nCont == 1)
                        {
                            sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'IN PROGRESS...','REPORT COMPLETE') WHERE QueueID=" + nQueueID.ToString();
                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                            aDB.setCommandText(sSQL);
                            aDB.execNoQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        nCont = 0;
                        sTempMsg = sTempMsg + (PadIt(("ERROR::ExecuteExcel():Excel Populate"), " ", 2, 2));
                        ErrorLogMess(3, sTempMsg);
                        ErrorLogEx(3, ex);
                        exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','EXCEL FAILURE-" + exMessage + "') WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Excel Populate; ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        if (oExcel != null)
                        {
                            Process[] pProcess;
                            pProcess = Process.GetProcessesByName("EXCEL");
                            foreach (Process p in Process.GetProcessesByName("EXCEL")) { if (p.Id == nExcelPID) { p.Kill(); nExcelPID = 0; break; } }
                        }
                    }
                    finally
                    {
                        if (nExcelPID != 0)
                        {
                            Process[] pProcess;
                            pProcess = Process.GetProcessesByName("EXCEL");
                            foreach (Process p in Process.GetProcessesByName("EXCEL")) { if (p.Id == nExcelPID) { p.Kill(); nExcelPID = 0; break; } }
                        }
                    }
                }
                else
                {
                    nCont = 0;
                    if (File.Exists(sFolderPathTemplateFile) == false)
                    {
                        sTempMsg = sTempMsg + WrapIt((PadIt(("ERROR: Template Missing-" + sFolderPathTemplateFile.ToString()), " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Template Missing-" + sFolderPathTemplateFile.ToString() + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','FAILURE: EXCEL TEMPLATE MISSING-" + sFolderPathTemplateFile.ToString() + "') WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                    }
                    else if (Directory.Exists(sFolderPathReport) == false)
                    {
                        sTempMsg = sTempMsg + WrapIt((PadIt(("ERROR: Report Group Folder Missing-" + sFolderPathReport), " ", 2, 2)), 2, 3);
                        ErrorLogMess(3, sTempMsg);
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Report Group Folder Missing-" + sFolderPathReport + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','FAILURE: RPT GRP FOLDER MISSING-" + sFolderPathReport + "') WHERE QueueID=" + nQueueID.ToString();
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                if (nExcelPID != 0)
                {
                    Process[] pProcess;
                    pProcess = Process.GetProcessesByName("EXCEL");
                    sTempMsg = "No. of Excel Processes open : " + pProcess.Length + "; ";
                    foreach (Process p in Process.GetProcessesByName("EXCEL")) { if (p.Id == nExcelPID) { p.Kill(); nExcelPID = 0; break; } }
                }
                nCont = 0;
                sTempMsg = sTempMsg + (PadIt(("ERROR::ExecuteExcel():Excel Open"), " ", 2, 2));
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'ExecuteExcel()', 'ERROR: Excel Open; ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                aDB.setCommandText(sSQLD);
                aDB.execNoQuery();
                sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'EXCEL IN PROGRESS...','EXCEL FAILURE-" + exMessage + "') WHERE QueueID=" + nQueueID.ToString();
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                CheckAlerts("O", 1);
            }
        }

        protected void FileTransfer(int nFTPID, int nQueueID, string sRunDatabase)
        {
            try
            {
                int nJobCount = 0, nJobStatus = 0, nJobCheck = 0;
                string sJobMessage = "", sJobID = "";
                if (sFileFormatCD == "E") sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=Status + '; FTP IN PROGRESS...' WHERE QueueID=" + nQueueID.ToString();
                else sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]='FTP IN PROGRESS...' WHERE QueueID=" + nQueueID.ToString();
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                sSQL = "DELETE FROM RAD_Reporting.rad.ReportJobID";
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                sSQL = "DELETE FROM RAD_Reporting.rad.ReportTransferLog WHERE TransferDate < DATEADD(DD,-365,GETDATE())";
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                sSQL = "EXEC RAD_Reporting.rad.sp_ReportExportFile " + nFTPID + ",'" + sRunDatabase + "'";
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                System.Threading.Thread.Sleep(5000);
                sSQL = "SELECT JobID FROM RAD_Reporting.rad.ReportJobID WITH (NOLOCK)";
                SqlDataReader drJobID = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                drJobID.Read();
                sJobID = Convert.ToString(drJobID[0]);
                drJobID.Close();
                while (nJobCount == 0)
                {
                    System.Threading.Thread.Sleep(5000);
                    sSQL = "DELETE FROM RAD_Reporting.rad.ReportTransfer";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQL = "INSERT INTO RAD_Reporting.rad.ReportTransfer EXEC msdb..sp_help_jobhistory null, @job_name='ReportExportFile',@mode=N'FULL'";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQL = "SELECT COUNT(*) FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0 AND job_id='" + sJobID + "'";
                    SqlDataReader drFTPCheck = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    drFTPCheck.Read();
                    nJobCheck = Convert.ToInt16(drFTPCheck[0]);
                    drFTPCheck.Close();
                    if (nJobCheck == 1)
                    {
                        sSQL = "UPDATE RAD_Reporting.rad.ReportTransfer SET message=REPLACE(SUBSTRING(message,1,8000), (SELECT SUBSTRING(message,(SELECT CHARINDEX('  The Job was invoked by',message) " +
                           "FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0), (SELECT CHARINDEX('.',(substring(message,(SELECT CHARINDEX('  The Job was invoked by',message) " +
                           "FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0),100))) FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0)) " +
                           "FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0),'') WHERE step_id=0;";
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        sSQL = "SELECT run_status, message FROM RAD_Reporting.rad.ReportTransfer WITH (NOLOCK) WHERE step_id=0";
                        SqlDataReader drJobStats = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                        while (drJobStats.Read())
                        {
                            nJobStatus = Convert.ToInt16(drJobStats["run_status"]);
                            sJobMessage = Convert.ToString(drJobStats["message"]);
                        }
                        drJobStats.Close();
                        nJobCount = 1;
                    }
                }
                sSQL = "INSERT INTO RAD_Reporting.rad.ReportTransferLog SELECT CONVERT(SMALLDATETIME, GETDATE(), 121) AS TransferDate, '" + sFTPName + "' AS FTPName, '" +
                   sFTPFileName + "' AS FTPFileName, '" + sFTPPath + "' AS FTPPath, b.* FROM RAD_Reporting.rad.ReportFTP a WITH (NOLOCK), RAD_Reporting.rad.ReportTransfer b " +
                   "WITH (NOLOCK) WHERE b.step_id=0 AND a.FTPID=" + nFTPID.ToString();
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
                if (nJobStatus == 1)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'FTP IN PROGRESS...','FTP COMPLETE'), EndTime='" + DateTime.Now + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                }
                else
                {
                    nCont = 0;
                    string sTempJobMess = (sJobMessage.Length > 1000) ? sJobMessage.Substring(0, 1000).ToString() : sJobMessage.ToString();
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'FTP IN PROGRESS...','FTP FAILURE-" + sTempJobMess + "'), EndTime='" + DateTime.Now + "' WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sTempMsg = WrapIt((PadIt(("ERROR: FTPID=" + nFTPID + "; Message=" + sJobMessage.ToString()), " ", 2, 2)), 2, 3);
                    ErrorLogMess(3, sTempMsg);
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'FileTransfer()', 'ERROR: FTPID=" + nFTPID + "; Message=" + sJobMessage.ToString() + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                sSQL = "EXEC RAD_Reporting.rad.sp_ReportExportFile 1,'D'";
                //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                aDB.setCommandText(sSQL);
                aDB.execNoQuery();
            }
            catch (Exception ex)
            {
                nCont = 0;
                sTempMsg = (PadIt(("ERROR::FileTransfer()"), " ", 2, 2));
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQL = "EXEC RAD_Reporting.rad.sp_ReportExportFile 1,'D'";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET [Status]=REPLACE(Status,'FTP IN PROGRESS...','FTP FAILURE') WHERE QueueID=" + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-RADx', 'FileTransfer()', 'ERROR: ex.Message=" + exMessage + "', " + nReportID.ToString() + ", " + nQueueID.ToString();
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    //CheckAlerts("O",1);
                }
                catch
                {
                    ueDescription = "ERROR::FileTransfer()";
                    SendAlertUnknown(exMessage);
                }
            }
        }

        // File manager code below.

        protected void ProcessFiles()
        {
            try
            {
                //Run at 6am
                int nQID = -1;
                DateTime today = DateTime.Now;
                DateTime dtRun = new DateTime(today.Year, today.Month, today.Day, 6, 0, 0);
                if (DateTime.Now > dtRun)
                {
                    sSQL = "SELECT TOP 1 QUEUEID FROM rad.REPORTQUEUE_NEW WITH (NOLOCK) " +
                        "WHERE STARTTIME IS NULL AND ENDTIME IS NULL AND ISNULL(STATUS,'')='' " +
                        "AND CONVERT(DATE,RUNDATE)=CONVERT(DATE,GETDATE()) AND REPORTID=0 ORDER BY 1 DESC; " +
                        "SELECT TOP 1 QUEUEID, ENDTIME FROM rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE " +
                        "CONVERT(DATE, ENDTIME) = CONVERT(DATE, GETDATE()) AND " +
                        "[STATUS] = 'FILE WEB REFRESH COMPLETE' AND REPORTID = 0 ORDER BY ENDTIME DESC;";
                    aDB.setCommandText(sSQL);
                    DataSet ds = aDB.getDataSet();
                    if ((ds != null) && (ds.Tables.Count>0))
                    {
                        DataTable dtQID = ds.Tables[0];
                        if ((dtQID != null) && (dtQID.Rows.Count>0))
                            RefreshFiles(int.Parse(dtQID.Rows[0][0].ToString()));
                        else
                        {
                            DataTable dtRefreshCount = ds.Tables[1];
                            if ((dtRefreshCount == null) || (dtRefreshCount.Rows.Count <= 0))
                            {
                                sSQL = "EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','0';";
                                aDB.setCommandText(sSQL);
                                aDB.execNoQuery();
                                ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "ProcessFiles() - Report Queue Refreshed (CFM Only)[L][B]");
                                sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'ProcessFiles()', 'Report Queue Refreshed (CFM Only)', NULL, NULL";
                                //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                                aDB.setCommandText(sSQLD);
                                aDB.execNoQuery();
                                sSQL = "SELECT TOP 1 QueueID FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE ReportID=0 " +
                                   "AND (StartTime IS NULL OR EndTime IS NULL OR Status='' OR Status LIKE '%...%') ORDER BY 1 DESC;";
                                SqlDataReader drCurrQueue = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                while (drCurrQueue.Read())
                                {
                                    nQID = Convert.ToInt16(drCurrQueue["QueueID"]);
                                }
                                drCurrQueue.Close();
                                RefreshFiles(nQID);
                            }
                            //else
                            //{
                            //    sTempMsg = "Last web refresh successfully completed at - " + dtRefreshCount.Rows[0]["ENDTIME"].ToString();
                            //    ErrorLogMess(3, sTempMsg);
                            //}
                        }
                    }

                }
                /*int nQID = -1, nCfmQID = -1, nRunNow = -1, nPast = -1, nStatus = -1;
                // 12/3 sSQL = "SELECT 'qCnt'=COUNT(*) FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE " +
                // 12/3   "(StartTime IS NULL OR EndTime IS NULL OR Status='' OR Status LIKE '%...%') OR RunNow='Y';";
                // 12/3 SqlDataReader drQueueCount = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                // 12/3 while (drQueueCount.Read()) { nRefresh = Convert.ToInt16(drQueueCount["qCnt"]); }
                // 12/3 drQueueCount.Close();
                sSQL = "SELECT QueueID, RunNow=CASE WHEN RunNow='Y' THEN 1 ELSE 0 END, 'Past'=(CASE WHEN (((DATEDIFF(MI,CONVERT(SMALLDATETIME,(CONVERT(VARCHAR(10),RunDate,121) + " +
                   "' ' + LEFT(CONVERT(TIME,RunTime),8))),GETDATE())) > 0)  OR ((EndTime IS NOT NULL) AND (CONVERT(DATE,EndTime) != CONVERT(DATE,GETDATE())))) THEN 1 ELSE 0 END), 'Status'=CASE WHEN (StartTime IS NULL OR EndTime IS NULL OR Status='' OR " +
                   "Status LIKE '%...%') THEN 1 ELSE 0 END FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE (ReportID=0)";
                SqlDataReader drQueueCfm = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                while (drQueueCfm.Read())
                {
                    nCfmQID = Convert.ToInt16(drQueueCfm["QueueID"]);
                    nRunNow = Convert.ToInt16(drQueueCfm["RunNow"]);
                    nPast = Convert.ToInt16(drQueueCfm["Past"]);
                    nStatus = Convert.ToInt16(drQueueCfm["Status"]);
                }
                drQueueCfm.Close();
                if (nCount > 0 || nCount2 > 0 || (nStatus > 0 && nPast > 0))  // changed from nRefresh 12/3
                {
                    if (nStatus == 0 && nPast > 0)
                    {
                        sSQL = "EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','0';";
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "ProcessFiles() - Report Queue Refreshed (CFM Only)[L][B]");
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'ProcessFiles()', 'Report Queue Refreshed (CFM Only)', NULL, NULL";
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQL = "SELECT QueueID FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE ReportID=0 " +
                           "AND (StartTime IS NULL OR EndTime IS NULL OR Status='' OR Status LIKE '%...%');";
                        SqlDataReader drCurrQueue = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                        while (drCurrQueue.Read())
                        {
                            nQID = Convert.ToInt16(drCurrQueue["QueueID"]);
                        }
                        drCurrQueue.Close();
                        RefreshFiles(nQID);
                    }
                    else if (nStatus > 0 && nPast > 0) RefreshFiles(nCfmQID);
                    else return;
                }
                else
                {
                    int nReturn = 0;
                    string sDataCall = "", sLastCheck = "", sCurrTime = "";
                    sCurrTime = DateTime.Now.ToString();
                    sSQL = "SELECT DataCheck, LastCheck FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE ID=(SELECT ID FROM RAD_Reporting.rad.REPORTALERT_NEW WITH (NOLOCK) WHERE AlertCode='F');";
                    SqlDataReader drDataCall = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    while (drDataCall.Read())
                    {
                        sDataCall = Convert.ToString(drDataCall["DataCheck"]);
                        sLastCheck = Convert.ToString(drDataCall["LastCheck"]);
                    }
                    drDataCall.Close();
                    SqlDataReader drDataCheck = connectDB.GetDR(sDataCall, sSQLConn, nTimeout);
                    while (drDataCheck.Read()) { nReturn = Convert.ToInt16(drDataCheck[0]); }
                    drDataCheck.Close();
                    if (nReturn == 1)
                    {
                        sSQL = "EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','0';";
                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQL);
                        aDB.execNoQuery();
                        ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "ProcessFiles() - Report Queue Refreshed (CFM Only)[L][B]");
                        sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'ProcessFiles()', 'Report Queue Refreshed (CFM Only)', NULL, NULL";
                        //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                        aDB.setCommandText(sSQLD);
                        aDB.execNoQuery();
                        sSQL = "SELECT QueueID FROM RAD_Reporting.rad.REPORTQUEUE_NEW WITH (NOLOCK) WHERE ReportID=0 " +
                           "AND (StartTime IS NULL OR EndTime IS NULL OR Status='' OR Status LIKE '%...%');";
                        SqlDataReader drCurrQueue = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                        while (drCurrQueue.Read()) { nQID = Convert.ToInt16(drCurrQueue["QueueID"]); }
                        drCurrQueue.Close();
                        RefreshFiles(nQID);
                    }
                }
                //////ADD ARCHIVING PROCESS HERE
                //NIC2028 - END HERE
                */
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::ProcessFiles()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'ProcessFiles()', 'ERROR: ex.Message=" + exMessage + "', NULL, NULL";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch (Exception e) { throw new System.Exception("ProcessFiles::EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW; Exception - " + e.Message + "; " + e.InnerException + "; " + e.StackTrace); }
            }
        }

        protected void RefreshFiles(int nQID)
        {
            try
            {
                int nID = 0, nCnt = 0, nReturn = 0;
                string sFileName = "", sFileSize = "", sFileDate = "", sSQLF = "", sFullPath = "", sFolderPath = "", sFolderPathUNC = "";
                double dTempFileSize = 0.00;
                string sCurrTime = DateTime.Now.ToString();
                if (nQID != 0)
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET StartTime=GETDATE(), [Status]='IN PROGRESS...' WHERE QueueID=" + nQID;
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                }
                DataTable dtFolders = aDB.ExecuteExcelwithString(1, sAppServer.Replace(@"\", ""));
                //sSQL = "DECLARE @RETURN INT; EXECUTE @RETURN=RAD_Reporting.rad.sp_ReportFileMgmt_NEW '" + sAppServer.Replace(@"\", "") + "'; SELECT @RETURN;";
                //SqlDataReader drExecute = connectDB.GetDR(sSQL, sSQLConn, 1800);
                //while (drExecute.Read()) { nReturn = Convert.ToInt32(drExecute[0]); }
                //drExecute.Close();

                if ((dtFolders == null) || (dtFolders.Rows.Count<=0))
                //if (nReturn != 0)
                {
                    sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::RefreshFiles(): ExecuteExcelwithString(1, sAppServer)";
                    ErrorLogMess(3, sTempMsg);
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'RefreshFiles()', 'ERROR::RefreshFiles():" + sSQL.ToString() + "', NULL, NULL";
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["ReportRefreshDays"] != null)
                    {
                        nRefreshDays = Int32.Parse(ConfigurationManager.AppSettings["ReportRefreshDays"].ToString());
                    }
                    DataTable dtFoldersFinal = dtFolders.Copy();
                    DataTable dtDistinctFolders = dtFolders.DefaultView.ToTable(true, "FolderPath");
                    foreach (DataRow dr in dtDistinctFolders.Rows)
                    {
                        sFolderPathUNC = Convert.ToString(dr["FolderPath"]);
                        sFolderPath = ReplaceUNC(sFolderPathUNC);
                        if (Directory.Exists(sFolderPath))
                        {
                            //String[] files = Directory.GetFiles(sFolderPath);
                            DirectoryInfo dir = new DirectoryInfo(sFolderPath);
                            var files = dir.EnumerateFiles().Where(file => (file.LastWriteTime.Date >= (DateTime.Today.Date.AddDays((nRefreshDays == 0) ? -15 : nRefreshDays)).Date)
                                                                            && (!file.Name.StartsWith("~$"))).ToList();
                            sTempMsg = sTempMsg + Environment.NewLine + "Refresh File Count : " + files.Count.ToString() + " for Folder :" + sFolderPathUNC;
                            foreach (FileInfo file in files)
                            {
                                //FileInfo file = new FileInfo(sfile);
                                string sTempFileName = file.Name.ToString();
                                sFileDate = file.LastWriteTime.ToString();
                                dTempFileSize = (file.Length / 1024) * .001;
                                sFileSize = String.Format("{0:###.00}", dTempFileSize);
                                var drFinal = dtFoldersFinal.AsEnumerable().Where(r => ((r.Field<String>("FileNamePattern").Contains(sTempFileName.Substring(11, (sTempFileName.Length - 11))) || (r.Field<String>("FileNamePattern").Contains(sTempFileName.Substring(13, (sTempFileName.Length - 13))))) 
                                                                                  && dr["FolderPath"].ToString().Equals(r.Field<String>("FolderPath")))).ToList();
                                if ((drFinal != null) && (drFinal.Count() > 0))
                                {
                                    sTempMsg = sTempMsg + Environment.NewLine + "File Matched : " + sFolderPathUNC + "\\" + sTempFileName;
                                    sSQL = "SELECT ID " +
                                            "FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WITH (NOLOCK) " +
                                            "WHERE FullPath='" + sFolderPathUNC + "\\" + sTempFileName + "'";
                                    SqlDataReader drFileEXT = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                    if (!drFileEXT.HasRows)
                                    {
                                        sSQL = "INSERT INTO [RAD_Reporting].[rad].[REPORTFILEEXTRACT_NEW] " +
                                                "(ReportID, TemplateID, GroupID, GroupCD, GroupAlias, GroupName, FolderID," +
                                                "FolderTypeCD, FolderName, FolderPath, FullPath, FileNamePattern, [FileName], Encrypted, URL, FileStatus, FOLDERPATHALT, FULLPATHALT, " +
                                                "FileSizeMb, FileDate, CreateDate)" +
                                            "SELECT '" + Convert.ToString(drFinal[0]["ReportID"]) + "', '" + Convert.ToString(drFinal[0]["TemplateID"]) + "', '" + Convert.ToString(drFinal[0]["GroupID"]) + "', '" + Convert.ToString(drFinal[0]["GroupCD"]) + "','" +
                                            Convert.ToString(drFinal[0]["GroupAlias"]) + "', '" + Convert.ToString(drFinal[0]["GroupName"]) + "', '" + Convert.ToString(drFinal[0]["FolderID"]) + "', '" + Convert.ToString(drFinal[0]["FolderTypeCD"]) +
                                            "', '" + Convert.ToString(drFinal[0]["FolderName"]) + "', '" + Convert.ToString(drFinal[0]["FolderPath"]) + "', '" + sFolderPathUNC + "\\" + sTempFileName + "', '" + Convert.ToString(drFinal[0]["FileNamePattern"]) + "','" + sTempFileName + "', " +
                                            "'Encrypted' = CASE WHEN '" + sTempFileName + "' LIKE 'E-%' THEN 1 ELSE 0 END, 'URL' = '<script language=JavaScript>window.open(''file:" + sFolderPathUNC + "\\" + sTempFileName +
                                                "'',''dn'',''width=1,height=1,toolbar=no,top=300,left=2000,right=1,scrollbars=no,location=1,resizable=1'');</script>', 'A', '" +
                                            sFolderPath + "', '" + (sFolderPath + "\\" + sTempFileName) + "', '" +
                                            sFileSize + "','" + sFileDate + "','" + sFileDate + "'";
                                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                        aDB.setCommandText(sSQL);
                                        aDB.execNoQuery();
                                    }
                                    if (nRefreshDays != 0)
                                    {
                                        while (drFileEXT.Read())
                                        {
                                            nID = Convert.ToInt32(drFileEXT["ID"]);
                                            sSQL = "UPDATE RAD_Reporting.rad.REPORTFILEEXTRACT_NEW SET FileStatus='A', FileSizeMb=" + sFileSize + ", FileDate='" + sFileDate + "', CreateDate='" + sFileDate + "' WHERE ID=" + nID;
                                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                            aDB.setCommandText(sSQL);
                                            aDB.execNoQuery();
                                        }
                                    }
                                    drFileEXT.Close();
                                    sSQL = "SELECT ID " +
                                            "FROM RAD_Reporting.rad.REPORTFILEWEB_NEW WITH (NOLOCK) " +
                                            "WHERE FullPath='" + sFolderPathUNC + "\\" + sTempFileName + "'";
                                    SqlDataReader drFileWeb = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                                    if (!drFileWeb.HasRows)
                                    {
                                        sSQL = "INSERT INTO [RAD_Reporting].[rad].[REPORTFILEWEB_NEW] " +
                                                "(ReportID, TemplateID, GroupID, GroupCD, GroupAlias, GroupName, FolderID," +
                                                "FolderTypeCD, FolderName, FolderPath, FullPath, FileNamePattern, [FileName], Encrypted, URL, FileStatus, FileSizeMb, FileDate, CreateDate)" +
                                            "SELECT '" + Convert.ToString(drFinal[0]["ReportID"]) + "', '" + Convert.ToString(drFinal[0]["TemplateID"]) + "', '" + Convert.ToString(drFinal[0]["GroupID"]) + "', '" + Convert.ToString(drFinal[0]["GroupCD"]) + "','" +
                                            Convert.ToString(drFinal[0]["GroupAlias"]) + "', '" + Convert.ToString(drFinal[0]["GroupName"]) + "', '" + Convert.ToString(drFinal[0]["FolderID"]) + "', '" + Convert.ToString(drFinal[0]["FolderTypeCD"]) +
                                            "', '" + Convert.ToString(drFinal[0]["FolderName"]) + "', '" + Convert.ToString(drFinal[0]["FolderPath"]) + "', '" + sFolderPathUNC + "\\" + sTempFileName + "', '" + Convert.ToString(drFinal[0]["FileNamePattern"]) + "','" + sTempFileName + "', " +
                                            "'Encrypted' = CASE WHEN '" + sTempFileName + "' LIKE 'E-%' THEN 1 ELSE 0 END, 'URL' = '<script language=JavaScript>window.open(''file:" + sFolderPathUNC + "\\" + sTempFileName +
                                                "'',''dn'',''width=1,height=1,toolbar=no,top=300,left=2000,right=1,scrollbars=no,location=1,resizable=1'');</script>', 'A', '" +
                                            sFileSize + "','" + sFileDate + "','" + sFileDate + "'";
                                        //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                        aDB.setCommandText(sSQL);
                                        aDB.execNoQuery();
                                    }
                                    if (nRefreshDays != 0)
                                    {
                                        while (drFileWeb.Read())
                                        {
                                            nID = Convert.ToInt32(drFileWeb["ID"]);
                                            sSQL = "UPDATE RAD_Reporting.rad.REPORTFILEWEB_NEW SET FileStatus='A', FileSizeMb=" + sFileSize + ", FileDate='" + sFileDate + "', CreateDate='" + sFileDate + "' WHERE ID=" + nID;
                                            //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                                            aDB.setCommandText(sSQL);
                                            aDB.execNoQuery();
                                        }
                                    }
                                    drFileWeb.Close();
                                }
                            }
                        }

                    }
                    //sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET StartTime=GETDATE(), [Status]='IN PROGRESS...' WHERE ReportID=0 AND (StartTime IS NULL OR EndTime IS NULL)";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    //sSQLF = "DELETE FROM RAD_Reporting.rad.REPORTFILEWEB_NEW WHERE GroupID NOT IN (SELECT DISTINCT GroupID FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WITH (NOLOCK)); " +
                    //   "DELETE FROM RAD_Reporting.rad.REPORTFILEWEB_NEW WHERE FolderID NOT IN (SELECT DISTINCT FolderID FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WITH (NOLOCK)); " +
                    //   "DELETE FROM RAD_Reporting.rad.REPORTFILEWEB_NEW WHERE FullPath NOT IN (SELECT DISTINCT FullPath FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WITH (NOLOCK)); " +
                    //   "UPDATE RAD_Reporting.rad.REPORTFILEEXTRACT_NEW SET FileStatus='-' WHERE ID IN (SELECT e.ID FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW e " +
                    //   "WITH (NOLOCK) INNER JOIN RAD_Reporting.rad.REPORTFILEWEB_NEW w WITH (NOLOCK) ON e.FullPath=w.FullPath); ";
                    //connectDB.ExecuteNonQuery(sSQLF, sSQLConn, nTimeout);
                    //sSQL = "SELECT ID, FOLDERPATHALT, FULLPATHALT, FileName FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WITH (NOLOCK) WHERE FileStatus='N' ORDER BY GroupAlias, FolderID";
                    //SqlDataReader drFileSize = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    ////NIC2028 - THIS SECTION NEEDS TESTING
                    //while (drFileSize.Read())
                    //{
                    //    nID = Convert.ToInt32(drFileSize["ID"]);
                    //    sFolderPath = Convert.ToString(drFileSize["FOLDERPATHALT"]);
                    //    sFullPath = Convert.ToString(drFileSize["FULLPATHALT"]);
                    //    sFileName = Convert.ToString(drFileSize["FileName"]);
                    //    if (File.Exists(sFullPath))
                    //    {
                    //        DirectoryInfo dir = new DirectoryInfo(sFolderPath);
                    //        FileInfo[] files = dir.GetFiles(sFileName);
                    //        foreach (FileInfo file in files)
                    //        {
                    //            string sTempFileName = file.Name.ToString();
                    //            sFileDate = file.LastWriteTime.ToString();
                    //            dTempFileSize = (file.Length / 1024) * .001;
                    //            sFileSize = String.Format("{0:###.00}", dTempFileSize);
                    //            sSQL = "UPDATE RAD_Reporting.rad.REPORTFILEEXTRACT_NEW SET FileStatus='A', FileSizeMb=" + sFileSize + ", FileDate='" + sFileDate + "', CreateDate='" + sCurrTime + "' WHERE ID=" + nID;
                    //            connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        sSQL = "UPDATE RAD_Reporting.rad.REPORTFILEEXTRACT_NEW SET FileStatus='X', FileSizeMb=0, CreateDate='" + sCurrTime + "' WHERE ID=" + nID;
                    //        connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    //    }
                    //}
                    //sSQLF += "INSERT INTO RAD_Reporting.rad.REPORTFILEWEB_NEW (ReportID, TemplateID, GroupID, GroupCD, GroupAlias, GroupName, FolderID, FolderTypeCD, " +
                    //   "FolderName, FolderPath, FullPath, FileNamePattern, FileName, Encrypted, FileSizeMb, FileDate, URL, FileStatus, CreateDate) " +
                    //   "SELECT ReportID, TemplateID, GroupID, GroupCD, GroupAlias, GroupName, FolderID, FolderTypeCD, FolderName, FolderPath, " +
                    //   "FullPath, FileNamePattern, FileName, Encrypted, FileSizeMb, FileDate, URL, FileStatus, CreateDate " +
                    //   "FROM RAD_Reporting.rad.REPORTFILEEXTRACT_NEW WHERE FileStatus='A';";
                    //connectDB.ExecuteNonQuery(sSQLF, sSQLConn, nTimeout);
                    ErrorLog(3, sTempMsg);
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTALERT_NEW SET LastCheck=GETDATE() WHERE AlertCode='F'";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQL = "SELECT 'Cnt'=COUNT(1) FROM RAD_Reporting.rad.REPORTFILEWEB_NEW WITH (NOLOCK) WHERE CONVERT(DATE,FILEDATE) = CONVERT(DATE,GETDATE())";
                    SqlDataReader drWebFileCount = connectDB.GetDR(sSQL, sSQLConn, nTimeout);
                    while (drWebFileCount.Read()) { nCnt = Convert.ToInt32(drWebFileCount["Cnt"]); }
                    drWebFileCount.Close();
                    sTempMsg = "File Web Refresh Complete - Record Count=" + nCnt;
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET StartTime=GETDATE(), EndTime=GETDATE(), RUNDATE=GETDATE(), RUNNOW='N', [Status]='FILE WEB REFRESH COMPLETE' WHERE ReportID=0 AND (StartTime IS NULL OR EndTime IS NULL OR ((EndTime IS NOT NULL) AND (CONVERT(DATE,EndTime) != CONVERT(DATE,GETDATE()))))";
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    ErrorLogMess(3, "[D]" + PadIt("Message:", " ", 1, 0) + "RefreshFiles() - " + sTempMsg + "[L][B]");
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'RefreshFiles()', '" + sTempMsg + "', 0, " + nQID;
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                    //NIC2028 - END HERE
                    EmailHelper._SendSingleEmail(true, MailPriority.High, sSMTPDefaultFrom, sSMTPDefaultTo, string.Empty, string.Empty, sTempMsg, sTempMsg);
                }
            }
            catch (Exception ex)
            {
                sTempMsg = PadIt("Error:", " ", 1, 0) + "ERROR::RefreshFiles()";
                ErrorLogMess(3, sTempMsg);
                ErrorLogEx(3, ex);
                exMessage = (ex.Message != null) ? ((ex.Message).Replace("'", "").ToString()) : "NULL";
                try
                {
                    sSQL = "UPDATE RAD_Reporting.rad.REPORTQUEUE_NEW SET EndTime=GETDATE(), [Status]='WEB FILE REFRESH FAILURE-" + exMessage + "' WHERE QueueID=" + nQID;
                    //connectDB.ExecuteNonQuery(sSQL, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQL);
                    aDB.execNoQuery();
                    sSQLD = "EXEC RAD_Reporting.rad.sp_ReportProcessLog_NEW 'Automation-CFM', 'RefreshFiles()', 'ERROR: ex.Message=" + exMessage + "', 0, " + nQID;
                    //connectDB.ExecuteNonQuery(sSQLD, sSQLConn, nTimeout);
                    aDB.setCommandText(sSQLD);
                    aDB.execNoQuery();
                }
                catch (Exception e) { throw new System.Exception("RefreshFiles::EXEC RAD_Reporting.rad.REPORTQUEUE_NEW/sp_ReportProcessLog_NEW; Exception - " + e.Message + "; " + e.InnerException + "; " + e.StackTrace); }
            }
        }

        private string ReplaceUNC(string sPath)
        {
            List<string> sFolderPathCfgList = sFolderPathCfg.Trim().Split(',').Select(p => p.Trim()).ToList();

            foreach (string str in sFolderPathCfgList)
            {
                if (sPath.Contains(str))
                    sPath = sPath.Replace(str, sFolderPathAltCfg);
            }
            return sPath;
        }
    }
}