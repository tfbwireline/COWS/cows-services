﻿namespace AppConstants
{
    public enum ERedesignNoteTypes
    {
        Status = 1,
        Notes = 2,
        Caveats = 3,
        System = 4
    }
}