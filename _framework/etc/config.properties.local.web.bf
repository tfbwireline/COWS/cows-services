################################################################################
# Copyright (c) 2010 by Sprint Nextel
#
# All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
# and is the sole and exclusive property of the copyright owner. 
#
################################################################################
#
# Optional host-specific local application configuration file. Used to store 
# host-specific config settings, including those to support host and environment
# sanity checking. Initialize the HOST_CHECK with the value of the string returned
# by the 'hostname' command. The JOB_FILTER setting is necessary on applications
# that deploy to multiple hosts. The LOCAL_SERVER_ROLE setting is used to trim
# the build for applications differentiated across multiple server types. The 
# WEB_SVC_INDEX setting can be used to differentiate configurations for apps on 
# multiple web servers. The ENVIRONMENT_CHECK setting can be used for sanity 
# check of the environment (to make sure the wrong configuration is not deployed).
# 
################################################################################

# host name for this config file (what is returned by the hostname command)
HOST_CHECK = tvmxd970

# filter to indicate what ^M jobs should be generated for this host
JOB_FILTER = a

# filter to trim staged code depending on server type (should be WEB, APP, or BOTH)
LOCAL_SERVER_ROLE = WEB

# index for web service domain (1, 2, 3, ...)
WEB_SVC_INDEX =

# environment validation (this must match value of ENV_NAME in config.properties)
ENVIRONMENT_CHECK = BREAKFIX
