﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body width="99%">
        <table border="1">
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 15px;">COMMON LANGUAGE Standards Request</th>
          </tr>
          <tr>
            <th colspan="2" style="padding: 5px; text-align: left;">REQUESTOR INFORMATION:</th>
          </tr>
          <tr>
            <th style="width: 30%; padding: 3px; text-align: left;">Requestor e-Mail:</th>
            <th style="width: 70%; padding: 3px; text-align: left; font-weight: normal;">
              <a href="mailto:peggy.wheaton@t-mobile.com?subject=COMMON%20LANGUAGE%20Standards%20Request">Peggy.Wheaton@t-mobile.com</a>
            </th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Telephone:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">913-439-3722</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Fascimile:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">913-523-0188</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Mailstop:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">KSOPHE0210</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Cost Center:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">01402</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Department:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">SVC Assurance - Care</th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px;"></th>
          </tr>
          <tr>
            <th colspan="2" style="padding: 5px; text-align: left;">LOCATION INFORMATION:</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">CLLI ID:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/CLLI_ID" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Site Name:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/SiteName" /></th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px;"></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Address:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/Address" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Geographical Place Name:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/GeoPlaceName" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Geopolitical Code:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/GeoCode" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Postal Code:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/PostalCode" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Country Code:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;">USA</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">County:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/County" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Legacy Community Name:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/CommunityName" /></th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px;"></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Floor:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/Floor" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Room:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/Room" /></th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px;"></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Latitude Coordinate:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/LatitudeCoordinate" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Longitude Coordinate:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/LocationInfo/LongitudeCoordinate" /></th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px;"></th>
          </tr>
          <tr>
            <th colspan="2" style="padding: 5px; text-align: left;">MISCELLANEOUS INFORMATION:</th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">T-Mobile Owned:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/SprintOwned" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Site Purpose:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/SitePurpose" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Site Owner:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/SiteOwner" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Site Phone:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/SitePhone" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">Comments:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/Comments" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">CC:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/CC" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">PSOFT Update Date:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/PSOFT" /></th>
          </tr>
          <tr>
            <th style="padding: 3px; text-align: left;">SSTAT Update Date:</th>
            <th style="padding: 3px; text-align: left; font-weight: normal;"><xsl:value-of select="/CLLI/MiscellaneousInfo/SSTAT" /></th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 3px; color: red;">
              To view this request, please
              <a>
                <xsl:attribute name="href">
									<xsl:value-of select="/CLLI/MiscellaneousInfo/CPE_Link" />
								</xsl:attribute>
                click here.
              </a>
            </th>
          </tr>
          <tr bgcolor="#D8D8D8">
            <th colspan="2" style="padding: 7px; color: red;">
              For questions contact:  <a href="mailto:LanguageStandards@t-mobile.com">Language Standards</a>
            </th>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>