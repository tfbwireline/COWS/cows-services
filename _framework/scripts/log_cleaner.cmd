@echo off
REM *******************************************************************************
REM Copyright (c) 2009 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Ryan O'Neill - initial implementation
REM   Scott Culbertson - removed hardcoding for NUM_DAYS
REM
REM Application script to delete log files over a certain age.  An ERRORLEVEL of 0
REM will be returned upon success, a non-zero ERRORLEVEL will be returned if there
REM are any errors.
REM 
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
PUSHD %~dp0..
SET APP_HOME=%CD%


REM
REM retrieve how old in days logs need to be before they are purged.
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd LOGS_DAYS_TO_PURGE NUM_DAYS
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd 120_log_cleaner.cmd 1 "ERROR: unable to retrieve LOGS_DAYS_TO_PURGE  from configuration"
  EXIT /b 1
)


REM
REM Cleans log files from %APP_HOME%\Logs\* directories.
REM
FORFILES /p "%APP_HOME%\Logs" /s /d -%NUM_DAYS% /C "CMD /C IF @ISDIR==FALSE ECHO DELETING... @FILE &DEL /Q @FILE"

IF ERRORLEVEL 0 goto end
echo An error ocurred during the process.
CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd 120_log_cleaner.cmd %errorlevel% "ERROR: Log cleaner failed to execute need to manually validate if log were cleared"
EXIT /B 2
:end
echo Command completed succesfully.
CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd 120_log_cleaner.cmd 0 "INFO : Logs successfully cleaned"
EXIT /B 0
