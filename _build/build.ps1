$dom = $env:userdomain
$usr = $env:username
$fullname = ([adsi]"WinNT://$dom/$usr,user").fullname
$buildemailLog = "build_email.txt"
if (Test-Path $buildemailLog) {
Clear-Content $buildemailLog } else { New-Item $buildemailLog -type file}
"Build completed by " + $fullname + " @ " + (Get-Date).ToString() | Out-File $buildemailLog -Append