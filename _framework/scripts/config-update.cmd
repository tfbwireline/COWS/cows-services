@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to update the application configuration. The configuration
REM will be compared against the master configuration, and if there are any deltas 
REM (settings missing or extra settings) the configuration will be updated to be in 
REM synch with the master. If a new configuration is written, the configuration will 
REM be annotated with the changes made and save with a filename containing the 
REM timestamp. Note that if any settings were added, the user should edit the 
REM configuration and initialize the new settins.
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\Scripts\_admin.xml


REM
REM call ant target
REM
"%ANT_PROG%" -f "%ANT_SCRIPT%" _config.update %*
