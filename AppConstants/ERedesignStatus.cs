﻿namespace AppConstants
{
    public enum ERedesignStatus
    {
        Draft = 220,
        Submitted = 221,
        Reviewing = 222,
        PendingSDE = 223,
        Approved = 225,
        Cancelled = 226,
        Completed = 227,
        Delete = 228,
        PendingNTE = 230
    }
}