﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt"/>
  <xsl:template match="/">
    <html>
      <body>
        <tr>
          <td width="100%" style="text-align: left; font-weight: 600">The following H5 Customer profile has been assigned to this PM. </td>
        </tr>
        <table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">

          <tr> </tr>
          <tr> </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">H5 Customer ID : </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/H5_CUST_ID" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Customer Name : </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/H5_CUST_NME" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">PM Assigned :  </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/DSPL_NME" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">MNS :  </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/MNS" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">ISIP : </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/ISIP" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Status: </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/Stus" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Effective Date: </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/Date" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Assigner: </td>
            <td width="80%">
              <xsl:value-of select="CustIDUserAssignEmail/Assigner" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>