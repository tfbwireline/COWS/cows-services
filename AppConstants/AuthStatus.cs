﻿namespace AppConstants
{
    public enum AuthStatus
    {
        Authorized = 1,
        UnAuthorized,
        NonSensitive
    }
}