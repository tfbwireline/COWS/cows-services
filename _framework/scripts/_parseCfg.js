/*******************************************************************************
* Copyright (c) 2009 by Sprint Nextel
*
* All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
* and is the sole and exclusive property of the copyright owner. 
*
* Author:
*   Woody Anderson - initial implementation
*
* synopsis: script to parse config setting from properties file
*
* arguments: required two arguments: cfg setting, cfg filename
*
*******************************************************************************/ 

  // make sure proper number of arguments passed
var error = null;
if (WScript.Arguments.Count() != 2)
{
  error = "wrong number of arguments (2 expected, "+WScript.Arguments.Count()+" found)";
}
else
{
  var setting = WScript.Arguments(0);
  var fname = WScript.Arguments(1);
  var fso = WScript.CreateObject("Scripting.FileSystemObject");
  if (!fso.FileExists(fname))
  {
    error = "config file '"+fname+"' not found";
  }
  else
  {
    var cfgFile = fso.GetFile(fname);
    var stream = cfgFile.OpenAsTextStream(1);
    while (!stream.AtEndOfStream)
    {
      var line = stream.ReadLine();
      var index, index2;
      for ( index = 0; index < (line.length-1) && line.charAt(index) <= ' '; index++ );
      if (index == line.length || line.charAt(index) == '#') continue;
      for ( index2 = index; index2 < (line.length-1) && line.charAt(index2) > ' ' && line.charAt(index2) != '='; index2++ );
      var name = line.substring(index,index2-index);
      if (name != setting) continue;
      for ( ; index2 < (line.length-1) && (line.charAt(index2) <= ' ' || line.charAt(index2) == '='); index2++ );
      var value = line.substring(index2);
      WScript.Echo(setting+"="+value);
      WScript.Quit(0);
      break;
    }
  }
}

  // if there was an error, print error message and exit
if (error != null)
{
  WScript.Echo("ERROR: _logMsg.js: "+error);
  WScript.Quit(1);
}

