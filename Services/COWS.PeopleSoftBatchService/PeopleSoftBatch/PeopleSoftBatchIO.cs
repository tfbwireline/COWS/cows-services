﻿using PgpCore;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using CL = LogManager.FileLogger;

namespace COWS.PeopleSoftBatchService
{
    public class PeopleSoftBatchIO
    {
        public int _iSleepTime = 0;

        private string _fileName = string.Empty;
        private string _encryptedFileName = string.Empty;
        private int _sequence = 0;

        private string recHdr = string.Empty;
        private string formatLineItem = string.Empty;
        private string recDtl = string.Empty;

        private PeopleSoftBatchDB _SenderDB = new PeopleSoftBatchDB();
        private RecordLayouts _Formats = new RecordLayouts();

        public void ProcessRequests()
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataRow DR;

            DS = _SenderDB.getPeopleSoftReqHdr();
            if (DS != null & DS.Tables.Count > 0)
            {
                DT = DS.Tables[0];

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    _fileName = string.Empty;
                    _encryptedFileName = string.Empty;
                    CreateFilename();

                    string bEncryptKey = ConfigurationManager.AppSettings["PsftPublicKey"].ToString();

                    //string bPath = ConfigurationManager.AppSettings["ReqFilePath"].ToString() + _fileName;
                    string bEncryptedPath = ConfigurationManager.AppSettings["ReqFilePath"].ToString() + _encryptedFileName;
                    string bPathArch = ConfigurationManager.AppSettings["ArchiveReqFilePath"].ToString() + _fileName;
                    //StreamWriter sw = new StreamWriter(bEncryptedPath, true);
                    StreamWriter swArch = new StreamWriter(bPathArch, true);

                    int iCnt = DT.Rows.Count;
                    CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " Requisition(s) found to send to SCM", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];
                        int _commentLength = DR["SHIP_CMMTS"].ToString().Length + 35;
                        string _scommentLength = _commentLength.ToString();

                        string recHdrString = string.Empty;

                        recHdrString = String.Format(_Formats.hdrFormat,
                            DR["REQSTN_NBR"].ToString(),      //Requisition Header
                            "00000",                           //Line number
                            " ",                              //Transaction Type
                            DR["CUST_ELID"].ToString(),       //ELID
                            DR["DLVY_CLLI"].ToString(),       //Deliver Code
                            DR["DLVY_NME"].ToString(),        //Deliver Name
                            DR["DLVY_ADDR1"].ToString(),      //Deliver Address1
                            DR["DLVY_ADDR2"].ToString(),      //Deliver Address2
                            DR["DLVY_ADDR3"].ToString(),      //Deliver Address3
                            DR["CSG_LVL"].ToString(),         //Deliver Address4
                            DR["DLVY_CTY"].ToString(),        //Deliver City
                            DR["DLVY_CNTY"].ToString(),       //Deliver Country
                            DR["DLVY_ST"].ToString(),         //Deliver State
                            DR["DLVY_ZIP"].ToString(),        //Deliver Zip
                            DR["DLVY_PHN_NBR"].ToString(),    //Deliver phone
                            DR["INST_CLLI"].ToString(),       //Install Code
                            DR["MRK_PKG"].ToString(),         //Mark Package
                            DR["REQSTN_DT"].ToString(),       //Requistion Date
                            DR["REF_NBR"].ToString(),         //Refrenece number
                            _scommentLength.PadLeft(4, '0'),   //Comment Length
                            DR["DLVY_BLDG"].ToString(),       // "Bldg: Building
                            DR["DLVY_FLR"].ToString(),        //"Flr: Floor
                            DR["DLVY_RM"].ToString(),         // "Rm: Room
                            DR["SHIP_CMMTS"].ToString(),      //Comments
                            DR["RFQ_INDCTR"].ToString(),      //RFQ Indicator
                            "".PadLeft(255));                  //Filler

                        recHdr = DR["REQSTN_NBR"].ToString();

                        if (recHdrString.Length > 0)
                        {
                            //sw.WriteLine(recHdrString);
                            swArch.WriteLine(recHdrString);

                            _SenderDB.updateHdrSent(Convert.ToInt32(DR["ORDR_ID"]), DR["REQSTN_NBR"].ToString(), _sequence);

                            try
                            {
                                DataSet ds = new DataSet();
                                DataTable dt = new DataTable();
                                DataRow dr;

                                ds = _SenderDB.getPeopleSoftLnItms(recHdr);
                                if (ds != null & ds.Tables.Count > 0)
                                {
                                    dt = ds.Tables[0];

                                    if ((dt != null) && (dt.Rows.Count > 0))
                                    {
                                        int tCnt = dt.Rows.Count;

                                        CL.WriteLogFile("Retrieved ", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, tCnt.ToString() + " line items found for " + recHdr, string.Empty);

                                        for (int cntr = 0; cntr < tCnt; cntr++)
                                        {
                                            int _LnoteLength = dt.Rows[cntr]["COMMENTS"].ToString().Length;
                                            int _lineNbr = cntr + 1;

                                            dr = dt.Rows[cntr];

                                            string lineItmString = string.Empty;

                                            lineItmString = String.Format(_Formats.lineFormat,
                                                   dr["REQSTN_NBR"].ToString(),         //Requisition Header
                                                   _lineNbr.ToString().PadLeft(5, '0'),                            //Line number
                                                   dr["MAT_CD"].ToString(),             //MatCode
                                                   " ",                                 //filler
                                                   dr["ITM_DES"].ToString(), //Item Desc
                                                   dr["MANF_PART_NBR"].ToString(),      //Part Number
                                                   dr["MANF_ID"].ToString(),            //Manf id
                                                   dr["ORDR_QTY"].ToString().PadLeft(11, '0'),  //Order Qty
                                                   dr["UNT_MSR"].ToString().PadRight(3, ' '),   //Unit Measure
                                                   dr["UNT_PRICE"].ToString().Replace(".", "").PadLeft(15, '0'), //Unit Price
                                                   dr["RAS_DT"].ToString(),             //RAS Date
                                                   dr["VNDR_NME"].ToString(),           // Suggested Vendor
                                                   dr["BUS_UNT_GL"].ToString(),         //Bus Unit/GL
                                                   dr["ACCT"].ToString(),               //Account
                                                   dr["COST_CNTR"].ToString(),          //Cost Center
                                                   dr["PRODCT"].ToString(),             //Product
                                                   dr["MRKT"].ToString(),               //Market
                                                   dr["AFFLT"].ToString(),              //Affiliate
                                                   dr["REGN"].ToString(),               //Region
                                                   dr["PROJ_ID"].ToString(),            //Project Id
                                                   dr["BUS_UNT_PC"].ToString(),         //PC Bus Unit
                                                   dr["ACTVY"].ToString(),              //Activity
                                                   dr["SOURCE_TYP"].ToString(),         //Source Type
                                                   dr["RSRC_CAT"].ToString(),           //Resource Category
                                                   dr["RSRC_SUB"].ToString(),           //Resource SubCategory
                                                   dr["CNTRCT_ID"].ToString(),          //Contract Id
                                                   dr["CNTRCT_LN_NBR"].ToString().PadLeft(5, '0'),     //Contract Line Nbr
                                                   dr["AXLRY_ID"].ToString(),           //Auxiliary ID
                                                   dr["INST_CD"].ToString(),            //Install Code
                                                   dr["RFQ_INDCTR"].ToString(),         //RFQ Indicator
                                                   _LnoteLength.ToString().PadLeft(4, '0'),    //Comment Length
                                                   dr["COMMENTS"].ToString(),           //Comments
                                                   "".PadRight(10),                     //Vendor Location
                                                   "".PadRight(50));                    //Filler

                                            //sw.WriteLine(lineItmString);
                                            swArch.WriteLine(lineItmString);

                                            _SenderDB.updateLineItem(Convert.ToInt32(dt.Rows[cntr]["FSA_CPE_LINE_ITEM_ID"]), dt.Rows[cntr]["REQSTN_NBR"].ToString(), _lineNbr);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error getting Line Items for " + recHdr, e.Message.ToString() + e.InnerException.ToString());
                            }
                        }
                    }
                    //sw.Close();
                    swArch.Close();

                    using (PGP pgp = new PGP())
                    {
                        pgp.EncryptFile(bPathArch, bEncryptedPath, bEncryptKey);
                        //Pgp.Pgp.EncryptFile(bEncryptedPath, bPathArch, bEncryptKey);
                        CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Request file encrypted: " + bEncryptedPath, string.Empty);
                    }
                }
                else
                {
                    if (DateTime.Now.Second % 30 == 0)
                    {
                        CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to SCM", string.Empty);
                    }
                }
            }
        }

        public void ProcessReceipts()
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataRow DR;

            DS = _SenderDB.getPeopleSoftPORcvd();
            if (DS != null & DS.Tables.Count > 0)
            {
                DT = DS.Tables[0];

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    _fileName = string.Empty;

                    CreateReceiptsFilename();
                    string bPath = ConfigurationManager.AppSettings["ReceiptFilePath"].ToString() + _fileName;
                    StreamWriter sw = new StreamWriter(bPath, true);
                    string bPathArch = ConfigurationManager.AppSettings["ArchiveReceiptFilePath"].ToString() + _fileName;
                    StreamWriter swArch = new StreamWriter(bPathArch, true);

                    //File Header
                    string _rcvdHdr = String.Format(_Formats.recHdrFormat,
                                    'H',
                                    DateTime.Now.ToString("yyyyMMdd"), // case sensitive,
                                    DateTime.Now.ToString("HHmmss"),
                                    ' '
                        );

                    sw.WriteLine(_rcvdHdr);
                    swArch.WriteLine(_rcvdHdr);

                    int iCnt = DT.Rows.Count;
                    CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " PO Receipts found to send to SCM", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];

                        string recDtlFormat = string.Empty;

                        recDtl = String.Format(_Formats.recDtlFormat,
                            "D",                                //RecordTyp
                            DR["PRCH_ORDR_NBR"].ToString(),     //Purchase Order ID
                            DR["PO_LN_NBR"].ToString().PadLeft(4, '0'),    //Purchase Order Line
                            "001",                              //Schedule Number
                            "V" + DR["PO_RCVD_ID"].ToString().PadLeft(9, '0'),  //Receipt Nbr
                            DR["RCVD_QTY"].ToString().PadLeft(9, '0'),          //Quantity Received
                            "0000",                                 //Qty Recieved behind Decimal point
                            "EA ",                              //Receipt UOM
                            DR["ORDR_QTY"].ToString().PadLeft(9, '0'),          //PO Quantity
                            "0000",                             //Qty Recieved behind Decimal point
                            DR["RCVD_DT"].ToString(),           //Receipt Date
                            DR["RCVD_TIME"].ToString().PadRight(6, '0')          //Receipt Time
                            );

                        if (recDtl.Length > 0)
                        {
                            sw.WriteLine(recDtl);
                            swArch.WriteLine(recDtl);

                            _SenderDB.updateRcvdSent(Convert.ToInt32(DR["PO_RCVD_ID"]), _sequence);
                        }
                    }

                    //File Trailer
                    iCnt = iCnt + 2; // adding Header and Trailer to line count.

                    string _rcvdTrlr = String.Format(_Formats.recTrlrFormat,
                                   'T',
                                   iCnt.ToString().PadLeft(18, '0'),
                                   ' '
                       );
                    sw.WriteLine(_rcvdTrlr);
                    swArch.WriteLine(_rcvdTrlr);
                    sw.Close();
                    swArch.Close();
                }
                else
                {
                    if (DateTime.Now.Second % 30 == 0)
                    {
                        CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Receipts to SCM", string.Empty);
                    }
                }
            }
        }

        public void GetPO_Nbrs()
        {
            _SenderDB.getPO_Nbrs();
        }

        //public void EncryptFiles()
        //{

        //}

        #region TransferFiles

        public void TransferFiles()
        {
            try
            {
                string MyBatchFile = ConfigurationManager.AppSettings["MyBatchFile"].ToString().Trim();
                string TargetDIR = ConfigurationManager.AppSettings["TargetDIR"].ToString().Trim();
                string _spathRequest = ConfigurationManager.AppSettings["spathRequest"].ToString().Trim();
                string _spathReceipt = ConfigurationManager.AppSettings["spathReceipt"].ToString().Trim();
                string _ssys = ConfigurationManager.AppSettings["ssys"].ToString().Trim();
                string _dsys = ConfigurationManager.AppSettings["dsys"].ToString().Trim();
                string _tname = ConfigurationManager.AppSettings["tname"].ToString().Trim();
                string _delete = ConfigurationManager.AppSettings["delete"].ToString().Trim();
                string _emailaddr = ConfigurationManager.AppSettings["emailaddr"].ToString().Trim();
                string _email = ConfigurationManager.AppSettings["email"].ToString().Trim();
                string _LocalFilePath = ConfigurationManager.AppSettings["LocalFilePath"].ToString().Trim();
                string _FileNameRequest = ConfigurationManager.AppSettings["FileNameRequest"].ToString().Trim();
                string _FileNameReceipts = ConfigurationManager.AppSettings["FileNameReceipts"].ToString().Trim();
                //Console.WriteLine(_spathRequest);

                ///Request
                if (System.IO.File.Exists(@_LocalFilePath + _FileNameRequest))
                {
                    // Use a try block to catch IOExceptions, to
                    // handle the case of the file already being
                    // opened by another process.
                    try
                    {
                        System.IO.File.Delete(@_LocalFilePath + _FileNameRequest);
                    }
                    catch (System.IO.IOException ex)
                    {
                        //Console.WriteLine(e.Message);
                        //Write Log
                        CL.WriteLogFile("DeleteOldRequestbat", CL.Event.Error, CL.MsgType.error, -1, string.Empty, string.Format("DeleteFile {0} Error Message: ", "") + ex.Message, ex.StackTrace);
                        return;
                    }
                }
                if (!System.IO.File.Exists(@_LocalFilePath + _FileNameRequest))
                {
                    string _MyRequestBatFile = TargetDIR + MyBatchFile + " -spath " + _spathRequest + " -ssys " + _ssys +
                                             " -dsys " + _dsys + " -tname " + _tname + " -delete " + _delete + " -emailaddr " +
                                             _emailaddr + " -email " + _email;
                    System.IO.StreamWriter SW = new System.IO.StreamWriter(@_LocalFilePath + _FileNameRequest);
                    SW.WriteLine(@_MyRequestBatFile);
                    SW.Flush();
                    SW.Close();
                    SW.Dispose();
                    SW = null;
                    bool exist = System.IO.Directory.EnumerateFiles(@_spathRequest.Substring(0, _spathRequest.Length - 5), _spathRequest.Substring(_spathRequest.Length - 5)).Any();
                    if (exist) System.Diagnostics.Process.Start(@_LocalFilePath + _FileNameRequest);
                }
                ///Receipts
                if (System.IO.File.Exists(@_LocalFilePath + _FileNameReceipts))
                {
                    // Use a try block to catch IOExceptions, to
                    // handle the case of the file already being
                    // opened by another process.
                    try
                    {
                        System.IO.File.Delete(@_LocalFilePath + _FileNameReceipts);
                    }
                    catch (System.IO.IOException ex)
                    {
                        //Console.WriteLine(e.Message);
                        //Write Log
                        CL.WriteLogFile("DeleteOldReceipts", CL.Event.Error, CL.MsgType.error, -1, string.Empty, string.Format("DeleteFile {0} Error Message: ", "") + ex.Message, ex.StackTrace);
                        return;
                    }
                }
                if (!System.IO.File.Exists(@_LocalFilePath + _FileNameReceipts))
                {
                    string _MyRequestBatFile = TargetDIR + MyBatchFile + " -spath " + _spathReceipt + " -ssys " + _ssys +
                                             " -dsys " + _dsys + " -tname " + _tname + " -delete " + _delete + " -emailaddr " +
                                             _emailaddr + " -email " + _email;
                    System.IO.StreamWriter SW = new System.IO.StreamWriter(@_LocalFilePath + _FileNameReceipts);
                    SW.WriteLine(@_MyRequestBatFile);
                    SW.Flush();
                    SW.Close();
                    SW.Dispose();
                    SW = null;
                    bool exist = System.IO.Directory.EnumerateFiles(@_spathReceipt.Substring(0, _spathReceipt.Length - 5), _spathReceipt.Substring(_spathReceipt.Length - 5)).Any();
                    if (exist) System.Diagnostics.Process.Start(@_LocalFilePath + _FileNameReceipts);
                }
            }
            catch (Exception ex)
            {
                //Write Logs
                //Console.WriteLine("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString());
                CL.WriteLogFile("Transfer", CL.Event.Error, CL.MsgType.error, -1, string.Empty, string.Format("TransferFiles {0} Error Message: ", "") + ex.Message, ex.StackTrace);
            }
        }

        #endregion TransferFiles

        #region "Private Methods"

        private void CreateFilename()
        {
            string prefix = "PO_REQLOAD";
            DateTime date = DateTime.Now;
            string extension = ".txt";

            DataTable _dt = new DataTable();

            string _type = "M";
            _dt = _SenderDB.getPeopleSoftBatchSeq(_type);

            _sequence = Convert.ToInt32(_dt.Rows[0]["BATCH_SEQ"]) + 1;

            StringBuilder sb = new StringBuilder();

            sb.Append(prefix);
            if (date == null)
            {
                sb.Append("".PadRight(8));
            }
            else
            {
                sb.Append(date.ToString("MMddyyyy"));
            }

            string _seq = _sequence.ToString();
            sb.Append(_seq.PadLeft(8, '0'));

            sb.Append(extension);

            _fileName = sb.ToString();
            _encryptedFileName = sb.ToString() + ".gpg";
        }

        private void CreateReceiptsFilename()
        {
            string prefix = "PO_RecCPE_";
            DateTime date = DateTime.Now;
            string extension = ".txt";

            DataTable _dt = new DataTable();

            string _type = "R";
            _dt = _SenderDB.getPeopleSoftBatchSeq(_type);

            _sequence = Convert.ToInt32(_dt.Rows[0]["BATCH_SEQ"]) + 1;

            StringBuilder sb = new StringBuilder();

            sb.Append(prefix);
            if (date == null)
            {
                sb.Append("".PadRight(8));
            }
            else
            {
                sb.Append(date.ToString("MMddyyyy"));
            }

            string _seq = _sequence.ToString();
            sb.Append(_seq.PadLeft(8, '0'));
            sb.Append(extension);

            _fileName = sb.ToString();
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        #endregion "Private Methods"
    }
}