﻿namespace AppConstants
{
    public enum EEventContactType
    {
        Phone,
        CellPhone,
        PhoneCD,
        CellPhoneCD,
        Email,
        PagerNumber,
        PagerPinNumber,
        ContactHrs,
        TimeZone
    }
}