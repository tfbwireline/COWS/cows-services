<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(normalize-space(/EventEmail/UCaaSEvent/CalenderEntryURL), '?eid=', /EventEmail/UCaaSEvent/EVENT_ID, '&amp;mode=0')" />
                </xsl:attribute>
                Click here to open the event, find it in your outlook calendar and remove it
              </a>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:choose>
                    <xsl:when test="contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cows.') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsdev2')">
                      <xsl:value-of select="concat(/EventEmail/UCaaSEvent/ViewEventURL, '/', /EventEmail/UCaaSEvent/EVENT_ID)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="concat(/EventEmail/UCaaSEvent/ViewEventURL, '?eid=', /EventEmail/UCaaSEvent/EVENT_ID, '&amp;email=1')" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                Click here to view Event
              </a>
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/EVENT_ID" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Status : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/EVENT_STUS_DES" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Reviewer : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/ModUserName" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Assigned UCaaS Resource : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/AssignUsers" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Title : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/EVENT_TITLE_TXT" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Activity Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Product Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_PROD_TYPE_DES" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Plan Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_PLAN_TYPE_DES" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>