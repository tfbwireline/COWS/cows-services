﻿using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace COWS.AutoReportingDx
{
    public class AutoReportingDB : MSSqlBase
    {

        public DataTable GetRptDBStatus()
        {
            setCommandText("WITH LastRestores AS (SELECT DatabaseName = [d].[name], [d].[create_date], d.state, d.state_desc, d.is_subscribed, r.restore_date, CASE WHEN (((DATEDIFF(mi, r.restore_date, getdate()) > 10) AND (CONVERT(DATE,r.restore_date) = CONVERT(DATE,GETDATE())) AND (d.state_desc = 'ONLINE') AND (d.is_subscribed != 1)) OR ((d.is_subscribed = 1) AND (d.state_desc = 'ONLINE'))) THEN 'UPD' " +
                           "ELSE 'OFF' END AS 'Report', RowNum = ROW_NUMBER() OVER(PARTITION BY d.Name ORDER BY r.[restore_date] DESC) FROM master.sys.databases d LEFT OUTER JOIN msdb.dbo.[restorehistory] r ON r.[destination_database_name] = d.Name " +
                           "WHERE [d].[name] = 'COWS') SELECT * FROM [LastRestores] WHERE [RowNum] = 1");

            return getDataTable();
        }

        public DataTable CheckIfDecryptWorking()
        {
            setCommandText("USE [COWS_Reporting] OPEN SYMMETRIC KEY FS@K3y " +
                            "DECRYPTION BY CERTIFICATE S3cFS@CustInf0; "+
                            "select top 1 dbo.decryptBinaryData(cust_nme) as cust_nme from cows.dbo.cust_scrd_data with (nolock) where cust_nme is not null");

            return getDataTable();
        }
        public DataTable ExecuteExcelwithString(byte qType, string sInput)
        {
            if (qType == 1)
            {
                setCommandText("EXECUTE RAD_Reporting.rad.sp_ReportFileMgmt_NEW '" + sInput + "'");
            }
            return getDataTable();
        }
        public DataTable ExecuteExcelwithID(byte qType, int nID)
        {
            if (qType == 1)
            {
                setCommandText("SELECT DISTINCT s.SheetCount, s.SheetNumber, s.SheetOrder, s.CreateNextSheet, COALESCE(s.TabName,'') AS TabName, 'DataGroupID'=s.DataGroupID, " +
                           "'FormulaGroupID'=s.FormulaGroupID, 'ConditionGroupID'=s.ConditionGroupID, 'StyleGroupID'=s.StyleGroupID, 'FormatGroupID'=s.FormatGroupID " +
                           "FROM RAD_Reporting.rad.ReportSheet s WITH (NOLOCK) LEFT OUTER JOIN RAD_Reporting.rad.ReportSheetData d WITH (NOLOCK) ON s.DataGroupID=d.DataGroupID " +
                           "WHERE s.SheetGroupID=" + nID + " AND d.ObjectID > 0 ORDER BY s.SheetOrder");
            }
            if (qType == 2)
            {
                setCommandText("SELECT DISTINCT 'ObjectCD'=o.ObjectCD, COALESCE(d.RangeFirst,'') AS 'RangeFirst', COALESCE(d.RangeLast,'') AS 'RangeLast', d.Variance, d.ExecOrder, " +
                                   "COALESCE(d.ExecCall,'') AS 'ExecCall' " +
                                   "FROM RAD_Reporting.rad.ReportSheetData d WITH (NOLOCK) " +
                                   "JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON d.ObjectID=o.ObjectID " +
                                   "WHERE d.DataGroupID=" + nID + " AND d.ObjectID > 0 " +
                                   "ORDER BY d.ExecOrder");
            }
            if (qType == 3)
            {
                setCommandText("SELECT DISTINCT 'ObjectCD'=o.ObjectCD, COALESCE(f.RangeFirst,'') AS 'RangeFirst', COALESCE(f.RangeLast,'') AS 'RangeLast', f.Variance, f.Formula, " +
                               "f.ExecOrder FROM RAD_Reporting.rad.ReportSheetFormula f WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON f.ObjectID=o.ObjectID " +
                               "WHERE f.FormulaGroupID=" + nID + " AND f.ObjectID > 0 ORDER BY f.ExecOrder");
            }
            if (qType == 4)
            {
                setCommandText("SELECT DISTINCT 'ObjectCD'=o.ObjectCD, COALESCE(s.RangeFirst,'') AS 'RangeFirst', COALESCE(s.RangeLast,'') AS 'RangeLast', s.Style, " +
                                   "s.ExecOrder FROM RAD_Reporting.rad.ReportSheetStyle s WITH (NOLOCK) JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON " +
                                   "s.ObjectID=o.ObjectID WHERE s.StyleGroupID=" + nID + " AND s.StyleGroupID > 0 ORDER BY s.ExecOrder");
            }
            if (qType == 5)
            {
                setCommandText("SELECT DISTINCT o.ObjectCD, 'Object'=o.Description, rsf.execOrder, rsf.FormatID, COALESCE(rsf.RangeFirst,'') AS RangeFirst, COALESCE(rsf.RangeLast,'') AS RangeLast, " +
                   "rsf.Variance, rf.AutoFilter, rf.ColWidth, rf.ColAutoFit, rf.RowHeight, rf.RowAutoFit, lha.ConstantValue AS 'AlignHorizontal', lva.ConstantValue " +
                   "AS 'AlignVertical', rf.AlignIndent, rf.AlignTextWrap, rf.AlignTextShrink, rf.AlignTextMerge, rf.AlignDegrees, rf.FontName, rf.FontSize, rf.FontStyleBold, " +
                   "rf.FontStyleItalic, rf.FontUnderline, rf.FontColorID, rf.FontStrikethrough, rf.BorderEdgeLeft, rf.BorderEdgeRight, rf.BorderEdgeTop, rf.BorderEdgeBottom, " +
                   "rf.BorderInsideHorizontal, rf.BorderInsideVertical, rf.BorderDiagonalDown, rf.BorderDiagonalUp, lbls.ConstantValue AS 'BorderLineStyleID', rf.BorderColorID, " +
                   "COALESCE(lci.ExcelColorIndex,0) AS 'BorderColorIndex', lbw.ConstantValue AS 'BorderWeightID', rf.CellColorID, lcp.ConstantValue AS 'CellPatternID', lnf.NumberFormat " +
                   "FROM RAD_Reporting.rad.ReportSheetFormat rsf WITH (NOLOCK) " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.ReportFormat rf WITH (NOLOCK) ON rsf.FormatID=rf.FormatID " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lha WITH (NOLOCK) ON rf.AlignHorizontalID=lha.ID AND lha.xlHAlign='Y' " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lva WITH (NOLOCK) ON rf.AlignVerticalID=lva.ID AND lva.xlVAlign='Y' " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lbls WITH (NOLOCK) ON rf.BorderLineStyleID=lbls.ID AND lbls.xlLineStyle='Y' " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lbw WITH (NOLOCK) ON rf.BorderWeightID=lbw.ID AND lbw.xlBorderWeight='Y' " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelEnums lcp WITH (NOLOCK) ON rf.CellPatternID=lcp.ID AND lcp.xlPattern='Y' " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_NumberFormat lnf WITH (NOLOCK) ON rf.NumberFormatID=lnf.ID " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.Report_L_ExcelColorIndex lci WITH (NOLOCK) ON rf.BorderColorID=lci.ID " +
                   "LEFT OUTER JOIN RAD_Reporting.rad.ReportObject o WITH (NOLOCK) ON rsf.ObjectID=o.ObjectID " +
                   "WHERE rsf.FormatGroupID=" + nID + " ORDER BY rsf.ExecOrder");
            }
            if (qType == 6)
            {
                setCommandText("SELECT DISTINCT COALESCE(Title,'') AS Title, COALESCE(Subject,'') AS Subject, COALESCE(Author,'') AS Author, COALESCE(Manager,'') AS Manager, " +
                               "COALESCE(Company,'') AS Company, COALESCE(Category,'') AS Category, COALESCE(Keywords,'') AS Keywords, COALESCE(Comments,'') AS Comments " +
                               "FROM RAD_Reporting.rad.ReportProperties WITH (NOLOCK) WHERE ReportID=" + nID);
            }
            return getDataTable();
        }

        public DataTable GenericSPExec(string spName)
        {
            setCommandText(spName);
            return getDataTable();
        }
    }
}