﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">

    <xsl:template match="/">
      <html>
        <body>
          <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
            <xsl:if test="/ASREmailData/ASR_TYPE_ID[.=1]">
              <tr>
                <td>SPA E Aggregated</td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Access City Name - Site Code:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ACCS_CTY_NME_SITE_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600"> IP Node (TOC/Router Location - Site code):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/IP_NODE_TXT" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Access Bandwidth (not port or service speed):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ACCS_BDWD_DES" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Transparent:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/TRNSPNT_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Partner and carrier code (access vendor):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/PTNR_CXR_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">LEC NNI (9 Digit Numeric Value):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/LEC_NNI_NBR" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Notes:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ASR_NOTES_TXT" />
                </td>
              </tr>
			  <tr>
					<td width="40%" style="text-align: left; font-weight: 600">VLAN IDs:</td>
					<td width="60%">
						<xsl:value-of select="/ASREmailData/VLAN_IDs" />
					</td>
			  </tr>
            </xsl:if>
            <xsl:if test="/ASREmailData/ASR_TYPE_ID[.=2]">
              <tr>
                <td>Dedicated</td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">IP Node (TOC/Router Location - Site code):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/IP_NODE_TXT" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Access Bandwidth (not port or service speed):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ACCS_BDWD_DES" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Partner and carrier code (access vendor):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/PTNR_CXR_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Notes:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ASR_NOTES_TXT" />
                </td>
              </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">VLAN IDs:</td>
				<td width="60%">
						<xsl:value-of select="/ASREmailData/VLAN_IDs" />
				</td>
			  </tr>
            </xsl:if>
            <xsl:if test="/ASREmailData/ASR_TYPE_ID[.=3]">
              <tr>
                <td>TDM</td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">IP Node (TOC/Router Location - Site code):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/IP_NODE_TXT" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Access Bandwidth (not port or service speed):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ACCS_BDWD_DES" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Partner and carrier code (access vendor):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/PTNR_CXR_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Entrance Assignment:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ENTRNC_ASMT_TXT" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Notes:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ASR_NOTES_TXT" />
                </td>
              </tr>
				<tr>
					<td width="40%" style="text-align: left; font-weight: 600">VLAN IDs:</td>
					<td width="60%">
						<xsl:value-of select="/ASREmailData/VLAN_IDs" />
					</td>
				</tr>
            </xsl:if>
            <xsl:if test="/ASREmailData/ASR_TYPE_ID[.=4]">
              <tr>
                <td>IPL/IP</td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Access Bandwidth (not port or service speed):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ACCS_BDWD_DES" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">IP Node (TOC/Router Location - Site code):</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/IP_NODE_TXT" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Notes:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ASR_NOTES_TXT" />
                </td>
              </tr>
				<tr>
					<td width="40%" style="text-align: left; font-weight: 600">VLAN IDs:</td>
					<td width="60%">
						<xsl:value-of select="/ASREmailData/VLAN_IDs" />
					</td>
				</tr>
            </xsl:if>
            <xsl:if test="/ASREmailData/ASR_TYPE_ID[.=5]">
              <tr>
                <td>Off Net</td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">What is the Master VAS FTN:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/MSTR_FTN_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">DLCI:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/DLCI_DES" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Does the H1 match the Master VAS:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/H1_MATCH_MSTR_VAS_CD" />
                </td>
              </tr>
              <tr>
                <td width="40%" style="text-align: left; font-weight: 600">Notes:</td>
                <td width="60%">
                  <xsl:value-of select="/ASREmailData/ASR_NOTES_TXT" />
                </td>
              </tr>
				<tr>
					<td width="40%" style="text-align: left; font-weight: 600">VLAN IDs:</td>
					<td width="60%">
						<xsl:value-of select="/ASREmailData/VLAN_IDs" />
					</td>
				</tr>
            </xsl:if>
          </table>
        </body>
      </html>
    </xsl:template>
</xsl:stylesheet>