using System.Data;
using System.Data.SqlClient;

namespace COWS.AutoReportingDx.Connect
{
    public class ConnectDB
    {
        public void ExecuteNonQuery(string sSQL, string sConnStr, int nTimeout)
        {
            SqlConnection sqlConn = new SqlConnection(sConnStr);
            SqlCommand sqlCmd = new SqlCommand(sSQL, sqlConn);
            sqlCmd.Connection.Open();
            sqlCmd.CommandTimeout = (nTimeout == 0) ? 600 : nTimeout;
            sqlCmd.ExecuteNonQuery();
            sqlCmd.Dispose();
            sqlConn.Close();
        }

        public SqlDataReader GetDR(string sSQL, string sConnStr, int nTimeout)
        {
            SqlConnection sqlConn = new SqlConnection(sConnStr);
            SqlCommand sqlCmd = new SqlCommand(sSQL, sqlConn);
            sqlCmd.Connection.Open();
            sqlCmd.CommandTimeout = (nTimeout == 0) ? 600 : nTimeout;
            SqlDataReader dr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
            return dr;
        }
    }
}