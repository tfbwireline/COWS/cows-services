﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:template name="Style">
    <style type="text/css">
      body {
      font-family:Verdana;
      font-size: 10pt;
      background-color: white;
      color: black; }
      td {
      font-family:Verdana;
      font-size: 10pt;
      background-color: white;
      color: black; }
      th {
      font-family:Verdana;
      font-size: 10pt;
      background-color: white;
      color: black; }
      p {
      margin-top: 8pt;
      margin-bottom: 8pt; }
      p + p {
      margin-top: 8pt;
      margin-bottom: 8pt;
	  WORD-BREAK:break-word;
      }
      <!-- More definitions -->
    </style>
  </xsl:template>
</xsl:stylesheet>