﻿namespace AppConstants
{
    public enum ECustomerType
    {
        Dedicated = 1,
        Managed = 2
    }
}