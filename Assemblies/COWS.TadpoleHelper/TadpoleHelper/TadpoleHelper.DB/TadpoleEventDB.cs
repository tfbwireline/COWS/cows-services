﻿using AppConstants;
using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Transactions;
using FL = LogManager.FileLogger;

namespace TadpoleHelper

{
    public class TadpoleEventDB : DBBase
    {
        private short FedlineEventID = 9;
        private short FedlineCalendarType = 30;
        private List<string> HeadendTypes = new List<string>() { "HED", "HEI", "HEM" };
        public static Func<COWS, int, IQueryable<ResponseEntity>> getMessages;   //Compile the getResponses query...

        public TadpoleEventDataEntity GetPreviousRequest(int FRBRequestID, int sequenceNumber)
        {
            TadpoleEventDataEntity response = new TadpoleEventDataEntity();
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    CDB.ObjectTrackingEnabled = false;

                    #region FEDLINE_EVENT_TADPOLE_DATA

                    FEDLINE_EVENT_TADPOLE_DATA td = (from fetd in CDB.FEDLINE_EVENT_TADPOLE_DATA
                                                     where fetd.FRB_REQ_ID == FRBRequestID && fetd.REC_STUS_ID == (byte)ERecStatus.Active
                                                        && !HeadendTypes.Contains(fetd.ORDR_TYPE_CD)
                                                     orderby fetd.FEDLINE_EVENT_ID descending
                                                     select fetd).FirstOrDefault();

                    //If no base event found...
                    if (td == null)
                        throw new OutOfSequence(string.Format("Modify/Cancel received but no Install Event request found for RequestID {0}", FRBRequestID));

                    // IF Sequence is out of order, throw new "OutOfSequence" error
                    if (sequenceNumber <= td.SEQ_NBR)
                        throw new OutOfSequence(string.Format("Version {0} received, current version is {1}", sequenceNumber, td.SEQ_NBR));

                    response.EventID = td.EVENT_ID;
                    response.FRBRequestID = td.FRB_REQ_ID;
                    response.SequenceNumber = td.SEQ_NBR;
                    response.FRBOrderType = td.FRB_ORDR_TYPE_CD;
                    response.OrderType = (ActionType)Enum.Parse(typeof(ActionType), td.ORDR_TYPE_CD, true);
                    response.DesignType = td.DSGN_TYPE_CD;
                    response.StartTime = td.STRT_TME;
                    response.EndTime = td.END_TME;
                    response.OrganizationName = CDB.GetDeCryptValue(td.CUST_NME).FirstOrDefault().Item;
                    response.OrganizationID = td.ORG_ID;
                    response.DeviceID = td.DEV_NME;
                    response.DeviceSerialNumber = td.DEV_SERIAL_NBR;
                    response.DeviceModel = td.DEV_MODEL_NBR;
                    response.DeviceVendor = td.DEV_VNDR_NME;
                    response.isMove = td.IS_MOVE_CD;
                    response.isExpedite = td.IS_EXPEDITE_CD;
                    response.ExistingDevice = td.XST_DEV_NME;
                    response.FRBSentDate = td.REQ_RECV_DT;
                    response.OrderSubType = td.ORDR_SUB_TYPE_NME;
                    response.FRBInstallType = td.FRB_INSTL_TYPE_NME;
                    response.ReplacementReason = td.REPLMT_REAS_NME;

                    response.OrigDeviceID = td.ORIG_DEV_ID;
                    response.OrigDeviceSerialNumber = td.ORIG_DEV_SERIAL_NBR;
                    response.OrigDeviceModel = td.ORIG_MODEL;
                    response.OrigFRBRequestID = td.ORIG_FRB_REQ_ID ?? 0;

                    #endregion FEDLINE_EVENT_TADPOLE_DATA

                    #region Contacts

                    var contacts = (from con in CDB.FEDLINE_EVENT_CNTCT
                                    where con.FEDLINE_EVENT_ID == td.FEDLINE_EVENT_ID
                                    select new
                                    {
                                        con.ROLE_ID,
                                        con.CNTCT_TYPE_ID,
                                        con.CNTCT_NME,
                                        con.PHN_NBR,
                                        con.PHN_EXT_NBR,
                                        con.EMAIL_ADR
                                    });

                    foreach (var fec in contacts)
                    {
                        TadpoleContact tc = new TadpoleContact();
                        tc.RoleID = fec.ROLE_ID;
                        tc.ContactTypeID = fec.CNTCT_TYPE_ID;
                        tc.Name = CDB.GetDeCryptValue(fec.CNTCT_NME).FirstOrDefault().Item;
                        tc.PhoneNumber = fec.PHN_NBR;
                        tc.PhoneExtension = fec.PHN_EXT_NBR;
                        tc.EmailAddress = CDB.GetDeCryptValue(fec.EMAIL_ADR).FirstOrDefault().Item;
                        if (tc.RoleID == 1)
                            response.SiteContacts.Add(tc);
                        else if (tc.RoleID == 74)
                            response.ShippingContacts.Add(tc);
                    }

                    #endregion Contacts

                    #region Addresses

                    var addresses = (from adr in CDB.FEDLINE_EVENT_ADR
                                     where adr.FEDLINE_EVENT_ID == td.FEDLINE_EVENT_ID
                                     select new
                                     {
                                         adr.ADR_TYPE_ID,
                                         adr.STREET_1_ADR,
                                         adr.STREET_2_ADR,
                                         adr.CITY_NME,
                                         adr.STT_CD,
                                         adr.ZIP_CD,
                                         adr.PRVN_NME,
                                         adr.CTRY_CD
                                     });

                    foreach (var ad in addresses)
                    {
                        TadpoleAddress ta = new TadpoleAddress();
                        ta.AddressTypeID = ad.ADR_TYPE_ID;
                        ta.StreetAddress1 = CDB.GetDeCryptValue(ad.STREET_1_ADR).FirstOrDefault().Item;
                        ta.StreetAddress2 = CDB.GetDeCryptValue(ad.STREET_2_ADR).FirstOrDefault().Item;
                        ta.City = CDB.GetDeCryptValue(ad.CITY_NME).FirstOrDefault().Item;
                        ta.State = CDB.GetDeCryptValue(ad.STT_CD).FirstOrDefault().Item;
                        ta.ZipPostalCode = CDB.GetDeCryptValue(ad.ZIP_CD).FirstOrDefault().Item;
                        ta.Province = CDB.GetDeCryptValue(ad.PRVN_NME).FirstOrDefault().Item;
                        ta.Country = ad.CTRY_CD;
                        response.Addresses.Add(ta);
                    }

                    #endregion Addresses

                    #region Config Data

                    //I don't think we need to return the config data to service...

                    #endregion Config Data

                    var userData = (from ud in CDB.FEDLINE_EVENT_USER_DATA
                                    where ud.EVENT_ID == response.EventID
                                    select new { ud.EVENT_STUS_ID, ud.EVENT_TITLE_TXT }).SingleOrDefault();

                    response.Status = userData.EVENT_STUS_ID;
                    response.Title = CDB.GetDeCryptValue(userData.EVENT_TITLE_TXT).FirstOrDefault().Item;

                    return response;
                }
            }
            catch (OutOfSequence oos)
            {
                throw oos;
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error getting previous request data, Tadpole Request ID: {0} Version: {1}", FRBRequestID, sequenceNumber) + Environment.NewLine + ex.ToString(), string.Empty);
                return response;
            }
        }

        public bool InsertEvent(TadpoleEventDataEntity tadpoleEvent, string xmlString, bool toLog)
        {
            /* If new Install:
             *      Insert new Event (unique, done here).
             *      Insert Tadpole Data (common)
             *      Create Manual entry Table (unique)
             *      Update Event History with incoming notes. (common)
             *      Insert Calendar Appointment
             *      Log XML
             * If Cancel:
             *      Insert new Sequence Number (common)
             *      Update Manual entry Table Status
             *      Update Event History with incoming notes. (common)
             *      Delete Calendar Appointment
             *      Log XML
             * If Modify:
             *      Insert new Sequence Number (common)
             *      Update Manual entry Table Status if the order is completed.
             *      Update Event History with incoming notes. (common)
             *      Update Calendar Appointment time
             *      Log XML
             */
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                using (TransactionScope scope = new TransactionScope()) //Exception automatically rolls back DB updates
                {
                    #region New Event

                    //EventID = 0 for New Install, otherwise event ID is already populated
                    if (tadpoleEvent.EventID == 0)
                    {
                        EVENT e = new EVENT();
                        e.CREAT_DT = DateTime.Now;
                        e.CSG_LVL_ID = (byte)ECSGLevel.CSGLevel2;
                        e.EVENT_TYPE_ID = FedlineEventID;

                        CDB.EVENT.InsertOnSubmit(e);
                        CDB.SubmitChanges();
                        tadpoleEvent.EventID = e.EVENT_ID;
                    }

                    #endregion New Event

                    #region Set old entries to Inactive

                    if (tadpoleEvent.EventID != 0)
                    {
                        var oldEv = (from fetd in CDB.FEDLINE_EVENT_TADPOLE_DATA
                                     where fetd.EVENT_ID == tadpoleEvent.EventID && fetd.REC_STUS_ID == (byte)ERecStatus.Active
                                        && !HeadendTypes.Contains(fetd.ORDR_TYPE_CD)
                                     select fetd.FEDLINE_EVENT_ID);
                        foreach (int i in oldEv)
                        {
                            //Using .Attach allows LINQ to SQL to generate an update statement without selecting the entire row
                            //Initialize the object, attach it, then change it.  Submit Changes then creates the update statement
                            FEDLINE_EVENT_TADPOLE_DATA fea = new FEDLINE_EVENT_TADPOLE_DATA() { FEDLINE_EVENT_ID = i, REC_STUS_ID = (byte)ERecStatus.Active };
                            CDB.FEDLINE_EVENT_TADPOLE_DATA.Attach(fea);
                            fea.REC_STUS_ID = (byte)ERecStatus.InActive;
                            CDB.SubmitChanges();
                        }
                    }

                    #endregion Set old entries to Inactive

                    #region Insert New Sequence

                    FEDLINE_EVENT_TADPOLE_DATA td = new FEDLINE_EVENT_TADPOLE_DATA();
                    td.EVENT_ID = tadpoleEvent.EventID;
                    td.FRB_REQ_ID = tadpoleEvent.FRBRequestID;
                    td.SEQ_NBR = tadpoleEvent.SequenceNumber;
                    td.ORDR_TYPE_CD = tadpoleEvent.OrderType.ToString();
                    td.FRB_ORDR_TYPE_CD = tadpoleEvent.FRBOrderType;
                    td.DSGN_TYPE_CD = tadpoleEvent.DesignType;
                    td.STRT_TME = tadpoleEvent.StartTime;
                    td.END_TME = tadpoleEvent.EndTime;
                    td.CUST_NME = CDB.GetEncryptValue(tadpoleEvent.OrganizationName).FirstOrDefault().Item;
                    td.ORG_ID = tadpoleEvent.OrganizationID;
                    td.DEV_NME = tadpoleEvent.DeviceID;
                    td.DEV_SERIAL_NBR = tadpoleEvent.DeviceSerialNumber;
                    td.DEV_MODEL_NBR = tadpoleEvent.DeviceModel;
                    td.DEV_VNDR_NME = tadpoleEvent.DeviceVendor;
                    td.CREAT_DT = DateTime.Now;
                    td.REC_STUS_ID = (byte)ERecStatus.Active;
                    td.IS_MOVE_CD = tadpoleEvent.isMove;
                    td.IS_EXPEDITE_CD = tadpoleEvent.isExpedite;
                    td.XST_DEV_NME = tadpoleEvent.ExistingDevice;
                    td.REQ_RECV_DT = tadpoleEvent.FRBSentDate;
                    td.ORDR_SUB_TYPE_NME = tadpoleEvent.OrderSubType;
                    td.FRB_INSTL_TYPE_NME = tadpoleEvent.FRBInstallType;
                    td.REPLMT_REAS_NME = tadpoleEvent.ReplacementReason;

                    td.ORIG_DEV_ID = tadpoleEvent.OrigDeviceID;
                    td.ORIG_DEV_SERIAL_NBR = tadpoleEvent.OrigDeviceSerialNumber;
                    td.ORIG_MODEL = tadpoleEvent.OrigDeviceModel;
                    td.ORIG_FRB_REQ_ID = tadpoleEvent.OrigFRBRequestID;

                    CDB.FEDLINE_EVENT_TADPOLE_DATA.InsertOnSubmit(td);
                    CDB.SubmitChanges();

                    foreach (TadpoleContact tc in tadpoleEvent.SiteContacts)
                    {
                        FEDLINE_EVENT_CNTCT fec = new FEDLINE_EVENT_CNTCT();
                        fec.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                        fec.ROLE_ID = tc.RoleID;
                        fec.CNTCT_TYPE_ID = tc.ContactTypeID;
                        fec.CNTCT_NME = CDB.GetEncryptValue(tc.Name).FirstOrDefault().Item;
                        fec.PHN_NBR = tc.PhoneNumber;
                        fec.PHN_EXT_NBR = tc.PhoneExtension;
                        fec.EMAIL_ADR = CDB.GetEncryptValue(tc.EmailAddress).FirstOrDefault().Item;
                        fec.CREAT_DT = DateTime.Now;

                        CDB.FEDLINE_EVENT_CNTCT.InsertOnSubmit(fec);
                        CDB.SubmitChanges();
                    }

                    foreach (TadpoleContact tc in tadpoleEvent.ShippingContacts)
                    {
                        FEDLINE_EVENT_CNTCT fec = new FEDLINE_EVENT_CNTCT();
                        fec.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                        fec.ROLE_ID = tc.RoleID;
                        fec.CNTCT_TYPE_ID = tc.ContactTypeID;
                        fec.CNTCT_NME = CDB.GetEncryptValue(tc.Name).FirstOrDefault().Item;
                        fec.PHN_NBR = tc.PhoneNumber;
                        fec.PHN_EXT_NBR = tc.PhoneExtension;
                        fec.EMAIL_ADR = CDB.GetEncryptValue(tc.EmailAddress).FirstOrDefault().Item;
                        fec.CREAT_DT = DateTime.Now;

                        CDB.FEDLINE_EVENT_CNTCT.InsertOnSubmit(fec);
                        CDB.SubmitChanges();
                    }

                    foreach (TadpoleAddress ta in tadpoleEvent.Addresses)
                    {
                        FEDLINE_EVENT_ADR fa = new FEDLINE_EVENT_ADR();
                        fa.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                        fa.ADR_TYPE_ID = ta.AddressTypeID;
                        fa.STREET_1_ADR = CDB.GetEncryptValue(ta.StreetAddress1).FirstOrDefault().Item;
                        fa.STREET_2_ADR = CDB.GetEncryptValue(ta.StreetAddress2).FirstOrDefault().Item;
                        fa.CITY_NME = CDB.GetEncryptValue(ta.City).FirstOrDefault().Item;
                        fa.STT_CD = CDB.GetEncryptValue(ta.State).FirstOrDefault().Item;
                        fa.ZIP_CD = CDB.GetEncryptValue(ta.ZipPostalCode).FirstOrDefault().Item;
                        fa.PRVN_NME = CDB.GetEncryptValue(ta.Province).FirstOrDefault().Item;
                        fa.CTRY_CD = ta.Country;
                        fa.CREAT_DT = DateTime.Now;

                        CDB.FEDLINE_EVENT_ADR.InsertOnSubmit(fa);
                        CDB.SubmitChanges();
                    }

                    #endregion Insert New Sequence

                    #region Insert ACK message to msg table so it can be resent if needed

                    FEDLINE_EVENT_MSG ackMSG = new FEDLINE_EVENT_MSG();
                    ackMSG.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                    ackMSG.FEDLINE_EVENT_MSG_STUS_NME = "ACK";
                    ackMSG.RSPN_DES = null;
                    ackMSG.STUS_ID = 11;    //Mark it as sent because the service will do the sending...
                    ackMSG.CREAT_DT = DateTime.Now;
                    ackMSG.SENT_DT = DateTime.Now;
                    CDB.FEDLINE_EVENT_MSG.InsertOnSubmit(ackMSG);
                    CDB.SubmitChanges();

                    #endregion Insert ACK message to msg table so it can be resent if needed

                    #region Config Data

                    if (tadpoleEvent.OrderType == ActionType.NCI)
                    {
                        foreach (TadpoleConfig tc in tadpoleEvent.Configs)
                        {
                            FEDLINE_EVENT_CFGRN_DATA cd = new FEDLINE_EVENT_CFGRN_DATA();
                            cd.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                            cd.PORT_NME = tc.PortName;
                            cd.IP_ADR = tc.IPAddress ?? "";
                            cd.SUBNET_MASK_ADR = tc.SubnetMask;
                            cd.DFLT_GTWY_ADR = tc.DefaultGateway;
                            cd.SPEED_NME = tc.SPEED;
                            cd.DX_NME = tc.Duplex;
                            cd.MAC_ADR = tc.MACAddress;
                            CDB.FEDLINE_EVENT_CFGRN_DATA.InsertOnSubmit(cd);
                            CDB.SubmitChanges();
                        }
                    }
                    else
                    {
                        IQueryable<FEDLINE_EVENT_CFGRN_DATA> configs = CDB.FEDLINE_EVENT_CFGRN_DATA.Where(a => a.FEDLINE_EVENT_TADPOLE_DATA.FRB_REQ_ID == td.FRB_REQ_ID && a.FEDLINE_EVENT_ID != td.FEDLINE_EVENT_ID);
                        //CDB.FEDLINE_EVENT_CFGRN_DATA.AttachAll(configs);
                        foreach (FEDLINE_EVENT_CFGRN_DATA cd in configs)
                        {
                            cd.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                        }

                        CDB.SubmitChanges();
                    }

                    #endregion Config Data

                    #region User Entered Event Data

                    if (tadpoleEvent.OrderType == ActionType.NCI || tadpoleEvent.OrderType == ActionType.DCS)
                    {
                        FEDLINE_EVENT_USER_DATA feu = new FEDLINE_EVENT_USER_DATA();
                        feu.EVENT_ID = tadpoleEvent.EventID;
                        feu.EVENT_STUS_ID = tadpoleEvent.Status;
                        feu.EVENT_TITLE_TXT = CDB.GetEncryptValue(tadpoleEvent.Title).FirstOrDefault().Item;
                        feu.CMPLT_EMAIL_CC_ADR = (from lu in CDB.LK_USER where lu.USER_ID == 5000 select lu.EMAIL_ADR).SingleOrDefault();
                        CDB.FEDLINE_EVENT_USER_DATA.InsertOnSubmit(feu);
                        CDB.SubmitChanges();
                    }
                    else if (tadpoleEvent.OrderType == ActionType.NCC || tadpoleEvent.OrderType == ActionType.DCC || tadpoleEvent.OrderType == ActionType.NIC)
                    {
                        FEDLINE_EVENT_USER_DATA feu = new FEDLINE_EVENT_USER_DATA() { EVENT_ID = tadpoleEvent.EventID, EVENT_STUS_ID = 0, ACTV_USER_ID = 0 };

                        CDB.FEDLINE_EVENT_USER_DATA.Attach(feu);
                        feu.EVENT_STUS_ID = tadpoleEvent.Status;
                        if (tadpoleEvent.ChangeLog.Contains("Status changed from Completed to"))
                        {
                            feu.ACTV_USER_ID = null;
                        }
                        CDB.SubmitChanges();

                        if (tadpoleEvent.Status == 13)
                        {
                            FEDLINE_EVENT_MSG fem = new FEDLINE_EVENT_MSG();
                            fem.FEDLINE_EVENT_ID = td.FEDLINE_EVENT_ID;
                            fem.FEDLINE_EVENT_MSG_STUS_NME = "StatusMessage";
                            fem.RSPN_DES = "Cancelled";
                            fem.STUS_ID = 10;
                            fem.CREAT_DT = DateTime.Now;
                            CDB.FEDLINE_EVENT_MSG.InsertOnSubmit(fem);
                            CDB.SubmitChanges();
                        }
                    }
                    else if (tadpoleEvent.OrderType == ActionType.NCM || tadpoleEvent.OrderType == ActionType.NIM || tadpoleEvent.OrderType == ActionType.NDM)
                    {
                        FEDLINE_EVENT_USER_DATA feu = new FEDLINE_EVENT_USER_DATA() { EVENT_ID = tadpoleEvent.EventID, EVENT_STUS_ID = 0, ACTV_USER_ID = 0 };

                        CDB.FEDLINE_EVENT_USER_DATA.Attach(feu);
                        feu.EVENT_STUS_ID = tadpoleEvent.Status;
                        feu.EVENT_TITLE_TXT = CDB.GetEncryptValue(tadpoleEvent.Title).FirstOrDefault().Item;
                        if (tadpoleEvent.ChangeLog.Contains("Status changed from Completed to") || tadpoleEvent.OrderType == ActionType.NDM)
                        {
                            feu.ACTV_USER_ID = null;
                        }
                        CDB.SubmitChanges();
                    }

                    #endregion User Entered Event Data

                    #region InsertEventNote

                    EVENT_HIST eh = new EVENT_HIST();
                    eh.EVENT_ID = tadpoleEvent.EventID;
                    eh.ACTN_ID = tadpoleEvent.ActionID;
                    eh.CMNT_TXT = tadpoleEvent.ChangeLog;
                    eh.CREAT_BY_USER_ID = 1;
                    eh.CREAT_DT = DateTime.Now;
                    eh.PRE_CFG_CMPLT_CD = "N";
                    CDB.EVENT_HIST.InsertOnSubmit(eh);
                    CDB.SubmitChanges();

                    #endregion InsertEventNote

                    #region Calendar

                    if (tadpoleEvent.OrderType == ActionType.NCI && tadpoleEvent.StartTime != null && tadpoleEvent.EndTime != null)
                    {
                        //If changes need to be made here, check in FedlineDB.cs file in DB project for Headend Submission.  That code is copied from here
                        string maskedCity = "Overland Park";
                        string maskedState_Province = "KS";
                        string maskedCountry_Region = "US";
                        string maskedNumber = "Installation Contact Phone: 9999999999";

                        APPT a = new APPT();
                        a.EVENT_ID = tadpoleEvent.EventID;
                        a.STRT_TMST = (DateTime)tadpoleEvent.StartTime;
                        a.END_TMST = tadpoleEvent.EndTime;
                        a.RCURNC_CD = false;
                        a.RCURNC_DES_TXT = "";
                        a.SUBJ_TXT = string.Format("Private Customer - {0},{1},{2} -- Initial Install Event ID : {3}", maskedCity, maskedState_Province, maskedCountry_Region, tadpoleEvent.EventID);
                        a.APPT_LOC_TXT = maskedNumber;
                        a.APPT_TYPE_ID = FedlineCalendarType;
                        a.CREAT_BY_USER_ID = 5000;  //COWS Fedline PDL
                        a.CREAT_DT = DateTime.Now;
                        a.ASN_TO_USER_ID_LIST_TXT = "<ResourceIds>    <ResourceId Type=\"System.Int32\" Value=\"5000\" />  </ResourceIds>";
                        CDB.APPT.InsertOnSubmit(a);
                        CDB.SubmitChanges();
                    }
                    else if (tadpoleEvent.OrderType == ActionType.NCC || tadpoleEvent.OrderType == ActionType.NIC)
                    {
                        List<APPT> a = (from apt in CDB.APPT
                                        where apt.EVENT_ID == tadpoleEvent.EventID && apt.APPT_TYPE_ID == FedlineCalendarType
                                        select apt).ToList();

                        if (a.Count() > 0)
                        {
                            CDB.APPT.DeleteAllOnSubmit(a);
                            CDB.SubmitChanges();
                        }
                    }
                    else if (tadpoleEvent.OrderType == ActionType.NDM)   //Only do this for NDM since the others do not have a date change
                    {
                        APPT a = (from apt in CDB.APPT
                                  where apt.EVENT_ID == tadpoleEvent.EventID && apt.APPT_TYPE_ID == FedlineCalendarType
                                  select apt).FirstOrDefault();
                        //If there is an appointment and we can set the new times...
                        if (a != null && (tadpoleEvent.StartTime != null && tadpoleEvent.EndTime != null))
                        {
                            a.STRT_TMST = (DateTime)tadpoleEvent.StartTime;
                            a.END_TMST = tadpoleEvent.EndTime;
                            a.MODFD_BY_USER_ID = 1;
                            a.MODFD_DT = DateTime.Now;
                            CDB.SubmitChanges();
                        }
                        //There is an appointment, but we don't have set times on new event
                        else if (a != null && (tadpoleEvent.StartTime == null || tadpoleEvent.EndTime == null))
                        {
                            CDB.APPT.DeleteOnSubmit(a);
                            CDB.SubmitChanges();
                        }
                        //Create a new event if start and end times are set.
                        else if (tadpoleEvent.StartTime != null && tadpoleEvent.EndTime != null)
                        {
                            string maskedCity = "Overland Park";
                            string maskedState_Province = "KS";
                            string maskedCountry_Region = "US";
                            string maskedNumber = "Installation Contact Phone: 9999999999";

                            APPT ap = new APPT();
                            ap.EVENT_ID = tadpoleEvent.EventID;
                            ap.STRT_TMST = (DateTime)tadpoleEvent.StartTime;
                            ap.END_TMST = tadpoleEvent.EndTime;
                            ap.RCURNC_CD = false;
                            ap.RCURNC_DES_TXT = "";
                            ap.SUBJ_TXT = string.Format("Private Customer - {0},{1},{2} -- Initial Install Event ID : {3}", maskedCity, maskedState_Province, maskedCountry_Region, tadpoleEvent.EventID);
                            ap.APPT_LOC_TXT = maskedNumber;
                            ap.APPT_TYPE_ID = FedlineCalendarType;
                            ap.CREAT_BY_USER_ID = 1;
                            ap.CREAT_DT = DateTime.Now;
                            ap.ASN_TO_USER_ID_LIST_TXT = "<ResourceIds>    <ResourceId Type=\"System.Int32\" Value=\"5000\" />  </ResourceIds>";
                            CDB.APPT.InsertOnSubmit(ap);
                            CDB.SubmitChanges();
                        }
                    }

                    #endregion Calendar

                    #region Log XML

                    if (toLog)
                    {
                        FEDLINE_EVENT_TADPOLE_XML x = new FEDLINE_EVENT_TADPOLE_XML();
                        x.MSG_XML_TXT = CDB.GetEncryptValue(xmlString).FirstOrDefault().Item;
                        x.FRB_REQ_ID = tadpoleEvent.FRBRequestID;
                        x.ERROR_MSG_TXT = string.Empty;
                        x.IS_ERROR_CD = false;
                        x.CREAT_DT = DateTime.Now;
                        x.IS_REJECT_CD = false;
                        CDB.FEDLINE_EVENT_TADPOLE_XML.InsertOnSubmit(x);
                        CDB.SubmitChanges();
                    }

                    #endregion Log XML

                    scope.Complete();   //Commit Transaction
                }
                return true;
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error inserting Tadpole Request ID: {0} Sequence: {1}", tadpoleEvent.FRBRequestID, tadpoleEvent.SequenceNumber) + Environment.NewLine + ex.ToString(), string.Empty);

                try
                {
                    if (toLog)
                    {
                        using (COWS CDB = new COWS(ConnStr))
                        {
                            FEDLINE_EVENT_TADPOLE_XML x = new FEDLINE_EVENT_TADPOLE_XML();
                            x.MSG_XML_TXT = CDB.GetEncryptValue(xmlString).FirstOrDefault().Item;
                            string exception = ex.ToString();
                            if (exception.Length > 8000)
                                x.ERROR_MSG_TXT = exception.Substring(0, 8000);
                            else
                                x.ERROR_MSG_TXT = exception;
                            x.FRB_REQ_ID = tadpoleEvent.FRBRequestID;
                            x.IS_ERROR_CD = true;
                            x.IS_REJECT_CD = false;
                            x.CREAT_DT = DateTime.Now;
                            CDB.FEDLINE_EVENT_TADPOLE_XML.InsertOnSubmit(x);
                            CDB.SubmitChanges();
                        }
                    }
                }
                catch (Exception ex2)
                {
                    throw new XMLNotDatabased(ex2);
                }
                return false;
            }
        }

        public void UpdateReqHist(int _eventID, DateTime? _oldDate, DateTime? _newDate)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    var failCD = (from etr in CDB.FEDLINE_EVENT_USER_DATA
                                  where etr.EVENT_ID == _eventID
                                  select etr.FAIL_CD_ID).FirstOrDefault();

                    int? HistID = null;

                    if (failCD != null)
                    {
                        int histID = (from eh in CDB.EVENT_HIST
                                      where eh.EVENT_ID == _eventID && eh.ACTN_ID == 45
                                      orderby eh.CREAT_DT descending
                                      select eh.EVENT_HIST_ID).FirstOrDefault();

                        HistID = histID;
                    }

                    FEDLINE_REQ_ACT_HIST rh = new FEDLINE_REQ_ACT_HIST();
                    rh.EVENT_ID = _eventID;
                    rh.OLD_DT = _oldDate;
                    rh.NEW_DT = _newDate;
                    rh.FAIL_CD_ID = Convert.ToInt16(failCD);
                    rh.EVENT_HIST_ID = HistID;
                    rh.CREAT_DT = DateTime.Now;
                    CDB.FEDLINE_REQ_ACT_HIST.InsertOnSubmit(rh);
                    CDB.SubmitChanges();

                    int? _inProgress = (from eh in CDB.EVENT_HIST
                                        where eh.EVENT_ID == _eventID && eh.ACTN_ID == 19
                                        orderby eh.CREAT_DT descending
                                        select eh.EVENT_HIST_ID).FirstOrDefault();

                    FEDLINE_EVENT_USER_DATA ud = (from feud in CDB.FEDLINE_EVENT_USER_DATA
                                                  where feud.EVENT_ID == _eventID
                                                  select feud).SingleOrDefault();

                    if (ud != null)
                    {
                        ud.FAIL_CD_ID = null;

                        if (_inProgress != 0)
                        {
                            ud.SLA_CD = "N";
                        }
                    }

                    CDB.SubmitChanges();
                }
            }
            catch (Exception exNew)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", "Error inserting Tadpole Reject XML" + Environment.NewLine + exNew.ToString(), string.Empty);
                throw new XMLNotDatabased(exNew);
            }
        }

        public List<TimeSlot> GetFedTimeSlots()
        {
            try
            {
                List<TimeSlot> List = new List<TimeSlot>();

                using (COWS CDB = new COWS(ConnStr))
                {
                    CDB.ObjectTrackingEnabled = false;

                    var times = (from t in CDB.LK_EVENT_TYPE_TME_SLOT
                                 where t.EVENT_TYPE_ID == FedlineEventID
                                 orderby t.TME_SLOT_STRT_TME
                                 select new { t.EVENT_TYPE_ID, t.TME_SLOT_ID, t.TME_SLOT_STRT_TME, t.TME_SLOT_END_TME, t.AVLBLTY_CAP_PCT_QTY }).Distinct();

                    foreach (var time in times)
                    {
                        TimeSlot ts = new TimeSlot();
                        ts.TimeSlotID = time.TME_SLOT_ID;
                        ts.Start = time.TME_SLOT_STRT_TME;
                        ts.End = time.TME_SLOT_END_TME;

                        List.Add(ts);
                    }

                    return List;
                }
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error getting Timeslots from DB:") + Environment.NewLine + ex.ToString(), string.Empty);
                return new List<TimeSlot>();
            }
        }

        public bool IncrementTimeSlot(DateTime day, byte TimeSlotID, short updateBy)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    var obj = (from etr in CDB.EVENT_TYPE_RSRC_AVLBLTY
                               where etr.EVENT_TYPE_ID == FedlineEventID && etr.DAY_DT == day && etr.TME_SLOT_ID == TimeSlotID
                               select etr);
                    if (obj.Count() > 0)
                    {
                        EVENT_TYPE_RSRC_AVLBLTY etr = obj.Single();
                        etr.CURR_AVAL_TME_SLOT_CNT += updateBy;
                        CDB.SubmitChanges();
                    }
                    else
                    {
                        EVENT_TYPE_RSRC_AVLBLTY etr = new EVENT_TYPE_RSRC_AVLBLTY();
                        int currentMaxID = (from e in CDB.EVENT_TYPE_RSRC_AVLBLTY select e.EVENT_TYPE_RSRC_AVLBLTY_ID).Max();
                        if (currentMaxID < 5000)
                            currentMaxID = 5000;
                        etr.EVENT_TYPE_RSRC_AVLBLTY_ID = currentMaxID + 1;
                        etr.AVAL_TME_SLOT_CNT = 0;
                        etr.CREAT_DT = DateTime.Now;
                        etr.CURR_AVAL_TME_SLOT_CNT = updateBy;
                        etr.DAY_DT = day;
                        etr.EVENT_TYPE_ID = FedlineEventID;
                        etr.MAX_AVAL_TME_SLOT_CAP_CNT = 0;
                        etr.TME_SLOT_ID = TimeSlotID;

                        CDB.EVENT_TYPE_RSRC_AVLBLTY.InsertOnSubmit(etr);
                        CDB.SubmitChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error updating the Time Slot Count, Day: {0}, TimeSlotID: {1}, Change Value: {2} ",
                    day, TimeSlotID, updateBy) + Environment.NewLine + ex.ToString(), string.Empty);
                return false;
            }
        }

        /// <summary>
        /// If time slot is valid, remove from Invalids list.  Else insert it.
        /// </summary>
        /// <param name="EventID"></param>
        /// <param name="toInsert"></param>
        /// <returns></returns>
        public bool UpdateInvalidTimeSlotTable(int EventID, bool isValid)
        {
            try
            {
                if (isValid)
                {
                    using (COWS CDB = new COWS(ConnStr))
                    {
                        CDB.EVENT_NVLD_TME_SLOT.DeleteAllOnSubmit(CDB.EVENT_NVLD_TME_SLOT.Where(a => a.EVENT_ID == EventID));
                        CDB.SubmitChanges();
                    }
                }
                else
                {
                    using (COWS CDB = new COWS(ConnStr))
                    {
                        if (CDB.EVENT_NVLD_TME_SLOT.Where(a => a.EVENT_ID == EventID).Count() == 0)
                        {
                            EVENT_NVLD_TME_SLOT ev = new EVENT_NVLD_TME_SLOT()
                            {
                                EVENT_ID = EventID,
                                CREAT_DT = DateTime.Now
                            };
                            CDB.EVENT_NVLD_TME_SLOT.InsertOnSubmit(ev);
                            CDB.SubmitChanges();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error updating the Invalid Time Slot events, EventID: {0}, Is Valid? {1}",
                    EventID, isValid) + Environment.NewLine + ex.ToString(), string.Empty);
                return false;
            }
        }

        public bool IsSprintHoliday(DateTime day)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    var obj = (from x in CDB.LK_SPRINT_HLDY
                               where x.SPRINT_HLDY_DT == day
                               select x.SPRINT_HLDY_ID);

                    if (obj.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error Checking if date is Sprint Holiday, Day: {0}",
                    day) + Environment.NewLine + ex.ToString(), string.Empty);
                return false;
            }
        }

        public List<ResponseEntity> GetResponses()
        {
            try
            {
                //Use static compiled query.  Check if already compiled, if not compile here.
                if (getMessages == null)
                {
                    getMessages = CompiledQuery.Compile((COWS context, int status)
                        => (from m in context.FEDLINE_EVENT_MSG
                            join td in context.FEDLINE_EVENT_TADPOLE_DATA on m.FEDLINE_EVENT_ID equals td.FEDLINE_EVENT_ID
                            join ud in context.FEDLINE_EVENT_USER_DATA on td.EVENT_ID equals ud.EVENT_ID
                            where m.STUS_ID == status
                            select new ResponseEntity
                            {
                                MessageID = m.FEDLINE_EVENT_MSG_ID,
                                FRBRequestID = td.FRB_REQ_ID,
                                SequenceNumber = td.SEQ_NBR,
                                FRBRequestType = td.FRB_ORDR_TYPE_CD,
                                ResponseType = m.FEDLINE_EVENT_MSG_STUS_NME,
                                ResponseDescription = m.RSPN_DES,
                                CreateDate = m.CREAT_DT,
                                ShipTrackingNumber = ud.TRK_NBR,
                                ShipCarrier = ud.SHIPPING_CXR_NME,
                                ShippedDate = m.CREAT_DT,
                                ShipDeliveryDate = (ud.ARRIVAL_DT ?? new DateTime(1900, 1, 1))
                            }));
                }

                using (COWS CDB = new COWS(ConnStr))
                {
                    CDB.ObjectTrackingEnabled = false;
                    return getMessages(CDB, 10).ToList();
                }
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Sender", FL.Event.BeginDBRead, FL.MsgType.text, -1, "", string.Format("Error getting messages from DB:") + Environment.NewLine + ex.ToString(), string.Empty);
                return new List<ResponseEntity>();
            }
        }

        public bool UpdateMessageStatus(int MessageID, byte StatusID)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    FEDLINE_EVENT_MSG fem = new FEDLINE_EVENT_MSG() { FEDLINE_EVENT_MSG_ID = MessageID, STUS_ID = 0, SENT_DT = null };
                    CDB.FEDLINE_EVENT_MSG.Attach(fem);
                    fem.STUS_ID = StatusID;
                    if (StatusID == 11)
                        fem.SENT_DT = DateTime.Now;
                    CDB.SubmitChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Sender", FL.Event.BeginDBRead, FL.MsgType.text, -1, "", string.Format("Error updating message status:") + Environment.NewLine + ex.ToString(), string.Empty);
                return false;
            }
        }

        public bool UpdateMessageStatusToErrored(int FRBID, int version, string messageType)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    FEDLINE_EVENT_MSG fem = (from td in CDB.FEDLINE_EVENT_TADPOLE_DATA
                                             join m in CDB.FEDLINE_EVENT_MSG on td.FEDLINE_EVENT_ID equals m.FEDLINE_EVENT_ID
                                             where td.FRB_REQ_ID == FRBID
                                                && td.SEQ_NBR == version
                                                && m.FEDLINE_EVENT_MSG_STUS_NME.Contains(messageType)
                                                && m.STUS_ID == 11      //Is in Sent status
                                             orderby m.FEDLINE_EVENT_MSG_ID descending  //If the same message has been queued to send multiple times there is no way to tell which message it is.  Update the most recent one.
                                             select m).FirstOrDefault();

                    if (fem != null)
                    {
                        fem.STUS_ID = 12;
                        CDB.SubmitChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Sender", FL.Event.BeginDBRead, FL.MsgType.text, -1, "", string.Format("Error updating message status to Errored:") + Environment.NewLine + ex.ToString(), string.Empty);
                return false;
            }
        }

        public int GetPreviousSequence(int TadpoleRequestID)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    int seq = (from td in CDB.FEDLINE_EVENT_TADPOLE_DATA
                               where td.FRB_REQ_ID == TadpoleRequestID && td.REC_STUS_ID == (byte)ERecStatus.Active
                                && !HeadendTypes.Contains(td.ORDR_TYPE_CD)
                               orderby td.SEQ_NBR descending
                               select td.SEQ_NBR).FirstOrDefault();
                    return seq;
                }
            }
            catch (Exception ex)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", string.Format("Error getting previous sequence number, Tadpole Request ID: {0}", TadpoleRequestID) + Environment.NewLine + ex.ToString(), string.Empty);
                return 0;
            }
        }

        public void LogRejectXML(string xmlString, string exception, int? FRBID)
        {
            try
            {
                using (COWS CDB = new COWS(ConnStr))
                {
                    FEDLINE_EVENT_TADPOLE_XML x = new FEDLINE_EVENT_TADPOLE_XML();
                    x.MSG_XML_TXT = CDB.GetEncryptValue(xmlString).FirstOrDefault().Item;
                    if (exception.Length > 8000)
                        x.ERROR_MSG_TXT = exception.Substring(0, 8000);
                    else
                        x.ERROR_MSG_TXT = exception;
                    x.FRB_REQ_ID = FRBID;
                    x.IS_ERROR_CD = false;
                    x.IS_REJECT_CD = true;
                    x.CREAT_DT = DateTime.Now;
                    CDB.FEDLINE_EVENT_TADPOLE_XML.InsertOnSubmit(x);
                    CDB.SubmitChanges();
                }
            }
            catch (Exception exNew)
            {
                FL.WriteLogFile("Receiver", FL.Event.BeginDBWrite, FL.MsgType.text, -1, "", "Error inserting Tadpole Reject XML" + Environment.NewLine + exNew.ToString(), string.Empty);
                throw new XMLNotDatabased(exNew);
            }
        }

        public DataSet getEncryptionTKey(string _AppID)
        {
            setCommand("dbo.getEncryptionKey");
            addStringParam("@APPL_ID", _AppID);
            return getDataSet();
        }
    }
}