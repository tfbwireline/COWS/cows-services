﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:include href="style.xslt" />
	<xsl:template match="/">
		<html>
			<body>
				<table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event ID : </td>
						<td width="70%">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="FedlineEmailEntity/EventLink" />
								</xsl:attribute>
								<xsl:value-of select="FedlineEmailEntity/EventID" />
							</a>
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Assigned Activator : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/AssignedActivator" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Scheduled Event Date &amp; Time :  </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/ScheduledEventDateTime" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Add to Calendar :  </td>
						<td width="70%">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="FedlineEmailEntity/CalendarEntryURL" />
								</xsl:attribute>
								Click Here
							</a>
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event Title :  </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/EventTitle" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">MDS Event Type :  </td>
						<td width="70%">
							Fedline
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">MDS Activity Type : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/MDSActivityType" />
						</td>
					</tr>
					<tr></tr>
					<tr></tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Install Site POC : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/InstallSitePOC" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/InstallSitePOCPhone" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Install Site Backup POC Phone : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/InstallSiteBackupPOCPhone" />
						</td>
					</tr>
					<tr></tr>
					<tr></tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600" colspan="2">===== Site Addresss Details =====</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Address : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/Address1" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/Address2" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">City : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/City" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">State/Province : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/State_Province" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Country/Region : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/Country_Region" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Zip : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/Zip" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">US/International : </td>
						<td width="70%">
							<xsl:value-of select="FedlineEmailEntity/US_International" />
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>