﻿using DataManager;
using System;

namespace FSAHelper
{
    public class ORDR_ADR_DB : MSSqlBase
    {
        #region "Fields and Properties"

        private int _ORDR_ADR_ID = 0;

        public int ORDR_ADR_ID
        {
            get { return _ORDR_ADR_ID; }
            set { _ORDR_ADR_ID = value; }
        }

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private Byte _ADR_TYPE_ID;

        public Byte ADR_TYPE_ID
        {
            get { return _ADR_TYPE_ID; }
            set { _ADR_TYPE_ID = value; }
        }

        private string _STREET_ADR_1;

        public string STREET_ADR_1
        {
            get { return _STREET_ADR_1; }
            set { _STREET_ADR_1 = value; }
        }

        private string _STREET_ADR_2;

        public string STREET_ADR_2
        {
            get { return _STREET_ADR_2; }
            set { _STREET_ADR_2 = value; }
        }

        private string _CTY_NME;

        public string CTY_NME
        {
            get { return _CTY_NME; }
            set { _CTY_NME = value; }
        }

        private string _PRVN_NME;

        public string PRVN_NME
        {
            get { return _PRVN_NME; }
            set { _PRVN_NME = value; }
        }

        private string _STT_CD;

        public string STT_CD
        {
            get { return _STT_CD; }
            set { _STT_CD = value; }
        }

        private string _ZIP_PSTL_CD;

        public string ZIP_PSTL_CD
        {
            get { return _ZIP_PSTL_CD; }
            set { _ZIP_PSTL_CD = value; }
        }

        private string _BLDG_NME;

        public string BLDG_NME
        {
            get { return _BLDG_NME; }
            set { _BLDG_NME = value; }
        }

        private string _FLR_ID;

        public string FLR_ID
        {
            get { return _FLR_ID; }
            set { _FLR_ID = value; }
        }

        private string _RM_NBR;

        public string RM_NBR
        {
            get { return _RM_NBR; }
            set { _RM_NBR = value; }
        }

        private string _CTRY_CD;

        public string CTRY_CD
        {
            get { return _CTRY_CD; }
            set { _CTRY_CD = value; }
        }

        private string _CIS_LVL_TYPE;

        public string CIS_LVL_TYPE
        {
            get { return _CIS_LVL_TYPE; }
            set { _CIS_LVL_TYPE = value; }
        }

        private string _FSA_MDUL_ID;

        public string FSA_MDUL_ID
        {
            get { return _FSA_MDUL_ID; }
            set { _FSA_MDUL_ID = value; }
        }

        private bool _HIER_LVL_CD = false;

        public bool HIER_LVL_CD
        {
            get { return _HIER_LVL_CD; }
            set { _HIER_LVL_CD = value; }
        }

        #endregion "Fields and Properties"

        public ORDR_ADR_DB(int _OrderID, string _ModuleID)
        {
            ORDR_ID = _OrderID;
            FSA_MDUL_ID = _ModuleID;
        }

        public int InsertOrderAddressInfo()
        {
            int _RetVal = -1;
            if (_ADR_TYPE_ID != 0)
            {
                setCommand("dbo.insertOrderAddressInfo");

                addIntParam("@ORDR_ADR_ID", _ORDR_ADR_ID);
                addIntParam("@ORDR_ID", _ORDR_ID);
                addTinyIntParam("@ADR_TYPE_ID", _ADR_TYPE_ID);
                addStringParam("@STREET_ADR_1", _STREET_ADR_1);
                addStringParam("@STREET_ADR_2", _STREET_ADR_2);
                addStringParam("@CTY_NME", _CTY_NME);
                addStringParam("@PRVN_NME", _PRVN_NME);
                addStringParam("@STT_CD", _STT_CD);
                addStringParam("@ZIP_PSTL_CD", _ZIP_PSTL_CD);
                addStringParam("@BLDG_NME", _BLDG_NME);
                addStringParam("@FLR_ID", _FLR_ID);
                addStringParam("@RM_NBR", _RM_NBR);
                addIntParam("@CREAT_BY_USER_ID", 1);
                addStringParam("@CTRY_CD", _CTRY_CD);
                addStringParam("@CIS_LVL_TYPE", _CIS_LVL_TYPE);
                addStringParam("@FSA_MDUL_ID", _FSA_MDUL_ID);
                addBooleanParam("@HIER_LVL_CD", _HIER_LVL_CD);

                _RetVal = execNoQuery();
            }
            return _RetVal;
        }
    }
}