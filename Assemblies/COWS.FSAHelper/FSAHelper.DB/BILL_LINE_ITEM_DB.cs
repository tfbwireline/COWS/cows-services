﻿using DataManager;
using System;

namespace FSAHelper
{
    public class BILL_LINE_ITEM_DB : MSSqlBase
    {
        #region "Fields and Properties"

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private Int16 _BILL_ITEM_TYPE_ID = 0;

        public Int16 BILL_ITEM_TYPE_ID
        {
            get { return _BILL_ITEM_TYPE_ID; }
            set { _BILL_ITEM_TYPE_ID = value; }
        }

        private string _BIC_CD;

        public string BIC_CD
        {
            get { return _BIC_CD; }
            set { _BIC_CD = value; }
        }

        private string _LINE_ITEM_DES;

        public string LINE_ITEM_DES
        {
            get { return _LINE_ITEM_DES; }
            set { _LINE_ITEM_DES = value; }
        }

        private Int16 _LINE_ITEM_QTY;

        public Int16 LINE_ITEM_QTY
        {
            get { return _LINE_ITEM_QTY; }
            set { _LINE_ITEM_QTY = value; }
        }

        private string _ITEM_CD;

        public string ITEM_CD
        {
            get { return _ITEM_CD; }
            set { _ITEM_CD = value; }
        }

        private string _ITEM_DES;

        public string ITEM_DES
        {
            get { return _ITEM_DES; }
            set { _ITEM_DES = value; }
        }

        private string _MRC_CHG_AMT;

        public string MRC_CHG_AMT
        {
            get { return _MRC_CHG_AMT; }
            set { _MRC_CHG_AMT = value; }
        }

        private string _NRC_CHG_AMT;

        public string NRC_CHG_AMT
        {
            get { return _NRC_CHG_AMT; }
            set { _NRC_CHG_AMT = value; }
        }

        private string _CHG_CD;

        public string CHG_CD
        {
            get { return _CHG_CD; }
            set { _CHG_CD = value; }
        }

        private string _ACTN_CD;

        public string ACTN_CD
        {
            get { return _ACTN_CD; }
            set { _ACTN_CD = value; }
        }

        private int _FSA_CPE_LINE_ITEM_ID = 0;

        public int FSA_CPE_LINE_ITEM_ID
        {
            get { return _FSA_CPE_LINE_ITEM_ID; }
            set { _FSA_CPE_LINE_ITEM_ID = value; }
        }

        #endregion "Fields and Properties"

        public enum BILL_LINE_ITEM_TYPE
        {
            OrderBillingLineItem = 1,
            MDSLineItem = 2,
            CPELineItem = 3,
            ValueAddedService = 4,
            TransportInstall = 5,
            PortInstall = 6,
            InstallBillingLineItem = 7,
            DisconnectBillingLineItem = 8,
            ChangeBillingLineItem = 9,
        }

        public BILL_LINE_ITEM_DB(int _OrderID, Int16 _BillTypeID)
        {
            ORDR_ID = _OrderID;
            BILL_ITEM_TYPE_ID = _BillTypeID;
        }

        public int InsertBillingLineItem()
        {
            setCommand("dbo.insertBillingLineItem");
            addIntParam("@FSA_ORDR_BILL_LINE_ITEM_ID", 0, true);
            addIntParam("@ORDR_ID", _ORDR_ID);
            addSmallIntParam("@BILL_ITEM_TYPE_ID", BILL_ITEM_TYPE_ID);
            addStringParam("@BIC_CD", BIC_CD);
            addStringParam("@LINE_ITEM_DES", LINE_ITEM_DES);
            addSmallIntParam("@LINE_ITEM_QTY", LINE_ITEM_QTY);
            addStringParam("@ITEM_CD", ITEM_CD);
            addStringParam("@ITEM_DES", ITEM_DES);
            addStringParam("@MRC_CHG_AMT", MRC_CHG_AMT);
            addStringParam("@NRC_CHG_AMT", NRC_CHG_AMT);
            addStringParam("@ACTN_CD", ACTN_CD);
            addStringParam("@CHG_CD", CHG_CD);
            addIntParam("@FSA_CPE_LINE_ITEM_ID", _FSA_CPE_LINE_ITEM_ID);
            return execNoQuery();
        }
    }
}