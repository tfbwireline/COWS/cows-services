﻿using System;
using System.Collections;
using System.Configuration;
using System.Threading;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;

namespace COWS.SSTATReceiverService
{
    public class SSTATReceiver : SSTATReceiverBase
    {
        private int _SleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);
        private MQC _mqc;
        private SSTATReceiverIO _SSTATReceiverIO = new SSTATReceiverIO();

        public void GetSSTATMessages()
        {
            try
            {
                _mqc = new MQC();
            }
            catch
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error connecting to the MQ Queue Manager.", "Please check MQ Connection string in App Config.");
                return;
            }

            CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATInService MQ connection has been instantiated.", "");
            CanBeStoppedNow = true;

            while (!base.bStopThread)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.BeginMQRead, CL.MsgType.text, -1, string.Empty, "Beginning MQ Read", string.Empty);

                    if (_mqc.MQBrowseFirst(MQC.QObjReturned.QueueOfMQMessageItems))
                    {
                        CanBeStoppedNow = false;
                        Queue MsgQue = _mqc.outQue;
                        int MsgCnt = MsgQue.Count;
                        CL.WriteLogFile("Main", CL.Event.EndMQRead, CL.MsgType.text, -1, string.Empty, "MQ Read Complete", "No of records retrieved from Queue: " + MsgCnt.ToString());

                        for (int iCnt = 0; iCnt < MsgCnt; iCnt++)
                        {
                            string sMsg = String.Empty;
                            MQC.MQMessageItem MsgItem = new MQC.MQMessageItem();
                            MsgItem = (MQC.MQMessageItem)MsgQue.Dequeue();
                            sMsg = MsgItem.message;

                            this.InsertSSTATMessage(sMsg);
                            _mqc.MQGetAfterBrowseFirst(MQManager.MQClient.QObjReturned.QueueOfMQMessageItems);
                            MsgQue.Dequeue();

                            Thread.SpinWait(2);
                        }

                        // In case Acks need to be resent, ACK_CD = 0 in SSTAT_RSPN table will do it.
                        ResendAckFunctionality();

                        //Separated the MQ interaction with the processing of the message.  The processing
                        // is done in the DB with SP dbo.processSSTATMsgs and done in bulk.
                        ProcessAllMsgs();
                    }
                }
                catch (Exception e)
                {
                    CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATReceiver Service Error: " + e.ToString(), "Stop reading from MQ!");
                }

                Thread.Sleep(_SleepTime);
                CanBeStoppedNow = true;
            }
        }

        private void InsertSSTATMessage(string sMessage)
        {
            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Message received: " + sMessage, string.Empty);

            _SSTATReceiverIO.sMessage = sMessage;
            if (!_SSTATReceiverIO.InsertSSTATMessage())
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "XML Read Error", "Error loading the XML from Queue. ");
            }
        }

        private void ResendAckFunctionality()
        {
            _SSTATReceiverIO.ResendAcks();
        }

        private void ProcessAllMsgs()
        {
            _SSTATReceiverIO.ProcessAllMsgs();
        }
    }
}