﻿namespace FSAHelper
{
    public partial class OrderResponse
    {
        private string timeStamp;
        private string orderID;
        private ResponseType responseTypeCode;
        private string completionDate;
        private string negativeAcknowledgementText;
        private string rejectText;

        public string TimeStamp
        {
            get { return this.timeStamp; }
            set { this.timeStamp = value; }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string OrderID
        {
            get { return this.orderID; }
            set { this.orderID = value; }
        }

        public ResponseType ResponseTypeCode
        {
            get { return this.responseTypeCode; }
            set { this.responseTypeCode = value; }
        }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string CompletionDate
        {
            get { return this.completionDate; }
            set { this.completionDate = value; }
        }

        public string NegativeAcknowledgmentText
        {
            get { return this.negativeAcknowledgementText; }
            set { this.negativeAcknowledgementText = value; }
        }

        public string RejectText
        {
            get { return this.rejectText; }
            set { this.rejectText = value; }
        }
    }

    public enum ResponseType
    {
        ACKNOWLEDGMENT = 1,
        NEGATIVEACKNOWLEDGMENT = 2,
        ACTIVATION = 3,
        REJECT = 4,
        DISCONNECTCOMPLETE = 5,
        CHANGECOMPLETE = 6,
    }
}