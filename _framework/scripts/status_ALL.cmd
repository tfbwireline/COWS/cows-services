@echo off
REM *******************************************************************************
REM Copyright (c) 2007 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM UDM framework script to report the status of all application components.  
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
CALL "%APP_HOME%"\Scripts\_envconfig.cmd
SET ANT_HOME=%APP_HOME%\Framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\Scripts\_admin.xml


REM
REM call ant target
REM
"%ANT_PROG%" -f "%ANT_SCRIPT%" _status.all
