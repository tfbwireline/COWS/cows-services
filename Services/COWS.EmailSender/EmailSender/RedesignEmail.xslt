﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:myExtension="urn:myExtension"
                exclude-result-prefixes="msxsl myExtension">
<msxsl:script implements-prefix="myExtension" language="C#">
    <![CDATA[
      public string FormatDateTime(string xsdDateTime, string format)
      {
          DateTime date = DateTime.Parse(xsdDateTime);
          return date.ToString(format);
      }

    ]]>
  </msxsl:script>
  <xsl:template match="/">
    <html>
      <body width="99%">
		<div>Please click the link <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(/RedesignEmail/RedesignInfo/RedesignURL, /RedesignEmail/RedesignInfo/RedesignID)" />
                </xsl:attribute>
                here
              </a>
              to open Redesign request</div>
			  <br></br>
        <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.='229']">
          <div>
            This message is to inform you that the following redesign will expire in 30 days. Please contact your NTE prior to expiration to request an extension.
            <br></br>
            <br></br>
            <span style="font-weight:900; color:red" >NOTE: </span>  <span style="font-weight:900"> If  this work has already been completed on a COWS event, please forward the event # to the MNS NIM so they can close out the redesign.
            If the customer decides not to proceed, please notify the MNS NIM and NTE.</span>
            <br></br>
            <br></br>
            <span style="font-weight:900"> Action must be taken to extend or schedule this activity. </span>
          </div>
		  <br></br>
        </xsl:if>
        <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.='2291']">
          <div>
            This message is to inform you that the following redesign will expire in 10 days. Please contact your NTE prior to expiration to request an extension.
            <br></br>
            <br></br>
            <span style="font-weight:900; color:red">NOTE: </span>  <span style="font-weight:900"> If this work has already been completed on a COWS event, please forward the event # to the MNS NIM so they can close out the redesign.
            If the customer decides not to proceed, please notify the MNS NIM and NTE.</span>
            <br></br>
            <br></br>
            <span style="font-weight:900">This is your </span> <span style="font-weight:900; color:red">FINAL</span> <span style="font-weight:900"> notice. Action must be taken to extend or schedule this activity.</span>
          </div>
		  <br></br>
        </xsl:if>
        <xsl:if test="(/RedesignEmail/RedesignInfo/RedesignStatusID[.!='229']) and (/RedesignEmail/RedesignInfo/RedesignStatusID[.!='2291'])">
          <div>This message is to inform you that the following redesign request has been placed in  <span style="font-weight: 600"><xsl:value-of select="/RedesignEmail/RedesignInfo/RedesignStatus" /></span>  status:</div>
		  <br></br>
        </xsl:if>
        <table width="99%" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Request ID: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/RedesignNumber" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">H1: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/H1" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Customer: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/CustomerName" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Locations Affected: </td>
            <td width="70%">
              <xsl:value-of disable-output-escaping="yes" select="/RedesignEmail/RedesignInfo/Devices" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Redesign Type: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/RedesignType" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">SD-WAN: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/SDWAN" />
            </td>
          </tr>
		  <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Cisco Smart License Activation Required ?: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/CiscoSmart" />
            </td>
          </tr>
		  <xsl:if test="/RedesignEmail/RedesignInfo/CiscoSmart[.='Yes']">
		  <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Smart Account Domain: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/SmartAccount" />
            </td>
          </tr>
		  <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Virtual Account: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/VirtualAccount" />
            </td>
          </tr>
		  </xsl:if>
          <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.='225']">
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Caveats: </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/Caveats" />
            </td>
          </tr>
          </xsl:if>
          <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.='225']">
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">The cost for this redesign is: </td>
            <td width="70%">$
              <xsl:value-of select="/RedesignEmail/RedesignInfo/Cost" />
            </td>
          </tr>
		  <tr />
          </xsl:if>
		  <xsl:if test="/RedesignEmail/RedesignInfo/REDSGN_CAT_ID[.='3']">
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">MSS Implementation Estimation(in minutes): </td>
            <td width="70%">
              <xsl:value-of select="/RedesignEmail/RedesignInfo/MSS_IMPL_EST_AMT" />
            </td>
          </tr>
		  <tr />
          </xsl:if>
        </table>
        <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.='225']">
          <div>
            <b>
              A COWS event should not be submitted for scheduling unless the customer has explicitly agreed to this charge. If any NCR or Redesign charge needs to be disputed, the Account Team can contact <a>REDESIGN.MNS.LIST@t-mobile.com</a>.
            </b>
          </div>
        </xsl:if>

        <br />
        <br />
        <xsl:if test="/RedesignEmail/RedesignInfo/RedesignStatusID[.!='220']">
          <div>
            <b>
              IMPORTANT: This redesign will expire on <span style="color: red"><xsl:value-of select="/RedesignEmail/RedesignInfo/ExpirationDate" /></span>.  If the redesign work is not completed by this date, the approval number becomes invalid and another redesign must be submitted.
            </b>
          </div>
        </xsl:if>
        <br></br>
        <div style="font-weight: 600">Redesign Documents</div>
        <table>
          <tr bgcolor="#D8D8D8">
            <th>File Name</th>
            <th>File Size</th>
          </tr>
          <xsl:for-each select="/RedesignEmail/RedesignDocsInfo">
            <tr>
              <td>
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="DocURL" />
                  </xsl:attribute>
                  <xsl:value-of select="DocName" />
                </a>
              </td>
              <td>
                <xsl:value-of select="DocSize" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <xsl:if test="/RedesignEmail/AdditionalDocuments/Document[.!='']">
          <br></br>
          <div style="font-weight: 600">Additional Documents</div>
          <table>
            <xsl:for-each select="/RedesignEmail/AdditionalDocuments">
              <tr>
                <td>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:value-of select="Document" />
                    </xsl:attribute>
                    <xsl:value-of select="Document" />
                  </a>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </xsl:if>
		<br></br>
        <xsl:if test="/RedesignEmail/RedesignNotesInfo/CreatedBy[.!='']">
        <div style="font-weight: 600">Case History</div>
          <xsl:for-each select="RedesignEmail">
            <table border="1">
              <tr bgcolor="#D8D8D8">
                <th>Created By</th>
                <th>Created Date</th>
                <th>Note Type</th>
                <th>Notes Desc</th>
               </tr>
                <xsl:for-each select="RedesignNotesInfo">
							    <tr>
								    <td>
									    <xsl:value-of select="CreatedBy" />
								    </td>
                    <td>
                      <xsl:value-of select="myExtension:FormatDateTime(CreatedDate, 'g')" />
                    </td>
                    <td>
									    <xsl:value-of select="RedesignNoteType" />
								    </td>
							      <td>
									    <xsl:value-of  disable-output-escaping="yes"  select="Notes" />
								    </td>
							    </tr>
				      </xsl:for-each>
            </table>
          </xsl:for-each>
       </xsl:if>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>