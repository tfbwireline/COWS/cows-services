﻿using System.ComponentModel;

namespace AppConstants
{
    public enum EGroups
    {
        GOM = 1,
        MDS = 2,
        CSC = 3,

        [Description("Sales Support")]
        SalesSupport = 4,

        [Description("xNCI Americas")]
        xNCIAmericas = 5,

        [Description("xNCI Asia")]
        xNCIAsia = 6,

        [Description("xNCI Europe")]
        xNCIEurope = 7,

        WBO = 8,
        CAND = 10,
        SIPTnUCaaS = 11,
        DCPE = 13,

        //VCPE = 14,
        [Description("CPE TECH")]
        CPETECH = 15,

        [Description("Domestic BroadBand")]
        DBB = 16,

        System = 98,

        [Description("Read Only")]
        ReadOnly = 99
    }
}