﻿using DataManager;
using System.Data;

namespace COWS.RulesetProcessor
{
    public class RulesetProcessorDB : DBBase
    {
        public DataTable getLogDetails()
        {
            setCommand("dbo.getUserAuditLog");
            return getDataTable();
        }

        public DataTable getEDWDetails()
        {
            setCommand("dbo.getEDWAuditData");
            return getDataTable();
        }
    }
}