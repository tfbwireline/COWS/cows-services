﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>

        <p>
          <td style="text-align: left; font-size=12px; font-weight: 600"> <xsl:value-of select="/ZscalerEmail/Customer" /> in <xsl:value-of select="/ZscalerEmail/City" />, <xsl:value-of select="/ZscalerEmail/State" />
                has advised Sprint to disconnect their SS WEB services effective <xsl:value-of select="/ZscalerEmail/CCD" /> .
          </td>
        </p>

        <p>
            <td style="text-align: left; font-size=12px; font-weight: 600">  Zscaler Install Quote(s):           <xsl:value-of select="/ZscalerEmail/QuoteID" /></td>
        </p>

        <tr>
            <td colspan="1">
              <table border="0"  width="98%" style="word-wrap: break-word; font-size=12px">
                    <tr>
                      <td style="text-align: left">Subscriptions to Disconnect:</td>
                    </tr>
                    <xsl:for-each select="/ZscalerEmail/Items">
                      <tr>
                        <td width="30%" style="text-align: left; font-size=12px">
                           <xsl:value-of select="ItemDesc" />
                        </td>
                        <td width="35%" style="text-align: left; font-size=12px">
                           Qty: <xsl:value-of select="ItemQty" />
                        </td>
                      </tr>
                    </xsl:for-each>
               </table>
            </td>
          </tr>
        <br></br>
        <br></br>

          <tr>
            <td style="text-align: left; font-size=12px; font-weight: 600">Feel free to contact me, </td>
            <td>
              <xsl:value-of select="/ZscalerEmail/IPM_NME" />
            </td>
            <td> at  </td>
            <td><xsl:value-of select="/ZscalerEmail/IPM_Phone" /></td>
            <td>, with any additional questions.  </td>
          </tr>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>