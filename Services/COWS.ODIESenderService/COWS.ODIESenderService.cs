﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;

namespace COWS.ODIESenderService
{
    public partial class ODIESenderService : ServiceBase
    {
        private string _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["RequestSleepTime"]);
        private Thread _SenderThread = null;
        private MQC _mqcClient = null;
        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private ASCIIEncoding encoding = new ASCIIEncoding();

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new ODIESenderService() };
            ServiceBase.Run(ServicesToRun);
        }

        public ODIESenderService()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.ODIESenderService is starting.", "");

                _bActive = true;
                _bStopThread = false;
                _bCanBeStoppedNow = true;
                _SenderThread = new Thread(new ThreadStart(this.SendRequests));
                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send ODIE Requests)", string.Empty);
                _SenderThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.ODIESenderService. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.ODIESenderService", "COWS.ODIESenderService Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                _bStopThread = true;
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.ODIESenderService", string.Empty);

                _SenderThread.Join(TimeSpan.FromSeconds(30));
                if (!_bCanBeStoppedNow)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send ODIE  Requests)", string.Empty);
                _SenderThread.Abort();
                _SenderThread.Join();

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.ODIESenderService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.ODIESenderService. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void SendRequests()
        {
            ODIESenderIO _oIO = new ODIESenderIO();
            try
            {
                _mqcClient = new MQC();
            }
            catch
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error connecting to the MQ Queue Manager", "Please check MQ Connection string in App Config.");
                return;
            }

            while (!_bStopThread)
            {
                if (_bActive)
                {
                    _oIO.GetODIERequests(ref _mqcClient);
                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                    return;
                }
            }
        }
    }
}