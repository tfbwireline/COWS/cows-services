﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:include href="style.xslt" />
	<xsl:template match="/">
		<html>
			<body style="font-family: Verdana; font-size: 10pt; margin: 5; padding: 5;">
				<!-- Email Header -->
				<p>
					<br>===========================================================</br>
					<br>= <xsl:value-of select="/CPTEmail/CPTInfo/EmailHeader" /> =</br>
					<br>= DO NOT REPLY TO THIS EMAIL =</br>
					<br>===========================================================</br>
				</p>

				<!-- Email Message -->
				<p>
					<xsl:value-of select="/CPTEmail/CPTInfo/EmailMessage" />
				</p>

				<table border="0" align="left" style="font-family: Verdana; font-size: 10pt; WORD-BREAK: break-word;" cellpadding="5">
					<!-- CPT Changed Data -->
					<xsl:if test="/CPTEmail/CPTInfo/Changes[.!='']">
						<tr>
							<td colspan="2">
								<div style="text-align: left; font-weight: bold;">
									<br></br>Changed Data
								</div>
								<div>
									======================================================
								</div>
							</td>
						</tr>
						<xsl:if test="/CPTEmail/NewGatekeeper/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">Gatekeeper:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewGatekeeper/UserName" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="/CPTEmail/NewManager/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">Manager:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewManager/UserName" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="/CPTEmail/NewMSS/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">MSS:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewMSS/UserName" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="/CPTEmail/NewNTE/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">NTE:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewNTE/UserName" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="/CPTEmail/NewPM/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">PM:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewPM/UserName" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="/CPTEmail/NewCRM/UserName[.!='']">
							<tr>
								<td width="20%" style="text-align: left; font-weight: bold;">CRM:</td>
								<td width="80%">
									<xsl:value-of select="/CPTEmail/NewCRM/UserName" />
								</td>
							</tr>
						</xsl:if>
					</xsl:if>

					<!-- CPT Request Details -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>CPT Request Details
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">CPT Number:</td>
						<td width="80%">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="/CPTEmail/CPTInfo/CPTViewLink" />
								</xsl:attribute>
								<xsl:value-of select="/CPTEmail/CPTInfo/CPTNumber" />
							</a>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Customer:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/CustomerName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">H1:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/H1" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Number of Devices:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/NoOfDevices" />
						</td>
					</tr>

					<!-- Additional Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>Original Information
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Customer Type:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTRelatedInfo/CustomerType" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">MDS Service Tier:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTRelatedInfo/ServiceTier" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">NDD Link:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTRelatedInfo/NDDLink" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Short Name:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTRelatedInfo/Shortname" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Gatekeeper:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/Gatekeeper/UserName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Manager:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/Manager/UserName" />
						</td>
					</tr>
					<xsl:if test="/CPTEmail/MSS/UserName[.!='']">
						<tr>
							<td width="20%" style="text-align: left; font-weight: bold;">MSS:</td>
							<td width="80%">
								<xsl:value-of select="/CPTEmail/MSS/UserName" />
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">PM:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/PM/UserName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">NTE:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/NTE/UserName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">CRM:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CRM/UserName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Note:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTRelatedInfo/Description" />
						</td>
					</tr>

					<!-- Additional Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>Additional Information
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">MS Hosted Service:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IsHostedManagedService" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Encrypted Modem:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IsEncryptedModem" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">TACACS Access:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IsTacacs" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Purchasing SDWAN/Meraki?:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IsSdwan" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">SDWAN and/or Meraki ONLY Customer?:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IsSdwanCustomer" />
						</td>
					</tr>

					<!-- Site Assignment Information -->
					<xsl:if test="/CPTEmail/CPTInfo/IsResourceAssignment[.!='']">
						<tr>
							<td colspan="2">
								<div style="text-align: left; font-weight: bold;">
									<br></br>Site Assignment Information
								</div>
								<div>
									======================================================
								</div>
								<div style="text-align: left;">
									If you feel the site assignments are incorrect, please contact the NOS Team via PDL
									<a href="mailto:IPS-MSOnBoarding@t-mobile.com">IPS-MSOnBoarding@t-mobile.com</a>.
								</div>
							</td>
						</tr>
						<tr>
							<td width="20%" style="text-align: left; font-weight: bold;">Prime Site:</td>
							<td width="80%">
								<xsl:value-of select="/CPTEmail/CPTInfo/PrimeSite" />
							</td>
						</tr>
						<tr>
							<td width="20%" style="text-align: left; font-weight: bold;">Govt Customer:</td>
							<td width="80%">
								<xsl:value-of select="/CPTEmail/CPTInfo/GovtCustomer" />
							</td>
						</tr>
						<tr>
							<td width="20%" style="text-align: left; font-weight: bold;">Primary Transport:</td>
							<td width="80%">
								<xsl:value-of select="/CPTEmail/CPTInfo/PrimaryTransport" />
							</td>
						</tr>
						<tr>
							<td width="20%" style="text-align: left; font-weight: bold;">Secondary Transport:</td>
							<td width="80%">
								<xsl:value-of select="/CPTEmail/CPTInfo/SecondaryTransport" />
							</td>
						</tr>
					</xsl:if>

					<!-- T-Mobile Sales Team Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>T-Mobile Sales Team Info
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">IM Email:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/IPMEmail" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Account Manager Email:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/AMEmail" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: 600">SE Email:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/SEEmail" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: 600">Submitted By:</td>
						<td width="80%">
							<xsl:value-of select="/CPTEmail/CPTInfo/SubmittedBy" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<xsl:if test="/CPTEmail/CPTInfo/UpdatedBy[.!='']">
								<p>
									<br></br><br></br>
									Changes made by: <xsl:value-of select="/CPTEmail/CPTInfo/UpdatedBy" />
								</p>
							</xsl:if>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>