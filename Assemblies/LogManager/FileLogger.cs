//Last-modified: 2006Oct27 dtn9708, 2006Dec06, 2006Dec07, 2006Dec28, 2007Jan10, 2007Jan30
//2007Feb02, 2007Feb06 FilePath - include year\month
using System;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace LogManager
{
    // These classes are designed for XML documentation. This gives you intellisense. To
    // turn on XML documentation, right-click on the project in solution explorer, click on
    // property, select build, go to the bottom of the page, below errors and warnings, then
    // scroll below output, check XML documentation file and use the bin\debug directory.

    /// <summary>
    /// Class to create htm or text files with messages. This will replace emails.
    /// </summary>
    public class FileLogger
    {
        #region Compatibility of new Logger - Enumerations

        public enum Event
        {
            BeginStart = 1,
            EndStart = 101,
            BeginDBRead = 3,
            EndDBRead = 104,
            BeginDBWrite = 5,
            EndDBWrite = 106,
            BeginMQPeek = 7,
            BeginMQRead = 8,
            BeginMQWrite = 9,
            EndMQPeek = 110,
            EndMQRead = 111,
            EndMQWrite = 112,
            BeginHTTPPost = 13,
            EndHTTPPost = 114,
            BeginHTTPGet = 15,
            EndHTTPGet = 116,
            BeginSession = 17,
            EndSession = 118,
            BeginAuthenticate = 19,
            EndAuthenticate = 120,
            BeginFTPPut = 21,
            EndFTPPut = 122,
            BeginFTPGet = 23,
            EndFTPGet = 124,
            BeginCaching = 25,
            EndCaching = 126,
            Error = 127,
            BeginConfigRead = 28,
            EndConfigRead = 129,
            BeginThread = 30,
            EndThread = 130,
            GenericMessage = 131
        }

        public enum MsgType
        {
            text = 1,
            xml = 2,
            error = 3,
            query = 4,
            JSError = 5
        }

        public enum MsgClass
        {
            Orders = 1,
            Events = 2,
            Redesigns = 3,
            EmailRequests = 4,
        }

        #endregion Compatibility of new Logger - Enumerations

        private string sLogRootDirectory = string.Empty;
        private string sLogApplicationDirectory = string.Empty;
        private string sLogAppDir = string.Empty; //2006Dec07
        private string sFullPath = string.Empty;

        //logging levels
        private static int _ALL = 100;

        private static int _INFO = 75;
        private static int _ERROR = 50;
        private static int _OFF = 0;

        public static int ALL
        {
            get { return _ALL; }
        }

        public static int ERROR
        {
            get { return _ERROR; }
        }

        public static int INFO
        {
            get { return _INFO; }
        }

        public static int OFF
        {
            get { return _OFF; }
        }

        //see enum MsgFormats for the values
        private int iMsgFormat = 0; //2007Jan10 default type 0 - htm, 1 - txt, 2 - xml

        /// <summary>
        /// Use this to set the file extension and format of the message.
        /// 0-htm viewable by Internet Explorer or 1-txt viewable by any text editor.
        /// 2-XML
        /// htm is the default
        /// </summary>
        public int MsgFormat  //2007Jan10
        {
            get
            {
                return iMsgFormat;
            }
            set
            {
                iMsgFormat = value;
            }
        }

        /// <summary>
        /// enum of message formats 0-htm: viewable by Internet Explorer or
        /// 1-txt viewable by any text editor, or 2-XML.
        /// htm is the default
        /// </summary>
        public enum MsgFormats : int //2007Jan10
        {
            /// <summary>
            /// This is the default type
            /// </summary>
            htm = 0, //2007Feb02 dtn9708

            /// <summary>
            /// no html markup
            /// </summary>
            txt = 1, //2007Feb02 dtn9708

            /// <summary>
            /// XML extension and message type
            /// </summary>
            xml = 2, //2007Jan30 dtn9708, //2007Feb02 dtn9708

            /// <summary>
            /// Err extension with Message Type, Date and Time
            /// </summary>
            err = 3
        }//enum MsgTypes

        private static FileLogger lInstance = null;

        public static FileLogger GetInstance()
        {
            if (lInstance == null)
            {
                lInstance = new FileLogger();
            }
            return lInstance;
        }

        /// <summary>
        /// The FileLogger gets its LogRootDirectory and LogAppicationDirectory
        /// from the applications app.config.
        /// A new sub-directory is created for each day of the month.
        /// 	<add key="LogRootDirectory" value="\\tvmx0960\mediacom\"/>
        ///	    <add key="LogApplicationDirectory" value="mconws\"/> ... Change this appropriately
        /// </summary>
        public FileLogger()
        {
            //Debug.Assert(1 != 1, "FileLogger constructor."); //Test***
            sLogRootDirectory = ConfigurationManager.AppSettings["LogRootDirectory"];
            if (sLogRootDirectory == null || sLogRootDirectory == string.Empty)
                sLogRootDirectory = ConfigurationManager.AppSettings["LogAppDir"];
            sLogApplicationDirectory = ConfigurationManager.AppSettings["LogApplicationDirectory"];

            //Debug.Assert(sLogRootDirectory != null && sLogRootDirectory.Length > 3, "Appsettings", "Missing LogRootDirectory");
            //Debug.Assert(sLogApplicationDirectory != null && sLogApplicationDirectory.Length > 2, "Appsettings", "Missing LogApplicationDirectory");

            //create the directory if it doesn't exist
            sLogAppDir = sLogRootDirectory + sLogApplicationDirectory; //2006Dec07

            sFullPath = CheckAndCreatePath(sLogAppDir); //2007Feb06
        }//FileLogger constructor

        /// <summary>
        /// Constructor where the app supplies a Log Application Directory
        /// A new sub-directory is created for each day of the month.
        /// </summary>
        /// <param name="pLogAppDir">Log Application Directory</param>
        public FileLogger(string pLogAppDir)
        {
            //Debug.Assert(1 != 1, "FileLogger constructor."); //Test***

            //a new directory is created for each day of the month
            //Debug.Assert(pLogAppDir != null && pLogAppDir.Length > 2, "FileLogger constructor", "Missing Log Application Directory in the constructor.");

            sLogAppDir = pLogAppDir; //2006Dec07
            //create the directory if it doesn't exist

            sFullPath = CheckAndCreatePath(sLogAppDir); //2007Feb06
        }//FileLogger constructor

        #region Compatibility of new Logger - Methods

        public static void Weblog(Event evt, MsgType msgtype, int iRecords,
            string sParameters, string sMessage, string sSubMessage, string sUserName)
        {
            Weblog(String.Empty, evt, msgtype, iRecords,
                    sParameters, sMessage, sSubMessage, sUserName);
        }

        public static void Weblog([Optional, DefaultParameterValue("")]
            string LogName, Event evt, MsgType msgtype, int iRecords,
                    string sParameters, string sMessage, string sSubMessage, string sUserName)
        {
            WriteLogFile(LogName, evt, msgtype, iRecords, sParameters, sMessage, string.Format("{0} - {1}", sSubMessage, sUserName));
        }

        public static void WriteLogFile(Event logEvent, MsgType msgType, int iRecords, string sParameters, string sMessage, string sSubMessage)
        {
            WriteLogFile(string.Empty, logEvent, msgType, iRecords, sParameters, sMessage, sSubMessage);
        }

        public static void WriteLogFile([Optional, DefaultParameterValue("")] string sFolder, Event logEvent, MsgType msgType, int iRecords, string sParameters, string sMessage, string sSubMessage)
        {
            string
                sLogDirectories = ConfigurationManager.AppSettings["LogDirectoryList"],
                sLogDir = string.Empty,
                sFinalPath = string.Empty,
                sMsg = string.Empty;

            if (sLogDirectories.Contains(","))
            {
                string[] sLogDirectoryList = sLogDirectories.Trim().Split(',');

                foreach (string str in sLogDirectoryList)
                {
                    if (str.Contains(sFolder))
                    {
                        sLogDir = str;
                        break;
                    }
                }
                if (sLogDir == string.Empty && sLogDirectoryList.Length > 0)
                    sLogDir = sLogDirectoryList[0];
            }
            else
            {
                sLogDir = sLogDirectories;
            }

            //create the directory if it doesn't exist
            //sFinalPath = CheckAndCreateDir(sLogDir);
            if (msgType == MsgType.error)
                sMsg = "Error";
            else
                sMsg = "Log";

            SaveFile(sMsg, string.Format("{0}. {1}", sMessage, sSubMessage), sLogDir);
        }

        private static string CheckAndCreateDir(string sLogAppDir) //2007Feb06 new method
        {
            string sFullPath = string.Empty;
            //a new directory is created for each day of the month

            if (sLogAppDir.Substring(sLogAppDir.Length - 1, 1) != @"\")
            {
                sLogAppDir = sLogAppDir + @"\";
            }

            //create the directory if it doesn't exist. sLogAppDir terminates with @"\"
            DateTime nowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.Append(sLogAppDir);
            sb.Append(nowTime.ToString("yyyy"));
            sb.Append(@"\");
            sb.Append(nowTime.ToString("MM"));
            sb.Append(@"\");
            sb.Append(nowTime.ToString("dd"));
            sb.Append(@"\");

            sFullPath = sb.ToString();
            if (!Directory.Exists(sFullPath))
            {
                //This id may not be able to create a directory
                try
                {
                    DirectoryInfo di = Directory.CreateDirectory(sFullPath);
                }
                catch (Exception ex)
                {
                    string sText = ex.Message;
                    throw new Exception("FileLogger Exception: " + sText, ex);
                }
            }//if

            if (sFullPath.Length < 9)
            {
                sFullPath = @"C:\FileLoggerOutput\";
            }

            return sFullPath;
        }

        private static void SaveFile(string sMsgType, string sMessage, string sLogDir)
        {
            string sSuffix = ".txt";
            //lock (this)
            //{
            if (sMsgType == null | sMsgType.Length < 2)
            {
                sMsgType = "Msg";
            }

            if (sMessage == null | sMessage.Length < 1)
            {
                sMessage = "The message is missing";
            }

            //We need to keep doing this to go from day to day.
            //An alternative is a try/catch block. The catch would create the
            //new directory.
            string sFinalPath = CheckAndCreateDir(sLogDir); //2007Feb06
            string sText = sMsgType;
            if (sMsgType.Length > 30)
            {
                sText = sMsgType.Substring(0, 30);
            }
            else
            {
                sText = sMsgType;
            }
            //construct the full file name
            DateTime nowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.Append(sFinalPath);
            sb.Append(sText);
            sb.Append("-");
            sb.Append(nowTime.ToString("yyyy-"));
            sb.Append(nowTime.ToString("MM-"));
            sb.Append(nowTime.ToString("dd-"));
            sb.Append(Convert.ToString(nowTime.Hour));
            sb.Append("-");
            sb.Append(Convert.ToString(nowTime.Minute / 10));
            sb.Append(sSuffix);

            FileStream fs = null;
            string sFileName = sb.ToString();
            try
            {
                try
                {
                    fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write);
                }
                catch
                { }

                //If file is already open by other application,
                //create other file by appending _i in file name
                int i = 1;
                string sFileName2 = string.Empty;
                while (fs == null)
                {
                    sFileName2 = sFileName.Replace(sSuffix, "_" + i.ToString() + sSuffix);
                    try
                    {
                        fs = new FileStream(sFileName2, FileMode.Append, FileAccess.Write);
                    }
                    catch
                    { }

                    i = i + 1;
                }

                //2007Feb06
                //using (FileStream fs = new FileStream(sFileName, //2007Jan10
                //    FileMode.Append, FileAccess.Write))
                //{
                StreamWriter sw = new StreamWriter(fs);
                DateTime dtNow = DateTime.Now;
                int iMsgFormatLOG = 1;

                switch (iMsgFormatLOG)
                {
                    case (int)MsgFormats.txt:
                        sw.WriteLine(dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":\r\n" +
                            sMessage);
                        break;

                    case (int)MsgFormats.xml:  //dtn9708 2007Jan30
                        sw.WriteLine(sMessage);
                        break;

                    case (int)MsgFormats.err: //dtn9708 2007Feb02
                        sw.WriteLine(dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":\r\n" +
                            sMessage);
                        break;

                    default:
                        //sw.WriteLine("<B>" + DateTime.Now.ToString() + ": " + sMsgType + ":</B><BR>" + sw.Replace("<", "&lt;").Replace(">", "&gt;") + "<BR>");
                        sw.WriteLine("<B>" + dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":</B><BR>" +
                            sMessage.Replace("<", "&lt;").Replace(">", "&gt;") + "<BR>");
                        break;
                }//switch

                sw.Flush();

                sw.Close();
                //}//using
            }//try
            catch (Exception ex)
            {
                string sMsgText = ex.Message;
                throw new Exception("FileLogger error: " + sMsgText, ex);
            }//catch
            //}//lock
        }//log

        public static void DequeueMessages()
        {
        }

        #endregion Compatibility of new Logger - Methods

        /// <summary>
        /// Write the message file
        /// </summary>
        /// <param name="sMessage">the message to write</param>
        public void WriteLogFile(string sMessage)
        {
            lock (this) //dtn9708 2006Dec28
            {
                WriteLogFile("Msg", sMessage);
            }//lock
        }//log

        //dtn9708 2007Feb02
        /// <summary>
        /// This overload sets the Format of the message.
        /// It sets property MsgFormat using enum MsgFormats.
        /// The format is set for the one instance, then set back afterwards.
        /// </summary>
        /// <param name="sMsgType">MQRec or CASA or ?</param>
        /// <param name="sMessage">The message</param>
        /// <param name="MsgFormat">htm, txt, xml etc. see enum MsgFormats</param>
        public void WriteLogFile(string sMsgType, string sMessage, int MsgFormat)
        {
            lock (this) //dtn9708 2006Dec28
            {
                int iSave = MsgFormat;
                iMsgFormat = MsgFormat;
                WriteLogFile(sMsgType, sMessage);
                iMsgFormat = iSave;
            }//lock
        }//log

        public void WriteLogFile(string sMsgType, string sMessage, int MsgFormat, int level)
        {
            int logLevel = _ERROR;
            String strLogLevel = ConfigurationManager.AppSettings["logLevel"];
            if (strLogLevel == null || strLogLevel.Equals(string.Empty))
            {
                strLogLevel = "ERROR";
            }
            switch (strLogLevel)
            {
                case "ALL":
                    logLevel = _ALL;
                    break;

                case "ERROR":
                    logLevel = _ERROR;
                    break;

                case "INFO":
                    logLevel = _INFO;
                    break;

                default:
                    logLevel = _OFF;
                    break;
            }
            if (logLevel >= level)
            {
                this.WriteLogFile(sMsgType, sMessage, MsgFormat);
            }
        }//log

        /// <summary>
        /// Write the message file
        /// </summary>
        /// <param name="sMsgType">Caption on every message such as service name.</param>
        /// <param name="sMessage">the message to write</param>
        public void WriteLogFile(string sMsgType, string sMessage)
        {
            //Debug.Assert(1 != 1, "FileLogger log."); //Test***

            string suffix = ".htm"; //2007Jan10
            if (sMsgType.ToLower().Equals("query"))
                iMsgFormat = (int)MsgFormats.htm;
            lock (this)
            {
                if (sMsgType == null | sMsgType.Length < 2)
                {
                    sMsgType = "Msg";
                }

                //Debug.Assert(sMessage.Length > 0, "2nd Parameter error.", "Message is too short");
                if (sMessage == null | sMessage.Length < 1)
                {
                    sMessage = "The message is missing";
                }

                switch (iMsgFormat) //2007Jan10
                {
                    case (int)MsgFormats.txt:
                        suffix = ".txt";
                        break;

                    case (int)MsgFormats.xml:  //dtn9708 2007Jan30
                        suffix = ".xml";
                        break;

                    case (int)MsgFormats.err: //dtn9708 2007Feb02
                        suffix = ".err";
                        break;

                    default:
                        suffix = ".htm";
                        break;
                }

                //We need to keep doing this to go from day to day.
                //An alternative is a try/catch block. The catch would create the
                //new directory.
                sFullPath = CheckAndCreatePath(sLogAppDir); //2007Feb06
                string sText = sMsgType;
                if (sMsgType.Length > 30)
                {
                    sText = sMsgType.Substring(0, 30);
                }
                else
                {
                    sText = sMsgType;
                }
                //construct the full file name
                DateTime nowTime = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                sb.Append(sFullPath);
                sb.Append(sText);
                sb.Append("-");
                sb.Append(nowTime.ToString("yyyy-"));
                sb.Append(nowTime.ToString("MM-"));
                sb.Append(nowTime.ToString("dd-"));
                sb.Append(Convert.ToString(nowTime.Hour));
                sb.Append("-");
                sb.Append(Convert.ToString(nowTime.Minute / 10));
                sb.Append(suffix);

                FileStream fs = null;
                string sFileName = sb.ToString();
                try
                {
                    try
                    {
                        fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write);
                    }
                    catch
                    { }

                    //If file is already open by other application,
                    //create other file by appending _i in file name
                    int i = 1;
                    string sFileName2 = string.Empty;
                    while (fs == null)
                    {
                        sFileName2 = sFileName.Replace(suffix, "_" + i.ToString() + suffix);
                        try
                        {
                            fs = new FileStream(sFileName2, FileMode.Append, FileAccess.Write);
                        }
                        catch
                        { }

                        i = i + 1;
                    }

                    //2007Feb06
                    //using (FileStream fs = new FileStream(sFileName, //2007Jan10
                    //    FileMode.Append, FileAccess.Write))
                    //{
                    StreamWriter sw = new StreamWriter(fs);
                    DateTime dtNow = DateTime.Now;

                    switch (iMsgFormat)
                    {
                        case (int)MsgFormats.txt:
                            sw.WriteLine(dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":\r\n" +
                                sMessage);
                            break;

                        case (int)MsgFormats.xml:  //dtn9708 2007Jan30
                            sw.WriteLine(sMessage);
                            break;

                        case (int)MsgFormats.err: //dtn9708 2007Feb02
                            sw.WriteLine(dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":\r\n" +
                                sMessage);
                            break;

                        default:
                            //sw.WriteLine("<B>" + DateTime.Now.ToString() + ": " + sMsgType + ":</B><BR>" + sw.Replace("<", "&lt;").Replace(">", "&gt;") + "<BR>");
                            sw.WriteLine("<B>" + dtNow.Date.ToString("d") + " " + dtNow.Hour.ToString() + ":" + dtNow.Minute.ToString() + ":" + dtNow.Second.ToString() + ":" + dtNow.Millisecond.ToString() + " " + sMsgType + ":</B><BR>" +
                                sMessage.Replace("<", "&lt;").Replace(">", "&gt;") + "<BR>");
                            break;
                    }//switch

                    sw.Flush();

                    sw.Close();
                    //}//using
                }//try
                catch (Exception ex)
                {
                    string sMsgText = ex.Message;
                    throw new Exception("FileLogger error: " + sMsgText, ex);
                }//catch
            }//lock
        }//log

        /// <summary>
        ///
        /// </summary>
        /// <param name="sMessage"></param>
        public void LogQueueMessages(string sMessage)
        {
            string sFullPath = CheckAndCreatePath(sLogAppDir);

            //construct the full file name
            DateTime nowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.Append(sFullPath);
            sb.Append("Queue Msg");
            sb.Append("-");
            sb.Append(nowTime.ToString("yyyy-MM-dd hh-mm-ss-fff tt"));
            sb.Append(".xml");

            string sFileName = sb.ToString();

            try
            {
                using (FileStream fs = new FileStream(sFileName, FileMode.CreateNew, FileAccess.Write))
                {
                    StreamWriter w = new StreamWriter(fs);
                    //w.WriteLine(sMessage.Replace("<", "&lt;").Replace(">", "&gt;"));
                    w.WriteLine(sMessage);
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                string sMsgText = ex.Message;
                throw new Exception("FileLogger error: " + sMsgText, ex);
            }
        }

        /// <summary>
        /// Use the available information to construct the full path and
        /// create the path if it doesn't exist.
        /// </summary>
        /// <param name="sLogAppDir">LogAppDir from the constructor or config file</param>
        /// <returns>sFullPath</returns>
        private string CheckAndCreatePath(string sLogAppDir) //2007Feb06 new method
        {
            //Debug.Assert(false, "CheckAndCreatePath()");

            string sFullPath = string.Empty;
            //a new directory is created for each day of the month
            //Debug.Assert(sLogAppDir != null && sLogAppDir.Length > 2, "CheckAndCreatePath()", "Missing Log Application Directory parameter.");

            if (sLogAppDir.Substring(sLogAppDir.Length - 1, 1) != @"\")
            {
                sLogAppDir = sLogAppDir + @"\";
            }

            //create the directory if it doesn't exist. sLogAppDir terminates with @"\"
            DateTime nowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.Append(sLogAppDir);
            sb.Append(nowTime.ToString("yyyy"));
            sb.Append(@"\");
            sb.Append(nowTime.ToString("MM"));
            sb.Append(@"\");
            sb.Append(nowTime.ToString("dd"));
            sb.Append(@"\");

            sFullPath = sb.ToString();
            if (!Directory.Exists(sFullPath))
            {
                //This id may not be able to create a directory
                try
                {
                    DirectoryInfo di = Directory.CreateDirectory(sFullPath);
                }
                catch (Exception ex)
                {
                    string sText = ex.Message;
                    throw new Exception("FileLogger Exception: " + sText, ex);
                }
            }//if

            if (sFullPath.Length < 9)
            {
                sFullPath = @"C:\FileLoggerOutput\";
            }

            return sFullPath;
        }

        /// <summary>
        /// Log all the activities of a complete day.
        /// </summary>
        /// <param name="sMsgType">Type of Message</param>
        /// <param name="sMessage">message to log in file</param>
        public void LogAllDayActivities(string sMsgType, string sMessage)
        {
            string sFullPath = CheckAndCreatePath(sLogAppDir);

            //Create the file name
            DateTime nowTime = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.Append(sFullPath);
            sb.Append(sMsgType);
            sb.Append("-");
            sb.Append(nowTime.ToString("yyyy-"));
            sb.Append(nowTime.ToString("MM-"));
            sb.Append(nowTime.ToString("dd"));
            sb.Append(".htm");

            string sFileName = sb.ToString();

            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);

                sw.WriteLine("<B>" + nowTime.Date.ToString("d") + " " + nowTime.Hour.ToString() + ":" + nowTime.Minute.ToString() + ":" + nowTime.Second.ToString() + ":" + nowTime.Millisecond.ToString() + " " + sMsgType + ":</B><BR>" +
                                    sMessage.Replace("<", "&lt;").Replace(">", "&gt;") + "<BR>");
            }
            catch (Exception ex)
            {
                string sMsgText = ex.Message;
                throw new Exception("FileLogger error: " + sMsgText, ex);
            }
            finally
            {
                sw.Flush();
                sw.Close();
                fs.Close();
            }
        }
    }//class FileLogger
}