﻿using System.ComponentModel;

namespace AppConstants
{
    public enum ERedesignSrchFields
    {
        [Description("Redesign Number")]
        RedesignNumer,

        [Description("Customer Name")]
        CustomerName,

        [Description("NTE Name")]
        NTE,

        [Description("ODIE Device")]
        ODIEDevice,

        [Description("PM Name")]
        PM
    }
}