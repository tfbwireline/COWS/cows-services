﻿namespace AppConstants
{
    public enum EAccessType
    {
        Originating = 1,
        Terminating
    }
}