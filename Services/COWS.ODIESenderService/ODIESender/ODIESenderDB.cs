﻿using DataManager;
using System.Data;

namespace COWS.ODIESenderService
{
    public class ODIESenderDB : MSSqlBase
    {
        public DataSet GetODIERequests()
        {
            setCommand("dbo.getODIERequests");
            return getDataSet();
        }

        public int UpdateODIERequest(int _ReqID, int _iStat)
        {
            setCommand("dbo.updateODIERequest");
            addIntParam("@REQ_ID", _ReqID);
            addIntParam("@STUS_ID", _iStat);

            return execNoQuery();
        }

        public DataSet getDesignCompleteData(int REQ_ID)
        {
            setCommand("dbo.getDesignCompleteData");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public DataSet getODIECPEMsgInfo(int REQ_ID)
        {
            setCommand("dbo.getODIECPEMsgInfo");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public DataSet getODIECPEStus(int REQ_ID)
        {
            setCommand("dbo.getODIECPEStus");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public DataSet getODIECPEPRCV(int REQ_ID)
        {
            setCommand("dbo.getODIECpePRCV");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public DataSet getODIECPEISMV(int REQ_ID)
        {
            setCommand("dbo.getODIECpeISMV");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public DataSet getODIECpeReUse(int REQ_ID)
        {
            setCommand("dbo.getODIECpeReus");
            addIntParam("@REQ_ID", REQ_ID);

            return getDataSet();
        }

        public int InsertCustomerSecuredLog(int SCRD_OBJ_ID, int SCRD_OBJ_TYPE_ID, string LogMsg)
        {
            setCommand("dbo.insertCustScrdLog");
            addIntParam("@SCRD_OBJ_ID", SCRD_OBJ_ID);
            addIntParam("@SCRD_OBJ_TYPE_ID", SCRD_OBJ_TYPE_ID);
            addStringParam("@LogMsg", LogMsg);

            return execNoQuery();
        }
    }
}