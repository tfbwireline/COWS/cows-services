﻿namespace AppConstants
{
    public enum EMPLSEventType
    {
        ScheduledImplementation = 1,
        VASScheduledPreConfiguration,
        VASScheduledImplementation
    }
}