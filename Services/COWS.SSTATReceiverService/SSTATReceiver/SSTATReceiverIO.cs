﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;

namespace COWS.SSTATReceiverService
{
    public class SSTATReceiverIO
    {
        public string sMessage = string.Empty;
        private string sError = string.Empty;
        private string RetXML = string.Empty;
        private int iCnt = 0;
        private int RetVal = -1;
        private SSTATReceiverDB _ReceiverDB = new SSTATReceiverDB();
        private MQC _mqc;
        private DataRow DR;

        public bool InsertSSTATMessage()
        {
            //CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Raw XML Message received: " + sMessage, string.Empty);

            XmlDocument _xmlDoc = new XmlDocument();
            XmlElement _xmlRoot;
            string _RootElement = string.Empty;
            bool _bRetVal = false;
            bool _bContinue = true;

            try
            {
                try
                {
                    _xmlDoc.LoadXml(sMessage);
                }
                catch
                {
                    sMessage = sMessage.Substring(2, sMessage.Length - 2);
                    _xmlDoc.LoadXml(sMessage);
                }

                _xmlRoot = _xmlDoc.DocumentElement;
                _RootElement = _xmlRoot.Name.ToString();

                CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "XML after basic cleaning: " + sMessage, string.Empty);
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error reading the XML Message with exception: " + e.ToString(), string.Empty);
                _bContinue = false;
            }

            sMessage = sMessage.Replace("tp:", string.Empty);
            if (_bContinue)
            {
                switch (_RootElement)
                {
                    case "SSTATOrderStus":
                        {
                            sError = InsertOrderXmlData();
                            if (sError.Length <= 0)
                                _bRetVal = true;
                            else
                                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "ERROR : " + sError, string.Empty);
                            break;
                        }
                    default:
                        {
                            CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "XML Message with Invalid Root Node!", string.Empty);
                            break;
                        }
                }
            }

            return _bRetVal;
        }

        public void ResendAcks()
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            string XMLStr = string.Empty;

            SSTATOrderStus _ack = new SSTATOrderStus();

            try
            {
                DS = _ReceiverDB.GetSSTATAcks();

                if ((DS != null) && (DS.Tables.Count > 0))
                {
                    DT = DS.Tables[0];

                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        int iCnt = DT.Rows.Count;
                        CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send ACK(s) to SSTAT", string.Empty);
                        for (int i = 0; i < iCnt; i++)
                        {
                            DR = DT.Rows[i];
                            XMLStr = string.Empty;

                            _ack.TRANID = DR["TRAN_ID"].ToString();
                            _ack.OrderID = DR["ORDR_ID"].ToString();
                            _ack.DeviceID = DR["DEVICE_ID"].ToString();
                            _ack.OrderAction = (SSTATOrderStusOrderAction)Enum.Parse(typeof(SSTATOrderStusOrderAction), "ACK");
                            _ack.CompletionDate = DR["COMPLETION_DATE"].ToString();
                            _ack.Note = DR["NOTE"].ToString();
                            _ack.ErrorMsg = DR["ERROR_MSG"].ToString();

                            XmlSerializer _Serializer = new XmlSerializer(typeof(SSTATOrderStus));
                            XMLStr = SerializeObject<SSTATOrderStus>(Encoding.UTF8, _ack);
                            _mqc = new MQC();
                            _mqc.MQPut(XMLStr);
                            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK SENT: " + XMLStr, string.Empty);

                            _ReceiverDB.UpdateSSTATAck(Convert.ToInt32(FixNull(_ack.TRANID.ToString())), true);
                            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK_CD Updated on TRAN_ID:  " + _ack.TRANID.ToString(), string.Empty);
                        }
                    }
                    else
                    {
                        if (DateTime.Now.Second % 30 == 0)
                        {
                            CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to SSTAT", string.Empty);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "ERROR: " + ex.Message + "; " + ex.StackTrace, string.Empty);
            }
        }

        public void ProcessAllMsgs()
        {
            _ReceiverDB.ProcessNewMsgs();
        }

        #region "Private Methods"

        private string InsertOrderXmlData()
        {
            SSTATOrderStus _Response = new SSTATOrderStus();
            sError = string.Empty;
            try
            {
                _Response = (SSTATOrderStus)DeserializeXML(_Response);
            }
            catch
            {
                sError = "Invalid XML. Unable to deserialize this XML data.";
                return sError;
            }

            try
            {
                _ReceiverDB.TRANID = Convert.ToInt32(FixNull(_Response.TRANID.ToString()));
            }
            catch
            {
                _ReceiverDB.TRANID = 0;
            }
            if (_ReceiverDB.TRANID == 0)
            {
                sError = "Invalid TranID. Cannot database the XML Data.";
                return sError;
            }

            if ((_Response.OrderAction.ToString() != "ACK") && (_Response.OrderAction.ToString() != "NACK"))
            {
                RetVal = _ReceiverDB.InsertSSTATResponse(ref _Response);

                if (RetVal != -1)
                {
                    sError = "Error inserting the XML Message data into the Database. ";
                    CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "ERROR: " + sError, string.Empty);

                    _mqc = new MQC();

                    _Response.OrderAction = (SSTATOrderStusOrderAction)Enum.Parse(typeof(SSTATOrderStusOrderAction), "NACK");
                    XmlSerializer _Serializer = new XmlSerializer(typeof(SSTATOrderStus));
                    RetXML = SerializeObject<SSTATOrderStus>(Encoding.UTF8, _Response);
                    _mqc.MQPut(RetXML);
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "NACK SENT: Error inserting the XML Message data into the Database.  " + _Response.ToString(), string.Empty);
                }
                else
                {
                    // CL.WriteLogFile("Response", CL.Event.EndDBWrite, CL.MsgType.text, -1, string.Empty, "XML Data is successfully databased", string.Empty);

                    if ((_Response.OrderAction.ToString() != "ACK") && (_Response.OrderAction.ToString() != "NACK"))
                    {
                        _mqc = new MQC();

                        _Response.OrderAction = (SSTATOrderStusOrderAction)Enum.Parse(typeof(SSTATOrderStusOrderAction), "ACK");
                        XmlSerializer _Serializer = new XmlSerializer(typeof(SSTATOrderStus));
                        RetXML = SerializeObject<SSTATOrderStus>(Encoding.UTF8, _Response);
                        _mqc.MQPut(RetXML);
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK SENT: " + _Response.ToString(), string.Empty);

                        _ReceiverDB.UpdateSSTATAck(Convert.ToInt32(FixNull(_Response.TRANID.ToString())), true);
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK_CD Updated:  " + _Response.TRANID.ToString(), string.Empty);
                    }
                }
            }
            else
            {
                if (_Response.OrderAction.ToString() == "ACK")
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK Recieved " + _Response.ToString(), string.Empty);
                }
                else
                {
                    _ReceiverDB.InsertSSTATResponse(ref _Response);
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "NACK Recieved " + _Response.ToString(), string.Empty);
                    _ReceiverDB.UpdateSSTATAck(Convert.ToInt32(FixNull(_Response.TRANID.ToString())), true);
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "ACK_CD Updated:  " + _Response.TRANID.ToString(), string.Empty);
                }
            }
            return sError;
        }

        #endregion "Private Methods"

        #region "Convertors"

        private Object DeserializeXML(Object _Object)
        {
            XmlSerializer _XS = new XmlSerializer(_Object.GetType());
            MemoryStream _MS = new MemoryStream(StringToUTF8ByteArray(sMessage));
            XmlTextWriter _XW = new XmlTextWriter(_MS, Encoding.UTF8);
            return _XS.Deserialize(_MS);
        }

        private Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        private bool ConvertStringToBool(string sInput)
        {
            sInput = sInput.ToUpper();
            return ((sInput == "Y") || (sInput == "YES") || (sInput == "ON")) ? true : false;
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        #endregion "Convertors"
    }
}