﻿namespace AppConstants
{
    public enum ESDEStatus
    {
        Unassigned = 501,
        Draft,
        Assigned,
        Win,
        Lost,
        Ongoing,
        Close,
    }
}