@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to apply the application configuration. All files under 
REM the templates subdirectory in the application home will be copied to the 
REM application home (preserving relative paths), and variables in the template 
REM files will be replaced with values from the application configuration. Note 
REM that the functionality is implemented by the framework ant script.
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM perform the host check
REM 
CALL "%_SCRIPT_HOME%"\perform-host-check.cmd
IF ERRORLEVEL 1 (
  CALL "%_SCRIPT_HOME%"\_logJobMsg.cmd configure.cmd 2 "ERROR: host validation failed"
  EXIT /b 22
)


REM
REM call ant target
REM
CALL "%ANT_HOME%\bin\ant.bat" -f "%_SCRIPT_HOME%\_admin.xml" _configure
IF ERRORLEVEL 1 EXIT /B 23


REM
REM write error message to job log if configure job was called
REM 
IF "%1" NEQ "-NOLOG" (
  IF ERRORLEVEL 1 (
    ECHO ERROR: application configuration failed
    "%windir%\system32\cscript.exe" /Nologo "%_SCRIPT_HOME%\_logJobMsg.js" configure.cmd %ERRORLEVEL% "ERROR: application configuration failed"
    EXIT /B 1
  ) ELSE (
    ECHO INFO: application configured succeccfully
    "%windir%\system32\cscript.exe" /Nologo "%_SCRIPT_HOME%\_logJobMsg.js" configure.cmd 0 "INFO: application configured successfully"
    EXIT /B 0
  )
)
