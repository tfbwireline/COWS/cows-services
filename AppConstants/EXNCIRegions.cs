﻿namespace AppConstants
{
    public enum EXNCIRegions
    {
        AMNCI = 1,
        ENCI = 2,
        ANCI = 3
    }
}