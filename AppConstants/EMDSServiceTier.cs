﻿namespace AppConstants
{
    public enum EMDSServiceTier
    {
        MNSStrategic = 1,
        MNSComplete,
        MNSCollaborate,
        MNSSupportDesign,
        MNSSupportImplementation,
        MNSSupportMN,
        MNSSupportDesignImp,
        MNSSupportDesignMN,
        MNSSupportDesignImpMN,
        MNSSupportImpMN,
        LegacyComplete,
        LegacyMTR,
        LegacyMN,
        LegacyDSOC,
        LegacyAimnet
    }
}