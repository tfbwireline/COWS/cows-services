﻿using DataManager;
using System;
using System.Data;

namespace COWS.SSTATReceiverService
{
    public class SSTATReceiverDB : MSSqlBase
    {
        #region "Properties"

        private int _TRANID = 0;

        public int TRANID
        {
            get { return _TRANID; }
            set { _TRANID = value; }
        }

        private string _OrderID = string.Empty;

        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }

        private string _DeviceID = string.Empty;

        public string DeviceID
        {
            get { return _DeviceID; }
            set { _DeviceID = value; }
        }

        private string _OrderAction;

        public string OrderAction
        {
            get { return _OrderAction; }
            set { _OrderAction = value; }
        }

        private string _CompletionDate;

        public string CompletionDate
        {
            get { return _CompletionDate; }
            set { _CompletionDate = value; }
        }

        private string _Note = string.Empty;

        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        private string _ErrorMsg = string.Empty;

        public string ErrorMsg
        {
            get { return _ErrorMsg; }
            set { _ErrorMsg = value; }
        }

        private bool _ACK_CD = false;

        public bool ACK_CD
        {
            get { return _ACK_CD; }
            set { _ACK_CD = value; }
        }

        #endregion "Properties"

        #region "Methods"

        public int InsertSSTATResponse(ref SSTATOrderStus resp)
        {
            setCommand("dbo.insertSSTATResponse");
            addIntParam("@TRAN_ID", int.Parse(resp.TRANID));
            addStringParam("@ORDR_ID", resp.OrderID);
            addStringParam("@DEVICE_ID", resp.DeviceID);
            addStringParam("@ORDER_ACTION", resp.OrderAction.ToString());
            addStringParam("@COMPLETION_DATE", resp.CompletionDate.Trim());
            addStringParam("@NOTE", resp.Note);
            addStringParam("@ERROR_MSG ", resp.ErrorMsg);
            return execNoQuery();
        }

        public int UpdateSSTATAck(int _TRANID, Boolean _ACK_CD)
        {
            setCommand("dbo.updateSSTATAck");
            addIntParam("@TRANID", _TRANID);
            addBooleanParam("@ACK_CD", _ACK_CD);

            return execNoQuery();
        }

        public DataSet GetSSTATAcks()
        {
            setCommand("dbo.getSSTATAcks");
            return getDataSet();
        }

        public int ProcessNewMsgs()
        {
            setCommand("dbo.processSSTATMsgs");
            return execNoQuery();
        }

        #endregion "Methods"
    }
}