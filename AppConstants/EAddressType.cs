﻿namespace AppConstants
{
    public enum EAddressType
    {
        Service,
        Primary = 2,
        Billing,
        Secondary,
        Support,
        CustomerOnSite,
        AlternateSite,
        ServiceAssurance,
        DNSAdministrator,
        SupportSecondary,
        IPLOriginationAddressDrop1,
        IPLTerminationAddressDrop1,
        Onsite,
        Hierarchy,
        Site,
        Shipping,
    }
}