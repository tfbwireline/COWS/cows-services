option explicit

On Error Resume Next

dim webSiteName
dim objAppPool
dim message
dim appPoolName
dim action
dim filesys
dim windir 
dim exeFile
dim exeCmd
dim exeLine
Function logMsg ( msg, code )

	dim objFSO
	dim objFile
	dim logScript
	dim cmd
	dim shell
	dim foo


	Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.GetFile(WScript.ScriptFullName)

	logScript = objFSO.GetParentFolderName(objFile)+"\_logJobMsg.js"
	
	cmd = "cscript.exe " +logScript +" " +WScript.ScriptName +" " +code +" """ +msg +""""

	Set shell = WScript.CreateObject("WScript.Shell")

	shell.run(cmd)

End Function

' Two arguments are expected.  The first is the website name.
' The second is the action to take, STOP or START.
If WScript.Arguments.Count <> 2 Then
	message = "wrong number of arguments, expected 2, found " & WScript.Arguments.Count
	logMsg message, "6"
	WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
	WScript.Quit(6)
End If

webSiteName = WScript.Arguments.Item(0)
action = WScript.Arguments.Item(1)


Set filesys = CreateObject("Scripting.FileSystemObject")
Set windir = filesys.GetSpecialFolder(0)
exeFile = windir & "\System32\inetsrv\appcmd.exe"
WScript.Echo("INFO: " & exeFile)

If filesys.FileExists(exeFile) Then
	appPoolName = webSiteName
	WScript.Echo("INFO: appPoolName is "+appPoolName)
	Set exeLine = WScript.CreateObject("WScript.Shell")
  	if action = "START" Then
		message = "INFO: Starting " +appPoolName
		logMsg message, "0"
		WScript.Echo(message)
		exeCmd = exeFile & " " & action & " apppool /apppool.name:" & appPoolName
		exeLine.run(exeCmd)
		If Err <> 0 Then
			message = "appcmd.exe Start failed: " & Err.Description
			logMsg message, "9"
			WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
			WScript.Quit(9)
		End If

  	elseif action = "STOP" Then
		message = "INFO: Stopping " +appPoolName
		logMsg message, "0"
		WScript.Echo(message)
		exeCmd = exeFile & " " & action & " apppool /apppool.name:" & appPoolName
		exeLine.run(exeCmd)
		If Err <> 0 Then
			message = "appcmd.exe Stop failed: " & Err.Description
			logMsg message, "10"
			WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
			WScript.Quit(10)
		End If

	Else
		message = "Invalid action, " & action & ". Should be START or STOP."
		logMsg message, "8"
		WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
		WScript.Quit(8)
	End If

Else

	appPoolName = "IIS://localhost/W3SVC/AppPools/" & webSiteName
	WScript.Echo("INFO: appPoolName is "+appPoolName)


	Set objAppPool = GetObject(appPoolName)
	If Err <> 0 Then
		message = "GetObject() failed: " & Err.Description
		logMsg message, "7"
		WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
		WScript.Quit(7)
	End If

	if action = "START" Then
		message = "INFO: Starting " +appPoolName
		logMsg message, "0"
		WScript.Echo(message)
		objAppPool.Start()
		If Err <> 0 Then
			message = "objAppPool.Start() failed: " & Err.Description
			logMsg message, "9"
			WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
			WScript.Quit(9)
		End If

	elseif action = "STOP" Then
		message = "INFO: Stopping " +appPoolName
		logMsg message, "0"
		WScript.Echo(message)
		objAppPool.Stop()
		If Err <> 0 Then
			message = "objAppPool.Stop() failed: " & Err.Description
			logMsg message, "10"
			WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
			WScript.Quit(10)
		End If

	Else
		message = "Invalid action, " & action & ". Should be START or STOP."
		logMsg message, "8"
		WScript.Echo("ERROR: _recycle_app_pool.vbs: "+message)
		WScript.Quit(8)
	End If

End If 

message = "INFO: " + appPoolName +" " + action + " successful"
logMsg message, "0"
WScript.Echo(message)
