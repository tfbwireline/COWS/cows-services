﻿using DataManager;
using System.Data;

namespace COWS.SSTATSenderService
{
    public class SSTATSenderDB : MSSqlBase
    {
        public DataSet GetSSTATRequests()
        {
            setCommand("dbo.getSSTATRequests");
            return getDataSet();
        }

        public int UpdateSSTATRequest(int TRAN_ID, int _iStat)
        {
            setCommand("dbo.updateSSTATRequest");
            addIntParam("@TRAN_ID", TRAN_ID);
            addIntParam("@STUS_ID", _iStat);

            return execNoQuery();
        }

        public DataSet getSSTATCPEMsgInfo(int TRAN_ID)
        {
            setCommand("dbo.getSSTATCPEMsgInfo");
            addIntParam("@TRAN_ID", TRAN_ID);

            return getDataSet();
        }

        public DataSet getSSTATCancel(int TRAN_ID)
        {
            setCommand("dbo.getSSTATCancel");
            addIntParam("@TRAN_ID", TRAN_ID);

            return getDataSet();
        }

        public DataSet getSSTATCCD(int TRAN_ID)
        {
            setCommand("dbo.getSSTATCCD");
            addIntParam("@TRAN_ID", TRAN_ID);

            return getDataSet();
        }

        public DataTable getSSTATCPEDispatchEmail(int TRAN_ID)
        {
            setCommand("dbo.getSSTATCPEDispatchEmail");
            addIntParam("@TRAN_ID", TRAN_ID);

            return getDataTable();
        }

        public DataSet getSSTATKey()
        {
            setCommand("dbo.getSSTATKey");
            return getDataSet();
        }

        public int InsertCustomerSecuredLog(int SCRD_OBJ_ID, int SCRD_OBJ_TYPE_ID, string LogMsg)
        {
            setCommand("dbo.insertCustScrdLog");
            addIntParam("@SCRD_OBJ_ID", SCRD_OBJ_ID);
            addIntParam("@SCRD_OBJ_TYPE_ID", SCRD_OBJ_TYPE_ID);
            addStringParam("@LogMsg", LogMsg);

            return execNoQuery();
        }
    }
}