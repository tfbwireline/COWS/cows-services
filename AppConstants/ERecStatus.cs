﻿namespace AppConstants
{
    public enum ERecStatus
    {
        Active = 1,
        InActive = 0,
        OnVacation = 2,
        DeleteHold = 3
    }
}