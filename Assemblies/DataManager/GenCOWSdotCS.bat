@echo off
IF NOT [%1] == [] GOTO OK
ECHO ERROR: Please enter a valid environment value like TEST/TEST1, TEST2, BF, ORT.. within double quotes
EXIT /B 1
:OK
set pathstr= %cd%\
call :Trim pathstr %pathstr%
call set pathstr=%pathstr: =~%
PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command "& '%~dp0\GenCOWSdotCS.ps1'" -path %pathstr% -env %1
PAUSE



:Trim
SetLocal EnableDelayedExpansion
set Params=%*
for /f "tokens=1*" %%a in ("!Params!") do EndLocal & set %1=%%b
exit /b

