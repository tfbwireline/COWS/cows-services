﻿using System.ComponentModel;

namespace AppConstants
{
    public enum EMDSActyType
    {
        InitialInstall = 1,
        MAC,
        Disconnect,

        [Description("Install+Pre-Staging")]
        Install_PreStaging,

        [Description("MAC+Pre-Staging")]
        MAC_PreStaging
    }
}