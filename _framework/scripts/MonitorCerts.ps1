## MonitorCerts.ps1 v1.0ps1
##
## This Script will iterate through a specified folder looking for *.cer files and check
## to see if any are approaching experation.  If so it will send an email to a specified 
## email address
##
## Four parameters should be used:
##  -LOCATION supplies the directory of where the certifications are stored.
##
##  -DAYS_2_REMIND is the amount of days in advance that you want to be reminded before
##  the certificates expire.
##
##  -EMAIL_ADDRESS is the email address you want the notification sent to.
##
##  -SUBJECT is the content of the subject line of the email.
##
## The script makes use of SQL Server Management Objects (SMO) thus must be run from
## a computer that has SQL SMO installed.  This script was written to be run locally on
## a SQL server to invoke agent jobs for the purpose of FIM deployments.  The author is
## not a SQL Server expert, rather an occasional user for the greater good of FIM and ILM
## projects.
##


PARAM
(
    [Parameter(Mandatory = $false)]
    [String]$LOCATION = [String]::Empty,

    [Parameter(Mandatory = $false)]
    [String]$DAYS_2_REMIND = [String]::Empty,
    
    [Parameter(Mandatory = $false)]
    [String]$EMAIL_ADDRESS = [String]::Empty,

    [Parameter(Mandatory = $false)]
    [String]$SUBJECT = [String]::Empty

);
#Assign Variables

$SMTPSERVER = "mailhost.corp.sprint.com"
$FILES=get-childitem $LOCATION *.cer|where-object {!($_.psiscontainer)}
$MESSAGE=""
$DEADLINE=(Get-Date).AddDays($DAYS_2_REMIND)
$HEADER="There are one or more digital certificates in COWS that will expire soon.  See below for the certificate details.`n`n
GOM team, please provide a renewed certificate to the COWS support team. `n`n
COWS support team, please update the config.properties with the expiration date when you receive the new certificate.`n`n"
foreach ($FILE in $FILES) {
  $CRT=New-Object System.Security.Cryptography.X509Certificates.X509Certificate
  $FILE="$LOCATION\$FILE"
  $CRT.Import($FILE)

  $EXPIRE_DATE=$CRT.GetExpirationDateString()
  $CONTACT_INFO=$CRT.GetName()
  $EXPIRE_DATE=get-date $EXPIRE_DATE


  If ($EXPIRE_DATE -lt $DEADLINE) {
    $MESSAGE+= "`n" 
    $MESSAGE+= "File Name: " + $FILE +"`n"
    $MESSAGE+= "File Expire Date: " + $EXPIRE_DATE +"`n"
    $MESSAGE+= "Certificate Contact Information: " + $CONTACT_INFO +"`n"

  }
}


If ($MESSAGE -ne "") {
  $MESSAGE=$HEADER + $MESSAGE
  Write-Host $MESSAGE
  $smtp = new-object Net.Mail.SmtpClient($SMTPSERVER)
  $smtp.Send($EMAIL_ADDRESS, $EMAIL_ADDRESS, $SUBJECT, $MESSAGE)
} else {
  Write-Host "No Certificates are Expected to Expire by $DEADLINE."
}

