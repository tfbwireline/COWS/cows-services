﻿namespace AppConstants
{
    public enum EUCaaSActyType
    {
        Install = 1,
        Move,
        Disconnect,
        ChangeAddRemoveUser,
        ChangeUserFeatureGroup,
        ChangeProgramming,
        ChangeOther,
        ChangeSPS
    }
}