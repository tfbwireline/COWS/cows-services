﻿using DataManager;
using System;
using System.Data;

namespace FSAHelper
{
    public class FSA_ORDR_DB : MSSqlBase
    {
        #region "Order Information Fields"

        private Byte _ORDR_ACTN_ID;

        public Byte ORDR_ACTN_ID
        {
            get { return _ORDR_ACTN_ID; }
            set { _ORDR_ACTN_ID = value; }
        }

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private string _FTN;

        public string FTN
        {
            get { return _FTN; }
            set { _FTN = value; }
        }

        private string _ORDR_TYPE_CD;

        public string ORDR_TYPE_CD
        {
            get { return _ORDR_TYPE_CD; }
            set { _ORDR_TYPE_CD = value; }
        }

        private string _ORDR_SUB_TYPE_CD;

        public string ORDR_SUB_TYPE_CD
        {
            get { return _ORDR_SUB_TYPE_CD; }
            set { _ORDR_SUB_TYPE_CD = value; }
        }

        private string _PROD_TYPE_CD;

        public string PROD_TYPE_CD
        {
            get { return _PROD_TYPE_CD; }
            set { _PROD_TYPE_CD = value; }
        }

        private string _PRNT_FTN;

        public string PRNT_FTN
        {
            get { return _PRNT_FTN; }
            set { _PRNT_FTN = value; }
        }

        private string _RELTD_FTN;

        public string RELTD_FTN
        {
            get { return _RELTD_FTN; }
            set { _RELTD_FTN = value; }
        }

        private string _SCA_NBR;

        public string SCA_NBR
        {
            get { return _SCA_NBR; }
            set { _SCA_NBR = value; }
        }

        private string _TSP_CD;

        public string TSP_CD
        {
            get { return _TSP_CD; }
            set { _TSP_CD = value; }
        }

        private string _CPW_TYPE;

        public string CPW_TYPE
        {
            get { return _CPW_TYPE; }
            set { _CPW_TYPE = value; }
        }

        private DateTime? _CUST_CMMT_DT;

        public DateTime? CUST_CMMT_DT
        {
            get { return _CUST_CMMT_DT; }
            set { _CUST_CMMT_DT = value; }
        }

        private DateTime? _CUST_WANT_DT;

        public DateTime? CUST_WANT_DT
        {
            get { return _CUST_WANT_DT; }
            set { _CUST_WANT_DT = value; }
        }

        private DateTime? _CUST_ORDR_SBMT_DT;

        public DateTime? CUST_ORDR_SBMT_DT
        {
            get { return _CUST_ORDR_SBMT_DT; }
            set { _CUST_ORDR_SBMT_DT = value; }
        }

        private DateTime? _CUST_SIGNED_DT;

        public DateTime? CUST_SIGNED_DT
        {
            get { return _CUST_SIGNED_DT; }
            set { _CUST_SIGNED_DT = value; }
        }

        private DateTime? _ORDR_SBMT_DT;

        public DateTime? ORDR_SBMT_DT
        {
            get { return _ORDR_SBMT_DT; }
            set { _ORDR_SBMT_DT = value; }
        }

        private string _CUST_PRMS_OCPY_CD;

        public string CUST_PRMS_OCPY_CD
        {
            get { return _CUST_PRMS_OCPY_CD; }
            set { _CUST_PRMS_OCPY_CD = value; }
        }

        private string _CUST_ACPT_ERLY_SRVC_CD;

        public string CUST_ACPT_ERLY_SRVC_CD
        {
            get { return _CUST_ACPT_ERLY_SRVC_CD; }
            set { _CUST_ACPT_ERLY_SRVC_CD = value; }
        }

        private string _MULT_CUST_ORDR_CD;

        public string MULT_CUST_ORDR_CD
        {
            get { return _MULT_CUST_ORDR_CD; }
            set { _MULT_CUST_ORDR_CD = value; }
        }

        private string _GVRMNT_TYPE_ID;

        public string GVRMNT_TYPE_ID
        {
            get { return _GVRMNT_TYPE_ID; }
            set { _GVRMNT_TYPE_ID = value; }
        }

        private string _SOTS_ID;

        public string SOTS_ID
        {
            get { return _SOTS_ID; }
            set { _SOTS_ID = value; }
        }

        private string _CHARS_ID;

        public string CHARS_ID
        {
            get { return _CHARS_ID; }
            set { _CHARS_ID = value; }
        }

        private string _FSA_EXP_TYPE_CD;

        public string FSA_EXP_TYPE_CD
        {
            get { return _FSA_EXP_TYPE_CD; }
            set { _FSA_EXP_TYPE_CD = value; }
        }

        private int _MULT_ORDR_INDEX_NBR;

        public int MULT_ORDR_INDEX_NBR
        {
            get { return _MULT_ORDR_INDEX_NBR; }
            set { _MULT_ORDR_INDEX_NBR = value; }
        }

        private int _MULT_ORDR_TOT_CNT;

        public int MULT_ORDR_TOT_CNT
        {
            get { return _MULT_ORDR_TOT_CNT; }
            set { _MULT_ORDR_TOT_CNT = value; }
        }

        private string _VNDR_VPN_CD;

        public string VNDR_VPN_CD
        {
            get { return _VNDR_VPN_CD; }
            set { _VNDR_VPN_CD = value; }
        }

        #endregion "Order Information Fields"

        #region "MDS Install Information Fields"

        private string _INSTL_ESCL_CD;

        public string INSTL_ESCL_CD
        {
            get { return _INSTL_ESCL_CD; }
            set { _INSTL_ESCL_CD = value; }
        }

        private string _INSTL_SOLU_SRVC_DES;

        public string INSTL_SOLU_SRVC_DES
        {
            get { return _INSTL_SOLU_SRVC_DES; }
            set { _INSTL_SOLU_SRVC_DES = value; }
        }

        private string _INSTL_DSGN_DOC_NBR;

        public string INSTL_DSGN_DOC_NBR
        {
            get { return _INSTL_DSGN_DOC_NBR; }
            set { _INSTL_DSGN_DOC_NBR = value; }
        }

        private string _INSTL_VNDR_CD;

        public string INSTL_VNDR_CD
        {
            get { return _INSTL_VNDR_CD; }
            set { _INSTL_VNDR_CD = value; }
        }

        private string _INSTL_NW_TYPE_CD;

        public string INSTL_NW_TYPE_CD
        {
            get { return _INSTL_NW_TYPE_CD; }
            set { _INSTL_NW_TYPE_CD = value; }
        }

        private string _INSTL_TRNSPRT_TYPE_CD;

        public string INSTL_TRNSPRT_TYPE_CD
        {
            get { return _INSTL_TRNSPRT_TYPE_CD; }
            set { _INSTL_TRNSPRT_TYPE_CD = value; }
        }

        private Byte _INSTL_SRVC_TIER_CD;

        public Byte INSTL_SRVC_TIER_CD
        {
            get { return _INSTL_SRVC_TIER_CD; }
            set { _INSTL_SRVC_TIER_CD = value; }
        }

        private string _INSTL_SRVC_TIER_DES;

        public string INSTL_SRVC_TIER_DES
        {
            get { return _INSTL_SRVC_TIER_DES; }
            set { _INSTL_SRVC_TIER_DES = value; }
        }

        #endregion "MDS Install Information Fields"

        #region "CPE Install Information Fields"

        private string _CPE_EQPT_ONLY_CD;

        public string CPE_EQPT_ONLY_CD
        {
            get { return _CPE_EQPT_ONLY_CD; }
            set { _CPE_EQPT_ONLY_CD = value; }
        }

        private string _CPE_CPE_ORDR_TYPE_CD;

        public string CPE_CPE_ORDR_TYPE_CD
        {
            get { return _CPE_CPE_ORDR_TYPE_CD; }
            set { _CPE_CPE_ORDR_TYPE_CD = value; }
        }

        private string _CPE_ACCS_PRVDR_CD;

        public string CPE_ACCS_PRVDR_CD
        {
            get { return _CPE_ACCS_PRVDR_CD; }
            set { _CPE_ACCS_PRVDR_CD = value; }
        }

        private string _CPE_PHN_NBR;

        public string CPE_PHN_NBR
        {
            get { return _CPE_PHN_NBR; }
            set { _CPE_PHN_NBR = value; }
        }

        private string _CPE_PHN_NBR_TYPE_CD;

        public string CPE_PHN_NBR_TYPE_CD
        {
            get { return _CPE_PHN_NBR_TYPE_CD; }
            set { _CPE_PHN_NBR_TYPE_CD = value; }
        }

        private string _CPE_ECCKT_ID;

        public string CPE_ECCKT_ID
        {
            get { return _CPE_ECCKT_ID; }
            set { _CPE_ECCKT_ID = value; }
        }

        private string _CPE_MSCP_CD;

        public string CPE_MSCP_CD
        {
            get { return _CPE_MSCP_CD; }
            set { _CPE_MSCP_CD = value; }
        }

        private string _CPE_DLVRY_DUTY_ID;

        public string CPE_DLVRY_DUTY_ID
        {
            get { return _CPE_DLVRY_DUTY_ID; }
            set { _CPE_DLVRY_DUTY_ID = value; }
        }

        private string _CPE_DLVRY_DUTY_AMT;

        public string CPE_DLVRY_DUTY_AMT
        {
            get { return _CPE_DLVRY_DUTY_AMT; }
            set { _CPE_DLVRY_DUTY_AMT = value; }
        }

        private string _CPE_SHIP_CHG_AMT;

        public string CPE_SHIP_CHG_AMT
        {
            get { return _CPE_SHIP_CHG_AMT; }
            set { _CPE_SHIP_CHG_AMT = value; }
        }

        private string _CPE_REC_ONLY_CD;

        public string CPE_REC_ONLY_CD
        {
            get { return _CPE_REC_ONLY_CD; }
            set { _CPE_REC_ONLY_CD = value; }
        }

        #endregion "CPE Install Information Fields"

        #region "Transport Install - Support Information Fields"

        private string _TSUP_SAC_AVLBLTY_HR_TXT;

        public string TSUP_SAC_AVLBLTY_HR_TXT
        {
            get { return _TSUP_SAC_AVLBLTY_HR_TXT; }
            set { _TSUP_SAC_AVLBLTY_HR_TXT = value; }
        }

        private string _TSUP_SAC_TME_ZONE_CD;

        public string TSUP_SAC_TME_ZONE_CD
        {
            get { return _TSUP_SAC_TME_ZONE_CD; }
            set { _TSUP_SAC_TME_ZONE_CD = value; }
        }

        private string _TSUP_SAC_INTPRTR_REQ_CD;

        public string TSUP_SAC_INTPRTR_REQ_CD
        {
            get { return _TSUP_SAC_INTPRTR_REQ_CD; }
            set { _TSUP_SAC_INTPRTR_REQ_CD = value; }
        }

        private string _TSUP_SAC_LANG_SUPPD_CD;

        public string TSUP_SAC_LANG_SUPPD_CD
        {
            get { return _TSUP_SAC_LANG_SUPPD_CD; }
            set { _TSUP_SAC_LANG_SUPPD_CD = value; }
        }

        private string _TSUP_TELCO_DEMARC_BLDG_NME;

        public string TSUP_TELCO_DEMARC_BLDG_NME
        {
            get { return _TSUP_TELCO_DEMARC_BLDG_NME; }
            set { _TSUP_TELCO_DEMARC_BLDG_NME = value; }
        }

        private string _TSUP_TELCO_DEMARC_FLR_ID;

        public string TSUP_TELCO_DEMARC_FLR_ID
        {
            get { return _TSUP_TELCO_DEMARC_FLR_ID; }
            set { _TSUP_TELCO_DEMARC_FLR_ID = value; }
        }

        private string _TSUP_TELCO_DEMARC_RM_NBR;

        public string TSUP_TELCO_DEMARC_RM_NBR
        {
            get { return _TSUP_TELCO_DEMARC_RM_NBR; }
            set { _TSUP_TELCO_DEMARC_RM_NBR = value; }
        }

        private string _TSUP_NEAREST_CROSS_STREET_NME;

        public string TSUP_NEAREST_CROSS_STREET_NME
        {
            get { return _TSUP_NEAREST_CROSS_STREET_NME; }
            set { _TSUP_NEAREST_CROSS_STREET_NME = value; }
        }

        private string _TSUP_PRS_QOT_NBR;

        public string TSUP_PRS_QOT_NBR
        {
            get { return _TSUP_PRS_QOT_NBR; }
            set { _TSUP_PRS_QOT_NBR = value; }
        }

        private string _TSUP_GCS_NBR;

        public string TSUP_GCS_NBR
        {
            get { return _TSUP_GCS_NBR; }
            set { _TSUP_GCS_NBR = value; }
        }

        private string _TSUP_RFQ_NBR;

        public string TSUP_RFQ_NBR
        {
            get { return _TSUP_RFQ_NBR; }
            set { _TSUP_RFQ_NBR = value; }
        }

        #endregion "Transport Install - Support Information Fields"

        #region "Transport Install - Transport Information Fields"

        private string _CKT_ID;

        public string CKT_ID
        {
            get { return _CKT_ID; }
            set { _CKT_ID = value; }
        }

        private string _TTRPT_ACCS_TYPE_CD;

        public string TTRPT_ACCS_TYPE_CD
        {
            get { return _TTRPT_ACCS_TYPE_CD; }
            set { _TTRPT_ACCS_TYPE_CD = value; }
        }

        private string _TTRPT_ACCS_TYPE_DES;

        public string TTRPT_ACCS_TYPE_DES
        {
            get { return _TTRPT_ACCS_TYPE_DES; }
            set { _TTRPT_ACCS_TYPE_DES = value; }
        }

        private string _TTRPT_ACCS_PRVDR_CD;

        public string TTRPT_ACCS_PRVDR_CD
        {
            get { return _TTRPT_ACCS_PRVDR_CD; }
            set { _TTRPT_ACCS_PRVDR_CD = value; }
        }

        private string _TTRPT_ACCS_ARNGT_CD;

        public string TTRPT_ACCS_ARNGT_CD
        {
            get { return _TTRPT_ACCS_ARNGT_CD; }
            set { _TTRPT_ACCS_ARNGT_CD = value; }
        }

        private string _TTRPT_ACCS_CNTRC_TERM_CD;

        public string TTRPT_ACCS_CNTRC_TERM_CD
        {
            get { return _TTRPT_ACCS_CNTRC_TERM_CD; }
            set { _TTRPT_ACCS_CNTRC_TERM_CD = value; }
        }

        private string _VNDO_CNTRC_TERM_ID;

        public string VNDO_CNTRC_TERM_ID
        {
            get { return _VNDO_CNTRC_TERM_ID; }
            set { _VNDO_CNTRC_TERM_ID = value; }
        }

        private string _TTRPT_SPD_OF_SRVC_BDWD_DES;

        public string TTRPT_SPD_OF_SRVC_BDWD_DES
        {
            get { return _TTRPT_SPD_OF_SRVC_BDWD_DES; }
            set { _TTRPT_SPD_OF_SRVC_BDWD_DES = value; }
        }

        private string _TTRPT_LINK_PRCOL_CD;

        public string TTRPT_LINK_PRCOL_CD
        {
            get { return _TTRPT_LINK_PRCOL_CD; }
            set { _TTRPT_LINK_PRCOL_CD = value; }
        }

        private string _TTRPT_ENCAP_CD;

        public string TTRPT_ENCAP_CD
        {
            get { return _TTRPT_ENCAP_CD; }
            set { _TTRPT_ENCAP_CD = value; }
        }

        private string _TTRPT_ROUTG_TYPE_CD;

        public string TTRPT_ROUTG_TYPE_CD
        {
            get { return _TTRPT_ROUTG_TYPE_CD; }
            set { _TTRPT_ROUTG_TYPE_CD = value; }
        }

        private string _TTRPT_MPLS_VPN_OVR_SPLK_CD;

        public string TTRPT_MPLS_VPN_OVR_SPLK_CD
        {
            get { return _TTRPT_MPLS_VPN_OVR_SPLK_CD; }
            set { _TTRPT_MPLS_VPN_OVR_SPLK_CD = value; }
        }

        private string _TTRPT_SRVC_TYPE_ID;

        public string TTRPT_SRVC_TYPE_ID
        {
            get { return _TTRPT_SRVC_TYPE_ID; }
            set { _TTRPT_SRVC_TYPE_ID = value; }
        }

        private string _TTRPT_FRMNG_DES;

        public string TTRPT_FRMNG_DES
        {
            get { return _TTRPT_FRMNG_DES; }
            set { _TTRPT_FRMNG_DES = value; }
        }

        private string _TTRPT_NCDE_DES;

        public string TTRPT_NCDE_DES
        {
            get { return _TTRPT_NCDE_DES; }
            set { _TTRPT_NCDE_DES = value; }
        }

        private string _TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD;

        public string TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD
        {
            get { return _TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD; }
            set { _TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD = value; }
        }

        private string _TTRPT_JACK_NRFC_TYPE_CD;

        public string TTRPT_JACK_NRFC_TYPE_CD
        {
            get { return _TTRPT_JACK_NRFC_TYPE_CD; }
            set { _TTRPT_JACK_NRFC_TYPE_CD = value; }
        }

        private string _TTRPT_STRATUM_LVL_CD;

        public string TTRPT_STRATUM_LVL_CD
        {
            get { return _TTRPT_STRATUM_LVL_CD; }
            set { _TTRPT_STRATUM_LVL_CD = value; }
        }

        private string _TTRPT_CLOCK_SRC_CD;

        public string TTRPT_CLOCK_SRC_CD
        {
            get { return _TTRPT_CLOCK_SRC_CD; }
            set { _TTRPT_CLOCK_SRC_CD = value; }
        }

        private string _TTRPT_IS_OCHRONOUS_CD;

        public string TTRPT_IS_OCHRONOUS_CD
        {
            get { return _TTRPT_IS_OCHRONOUS_CD; }
            set { _TTRPT_IS_OCHRONOUS_CD = value; }
        }

        private string _TTRPT_PRLEL_PRCS_TYPE_CD;

        public string TTRPT_PRLEL_PRCS_TYPE_CD
        {
            get { return _TTRPT_PRLEL_PRCS_TYPE_CD; }
            set { _TTRPT_PRLEL_PRCS_TYPE_CD = value; }
        }

        private string _TTRPT_PRLEL_PRCS_CKT_ID;

        public string TTRPT_PRLEL_PRCS_CKT_ID
        {
            get { return _TTRPT_PRLEL_PRCS_CKT_ID; }
            set { _TTRPT_PRLEL_PRCS_CKT_ID = value; }
        }

        private string _TTRPT_RELTD_FCTY_REUSE_CD;

        public string TTRPT_RELTD_FCTY_REUSE_CD
        {
            get { return _TTRPT_RELTD_FCTY_REUSE_CD; }
            set { _TTRPT_RELTD_FCTY_REUSE_CD = value; }
        }

        private string _TTRPT_RELTD_FCTY_CKT_ID;

        public string TTRPT_RELTD_FCTY_CKT_ID
        {
            get { return _TTRPT_RELTD_FCTY_CKT_ID; }
            set { _TTRPT_RELTD_FCTY_CKT_ID = value; }
        }

        private Int16 _TTRPT_TME_SOLT_CNT;

        public Int16 TTRPT_TME_SOLT_CNT
        {
            get { return _TTRPT_TME_SOLT_CNT; }
            set { _TTRPT_TME_SOLT_CNT = value; }
        }

        private Int16 _TTRPT_STRT_TME_SLOT_NBR;

        public Int16 TTRPT_STRT_TME_SLOT_NBR
        {
            get { return _TTRPT_STRT_TME_SLOT_NBR; }
            set { _TTRPT_STRT_TME_SLOT_NBR = value; }
        }

        private string _TTRPT_NW_ADR;

        public string TTRPT_NW_ADR
        {
            get { return _TTRPT_NW_ADR; }
            set { _TTRPT_NW_ADR = value; }
        }

        private string _TTRPT_SHRD_TNANT_CD;

        public string TTRPT_SHRD_TNANT_CD
        {
            get { return _TTRPT_SHRD_TNANT_CD; }
            set { _TTRPT_SHRD_TNANT_CD = value; }
        }

        private string _TTRPT_ALT_ACCS_CD;

        public string TTRPT_ALT_ACCS_CD
        {
            get { return _TTRPT_ALT_ACCS_CD; }
            set { _TTRPT_ALT_ACCS_CD = value; }
        }

        private string _TTRPT_ALT_ACCS_PRVDR_CD;

        public string TTRPT_ALT_ACCS_PRVDR_CD
        {
            get { return _TTRPT_ALT_ACCS_PRVDR_CD; }
            set { _TTRPT_ALT_ACCS_PRVDR_CD = value; }
        }

        private string _TTRPT_FBR_HAND_OFF_CD;

        public string TTRPT_FBR_HAND_OFF_CD
        {
            get { return _TTRPT_FBR_HAND_OFF_CD; }
            set { _TTRPT_FBR_HAND_OFF_CD = value; }
        }

        private string _TTRPT_MNGD_DATA_SRVC_CD;

        public string TTRPT_MNGD_DATA_SRVC_CD
        {
            get { return _TTRPT_MNGD_DATA_SRVC_CD; }
            set { _TTRPT_MNGD_DATA_SRVC_CD = value; }
        }

        private string _TTRPT_OSS_CD;

        public string TTRPT_OSS_CD
        {
            get { return _TTRPT_OSS_CD; }
            set { _TTRPT_OSS_CD = value; }
        }

        private string _TTRPT_MPLS_BACKHAUL_CD;

        public string TTRPT_MPLS_BACKHAUL_CD
        {
            get { return _TTRPT_MPLS_BACKHAUL_CD; }
            set { _TTRPT_MPLS_BACKHAUL_CD = value; }
        }

        private string _CXR_ACCS_CD;

        public string CXR_ACCS_CD
        {
            get { return _CXR_ACCS_CD; }
            set { _CXR_ACCS_CD = value; }
        }

        private string _INTL_PL_SRVC_TYPE_CD;

        public string INTL_PL_SRVC_TYPE_CD
        {
            get { return _INTL_PL_SRVC_TYPE_CD; }
            set { _INTL_PL_SRVC_TYPE_CD = value; }
        }

        private string _INTL_PL_CKT_TYPE_CD;

        public string INTL_PL_CKT_TYPE_CD
        {
            get { return _INTL_PL_CKT_TYPE_CD; }
            set { _INTL_PL_CKT_TYPE_CD = value; }
        }

        private string _INTL_PL_PLUS_IP_CD;

        public string INTL_PL_PLUS_IP_CD
        {
            get { return _INTL_PL_PLUS_IP_CD; }
            set { _INTL_PL_PLUS_IP_CD = value; }
        }

        private string _INTL_PL_CAT_ID;

        public string INTL_PL_CAT_ID
        {
            get { return _INTL_PL_CAT_ID; }
            set { _INTL_PL_CAT_ID = value; }
        }

        private string _OSS_OPT_CD;

        public string OSS_OPT_CD
        {
            get { return _OSS_OPT_CD; }
            set { _OSS_OPT_CD = value; }
        }

        #endregion "Transport Install - Transport Information Fields"

        #region "Transport Install - Port Information Fields"

        private string _TPORT_IP_VER_TYPE_CD;

        public string TPORT_IP_VER_TYPE_CD
        {
            get { return _TPORT_IP_VER_TYPE_CD; }
            set { _TPORT_IP_VER_TYPE_CD = value; }
        }

        private string _TPORT_IPV4_ADR_PRVDR_CD;

        public string TPORT_IPV4_ADR_PRVDR_CD
        {
            get { return _TPORT_IPV4_ADR_PRVDR_CD; }
            set { _TPORT_IPV4_ADR_PRVDR_CD = value; }
        }

        private string _TPORT_IPV4_ADR_QTY;

        public string TPORT_IPV4_ADR_QTY
        {
            get { return _TPORT_IPV4_ADR_QTY; }
            set { _TPORT_IPV4_ADR_QTY = value; }
        }

        private string _TPORT_IPV6_ADR_PRVDR_CD;

        public string TPORT_IPV6_ADR_PRVDR_CD
        {
            get { return _TPORT_IPV6_ADR_PRVDR_CD; }
            set { _TPORT_IPV6_ADR_PRVDR_CD = value; }
        }

        private string _TPORT_IPV6_ADR_QTY;

        public string TPORT_IPV6_ADR_QTY
        {
            get { return _TPORT_IPV6_ADR_QTY; }
            set { _TPORT_IPV6_ADR_QTY = value; }
        }

        private string _IPV4_SUBNET_MASK_ADR;

        public string IPV4_SUBNET_MASK_ADR
        {
            get { return _IPV4_SUBNET_MASK_ADR; }
            set { _IPV4_SUBNET_MASK_ADR = value; }
        }

        private string _TPORT_USER_TYPE_ID;

        public string TPORT_USER_TYPE_ID
        {
            get { return _TPORT_USER_TYPE_ID; }
            set { _TPORT_USER_TYPE_ID = value; }
        }

        private string _TPORT_MULTI_MEG_CD;

        public string TPORT_MULTI_MEG_CD
        {
            get { return _TPORT_MULTI_MEG_CD; }
            set { _TPORT_MULTI_MEG_CD = value; }
        }

        private string _SPAE_ACCS_INDCTR;

        public string SPAE_ACCS_INDCTR
        {
            get { return _SPAE_ACCS_INDCTR; }
            set { _SPAE_ACCS_INDCTR = value; }
        }

        private Int16 _TPORT_VLAN_QTY;

        public Int16 TPORT_VLAN_QTY
        {
            get { return _TPORT_VLAN_QTY; }
            set { _TPORT_VLAN_QTY = value; }
        }

        private string _TPORT_ETHRNT_NRFC_INDCR;

        public string TPORT_ETHRNT_NRFC_INDCR
        {
            get { return _TPORT_ETHRNT_NRFC_INDCR; }
            set { _TPORT_ETHRNT_NRFC_INDCR = value; }
        }

        private string _TPORT_SCRMB_TXT;

        public string TPORT_SCRMB_TXT
        {
            get { return _TPORT_SCRMB_TXT; }
            set { _TPORT_SCRMB_TXT = value; }
        }

        private string _TPORT_PORT_TYPE_ID;

        public string TPORT_PORT_TYPE_ID
        {
            get { return _TPORT_PORT_TYPE_ID; }
            set { _TPORT_PORT_TYPE_ID = value; }
        }

        private string _TPORT_CNCTR_TYPE_ID;

        public string TPORT_CNCTR_TYPE_ID
        {
            get { return _TPORT_CNCTR_TYPE_ID; }
            set { _TPORT_CNCTR_TYPE_ID = value; }
        }

        private string _TPORT_CUST_ROUTR_TAG_TXT;

        public string TPORT_CUST_ROUTR_TAG_TXT
        {
            get { return _TPORT_CUST_ROUTR_TAG_TXT; }
            set { _TPORT_CUST_ROUTR_TAG_TXT = value; }
        }

        private string _TPORT_CUST_ROUTR_AUTO_NEGOT_CD;

        public string TPORT_CUST_ROUTR_AUTO_NEGOT_CD
        {
            get { return _TPORT_CUST_ROUTR_AUTO_NEGOT_CD; }
            set { _TPORT_CUST_ROUTR_AUTO_NEGOT_CD = value; }
        }

        private string _TPORT_CUST_PRVDD_IP_ADR_INDCR;

        public string TPORT_CUST_PRVDD_IP_ADR_INDCR
        {
            get { return _TPORT_CUST_PRVDD_IP_ADR_INDCR; }
            set { _TPORT_CUST_PRVDD_IP_ADR_INDCR = value; }
        }

        private string _TPORT_CUST_IP_ADR;

        public string TPORT_CUST_IP_ADR
        {
            get { return _TPORT_CUST_IP_ADR; }
            set { _TPORT_CUST_IP_ADR = value; }
        }

        private string _TPORT_XST_IP_CNCTN_CD;

        public string TPORT_XST_IP_CNCTN_CD
        {
            get { return _TPORT_XST_IP_CNCTN_CD; }
            set { _TPORT_XST_IP_CNCTN_CD = value; }
        }

        private string _TPORT_CURR_INET_PRVDR_CD;

        public string TPORT_CURR_INET_PRVDR_CD
        {
            get { return _TPORT_CURR_INET_PRVDR_CD; }
            set { _TPORT_CURR_INET_PRVDR_CD = value; }
        }

        private string _TPORT_BRSTBL_PRICE_DISC_CD;

        public string TPORT_BRSTBL_PRICE_DISC_CD
        {
            get { return _TPORT_BRSTBL_PRICE_DISC_CD; }
            set { _TPORT_BRSTBL_PRICE_DISC_CD = value; }
        }

        private string _TPORT_BRSTBL_USAGE_TYPE_CD;

        public string TPORT_BRSTBL_USAGE_TYPE_CD
        {
            get { return _TPORT_BRSTBL_USAGE_TYPE_CD; }
            set { _TPORT_BRSTBL_USAGE_TYPE_CD = value; }
        }

        private string _TPORT_PPLVLS;

        public string TPORT_PPLVLS
        {
            get { return _TPORT_PPLVLS; }
            set { _TPORT_PPLVLS = value; }
        }

        private string _DMN_NME;

        public string DMN_NME
        {
            get { return _DMN_NME; }
            set { _DMN_NME = value; }
        }

        private string _ADDL_IP_ADR_CD;

        public string ADDL_IP_ADR_CD
        {
            get { return _ADDL_IP_ADR_CD; }
            set { _ADDL_IP_ADR_CD = value; }
        }

        private string _NRFC_PRCOL_CD;

        public string NRFC_PRCOL_CD
        {
            get { return _NRFC_PRCOL_CD; }
            set { _NRFC_PRCOL_CD = value; }
        }

        #endregion "Transport Install - Port Information Fields"

        #region "Disconnect Specific Information Fields"

        private string _DISC_REAS_CD;

        public string DISC_REAS_CD
        {
            get { return _DISC_REAS_CD; }
            set { _DISC_REAS_CD = value; }
        }

        private string _DISC_CNCL_BEF_STRT_REAS_CD;

        public string DISC_CNCL_BEF_STRT_REAS_CD
        {
            get { return _DISC_CNCL_BEF_STRT_REAS_CD; }
            set { _DISC_CNCL_BEF_STRT_REAS_CD = value; }
        }

        private string _DISC_CNCL_BEF_STRT_REAS_DETL_TXT;

        public string DISC_CNCL_BEF_STRT_REAS_DETL_TXT
        {
            get { return _DISC_CNCL_BEF_STRT_REAS_DETL_TXT; }
            set { _DISC_CNCL_BEF_STRT_REAS_DETL_TXT = value; }
        }

        #endregion "Disconnect Specific Information Fields"

        #region "Change Specific Information Fields"

        private string _ORDR_CHNG_DES;

        public string ORDR_CHNG_DES
        {
            get { return _ORDR_CHNG_DES; }
            set { _ORDR_CHNG_DES = value; }
        }

        private string _CXR_SRVC_ID;

        public string CXR_SRVC_ID
        {
            get { return _CXR_SRVC_ID; }
            set { _CXR_SRVC_ID = value; }
        }

        private string _CXR_CKT_ID;

        public string CXR_CKT_ID
        {
            get { return _CXR_CKT_ID; }
            set { _CXR_CKT_ID = value; }
        }

        private int _FSA_ORGNL_INSTL_ORDR_ID;

        public int FSA_ORGNL_INSTL_ORDR_ID
        {
            get { return _FSA_ORGNL_INSTL_ORDR_ID; }
            set { _FSA_ORGNL_INSTL_ORDR_ID = value; }
        }

        private string _PORT_RT_TYPE_CD;

        public string PORT_RT_TYPE_CD
        {
            get { return _PORT_RT_TYPE_CD; }
            set { _PORT_RT_TYPE_CD = value; }
        }

        private string _NW_USER_ADR;

        public string NW_USER_ADR
        {
            get { return _NW_USER_ADR; }
            set { _NW_USER_ADR = value; }
        }

        #endregion "Change Specific Information Fields"

        #region "Transport General Information Fields"

        //private string _ORANGE_CMNTY_NME;
        //public string ORANGE_CMNTY_NME
        //{
        //    get { return _ORANGE_CMNTY_NME; }
        //    set { _ORANGE_CMNTY_NME = value; }
        //}

        //private string _ORANGE_SITE_ID;
        //public string ORANGE_SITE_ID
        //{
        //    get { return _ORANGE_SITE_ID; }
        //    set { _ORANGE_SITE_ID = value; }
        //}

        private string _ORANGE_OFFR_CD;

        public string ORANGE_OFFR_CD
        {
            get { return _ORANGE_OFFR_CD; }
            set { _ORANGE_OFFR_CD = value; }
        }

        private string _CUSTOMER_FIREWALL_FLAG;

        public string CUSTOMER_FIREWALL_FLAG
        {
            get { return _CUSTOMER_FIREWALL_FLAG; }
            set { _CUSTOMER_FIREWALL_FLAG = value; }
        }

        #endregion "Transport General Information Fields"

        #region "IP Address Fields"

        private string _IP_ADR_TYPE_CD;

        public string IP_ADR_TYPE_CD
        {
            get { return _IP_ADR_TYPE_CD; }
            set { _IP_ADR_TYPE_CD = value; }
        }

        private string _IP_ADR;

        public string IP_ADR
        {
            get { return _IP_ADR; }
            set { _IP_ADR = value; }
        }

        #endregion "IP Address Fields"

        #region "Public Methods"

        public int GetFSAOrderID()
        {
            setCommand("dbo.getFSAOrderID");
            addIntParam("@ORDR_ID", _ORDR_ID, true);
            addIntParam("@ORDR_ACTN_ID", _ORDR_ACTN_ID);
            addStringParam("@FTN", _FTN);
            addSmallIntParam("@ORDR_CAT_ID", 2);
            return execNoQueryOut("@ORDR_ID", true);
        }

        public int DeletePartialFSAOrder()
        {
            setCommand("dbo.deletePartialFSAOrder");
            addIntParam("@ORDR_ID", _ORDR_ID);
            return execNoQuery();
        }

        public int InsertFSAOrder()
        {
            setCommand("dbo.insertFSAOrder");

            //Order Install Information Data & Parameters.
            addIntParam("@ORDR_ID", _ORDR_ID);
            addTinyIntParam("@ORDR_ACTN_ID", _ORDR_ACTN_ID);
            addStringParam("@FTN", _FTN);
            addStringParam("@ORDR_TYPE_CD", _ORDR_TYPE_CD);
            addStringParam("@ORDR_SUB_TYPE_CD", _ORDR_SUB_TYPE_CD);
            addStringParam("@PROD_TYPE_CD", _PROD_TYPE_CD);
            addStringParam("@PRNT_FTN", _PRNT_FTN);
            addStringParam("@RELTD_FTN", _RELTD_FTN);
            addStringParam("@SCA_NBR", _SCA_NBR);
            addStringParam("@TSP_CD", _TSP_CD);
            addStringParam("@CPW_TYPE", _CPW_TYPE);
            addDateParam("@CUST_CMMT_DT", _CUST_CMMT_DT);
            addDateParam("@CUST_WANT_DT", _CUST_WANT_DT);
            addDateParam("@CUST_ORDR_SBMT_DT", _CUST_ORDR_SBMT_DT);
            addDateParam("@CUST_SIGNED_DT", _CUST_SIGNED_DT);
            addDateParam("@ORDR_SBMT_DT", _ORDR_SBMT_DT);
            addStringParam("@CUST_PRMS_OCPY_CD", _CUST_PRMS_OCPY_CD);
            addStringParam("@CUST_ACPT_ERLY_SRVC_CD", _CUST_ACPT_ERLY_SRVC_CD);
            addStringParam("@MULT_CUST_ORDR_CD", _MULT_CUST_ORDR_CD);
            addStringParam("@GVRMNT_TYPE_ID", _GVRMNT_TYPE_ID);
            addStringParam("@SOTS_ID", _SOTS_ID);
            addStringParam("@CHARS_ID", _CHARS_ID);
            addStringParam("@FSA_EXP_TYPE_CD", _FSA_EXP_TYPE_CD);

            //MDS Install Information Data & Parameters.
            addStringParam("@INSTL_ESCL_CD", _INSTL_ESCL_CD);
            addStringParam("@INSTL_DSGN_DOC_NBR", _INSTL_DSGN_DOC_NBR);
            addStringParam("@INSTL_VNDR_CD", _INSTL_VNDR_CD);
            addStringParam("@INSTL_SOLU_SRVC_DES", _INSTL_SOLU_SRVC_DES);
            addStringParam("@INSTL_NW_TYPE_CD", _INSTL_NW_TYPE_CD);
            addStringParam("@INSTL_TRNSPRT_TYPE_CD", _INSTL_TRNSPRT_TYPE_CD);
            addTinyIntParam("@INSTL_SRVC_TIER_CD", _INSTL_SRVC_TIER_CD);

            //CPE Install Information Data & Parameters.
            addStringParam("@CPE_CPE_ORDR_TYPE_CD", _CPE_CPE_ORDR_TYPE_CD);
            addStringParam("@CPE_EQPT_ONLY_CD", _CPE_EQPT_ONLY_CD);
            addStringParam("@CPE_ACCS_PRVDR_CD", _CPE_ACCS_PRVDR_CD);
            addStringParam("@CPE_PHN_NBR", _CPE_PHN_NBR);
            addStringParam("@CPE_PHN_NBR_TYPE_CD", _CPE_PHN_NBR_TYPE_CD);
            addStringParam("@CPE_ECCKT_ID", _CPE_ECCKT_ID);
            addStringParam("@CPE_MSCP_CD", _CPE_MSCP_CD);
            addStringParam("@CPE_DLVRY_DUTY_ID", _CPE_DLVRY_DUTY_ID);
            addStringParam("@CPE_DLVRY_DUTY_AMT", _CPE_DLVRY_DUTY_AMT);
            addStringParam("@CPE_SHIP_CHG_AMT", _CPE_SHIP_CHG_AMT);
            addStringParam("@CPE_REC_ONLY_CD", _CPE_REC_ONLY_CD);

            //Transport Install - Support Information Data & Parameters.
            //Moving these four fields to ContactInfo section
            //addStringParam("@TSUP_SAC_AVLBLTY_HR_TXT", _TSUP_SAC_AVLBLTY_HR_TXT);
            //addStringParam("@TSUP_SAC_TME_ZONE_CD", _TSUP_SAC_TME_ZONE_CD);
            //addStringParam("@TSUP_SAC_INTPRTR_REQ_CD", _TSUP_SAC_INTPRTR_REQ_CD);
            //addStringParam("@TSUP_SAC_LANG_SUPPD_CD", _TSUP_SAC_LANG_SUPPD_CD);
            addStringParam("@TSUP_TELCO_DEMARC_BLDG_NME", _TSUP_TELCO_DEMARC_BLDG_NME);
            addStringParam("@TSUP_TELCO_DEMARC_FLR_ID", _TSUP_TELCO_DEMARC_FLR_ID);
            addStringParam("@TSUP_TELCO_DEMARC_RM_NBR", _TSUP_TELCO_DEMARC_RM_NBR);
            addStringParam("@TSUP_NEAREST_CROSS_STREET_NME", _TSUP_NEAREST_CROSS_STREET_NME);
            addStringParam("@TSUP_PRS_QOT_NBR", _TSUP_PRS_QOT_NBR);
            addStringParam("@TSUP_GCS_NBR", _TSUP_GCS_NBR);
            addStringParam("@TSUP_RFQ_NBR", _TSUP_RFQ_NBR);

            //Transport Install - Transport Information Data & Parameters.
            addStringParam("@TTRPT_ACCS_TYPE_CD", _TTRPT_ACCS_TYPE_CD);
            addStringParam("@TTRPT_ACCS_TYPE_DES", _TTRPT_ACCS_TYPE_DES);
            addStringParam("@TTRPT_ACCS_PRVDR_CD", _TTRPT_ACCS_PRVDR_CD);
            addStringParam("@TTRPT_ACCS_ARNGT_CD", _TTRPT_ACCS_ARNGT_CD);
            addStringParam("@TTRPT_ACCS_CNTRC_TERM_CD", _TTRPT_ACCS_CNTRC_TERM_CD);
            addStringParam("@TTRPT_SPD_OF_SRVC_BDWD_DES", _TTRPT_SPD_OF_SRVC_BDWD_DES);
            addStringParam("@TTRPT_LINK_PRCOL_CD", _TTRPT_LINK_PRCOL_CD);
            addStringParam("@TTRPT_ENCAP_CD", _TTRPT_ENCAP_CD);
            addStringParam("@TTRPT_ROUTG_TYPE_CD", _TTRPT_ROUTG_TYPE_CD);
            addStringParam("@TTRPT_MPLS_VPN_OVR_SPLK_CD", _TTRPT_MPLS_VPN_OVR_SPLK_CD);
            addStringParam("@TTRPT_SRVC_TYPE_ID", _TTRPT_SRVC_TYPE_ID);
            addStringParam("@TTRPT_FRMNG_DES", _TTRPT_FRMNG_DES);
            addStringParam("@TTRPT_NCDE_DES", _TTRPT_NCDE_DES);
            addStringParam("@TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD", _TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD);
            addStringParam("@TTRPT_JACK_NRFC_TYPE_CD", _TTRPT_JACK_NRFC_TYPE_CD);
            addStringParam("@TTRPT_STRATUM_LVL_CD", _TTRPT_STRATUM_LVL_CD);
            addStringParam("@TTRPT_CLOCK_SRC_CD", _TTRPT_CLOCK_SRC_CD);
            addStringParam("@TTRPT_IS_OCHRONOUS_CD", _TTRPT_IS_OCHRONOUS_CD);
            addStringParam("@TTRPT_PRLEL_PRCS_TYPE_CD", _TTRPT_PRLEL_PRCS_TYPE_CD);
            addStringParam("@TTRPT_PRLEL_PRCS_CKT_ID", _TTRPT_PRLEL_PRCS_CKT_ID);
            addStringParam("@TTRPT_RELTD_FCTY_REUSE_CD", _TTRPT_RELTD_FCTY_REUSE_CD);
            addStringParam("@TTRPT_RELTD_FCTY_CKT_ID", _TTRPT_RELTD_FCTY_CKT_ID);
            addSmallIntParam("@TTRPT_TME_SOLT_CNT", _TTRPT_TME_SOLT_CNT);
            addSmallIntParam("@TTRPT_STRT_TME_SLOT_NBR", _TTRPT_STRT_TME_SLOT_NBR);
            addStringParam("@TTRPT_NW_ADR", _TTRPT_NW_ADR);
            addStringParam("@TTRPT_SHRD_TNANT_CD", _TTRPT_SHRD_TNANT_CD);
            addStringParam("@TTRPT_ALT_ACCS_CD", _TTRPT_ALT_ACCS_CD);
            addStringParam("@TTRPT_ALT_ACCS_PRVDR_CD", _TTRPT_ALT_ACCS_PRVDR_CD);
            addStringParam("@TTRPT_FBR_HAND_OFF_CD", _TTRPT_FBR_HAND_OFF_CD);
            addStringParam("@TTRPT_MNGD_DATA_SRVC_CD", _TTRPT_MNGD_DATA_SRVC_CD);
            addStringParam("@TTRPT_OSS_CD", _TTRPT_OSS_CD);
            addStringParam("@TTRPT_MPLS_BACKHAUL_CD", _TTRPT_MPLS_BACKHAUL_CD);
            addStringParam("@CXR_ACCS_CD", _CXR_ACCS_CD);

            //Transport Install - Port Information Data & Parameters.
            addStringParam("@TPORT_IP_VER_TYPE_CD", _TPORT_IP_VER_TYPE_CD);
            addStringParam("@TPORT_IPV4_ADR_PRVDR_CD", _TPORT_IPV4_ADR_PRVDR_CD);
            addStringParam("@TPORT_IPV4_ADR_QTY", _TPORT_IPV4_ADR_QTY);
            addStringParam("@TPORT_IPV6_ADR_PRVDR_CD", _TPORT_IPV6_ADR_PRVDR_CD);
            addStringParam("@TPORT_IPV6_ADR_QTY", _TPORT_IPV6_ADR_QTY);
            addStringParam("@TPORT_USER_TYPE_ID", _TPORT_USER_TYPE_ID);
            addStringParam("@TPORT_MULTI_MEG_CD", _TPORT_MULTI_MEG_CD);
            addSmallIntParam("@TPORT_VLAN_QTY", _TPORT_VLAN_QTY);
            addStringParam("@TPORT_ETHRNT_NRFC_INDCR", _TPORT_ETHRNT_NRFC_INDCR);
            addStringParam("@TPORT_SCRMB_TXT", _TPORT_SCRMB_TXT);
            addStringParam("@TPORT_PORT_TYPE_ID", _TPORT_PORT_TYPE_ID);
            addStringParam("@TPORT_CNCTR_TYPE_ID", _TPORT_CNCTR_TYPE_ID);
            addStringParam("@TPORT_CUST_ROUTR_TAG_TXT", _TPORT_CUST_ROUTR_TAG_TXT);
            addStringParam("@TPORT_CUST_ROUTR_AUTO_NEGOT_CD", _TPORT_CUST_ROUTR_AUTO_NEGOT_CD);
            addStringParam("@TPORT_CUST_PRVDD_IP_ADR_INDCR", _TPORT_CUST_PRVDD_IP_ADR_INDCR);
            addStringParam("@TPORT_CUST_IP_ADR", _TPORT_CUST_IP_ADR);
            addStringParam("@TPORT_XST_IP_CNCTN_CD", _TPORT_XST_IP_CNCTN_CD);
            addStringParam("@TPORT_CURR_INET_PRVDR_CD", _TPORT_CURR_INET_PRVDR_CD);
            addStringParam("@TPORT_BRSTBL_PRICE_DISC_CD", _TPORT_BRSTBL_PRICE_DISC_CD);
            addStringParam("@TPORT_BRSTBL_USAGE_TYPE_CD", _TPORT_BRSTBL_USAGE_TYPE_CD);
            addStringParam("@TPORT_PPLVLS", _TPORT_PPLVLS);

            //Disconnect Specific Information Data & Parameters.
            addStringParam("@DISC_REAS_CD", _DISC_REAS_CD);
            addStringParam("@DISC_CNCL_BEF_STRT_REAS_CD", _DISC_CNCL_BEF_STRT_REAS_CD);
            addStringParam("@DISC_CNCL_BEF_STRT_REAS_DETL_TXT", _DISC_CNCL_BEF_STRT_REAS_DETL_TXT);

            addStringParam("@INTL_PL_SRVC_TYPE_CD", _INTL_PL_SRVC_TYPE_CD);
            addStringParam("@INTL_PL_CKT_TYPE_CD", _INTL_PL_CKT_TYPE_CD);
            addStringParam("@INTL_PL_PLUS_IP_CD", _INTL_PL_PLUS_IP_CD);
            addStringParam("@INTL_PL_CAT_ID", _INTL_PL_CAT_ID);
            addStringParam("@OSS_OPT_CD", _OSS_OPT_CD);

            //CR 96 Parameters.
            addIntParam("@MULT_ORDR_INDEX_NBR", _MULT_ORDR_INDEX_NBR);
            addIntParam("@MULT_ORDR_TOT_CNT", _MULT_ORDR_TOT_CNT);
            addStringParam("@VNDR_VPN_CD", _VNDR_VPN_CD);

            addStringParam("@ORDR_CHNG_DES", _ORDR_CHNG_DES);
            addStringParam("@CXR_SRVC_ID", _CXR_SRVC_ID);
            addStringParam("@CXR_CKT_ID", _CXR_CKT_ID);
            addIntParam("@FSA_ORGNL_INSTL_ORDR_ID", _FSA_ORGNL_INSTL_ORDR_ID);
            addStringParam("@PORT_RT_TYPE_CD", _PORT_RT_TYPE_CD);
            addStringParam("@NW_USER_ADR", _NW_USER_ADR);

            //addStringParam("@ORANGE_CMNTY_NME", _ORANGE_CMNTY_NME);
            //addStringParam("@ORANGE_SITE_ID", _ORANGE_SITE_ID);
            addStringParam("@ORANGE_OFFR_CD", _ORANGE_OFFR_CD);
            addStringParam("@CUSTOMER_FIREWALL_CD", _CUSTOMER_FIREWALL_FLAG);
            addStringParam("@CKT_ID", _CKT_ID);
            addStringParam("@VNDO_CNTRC_TERM_ID", _VNDO_CNTRC_TERM_ID);
            addStringParam("@DMN_NME", _DMN_NME);
            addStringParam("@ADDL_IP_ADR_CD", _ADDL_IP_ADR_CD);
            addStringParam("@NRFC_PRCOL_CD", _NRFC_PRCOL_CD);
            addStringParam("@INSTL_SRVC_TIER_DES", _INSTL_SRVC_TIER_DES);
            addStringParam("@IPV4_SUBNET_MASK_ADR", _IPV4_SUBNET_MASK_ADR);
            addStringParam("@SPAE_ACCS_INDCTR", _SPAE_ACCS_INDCTR);

            return execNoQuery();
        }

        public int InsertFSAIPAddress()
        {
            setCommand("dbo.insertFSAIPAddress");
            addIntParam("@ORDR_ID", _ORDR_ID);
            addStringParam("@IP_ADR_TYPE_CD", _IP_ADR_TYPE_CD);
            addStringParam("@IP_ADR", _IP_ADR);
            return execNoQuery();
        }

        public DataTable GetFSAResponses()
        {
            setCommand("dbo.getFSAResponses");
            return getDataTable();
        }

        public int UpdateFSAResponseByOrderID(int _iFTN, int _Status)
        {
            setCommand("dbo.updateFSAResponseByOrderID");
            addIntParam("@ORDR_ID", _iFTN);
            addIntParam("@STUS_ID", _Status);
            return execNoQuery();
        }

        public int LoadInitialTask()
        {
            setCommand("dbo.insertInitialTask");
            addIntParam("@OrderID", _ORDR_ID);
            addIntParam("@CategoryID", 2);
            return execNoQuery();
        }

        public int InsertOrderVLAN(string _VLAN_ID, string _VLAN_PCT_QTY)
        {
            setCommand("dbo.insertOrderVLAN");
            addIntParam("@ORDR_ID", _ORDR_ID);
            addStringParam("@VLAN_ID", _VLAN_ID);
            addStringParam("@VLAN_SRC_NME", "FSA");
            addStringParam("@VLAN_PCT_QTY", _VLAN_PCT_QTY);
            addIntParam("@CREAT_BY_USER_ID", 1);

            return execNoQuery();
        }

        #endregion "Public Methods"
    }
}