################################################################################
#
# Copyright (c) 2011 Sprint Communications Company
#
# The content of this document is PROPRIETARY INFORMATION, and is the sole
# and exclusive property of Sprint. Information provided in this document is
# classified as SPRINT RESTRICTED. Originator must grant permission before
# reproductions of this document can be made. If you need to dispose of any
# hard copies of this document, please shred it.
#
################################################################################
#
# This is the application configuration file. All configuration settings for the
# application are consolidated into this one place, and the various config files
# are initialized from values in this configuration. This permits the app to be
# environment-neutral. 
#
# Configuration settings are key/value pairs. The key is a unique identifier. 
# Values are text strings. Values may contain nested config settings. For example
# if you have defined a config setting named XYZ you can use it in another config
# setting (ex: ABC = 123.%XYZ%.456).
#
# It is recommended to use the forward slash when possible in file paths. Note 
# that backslashes must be escaped (ex: c:\\bea) if they are used.  
#
# Each application has a master configuration file. This config file has all the
# settings and has default values where appropriate. The master config file is
# used to validate the local config file. If there are settings in the master 
# config file that aren't in the local config file the configuration will fail. 
# Whenever the application adds a new configuration setting it should always be
# added to the master config file.
#
# There is script to validate a local configuration against the master. It will 
# report any inconsistencies it finds.
#
# There is another script to update a local configuration from the master. If 
# the local configuration needs updating the current configuration will be saved
# and annotated with the deltas and a new configuration will be written with the
# updates from the master. 
# 
################################################################################


################################################################################
#
# The following settings are general settings for all components.
#
################################################################################
# location where Java is installed (where bin/java or bin\\java.exe is expected)
JAVA_HOME = E:\\Program Files (x86)\\Java\\jre1.8.0_201
# writable folder for writing log files
LOGFILE_HOME = E:\\BF2\\QDA\\Logs\\COWS
SERVICES_HOME = E:\\BF2\\QDA\\Application\\COWS\\Services
WEB_HOME = E:\\BF2\\QDA\\WEB
WEBSITE_HOME = https://cowsbreakfix.test.sprint.com
#Data files folder
DATA_HOME = E:\\BF2\\QDA\\Data\\COWS
# environment name for performing environment checks etc (ex: PROD, PRODSUPT, DEV1, etc)
ENV_NAME = BREAKFIX
# environment-specific prefix for service names (usually %ENV_NAME%_ when used)
SVC_ENV_PREFIX = %ENV_NAME%_

# dirs/files (if any) to persist during deploy process (space-delimited list in ant format)
DEPLOY_PERSISTENT_PATHS = Logs/** Data/** Application/COWS/Services Application/COWS Web/* Web/COWS/aspnet_client/** Application/COWS/QDAUSR/mqs** Application/COWS/Services/COWS.ODIESenderService Application/COWS/Services/COWS.ODIEReceiverService Application/COWS/Services/COWS.FSAService Application/COWS/Services/COWS.EmailSender Application/COWS/Services/COWS.TadpoleService Application/COWS/Services/COWS.RecurringApptParser
Application/COWS/Services/COWS.Mach5SenderService Application/COWS/Services/COWS.PeopleSoftBatchService Application/COWS/Services/COWS.RulesetProcessor Application/COWS/Services/COWS.SSTATSenderService Application/COWS/Services/COWS.SSTATReceiverService
Application/COWS/Services/COWS.AutoReportingDx


# list of dirs/files (if any) to ignore when backing up current build
BACKUP_IGNORE_PATHS = Logs/** Data/** Web/COWS/aspnet_client/**

# MQ connectivity setttings
DEFAULT_MQ_CHANNEL = QDA.CLIENT
DEFAULT_MQ_HOST_NAME = TREAA642.test.sprint.com (1417)
DEFAULT_MQ_QUEUE_MANAGER = BCL02ABF

# IIS App pool name
COWS_APP_POOL_NAME = COWSBreakfix

################################################################################
#
# Application Wide Settings
#
################################################################################
# For log_cleaner.cmd Script.
LOGS_DAYS_TO_PURGE = 60

################################################################################
#
# The following settings are for the COWS SNAPSHOT Jobs.
#
################################################################################
 
DB_JOB_CONNECT_STRING = 
DB_JOB_START_READONLY = WSPS-COWS-CreateSnapshot
DB_JOB_STOP_READONLY = WSPS-COWS-DropSnapshot

################################################################################
#
# The following settings are for the Certificate Monitor Jobs.
#
################################################################################

CERT_LOCATION = E:\\BF2\\QDA\\Data\\COWS\\App_Data


################################################################################
#
# The following settings are for the Staging Folder Monitor Jobs.
#
################################################################################
DEPLOY_EMAIL_ADDRESS = "WholesaleServiceProductionSupport-DeploymentTeam@sprint.com"

################################################################################
#
# The following settings are for the COWS web app.
#
################################################################################
COWS_DB_CONNECT_STRING = Data Source=TVMXD972\\MS2017_TEST, 2787;Initial Catalog=COWS;Trusted_Connection=true;MultipleActiveResultSets=True;Connect Timeout=600;" providerName="System.Data.SqlClient
COWS_CONNECT_STRING = Data Source=TVMXD972\\MS2017_TEST, 2787;Initial Catalog=COWS;Trusted_Connection=true;MultipleActiveResultSets=True;Connect Timeout=600;" providerName="System.Data.SqlClient
COWS_SVCS_CONNECT_STRING = data source=.\\SQLEXPRESS;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\\aspnetdb.mdf;User Instance=true
COWS_APP_SVCS_CONNECT_STRING = data source=.\\SQLEXPRESS;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\\aspnetdb.mdf;User Instance=true
COWS_REPORTING_CONNECT_STRING = Data Source= TVMXD969\\MS2017_TEST,2778;Initial Catalog=COWS_Reporting;Trusted_Connection=true;Connect Timeout=600;MultipleActiveResultSets=True
COWS_ON_REPORTING_CONNECT_STRING = Data Source=TVMXD972\\MS2017_TEST, 2787;Initial Catalog=COWS;Trusted_Connection=true;Connect Timeout=600;MultipleActiveResultSets=True;
COWS_CACHE_TIMEOUT = 120
COWS_CALENDAR_ENTRY_URL = %WEBSITE_HOME%/event/GenerateICS
COWS_VIEW_EVENT_URL = %WEBSITE_HOME%/event/
COWS_MAIL_SERVER_NAME = mailhost.corp.sprint.com
COWS_DEFAULT_FROM_EMAIL = COWSBreakFix@T-Mobile.com
COWS_ADHOC_REPORT_NOTIFY = %WEB_HOME%\\COWS\\xslt\\AdhocReportNotify.xslt
COWS_EVENT_MAIL = %WEB_HOME%\\COWS\\xslt\\EventMail.xslt
COWS_MDS_SHIPPED_EMAIL = %WEB_HOME%\\COWS\\xslt\\MDSShippedEmail.xslt
COWS_RETRACT_MAIL = %WEB_HOME%\\COWS\\xslt\\RetractMail.xslt
COWS_CPEORDR_XSLT = %WEB_HOME%\\COWS\\xslt\\CPEOrderDetPopup.xslt
COWS_DESIGNDOC_XSLT = %WEB_HOME%\\COWS\\xslt\\DesignDocDetPopup.xslt

COWS_LOG_APP_DIR = %LOGFILE_HOME%\\COWSWebGUI
COWS_LOG_DIRECTORY_LIST = %LOGFILE_HOME%, %LOGFILE_HOME%\\WFM
COWS_DETAIL_LOG = 0
COWS_ERROR_MAIL = jagannath.r.gangi@sprint.com
COWS_MDSRedesignLkp = 1
COWS_REJECT_MAIL = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
COWS_FEDLINE_REJECT_MAIL = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
COWS_REGION = BREAKFIX

COWS_OSXslt = %WEB_HOME%\\COWS\\xslt\\OpenSlotsDisplay.xslt

COWS_PAGESIZE =20
COWS_EXPORT_FOLDER = %DATA_HOME%\\ExportData
COWS_VENDORCERTOrange = %CERT_LOCATION%\\Orange.cer
COWS_Orange_EOC = 2099,12,31
COWS_VENDORCERTNAVEGA = %CERT_LOCATION%\\G. Mebius.cer
COWS_NAVEGA_EOC = 2099,12,31
COWS_EOCPDL = cableenvironment-internal@sprint.com
COWS_VNDRENCRYPTFLG = 0
COWS_CHARTIMAGE_HANDLER = storage=memory;timeout=20;deleteAfterServicing=false;
COWS_MSMGR_FILE_NAME = \\MNSMsMgrEsclReport3.xlsx
COWS_CPEMNS_FILE_NAME = \\CSC_CPE_MNS_Orders.xlsx

COWS_BLOCK_UI = false
COWS_BLOCK_UI_MESSAGE = COWS\ Read\ Only\ -\ Maintenance\ Mode
COWS_BLOCKED_IDS = btnSave,btnSave2,lkbDelete,btnDelete,btnCalculate,btnUpdate,btnUpdateSched,btnAddADID,btnUpdateRoles,btnAddAssignment,lkbDelete_Click,btnAlterMan,lbAdd,lbRemove,btnNewEvent,btnEditView,btnOk,btnAdd,btnLookupFTN,lkbRemove,btnCheckCurrentSlot,btnLaunchPicker,imgbtnClear,btnPplList,btnCheckPpl,btnLookUp,btnRDInfo,btnMDAdd,btnOEAdd,btnADInfo,btnMDAddData,btnWTAdd,btnSlotPicker,btnActivatorPicker,lkbOERemove,lkbWTRemove,btnDeleteFolder,btnEditFolder,btnSubmit,btnH5New,btnACCIADD,btnVenOrdNew,btnAttach,btnADDCM,btnVLANAddt,btnHoldUnHold,btnCIADD,btnSaveChanges,btnCalculateftr,btnMove,btnComplete,btnRTS,btnRTSBottom,btnACRComplete,btnACRComplete2,btngrvAssign,btngrvUnAssign,btnAllSlots,btnUploadFiles,btnCreateOrder,btnUpdateOrder,btnUploadForm,btnAddEmail,ibDelete,btnSOWGo,lbEventHist,UploadForm,imgGeneralReset,imgGeneralSave,fuAttachment1,fuAttachment2,fuAttachment3,fuAttachment4,fuAttachment5,btnAttachEmailDoc,btnAddVendorForm,btnSendEmail,btnSaveEmail,ibDeleteAttch,btnSaveAck,ContentPlaceHolder1_pnlUpload,imbDelete

COWS_ASPNET_MAXHTTPCOLLECTIONKEYS = 10000

COWS_XSD_PATH = %WEB_HOME%\\COWS\\App_Data\\Tocows.xsd
COWS_ARCHIVED_REDESIGNS = \\\\pvmx0668\\prod_e$\\QDA\\Data\\COWS\\Conversion\\Redesign\\Old Redesign Data.xlsx
COWS_VIEW_CPT_URL = %WEBSITE_HOME%/cpt/cpt-form/
COWS_VIEW_CPE_CLLI_URL = %WEBSITE_HOME%/tools/cpe-clli/
COWS_SIPT_DOC_URL = %WEBSITE_HOME%/doc-download/

    
################################################################################
#
# The following settings are for the COWS Odie Sender windows service.
#
################################################################################
ODIE_SENDER_SVC_STARTUP_TYPE = Automatic
ODIE_SENDER_SVC_MQ_HOST = %DEFAULT_MQ_HOST_NAME%
ODIE_SENDER_SVC_MQ_QUEUE_MGR = %DEFAULT_MQ_QUEUE_MANAGER%
ODIE_SENDER_SVC_MQ_CHANNEL = %DEFAULT_MQ_CHANNEL%
ODIE_SENDER_SVC_MQ_QUEUE_IN = 
ODIE_SENDER_SVC_MQ_QUEUE_OUT = OXP.REQUEST
ODIE_SENDER_SVC_MQ_CHARSET = 1208
ODIE_SENDER_SVC_MQ_FORMAT = MQSTR\ \ \ 
ODIE_SENDER_SVC_MQ_WAIT_INTERVAL = 2000
ODIE_SENDER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.ODIESenderService
ODIE_SENDER_SVC_LOG_USER = 
ODIE_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
ODIE_SENDER_SVC_LOG_TIMER = 180000
ODIE_SENDER_SVC_LOG_TYPE = Detail
ODIE_SENDER_SVC_LOG_QUEUE_SIZE = 200
ODIE_SENDER_SVC_LOG_XSL = 
ODIE_SENDER_SVC_LOG_NAME_LIST = Main
ODIE_SENDER_SVC_LOG_MODE = Text
ODIE_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

    
################################################################################
#
# The following settings are for the COWS Odie Receiver windows service.
#
################################################################################
ODIE_RECEIVER_SVC_STARTUP_TYPE = Automatic
ODIE_RECEIVER_SVC_MQ_HOST = %DEFAULT_MQ_HOST_NAME%
ODIE_RECEIVER_SVC_MQ_QUEUE_MGR = %DEFAULT_MQ_QUEUE_MANAGER%
ODIE_RECEIVER_SVC_MQ_CHANNEL = %DEFAULT_MQ_CHANNEL%
ODIE_RECEIVER_SVC_MQ_QUEUE_IN = QDA.REPLY01
ODIE_RECEIVER_SVC_MQ_QUEUE_OUT = 
ODIE_RECEIVER_SVC_MQ_CHARSET = 1208
ODIE_RECEIVER_SVC_MQ_FORMAT = MQSTR\ \ \ 
ODIE_RECEIVER_SVC_MQ_WAIT_INTERVAL = 2000
ODIE_RECEIVER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.ODIEReceiverService
ODIE_RECEIVER_SVC_LOG_USER = 
ODIE_RECEIVER_SVC_LOG_QUEUE_GROW_FACTOR = 4
ODIE_RECEIVER_SVC_LOG_TIMER = 180000
ODIE_RECEIVER_SVC_LOG_TYPE = Detail
ODIE_RECEIVER_SVC_LOG_QUEUE_SIZE = 200
ODIE_RECEIVER_SVC_LOG_XSL = 
ODIE_RECEIVER_SVC_LOG_NAME_LIST = Main
ODIE_RECEIVER_SVC_LOG_MODE = Text
ODIE_RECEIVER_SVC_LOG_QUEUE_FLUSH_SIZE = 200


################################################################################
#
# The following settings are for the COWS EmailSender windows service.
#
################################################################################
Email_Sender_SVC_STARTUP_TYPE = Automatic
Email_Sender_SVC_SleepTime = 2000
Email_Sender_SVC_XSLTPath = %SERVICES_HOME%\\COWS.EmailSender\\EmailFormat.xslt
Email_Sender_SVC_MDSXLST = %SERVICES_HOME%\\COWS.EmailSender\\MDSEventEmail.xslt
Email_Sender_SVC_UCaaSEmail = %SERVICES_HOME%\\COWS.EmailSender\\UCaaSEventEmail.xslt
Email_Sender_SVC_MDSFTEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSFTEmail.xslt
Email_Sender_SVC_MDSCPEFTEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSCPEEmail.xslt
Email_Sender_SVC_MDSCPEEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSCPEEmail.xslt
Email_Sender_SVC_CPEFTEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSCPEFTEmail.xslt
Email_Sender_SVC_Server_Name = mailhost.corp.sprint.com
Email_Sender_SVC_DefaultFromEmail = COWSBreakfix@T-Mobile.com
Email_Sender_SVC_CPEEmail = %SERVICES_HOME%\\COWS.EmailSender\\CPEEmail.xslt
Email_Sender_SVC_MDSRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSRetractEmail.xslt
Email_Sender_SVC_UCaaSCPEEmail = %SERVICES_HOME%\\COWS.EmailSender\\UCaaSCPEEmail.xslt
Email_Sender_SVC_UCaaSRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\UCaaSRetractEmail.xslt
Email_Sender_SVC_CalenderEntryURL = %WEBSITE_HOME%/event/GenerateICS
Email_Sender_SVC_ViewEventURL = %WEBSITE_HOME%/event/
Email_Sender_SVC_ReworkEmail = %SERVICES_HOME%\\COWS.EmailSender\\ReworkEmail.xslt
Email_Sender_SVC_CANDEventEmail = %SERVICES_HOME%\\COWS.EmailSender\\CANDEventEmail.xslt
Email_Sender_SVC_CANDRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\CANDRetractEmail.xslt
Email_Sender_SVC_SIPTEventEmail = %SERVICES_HOME%\\COWS.EmailSender\\SIPTEventEmail.xslt
Email_Sender_SVC_SIPTRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\SIPTRetractEmail.xslt
Email_Sender_SVC_MDSShippedEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSShippedEmail.xslt
Email_Sender_SVC_GomPLCompEmail = %SERVICES_HOME%\\COWS.EmailSender\\GomPLCompEmail.xslt
Email_Sender_SVC_GomPLCompPDL = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
Email_Sender_SVC_GomPLCompPDLcc = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
Email_Sender_SVC_GOM_IPLIPASR = %SERVICES_HOME%\\COWS.EmailSender\\GomIPLIPASR.xslt
Email_Sender_SVC_CustIDUserAssignEmail = %SERVICES_HOME%\\COWS.EmailSender\\CustIDUserAssignEmail.xslt
Email_Sender_SVC_FedlineEmail = %SERVICES_HOME%\\COWS.EmailSender\\FedlineEmail.xslt
Email_Sender_SVC_NCCOOrderEmail = %SERVICES_HOME%\\COWS.EmailSender\\NCCOOrderEmail.xslt
Email_Sender_SVC_AdhocReportNotify = %SERVICES_HOME%\\COWS.EmailSender\\AdhocReportNotify.xslt
Email_Sender_SVC_EmailThreadActive = 1
Email_Sender_SVC_EOCThreadActive = 1
Email_Sender_SVC_ReworkThreadActive = 1
Email_Sender_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.EmailSender
Email_Sender_SVC_LOG_USER = 
Email_Sender_SVC_LOG_QUEUE_GROW_FACTOR = 4
Email_Sender_SVC_LOG_TIMER = 180000
Email_Sender_SVC_LOG_TYPE = Detail
Email_Sender_SVC_LOG_QUEUE_SIZE = 200
Email_Sender_SVC_LOG_XSL = 
Email_Sender_SVC_LOG_NAME_LIST = Main
Email_Sender_SVC_LOG_MODE = Xml
Email_Sender_SVC_LOG_QUEUE_FLUSH_SIZE = 200
Email_Sender_SVC_CancelEmailCCList = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
Email_Sender_SVC_MDSSrvcActvnEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSSrvcActvnEmail.xslt
Email_Sender_SVC_MDSSrvcCommentsEmail = %SERVICES_HOME%\\COWS.EmailSender\\MDSSrvcCommentsEmail.xslt

Email_Sender_SVC_ASREmail = %SERVICES_HOME%\\COWS.EmailSender\\ASR.xslt
Email_Sender_SVC_ASREmailTo = davidphillips1001@gmail.com
Email_Sender_SVC_ASREmailToDmstc = david.l2.phillips@sprint.com	
Email_Sender_SVC_ASREmailCC = david.l2.phillips@sprint.com
Email_Sender_SVC_RedesignEmail = %SERVICES_HOME%\\COWS.EmailSender\\RedesignEmail.xslt
Email_Sender_SVC_RedesignURL = %WEBSITE_HOME%/redesign/update-redesign/
Email_Sender_SVC_CPTNotificationEmail =  %SERVICES_HOME%\\COWS.EmailSender\\CPTNotificationEmail.xslt
Email_Sender_SVC_CPTCompleteEmail = %SERVICES_HOME%\\COWS.EmailSender\\CPTCompleteEmail.xslt
Email_Sender_SVC_FedlineReorderNtfyEmail = %SERVICES_HOME%\\COWS.EmailSender\\FedlineReorderNtfyEmail.xslt
Email_Sender_SVC_FedlineReorderEmailTo =  ETS-TFB-Wireline-COWS-Support@T-Mobile.com
Email_Sender_SVC_FedlineReorderEmailCC = 
Email_Sender_SVC_SDEOpportunityEmail = %SERVICES_HOME%\\COWS.EmailSender\\SDEOpportunityEmail.xslt
Email_Sender_SVC_CLLIRequestEmail = %SERVICES_HOME%\\COWS.EmailSender\\CLLIRequestEmail.xslt
Email_Sender_SVC_ViewSDEUrl = %WEBSITE_HOME%/mss/mss-new-engagement
Email_Sender_SVC_DownloadSDEURL = %WEBSITE_HOME%/doc-download/
Email_Sender_SVC_ZscalerDiscEmail = %SERVICES_HOME%\\COWS.EmailSender\\ZscalerDiscEmail.xslt
Email_Sender_SVC_CradlePointShippedEmail = %SERVICES_HOME%\\COWS.EmailSender\\CradlePointShippedEmail.xslt
Email_Sender_SVC_ISInitiateBillOnlyEmail = %SERVICES_HOME%\\COWS.EmailSender\\ISInitiateBillOnlyEmail.xslt

################################################################################
#
# The following settings are for the COWS Tadpole windows service.
#
################################################################################
TADPOLE_SVC_RECEIVER_SLEEP_TIME = 5000
TADPOLE_SVC_SENDER_SLEEP_TIME = 60000
TADPOLE_SVC_XSD_PATH = %SERVICES_HOME%\\COWS.TadpoleService\\Tocows.xsd
TADPOLE_SVC_DEFAULT_FROM_EMAIL = COWSTadpoleBF@sprint.com
TADPOLE_SSL_KEY_REPOSITORY = 
TADPOLE_SSL_CIPHER_SPEC = 
TADPOLE_SVC_MQ_HOST_NAME = TREAA640.test.sprint.com \ (1417)
TADPOLE_SVC_MQ_QUEUE_MANAGER = BCL02A2T
TADPOLE_SVC_MQ_CHANNEL = %DEFAULT_MQ_CHANNEL%
TADPOLE_SVC_MQ_QUEUE_IN = QDA.REQUEST.ZA9
TADPOLE_SVC_MQ_QUEUE_OUT = ZA9.REPLY2T.QDA.TEST
TADPOLE_SVC_WAIT_INTERVAL = 2000
TADPOLE_SVC_CHARACTER_SET = 1208
TADPOLE_SVC_FORMAT = MQSTR\ \ \ 
TADPOLE_SVC_DETAIL_LOG = 0
TADPOLE_SVC_LOG_DIRECTORY_LIST = %LOGFILE_HOME%\\COWS.TadpoleService\\Main,%LOGFILE_HOME%\\COWS.TadpoleService\\Sender,%LOGFILE_HOME%\\COWS.TadpoleService\\Receiver
TADPOLE_SVC_LOG_USER = 
TADPOLE_SVC_LOG_QUEUE_GROW_FACTOR = 4
TADPOLE_SVC_LOG_TIMER = 180000
TADPOLE_SVC_LOG_TYPE = Detail
TADPOLE_SVC_LOG_QUEUE_SIZE = 200
TADPOLE_SVC_LOG_XSL = 
TADPOLE_SVC_LOG_NAME_LIST = Main
TADPOLE_SVC_LOG_MODE = Text
TADPOLE_SVC_LOG_QUEUE_FLUSHSIZE = 200
TADPOLE_SVC_ERROR_EMAIL_LIST = David.L2.Phillips@sprint.com
TADPOLE_SVC_SERVER_NAME = mailhost.corp.sprint.com

################################################################################
#
# The following settings are for the COWS RecurringApptParser windows service.
#
################################################################################
RECURRING_APPT_PARSER_SVC_SLEEP_TIME = 500
RECURRING_APPT_PARSER_SVC_SERVER_NAME = mailhost.corp.sprint.com
RECURRING_APPT_PARSER_SVC_DEFAULT_FROM_EMAIL = COWSBREAKFIX@sprint.com
RECURRING_APPT_PARSER_SVC_COWS_REGION = BREAKFIX
RECURRING_APPT_PARSER_SVC_LOG_DIRECTORY_LIST = %LOGFILE_HOME%\\COWS.RecurringApptParser
RECURRING_APPT_PARSER_SVC_LOG_USER = 
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_GROW_FACTOR = 4
RECURRING_APPT_PARSER_SVC_LOG_TIMER = 180000
RECURRING_APPT_PARSER_SVC_LOG_TYPE = Detail
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_SIZE = 200
RECURRING_APPT_PARSER_SVC_LOG_XSL = 
RECURRING_APPT_PARSER_SVC_LOG_NAME_LIST = Main
RECURRING_APPT_PARSER_SVC_LOG_MODE = Xml
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

################################################################################
#
# The following settings are for the COWS AutoReportingDx windows service.
#
################################################################################
AUTO_REPORTING_DX_SVC_ClientSettingsProvider_ServiceURI = 
AUTO_REPORTING_DX_SVC_TimerCycle = 1
AUTO_REPORTING_DX_SVC_HeartbeatCycle = 1
AUTO_REPORTING_DX_SVC_LogFileName = COWS.AutoReportingDx_YYYYMMDD.log
AUTO_REPORTING_DX_SVC_LogFileDelAfterDays = 45
AUTO_REPORTING_DX_SVC_LogFileWidth = 120
AUTO_REPORTING_DX_SVC_DataCurrMins = 45
AUTO_REPORTING_DX_SVC_SMTPHost = mailhost.corp.sprint.com
AUTO_REPORTING_DX_SVC_SMTPPort = 587
AUTO_REPORTING_DX_SVC_SMTPUID = $cors
AUTO_REPORTING_DX_SVC_SMTPPassword = eBC3jrj9Bah?k2u
AUTO_REPORTING_DX_SVC_TempFilePath = %DATA_HOME%\\Reports\\temp

AUTO_REPORTING_DX_SVC_ENVIRONMENT = T
AUTO_REPORTING_DX_SVC_APP_FILE_SERVER = TVMXD968
AUTO_REPORTING_DX_SVC_LOG_FILE_PATH = %LOGFILE_HOME%\\COWS.AutoReportingDx\\
AUTO_REPORTING_DX_SVC_LOG_FILE_PATH_ALT =
AUTO_REPORTING_DX_SVC_SQL_SERVER = TVMXD969.TEST.SPRINT.COM,2778
AUTO_REPORTING_DX_SVC_SQL_CONN =Server=TVMXD969.TEST.SPRINT.COM,2778;Database=RAD_Reporting;Trusted_Connection=True;Connect Timeout=600;
AUTO_REPORTING_DX_SVC_SQLOLEDB = Provider=SQLOLEDB.1;Trusted_Connection=true;MultipleActiveResultSets=True;Initial Catalog=RAD_Reporting;Data Source=TVMXD969.TEST.SPRINT.COM,2778
AUTO_REPORTING_DX_SVC_FTP_SERVER = TVMXB029.TEST.SPRINT.COM
AUTO_REPORTING_DX_SVC_FTP_FILE_PATH = tvmxd968.test.sprint.com\\Drive_E$\\TEST\\QDA\\Data\\COWS\\ExternalFTPFiles
AUTO_REPORTING_DX_SVC_SMTP_TO = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
AUTO_REPORTING_DX_SVC_SMTP_FROM = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
AUTO_REPORTING_DX_SVC_SMTP_DEFAULT_CC =
AUTO_REPORTING_DX_SVC_SMTP_DEFAULT_BCC = 
AUTO_REPORTING_DX_SVC_DEVEMAIL_FROM = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
AUTO_REPORTING_DX_SVC_DEVEMAIL_TO = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
AUTO_REPORTING_DX_SVC_DEVEMAIL_CC =
AUTO_REPORTING_DX_SVC_DEVEMAIL_BCC =

################################################################################
#
# The following settings are for the COWS PeopleSoft Batch windows service.
#
################################################################################
PSOFT_BATCH_SVC_STARTUP_TYPE = Automatic
PSOFT_BATCH_SVC_REQ_SLEEP_TIME = 1000
PSOFT_BATCH_SVC_EXC_MINUTE_TIME = 46
PSOFT_BATCH_SVC_REQ_FILE_PATH = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Request\\
PSOFT_BATCH_SVC_REC_FILE_PATH = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Receipts\\
PSOFT_BATCH_SVC_ARCHIVE_REQ_FILE_PATH = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Archive\\Request\\
PSOFT_BATCH_SVC_ARCHIVE_REC_FILE_PATH = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Archive\\Receipts\\
PSOFT_BATCH_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.PeopleSoftBatchService
PSOFT_BATCH_SVC_LOG_USER = 
PSOFT_BATCH_SVC_LOG_QUEUE_GROW_FACTOR = 4
PSOFT_BATCH_SVC_LOG_TIMER = 180000
PSOFT_BATCH_SVC_LOG_TYPE = Detail
PSOFT_BATCH_SVC_LOG_QUEUE_SIZE = 200
PSOFT_BATCH_SVC_LOG_XSL = 
PSOFT_BATCH_SVC_LOG_NAME_LIST = Main
PSOFT_BATCH_SVC_LOG_MODE = Text
PSOFT_BATCH_SVC_LOG_QUEUE_FLUSH_SIZE = 200

PSOFT_BATCH_SVC_LOCALFILEPATH = E:\\BF2\\QDA\\Data\\COWS\\FTP_Batch_Prgs\\
PSOFT_BATCH_SVC_FILENAMEREQUEST = EAI_TransferRequest.bat
PSOFT_BATCH_SVC_FILENAMERECEIPTS = EAI_TransferReceipts.bat
PSOFT_BATCH_SVC_MYBATCHFILE = eai_transfer_file
PSOFT_BATCH_SVC_TARGETDIR = E:\\EAI\\bin\\
PSOFT_BATCH_SVC_SPATHREQUEST = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Request\\*.txt
PSOFT_BATCH_SVC_SPATHRECEIPT = E:\\BF2\\QDA\\Data\\COWS\\F7P\\Receipts\\*.txt
PSOFT_BATCH_SVC_SSYS = QDA.XMISSION
PSOFT_BATCH_SVC_DSYS = F5P.INPUT
PSOFT_BATCH_SVC_TNAME  = QDA_RECEIPTS_AND_REQUESTS
PSOFT_BATCH_SVC_DELETE =
PSOFT_BATCH_SVC_EMALADDR = ETS-TFB-Wireline-COWS-Support@T-Mobile.com
PSOFT_BATCH_SVC_EMAIL = all

################################################################################
#
# The following settings are for the COWS Mach5 Sender windows service.
#
################################################################################
MACH5_SENDER_SVC_STARTUP_TYPE = Automatic
MACH5_SENDER_SVC_REQ_SLEEP_TIME = 2000
MACH5_SENDER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.Mach5SenderService
MACH5_SENDER_PS_KEY_NAME = COWS_Mach5SenderService_com_sprint_ProvisioningService
MACH5_SENDER_RCS_KEY_NAME = COWS_Mach5SenderService_com_sprint_RedesignChargeService
MACH5_SENDER_URL_VALUE = http://144.229.206.116:8180/Mach5WS/ProvisioningService
MACH5_SENDER_SVC_LOG_USER = 
MACH5_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
MACH5_SENDER_SVC_LOG_TIMER = 180000
MACH5_SENDER_SVC_LOG_TYPE = Detail
MACH5_SENDER_SVC_LOG_QUEUE_SIZE = 200
MACH5_SENDER_SVC_LOG_XSL = 
MACH5_SENDER_SVC_LOG_NAME_LIST = Main
MACH5_SENDER_SVC_LOG_MODE = Text
MACH5_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 200
MACH5_SENDER_SVC_SHAREDSECRET =  jgJ2bu59o8
MACH5_SENDER_SVC_URL = http://144.229.206.116:8380/Mach5WS/ProvisioningService
MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = jgJ2bu59o8
MACH5_SENDER_REDESIGN_URL_VALUE = http://144.229.206.116:8380/Mach5WS/RedesignChargeService
#Mach5Test1
#MACH5_SENDER_URL_VALUE = http://144.229.206.116:8180/Mach5WS/ProvisioningService
#MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = VkQdoTBQBI
#MACH5_SENDER_REDESIGN_URL_VALUE = http://144.229.206.116:8180/Mach5WS/RedesignChargeService
#Mach5Test2
#MACH5_SENDER_URL_VALUE = http://144.229.206.116:8280/Mach5WS/ProvisioningService
#MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = gnRWFxuA3t
#MACH5_SENDER_REDESIGN_URL_VALUE = http://144.229.206.116:8280/Mach5WS/RedesignChargeService
#Mach5Test3
#MACH5_SENDER_URL_VALUE = http://144.229.206.116:8380/Mach5WS/ProvisioningService
#MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = qJg2i02Vwg
#MACH5_SENDER_REDESIGN_URL_VALUE = http://144.229.206.116:8380/Mach5WS/RedesignChargeService
#Mach5Test4
#MACH5_SENDER_URL_VALUE = http://144.229.206.116:8480/Mach5WS/ProvisioningService
#MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = hmTtGq9SfV
#MACH5_SENDER_REDESIGN_URL_VALUE = http://144.229.206.116:8480/Mach5WS/RedesignChargeService

################################################################################
#
# The following settings are for the COWS SSTAT Receiver windows service.
#
################################################################################
SSTAT_RECEIVER_SVC_STARTUP_TYPE = Automatic
SSTAT_RECEIVER_SVC_SLEEP_TIME = 2000
SSTAT_RECEIVER_SVC_MQ_HOST = TREAA640.test.sprint.com (1417)
SSTAT_RECEIVER_SVC_MQ_QUEUE_MGR = BCL02A2T
SSTAT_RECEIVER_SVC_MQ_CHANNEL = GNP.CLIENT
SSTAT_RECEIVER_SVC_MQ_QUEUE_IN = IKL.REPLY.QDA
SSTAT_RECEIVER_SVC_MQ_QUEUE_OUT = QDA.REQUEST.IKL
SSTAT_RECEIVER_SVC_MQ_CHARSET = 1208
SSTAT_RECEIVER_SVC_MQ_FORMAT = MQSTR\ \ \ 
SSTAT_RECEIVER_SVC_MQ_WAIT_INTERVAL = 2000
SSTAT_RECEIVER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.SSTATReceiverService
SSTAT_RECEIVER_SVC_LOG_USER = 
SSTAT_RECEIVER_SVC_LOG_QUEUE_GROW_FACTOR = 4
SSTAT_RECEIVER_SVC_LOG_TIMER = 180000
SSTAT_RECEIVER_SVC_LOG_TYPE = Detail
SSTAT_RECEIVER_SVC_LOG_QUEUE_SIZE = 200
SSTAT_RECEIVER_SVC_LOG_XSL = 
SSTAT_RECEIVER_SVC_LOG_NAME_LIST = Main
SSTAT_RECEIVER_SVC_LOG_MODE = Text
SSTAT_RECEIVER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

################################################################################
#
# The following settings are for the COWS SSTAT Sender windows service.
#
################################################################################
SSTAT_SENDER_SVC_STARTUP_TYPE = Automatic
SSTAT_SENDER_SVC_SLEEP_TIME = 2000
SSTAT_SENDER_SVC_MQ_HOST = TREAA640.test.sprint.com (1417)
SSTAT_SENDER_SVC_MQ_QUEUE_MGR = BCL02A2T
SSTAT_SENDER_SVC_MQ_CHANNEL = GNP.CLIENT
SSTAT_SENDER_SVC_MQ_QUEUE_IN = QDA.REQUEST.IKL
SSTAT_SENDER_SVC_MQ_QUEUE_OUT = IKL.REPLY.QDA
SSTAT_SENDER_SVC_MQ_CHARSET = 1208
SSTAT_SENDER_SVC_MQ_FORMAT = MQSTR\ \ \ 
SSTAT_SENDER_SVC_MQ_WAIT_INTERVAL = 2000
SSTAT_SENDER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.SSTATSenderService
SSTAT_SENDER_SVC_LOG_USER = 
SSTAT_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
SSTAT_SENDER_SVC_LOG_TIMER = 180000
SSTAT_SENDER_SVC_LOG_TYPE = Detail
SSTAT_SENDER_SVC_LOG_QUEUE_SIZE = 200
SSTAT_SENDER_SVC_LOG_XSL = 
SSTAT_SENDER_SVC_LOG_NAME_LIST = Main
SSTAT_SENDER_SVC_LOG_MODE = Text
SSTAT_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

################################################################################
#
# The following settings are for the COWS RulesetProcessor Service
#
################################################################################
#
# ENVIRONMENT CONFIGURATION FOR BPM
#
# https://appiandev0.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService				
# https://appiandev1.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService				
# https://appiandev2.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiandev4.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest2.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest3.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appianbreakfix.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://bpmcore.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
#
# CoWs.D@1!4NsF --> Dev1
# CoWs.D@2!4NsF --> Dev2
# CoWs.D@3!4NsF --> Dev3
# CoWs.R@1!4NsF --> RTB1
# CoWs.R@2!4NsF --> RTB2
# CoWs.R@3!4NsF --> RTB3
#
################################################################################
RULESETPROCESSOR_SVC_STARTUP_TYPE = Automatic
RULESETPROCESSOR_SVC_SLEEP_TIME = 700
RULESETPROCESSOR_SVC_EXECAPPLIST = 30R,EDW,BPM
RULESETPROCESSOR_SVC_EXECHOURTIMER = 22,23,0
RULESETPROCESSOR_SVC_EXECMINUTETIMER = 50,58,2
RULESETPROCESSOR_SVC_EXECSECONDTIMER = 0,0,0
RULESETPROCESSOR_SVC_AUDITFILEPATH = E:\\BF2\\QDA\\Data\\COWS\\
RULESETPROCESSOR_SVC_ARCHIVEAUDITFILEPATH = E:\\BF2\\QDA\\Data\\COWS\\Archive\\
RULESETPROCESSOR_SVC_EDWDIRECTORY = plsq00454e1-/data/apps/edw/ingestion/qda
RULESETPROCESSOR_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.RulesetProcessor
RULESETPROCESSOR_SVC_LOG_USER = 
RULESETPROCESSOR_SVC_LOG_QUEUE_GROW_FACTOR = 4
RULESETPROCESSOR_SVC_LOG_TIMER = 180000
RULESETPROCESSOR_SVC_LOG_TYPE = Detail
RULESETPROCESSOR_SVC_LOG_QUEUE_SIZE = 200
RULESETPROCESSOR_SVC_LOG_XSL = 
RULESETPROCESSOR_SVC_LOG_NAME_LIST = Main
RULESETPROCESSOR_SVC_LOG_MODE = Text
RULESETPROCESSOR_SVC_LOG_QUEUE_FLUSH_SIZE = 200
RULESETPROCESSOR_BPM_REDESIGN_URL = https://appianbreakfix.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
RULESETPROCESSOR_BPM_REDESIGN_USERNAME = qda-appian
RULESETPROCESSOR_BPM_REDESIGN_PASSWORD = CoWs.1@3!4NsF
RULESETPROCESSOR_EXECACTIVETHREAD = false,false,true