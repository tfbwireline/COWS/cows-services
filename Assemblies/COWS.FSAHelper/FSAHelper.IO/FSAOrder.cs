﻿namespace FSAHelper
{
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Order
    {
        private string timeStampField;
        private OrderOrderInformation orderInformationField;
        private OrderCISHierarchyInfo[] customerInfoField;
        private OrderAccountTeamMembers[] accountTeamField;
        private OrderInstallInfo installInfoField;
        private OrderDisconnectInfo disconnectInfoField;
        private OrderBillingInfo billingOnlyInformationField;
        private OrderChangeInformation changeInformation;
        private OrderNotes[] notesField;

        public string TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        public OrderOrderInformation OrderInformation
        {
            get
            {
                return this.orderInformationField;
            }
            set
            {
                this.orderInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("CISHierarchyInfo", IsNullable = false)]
        public OrderCISHierarchyInfo[] CustomerInfo
        {
            get
            {
                return this.customerInfoField;
            }
            set
            {
                this.customerInfoField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("AccountTeamMember", IsNullable = false)]
        public OrderAccountTeamMembers[] AccountTeam
        {
            get
            {
                return this.accountTeamField;
            }
            set
            {
                this.accountTeamField = value;
            }
        }

        public OrderInstallInfo InstallInfo
        {
            get
            {
                return this.installInfoField;
            }
            set
            {
                this.installInfoField = value;
            }
        }

        public OrderDisconnectInfo DisconnectInfo
        {
            get
            {
                return this.disconnectInfoField;
            }
            set
            {
                this.disconnectInfoField = value;
            }
        }

        public OrderBillingInfo BillingOnlyInformation
        {
            get
            {
                return this.billingOnlyInformationField;
            }
            set
            {
                this.billingOnlyInformationField = value;
            }
        }

        public OrderChangeInformation ChangeInformation
        {
            get
            {
                return this.changeInformation;
            }
            set
            {
                this.changeInformation = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Note", IsNullable = false)]
        public OrderNotes[] Notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderOrderInformation
    {
        private OrderOrderInformationOrderAction orderActionField;
        private string orderIDField;
        private OrderOrderInformationOrderTypeCode orderTypeCodeField;
        private OrderOrderInformationOrderSubTypeCode orderSubTypeCodeField;
        private OrderOrderInformationProductTypeCode productTypeCodeField;
        private string parentOrderIDField;
        private string relatedOrderIDField;
        private OrderOrderInformationSpecialCustomerArrangement[] specialCustomerArrangementsField;
        private string telecomServicePriorityCodeField;
        private OrderOrderInformationCarrierPartnerWholeSaleType carrierPartnerWholeSaleTypeField;
        private System.DateTime customerCommitDateField;
        private string customerWantDateField;
        private string customerOrderSubmitDateField;
        private string customerSignedDateField;
        private string orderSubmitDateField;
        private string customerPremiseCurrentlyOccupiedFlagField;
        private string willCustomerAcceptServiceEarlyFlagField;
        private string multipleOrderFlagField;
        private string multipleOrderIndex;
        private string multipleOrderTotal;
        private OrderOrderInformationExpediteInformation expediteInformationField;
        private OrderOrderInformationGovernmentInformation governmentInformationField;
        private EnumBoolYesNoFlag escalatedFlagField;
        private string vendorCodeField;
        private string vendorVPNCode;

        public OrderOrderInformationOrderAction OrderAction
        {
            get
            {
                return this.orderActionField;
            }
            set
            {
                this.orderActionField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string OrderID
        {
            get
            {
                return this.orderIDField;
            }
            set
            {
                this.orderIDField = value;
            }
        }

        public OrderOrderInformationOrderTypeCode OrderTypeCode
        {
            get
            {
                return this.orderTypeCodeField;
            }
            set
            {
                this.orderTypeCodeField = value;
            }
        }

        public OrderOrderInformationOrderSubTypeCode OrderSubTypeCode
        {
            get
            {
                return this.orderSubTypeCodeField;
            }
            set
            {
                this.orderSubTypeCodeField = value;
            }
        }

        public OrderOrderInformationProductTypeCode ProductTypeCode
        {
            get
            {
                return this.productTypeCodeField;
            }
            set
            {
                this.productTypeCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string ParentOrderID
        {
            get
            {
                return this.parentOrderIDField;
            }
            set
            {
                this.parentOrderIDField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string RelatedOrderID
        {
            get
            {
                return this.relatedOrderIDField;
            }
            set
            {
                this.relatedOrderIDField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("SpecialCustomerArrangement", IsNullable = false)]
        public OrderOrderInformationSpecialCustomerArrangement[] SpecialCustomerArrangements
        {
            get
            {
                return this.specialCustomerArrangementsField;
            }
            set
            {
                this.specialCustomerArrangementsField = value;
            }
        }

        public string TelecomServicePriorityCode
        {
            get
            {
                return this.telecomServicePriorityCodeField;
            }
            set
            {
                this.telecomServicePriorityCodeField = value;
            }
        }

        public OrderOrderInformationCarrierPartnerWholeSaleType CarrierPartnerWholeSaleType
        {
            get
            {
                return this.carrierPartnerWholeSaleTypeField;
            }
            set
            {
                this.carrierPartnerWholeSaleTypeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CustomerCommitDate
        {
            get
            {
                return this.customerCommitDateField;
            }
            set
            {
                this.customerCommitDateField = value;
            }
        }

        public string CustomerWantDate
        {
            get
            {
                return this.customerWantDateField;
            }
            set
            {
                this.customerWantDateField = value;
            }
        }

        public string CustomerOrderSubmitDate
        {
            get
            {
                return this.customerOrderSubmitDateField;
            }
            set
            {
                this.customerOrderSubmitDateField = value;
            }
        }

        public string CustomerSignedDate
        {
            get
            {
                return this.customerSignedDateField;
            }
            set
            {
                this.customerSignedDateField = value;
            }
        }

        public string OrderSubmitDate
        {
            get
            {
                return this.orderSubmitDateField;
            }
            set
            {
                this.orderSubmitDateField = value;
            }
        }

        public string CustomerPremiseCurrentlyOccupiedFlag
        {
            get
            {
                return this.customerPremiseCurrentlyOccupiedFlagField;
            }
            set
            {
                this.customerPremiseCurrentlyOccupiedFlagField = value;
            }
        }

        public string WillCustomerAcceptServiceEarlyFlag
        {
            get
            {
                return this.willCustomerAcceptServiceEarlyFlagField;
            }
            set
            {
                this.willCustomerAcceptServiceEarlyFlagField = value;
            }
        }

        public string MultipleOrderFlag
        {
            get
            {
                return this.multipleOrderFlagField;
            }
            set
            {
                this.multipleOrderFlagField = value;
            }
        }

        public string MultipleOrderIndex
        {
            get
            {
                return this.multipleOrderIndex;
            }
            set
            {
                this.multipleOrderIndex = value;
            }
        }

        public string MultipleOrderTotal
        {
            get
            {
                return this.multipleOrderTotal;
            }
            set
            {
                this.multipleOrderTotal = value;
            }
        }

        public OrderOrderInformationExpediteInformation ExpediteInformation
        {
            get
            {
                return this.expediteInformationField;
            }
            set
            {
                this.expediteInformationField = value;
            }
        }

        public OrderOrderInformationGovernmentInformation GovernmentInformation
        {
            get
            {
                return this.governmentInformationField;
            }
            set
            {
                this.governmentInformationField = value;
            }
        }

        public EnumBoolYesNoFlag EscalatedFlag
        {
            get
            {
                return this.escalatedFlagField;
            }
            set
            {
                this.escalatedFlagField = value;
            }
        }

        public string VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        public string VendorVPNCode
        {
            get
            {
                return this.vendorVPNCode;
            }
            set
            {
                this.vendorVPNCode = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderOrderInformationOrderAction
    {
        QQQ,
        Presubmit = 1,
        Submit = 2,
        Delete = 3,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderOrderInformationOrderTypeCode
    {
        QQQ,
        IN,
        AD,   //Add-On Should be NACKed!, uncommented per Pramod
        CN,
        DC,
        UP,
        DN,
        MV,
        BC,
        CH,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderOrderInformationOrderSubTypeCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        INST,
        HCDN,
        COLODN,
        HCMV,
        COLOMV,
        ADDVAS, //Add-On Should be NACKed!, uncommented per Pramod
        PORT,
        CPE,
        CPEHCMV,
        CPEREMV,
        HCUP,
        COLOUP,
        SIPTRK,
        MPLSVAS
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderOrderInformationProductTypeCode
    {
        QQQ,
        IP,
        MN,
        SE,
        CP,
        DO,
        SO,
        DN,
        MP,
        SN,
        GP,   //Commented out per V24. Uncommented out per V33 (CR 138).
        MO,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderOrderInformationSpecialCustomerArrangement
    {
        private string sCA_NBRField;

        public string Number
        {
            get
            {
                return this.sCA_NBRField;
            }
            set
            {
                this.sCA_NBRField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum EnumBoolYesNoFlag
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Y,
        N,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderOrderInformationCarrierPartnerWholeSaleType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        CarrierPartner,
        WholesaleNNI,
        Wholesale,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderOrderInformationExpediteInformation
    {
        private string typeCodeField;

        public string TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderOrderInformationGovernmentInformation
    {
        private string typeCodeField;
        private string sOTSIDField;
        private string cHARSIDField;

        public string TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }

        public string SOTSID
        {
            get
            {
                return this.sOTSIDField;
            }
            set
            {
                this.sOTSIDField = value;
            }
        }

        public string CHARSID
        {
            get
            {
                return this.cHARSIDField;
            }
            set
            {
                this.cHARSIDField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderCISHierarchyInfo
    {
        private OrderCISHierarchyInfoLevelType levelTypeField;
        private string customerIDField;
        private string customerNameField;
        private string currentBillCycleCodeField;
        private string futureBillCycleCodeField;
        private OrderCISHierarchyInfoServiceSubType serviceSubTypeField;
        private bool serviceSubTypeFieldSpecified;
        private string branchCodeField;
        private string salesOfficeIDCodeField;
        private string salesPersonPrimaryCIDField;
        private string salesPersonSecondaryCIDField;
        private string cLLICodeField;
        private OrderContactInfo contactInfoField;
        private OrderCISHierarchyAddress addressField;
        private OrderPhoneInfoData phoneInfoField;

        public OrderCISHierarchyInfoLevelType LevelType
        {
            get
            {
                return this.levelTypeField;
            }
            set
            {
                this.levelTypeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string CustomerID
        {
            get
            {
                return this.customerIDField;
            }
            set
            {
                this.customerIDField = value;
            }
        }

        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        public string CurrentBillCycleCode
        {
            get
            {
                return this.currentBillCycleCodeField;
            }
            set
            {
                this.currentBillCycleCodeField = value;
            }
        }

        public string FutureBillCycleCode
        {
            get
            {
                return this.futureBillCycleCodeField;
            }
            set
            {
                this.futureBillCycleCodeField = value;
            }
        }

        public OrderCISHierarchyInfoServiceSubType ServiceSubType
        {
            get
            {
                return this.serviceSubTypeField;
            }
            set
            {
                this.serviceSubTypeField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ServiceSubTypeSpecified
        {
            get
            {
                return this.serviceSubTypeFieldSpecified;
            }
            set
            {
                this.serviceSubTypeFieldSpecified = value;
            }
        }

        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        public string SalesOfficeIDCode
        {
            get
            {
                return this.salesOfficeIDCodeField;
            }
            set
            {
                this.salesOfficeIDCodeField = value;
            }
        }

        public string SalesPersonPrimaryCID
        {
            get
            {
                return this.salesPersonPrimaryCIDField;
            }
            set
            {
                this.salesPersonPrimaryCIDField = value;
            }
        }

        public string SalesPersonSecondaryCID
        {
            get
            {
                return this.salesPersonSecondaryCIDField;
            }
            set
            {
                this.salesPersonSecondaryCIDField = value;
            }
        }

        public string CLLICode
        {
            get
            {
                return this.cLLICodeField;
            }
            set
            {
                this.cLLICodeField = value;
            }
        }

        public OrderContactInfo ContactInfo
        {
            get
            {
                return this.contactInfoField;
            }
            set
            {
                this.contactInfoField = value;
            }
        }

        public OrderCISHierarchyAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        public OrderPhoneInfoData PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderCISHierarchyInfoLevelType
    {
        QQQ,
        H1,
        H4,
        IP,
        CP,
        ES,
        GP,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderCISHierarchyInfoServiceSubType
    {
        QQQ,
        MPLS,
        DIA,
        SLFR,
        SLVLAN,
        SLPPL,
        IPT,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderCISHierarchyAddress
    {
        private OrderAddressType typeField;
        private string addressLine1Field;
        private string addressLine2Field;
        private string cityField;
        private string stateCodeField;
        private string zIPCodeField;
        private string zIPCodePlusFourField;
        private string postalCodeField;
        private string countryCodeField;
        private string provinceMuncipalityField;

        public OrderAddressType Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        public string StateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        public string ZIPCode
        {
            get
            {
                return this.zIPCodeField;
            }
            set
            {
                this.zIPCodeField = value;
            }
        }

        public string ZIPCodePlusFour
        {
            get
            {
                return this.zIPCodePlusFourField;
            }
            set
            {
                this.zIPCodePlusFourField = value;
            }
        }

        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        public string ProvinceMuncipality
        {
            get
            {
                return this.provinceMuncipalityField;
            }
            set
            {
                this.provinceMuncipalityField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderContactInfo
    {
        private OrderAddressType typeField;
        private OrderContactInfoData contactNameInfoField;
        private string addressLine1Field;
        private string addressLine2Field;
        private string cityField;
        private string stateCodeField;
        private string zIPCodeField;
        private string zIPCodePlusFourField;
        private string postalCodeField;
        private string countryCodeField;
        private string provinceMuncipalityField;
        private OrderPhoneInfoData phoneInfoField;
        private string availabilityHoursField;
        private string timeZoneField;
        private string interpreterNeededFlagField;

        //private string languageSupportedField;
        public OrderAddressType Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        public OrderContactInfoData ContactNameInfo
        {
            get
            {
                return this.contactNameInfoField;
            }
            set
            {
                this.contactNameInfoField = value;
            }
        }

        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        public string StateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        public string ZIPCode
        {
            get
            {
                return this.zIPCodeField;
            }
            set
            {
                this.zIPCodeField = value;
            }
        }

        public string ZIPCodePlusFour
        {
            get
            {
                return this.zIPCodePlusFourField;
            }
            set
            {
                this.zIPCodePlusFourField = value;
            }
        }

        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        public string ProvinceMuncipality
        {
            get
            {
                return this.provinceMuncipalityField;
            }
            set
            {
                this.provinceMuncipalityField = value;
            }
        }

        public OrderPhoneInfoData PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }

        public string AvailabilityHours
        {
            get
            {
                return this.availabilityHoursField;
            }
            set
            {
                this.availabilityHoursField = value;
            }
        }

        public string TimeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        public string InterpreterNeededFlag
        {
            get
            {
                return this.interpreterNeededFlagField;
            }
            set
            {
                this.interpreterNeededFlagField = value;
            }
        }

        //public string LanguageSupported
        //{
        //    get
        //    {
        //        return this.languageSupportedField;
        //    }
        //    set
        //    {
        //        this.languageSupportedField = value;
        //    }
        //}
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderAddressType
    {
        QQQ,
        Service = 1,
        Primary = 2,
        Billing = 3,
        Secondary = 4,
        Support = 5,
        CustomerOnsite = 6,

        [System.Xml.Serialization.XmlEnumAttribute("Alternate Site")]
        AlternateSite = 7,

        [System.Xml.Serialization.XmlEnumAttribute("Service Assurance")]
        ServiceAssurance = 8,

        DNSAdministrator = 9,
        Onsite = 16,
        Site = 18,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderContactInfoData
    {
        private OrderContactType contactTypeField;
        private string firstNameField;
        private string lastNameField;
        private string nameField;
        private string emailAddressField;

        public OrderContactType ContactType
        {
            get
            {
                return this.contactTypeField;
            }
            set
            {
                this.contactTypeField = value;
            }
        }

        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderContactType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Primary = 1,
        Secondary = 2,
        Service = 3,
        Onsite = 4,
        Requestor = 5,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderPhoneInfoData
    {
        private OrderPhoneType phoneTypeField;
        private string countryCodeField;
        private string stationField;
        private string cityCodeField;
        private string nPAField;
        private string nXXField;
        private string numberField;
        private string extensionField;

        public OrderPhoneType PhoneType
        {
            get
            {
                return this.phoneTypeField;
            }
            set
            {
                this.phoneTypeField = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        public string Station
        {
            get
            {
                return this.stationField;
            }
            set
            {
                this.stationField = value;
            }
        }

        public string CityCode
        {
            get
            {
                return this.cityCodeField;
            }
            set
            {
                this.cityCodeField = value;
            }
        }

        public string NPA
        {
            get
            {
                return this.nPAField;
            }
            set
            {
                this.nPAField = value;
            }
        }

        public string NXX
        {
            get
            {
                return this.nXXField;
            }
            set
            {
                this.nXXField = value;
            }
        }

        public string Number
        {
            get
            {
                return this.numberField;
            }
            set
            {
                this.numberField = value;
            }
        }

        public string Extension
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderPhoneType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Phone,
        Fax,
        Billing,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderAccountTeamMembers
    {
        private OrderAccountTeamMembersRoleCode roleCodeField;
        private OrderContactInfoData contactNameInfoField;
        private OrderPhoneInfoData phoneInfoField;

        public OrderAccountTeamMembersRoleCode RoleCode
        {
            get
            {
                return this.roleCodeField;
            }
            set
            {
                this.roleCodeField = value;
            }
        }

        public OrderContactInfoData ContactNameInfo
        {
            get
            {
                return this.contactNameInfoField;
            }
            set
            {
                this.contactNameInfoField = value;
            }
        }

        public OrderPhoneInfoData PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderAccountTeamMembersRoleCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        [System.Xml.Serialization.XmlEnumAttribute("Salesperson - Primary")]
        SalespersonPrimary = 10,

        [System.Xml.Serialization.XmlEnumAttribute("SIS - Primary")]
        SISPrimary = 11,

        [System.Xml.Serialization.XmlEnumAttribute("SDE - Primary")]
        SDEPrimary = 12,

        [System.Xml.Serialization.XmlEnumAttribute("TAC - Primary")]
        TACPrimary = 13,

        ATSM = 14,

        [System.Xml.Serialization.XmlEnumAttribute("Salesperson - Secondary")]
        SalespersonSecondary = 60,

        [System.Xml.Serialization.XmlEnumAttribute("SDE - Secondary")]
        SDESecondary = 61,

        [System.Xml.Serialization.XmlEnumAttribute("SIS - Secondary")]
        SISSecondary = 62,

        [System.Xml.Serialization.XmlEnumAttribute("TAC - Secondary")]
        TACSecondary = 63,

        [System.Xml.Serialization.XmlEnumAttribute("SSE - Primary")]
        SSEPrimary = 64,

        [System.Xml.Serialization.XmlEnumAttribute("SSE - Secondary")]
        SSESecondary = 65,

        [System.Xml.Serialization.XmlEnumAttribute("AEM")]
        AEM = 66,

        [System.Xml.Serialization.XmlEnumAttribute("IE")]
        IE = 67,

        [System.Xml.Serialization.XmlEnumAttribute("QAS - Quality Assurance Specialist")]
        QAS = 68,

        [System.Xml.Serialization.XmlEnumAttribute("Aggregate")]
        Aggregate = 69,

        [System.Xml.Serialization.XmlEnumAttribute("OMG - Order Management Group")]
        OMG = 70,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfo
    {
        private string designDocumentNumberField;
        private OrderInstallInfoMDSInstallationInformation mDSInstallationInformationField;
        private OrderInstallInfoCPEInstallationInformation cPEInstallationInformationField;
        private OrderInstallInfoValueAddedService[] valueAddedServiceField;
        private OrderInstallInfoTransportInstallInformation transportInstallInformationField;
        private OrderBillingLineItems[] billingLineItemsField;
        private SIPTrunkInformation sIPTrunkInstallInformation;

        public string DesignDocumentNumber
        {
            get
            {
                return this.designDocumentNumberField;
            }
            set
            {
                this.designDocumentNumberField = value;
            }
        }

        public OrderInstallInfoMDSInstallationInformation MDSInstallationInformation
        {
            get
            {
                return this.mDSInstallationInformationField;
            }
            set
            {
                this.mDSInstallationInformationField = value;
            }
        }

        public OrderInstallInfoCPEInstallationInformation CPEInstallationInformation
        {
            get
            {
                return this.cPEInstallationInformationField;
            }
            set
            {
                this.cPEInstallationInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("ValueAddedService", IsNullable = false)]
        public OrderInstallInfoValueAddedService[] ValueAddedServices
        {
            get
            {
                return this.valueAddedServiceField;
            }
            set
            {
                this.valueAddedServiceField = value;
            }
        }

        public OrderInstallInfoTransportInstallInformation TransportInstallInformation
        {
            get
            {
                return this.transportInstallInformationField;
            }
            set
            {
                this.transportInstallInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItemsField;
            }
            set
            {
                this.billingLineItemsField = value;
            }
        }

        public SIPTrunkInformation SIPTrunkInstallInformation
        {
            get
            {
                return this.sIPTrunkInstallInformation;
            }
            set
            {
                this.sIPTrunkInstallInformation = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoMDSInstallationInformation
    {
        private string activityTypeField;
        private string solutionServiceField;
        private string networkTypeCodeField;
        private string serviceTierCodeField;
        private string transportOrderTypeCodeField;
        private OrderContactInfo[] contactInfoField;
        private OrderInstallInfoMDSInstallationInformationMDSLineItem[] mDSLineItemsField;

        public string ActivityType
        {
            get
            {
                return this.activityTypeField;
            }
            set
            {
                this.activityTypeField = value;
            }
        }

        public string SolutionService
        {
            get
            {
                return this.solutionServiceField;
            }
            set
            {
                this.solutionServiceField = value;
            }
        }

        public string NetworkTypeCode
        {
            get
            {
                return this.networkTypeCodeField;
            }
            set
            {
                this.networkTypeCodeField = value;
            }
        }

        public string ServiceTierCode
        {
            get
            {
                return this.serviceTierCodeField;
            }
            set
            {
                this.serviceTierCodeField = value;
            }
        }

        public string TransportOrderTypeCode
        {
            get
            {
                return this.transportOrderTypeCodeField;
            }
            set
            {
                this.transportOrderTypeCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("ContactInfo", IsNullable = false)]
        public OrderContactInfo[] ContactInfos
        {
            get
            {
                return this.contactInfoField;
            }
            set
            {
                this.contactInfoField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("MDSLineItem", IsNullable = false)]
        public OrderInstallInfoMDSInstallationInformationMDSLineItem[] MDSLineItems
        {
            get
            {
                return this.mDSLineItemsField;
            }
            set
            {
                this.mDSLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoMDSInstallationInformationMDSLineItem
    {
        private string serviceLineItemCodeField;
        private string descriptionField;
        private string quantityField;
        private string monthlyRecurringChargeAmountField;
        private string nonRecurringChargeAmountField;

        public string ServiceLineItemCode
        {
            get
            {
                return this.serviceLineItemCodeField;
            }
            set
            {
                this.serviceLineItemCodeField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        public string MonthlyRecurringChargeAmount
        {
            get
            {
                return this.monthlyRecurringChargeAmountField;
            }
            set
            {
                this.monthlyRecurringChargeAmountField = value;
            }
        }

        public string NonRecurringChargeAmount
        {
            get
            {
                return this.nonRecurringChargeAmountField;
            }
            set
            {
                this.nonRecurringChargeAmountField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoCPEInstallationInformation
    {
        private string activityTypeField;
        private OrderInstallInfoCPEInstallationInformationCPEOrderTypeCode cPEOrderTypeCodeField;
        private string equipmentOnlyFlagCodeField;
        private string recordsOnlyOrderFlagCodeField;
        private string accessProvideCodeField;
        private OrderInstallInfoCPEInstallationInformationCPEPhoneNumber cPEPhoneNumberField;
        private string eCCKTIdentifierField;
        private string managedServicesChannelProgramCodeField;
        private string deliveryDutiesField;
        private string deliveryDutyAmountField;
        private string shippingChargeAmountField;
        private OrderInstallInfoCPEInstallationInformationCPELineItem[] cPELineItemsField;

        public string ActivityType
        {
            get
            {
                return this.activityTypeField;
            }
            set
            {
                this.activityTypeField = value;
            }
        }

        public OrderInstallInfoCPEInstallationInformationCPEOrderTypeCode CPEOrderTypeCode
        {
            get
            {
                return this.cPEOrderTypeCodeField;
            }
            set
            {
                this.cPEOrderTypeCodeField = value;
            }
        }

        public string EquipmentOnlyFlagCode
        {
            get
            {
                return this.equipmentOnlyFlagCodeField;
            }
            set
            {
                this.equipmentOnlyFlagCodeField = value;
            }
        }

        public string RecordsOnlyOrderFlagCode
        {
            get
            {
                return this.recordsOnlyOrderFlagCodeField;
            }
            set
            {
                this.recordsOnlyOrderFlagCodeField = value;
            }
        }

        public string AccessProvideCode
        {
            get
            {
                return this.accessProvideCodeField;
            }
            set
            {
                this.accessProvideCodeField = value;
            }
        }

        public OrderInstallInfoCPEInstallationInformationCPEPhoneNumber CPEPhoneNumber
        {
            get
            {
                return this.cPEPhoneNumberField;
            }
            set
            {
                this.cPEPhoneNumberField = value;
            }
        }

        public string ECCKTIdentifier
        {
            get
            {
                return this.eCCKTIdentifierField;
            }
            set
            {
                this.eCCKTIdentifierField = value;
            }
        }

        public string ManagedServicesChannelProgramCode
        {
            get
            {
                return this.managedServicesChannelProgramCodeField;
            }
            set
            {
                this.managedServicesChannelProgramCodeField = value;
            }
        }

        public string DeliveryDuties
        {
            get
            {
                return this.deliveryDutiesField;
            }
            set
            {
                this.deliveryDutiesField = value;
            }
        }

        public string DeliveryDutyAmount
        {
            get
            {
                return this.deliveryDutyAmountField;
            }
            set
            {
                this.deliveryDutyAmountField = value;
            }
        }

        public string ShippingChargeAmount
        {
            get
            {
                return this.shippingChargeAmountField;
            }
            set
            {
                this.shippingChargeAmountField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("CPELineItem", IsNullable = false)]
        public OrderInstallInfoCPEInstallationInformationCPELineItem[] CPELineItems
        {
            get
            {
                return this.cPELineItemsField;
            }
            set
            {
                this.cPELineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderInstallInfoCPEInstallationInformationCPEOrderTypeCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        STD,
        MNS,
        SVSO,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoCPEInstallationInformationCPEPhoneNumber
    {
        private string typeField;
        private string phoneNumberField;

        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoCPEInstallationInformationCPELineItemCPEFinancial
    {
        private string monthlyRecurringChargeAmountField;
        private string nonRecurringChargeAmountField;
        private string chargeCodeField;

        public string MonthlyRecurringChargeAmount
        {
            get { return monthlyRecurringChargeAmountField; }
            set { monthlyRecurringChargeAmountField = value; }
        }

        public string NonRecurringChargeAmount
        {
            get { return nonRecurringChargeAmountField; }
            set { nonRecurringChargeAmountField = value; }
        }

        public string ChargeCode
        {
            get { return chargeCodeField; }
            set { chargeCodeField = value; }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoCPEInstallationInformationCPELineItem
    {
        private string equipmentTypeCodeField;
        private string equipmentIDField;
        private string descriptionField;
        private string manufacturerField;
        private string quantityField;
        private OrderInstallInfoCPEInstallationInformationCPELineItemContractType contractTypeField;
        private string contractTermField;
        private string installationFlagCodeField;
        private string maintenanceFlagCodeField;
        private OrderInstallInfoCPEInstallationInformationCPELineItemCPEFinancial[] cPEFinancialsField;

        //private string monthlyRecurringChargeAmountField;
        //private string nonRecurringChargeAmountField;
        public string EquipmentTypeCode
        {
            get
            {
                return this.equipmentTypeCodeField;
            }
            set
            {
                this.equipmentTypeCodeField = value;
            }
        }

        public string EquipmentID
        {
            get
            {
                return this.equipmentIDField;
            }
            set
            {
                this.equipmentIDField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public string Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        public OrderInstallInfoCPEInstallationInformationCPELineItemContractType ContractType
        {
            get
            {
                return this.contractTypeField;
            }
            set
            {
                this.contractTypeField = value;
            }
        }

        public string ContractTerm
        {
            get
            {
                return this.contractTermField;
            }
            set
            {
                this.contractTermField = value;
            }
        }

        public string InstallationFlagCode
        {
            get
            {
                return this.installationFlagCodeField;
            }
            set
            {
                this.installationFlagCodeField = value;
            }
        }

        public string MaintenanceFlagCode
        {
            get
            {
                return this.maintenanceFlagCodeField;
            }
            set
            {
                this.maintenanceFlagCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("CPEFinancial", IsNullable = false)]
        public OrderInstallInfoCPEInstallationInformationCPELineItemCPEFinancial[] CPEFinancials
        {
            get { return cPEFinancialsField; }
            set { cPEFinancialsField = value; }
        }

        //public string MonthlyRecurringChargeAmount
        //{
        //    get
        //    {
        //        return this.monthlyRecurringChargeAmountField;
        //    }
        //    set
        //    {
        //        this.monthlyRecurringChargeAmountField = value;
        //    }
        //}
        //public string NonRecurringChargeAmount
        //{
        //    get
        //    {
        //        return this.nonRecurringChargeAmountField;
        //    }
        //    set
        //    {
        //        this.nonRecurringChargeAmountField = value;
        //    }
        //}
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderInstallInfoCPEInstallationInformationCPELineItemContractType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Purchase,
        Rental,
        Installment,
        Customer,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoValueAddedService
    {
        private OrderInstallInfoValueAddedServiceVASListVASType vASTypeField;
        private string levelField;
        private string networkAddressField;
        private string quantityField;
        private OrderBillingLineItems[] billingLineItemsField;

        public OrderInstallInfoValueAddedServiceVASListVASType VASType
        {
            get
            {
                return this.vASTypeField;
            }
            set
            {
                this.vASTypeField = value;
            }
        }

        public string Level
        {
            get
            {
                return this.levelField;
            }
            set
            {
                this.levelField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string NetworkAddress
        {
            get
            {
                return this.networkAddressField;
            }
            set
            {
                this.networkAddressField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItemsField;
            }
            set
            {
                this.billingLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderInstallInfoValueAddedServiceVASListVASType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        NOC,
        COS,

        //      TUN,
        WIC,

        WIA,
        WIBSC,
        WIBSA,
        HCC,
        SIA,
        RAS,
        HLFTN,
        VSYS,
        ZONE,
        PLCY,
        NWSFD,
        DNS,
        PORT,
        PRDNS,
        SCDNS,
        DTLNK,
        WHVPN,
        CPVPN,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderBillingOnlyInformation
    {
        private OrderBillingLineItems[] billingOnlyLineItemsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get { return billingOnlyLineItemsField; }
            set { billingOnlyLineItemsField = value; }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderBillingLineItems
    {
        private string bICCodeField;
        private string descriptionField;
        private string quantityField;
        private string itemCodeField;
        private string itemDescriptionField;
        private string monthlyRecurringChargeAmountField;
        private string nonRecurringChargeAmountField;
        private OrderBillingLineItemsActionCode actionCodeField;

        public string BICCode
        {
            get
            {
                return this.bICCodeField;
            }
            set
            {
                this.bICCodeField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        public string ItemCode
        {
            get
            {
                return this.itemCodeField;
            }
            set
            {
                this.itemCodeField = value;
            }
        }

        public string ItemDescription
        {
            get
            {
                return this.itemDescriptionField;
            }
            set
            {
                this.itemDescriptionField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string MonthlyRecurringChargeAmount
        {
            get
            {
                return this.monthlyRecurringChargeAmountField;
            }
            set
            {
                this.monthlyRecurringChargeAmountField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string NonRecurringChargeAmount
        {
            get
            {
                return this.nonRecurringChargeAmountField;
            }
            set
            {
                this.nonRecurringChargeAmountField = value;
            }
        }

        public OrderBillingLineItemsActionCode ActionCode
        {
            get
            {
                return this.actionCodeField;
            }
            set
            {
                this.actionCodeField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderBillingLineItemsActionCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Install,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformation
    {
        private string orangeCommunityName;
        private string orangeSiteID;
        private string orangeOfferCode;
        private string carrierServiceID;
        private string carrierCircuitID;
        private EnumBoolYesNoFlag customerFirewallFlagField;
        private OrderInstallInfoTransportInstallInformationSupportInformation supportInformationField;
        private OrderInstallInfoTransportInstallInformationTransport transportField;
        private OrderInstallInfoTransportInstallInformationPort portField;

        public string OrangeCommunityName
        {
            get
            {
                return this.orangeCommunityName;
            }
            set
            {
                this.orangeCommunityName = value;
            }
        }

        public string OrangeSiteID
        {
            get
            {
                return this.orangeSiteID;
            }
            set
            {
                this.orangeSiteID = value;
            }
        }

        public string OrangeOfferCode
        {
            get
            {
                return this.orangeOfferCode;
            }
            set
            {
                this.orangeOfferCode = value;
            }
        }

        public string CarrierServiceID
        {
            get
            {
                return this.carrierServiceID;
            }
            set
            {
                this.carrierServiceID = value;
            }
        }

        public string CarrierCircuitID
        {
            get
            {
                return this.carrierCircuitID;
            }
            set
            {
                this.carrierCircuitID = value;
            }
        }

        public EnumBoolYesNoFlag CustomerFirewallFlag
        {
            get
            {
                return this.customerFirewallFlagField;
            }
            set
            {
                this.customerFirewallFlagField = value;
            }
        }

        public OrderInstallInfoTransportInstallInformationSupportInformation SupportInformation
        {
            get
            {
                return this.supportInformationField;
            }
            set
            {
                this.supportInformationField = value;
            }
        }

        public OrderInstallInfoTransportInstallInformationTransport Transport
        {
            get
            {
                return this.transportField;
            }
            set
            {
                this.transportField = value;
            }
        }

        public OrderInstallInfoTransportInstallInformationPort Port
        {
            get
            {
                return this.portField;
            }
            set
            {
                this.portField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SIPTrunkInformation
    {
        private SIPTrunkGroups[] sIPTrunkGroups;

        [System.Xml.Serialization.XmlArrayItemAttribute("SIPTrunkGroup", IsNullable = false)]
        public SIPTrunkGroups[] SIPTrunkGroups
        {
            get
            {
                return this.sIPTrunkGroups;
            }
            set
            {
                this.sIPTrunkGroups = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SIPTrunkGroups
    {
        private string componentID;
        private string sIPTrunkQuantity;
        private string networkUserAddress;
        private MPLSAccesses[] mPLSAccesses;

        public string ComponentID
        {
            get
            {
                return this.componentID;
            }
            set
            {
                this.componentID = value;
            }
        }

        public string SIPTrunkQuantity
        {
            get
            {
                return this.sIPTrunkQuantity;
            }
            set
            {
                this.sIPTrunkQuantity = value;
            }
        }

        public string NetworkUserAddress
        {
            get
            {
                return this.networkUserAddress;
            }
            set
            {
                this.networkUserAddress = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("MPLSAccess", IsNullable = false)]
        public MPLSAccesses[] MPLSAccesses
        {
            get
            {
                return this.mPLSAccesses;
            }
            set
            {
                this.mPLSAccesses = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class MPLSAccesses
    {
        private string circuitID;
        private string networkUserAddress;

        public string CircuitID
        {
            get
            {
                return this.circuitID;
            }
            set
            {
                this.circuitID = value;
            }
        }

        public string NetworkUserAddress
        {
            get
            {
                return this.networkUserAddress;
            }
            set
            {
                this.networkUserAddress = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationSupportInformation
    {
        private OrderContactInfo[] contactInfoField;
        private string telcoDemarcationBuildingField;
        private string telcoDemarcationFloorField;
        private string telcoDemarcationRoomField;
        private string nearestCrossStreetField;
        private string pRSQuoteNumberField;
        private string gCSNumberField;
        private string requestForQuoteNumberField;

        [System.Xml.Serialization.XmlArrayItemAttribute("ContactInfo", IsNullable = false)]
        public OrderContactInfo[] ContactInfos
        {
            get
            {
                return this.contactInfoField;
            }
            set
            {
                this.contactInfoField = value;
            }
        }

        public string TelcoDemarcationBuilding
        {
            get
            {
                return this.telcoDemarcationBuildingField;
            }
            set
            {
                this.telcoDemarcationBuildingField = value;
            }
        }

        public string TelcoDemarcationFloor
        {
            get
            {
                return this.telcoDemarcationFloorField;
            }
            set
            {
                this.telcoDemarcationFloorField = value;
            }
        }

        public string TelcoDemarcationRoom
        {
            get
            {
                return this.telcoDemarcationRoomField;
            }
            set
            {
                this.telcoDemarcationRoomField = value;
            }
        }

        public string NearestCrossStreet
        {
            get
            {
                return this.nearestCrossStreetField;
            }
            set
            {
                this.nearestCrossStreetField = value;
            }
        }

        public string PRSQuoteNumber
        {
            get
            {
                return this.pRSQuoteNumberField;
            }
            set
            {
                this.pRSQuoteNumberField = value;
            }
        }

        public string GCSNumber
        {
            get
            {
                return this.gCSNumberField;
            }
            set
            {
                this.gCSNumberField = value;
            }
        }

        public string RequestForQuoteNumber
        {
            get
            {
                return this.requestForQuoteNumberField;
            }
            set
            {
                this.requestForQuoteNumberField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationTransport
    {
        private string circuitIDField;
        private string accessTypeCodeField;
        private string accessTypeDescriptionField;
        private string accessProviderCodeField;
        private string accessArrangmentCodeField;
        private string accessContractTermField;
        private string vendorContractTerm;
        private string speedofServiceField;
        private string linkProtocolCodeField;
        private string encapsulationCodeField;
        private string routingTypeCodeField;
        private string mPLSVPNOverSprintLinkFlagField;
        private string service_TypeField;
        private string framingField;
        private string encodingField;
        private string cSUDSUProvidedByVendorFlagField;
        private string jackInterfaceTypeCodeField;
        private string stratumLevelCodeField;
        private string clockSourceCodeField;
        private string isochronousFlagField;
        private string parallelProcessTypeCodeField;
        private string parallelProcessCircuitIDField;
        private string relatedFacilityReuseFlagField;
        private string relatedFacilityCircuitIDField;
        private short numberofTimeSlotsField;
        private short startingTimeSlotField;
        private string networkAddressField;
        private string sharedTenantFlagField;
        private string alternateAccessFlagField;
        private string alternateAccessProviderCodeField;
        private string fiberHandOffCodeField;
        private EnumBoolYesNoFlag managedDataServicesFlagField;
        private string oneStopShopFlagField;
        private EnumBoolYesNoFlag internationalPrivateLinePlusIPFlag;
        private TransportInternationalPrivateLineCategory internationalPrivateLineCategory;
        private string internationalPrivateLineServiceTypeCode;
        private string internationalPrivateLineCircuitTypeCode;
        private TransportOneStopShopOptionCode oneStopShopOptionCode;
        private string mPLSBackHaulFlagField;
        private string carrierAccessCode;
        private OrderBillingLineItems[] billingLineItemsField;

        public string CircuitID
        {
            get
            {
                return this.circuitIDField;
            }
            set
            {
                this.circuitIDField = value;
            }
        }

        public string AccessTypeCode
        {
            get
            {
                return this.accessTypeCodeField;
            }
            set
            {
                this.accessTypeCodeField = value;
            }
        }

        public string AccessTypeDescription
        {
            get { return accessTypeDescriptionField; }
            set { accessTypeDescriptionField = value; }
        }

        public string AccessProviderCode
        {
            get
            {
                return this.accessProviderCodeField;
            }
            set
            {
                this.accessProviderCodeField = value;
            }
        }

        public string AccessArrangementCode
        {
            get
            {
                return this.accessArrangmentCodeField;
            }
            set
            {
                this.accessArrangmentCodeField = value;
            }
        }

        public string AccessContractTerm
        {
            get
            {
                return this.accessContractTermField;
            }
            set
            {
                this.accessContractTermField = value;
            }
        }

        public string VendorContractTerm
        {
            get
            {
                return this.vendorContractTerm;
            }
            set
            {
                this.vendorContractTerm = value;
            }
        }

        public string SpeedOfService
        {
            get
            {
                return this.speedofServiceField;
            }
            set
            {
                this.speedofServiceField = value;
            }
        }

        public string LinkProtocolCode
        {
            get
            {
                return this.linkProtocolCodeField;
            }
            set
            {
                this.linkProtocolCodeField = value;
            }
        }

        public string EncapsulationCode
        {
            get
            {
                return this.encapsulationCodeField;
            }
            set
            {
                this.encapsulationCodeField = value;
            }
        }

        public string RoutingTypeCode
        {
            get
            {
                return this.routingTypeCodeField;
            }
            set
            {
                this.routingTypeCodeField = value;
            }
        }

        public string MPLSVPNOverSprintLinkFlag
        {
            get
            {
                return this.mPLSVPNOverSprintLinkFlagField;
            }
            set
            {
                this.mPLSVPNOverSprintLinkFlagField = value;
            }
        }

        public string ServiceType
        {
            get
            {
                return this.service_TypeField;
            }
            set
            {
                this.service_TypeField = value;
            }
        }

        public string Framing
        {
            get
            {
                return this.framingField;
            }
            set
            {
                this.framingField = value;
            }
        }

        public string Encoding
        {
            get
            {
                return this.encodingField;
            }
            set
            {
                this.encodingField = value;
            }
        }

        public string CSUDSUProvidedByVendorFlag
        {
            get
            {
                return this.cSUDSUProvidedByVendorFlagField;
            }
            set
            {
                this.cSUDSUProvidedByVendorFlagField = value;
            }
        }

        public string JackInterfaceTypeCode
        {
            get
            {
                return this.jackInterfaceTypeCodeField;
            }
            set
            {
                this.jackInterfaceTypeCodeField = value;
            }
        }

        public string StratumLevelCode
        {
            get
            {
                return this.stratumLevelCodeField;
            }
            set
            {
                this.stratumLevelCodeField = value;
            }
        }

        public string ClockSourceCode
        {
            get
            {
                return this.clockSourceCodeField;
            }
            set
            {
                this.clockSourceCodeField = value;
            }
        }

        public string IsochronousFlag
        {
            get
            {
                return this.isochronousFlagField;
            }
            set
            {
                this.isochronousFlagField = value;
            }
        }

        public string ParallelProcessTypeCode
        {
            get
            {
                return this.parallelProcessTypeCodeField;
            }
            set
            {
                this.parallelProcessTypeCodeField = value;
            }
        }

        public string ParallelProcessCircuitID
        {
            get
            {
                return this.parallelProcessCircuitIDField;
            }
            set
            {
                this.parallelProcessCircuitIDField = value;
            }
        }

        public string RelatedFacilityReuseFlag
        {
            get
            {
                return this.relatedFacilityReuseFlagField;
            }
            set
            {
                this.relatedFacilityReuseFlagField = value;
            }
        }

        public string RelatedFacilityCircuitID
        {
            get
            {
                return this.relatedFacilityCircuitIDField;
            }
            set
            {
                this.relatedFacilityCircuitIDField = value;
            }
        }

        public short NumberofTimeSlots
        {
            get
            {
                return this.numberofTimeSlotsField;
            }
            set
            {
                this.numberofTimeSlotsField = value;
            }
        }

        public short StartingTimeSlot
        {
            get
            {
                return this.startingTimeSlotField;
            }
            set
            {
                this.startingTimeSlotField = value;
            }
        }

        public string NetworkAddress
        {
            get
            {
                return this.networkAddressField;
            }
            set
            {
                this.networkAddressField = value;
            }
        }

        public string SharedTenantFlag
        {
            get
            {
                return this.sharedTenantFlagField;
            }
            set
            {
                this.sharedTenantFlagField = value;
            }
        }

        public string AlternateAccessFlag
        {
            get
            {
                return this.alternateAccessFlagField;
            }
            set
            {
                this.alternateAccessFlagField = value;
            }
        }

        public string AlternateAccessProviderCode
        {
            get
            {
                return this.alternateAccessProviderCodeField;
            }
            set
            {
                this.alternateAccessProviderCodeField = value;
            }
        }

        public string FiberHandOffCode
        {
            get
            {
                return this.fiberHandOffCodeField;
            }
            set
            {
                this.fiberHandOffCodeField = value;
            }
        }

        public EnumBoolYesNoFlag ManagedDataServicesFlag
        {
            get
            {
                return this.managedDataServicesFlagField;
            }
            set
            {
                this.managedDataServicesFlagField = value;
            }
        }

        public string OneStopShopFlag
        {
            get
            {
                return this.oneStopShopFlagField;
            }
            set
            {
                this.oneStopShopFlagField = value;
            }
        }

        public EnumBoolYesNoFlag InternationalPrivateLinePlusIPFlag
        {
            get
            {
                return this.internationalPrivateLinePlusIPFlag;
            }
            set
            {
                this.internationalPrivateLinePlusIPFlag = value;
            }
        }

        public TransportInternationalPrivateLineCategory InternationalPrivateLineCategory
        {
            get
            {
                return this.internationalPrivateLineCategory;
            }
            set
            {
                this.internationalPrivateLineCategory = value;
            }
        }

        public string InternationalPrivateLineServiceTypeCode
        {
            get
            {
                return this.internationalPrivateLineServiceTypeCode;
            }
            set
            {
                this.internationalPrivateLineServiceTypeCode = value;
            }
        }

        public string InternationalPrivateLineCircuitTypeCode
        {
            get
            {
                return this.internationalPrivateLineCircuitTypeCode;
            }
            set
            {
                this.internationalPrivateLineCircuitTypeCode = value;
            }
        }

        public TransportOneStopShopOptionCode OneStopShopOptionCode
        {
            get
            {
                return this.oneStopShopOptionCode;
            }
            set
            {
                this.oneStopShopOptionCode = value;
            }
        }

        public string MPLSBackHaulFlag
        {
            get
            {
                return this.mPLSBackHaulFlagField;
            }
            set
            {
                this.mPLSBackHaulFlagField = value;
            }
        }

        public string CarrierAccessCode
        {
            get
            {
                return this.carrierAccessCode;
            }
            set
            {
                this.carrierAccessCode = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItemsField;
            }
            set
            {
                this.billingLineItemsField = value;
            }
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public enum TransportInternationalPrivateLineCategory
        {
            [System.Xml.Serialization.XmlEnumAttribute("")]
            QQQ,

            BIL,
            CAN,
            LES,
            MEX,
            OFF,
            RES,
        }

        [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public enum TransportOneStopShopOptionCode
        {
            [System.Xml.Serialization.XmlEnumAttribute("")]
            QQQ,

            SPO,
            SPB,
            SPOB,
            PPO,
            PPB,
            PPOB,
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationPort
    {
        private OrderContactInfo contactInfoField;
        private OrderInstallInfoTransportInstallInformationPortIPAddressInformation iPAddressInformationField;
        private string userTypeField;
        private string multiMegFlagField;
        private OrderInstallInfoTransportInstallSPAEAccessIndicator sPAEAccessIndicatorield;
        private string vlANQuantityField;
        private OrderInstallInfoTransportInstallInformationPortVLAN[] vLAN;
        private string ethernetInterfaceIndicatorField;
        private string scramblingField;
        private string portTypePreferenceField;
        private string connectorTypeCodeField;
        private string connectorRouterTaggingField;
        private string customerRouterAutoNegotiationField;
        private string customerProvidedIPAddressIndicatorField;
        private OrderInstallInfoTransportInstallInformationPortCustomerIPAddress[] customerIPAddressesField;

        private string burstableInternetPricingCodeField;
        private string burstableUsageTypeCodeField;
        private string pplvls;
        private OrderInstallInfoTransportInstallInterfaceProtocolCode interfaceProtocolCodeField;
        private OrderBillingLineItems[] billingLineItemsField;

        public OrderContactInfo ContactInfo
        {
            get
            {
                return this.contactInfoField;
            }
            set
            {
                this.contactInfoField = value;
            }
        }

        public OrderInstallInfoTransportInstallInformationPortIPAddressInformation IPAddressInformation
        {
            get
            {
                return this.iPAddressInformationField;
            }
            set
            {
                this.iPAddressInformationField = value;
            }
        }

        public string UserType
        {
            get
            {
                return this.userTypeField;
            }
            set
            {
                this.userTypeField = value;
            }
        }

        public string MultiMegFlag
        {
            get
            {
                return this.multiMegFlagField;
            }
            set
            {
                this.multiMegFlagField = value;
            }
        }

        public OrderInstallInfoTransportInstallSPAEAccessIndicator SPAEAccessIndicator
        {
            get
            {
                return this.sPAEAccessIndicatorield;
            }
            set
            {
                this.sPAEAccessIndicatorield = value;
            }
        }

        public string VLANQuantity
        {
            get
            {
                return this.vlANQuantityField;
            }
            set
            {
                this.vlANQuantityField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("VLAN", IsNullable = false)]
        public OrderInstallInfoTransportInstallInformationPortVLAN[] VLANS
        {
            get
            {
                return this.vLAN;
            }
            set
            {
                this.vLAN = value;
            }
        }

        public string EthernetInterfaceIndicator
        {
            get
            {
                return this.ethernetInterfaceIndicatorField;
            }
            set
            {
                this.ethernetInterfaceIndicatorField = value;
            }
        }

        public string Scrambling
        {
            get
            {
                return this.scramblingField;
            }
            set
            {
                this.scramblingField = value;
            }
        }

        public string PortTypePreference
        {
            get
            {
                return this.portTypePreferenceField;
            }
            set
            {
                this.portTypePreferenceField = value;
            }
        }

        public string ConnectorTypeCode
        {
            get
            {
                return this.connectorTypeCodeField;
            }
            set
            {
                this.connectorTypeCodeField = value;
            }
        }

        public string ConnectorRouterTagging
        {
            get
            {
                return this.connectorRouterTaggingField;
            }
            set
            {
                this.connectorRouterTaggingField = value;
            }
        }

        public string CustomerRouterAutoNegotiation
        {
            get
            {
                return this.customerRouterAutoNegotiationField;
            }
            set
            {
                this.customerRouterAutoNegotiationField = value;
            }
        }

        public string CustomerProvidedIPAddressIndicator
        {
            get
            {
                return this.customerProvidedIPAddressIndicatorField;
            }
            set
            {
                this.customerProvidedIPAddressIndicatorField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("CustomerIPAddress", IsNullable = false)]
        public OrderInstallInfoTransportInstallInformationPortCustomerIPAddress[] CustomerIPAddresses
        {
            get
            {
                return this.customerIPAddressesField;
            }
            set
            {
                this.customerIPAddressesField = value;
            }
        }

        public string BurstableInternetPricingCode
        {
            get
            {
                return this.burstableInternetPricingCodeField;
            }
            set
            {
                this.burstableInternetPricingCodeField = value;
            }
        }

        public string BurstableUsageTypeCode
        {
            get
            {
                return this.burstableUsageTypeCodeField;
            }
            set
            {
                this.burstableUsageTypeCodeField = value;
            }
        }

        public string PPLVLS
        {
            get
            {
                return this.pplvls;
            }
            set
            {
                this.pplvls = value;
            }
        }

        public OrderInstallInfoTransportInstallInterfaceProtocolCode InterfaceProtocolCode;

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItemsField;
            }
            set
            {
                this.billingLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationPortIPAddressInformation
    {
        private string iPVersionTypeCodeField;
        private string iPv4AddressProviderCodeField;
        private string iPv4AddressQuantityField;
        private string iPv6AddressProviderCodeField;
        private string iPv6AddressQuantityField;
        private string domainName;
        private EnumBoolYesNoFlag additionalIPAddressFlag;
        private string iPv4SubnetMaskField;
        private string existingIPConnectionFlagField;
        private string currentInternetProviderCodeField;
        public OrderInstallInfoTransportInstallInformationPortIPAddress[] iPAddressesField;

        public string IPVersionTypeCode
        {
            get
            {
                return this.iPVersionTypeCodeField;
            }
            set
            {
                this.iPVersionTypeCodeField = value;
            }
        }

        public string IPv4AddressProviderCode
        {
            get
            {
                return this.iPv4AddressProviderCodeField;
            }
            set
            {
                this.iPv4AddressProviderCodeField = value;
            }
        }

        public string IPv4AddressQuantity
        {
            get
            {
                return this.iPv4AddressQuantityField;
            }
            set
            {
                this.iPv4AddressQuantityField = value;
            }
        }

        public string IPv6AddressProviderCode
        {
            get
            {
                return this.iPv6AddressProviderCodeField;
            }
            set
            {
                this.iPv6AddressProviderCodeField = value;
            }
        }

        public string IPv6AddressQuantity
        {
            get
            {
                return this.iPv6AddressQuantityField;
            }
            set
            {
                this.iPv6AddressQuantityField = value;
            }
        }

        public string DomainName
        {
            get
            {
                return this.domainName;
            }
            set
            {
                this.domainName = value;
            }
        }

        public EnumBoolYesNoFlag AdditionalIPAddressFlag
        {
            get
            {
                return this.additionalIPAddressFlag;
            }
            set
            {
                this.additionalIPAddressFlag = value;
            }
        }

        public string IPv4SubnetMask
        {
            get
            {
                return this.iPv4SubnetMaskField;
            }
            set
            {
                this.iPv4SubnetMaskField = value;
            }
        }

        public string ExistingIPConnectionFlag
        {
            get
            {
                return this.existingIPConnectionFlagField;
            }
            set
            {
                this.existingIPConnectionFlagField = value;
            }
        }

        public string CurrentInternetProviderCode
        {
            get
            {
                return this.currentInternetProviderCodeField;
            }
            set
            {
                this.currentInternetProviderCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("IPAddress", IsNullable = false)]
        public OrderInstallInfoTransportInstallInformationPortIPAddress[] IPAddresses
        {
            get
            {
                return this.iPAddressesField;
            }
            set
            {
                this.iPAddressesField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationPortCustomerIPAddress
    {
        private string iPAddressField;

        public string IPAddress
        {
            get
            {
                return this.iPAddressField;
            }
            set
            {
                this.iPAddressField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationPortVLAN
    {
        private string vLANIDField;
        private string allocationPercentageField;

        public string VLANID
        {
            get
            {
                return this.vLANIDField;
            }
            set
            {
                this.vLANIDField = value;
            }
        }

        public string AllocationPercentage
        {
            get
            {
                return this.allocationPercentageField;
            }
            set
            {
                this.allocationPercentageField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderInstallInfoTransportInstallInterfaceProtocolCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        CISCO,
        ANSI,
        NONE,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderInstallInfoTransportInstallSPAEAccessIndicator
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Dedicated,
        Aggregated,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderInstallInfoTransportInstallInformationPortIPAddress
    {
        private string iPAddressField;

        public string IPAddress
        {
            get
            {
                return this.iPAddressField;
            }
            set
            {
                this.iPAddressField = value;
            }
        }

        private string iPAddressTypeField;

        public string IPAddressTypeCode
        {
            get
            {
                return this.iPAddressTypeField;
            }
            set
            {
                this.iPAddressTypeField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfo
    {
        private string reasonCodeField;
        private string cancelBeforeStartReasonCodeField;
        private string cancelBeforeStartReasonDetailField;
        private OrderContactInfoData contactNameInfoField;
        private OrderPhoneInfoData phoneInfoField;
        private OrderDisconnectInfoMDSDisconnectInformation mDSDisconnectInformationField;
        private OrderDisconnectInfoValueAddedServicesDisconnectInformation valueAddedServicesDisconnectInformationField;
        private OrderDisconnectInfoTransportDisconnectInformation transportDisconnectInformationField;
        private OrderDisconnectInfoCPEDisconnectInformation cPEDisconnectInformationField;
        private OrderBillingLineItems[] billingLineItems;
        private SIPTrunkInformation sIPTrunkDisconnectInformation;

        public string ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        public string CancelBeforeStartReasonCode
        {
            get
            {
                return this.cancelBeforeStartReasonCodeField;
            }
            set
            {
                this.cancelBeforeStartReasonCodeField = value;
            }
        }

        public string CancelBeforeStartReasonDetail
        {
            get
            {
                return this.cancelBeforeStartReasonDetailField;
            }
            set
            {
                this.cancelBeforeStartReasonDetailField = value;
            }
        }

        public OrderContactInfoData ContactNameInfo
        {
            get
            {
                return this.contactNameInfoField;
            }
            set
            {
                this.contactNameInfoField = value;
            }
        }

        public OrderPhoneInfoData PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }

        public OrderDisconnectInfoMDSDisconnectInformation MDSDisconnectInformation
        {
            get
            {
                return this.mDSDisconnectInformationField;
            }
            set
            {
                this.mDSDisconnectInformationField = value;
            }
        }

        public OrderDisconnectInfoValueAddedServicesDisconnectInformation ValueAddedServicesDisconnectInformation
        {
            get
            {
                return this.valueAddedServicesDisconnectInformationField;
            }
            set
            {
                this.valueAddedServicesDisconnectInformationField = value;
            }
        }

        public OrderDisconnectInfoTransportDisconnectInformation TransportDisconnectInformation
        {
            get
            {
                return this.transportDisconnectInformationField;
            }
            set
            {
                this.transportDisconnectInformationField = value;
            }
        }

        public OrderDisconnectInfoCPEDisconnectInformation CPEDisconnectInformation
        {
            get
            {
                return this.cPEDisconnectInformationField;
            }
            set
            {
                this.cPEDisconnectInformationField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItems;
            }
            set
            {
                this.billingLineItems = value;
            }
        }

        public SIPTrunkInformation SIPTrunkDisconnectInformation
        {
            get
            {
                return this.sIPTrunkDisconnectInformation;
            }
            set
            {
                this.sIPTrunkDisconnectInformation = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderBillingInfo
    {
        private OrderBillingLineItems[] billingLineItems;

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItems;
            }
            set
            {
                this.billingLineItems = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoMDSDisconnectInformation
    {
        private OrderDisconnectInfoMDSDisconnectInformationMDSLineItem[] mDSLineItemsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("MDSLineItem", IsNullable = false)]
        public OrderDisconnectInfoMDSDisconnectInformationMDSLineItem[] MDSLineItems
        {
            get
            {
                return this.mDSLineItemsField;
            }
            set
            {
                this.mDSLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoMDSDisconnectInformationMDSLineItem
    {
        private string serviceLineItemCodeField;
        private string descriptionField;
        private string originalInstallOrderID;

        public string ServiceLineItemCode
        {
            get
            {
                return this.serviceLineItemCodeField;
            }
            set
            {
                this.serviceLineItemCodeField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public string OriginalInstallOrderID
        {
            get
            {
                return this.originalInstallOrderID;
            }
            set
            {
                this.originalInstallOrderID = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoValueAddedServicesDisconnectInformation
    {
        private OrderDisconnectInfoValueAddedServicesDisconnectInformationValueAddedServicesDisconnectLineItem[] valueAddedServicesDisconnectLineItemsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("ValueAddedServicesDisconnectLineItem", IsNullable = false)]
        public OrderDisconnectInfoValueAddedServicesDisconnectInformationValueAddedServicesDisconnectLineItem[] ValueAddedServicesDisconnectLineItems
        {
            get
            {
                return this.valueAddedServicesDisconnectLineItemsField;
            }
            set
            {
                this.valueAddedServicesDisconnectLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoValueAddedServicesDisconnectInformationValueAddedServicesDisconnectLineItem
    {
        private string typeField;
        private string networkAddressField;
        private string carrierServiceID;
        private string carrierCircuitID;
        private string originalInstallOrderID;

        public string CarrierServiceID
        {
            get
            {
                return this.carrierServiceID;
            }
            set
            {
                this.carrierServiceID = value;
            }
        }

        public string CarrierCircuitID
        {
            get
            {
                return this.carrierCircuitID;
            }
            set
            {
                this.carrierCircuitID = value;
            }
        }

        public string OriginalInstallOrderID
        {
            get
            {
                return this.originalInstallOrderID;
            }
            set
            {
                this.originalInstallOrderID = value;
            }
        }

        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        public string NetworkAddress
        {
            get
            {
                return this.networkAddressField;
            }
            set
            {
                this.networkAddressField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoTransportDisconnectInformation
    {
        private OrderDisconnectInfoTransportDisconnectInformationTransportDisconnectLineItem[] transportDisconnectLineItemsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("TransportDisconnectLineItem", IsNullable = false)]
        public OrderDisconnectInfoTransportDisconnectInformationTransportDisconnectLineItem[] TransportDisconnectLineItems
        {
            get
            {
                return this.transportDisconnectLineItemsField;
            }
            set
            {
                this.transportDisconnectLineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoTransportDisconnectInformationTransportDisconnectLineItem
    {
        private string privateLineNumberField;
        private string fMSCircuitNumberField;
        private string networkAddressField;
        private string accessTypeCode;
        private string accessTypeDescription;
        private string speedOfService;
        private string carrierServiceID;
        private string carrierCircuitID;
        private string originalInstallOrderID;

        public string AccessTypeCode
        {
            get
            {
                return this.accessTypeCode;
            }
            set
            {
                this.accessTypeCode = value;
            }
        }

        public string AccessTypeDescription
        {
            get { return accessTypeDescription; }
            set { accessTypeDescription = value; }
        }

        public string SpeedOfService
        {
            get
            {
                return this.speedOfService;
            }
            set
            {
                this.speedOfService = value;
            }
        }

        public string CarrierServiceID
        {
            get
            {
                return this.carrierServiceID;
            }
            set
            {
                this.carrierServiceID = value;
            }
        }

        public string CarrierCircuitID
        {
            get
            {
                return this.carrierCircuitID;
            }
            set
            {
                this.carrierCircuitID = value;
            }
        }

        public string OriginalInstallOrderID
        {
            get
            {
                return this.originalInstallOrderID;
            }
            set
            {
                this.originalInstallOrderID = value;
            }
        }

        public string PrivateLineNumber
        {
            get
            {
                return this.privateLineNumberField;
            }
            set
            {
                this.privateLineNumberField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string FMSCircuitNumber
        {
            get
            {
                return this.fMSCircuitNumberField;
            }
            set
            {
                this.fMSCircuitNumberField = value;
            }
        }

        public string NetworkAddress
        {
            get
            {
                return this.networkAddressField;
            }
            set
            {
                this.networkAddressField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoCPEDisconnectInformation
    {
        private OrderDisconnectInfoCPEDisconnectInformationCPELineItem[] cPELineItemsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("CPELineItem", IsNullable = false)]
        public OrderDisconnectInfoCPEDisconnectInformationCPELineItem[] CPELineItems
        {
            get
            {
                return this.cPELineItemsField;
            }
            set
            {
                this.cPELineItemsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderDisconnectInfoCPEDisconnectInformationCPELineItem
    {
        private string componentIDField;
        private string equipmentTypeCodeField;
        private string equipmentIDField;
        private string descriptionField;
        private OrderDisconnectInfoCPEDisconnectInformationCPELineItemContractType contractTypeField;
        private string contractTermField;
        private string originalInstallOrderID;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string ComponentID
        {
            get
            {
                return this.componentIDField;
            }
            set
            {
                this.componentIDField = value;
            }
        }

        public string EquipmentTypeCode
        {
            get
            {
                return this.equipmentTypeCodeField;
            }
            set
            {
                this.equipmentTypeCodeField = value;
            }
        }

        public string EquipmentID
        {
            get
            {
                return this.equipmentIDField;
            }
            set
            {
                this.equipmentIDField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public OrderDisconnectInfoCPEDisconnectInformationCPELineItemContractType ContractType
        {
            get
            {
                return this.contractTypeField;
            }
            set
            {
                this.contractTypeField = value;
            }
        }

        public string ContractTerm
        {
            get
            {
                return this.contractTermField;
            }
            set
            {
                this.contractTermField = value;
            }
        }

        public string OriginalInstallOrderID
        {
            get
            {
                return this.originalInstallOrderID;
            }
            set
            {
                this.originalInstallOrderID = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderDisconnectInfoCPEDisconnectInformationCPELineItemContractType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Purchase,
        Rental,
        Installment,
        Customer,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderChangeInformation
    {
        private string changeDescription;
        private string carrierServiceID;
        private string carrierCircuitID;
        private string originalInstallOrderID;
        private string networkUserAddress;
        private OrderChangeInformationPortRateTypeCode portRateTypeCode;
        private OrderBillingLineItems[] billingLineItems;

        public string ChangeDescription
        {
            get
            {
                return this.changeDescription;
            }
            set
            {
                this.changeDescription = value;
            }
        }

        public string CarrierServiceID
        {
            get
            {
                return this.carrierServiceID;
            }
            set
            {
                this.carrierServiceID = value;
            }
        }

        public string CarrierCircuitID
        {
            get
            {
                return this.carrierCircuitID;
            }
            set
            {
                this.carrierCircuitID = value;
            }
        }

        public string OriginalInstallOrderID
        {
            get
            {
                return this.originalInstallOrderID;
            }
            set
            {
                this.originalInstallOrderID = value;
            }
        }

        public string NetworkUserAddress
        {
            get { return networkUserAddress; }
            set { networkUserAddress = value; }
        }

        public OrderChangeInformationPortRateTypeCode PortRateTypeCode
        {
            get { return portRateTypeCode; }
            set { portRateTypeCode = value; }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BillingLineItem", IsNullable = false)]
        public OrderBillingLineItems[] BillingLineItems
        {
            get
            {
                return this.billingLineItems;
            }
            set
            {
                this.billingLineItems = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderChangeInformationPortRateTypeCode
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        Usage,
        Flat,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class OrderNotes
    {
        private OrderNotesNoteNoteType noteTypeField;
        private string noteTextField;

        public OrderNotesNoteNoteType NoteType
        {
            get
            {
                return this.noteTypeField;
            }
            set
            {
                this.noteTypeField = value;
            }
        }

        public string NoteText
        {
            get
            {
                return this.noteTextField;
            }
            set
            {
                this.noteTextField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum OrderNotesNoteNoteType
    {
        [System.Xml.Serialization.XmlEnumAttribute("")]
        QQQ,

        [System.Xml.Serialization.XmlEnumAttribute("FMS-ES")]
        FMSES = 4,

        NetworkEngineer = 5,
        CPE = 6,
        Pricing = 7,
        International = 19,
        Cancel = 20,
    }
}