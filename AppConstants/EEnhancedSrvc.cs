﻿namespace AppConstants
{
    public enum EEnhancedSrvc
    {
        STANDARD = 1,
        VAS,
        SLNKSTANDARD,
        NGVNSTANDARD,
        MDSSTANDARD,
        FirewallSecurity,
        ADBROADBAND,
        ADNARROWBAND,
        ADGI,
        MDSFASTTRACK
    }
}