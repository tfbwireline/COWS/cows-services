﻿using DataManager;

namespace FSAHelper
{
    public class SIP_TRNK_GRP_DB : MSSqlBase
    {
        private int _SIP_TRNK_GRP_ID = 0;

        public int SIP_TRNK_GRP_ID
        {
            get { return _SIP_TRNK_GRP_ID; }
            set { _SIP_TRNK_GRP_ID = value; }
        }

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private int _CMPNT_ID = 0;

        public int CMPNT_ID
        {
            get { return _CMPNT_ID; }
            set { _CMPNT_ID = value; }
        }

        private int _SIP_TRNK_QTY = 0;

        public int SIP_TRNK_QTY
        {
            get { return _SIP_TRNK_QTY; }
            set { _SIP_TRNK_QTY = value; }
        }

        private string _NW_USER_ADR = "";

        public string NW_USER_ADR
        {
            get { return _NW_USER_ADR; }
            set { _NW_USER_ADR = value; }
        }

        private bool _IS_INSTL_CD = false;

        public bool IS_INSTL_CD
        {
            get { return _IS_INSTL_CD; }
            set { _IS_INSTL_CD = value; }
        }

        public int InsertSIPTrunkGroup()
        {
            setCommand("dbo.insertSIPTrunkGroup");
            addIntParam("@SIP_TRNK_GRP_ID", _SIP_TRNK_GRP_ID, true);
            addIntParam("@ORDR_ID", _ORDR_ID);
            addIntParam("@CMPNT_ID", _CMPNT_ID);
            addIntParam("@SIP_TRNK_QTY", _SIP_TRNK_QTY);
            addStringParam("@NW_USER_ADR", _NW_USER_ADR);
            addBooleanParam("@IS_INSTL_CD", _IS_INSTL_CD);

            return execNoQueryOut("@SIP_TRNK_GRP_ID", true);
        }
    }
}