/*******************************************************************************
* Copyright (c) 2007 by Sprint Nextel
*
* All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
* and is the sole and exclusive property of the copyright owner. 
*
* Author:
*   Woody Anderson - initial implementation
*
* synopsis: script to uninstall a given service
*
* arguments: requires one argument, the name of the service to uninstall
*
*******************************************************************************/ 

  // make sure argument is passed
var error = null, state = null, msg = null, service_name = null, exitCode = 0;
if (WScript.Arguments.Count() != 1)
{
  error = "service name is required";
  exitCode = 100;
}

  // query for service and iterate over results
else
{
  service_name = WScript.Arguments(0);
  var objWMIService = GetObject("winmgmts:");
  var query = "SELECT * FROM win32_service WHERE name = '"+service_name+"'";
  var services = objWMIService.ExecQuery(query);
  var enumerator = new Enumerator(services);
  if (enumerator.atEnd())
  {
    error = "service '"+service_name+"' not found";
    exitCode = 101;
  }
  else
  {
    state = enumerator.item().state;
    if ("Stopped" != state)
    {
      error = "service '"+service_name+"' must be in stopped state before uninstalling";
      exitCode = 102;
    }
    else
    {
      var result = enumerator.item().Delete();
      if (result != 0)
      {
        error = "request to remove '"+service_name+"' failed, error code = "+result;
        exitCode = result;
      }
    }
  }
}

  // print message about results, log results, then exit
if (error != null) msg = "ERROR: "+error;
else msg = "INFO: service '"+service_name+"' successfully uninstalled"; 
WScript.Echo(msg);
logMsg(msg,exitCode);
WScript.Quit(exitCode);


function logMsg ( msg, code )
{
    // get name of log script
  var objFSO = WScript.CreateObject("Scripting.FileSystemObject");
  var objFile = objFSO.GetFile(WScript.ScriptFullName);
  var logScript = objFSO.GetParentFolderName(objFile)+"\\_logJobMsg.js";

    // call log script
  var cmd = "cscript.exe "+logScript+" "+WScript.ScriptName+" "+code+" \""+msg+"\"";
  var shell = WScript.CreateObject("WScript.Shell");
  shell.Run(cmd,7,true);
}
