﻿namespace COWS.PeopleSoftBatchService
{
    public class RecordLayouts

    {
        public string hdrFormat = "{0,-10}" +     //Requisition Header    10 bytes
                                    "{1,5:N0}" +    //Line number           5 numeric
                                    "{2,-1}" +      //Transaction Type      1 byte
                                    "{3,-10}" +     //ELID                  10 bytes
                                    "{4,-10}" +     //Deliver Code          10 bytes
                                    "{5,-30}" +     //Deliver Name          30 bytes
                                    "{6,-35}" +     //Deliver Address1      35 bytes
                                    "{7,-35}" +     //Deliver Address2      35 bytes
                                    "{8,-35}" +     //Deliver Address3      35 bytes
                                    "{9,-35}" +     //Deliver Address4      35 bytes
                                    "{10,-30}" +     //Deliver City          30 bytes
                                    "{11,-3}" +     //Deliver Country        3 bytes
                                    "{12,-2}" +     //Deliver State          2 bytes
                                    "{13,-12}" +    //Deliver Zip           12 bytes
                                    "{14,-24}" +    //Deliver phone         24 bytes
                                    "{15,-10}" +    //Install Code          10 bytes
                                    "{16,-20}" +    //Mark Package          20 bytes
                                    "{17,8}" +      //Requistion Date        8 numeric
                                    "{18,12}" +     //Refrenece number      12 bytes
                                    "{19,-4}" +     //Comment Length         4 numeric
                                    "{20,-15}" +    // "Bldg: Building      15 bytes
                                    "{21,-9}" +     //"Flr: Floor            9 bytes
                                    "{22,-8}" +     // "Rm: Room             8 bytes
                                    "{23,-497}" +    //Comments            497 bytes
                                    "{24,1}" +      //RFQ Indicator          1 bytes
                                    "{25,255}";    //Filler               225 bytes

        public string lineFormat = "{0,-10}" +      //Requisition Header   10 bytes
                                   "{1,5:N0}" +        //Line number           5 numeric
                                   "{2,-6}" +       //MatCode               6 byte
                                   "{3,12}" +       //filler               12 bytes
                                   "{4,-254}" +     //Item Desc           254 bytes
                                   "{5,-20}" +      //Part Number          20 bytes
                                   "{6,-10}" +      //Manf id              10 bytes
                                   "{7,11:N0}" +    //Order Qty            11 numeric
                                   "{8,-3}" +       //Unit Measure          3 bytes
                                   "{9,15:N4}" +    //Unit Price           15 numeric
                                   "{10,8}" +       //RAS Date              8 numeric
                                   "{11,10}" +      //Suggested Vendor      10
                                   "{12,-5}" +      //Bus Unit/GL           5 bytes
                                   "{13,-6}" +      //Account               6 bytes
                                   "{14,-10}" +     //Cost Center          10 bytes
                                   "{15,-6}" +      //Product               6 bytes
                                   "{16,-4}" +      //Market                4 bytes
                                   "{17,-5}" +      //Affiliate             5 bytes
                                   "{18,-6}" +      //Region                6 bytes
                                   "{19,-15}" +     //Project Id           15 bytes
                                   "{20,-5}" +      //PC Bus Unit           5 bytes
                                   "{21,-15}" +     //Activity             15 bytes
                                   "{22,-5}" +      //Source Type           5 bytes
                                   "{23,-5}" +      //Resource Category     5 bytes
                                   "{24,-5}" +      //Resource SubCategory  5 bytes
                                   "{25,-25}" +     //Contract Id          25 bytes
                                   "{26,-5}" +      //Contract Line Nbr     5 numeric
                                   "{27,-24}" +     //Auxiliary ID         24 bytes
                                   "{28,-10}" +     //Install Code         10 bytes
                                   "{29,-1}" +      //RFQ Indicator         1 bytes
                                   "{30,-4}" +     //Comment Length        4 numeric
                                   "{31,-512}" +    //Comments            512 bytes
                                   "{32,-10}" +     //Vendor Location      10 bytes
                                   "{33,-50}";     //filler                50 bytes

        public string recHdrFormat = "{0,1}" +      //RecordTyp             1 bytes  'H'
                                     "{1,8}" +      //Transaction Date      8 numeric yyyymmdd
                                     "{2,6}" +      //Transaction Time      6 byte hhmmss
                                     "{3,3}";       //filler                3 bytes

        public string recTrlrFormat = "{0,1}" +   //RecordTyp             1 bytes  'T'
                                        "{1,18}" +   //Record Count          18 numeric
                                        "{2,3}";    //filler                3 bytes

        public string recDtlFormat = "{0,1}" +      //RecordTyp             1 bytes  'D'
                                     "{1,-10}" +       //Purchase Order ID           10 bytes
                                     "{2,-4}" +        //Purchase Order Line         4 byte
                                     "{3,-3}" +        //Schedule Number             3 bytes
                                     "{4,-10}" +       //Receipt Nbr               10 bytes  Start with Alpha
                                     "{5,-9}" +       //Quantity Received   before decimal point 13 bytes
                                     "{6,-4}" +       //Quantity Received   after decimal point   4 bytes
                                     "{7,-3}" +        //Receipt UOM               3 bytes
                                     "{8,-9}" +       //PO Quantity before decimal point   13.4 bytes
                                     "{9,-4}" +        //PO Quantity after decimal point 4 numeric
                                     "{10,-6}" +        //Receipt Date              6 numeric yymmdd
                                     "{11,-6}";        //Receipt Time               6 byte hhmmss
    }
}