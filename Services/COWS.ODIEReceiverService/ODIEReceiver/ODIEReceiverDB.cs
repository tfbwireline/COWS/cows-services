﻿using DataManager;
using System;

namespace COWS.ODIEReceiverService
{
    public class ODIEReceiverDB : MSSqlBase
    {
        #region "Properties"

        private int _REQ_ID = 0;

        public int REQ_ID
        {
            get { return _REQ_ID; }
            set { _REQ_ID = value; }
        }

        private int _ODIE_DISC_REQ_ID = 0;

        public int ODIE_DISC_REQ_ID
        {
            get { return _ODIE_DISC_REQ_ID; }
            set { _ODIE_DISC_REQ_ID = value; }
        }

        private int _H5_H6_CUST_ID = 0;

        public int H5_H6_CUST_ID
        {
            get { return _H5_H6_CUST_ID; }
            set { _H5_H6_CUST_ID = value; }
        }

        private DateTime _RSPN_DT;

        public DateTime RSPN_DT
        {
            get { return _RSPN_DT; }
            set { _RSPN_DT = value; }
        }

        private string _CUST_TEAM_PDL = string.Empty;

        public string CUST_TEAM_PDL
        {
            get { return _CUST_TEAM_PDL; }
            set { _CUST_TEAM_PDL = value; }
        }

        private string _MNSPM_ID;

        public string MNSPM_ID
        {
            get { return _MNSPM_ID; }
            set { _MNSPM_ID = value; }
        }

        private string _NTEAssigned;

        public string NTEAssigned
        {
            get { return _NTEAssigned; }
            set { _NTEAssigned = value; }
        }

        private string _SDEAssigned;

        public string SDEAssigned
        {
            get { return _SDEAssigned; }
            set { _SDEAssigned = value; }
        }

        private string _RSPN_ERROR_TXT = string.Empty;

        public string RSPN_ERROR_TXT
        {
            get { return _RSPN_ERROR_TXT; }
            set { _RSPN_ERROR_TXT = value; }
        }

        private string _CUST_NME;

        public string CUST_NME
        {
            get { return _CUST_NME; }
            set { _CUST_NME = value; }
        }

        private string _SOWS_FOLDR_PATH_NME;

        public string SOWS_FOLDR_PATH_NME
        {
            get { return _SOWS_FOLDR_PATH_NME; }
            set { _SOWS_FOLDR_PATH_NME = value; }
        }

        private string _MODEL_ID;

        public string MODEL_ID
        {
            get { return _MODEL_ID; }
            set { _MODEL_ID = value; }
        }

        private string _DEV_ID;

        public string DEV_ID
        {
            get { return _DEV_ID; }
            set { _DEV_ID = value; }
        }

        private string _MANF_ID;

        public string MANF_ID
        {
            get { return _MANF_ID; }
            set { _MANF_ID = value; }
        }

        private string _SERIAL_NO;

        public string SERIAL_NO
        {
            get { return _SERIAL_NO; }
            set { _SERIAL_NO = value; }
        }

        private string _RDSN_NBR;

        public string RDSN_NBR
        {
            get { return _RDSN_NBR; }
            set { _RDSN_NBR = value; }
        }

        private bool _FAST_TRK_CD = false;

        public bool FAST_TRK_CD
        {
            get { return _FAST_TRK_CD; }
            set { _FAST_TRK_CD = value; }
        }

        private bool _DSPCH_RDY_CD = false;

        public bool DSPCH_RDY_CD
        {
            get { return _DSPCH_RDY_CD; }
            set { _DSPCH_RDY_CD = value; }
        }

        private DateTime _RSPN_INFO_DT;

        public DateTime RSPN_INFO_DT
        {
            get { return _RSPN_INFO_DT; }
            set { _RSPN_INFO_DT = value; }
        }

        private bool _SLCTD_CD = false;

        public bool SLCTD_CD
        {
            get { return _SLCTD_CD; }
            set { _SLCTD_CD = value; }
        }

        private bool _ACK_CD = false;

        public bool ACK_CD
        {
            get { return _ACK_CD; }
            set { _ACK_CD = value; }
        }

        private string _ODIE_CUST_ID = "";

        public string ODIE_CUST_ID
        {
            get { return _ODIE_CUST_ID; }
            set { _ODIE_CUST_ID = value; }
        }

        private string _H6_CUST_ID;

        public string H6_CUST_ID
        {
            get { return _H6_CUST_ID; }
            set { _H6_CUST_ID = value; }
        }

        private string _M5_DEVICE_ID;

        public string M5_DEVICE_ID
        {
            get { return _M5_DEVICE_ID; }
            set { _M5_DEVICE_ID = value; }
        }

        private string _SITE_ID;

        public string SITE_ID
        {
            get { return _SITE_ID; }
            set { _SITE_ID = value; }
        }

        private string _CTRCT_NBR;

        public string CTRCT_NBR
        {
            get { return _CTRCT_NBR; }
            set { _CTRCT_NBR = value; }
        }

        #endregion "Properties"

        #region "Methods"

        public int InsertODIEResponse()
        {
            setCommand("dbo.insertODIEResponse");
            addIntParam("@REQ_ID", _REQ_ID);
            addDateParam("@RSPN_DT", _RSPN_DT);
            addStringParam("@CUST_TEAM_PDL", _CUST_TEAM_PDL);
            addStringParam("@MNSPM_ID", _MNSPM_ID);
            addStringParam("@NTEAssigned", _NTEAssigned);
            addStringParam("@RSPN_ERROR_TXT", _RSPN_ERROR_TXT);
            addStringParam("@CUST_NME_LIST", _CUST_NME);
            addStringParam("@SOWS_FOLDR_PATH_NME", _SOWS_FOLDR_PATH_NME);
            addBooleanParam("@ACK_CD", _ACK_CD);
            addStringParam("@ODIE_CUST_ID", _ODIE_CUST_ID);
            addStringParam("@SDEAssigned", _SDEAssigned);
            return execNoQuery();
        }

        public int InsertODIEResponseInfo()
        {
            setCommand("dbo.insertODIEResponseInfo");
            addIntParam("@REQ_ID", _REQ_ID);
            addStringParam("@MODEL_ID", _MODEL_ID);
            addStringParam("@DEV_ID", _DEV_ID);
            addStringParam("@SERIAL_NO", _SERIAL_NO);
            addStringParam("@RDSN_NBR", _RDSN_NBR);
            addBooleanParam("@FAST_TRK_CD", _FAST_TRK_CD);
            addBooleanParam("@DSPCH_RDY_CD", _DSPCH_RDY_CD);
            addDateParam("@RSPN_INFO_DT", _RSPN_INFO_DT);
            addBooleanParam("@SLCTD_CD", _SLCTD_CD);
            addStringParam("@MANF_ID", _MANF_ID);
            addStringParam("@H6_CUST_ID", H6_CUST_ID);

            return execNoQuery();
        }

        public void UpdateODIERequest(int _Status)
        {
            setCommand("dbo.updateODIERequest");
            addIntParam("@REQ_ID", _REQ_ID);
            addIntParam("@STUS_ID", _Status);

            int i = execNoQuery();
        }

        public int UpdateODIEDiscoRequest()
        {
            setCommand("dbo.updateODIEDiscoResponse");
            addIntParam("@REQ_ID", _REQ_ID);
            addIntParam("@ODIE_DISC_REQ_ID", _ODIE_DISC_REQ_ID);
            addIntParam("@H5_H6_CUST_ID", _H5_H6_CUST_ID);
            addStringParam("@MODEL_NME", _MODEL_ID);
            addStringParam("@VNDR_NME", _DEV_ID);
            addStringParam("@SERIAL_NBR", _SERIAL_NO);
            addStringParam("@M5_DEVICE_ID", _M5_DEVICE_ID);
            addStringParam("@SITE_ID", _SITE_ID);
            addStringParam("@CTRCT_NBR", _CTRCT_NBR);

            return execNoQuery();
        }

        #endregion "Methods"
    }
}