/*******************************************************************************
* Copyright (c) 2007 by Sprint Nextel
*
* All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
* and is the sole and exclusive property of the copyright owner. 
*
* Author:
*   Woody Anderson - initial implementation
*
* synopsis: script to log messages from job scripts
*
* arguments: required three arguments: calling script name, code, and message
*
*******************************************************************************/ 

var error = null;
var logDir = getLogDir();
var logFile = logDir+"\\jobs.log";

  // make sure proper number of arguments passed
if (WScript.Arguments.Count() != 3)
{
  error = "wrong number of arguments (3 expected, "+WScript.Arguments.Count()+" found)";
}
else
{
  var caller = WScript.Arguments(0);
  var exitCode = WScript.Arguments(1);
  var msg = WScript.Arguments(2);

    // see if logfile dir exists
  var fso = WScript.CreateObject("Scripting.FileSystemObject");
  if (!fso.FolderExists(logDir))
  {
    error = "logging directory '"+logDir+"' not found";
  }

    // see if we need to create the log file
  else
  {
    if (!fso.FileExists(logFile))
    {
      fso.CreateTextFile(logFile);
    }

      // now write log message
    var file = fso.OpenTextFile(logFile,8);
    file.Writeline(getTimestamp()+" "+caller+" "+exitCode+" "+msg);
    file.Close();
  }
}

  // if there was an error, print error message and exit
if (error != null)
{
  WScript.Echo("ERROR: _logMsg.js: "+error);
  WScript.Quit(1);
}


function getTimestamp()
{
  var date = new Date();
  var timestamp = date.getYear() + "-" + getField(date.getMonth()+1) + "-"
    + getField(date.getDate()) + " " + getField(date.getHours()) + ":" 
    + getField(date.getMinutes()) + ":" + getField(date.getSeconds());
  return timestamp;
}


function getField ( value )
{
  value = ""+value;
  if (value.length == 1) value = "0"+value;
  return value;
}


function getLogDir()
{
  var logdir = _getLogDir("config.properties.local");
  if (logdir) return logdir;
  logdir = _getLogDir("config.properties");
  if (!logdir)
  {
    if (!error) error = "unable to get logdir from config";
    WScript.Echo("ERROR: "+error);
    WScript.Quit(1);
  }
  else
  {
    error = null;
  }
  return logdir;
}


function _getLogDir ( cfgname )
{
  var fso = WScript.CreateObject("Scripting.FileSystemObject");
  var self = fso.GetFile(WScript.ScriptFullName);
  var dir = fso.GetParentFolderName(self);
  dir = fso.GetParentFolderName(dir);
  if (dir.match("\\\\Staging$")) dir = dir.substring(0,dir.length-8);
  var name = dir + "\\Framework\\etc\\" + cfgname;
  if (!fso.FileExists(name))
  {
    return null;
  }
  else
  {
    var cfgFile = fso.GetFile(name);
    var stream = cfgFile.OpenAsTextStream(1);
    while (!stream.AtEndOfStream)
    {
      var line = stream.ReadLine();
      for ( index = 0; index < (line.length-1) && line.charAt(index) <= ' '; index++ );
      if (index == line.length || line.charAt(index) == '#') continue;
      if (line.indexOf("LOGFILE_HOME") != index) continue;
      for ( index += 12; 
        index < line.length && (line.charAt(index) <= ' ' || line.charAt(index) == '=');
          index++ );
      if (index < line.length-1) 
      {
        while (line.indexOf("\\\\") >= 0) line = line.replace("\\\\","\\");
        return line.substring(index);
      }
      error = "LOGFILE_HOME setting in configuration is blank";
      break;
    }
    if (!error) error = "LOGFILE_HOME setting not found in configuration";
  }
  if (error) return null;
}
