<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt">
<xsl:template match="/">
	<html>
		<body width="99%">
			Dear Logicalis team,
			<br /><br />
			This is to terminate the following CPE rental order:
			<br /><br />
			<span style="font-weight:900">Customer Details</span>
			<br />
			<table width="99%" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Customer: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/Customer" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Address: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/Address" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Site Contact: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/SiteContact" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Site Office Phone: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/SiteOfficePhone" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Site Mobile Phone: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/SiteMobilePhone" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Site Email: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CustomerDetails/SiteEmail" /></td>
				</tr>
			</table>
			<br /><br />
			<span style="font-weight:900">CPE</span>
			<br />
			<table width="99%" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Original PO#: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CPE/OriginalPO" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Disconnection Date: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CPE/DisconnectionDate" /></td>
				</tr>
				<tr>
					<td width="30%" style="text-align: left; font-weight: 600">Special Remarks: </td>
					<td width="70%"><xsl:value-of select="/CPEDisconnectEmail/CPE/SpecialRemarks" /></td>
				</tr>
			</table>
			<br /><br />
			<span style="font-weight:900">Please arrange to recover the device accordingly. </span>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>