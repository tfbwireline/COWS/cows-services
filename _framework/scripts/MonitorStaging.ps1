## MonitorStaging.ps1 v1.0ps1
##
## This Script will Verify the Staging folder is empty, if not it will email the content
## to a specified email address
##
## Four parameters should be used:
##  -LOCATION supplies the directory of Staging Folder.
##
##  -EMAIL_ADDRESS is the email address you want the notification sent to.
##
##  -SERVER is the current server the script is running on.
##
## The script makes use of SQL Server Management Objects (SMO) thus must be run from
## a computer that has SQL SMO installed.  This script was written to be run locally on
## a SQL server to invoke agent jobs for the purpose of FIM deployments.  The author is
## not a SQL Server expert, rather an occasional user for the greater good of FIM and ILM
## projects.
##


PARAM
(
    [Parameter(Mandatory = $false)]
    [String]$LOCATION = [String]::Empty,

    [Parameter(Mandatory = $false)]
    [String]$EMAIL_ADDRESS = [String]::Empty,

    [Parameter(Mandatory = $false)]
    [String]$SERVER = [String]::Empty

);
#Assign Variables

$SMTPSERVER = "mailhost.corp.sprint.com"
$SUBJECT="Files Found in Staging on " + $SERVER + " in Directory " + $LOCATION + "."
$FILES=get-childitem $LOCATION *.cer|where-object {!($_.psiscontainer)}
$MESSAGE=""
$HEADER="There appears to be files left within " + $LOCATION + " on " + $SERVER + ".`n`n
The Top level view is listed below for your review. `n`n"



If (Test-Path -Path $LOCATION\*) {
  $items = Get-ChildItem -Path $LOCATION
  foreach($file in $items) { 
    $MESSAGE+= $LOCATION + "\" + $file.Name + "`n"
  }         
} else {
  Write-Host "Staging folder is empty1."
}



If ($MESSAGE -ne "") {
  $MESSAGE=$HEADER + $MESSAGE
  Write-Host $MESSAGE
  $smtp = new-object Net.Mail.SmtpClient($SMTPSERVER)
  $smtp.Send($EMAIL_ADDRESS, $EMAIL_ADDRESS, $SUBJECT, $MESSAGE)
  Write-Host "Email Sent \n\n" + $MESSAGE 

} else {
  Write-Host "Staging folder is empty2."
}





function Test-EmptyFolder {
<#
	.SYNOPSIS
		Tests for empty folders.

	.DESCRIPTION
		The Test-EmptyFolder function tests if a specified folder is empty by checking if it
		contains files or folders. The function returns the folder path and a boolean value for empty.

	.PARAMETER Path
		Specifies the path of the folder to test.

	.EXAMPLE
		Test-EmptyFolder
		Returns if the current path is an empty folder.

	.EXAMPLE
		Test-EmptyFolder C:\temp
		Returns if the path C:\temp is an empty folder.

	.EXAMPLE
		dir $env:TEMP -Recurse | Test-EmptyFolder
		Gets a directory listing of the TEMP folder including recursive items and
		passes each object into the pipeline testing for empty folders.

	.INPUTS
		System.String

	.OUTPUTS
		PSObject

	.NOTES
		Name: Test-EmptyFolder
		Author: Rich Kusak
		Created: 2011-09-25
		LastEdit: 2011-10-03 10:44
		Version: 1.0.0.0

	.LINK
		http://msdn.microsoft.com/en-us/library/kx74h1k4.aspx

#>

	[CmdletBinding()]
	param (
		[Parameter(Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
		[ValidateScript({
			if (Test-Path -LiteralPath $_) {$true} else {
				throw "The argument '$_' does not specify an existing path."
			}
		})]
		[Alias('FullName')]
		[string]$Path = '.'
	)
	
	begin {
	
		$properties = 'Path','IsEmpty'

	} # begin
	
	process {
	
		if (Test-Path -LiteralPath $Path -PathType Leaf) {
			return Write-Verbose "The path '$Path' is a file. Only folders are tested."
		}
		
		try {
			$folder = Get-Item $Path -Force -ErrorAction Stop
		} catch {
			return Write-Error $_
		}

		try {
			$isEmpty = ($folder.GetFiles().Count -eq 0) -and ($folder.GetDirectories().Count -eq 0)
		} catch {
			return Write-Error $_.Exception.InnerException.Message
		}
			
		New-Object PSObject -Property @{
			'Path' = $folder.FullName
			'IsEmpty' = $isEmpty
		} | Select $properties

	} # process
} # function Test-EmptyFolder