﻿using DataManager;

namespace FSAHelper
{
    public class MPLS_ACCS : MSSqlBase
    {
        private int _SIP_TRNK_GRP_ID = 0;

        public int SIP_TRNK_GRP_ID
        {
            get { return _SIP_TRNK_GRP_ID; }
            set { _SIP_TRNK_GRP_ID = value; }
        }

        private string _CKT_ID = "";

        public string CKT_ID
        {
            get { return _CKT_ID; }
            set { _CKT_ID = value; }
        }

        private string _NW_USER_ADR = "";

        public string NW_USER_ADR
        {
            get { return _NW_USER_ADR; }
            set { _NW_USER_ADR = value; }
        }

        public int InsertMPLSAccess()
        {
            setCommand("dbo.insertMPLSAccess");
            addIntParam("@SIP_TRNK_GRP_ID", _SIP_TRNK_GRP_ID);
            addStringParam("@CKT_ID", _CKT_ID);
            addStringParam("@NW_USER_ADR", _NW_USER_ADR);
            return execNoQuery();
        }
    }
}