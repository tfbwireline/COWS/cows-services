@echo off
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to bounce the "peoplesoftbatch_svc" application component. An ERRORLEVEL
REM of 0 will be returned if the component is bounced successfully, and a non-zero 
REM ERRORLEVEL will be returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM retrieve and validate the startup mode for the service
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd PSOFT_BATCH_SVC_STARTUP_TYPE START_MODE
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_peoplesoftbatch_svc.cmd 5 "ERROR: unable to retrieve PSOFT_BATCH_SVC_STARTUP_TYPE from configuration"
  EXIT /b 5
)
IF "%START_MODE%" NEQ "Automatic" (
  IF "%START_MODE%" NEQ "Manual" (
    echo ERROR: cannot bounce service unless start mode is Automatic or Manual
    CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_peoplesoftbatch_svc.cmd 6 "ERROR: cannot bounce service unless start mode is Automatic or Manual"
    EXIT /b 6
  )
)
CALL "%APP_HOME%\Scripts\_getconfig.cmd" SVC_ENV_PREFIX
SET SERVICE_NAME=%SVC_ENV_PREFIX%COWS.PeopleSoftBatchService


REM
REM perform the host check
REM 
CALL "%APP_HOME%"\Scripts\perform-host-check.cmd
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_@SERVICE_ID@.cmd 10 "ERROR: host validation failed"
  EXIT /b 10
)


REM
REM make sure service is running
REM
CALL "%APP_HOME%"\Scripts\status_emailsender_svc.cmd
IF ERRORLEVEL 1 (
  ECHO ERROR: service %SERVICE_NAME% is not running, unable to bounce
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_emailsender_svc.cmd 7 "ERROR: service %SERVICE_NAME% is not running, unable to bounce"
  EXIT /b 7
)


REM
REM invoke service stop script, then service start script
REM
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_stopService.js %SERVICE_NAME%
IF ERRORLEVEL 1 (
  ECHO ERROR: problem stopping service %SERVICE_NAME%, unable to bounce
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_emailsender_svc.cmd 12 "ERROR: problem stopping service %SERVICE_NAME%, unable to bounce"
  EXIT /b 12
)
@ping 1.1.1.1 -n 15 -w 10000 > nul
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_startService.js %SERVICE_NAME%
IF ERRORLEVEL 1 (
  ECHO ERROR: problem restarting service %SERVICE_NAME%, unable to bounce
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd bounce_emailsender_svc.cmd 13 "ERROR: problem restarting service %SERVICE_NAME%, unable to bounce"
  EXIT /b 13
)
