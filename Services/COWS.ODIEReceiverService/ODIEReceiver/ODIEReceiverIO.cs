﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CL = LogManager.FileLogger;

namespace COWS.ODIEReceiverService
{
    public class ODIEReceiverIO
    {
        public string sMessage = string.Empty;
        private string sError = string.Empty;
        private int iCnt = 0;
        private int RetVal = -1;
        private ODIEReceiverDB _ReceiverDB = new ODIEReceiverDB();

        public bool ProcessODIEMessage()
        {
            //CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Raw XML Message received: " + sMessage, string.Empty);

            XmlDocument _xmlDoc = new XmlDocument();
            XmlElement _xmlRoot;
            string _RootElement = string.Empty;
            bool _bRetVal = false;
            bool _bContinue = true;

            try
            {
                try
                {
                    _xmlDoc.LoadXml(sMessage);
                }
                catch
                {
                    sMessage = sMessage.Substring(2, sMessage.Length - 2);
                    _xmlDoc.LoadXml(sMessage);
                }

                _xmlRoot = _xmlDoc.DocumentElement;
                _RootElement = _xmlRoot.Name.ToString();

                //CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "XML after basic cleaning: " + sMessage, string.Empty);
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error reading the XML Message with exception: " + e.ToString(), string.Empty);
                _bContinue = false;
            }

            sMessage = sMessage.Replace("tp:", string.Empty);
            if (_bContinue)
            {
                switch (_RootElement)
                {
                    case "COWSODIEOrderResponse":
                        {
                            sError = ReadOrderResponse();
                            break;
                        }
                    case "ODIECustomerInfoResponse":
                        {
                            sError = ReadCustomerInfoResponse();
                            break;
                        }
                    case "ODIECustomerNameResponse":
                        {
                            sError = ReadCustomerNameResponse();
                            break;
                        }
                    case "ODIEDiscoResponse":
                        {
                            sError = ReadDiscoResponse();
                            break;
                        }
                    case "ODIEShortNameValidationResponse":
                        {
                            sError = ReadShortNameValidationResponse();
                            break;
                        }
                    default:
                        {
                            CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "XML Message with Invalid Root Node!", string.Empty);
                            break;
                        }
                }

                int _ReturnStatus = 12;
                if (sError != string.Empty)
                {
                    CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "ERROR: " + sError, string.Empty);
                }
                else
                {
                    _ReturnStatus = 21;
                    CL.WriteLogFile("Response", CL.Event.EndDBWrite, CL.MsgType.text, -1, string.Empty, "XML Data is successfully processed and databased", string.Empty);
                    _bRetVal = true;
                }
                _ReceiverDB.UpdateODIERequest(_ReturnStatus);
            }

            return _bRetVal;
        }

        #region "Private Methods"

        private string ReadOrderResponse()
        {
            COWSODIEOrderResponse _Response = new COWSODIEOrderResponse();
            try
            {
                _Response = (COWSODIEOrderResponse)DeserializeXML(_Response);
            }
            catch
            {
                sError = "Invalid XML. Unable to deserialize this XML data.";
                return sError;
            }

            try
            {
                _ReceiverDB.REQ_ID = Convert.ToInt32(FixNull(_Response.TranID.ToString()));
            }
            catch
            {
                _ReceiverDB.REQ_ID = 0;
            }
            if (_ReceiverDB.REQ_ID == 0)
            {
                sError = "Invalid TranID. Cannot database the XML Data.";
                return sError;
            }

            if (FixNull(_Response.Acknowledgement.ToString().ToUpper()) == "X")
            {
                _ReceiverDB.ACK_CD = true;
            }
            else
            {
                _ReceiverDB.ACK_CD = false;
            }
            _ReceiverDB.RSPN_ERROR_TXT = FixNull(_Response.errorMessage).ToString();
            _ReceiverDB.RSPN_DT = DateTime.Now;

            RetVal = _ReceiverDB.InsertODIEResponse();
            if (RetVal != -1)
            {
                return "Error inserting the XML Message data into the Database. ";
            }
            return sError;
        }

        private string ReadCustomerNameResponse()
        {
            ODIECustomerNameResponse _Name = new ODIECustomerNameResponse();
            try
            {
                _Name = (ODIECustomerNameResponse)DeserializeXML(_Name);
            }
            catch
            {
                sError = "Invalid XML. Unable to deserialize this XML data.";
                return sError;
            }

            try
            {
                _ReceiverDB.REQ_ID = Convert.ToInt32(FixNull(_Name.TranID.ToString()));
                _ReceiverDB.RSPN_DT = DateTime.Now;
                _ReceiverDB.RSPN_ERROR_TXT = "";
            }
            catch
            {
                _ReceiverDB.REQ_ID = 0;
            }
            if (_ReceiverDB.REQ_ID == 0)
            {
                sError = "Invalid TranID. Cannot database the XML Data.";
                return sError;
            }

            try
            {
                iCnt = _Name.CustomerName.Length;
            }
            catch { }

            if (iCnt == 0)
            {
                _ReceiverDB.CUST_TEAM_PDL = "";
                _ReceiverDB.MNSPM_ID = "";
                _ReceiverDB.NTEAssigned = "";
                _ReceiverDB.ODIE_CUST_ID = "";
                _ReceiverDB.CUST_NME = "";
                _ReceiverDB.SOWS_FOLDR_PATH_NME = "";
                _ReceiverDB.SDEAssigned = "";

                RetVal = _ReceiverDB.InsertODIEResponse();
            }
            else
            {
                for (int j = 0; j < iCnt; j++)
                {
                    _ReceiverDB.CUST_TEAM_PDL = FixNull(_Name.CustomerName[j].CustomerTeamPDL).ToString();
                    _ReceiverDB.MNSPM_ID = FixNull(_Name.CustomerName[j].MNSPMId).ToString();
                    _ReceiverDB.NTEAssigned = FixNull(_Name.CustomerName[j].NTEAssigned).ToString();
                    _ReceiverDB.ODIE_CUST_ID = FixNull(_Name.CustomerName[j].CustID).ToString();
                    _ReceiverDB.CUST_NME = FixNull(_Name.CustomerName[j].CustomerName).ToString();
                    _ReceiverDB.SOWS_FOLDR_PATH_NME = FixNull(_Name.CustomerName[j].DocumentationURL).ToString();
                    _ReceiverDB.SDEAssigned = FixNull(_Name.CustomerName[j].SDEAssigned).ToString();
                    RetVal = _ReceiverDB.InsertODIEResponse();
                }
            }

            if (RetVal != -1)
            {
                sError = "Error inserting the XML Message data into the Database. ";
            }
            return sError;
        }

        private string ReadCustomerInfoResponse()
        {
            ODIECustomerInfoResponse _Info = new ODIECustomerInfoResponse();
            try
            {
                _Info = (ODIECustomerInfoResponse)DeserializeXML(_Info);
            }
            catch
            {
                sError = "Invalid XML. Unable to deserialize this XML data.";
                return sError;
            }

            try
            {
                iCnt = _Info.CustomerNameList.Length;
            }
            catch { }

            try
            {
                _ReceiverDB.REQ_ID = Convert.ToInt32(FixNull(_Info.TranID.ToString()));
            }
            catch
            {
                _ReceiverDB.REQ_ID = 0;
            }
            if (_ReceiverDB.REQ_ID == 0)
            {
                sError = "Invalid TranID. Cannot database the XML Data.";
                return sError;
            }

            if (iCnt == 0)
            {
                sError = "No Customer Info details exist in the XML. Invalid XML Data, but updating the Response TimeStamp ONLY.";
                _ReceiverDB.RSPN_INFO_DT = DateTime.Now;
                _ReceiverDB.FAST_TRK_CD = false;
                _ReceiverDB.DSPCH_RDY_CD = false;

                RetVal = _ReceiverDB.InsertODIEResponseInfo();
                return sError;
            }

            for (int i = 0; i < iCnt; i++)
            {
                _ReceiverDB.MODEL_ID = FixNull(_Info.CustomerNameList[i].ModelId).ToString();
                _ReceiverDB.DEV_ID = FixNull(_Info.CustomerNameList[i].DeviceId).ToString();
                _ReceiverDB.SERIAL_NO = FixNull(_Info.CustomerNameList[i].SerialNo).ToString();
                _ReceiverDB.RDSN_NBR = FixNull(_Info.CustomerNameList[i].RedesignNbr).ToString();
                _ReceiverDB.FAST_TRK_CD = ConvertStringToBool(FixNull(_Info.CustomerNameList[i].FastTrackFlag.ToString()));
                _ReceiverDB.DSPCH_RDY_CD = ConvertStringToBool(FixNull(_Info.CustomerNameList[i].DispatchReadyFlag.ToString()));
                _ReceiverDB.RSPN_INFO_DT = DateTime.Now;
                _ReceiverDB.MANF_ID = FixNull(_Info.CustomerNameList[i].MfrID).ToString();
                _ReceiverDB.MANF_ID = FixNull(_Info.CustomerNameList[i].MfrID).ToString();
                _ReceiverDB.H6_CUST_ID = FixNull(_Info.CustomerNameList[i].H6).ToString();

                RetVal = _ReceiverDB.InsertODIEResponseInfo();
                if (RetVal != -1)
                {
                    return "Error inserting the XML Message data into the Database. ";
                }
            }

            return sError;
        }

        private string ReadDiscoResponse()
        {
            string _Error = "";

            ODIEDiscoResponse _Disco = new ODIEDiscoResponse();
            try
            {
                _Disco = (ODIEDiscoResponse)DeserializeXML(_Disco);
            }
            catch (Exception ex)
            {
                _Error = "Invalid XML. Unable to deserialize this XML data. Error: " + ex.Message.ToString();
                return _Error;
            }

            try
            {
                iCnt = _Disco.DevicesList.Length;
            }
            catch { }

            try
            {
                _ReceiverDB.REQ_ID = Convert.ToInt32(FixNull(_Disco.TranID.ToString()));
            }
            catch
            {
                _ReceiverDB.REQ_ID = 0;
            }

            if (_ReceiverDB.REQ_ID == 0)
            {
                _Error = "Invalid TranID. Cannot database the XML Data.";
                return _Error;
            }

            if (iCnt == 0)
            {
                _Error = "No Device List details exist in the XML. Invalid XML Data, but updating the Response TimeStamp ONLY.";
                return _Error;
            }

            if (_Disco.DevicesList != null)
            {
                for (int i = 0; i < iCnt; i++)
                {
                    try
                    {
                        _ReceiverDB.ODIE_DISC_REQ_ID = Convert.ToInt32(FixNull(_Disco.DevicesList[i].Dev_Req_ID.ToString()));
                    }
                    catch
                    { }

                    try
                    {
                        _ReceiverDB.H5_H6_CUST_ID = Convert.ToInt32(FixNull(_Disco.DevicesList[i].H5_H6.ToString()));
                    }
                    catch
                    { }

                    _ReceiverDB.MODEL_ID = FixNull(_Disco.DevicesList[i].Model).ToString();
                    _ReceiverDB.DEV_ID = FixNull(_Disco.DevicesList[i].Vendor).ToString();
                    _ReceiverDB.SERIAL_NO = FixNull(_Disco.DevicesList[i].Serial_No).ToString();
                    _ReceiverDB.M5_DEVICE_ID = FixNull(_Disco.DevicesList[i].Mach5DeviceID).ToString();
                    _ReceiverDB.SITE_ID = FixNull(_Disco.DevicesList[i].SiteID).ToString();
                    _ReceiverDB.CTRCT_NBR = FixNull(_Disco.DevicesList[i].ContractNumber).ToString();

                    RetVal = _ReceiverDB.UpdateODIEDiscoRequest();
                    if (RetVal != -1)
                    {
                        _Error = "Error inserting the XML Message data into the Database. ";
                    }
                }
            }

            return _Error;
        }

        private string ReadShortNameValidationResponse()
        {
            ODIEShortNameValidationResponse _Response = new ODIEShortNameValidationResponse();

            try
            {
                _Response = (ODIEShortNameValidationResponse)DeserializeXML(_Response);
            }
            catch (Exception e)
            {
                sError = "Invalid XML. Unable to deserialize this XML data.";
                return sError;
            }

            try
            {
                _ReceiverDB.REQ_ID = Convert.ToInt32(FixNull(_Response.TranID.ToString()));
            }
            catch (Exception e)
            {
                _ReceiverDB.REQ_ID = 0;
            }

            if (_ReceiverDB.REQ_ID == 0)
            {
                sError = "Invalid TranID. Can't save this XML Data into database.";
                return sError;
            }

            if (FixNull(_Response.IsValid.ToString().ToUpper()) == "Y")
            {
                _ReceiverDB.ACK_CD = true;
            }
            else
            {
                _ReceiverDB.ACK_CD = false;
            }
            _ReceiverDB.RSPN_DT = DateTime.Now;

            RetVal = _ReceiverDB.InsertODIEResponse();
            if (RetVal != -1)
            {
                return "Error inserting the XML Message data into the Database. ";
            }

            return sError;
        }

        #endregion "Private Methods"

        #region "Convertors"

        private Object DeserializeXML(Object _Object)
        {
            XmlSerializer _XS = new XmlSerializer(_Object.GetType());
            MemoryStream _MS = new MemoryStream(StringToUTF8ByteArray(sMessage));
            XmlTextWriter _XW = new XmlTextWriter(_MS, Encoding.UTF8);
            return _XS.Deserialize(_MS);
        }

        private Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        private bool ConvertStringToBool(string sInput)
        {
            sInput = sInput.ToUpper();
            return ((sInput == "Y") || (sInput == "YES") || (sInput == "ON")) ? true : false;
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        #endregion "Convertors"
    }
}