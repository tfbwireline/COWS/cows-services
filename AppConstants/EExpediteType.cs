﻿namespace AppConstants
{
    public enum EExpediteType
    {
        Internal = 1,
        External = 2,
        InternalExternal = 3,
        None = 4
    }
}