@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM *** WINDOWS SERVICE SCRIPT ***
REM 
REM AVT framework job to check status of the "@SERVICE_TAG@" application component. 
REM
REM *******************************************************************************


REM
REM initialize application home and call status script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\Scripts\status_@SERVICE_TAG@.cmd"
