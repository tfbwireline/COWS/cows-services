/*******************************************************************************
* Copyright (c) 2007 by Sprint Nextel
*
* All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
* and is the sole and exclusive property of the copyright owner. 
*
* Author:
*   Woody Anderson - initial implementation
*
* synopsis: script to install a service
*
* arguments: required 3 args - the service name, start mode, and path to executable
*
*******************************************************************************/ 

  // make sure arguments are passed and valid
var error = null, state = null, msg = null, service_name = null, exitCode = 0;
var username = null, password = null;
if (WScript.Arguments.Count() < 3)
{
  error = "service name, start mode, and path to executable are required";
  exitCode = 100;
}
else if (WScript.Arguments.Count() > 3)
{
  var arg = WScript.Arguments(3);
  if (arg.substring(0,6).toUpperCase().match(/[-/]USER=/))
  {
    arg = arg.substring(6);
    var index = arg.indexOf(';');
    if (index < 1 || index >= arg.length-1) 
    {
      error = "problem parsing starting username/password (got '"+arg+"')";
      exitCode = 105;
    }
    else
    {
      username = arg.substring(0,index);
      password = arg.substring(index+1);
    }
  }
}
if (error == null)
{
  service_name = WScript.Arguments(0);
  var path = WScript.Arguments(2);
  var fso = WScript.CreateObject("Scripting.FileSystemObject");
  var fullpath = fso.GetAbsolutePathName(path);
  if (path.substring(1,2) == ":") 
    path = path.substring(0,1).toUpperCase()+path.substring(1);
  if (path != fullpath) 
  {
    error = "fully qualified path to service executable must be specified";
    exitCode = 101;
  }
  else if (!fso.FileExists(fullpath))
  {
    error = "service executable not found at specified path";
    exitCode = 102;
  }
}

  // see if service already exists
if (error == null)
{
  var objWMIService = GetObject("winmgmts:");
  var query = "SELECT * FROM win32_service WHERE name = '"+service_name+"'";
  var services = objWMIService.ExecQuery(query);
  var enumerator = new Enumerator(services);
  if (!enumerator.atEnd())
  {
    error = "service already exists";
    exitCode = 103;
  }
}

  // validate start mode
if (error == null)
{
  var startMode = WScript.Arguments(1);
  if (startMode != "Manual" && startMode != "Automatic" && startMode != "Disabled")
  {
    error = "start mode ("+startMode+") must be one of 'Manual', 'Automatic', or 'Disabled'";
    exitCode = 104;
  }
}

  // create the service entry
if (error == null)
{
  var OWN_PROCESS = 16;
  var NOT_INTERACTIVE = false;
  var NORMAL_ERR_CONTROL = 2;
  var objService = objWMIService.Get("Win32_BaseService");
  exitCode = objService.Create(service_name,service_name,fullpath,OWN_PROCESS,NORMAL_ERR_CONTROL,
    startMode,NOT_INTERACTIVE,username,password);
  if (exitCode != 0) error = "unable to create service, error code = "+exitCode;
}

  // print message about results, log results, then exit
if (error != null) msg = "ERROR: "+error;
else msg = "INFO: service '"+service_name+"' successfully installed"; 
WScript.Echo(msg);
logMsg(msg,exitCode);
WScript.Quit(exitCode);


function logMsg ( msg, code )
{
    // get name of log script
  var objFSO = WScript.CreateObject("Scripting.FileSystemObject");
  var objFile = objFSO.GetFile(WScript.ScriptFullName);
  var logScript = objFSO.GetParentFolderName(objFile)+"\\_logJobMsg.js";

    // call log script
  var cmd = "cscript.exe "+logScript+" "+WScript.ScriptName+" "+code+" \""+msg+"\"";
  var shell = WScript.CreateObject("WScript.Shell");
  shell.Run(cmd,7,true);
}
