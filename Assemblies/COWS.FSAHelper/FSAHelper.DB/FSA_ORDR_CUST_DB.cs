﻿using DataManager;

namespace FSAHelper
{
    public class FSA_ORDR_CUST_DB : MSSqlBase
    {
        #region "Fields and Properties"

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private int _CUST_ID;

        public int CUST_ID
        {
            get { return _CUST_ID; }
            set { _CUST_ID = value; }
        }

        private string _CIS_LVL_TYPE;

        public string CIS_LVL_TYPE
        {
            get { return _CIS_LVL_TYPE; }
            set { _CIS_LVL_TYPE = value; }
        }

        private string _CUST_NME;

        public string CUST_NME
        {
            get { return _CUST_NME; }
            set { _CUST_NME = value; }
        }

        private string _BR_CD;

        public string BR_CD
        {
            get { return _BR_CD; }
            set { _BR_CD = value; }
        }

        private string _CURR_BILL_CYC_CD = string.Empty;

        public string CURR_BILL_CYC_CD
        {
            get { return _CURR_BILL_CYC_CD; }
            set { _CURR_BILL_CYC_CD = value; }
        }

        private string _FUT_BILL_CYC_CD;

        public string FUT_BILL_CYC_CD
        {
            get { return _FUT_BILL_CYC_CD; }
            set { _FUT_BILL_CYC_CD = value; }
        }

        private string _SRVC_SUB_TYPE_ID;

        public string SRVC_SUB_TYPE_ID
        {
            get { return _SRVC_SUB_TYPE_ID; }
            set { _SRVC_SUB_TYPE_ID = value; }
        }

        private string _SOI_CD;

        public string SOI_CD
        {
            get { return _SOI_CD; }
            set { _SOI_CD = value; }
        }

        private string _SALS_PERSN_PRIM_CID;

        public string SALS_PERSN_PRIM_CID
        {
            get { return _SALS_PERSN_PRIM_CID; }
            set { _SALS_PERSN_PRIM_CID = value; }
        }

        private string _SALS_PERSN_SCNDY_CID;

        public string SALS_PERSN_SCNDY_CID
        {
            get { return _SALS_PERSN_SCNDY_CID; }
            set { _SALS_PERSN_SCNDY_CID = value; }
        }

        private string _CLLI_CD;

        public string CLLI_CD
        {
            get { return _CLLI_CD; }
            set { _CLLI_CD = value; }
        }

        #endregion "Fields and Properties"

        public FSA_ORDR_CUST_DB(int _OrderID)
        {
            ORDR_ID = _OrderID;
        }

        public int InsertFSAOrderCustInfo(ORDR_CNTCT_DB _CustInfo, ORDR_ADR_DB _AdrInfo)
        {
            setCommand("dbo.insertFSAOrderCustInfo");
            addIntParam("@ORDR_ID", _ORDR_ID);
            addStringParam("@CIS_LVL_TYPE", _CIS_LVL_TYPE);
            addIntParam("@CUST_ID", _CUST_ID);
            addStringParam("@CUST_NME", _CUST_NME);
            addStringParam("@BR_CD", _BR_CD);
            addStringParam("@CURR_BILL_CYC_CD", _CURR_BILL_CYC_CD);
            addStringParam("@FUT_BILL_CYC_CD", _FUT_BILL_CYC_CD);
            addStringParam("@SRVC_SUB_TYPE_ID", _SRVC_SUB_TYPE_ID);
            addStringParam("@SOI_CD", _SOI_CD);
            addStringParam("@SALS_PERSN_PRIM_CID", _SALS_PERSN_PRIM_CID);
            addStringParam("@SALS_PERSN_SCNDY_CID", _SALS_PERSN_SCNDY_CID);
            addStringParam("@CLLI_CD", _CLLI_CD);

            addIntParam("@ORDR_CNTCT_ID", _CustInfo.ORDR_CNTCT_ID);
            addTinyIntParam("@CNTCT_TYPE_ID", _CustInfo.CNTCT_TYPE_ID);
            addStringParam("@FRST_NME", _CustInfo.FRST_NME);
            addStringParam("@LST_NME", _CustInfo.LST_NME);
            addStringParam("@NME", _CustInfo.NME);
            addStringParam("@PHN_NBR", _CustInfo.PHN_NBR);
            addStringParam("@EMAIL_ADR", _CustInfo.EMAIL_ADR);
            addStringParam("@TME_ZONE_ID", _CustInfo.TME_ZONE_ID);
            addBooleanParam("@INTPRTR_CD", _CustInfo.INTPRTR_CD);
            addIntParam("@CREAT_BY_USER_ID", 1);
            addStringParam("@FAX_NBR", _CustInfo.FAX_NBR);
            addTinyIntParam("@ROLE_ID", _CustInfo.ROLE_ID);
            addStringParam("@NPA", _CustInfo.NPA);
            addStringParam("@NXX", _CustInfo.NXX);
            addStringParam("@STN_NBR", _CustInfo.STN_NBR);
            addStringParam("@CTY_CD", _CustInfo.CTY_CD);
            addStringParam("@ISD_CD", _CustInfo.ISD_CD);
            addStringParam("@PHN_EXT_NBR", _CustInfo.PHN_EXT_NBR);

            addIntParam("@ORDR_ADR_ID", _AdrInfo.ORDR_ADR_ID);
            addTinyIntParam("@ADR_TYPE_ID", _AdrInfo.ADR_TYPE_ID);
            addStringParam("@STREET_ADR_1", _AdrInfo.STREET_ADR_1);
            addStringParam("@STREET_ADR_2", _AdrInfo.STREET_ADR_2);
            addStringParam("@CTY_NME", _AdrInfo.CTY_NME);
            addStringParam("@PRVN_NME", _AdrInfo.PRVN_NME);
            addStringParam("@STT_CD", _AdrInfo.STT_CD);
            addStringParam("@ZIP_PSTL_CD", _AdrInfo.ZIP_PSTL_CD);
            addStringParam("@CTRY_CD", _AdrInfo.CTRY_CD);
            addStringParam("@FSA_MDUL_ID", _AdrInfo.FSA_MDUL_ID);
            addBooleanParam("@HIER_LVL_CD", _AdrInfo.HIER_LVL_CD);

            return execNoQuery();
        }
    }
}