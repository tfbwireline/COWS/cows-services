﻿using System;

namespace COWSRecurringApptParser
{
    public class ApptRcurncData
    {
        private int _ApptID;

        public int ApptID
        {
            get { return _ApptID; }
            set { _ApptID = value; }
        }

        private int _ApptTypeID;

        public int ApptTypeID
        {
            get { return _ApptTypeID; }
            set { _ApptTypeID = value; }
        }

        private DateTime _StrtTmst;

        public DateTime StrtTmst
        {
            get { return _StrtTmst; }
            set { _StrtTmst = value; }
        }

        private DateTime _EndTmst;

        public DateTime EndTmst
        {
            get { return _EndTmst; }
            set { _EndTmst = value; }
        }

        private int _AsnUserID;

        public int AsnUserID
        {
            get { return _AsnUserID; }
            set { _AsnUserID = value; }
        }

        private DateTime _CreatDt;

        public DateTime CreatDt
        {
            get { return _CreatDt; }
            set { _CreatDt = value; }
        }
    }
}