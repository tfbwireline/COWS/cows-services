﻿using IOManager;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWS.Mach5SenderService
{
    public partial class Mach5SenderService : ServiceBase
    {
        private string _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["RequestSleepTime"]);
        private Thread _SenderThread = null;
        private Thread _RedesignThread = null;
        private Thread _BDRThread = null;

        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private ASCIIEncoding encoding = new ASCIIEncoding();

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new Mach5SenderService() };
            ServiceBase.Run(ServicesToRun);
        }

        public Mach5SenderService()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.Mach5SenderService is starting.", "");

                _bActive = true;
                _bStopThread = false;
                _bCanBeStoppedNow = true;
                _SenderThread = new Thread(new ThreadStart(this.SendRequests));
                _RedesignThread = new Thread(new ThreadStart(this.SendRedesigns));
                _BDRThread = new Thread(new ThreadStart(this.SendBDRs));

                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send Mach5 Requests)", string.Empty);
                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send Mach5 Redesigns)", string.Empty);
                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send Mach5 Billable Dispatch Requests)", string.Empty);

                _SenderThread.Start();
                _RedesignThread.Start();
                _BDRThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.Mach5SenderService. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.Mach5SenderService", "COWS.Mach5SenderService Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                _bStopThread = true;
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.Mach5SenderService", string.Empty);

                _SenderThread.Join(TimeSpan.FromSeconds(30));
                _RedesignThread.Join(TimeSpan.FromSeconds(30));
                _BDRThread.Join(TimeSpan.FromSeconds(30));

                if (!_bCanBeStoppedNow)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send Mach5  Requests)", string.Empty);
                _SenderThread.Abort();
                _SenderThread.Join();

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send Mach5  Redesigns)", string.Empty);
                _RedesignThread.Abort();
                _RedesignThread.Join();

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send Mach5 Billable Dispatch Requests)", string.Empty);
                _BDRThread.Abort();
                _BDRThread.Join();

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.Mach5SenderService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.Mach5SenderService. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void SendRequests()
        {
            Mach55SenderIO _oIO = new Mach55SenderIO();

            while (!_bStopThread)
            {
                if (_bActive)
                {
                    _oIO.GetMach5Responses();
                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                    return;
                }
            }
        }

        private void SendRedesigns()
        {
            Mach55SenderIO _oIO = new Mach55SenderIO();

            while (!_bStopThread)
            {
                if (_bActive)
                {
                    if (((DateTime.Now.Minute % 2) == 0)
                        && (DateTime.Now.Second < 5))
                        _oIO.GetMach5RedesignMsgs();
                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                    return;
                }
            }
        }

        private void SendBDRs()
        {
            Mach55SenderIO _oIO = new Mach55SenderIO();

            while (!_bStopThread)
            {
                if (_bActive)
                {
                    if (((DateTime.Now.GetNthBusinessDayOfMonth(8) == DateTime.Now.Day)
                        && (DateTime.Now.Minute == 1)
                        && (DateTime.Now.Second < 5)))
                        _oIO.GetMach5BillDsptchMsgs();
                    else
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Not time(8th business day) to pickup billable dispatch records", string.Empty);
                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                    return;
                }
            }
        }
    }
}