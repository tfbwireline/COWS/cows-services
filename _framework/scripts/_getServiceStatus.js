/*******************************************************************************
* Copyright (c) 2007 by Sprint Nextel
*
* All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
* and is the sole and exclusive property of the copyright owner. 
*
* Author:
*   Woody Anderson - initial implementation
*
* synopsis: script to get the state of a given service
*
* arguments: required one argument, the name of the service
*
* returns: the state of the service (ex: Running, Stopped, ...)
*
*******************************************************************************/ 

  // make sure argument is passed
var error = null, state = null;
if (WScript.Arguments.Count() != 1)
{
  error = "service name is required";
}

  // query for service and iterate over results
else
{
  var service_name = WScript.Arguments(0);
  var objWMIService = GetObject("winmgmts:");
  var query = "SELECT * FROM win32_service WHERE name = '"+service_name+"'";
  var services = objWMIService.ExecQuery(query);
  var enumerator = new Enumerator(services);
  if (enumerator.atEnd())
    error = "service '"+service_name+"' not found";
  else
    state = enumerator.item().state;
}

  // if there was an error, print error message and exit
if (error != null)
{
  WScript.Echo("ERROR: "+error);
  WScript.Quit(1);
}

  // print results
WScript.Echo("state of service '"+service_name+"': "+state);
// log the status to a file
var errObj = new Error();
var tryCount = 0;
do
{
	try 
	{	
		var fsObject = new ActiveXObject("Scripting.FileSystemObject");
		var wshShell = WScript.CreateObject("WScript.Shell");
		var objEnv = wshShell.Environment("PROCESS");
		var regEx = /"/g;
		var pathLogs = objEnv("COWS_LOG_PATH").replace(regEx, "");
		var strFilePath = pathLogs + "\\AllServicesStatuses.txt";
		var strTempFilePath = pathLogs + "\\tmp_" + Math.round(Math.random() * 100000000) + ".tmp"
		var tmpFile = fsObject.CreateTextFile(strTempFilePath, true);
		var dt = new Date();
		if (fsObject.FileExists(strFilePath))
		{
			var readFile = fsObject.OpenTextFile(strFilePath, 1);
			while (!readFile.AtEndOfStream)
			{
				var strReadLine = readFile.ReadLine();
				var strDate = strReadLine.substring(0, 16);
				var msecsLine = Date.parse(strDate);
				var msecsNow = Date.parse(dt);
				var msecsDiff = msecsNow - msecsLine;
				if (msecsDiff < 86400000)
				{
					tmpFile.WriteLine(strReadLine);
				}
			}
			readFile.Close();
		}
		var strDay = dt.getDate().toString();
		var strMonth = (dt.getMonth() + 1).toString();
		var strYear = dt.getFullYear().toString();
		var strHour = dt.getHours().toString();
		var strMin = dt.getMinutes().toString();
		if (strDay.length == 1)
			strDay = "0" + strDay;
		if (strMonth.length == 1)
			strMonth = "0" + strMonth;
		if (strHour.length == 1)
			strHour = "0" + strHour;
		if (strMin.length == 1)
			strMin = "0" + strMin;
		var strOutText = strMonth + "/";
		strOutText += strDay + "/";
		strOutText += strYear;
		strOutText += " " + strHour;
		strOutText += ":" + strMin;
		strOutText += " - state of service '"+service_name+"': "+state;
		tmpFile.WriteLine(strOutText);
		tmpFile.Close();
		if (fsObject.FileExists(strFilePath))
		{
			fsObject.DeleteFile(strFilePath, true);
		}
		fsObject.MoveFile(strTempFilePath, strFilePath);
		errObj.number = 0;
		WScript.Echo("The services status logs file has been updated successfully.");
	}
	catch (errObj)
	{
		if (errObj.message != "Permission denied")
		{
			WScript.Echo("Error: Unable to write to the services status logs file. Error details: " + errObj.number + " - " + errObj.message);
			break;
		}
		WScript.Echo("Warning: The services status logs file has been locked out by another process. Trying again for: " + (30 - tryCount) + " seconds");
		WScript.Sleep(1000);
		tryCount++;
		if (tryCount > 30)
		{
			WScript.Echo("Error: Error timed out, exiting now");
			break;
		}
	}
	finally
	{
		if (fsObject.FileExists(strTempFilePath))
		{
			fsObject.DeleteFile(strTempFilePath, true);
		}
	}
} while (errObj.number != 0)
// exit
var exitStatus = (state == "Running" ? 0 : 2);
WScript.Quit(exitStatus);
