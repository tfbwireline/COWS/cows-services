@echo off
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM *** WEB APPLICATION SCRIPT ***
REM 
REM AVT framework job to @ACTION@ the IIS application pool for the @WEBSSITENAME@ website.
REM
REM Return codes:
REM   (pls see return codes for _recycle_app_pool.cmd)
REM
REM *******************************************************************************


REM
REM @ACTION@ the IIS application pool for the website specified
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\Scripts\@SCRIPTNAME@"
