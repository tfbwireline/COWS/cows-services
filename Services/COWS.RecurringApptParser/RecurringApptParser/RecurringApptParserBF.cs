﻿using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using CL = LogManager.FileLogger;

namespace COWSRecurringApptParser
{
    internal class RecurringApptParserBF
    {
        private DateTime _FinalStDt;
        private DateTime _FinalEndDt;
        private int _StHr = 0;
        private int _StMin = 0;
        private int _StSec = 0;
        private int _EndHr = 0;
        private int _EndMin = 0;
        private int _EndSec = 0;
        private RecurringApptParserDB rapdb;
        private string StrtTmst = string.Empty;
        private string EndTmst = string.Empty;
        private int DeltaDays = 0;

        public RecurringApptParserBF()
        {
            rapdb = new RecurringApptParserDB();
        }

        internal void ProcessNoEndRecAppntmnts()
        {
            try
            {
                DataTable dtIn = rapdb.GetNoEndRecurringAppts();

                if ((dtIn != null) && (dtIn.Rows.Count > 0))
                {
                    rapdb.InsertNoEndRecAppts(dtIn);

                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Processed " + dtIn.Rows.Count.ToString() + " NoEnd records", string.Empty);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "There are no NoEnd records to process", string.Empty);
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error: " + ex.Message + " ; " + ex.StackTrace, string.Empty);
            }
        }

        internal void ParseRecApptmnts()
        {
            try
            {
                DataTable dtIn = rapdb.GetRecurringAppts();
                if ((dtIn != null) && (dtIn.Rows.Count > 0))
                {
                    #region RecurringDescription

                    /*Convert recurring shift appts to non-recurring appts and insert into datatable

			DevExpress rules - \

			Type -
			Not Present - Every Day
			1	- Weekly, on selected days of each week, every Periodicity Weeks
			2	- Monthly, see below
			3	- Yearly

			If daily,
			If Periodicity is selected, then every x days
			If WeekDays=62, then Every Weekday is selected.

			If Weekly, calculate days as below.  Periodicity = how often to start
				Here is how to find the Days selected
				•Sunday = 1,
				•Monday = 2,
				•Tuesday = 4,
				•Wednesday = 8,
				•Thursday = 16,
				•Friday = 32,
				•Saturday = 64,

				To find out which days the recurring meeting is on, take the Weekdays entry.
				If Weekdays > 64, then Saturday.  (subtract 64).
				If Weekdays > 32 (after removing Saturday if needed above), then Friday
				If Weekdays > 16 (...) then Thursday.  etc...

			If Monthly,
				Week of Month
				--blank = first week of month
				--0 = Take DayNumber, ignore week
				--2 = Second occurrence
				--3 = Third
				--4 = Fourth
				--5 = Last
					Weekdays (calculate as above, 62 = first/last weekday, 65(66?) = first/last weekend)
				of Periodicity months

			If Yearly
				WeekOfMonth
					None - On Month month and Day DayNumber
					First, second, etc day of Month month, weekday as above in month.

			Range
			- Not Present = No End Date
			- 1 = End After x Occurrences
			- 2 = End By Date

			If not present, goes forever
			If 1, End after OccurenceCount
			If 2, End by EndDate (if end by is set for Saturday, that Saturday is an occurence.

			*/

                    #endregion RecurringDescription

                    //Delete rows with invalid data
                    dtIn.Delete("StartDt = '' AND EndDt = ''");

                    int Type = 0;
                    int WeekDays = 0;
                    int Periodicity = 0;
                    int WeekOfMonth = -1;
                    int DayofMonth = 0;
                    int Month = 0;
                    int Range = 0;
                    int OccurenceCT = 0;
                    DateTime StartOn = new DateTime();
                    DateTime EndByDate = new DateTime();
                    DateTime MonthsOut = DateTime.Now.AddMonths(12);
                    int DayCtr = 0;
                    DateTime Today = DateTime.Now;
                    int OccurCtr = 1;
                    List<int> SelectedDays = new List<int>();
                    DateTime TmpStartOn;
                    DateTime TmpEndByDate;
                    int ApptID = -1;
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    foreach (DataRow dr in dtIn.Rows)
                    {
                        sw.Restart();
                        if ((Int32.Parse(dr["APPT_ID"].ToString()) != ApptID) && (ApptID != -1))
                        {
                            CommitApptData(ApptID);
                            ApptID = Int32.Parse(dr["APPT_ID"].ToString());
                            rapdb.UpdateTrigStatus(ApptID, ((byte)EApptRecStus.Processing));
                        }
                        else if (ApptID == -1)
                        {
                            ApptID = Int32.Parse(dr["APPT_ID"].ToString());
                            rapdb.UpdateTrigStatus(ApptID, ((byte)EApptRecStus.Processing));
                        }

                        //if (rapdb.lARData.Count <= 0)
                        //{
                        DayCtr = 0;
                        OccurCtr = 1;
                        Today = DateTime.Now;

                        //if (dr["APPT_ID"].ToString().Equals("51297"))// || (dr["APPT_ID"].ToString().Equals("523")) || (dr["APPT_ID"].ToString().Equals("980")))  // test
                        //{
                        Type = Int32.Parse(dr["AType"].ToString());
                        WeekDays = Int32.Parse(dr["WeekDays"].ToString());
                        Periodicity = Int32.Parse(dr["Periodicity"].ToString());
                        WeekOfMonth = Int32.Parse(dr["WeekOfMonth"].ToString());
                        DayofMonth = Int32.Parse(dr["DayNumber"].ToString());
                        Month = Int32.Parse(dr["Month"].ToString());
                        Range = Int32.Parse(dr["Range"].ToString());
                        OccurenceCT = Int32.Parse(dr["OccurrenceCount"].ToString());
                        StartOn = DateTime.Parse(dr["StartDt"].ToString());
                        EndByDate = (dr["EndDt"].ToString().Length > 0) ? DateTime.Parse(dr["EndDt"].ToString()) : StartOn;
                        StrtTmst = DateTime.Parse(dr["strtTmst"].ToString()).ToString("HH:mm:ss");
                        EndTmst = DateTime.Parse(dr["EndTmst"].ToString()).ToString("HH:mm:ss");
                        DeltaDays = (DateTime.Parse(dr["EndTmst"].ToString()).Date - DateTime.Parse(dr["strtTmst"].ToString()).Date).Days;

                        string[] StrTmst = StrtTmst.Split(new Char[] { ':' });
                        if (StrTmst.Length > 0)
                        {
                            _StHr = Int32.Parse(StrTmst[0]);
                            _StMin = Int32.Parse(StrTmst[1]);
                            _StSec = 0;
                        }
                        StrTmst = EndTmst.Split(new Char[] { ':' });
                        if (StrTmst.Length > 0)
                        {
                            _EndHr = Int32.Parse(StrTmst[0]);
                            _EndMin = Int32.Parse(StrTmst[1]);
                            _EndSec = 0;
                        }

                        MonthsOut = (Type == 0) ? DateTime.Now.AddMonths(9) : ((Type == 3) ? DateTime.Now.AddMonths(48) : DateTime.Now.AddMonths(12));
                        MonthsOut = MonthsOut.Date.AddHours(_EndHr).AddMinutes(_EndMin).AddSeconds(_EndSec);
                        /*****************************************************
                        General updates used in multiple conditions below, group here
                        *****************************************************/

                        //For End Date, when no end in site go out to six months.  If there is an end date, take the smaller of Six months out or end date
                        EndByDate = ((Range == 0) || (Range == 1)) ? MonthsOut : ((EndByDate < MonthsOut) ? EndByDate : MonthsOut);

                        //If start date is after today, start on start day.  If start day <= today, start today
                        Today = StartOn;// (Range != 1) ? ((Today < StartOn) ? StartOn : Today) : Today;//Always use _StartRange to restrict number of recurring slots(performance improvement)// (Range != 1) ? ((DateTime.Now < StartOn) ? StartOn : DateTime.Now) : Today;

                        Today = Today.Date.AddHours(_StHr).AddMinutes(_StMin).AddSeconds(_StSec);
                        EndByDate = EndByDate.Date.AddHours(_EndHr).AddMinutes(_EndMin).AddSeconds(_EndSec);

                        Periodicity = (Periodicity == 0) ? 1 : Periodicity;

                        //devx bug, when range is 1 and occurence is 10, OccurrenceCount in xml is 0
                        OccurenceCT = ((OccurenceCT == 0) && (Range == 1)) ? 10 : OccurenceCT;

                        //StartOn = dcStart.AddDays(-1);//test;

                        /*   Pull out days from @WeekDays variable, perform calculations for each one.  I have set up weeks so that Sunday = dayofweek 7

                        •Sunday = 1,
                        •Monday = 2,
                        •Tuesday = 4,
                        •Wednesday = 8,
                        •Thursday = 16,
                        •Friday = 32,
                        •Saturday = 64,
                        */
                        if ((Type == 1) && (WeekDays == 0))
                            WeekDays = 127;
                        SelectedDays.Clear();
                        if (WeekDays >= 64)
                        {	//Saturday
                            SelectedDays.Add(6);
                            WeekDays = WeekDays - 64;
                        }
                        if (WeekDays >= 32)
                        {	//Friday
                            SelectedDays.Add(5);
                            WeekDays = WeekDays - 32;
                        }
                        if (WeekDays >= 16)
                        {	//Thursday
                            SelectedDays.Add(4);
                            WeekDays = WeekDays - 16;
                        }
                        if (WeekDays >= 8)
                        {	//Wednesday
                            SelectedDays.Add(3);
                            WeekDays = WeekDays - 8;
                        }
                        if (WeekDays >= 4)
                        {	//Tuesday
                            SelectedDays.Add(2);
                            WeekDays = WeekDays - 4;
                        }
                        if (WeekDays >= 2)
                        {	//Monday
                            SelectedDays.Add(1);
                            WeekDays = WeekDays - 2;
                        }
                        if (WeekDays >= 1)
                        {	//Sunday
                            SelectedDays.Add(0);
                            WeekDays = WeekDays - 1;
                        }
                        /*****************************************************
                        General updates used in multiple conditions below, End
                        *****************************************************/

                        /*****************************************************
                        Daily occurrences Start
                        *****************************************************/
                        if (Type == 0)
                        {//This is an every day occurrence
                            if (SelectedDays.Count > 0)
                            {//This means its a weekday
                                if (StartOn != null)
                                {//For each weekday from deStartOn to...
                                    if ((Range == 0) || (Range == 2))
                                    {//0 = No end date, 2 = end on @deEndDate
                                        //From start until the end, if its a week day add it to the calendar (remember this is a weekdays only loop)
                                        while (Today < EndByDate)
                                        {
                                            if (IsWeekDay(Today))
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }
                                            Today = Today.AddDays(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                    else if (Range == 1)//End after x occurences
                                    {
                                        Today = StartOn;
                                        while (OccurCtr <= OccurenceCT)
                                        {
                                            if (IsWeekDay(Today))
                                            {
                                                if (Today < MonthsOut)
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                }
                                                OccurCtr++;
                                            }
                                            Today = Today.AddDays(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                }//Else bad start date, skip this item
                            }
                            else //This is a periodicity check instead of days check
                            {
                                if (StartOn != null)
                                {
                                    if ((Range == 0) || (Range == 2))
                                    {//0 = No end date, 2 = end on @deEndDate
                                        while (Today <= EndByDate)
                                        {
                                            UpdateApptRecTable(Today, dr);
                                            Today = Today.AddDays(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                    else if (Range == 1)//End after x occurences
                                    {
                                        Today = StartOn;
                                        while (OccurCtr <= OccurenceCT)
                                        {
                                            UpdateApptRecTable(Today, dr);
                                            OccurCtr++;

                                            Today = Today.AddDays(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }//End Periodicity
                        }//Type=0 daily
                        /*****************************************************
                        Daily occurrences End, Weekly Start
                        *****************************************************/
                        else if (Type == 1)
                        { //This is a weekly occurence
                            if ((Range == 0) || (Range == 2))
                            {   /*0 = No end date, 2 = end on @deEndDate*/
                                Today = StartOn;

                                while (Today <= EndByDate)
                                {
                                    if (SelectedDays.Contains(((int)Today.DayOfWeek)))
                                    {
                                        TmpStartOn = StartOn.AddDays(-1);
                                        if ((Today > TmpStartOn) && (Today <= EndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }
                                    }
                                    Today = Today.AddDays(1);
                                    if (Today.DayOfWeek == DayOfWeek.Sunday)
                                        Today = Today.AddDays((Periodicity - 1) * 7); //Set the week forward x weeks to the next eligible set
                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                    {
                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                        break;
                                    }
                                }
                            }//end Range=0,2
                            else if (Range == 1)
                            {
                                Today = StartOn;

                                while (OccurCtr <= OccurenceCT)
                                {
                                    if (SelectedDays.Contains(((int)Today.DayOfWeek)))
                                    {
                                        if (Today >= StartOn)
                                        {
                                            if (Today <= MonthsOut)
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }

                                            OccurCtr++;
                                        }
                                    }
                                    Today = Today.AddDays(1);
                                    if (Today.DayOfWeek == DayOfWeek.Sunday)
                                        Today = Today.AddDays((Periodicity - 1) * 7); //Set the week forward x weeks to the next eligible set
                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                    {
                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                        break;
                                    }
                                }
                            }//Range=1
                        }//Type=1
                        /*****************************************************
                        Weekly occurrences End, Monthly Start
                        *****************************************************/
                        else if (Type == 2)
                        {
                            /*This is a monthly occurrence
                            Start by looking for week of month.
                            blank = first week of month
                            0 = Take DayNumber, ignore week
                            2 = Second occurrence
                            3 = Third
                            4 = Fourth
                            5 = Last*/
                            if (WeekOfMonth == 0)
                            {//Using Day Number and periodicity
                                //if (DayofMonth != 0)
                                //{
                                DayofMonth = (DayofMonth != 0) ? DayofMonth : 1;
                                if ((Range == 0) || (Range == 2))
                                {
                                    Today = DateTime.Now;
                                    Today = (StartOn.Day != DayofMonth) ? StartOn.AddDays(DayofMonth - StartOn.Day) : StartOn;

                                    while (Today <= EndByDate)
                                    {
                                        if (Today >= StartOn)
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }
                                        if (DateTime.DaysInMonth(Today.Year, Today.Month) < DayofMonth)
                                            Today = Today.AddMonths(Periodicity).AddDays(DayofMonth - Today.Day);
                                        else
                                            Today = Today.AddMonths(Periodicity);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }
                                else if (Range == 1)
                                {
                                    Today = StartOn;
                                    Today = (StartOn.Day != DayofMonth) ? StartOn.AddDays(DayofMonth - StartOn.Day) : StartOn;

                                    while ((Today <= MonthsOut) && (OccurCtr <= OccurenceCT))
                                    {
                                        if (Today >= StartOn)
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }

                                        OccurCtr++;
                                        if (DateTime.DaysInMonth(Today.Year, Today.Month) < DayofMonth)
                                            Today = Today.AddMonths(Periodicity).AddDays(DayofMonth - Today.Day);
                                        else
                                            Today = Today.AddMonths(Periodicity);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }
                                //}
                            }
                            else if ((WeekOfMonth == -1) || (WeekOfMonth == 1) || (WeekOfMonth == 2) || (WeekOfMonth == 3) || (WeekOfMonth == 4))
                            {//First day of month
                                //If "Day" is selected in monthly-week-day selection, think of "weekof" as being the DayNumber above
                                if (SelectedDays.Count == 0)
                                {
                                    WeekOfMonth = (WeekOfMonth == -1) ? 1 : WeekOfMonth;
                                    Today = StartOn.AddDays((StartOn.Day * -1) + 1);
                                    if ((Range == 0) || (Range == 2))
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddDays(WeekOfMonth - 1);

                                        while (Today <= EndByDate)
                                        {
                                            if (Today >= StartOn)
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }

                                            Today = Today.AddMonths(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                    else if (Range == 1)
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddDays(WeekOfMonth - 1);

                                        while ((Today <= MonthsOut) && (OccurCtr <= OccurenceCT))
                                        {
                                            if (Today >= StartOn)
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }

                                            OccurCtr++;
                                            Today = Today.AddMonths(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                }// no days entered
                                else
                                {
                                    WeekOfMonth = (WeekOfMonth == -1) ? 1 : WeekOfMonth;
                                    if ((Range == 0) || (Range == 2))
                                    {// end by date or no end date
                                        while ((Today.Year < EndByDate.Year) || ((Today.Month <= EndByDate.Month)) && (Today.Year == EndByDate.Year))
                                        {
                                            //Get to first day of this month, then move forward until you hit the first instance
                                            Today = Today.AddDays((Today.Day * -1) + 1);
                                            DayCtr = 0;
                                            if (WeekOfMonth != 1)
                                            {
                                                while (DayCtr < WeekOfMonth)
                                                {
                                                    if (SelectedDays.Contains((int)Today.DayOfWeek))
                                                    {
                                                        Today = Today.AddDays(1);
                                                        DayCtr = DayCtr + 1;
                                                    }
                                                    else
                                                        Today = Today.AddDays(1);
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                Today = Today.AddDays(-1);//subtract one here because you had to shift it one above this
                                                if ((Today >= StartOn) && (Today <= EndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                }
                                            }
                                            else
                                            {
                                                bool bHit = false;
                                                while ((DayCtr < (WeekOfMonth * 7)) && (!bHit))
                                                {
                                                    if (!(SelectedDays.Contains((int)Today.DayOfWeek)))
                                                        Today = Today.AddDays(1);
                                                    else
                                                        bHit = true;
                                                    DayCtr = DayCtr + 1;
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                if ((Today >= StartOn) && (Today <= EndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    bHit = false;
                                                }
                                            }
                                            Today = Today.AddMonths(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }//End of section for End by Date or No End
                                    else if (Range == 1)
                                    {//occurences
                                        //Get to first day of the start
                                        Today = StartOn;
                                        TmpEndByDate = EndByDate.AddMonths(1);
                                        while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                        {
                                            DayCtr = 0;

                                            //Get to first day of this month, then move forward until you hit the first instance
                                            Today = Today.AddDays((Today.Day * -1) + 1);
                                            if (WeekOfMonth != 1)
                                            {
                                                while (DayCtr < WeekOfMonth)
                                                {
                                                    if (SelectedDays.Contains((int)Today.DayOfWeek))
                                                    {
                                                        Today = Today.AddDays(1);
                                                        DayCtr = DayCtr + 1;
                                                    }
                                                    else
                                                        Today = Today.AddDays(1);
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                Today = Today.AddDays(-1);//subtract one here because you had to shift it one above this
                                                if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    OccurCtr++;
                                                }
                                            }
                                            else
                                            {
                                                bool bHit = false;
                                                while ((DayCtr < (WeekOfMonth * 7)) && (!bHit))
                                                {
                                                    if (!(SelectedDays.Contains((int)Today.DayOfWeek)))
                                                        Today = Today.AddDays(1);
                                                    else
                                                        bHit = true;
                                                    DayCtr = DayCtr + 1;
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    bHit = false;
                                                    OccurCtr++;
                                                }
                                            }
                                            Today = Today.AddMonths(Periodicity);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                }//No Weekday selected
                            }
                            else if (WeekOfMonth == 5)
                            {
                                if ((Range == 0) || (Range == 2))
                                {
                                    Today = StartOn.AddDays((StartOn.Day * -1) + 1);
                                    // end by date or no end date
                                    while ((Today.Year < EndByDate.Year) || ((Today.Month <= EndByDate.Month)) && (Today.Year == EndByDate.Year))
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddMonths(1).AddDays(-1);// go to first of next month and then -1 to get to the last of this month

                                        //traverse back to check if today is one of the desired day and then exit loop; if it is the last day criteria, dont enter loop
                                        while ((SelectedDays.Count > 0) && (!(SelectedDays.Contains((int)Today.DayOfWeek))))
                                        {
                                            Today = Today.AddDays(-1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }

                                        if ((Today >= StartOn) && (Today <= EndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }

                                        Today = Today.AddMonths(Periodicity);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }
                                else if (Range == 1)
                                {//occurences
                                    //Today = StartOn;
                                    TmpEndByDate = EndByDate.AddMonths(1);
                                    Today = StartOn.AddDays((StartOn.Day * -1) + 1);
                                    while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddMonths(1).AddDays(-1);// go to first of next month and then -1 to get to the last of this month

                                        //traverse back to check if today is one of the desired day and then exit loop; if it is the last day criteria, dont enter loop
                                        while ((SelectedDays.Count > 0) && (!(SelectedDays.Contains((int)Today.DayOfWeek))))
                                        {
                                            Today = Today.AddDays(-1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }

                                        if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                            OccurCtr++;
                                        }
                                        Today = Today.AddMonths(Periodicity);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }//range=1
                            }//weekmonth=5
                        }//monthly recurrences
                        /*****************************************************
                        Monthly occurrences End, Yearly Start
                        *****************************************************/
                        else if (Type == 3)
                        {//This is a yearly occurrence
                            if (Month == 0)
                                Month = 1;
                            if (DayofMonth == 0)
                                DayofMonth = 1;

                            if (WeekOfMonth == 0)
                            {
                                if ((Range == 0) || (Range == 2))
                                {
                                    while (Today < EndByDate)
                                    {
                                        //Move back to first of the month and add diff in no. of months needed to get to the selected month
                                        Today = Today.AddDays((Today.Day * -1) + 1).AddMonths(Month - Today.Month);
                                        //To the first day of the selected month, Add days needed(determine if daysinmonth < selectedday) and -1 to get the selected day
                                        Today = Today.AddDays(((DateTime.DaysInMonth(Today.Year, Today.Month) < DayofMonth) ? DateTime.DaysInMonth(Today.Year, Today.Month) : DayofMonth) - 1);
                                        if ((Today >= StartOn) && (Today <= EndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }

                                        Today = Today.AddYears(1);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }
                                else if (Range == 1)
                                {//# of occurrences
                                    Today = StartOn;
                                    TmpEndByDate = EndByDate.AddYears(1);
                                    while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                    {
                                        //Move back to first of the month and add diff in no. of months needed to get to the selected month
                                        Today = Today.AddDays((Today.Day * -1) + 1).AddMonths(Month - Today.Month);
                                        //To the first day of the selected month, Add days needed(determine if daysinmonth < selectedday) and -1 to get the selected day
                                        Today = Today.AddDays(((DateTime.DaysInMonth(Today.Year, Today.Month) < DayofMonth) ? DateTime.DaysInMonth(Today.Year, Today.Month) : DayofMonth) - 1);
                                        if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                            OccurCtr = OccurCtr + 1;
                                        }
                                        Today = Today.AddYears(1);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }//# of occurrences
                            }//End Day of Month
                            else if ((WeekOfMonth == -1) || (WeekOfMonth == 1) || (WeekOfMonth == 2) || (WeekOfMonth == 3) || (WeekOfMonth == 4))
                            {//On nth day of Month of year
                                //If "Day" is selected in yearly-month-week-day selection, think of "weekof" as being the DayNumber above
                                if (SelectedDays.Count == 0)
                                {
                                    Today = StartOn;

                                    //Move back to first of the month and add diff in no. of months needed to get to the selected month
                                    Today = Today.AddDays((Today.Day * -1) + 1).AddMonths(Month - Today.Month);
                                    DayCtr = 1;

                                    while (DayCtr < WeekOfMonth)
                                    {
                                        DayCtr++;
                                        Today = Today.AddDays(1);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }

                                    if ((Range == 0) || (Range == 2))
                                    {
                                        while (Today <= EndByDate)
                                        {
                                            if ((Today >= StartOn) && (Today <= EndByDate))
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }
                                            Today = Today.AddYears(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }
                                    else if (Range == 1)
                                    {
                                        TmpEndByDate = EndByDate.AddYears(1);
                                        while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                        {
                                            if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                            {
                                                UpdateApptRecTable(Today, dr);
                                            }
                                            OccurCtr++;
                                            Today = Today.AddYears(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }//range=1
                                }//no days entered
                                else // weekday is selected
                                {
                                    WeekOfMonth = (WeekOfMonth == -1) ? 1 : WeekOfMonth;
                                    if ((Range == 0) || (Range == 2))
                                    {
                                        while ((Today.Year < EndByDate.Year) || ((Today.Month <= EndByDate.Month)) && (Today.Year == EndByDate.Year))
                                        {
                                            //Move back to first of the month and add diff in no. of months needed to get to the selected month
                                            Today = Today.AddDays((Today.Day * -1) + 1).AddMonths(Month - Today.Month);
                                            DayCtr = 0;

                                            if (WeekOfMonth != 1)
                                            {
                                                while (DayCtr < WeekOfMonth)
                                                {
                                                    if (SelectedDays.Contains((int)Today.DayOfWeek))
                                                    {
                                                        Today = Today.AddDays(1);
                                                        DayCtr = DayCtr + 1;
                                                    }
                                                    else
                                                        Today = Today.AddDays(1);
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                Today = Today.AddDays(-1);//subtract one here because you had to shift it one above this
                                                if ((Today >= StartOn) && (Today <= EndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                }
                                            }
                                            else
                                            {
                                                bool bHit = false;
                                                while ((DayCtr < (WeekOfMonth * 7)) && (!bHit))
                                                {
                                                    if (!(SelectedDays.Contains((int)Today.DayOfWeek)))
                                                        Today = Today.AddDays(1);
                                                    else
                                                        bHit = true;
                                                    DayCtr = DayCtr + 1;
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                if ((Today >= StartOn) && (Today <= EndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    bHit = false;
                                                }
                                            }

                                            Today = Today.AddYears(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }
                                    }//range=0,2
                                    else if (Range == 1)
                                    {//occurrences
                                        //Get to first day of the start
                                        Today = StartOn;
                                        TmpEndByDate = EndByDate.AddMonths(1);
                                        while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                        {
                                            DayCtr = 0;

                                            //Move back to first of the month and add diff in no. of months needed to get to the selected month
                                            Today = Today.AddDays((Today.Day * -1) + 1).AddMonths(Month - Today.Month);

                                            if (WeekOfMonth != 1)
                                            {
                                                while (DayCtr < WeekOfMonth)
                                                {
                                                    if (SelectedDays.Contains((int)Today.DayOfWeek))
                                                    {
                                                        Today = Today.AddDays(1);
                                                        DayCtr = DayCtr + 1;
                                                    }
                                                    else
                                                        Today = Today.AddDays(1);
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                Today = Today.AddDays(-1);//subtract one here because you had to shift it one above this
                                                if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    OccurCtr++;
                                                }
                                            }
                                            else
                                            {
                                                bool bHit = false;
                                                while ((DayCtr < (WeekOfMonth * 7)) && (!bHit))
                                                {
                                                    if (!(SelectedDays.Contains((int)Today.DayOfWeek)))
                                                        Today = Today.AddDays(1);
                                                    else
                                                        bHit = true;
                                                    DayCtr = DayCtr + 1;
                                                    if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                                    {
                                                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                        break;
                                                    }
                                                }
                                                if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                                {
                                                    UpdateApptRecTable(Today, dr);
                                                    bHit = false;
                                                    OccurCtr++;
                                                }
                                            }
                                            Today = Today.AddYears(1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }//while
                                    }
                                }//no weekday selected
                            }//on nth day of month
                            else if (WeekOfMonth == 5)
                            {
                                if ((Range == 0) || (Range == 2))
                                {
                                    Today = StartOn.AddDays((StartOn.Day * -1) + 1);
                                    // end by date or no end date
                                    while ((Today.Year < (EndByDate.Year + 1)) || ((Today.Month <= EndByDate.Month)) && (Today.Year == EndByDate.Year))
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddMonths(1).AddDays(-1);// go to first of next month and then -1 to get to the last of this month
                                        Today = Today.AddMonths(Month - Today.Month);//Add difference to get to the selected month.

                                        //traverse back to check if today is one of the desired day and then exit loop; if it is the last day criteria, dont enter loop
                                        while ((SelectedDays.Count > 0) && (!(SelectedDays.Contains((int)Today.DayOfWeek))))
                                        {
                                            Today = Today.AddDays(-1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }

                                        if ((Today >= StartOn) && (Today <= EndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                        }

                                        Today = Today.AddYears(Periodicity);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }
                                else if (Range == 1)
                                {
                                    TmpEndByDate = EndByDate.AddYears(1);
                                    Today = StartOn.AddDays((StartOn.Day * -1) + 1);
                                    while ((OccurCtr <= OccurenceCT) && (Today < TmpEndByDate))
                                    {
                                        Today = Today.AddDays((Today.Day * -1) + 1);//go back to first of this month.
                                        Today = Today.AddMonths(1).AddDays(-1);// go to first of next month and then -1 to get to the last of this month
                                        Today = Today.AddMonths(Month - Today.Month);//Add difference to get to the selected month.

                                        //traverse back to check if today is one of the desired day and then exit loop; if it is the last day criteria, dont enter loop
                                        while ((SelectedDays.Count > 0) && (!(SelectedDays.Contains((int)Today.DayOfWeek))))
                                        {
                                            Today = Today.AddDays(-1);
                                            if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                            {
                                                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                                break;
                                            }
                                        }

                                        if ((Today >= StartOn) && (Today <= TmpEndByDate))
                                        {
                                            UpdateApptRecTable(Today, dr);
                                            OccurCtr++;
                                        }
                                        Today = Today.AddYears(1);
                                        if (sw.Elapsed > TimeSpan.FromMinutes(1))
                                        {
                                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Infinite Loop ? Check = ApptID:" + ApptID.ToString() + "; Type:" + Type.ToString() + "; Range:" + Range.ToString() + "; SelectedDays.Count:" + SelectedDays.Count.ToString(), string.Empty);
                                            break;
                                        }
                                    }
                                }//end range
                            }// last day of month
                        }//done with recurrence

                        //}//test
                        //}//if count <= 0
                    }
                    sw.Stop();
                    if (ApptID != -1)
                        CommitApptData(ApptID);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "There are no records to process", string.Empty);
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error: " + ex.Message + " ; " + ex.StackTrace, string.Empty);
            }
        }

        private void UpdateApptRecTable(DateTime dateIn, DataRow dr)
        {
            _FinalStDt = dateIn.Date.AddHours(_StHr).AddMinutes(_StMin).AddSeconds(_StSec);
            _FinalEndDt = ((DeltaDays>0) && ((StrtTmst != "00:00:00") || (EndTmst != "00:00:00"))) ? dateIn.Date.AddDays(DeltaDays).AddHours(_EndHr).AddMinutes(_EndMin).AddSeconds(_EndSec) : dateIn.Date.AddHours(_EndHr).AddMinutes(_EndMin).AddSeconds(_EndSec);
            if ((_FinalEndDt <= _FinalStDt) && ((StrtTmst != "00:00:00") || (EndTmst != "00:00:00")))
                _FinalEndDt = _FinalEndDt.Add(new TimeSpan(1, 0, 0, 0));
            else if (_FinalEndDt == _FinalStDt)
                _FinalEndDt = _FinalEndDt.Add(new TimeSpan(0, 23, 59, 59));

            ApptRcurncData apd = new ApptRcurncData();
            apd.ApptID = Int32.Parse(dr["APPT_ID"].ToString());
            apd.ApptTypeID = Int32.Parse(dr["APPTTypeID"].ToString());
            apd.StrtTmst = _FinalStDt;
            apd.EndTmst = _FinalEndDt;
            apd.AsnUserID = Int32.Parse(dr["ResourceID"].ToString());
            apd.CreatDt = DateTime.Now;

            rapdb.lARData.Add(apd);
        }

        private void CommitApptData(int ApptID)
        {
            if (rapdb.lARData.Count > 0)
            {
                try
                {
                    //remove data which is older than past 30 days
                    if (rapdb.lARData.Count > 0)
                        rapdb.lARData.RemoveAll(delegate (ApptRcurncData ard) { return (ard.StrtTmst < DateTime.Now.AddDays(-30).Date); });

                    if (rapdb.lARData.Count == 0)
                    {
                        rapdb.UpdateTrigStatus(ApptID, ((byte)EApptRecStus.OlderThan30days));
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "All data within APPT_ID : " + ApptID.ToString() + "  is older than 30 days", string.Empty);
                    }
                    else
                    {
                        rapdb.CommitApptRecData(ApptID);
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Successfully Processed APPT_ID : " + ApptID.ToString() + " ; Inserted " + rapdb.lARData.Count.ToString() + " records into APPT_RCURNC_DATA table", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    rapdb.UpdateTrigStatus(ApptID, ((byte)EApptRecStus.Failure));
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Failed to Process ApptID:" + ApptID.ToString() + "; Error: " + ex.Message + " ; " + ex.StackTrace, string.Empty);
                }
                finally { rapdb.lARData.Clear(); }
            }
            else
            {
                rapdb.UpdateTrigStatus(ApptID, ((byte)EApptRecStus.Failure));
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Failed to Process APPT_ID : " + ApptID.ToString(), string.Empty);
            }
        }

        private bool IsWeekDay(DateTime inDT)
        {
            if (!((inDT.DayOfWeek == DayOfWeek.Sunday) || (inDT.DayOfWeek == DayOfWeek.Saturday)))
                return true;
            else
                return false;
        }
    }
}