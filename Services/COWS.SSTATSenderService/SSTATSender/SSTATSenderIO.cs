﻿using IOManager;
using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;


namespace COWS.SSTATSenderService
{
    public class SSTATSenderIO
    {
        public int _iSleepTime = 0;

        private DataRow DR;
        private string XMLStr = string.Empty;
        private string TRAN_ID = string.Empty;
        private string CSG_LVL_ID = string.Empty;
        private SSTATSenderDB _SenderDB = new SSTATSenderDB();

        public void GetSSTATRequests(ref MQC _mqClient)
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataTable DTDisc = new DataTable();
            CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Reading DB", string.Empty);

            DS = _SenderDB.GetSSTATRequests();

            CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Back from DB", string.Empty);

            if (DS != null & DS.Tables.Count > 0)
            {
                CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "falling into logic", string.Empty);

                DT = DS.Tables[0];

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "falling into table logic", string.Empty);

                    int iCnt = DT.Rows.Count;

                    CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send Request(s) to ODIE", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];
                        XMLStr = string.Empty;
                        TRAN_ID = DR["TRAN_ID"].ToString();
                        CSG_LVL_ID = DR["CSG_LVL_ID"].ToString();
                        int iStat = 11;

                        try
                        {
                            switch (Convert.ToInt32(DR["SSTAT_MSG_ID"].ToString()))
                            {
                                case 1:
                                    XMLStr = GenerateCPEInfoMsg();
                                    break;

                                case 2:
                                    XMLStr = GenerateCPECancelMsg();
                                    break;

                                case 3:
                                    XMLStr = GenerateCPECompleteMsg();
                                    break;

                                case 4:
                                    XMLStr = GenerateCPErtsMsg();
                                    break;

                                case 5:
                                    XMLStr = GenerateCPEccdMsg();
                                    break;

                                case 6:
                                    XMLStr = GenerateCpeDispatchEmail();
                                    break;

                                default:
                                    break;
                            }

                            if (CSG_LVL_ID == "0")
                                CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "SSTAT Request XML is: ", XMLStr);
                            else
                            {
                                try
                                {
                                    _SenderDB.InsertCustomerSecuredLog(Convert.ToInt32(TRAN_ID), 102, XMLStr);
                                }
                                catch (Exception e)
                                {
                                    CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error inserting Log message into CUST_SCRD_LOG table for REQ_ID " + TRAN_ID, e.Message.ToString());
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            iStat = 12;
                            CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error generating SSTAT XML for REQ_ID " + TRAN_ID, e.Message.ToString());
                        }

                        if (XMLStr.Length > 0)
                        {
                            try
                            {
                                CL.WriteLogFile("Request", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Request send to SSTAT with Success, REQ_ID = " + TRAN_ID, string.Empty);

                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                DataRow dr;
                                ds = _SenderDB.getSSTATKey();
                                dt = ds.Tables[0];
                                dr = dt.Rows[0];
                                
                                string key = dr["KEY"].ToString();

                                string encryptedData = Encrypt(XMLStr, key);

                                _mqClient.MQPut(encryptedData);
                            }
                            catch (Exception e)
                            {
                                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "MQ Error putting XML in the SSTAT Request Queue for REQ_ID " + TRAN_ID, e.Message.ToString() + e.InnerException.ToString());
                                iStat = 12;
                            }
                        }

                        if (_SenderDB.UpdateSSTATRequest(Convert.ToInt32(TRAN_ID), iStat) == 0)
                        {
                            CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error updating the status in the SSTAT_REQ table for REQ_ID: " + TRAN_ID, string.Empty);
                        }
                    }
                }
                else
                {
                    CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Entering wait", string.Empty);

                    if (DateTime.Now.Second % 30 == 0)
                    {
                        CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to SSTAT", string.Empty);
                    }
                }
            }
            CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to SSTAT", string.Empty);
        }

        #region "Private Methods"

        private string GenerateCPEInfoMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtCustomer = new DataTable();
                DataTable dtContact = new DataTable();
                DataTable dtOrderLevel = new DataTable();
                DataTable dtLineItem = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getSSTATCPEMsgInfo(Convert.ToInt32(TRAN_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];
                    dtCustomer = ds.Tables[1];
                    dtContact = ds.Tables[2];
                    dtOrderLevel = ds.Tables[3];
                    dtLineItem = ds.Tables[4];
                    dtNotes = ds.Tables[5];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrder",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TranID", Convert.ToInt32(TRAN_ID))
                            );
                    XElement _orderInfo = new XElement("OrderInformation",
                           new XElement("OrderID", dtOrder.Rows[0]["OrderID"]),
                           new XElement("OrderType", dtOrder.Rows[0]["OrderType"]),
                           new XElement("OrderSubType", dtOrder.Rows[0]["OrderSubType"]),
                           new XElement("OrderAction", dtOrder.Rows[0]["OrderAction"]),
                           new XElement("ProductType", dtOrder.Rows[0]["ProductType"]),
                           new XElement("DomesticInternationalFlag", dtOrder.Rows[0]["DomesticInternationalFlag"]),
                           new XElement("CSGLevel", dtOrder.Rows[0]["CSGLevel"]),
                           new XElement("ParentOrderID", dtOrder.Rows[0]["ParentOrderID"]),
                           new XElement("RelatedOrderID", dtOrder.Rows[0]["RelatedOrderID"]),
                           new XElement("CustomerCommitDate", dtOrder.Rows[0]["CustomerCommitDate"]),
                           new XElement("CustomerPremiseCurrentlyOccupiedFlag", dtOrder.Rows[0]["CustomerPremiseCurrentlyOccupiedFlag"]),
                           new XElement("SiteID", dtOrder.Rows[0]["SiteID"]),
                           new XElement("ScaNbr", dtOrder.Rows[0]["ScaNbr"])
                           );
                    _cpeMessage.Add(_orderInfo);

                    XElement _cpeCustInfo = new XElement("CustomerInfo");

                    if (dtCustomer.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCustomer.Rows.Count; i++)
                        {
                            XElement _hierachyInfo = new XElement("CISHierarchyInfo",
                               new XElement("LevelType", dtCustomer.Rows[i]["LevelType"]),
                               new XElement("CustomerID", dtCustomer.Rows[i]["CustomerID"]),
                               new XElement("CustomerName", dtCustomer.Rows[i]["CustomerName"])
                               );

                            XElement _addressInfo = new XElement("Address",
                               new XElement("AddressLine1", dtCustomer.Rows[i]["AddressLine1"]),
                               new XElement("AddressLine2", dtCustomer.Rows[i]["AddressLine2"]),
                               new XElement("AddressLine3", dtCustomer.Rows[i]["AddressLine3"]),
                               new XElement("Suite", dtCustomer.Rows[i]["Suite"]),
                               new XElement("Building", dtCustomer.Rows[i]["Building"]),
                               new XElement("Floor", dtCustomer.Rows[i]["Floor"]),
                               new XElement("Room", dtCustomer.Rows[i]["Room"]),
                               new XElement("City", dtCustomer.Rows[i]["City"]),
                               new XElement("StateCode", dtCustomer.Rows[i]["StateCode"]),
                               new XElement("ZIPPostalCode", dtCustomer.Rows[i]["ZIPPostalCode"]),
                               new XElement("CntryCode", dtCustomer.Rows[i]["CntryCode"]),
                               new XElement("ProvinceMuncipality", dtCustomer.Rows[i]["ProvinceMuncipality"])
                               );

                            XElement _phoneInfo = new XElement("PhoneInfo",
                              new XElement("PhoneType", dtCustomer.Rows[i]["PhoneType"]),
                              new XElement("CountryCode", dtCustomer.Rows[i]["CountryCode"]),
                              new XElement("NPA", dtCustomer.Rows[i]["NPA"]),
                              new XElement("NXX", dtCustomer.Rows[i]["NXX"]),
                              new XElement("Station", dtCustomer.Rows[i]["Station"]),
                              new XElement("CityCode", dtCustomer.Rows[i]["CityCode"]),
                              new XElement("Number", dtCustomer.Rows[i]["Number"])
                              );

                            _hierachyInfo.Add(_addressInfo);
                            _hierachyInfo.Add(_phoneInfo);
                            _cpeCustInfo.Add(_hierachyInfo);
                        }
                    }

                    if (dtContact.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtContact.Rows.Count; i++)
                        {
                            XElement _contactInfo = new XElement("ContactInfo");
                            _contactInfo.Add(new XElement("Type", dtContact.Rows[i]["Type"]));
                            _contactInfo.Add(new XElement("Role", dtContact.Rows[i]["Role"]));

                            XElement _cntctNameInfo = new XElement("ContactNameInfo",
                             new XElement("FirstName", dtContact.Rows[i]["FirstName"]),
                             new XElement("LastName", dtContact.Rows[i]["LastName"]),
                             new XElement("Name", dtContact.Rows[i]["Name"]),
                             new XElement("EmailAddress", dtContact.Rows[i]["EmailAddress"]));

                            _contactInfo.Add(_cntctNameInfo);

                            _contactInfo.Add(
                               new XElement("AddressLine1", dtContact.Rows[i]["AddressLine1"]),
                               new XElement("AddressLine2", dtContact.Rows[i]["AddressLine2"]),
                               new XElement("City", dtContact.Rows[i]["City"]),
                               new XElement("StateCode", dtContact.Rows[i]["StateCode"]),
                               new XElement("ZIPPostalCode", dtContact.Rows[i]["ZIPPostalCode"]),
                               new XElement("CntryCode", dtContact.Rows[i]["CntryCode"]),
                               new XElement("ProvinceMuncipality", dtContact.Rows[i]["ProvinceMuncipality"])

                               );

                            XElement _cntctphone = new XElement("PhoneInfo",
                              new XElement("PhoneType", dtContact.Rows[i]["PhoneType"]),
                              new XElement("CountryCode", dtContact.Rows[i]["CountryCode"]),
                              new XElement("NPA", dtContact.Rows[i]["NPA"]),
                              new XElement("NXX", dtContact.Rows[i]["NXX"]),
                              new XElement("Station", dtContact.Rows[i]["Station"]),
                              new XElement("CityCode", dtContact.Rows[i]["CityCode"]),
                              new XElement("Number", dtContact.Rows[i]["Number"]),
                              new XElement("Extension", dtContact.Rows[i]["Extension"])
                              );

                            _contactInfo.Add(_cntctphone);
                            _cpeCustInfo.Add(_contactInfo);
                        }

                        _cpeMessage.Add(_cpeCustInfo);
                    }

                    XElement _itemlist = new XElement("CPEInformation");

                    XElement _cpeInfo = new XElement("CPEInfo",
                                new XElement("CPEOrderTypeCode", dtOrderLevel.Rows[0]["CPEOrderTypeCode"]),
                                new XElement("EquipmentOnlyFlagCode", dtOrderLevel.Rows[0]["EquipmentOnlyFlagCode"]),
                                new XElement("RecordsOnlyOrderFlagCode", dtOrderLevel.Rows[0]["RecordsOnlyOrderFlagCode"]),
                                new XElement("AccessProviderCode", dtOrderLevel.Rows[0]["AccessProviderCode"]),
                                new XElement("CPEPhoneNumber", dtOrderLevel.Rows[0]["CPEPhoneNumber"]),
                                new XElement("ECCKTIdentifier", dtOrderLevel.Rows[0]["ECCKTIdentifier"]),
                                new XElement("ManagedServicesChannelProgramCode", dtOrderLevel.Rows[0]["ManagedServicesChannelProgramCode"]),
                                new XElement("DeliveryDuties", dtOrderLevel.Rows[0]["DeliveryDuties"])
                                );

                    if (dtLineItem.Rows.Count > 0)
                    {
                        XElement _lineInfo = new XElement("CPEEquipItems");

                        for (int i = 0; i < dtLineItem.Rows.Count; i++)
                        {
                            XElement _lineitem = new XElement("CPEItem",
                                new XElement("ItemAction", dtLineItem.Rows[i]["ItemAction"]),
                                new XElement("EquipmentTypeCode", dtLineItem.Rows[i]["EquipmentTypeCode"]),
                                new XElement("MatCode", dtLineItem.Rows[i]["MatCode"]),
                                new XElement("Description", dtLineItem.Rows[i]["Description"]),
                                new XElement("Manufacturer", dtLineItem.Rows[i]["Manufacturer"]),
                                new XElement("DeviceID", dtLineItem.Rows[i]["DeviceID"]),
                                new XElement("ProjectID", dtLineItem.Rows[i]["ProjectID"]),
                                new XElement("Quantity", dtLineItem.Rows[i]["Quantity"]),
                                new XElement("ContractType", dtLineItem.Rows[i]["ContractType"]),
                                new XElement("ContractTerm", dtLineItem.Rows[i]["ContractTerm"]),
                                new XElement("InstallationFlagCode", dtLineItem.Rows[i]["InstallationFlagCode"]),
                                new XElement("MaintenanceFlagCode", dtLineItem.Rows[i]["MaintenanceFlagCode"]),
                                new XElement("FMSCktNbr", dtLineItem.Rows[i]["FMSCktNbr"]),
                                new XElement("CompFamily", dtLineItem.Rows[i]["CompFamily"]),
                                new XElement("ComponentId", dtLineItem.Rows[i]["ComponentId"]),
                                new XElement("PoNbr", dtLineItem.Rows[i]["PoNbr"]),
                                new XElement("Reuse", dtLineItem.Rows[i]["Reuse"])
                                );

                            _lineInfo.Add(_lineitem);
                        }
                        _cpeInfo.Add(_lineInfo);
                    }

                    _itemlist.Add(_cpeInfo);
                    _cpeMessage.Add(_itemlist);

                    XElement _noteslist = new XElement("Notes");

                    if (dtNotes.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtNotes.Rows.Count; i++)
                        {
                            XElement _note = new XElement("Note",
                                new XElement("NoteText", dtNotes.Rows[i]["NoteText"])
                                );

                            _noteslist.Add(_note);
                        }
                    }
                    
                    
                    _cpeMessage.Add(_noteslist);
                    
                    return _cpeMessage.ToStringIgnoreInvalidChars();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error generating the CPE Info XML request : " + ex.Message + "; " + ex.StackTrace, string.Empty);
                return string.Empty;
            }
        }

        private string GenerateCPECancelMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getSSTATCancel(Convert.ToInt32(TRAN_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderStatus",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TRANID", Convert.ToInt32(TRAN_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["FTN"]),
                            new XElement("DeviceID", dtOrder.Rows[0]["DEVICE_ID"]),
                            new XElement("OrderAction", "Cancel"),
                           new XElement("CompletionDate", ""),
                           new XElement("Note", ""),
                           new XElement("ErrorMsg", ""),
                           new XElement("CCD", "")
                           );

                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPECompleteMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getSSTATCancel(Convert.ToInt32(TRAN_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderStatus",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TRANID", Convert.ToInt32(TRAN_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["FTN"]),
                            new XElement("DeviceID", dtOrder.Rows[0]["DEVICE_ID"]),
                            new XElement("OrderAction", "Complete"),
                           new XElement("CompletionDate", DateTime.Now),
                           new XElement("Note", ""),
                           new XElement("ErrorMsg", ""),
                           new XElement("CCD", "")
                           );

                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPErtsMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getSSTATCancel(Convert.ToInt32(TRAN_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderStatus",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TRANID", Convert.ToInt32(TRAN_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["FTN"]),
                            new XElement("DeviceID", dtOrder.Rows[0]["DEVICE_ID"]),
                            new XElement("OrderAction", "RTS-Hold"),
                           new XElement("CompletionDate", ""),
                           new XElement("Note", ""),
                           new XElement("ErrorMsg", ""),
                           new XElement("CCD", "")
                           );

                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCPEccdMsg()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtOrder = new DataTable();
                DataTable dtNotes = new DataTable();

                ds = _SenderDB.getSSTATCCD(Convert.ToInt32(TRAN_ID));

                if (ds != null)
                {
                    dtOrder = ds.Tables[0];

                    XMLHelper _XHelper = new XMLHelper();

                    XElement _cpeMessage = new XElement("CPEOrderStatus",
                            new XElement("TimeStamp", DateTime.Now),
                            new XElement("TRANID", Convert.ToInt32(TRAN_ID)),
                            new XElement("OrderID", dtOrder.Rows[0]["FTN"]),
                            new XElement("DeviceID", dtOrder.Rows[0]["DEVICE_ID"]),
                            new XElement("OrderAction", "CCD"),
                           new XElement("CompletionDate", ""),
                           new XElement("Note", ""),
                           new XElement("ErrorMsg", ""),
                           new XElement("CCD", dtOrder.Rows[0]["CCD"])
                           );

                    return _cpeMessage.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCpeDispatchEmail()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtCpeDispatchEmail = new DataTable();

                dtCpeDispatchEmail = _SenderDB.getSSTATCPEDispatchEmail(Convert.ToInt32(TRAN_ID));

                if (dtCpeDispatchEmail != null)
                {
                    //Test
                    var xml = dtCpeDispatchEmail.Rows[0]["EMAIL_BODY_TXT"].ToString();

                    return xml;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error generating the CPE Dispatch XML request : " + ex.Message + "; " + ex.StackTrace, string.Empty);
                return string.Empty;
            }
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        private RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private String Encrypt(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
        }

        private byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        private String Decrypt(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }

        #endregion "Private Methods"
    }
}