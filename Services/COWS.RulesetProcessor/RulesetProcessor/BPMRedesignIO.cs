﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using CL = LogManager.FileLogger;

namespace COWS.RulesetProcessor
{
    public class BPMRedesignIO
    {
        private BPMRedesignDB _SenderDB = new BPMRedesignDB();

        public void ProcessBPMRedesigns()
        {
            SendBPMTransactionsByRecStusId(261); // FOR REC_STUS_ID = 261
            SendBPMTransactionsByRecStusId(263); // FOR REC_STUS_ID = 263
        }

        public void SendBPMTransactionsByRecStusId(int recStusId)
        {
            int id = 0;

            try
            {
                DataTable DT = new DataTable();

                // Get Transactions
                DT = _SenderDB.GetByRecStusId(recStusId);

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    foreach (DataRow dr in DT.Rows)
                    {
                        id = Convert.ToInt32(dr["ID"]);
                        string data = dr["BPM_DATA_TXT"].ToString();

                        if (!string.IsNullOrEmpty(data))
                        {
                            // Transform into XML
                            XmlDocument xml = new XmlDocument();
                            xml.LoadXml(data);

                            XmlNode xmlNode = xml.SelectSingleNode("descendant::BPMInfo");
                            CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Processing BPM Redesign: " + xmlNode.OuterXml, string.Empty);

                            string comment = xmlNode.SelectSingleNode("CMNT_TXT") != null ? xmlNode.SelectSingleNode("CMNT_TXT").InnerXml : "";
                            string createDate = xmlNode.SelectSingleNode("CREATE_DT") != null ? xmlNode.SelectSingleNode("CREATE_DT").InnerXml : "";
                            string expiryDate = xmlNode.SelectSingleNode("EXPIRY_DT") != null ? xmlNode.SelectSingleNode("EXPIRY_DT").InnerXml : "";
                            string h1 = xmlNode.SelectSingleNode("H1_ID") != null ? xmlNode.SelectSingleNode("H1_ID").InnerXml : "";
                            string mns_dd_nbr = xmlNode.SelectSingleNode("MNS_DD_NBR") != null ? xmlNode.SelectSingleNode("MNS_DD_NBR").InnerXml : "";
                            string pr_dd_nbr = xmlNode.SelectSingleNode("PR_DD_NBR") != null ? xmlNode.SelectSingleNode("PR_DD_NBR").InnerXml : "";
                            string slaDate = xmlNode.SelectSingleNode("SLA_DT") != null ? xmlNode.SelectSingleNode("SLA_DT").InnerXml : "";
                            string submitDate = xmlNode.SelectSingleNode("SUBMIT_DT") != null ? xmlNode.SelectSingleNode("SUBMIT_DT").InnerXml : "";
                            string status = (xmlNode.SelectSingleNode("STUS_NME") != null) ? xmlNode.SelectSingleNode("STUS_NME").InnerXml : "";

                            // Send to BPM
                            string username = ConfigurationManager.AppSettings["BPMRedesignUsername"].ToString();
                            string password = ConfigurationManager.AppSettings["BPMRedesignPassword"].ToString();
                            long response = SendBPMTransactions(username, password, comment, createDate, expiryDate, h1, mns_dd_nbr, pr_dd_nbr, slaDate, submitDate, status);

                            //foreach (XmlNode xmlNode in xml.GetElementsByTagName("BPMInfo"))
                            //{
                            //    string comment = xmlNode.SelectSingleNode("CMNT_TXT").InnerXml;
                            //    string createDate = xmlNode.SelectSingleNode("CREATE_DT").InnerXml;
                            //    string h1 = xmlNode.SelectSingleNode("H1_ID").InnerXml;
                            //    string mns_dd_nbr = xmlNode.SelectSingleNode("MNS_DD_NBR").InnerXml;
                            //    string pr_dd_nbr = xmlNode.SelectSingleNode("MNS_DD_NBR").InnerXml;
                            //    string status = xmlNode.SelectSingleNode("STUS_NME").InnerXml;

                            //    // Send to BPM
                            //    SendBPMTransactions("", "", comment, createDate, h1, mns_dd_nbr, pr_dd_nbr, status);
                            //}

                            // SUCCESS
                            if (response > 0)
                            {
                                CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "BPM_REDSN_TXN.ID = " + id + " successfully sent to BPM", string.Empty);
                                // Update RecStusId (261 > 262), (263 > 264)
                                _SenderDB.Update(id, recStusId + 1);
                            }
                            // FAILED
                            else
                            {
                                CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "BPM_REDSN_TXN.ID = " + id + " request failed", string.Empty);
                                _SenderDB.Update(id, 265);
                            }
                        }
                        else
                        {
                            CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "BPM_REDSN_TXN.ID = " + id + " has no XML Data", string.Empty);
                            _SenderDB.Update(id, 265);
                        }

                        id = 0;
                    }
                }
                else
                {
                    CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "No new transactions for: (REC_STUS_ID = " + recStusId + ")", string.Empty);
                }
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error sending BPM Redesign Transactions", e.Message + e.StackTrace);
                _SenderDB.Update(id, 265);
            }
        }

        public long SendBPMTransactions(string username, string password, string comments, string createDate, string expiry_date, string h1, string mns_dd_nbr, string pr_dd_nbr, string sla_date, 
            string submit_date, string status)
        {
            long resp = 0;
            BPMRedesignReference.StartProcessIFClient soapClient = new BPMRedesignReference.StartProcessIFClient();

            CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Request sent to BPM for Redesign #: " + mns_dd_nbr, string.Empty);
            try
            {
                if (ConfigurationManager.AppSettings["BPMRedesignUrl"].ToString().Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }

                resp = soapClient.start(username, password, comments, createDate, expiry_date, h1, mns_dd_nbr, pr_dd_nbr, sla_date, submit_date, status);
                CL.WriteLogFile("Response", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Response received from BPM-- " + resp + "; Redesign #: " + mns_dd_nbr, string.Empty);
            }
            catch (Exception error)
            {
                resp = 0;
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error received from BPM for Redesign #: " + mns_dd_nbr, error.Message + error.StackTrace);
            }

            return resp;
        }

        private bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}