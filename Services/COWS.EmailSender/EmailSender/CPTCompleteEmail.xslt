﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:include href="style.xslt" />
	<xsl:template match="/">
		<html>
			<body style="font-family: Verdana; font-size: 10pt; margin: 5; padding: 5;WORD-BREAK:break-word;">
				<p>
					This is an automated message from <a href="http://cows">http://cows</a> for the
					customer provisioning request placed on <xsl:value-of select="/CPTEmail/CPTInfo/CreatedDate" />.
					All application provisioning associated with this request has been completed and status
					has been changed to Complete. Below is the status of the various application provisioning tasks.
					If you have questions about this request, please send an email to
					<a href="mailto:IPS-MSOnBoarding@t-mobile.com">IPS-MS OnBoarding</a>.
				</p>

				<p>
					<ul>
						<li>
							Shortname: <b>
								<i>
									<xsl:value-of select="/CPTEmail/CPTInfo/Shortname" />
								</i>
							</b> has been created for this request.
						</li>
						<li>
							MNS Managed Domain: <b>
								<i>
									<xsl:value-of select="/CPTEmail/CPTInfo/QIPSubnetName" />
								</i>
							</b> has been created for this request.
						</li>
						<li>
							QIP Provisioning: <xsl:value-of select="/CPTEmail/CPTInfo/QIP" />
						</li>
						<li>
							Spectrum Server:
							<xsl:if test="/CPTEmail/CPTInfo/Spectrum[.!='']">
								<xsl:value-of select="/CPTEmail/CPTInfo/Spectrum" />
							</xsl:if>
							<xsl:if test="/CPTEmail/CPTInfo/Spectrum[.='']">
								N/A
							</xsl:if>
						</li>
						<li>
							Voyence Provisioning: <xsl:value-of select="/CPTEmail/CPTInfo/Voyence" />
						</li>
					</ul>
				</p>

				<p>
					To view the request please go the <a href="http://cows">http://cows</a> and navigate to CPT then Search.
					Using the search criteria please search for the CPT request you are looking for.
				</p>

				<p>
					If you have concerns regarding the MNS personnel assignment process,
					please send an email to the <a href="mailto:MNS-CPT@t-mobile.com">MNS-CPT@t-mobile.com PDL</a>.
				</p>

				<p>
					If you have concerns about the application provisioning aspects of this request,
					you can open a ticket with the NOS team by: <br></br>
					<ol>
						<li>Visiting IP Services.</li>
						<li>Select Systems Issues / TST Escalation under the "What do you need help with?" pull-down menu.</li>
						<li>
							Select Customer Provisioning Tool - Managed Services under the
							"Step 1: What are you currently experiencing problems with?" pull down menu.
						</li>
					</ol>
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>