﻿namespace AppConstants
{
    public enum EEventWorkFlowStatus
    {
        Visible = 1,
        Submit,
        Retract,
        Publish,
        Reschedule,
        InProgress,
        Complete,
        Return,
        Reject,
        OnHold,
        Delete,
        Fulfilling,
        Shipped,
        PendingDeviceReturn,
        CompletePendingUAT,
        InProgressUAT
    }
}