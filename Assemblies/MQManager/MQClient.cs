using IBM.WMQ;
using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using FL = LogManager.FileLogger;

namespace MQManager // was mcon
{
    /// <summary>
    /// This class provides access to the MQ Server and Queues.
    /// This version does cleanup.
    /// These classes are slightly thread safe as of 2006Dec29. You cannot
    /// have 2 apps doing a get, since they overwrite the input queue buffer.
    /// To make it safe, you would need to lock the queue, in case of simultaneous
    /// gets. It would be safer to have a instance of MQClient for each thread.
    /// </summary>
    public class MQClient
    {
        private MQQueueManager _mqQMgr;				// MQQueueManager instance
        private MQQueue _mqQueue;				// MQQueue instance
        private MQMessage _mqMsg;					// MQMessage instance
        private MQPutMessageOptions _mqPutMsgOpts;			// MQPutMessageOptions instance

        private string _hostName = "";
        private string _mqManagerName = "";
        private string _mqQueueInName = "";		// Name of queue to use
        private string _mqQueueOutName = "";
        private string _channelName = "";
        private string _mqError = "";
        private string _mqReason = "";
        private int _mqWaitInterval = -1;
        private int _mqSyncPointAvailability = -1;
        private bool _mqSyncPointAvailable = false;
        private bool _mqSyncPointInProgress = false;

        //private string              _MQMessageFormat = "String A"; //dtn9708 2006Dec19
        //private string              _MQMessageFormat = MQC.MQFMT_STRING; //dtn9708 2006Dec19, 2007Jan31
        private string _MQMessageFormat = ""; //dtn9708 2006Dec19, dtn9708 2007Jan31

        //private int                 _MQMessageCharacterSet = 1200; //Unicode  dtn9708 2007Jan31
        private int _MQMessageCharacterSet = -1; //Unicode  dtn9708 2007Jan31

        private Exception _mqException = null;
        private Queue _que = null;
        private Queue _messageQue = null;
        private string _outString = "";

        private string _mqService = "";
        private string _mqConn = "";
        private DateTime dtPutDateTime;
        private bool _mqHasBeenInitialized = false;

        /// <summary>
        /// type of error
        /// </summary>
        public enum mqErrorType
        {
            /// <summary>
            /// OK
            /// </summary>
            mqNO_ERROR,

            /// <summary>
            /// Error
            /// </summary>
            mqERROR,

            /// <summary>
            /// Exception
            /// </summary>
            mqEXCEPTION
        };

        /// <summary>
        /// Type of objects returned by outQue, or outString.
        /// </summary>
        public enum QObjReturned
        {
            /// <summary>
            /// Returns a .Net Queue of strings
            /// </summary>
            QueueOfStrings,

            /// <summary>
            /// Returns a .Net Queue of MQMessageItem.
            /// This is a class of string message, byte[] messageId, replyToQueue and correlationId's.
            /// Just use the ones you need.
            /// </summary>
            QueueOfMQMessageItems,

            /// <summary>
            /// Returns one .Net string message
            /// </summary>
            OneString
        }//enum TypeOfQueueObjectsReturned

        private mqErrorType _errorType = mqErrorType.mqNO_ERROR;

        /// <summary>
        /// Used to queue and dequeue the combination
        /// of message, msgId, replyToQueue and correlationID as a queue of objects
        /// </summary>
        public class MQMessageItem
        {
            /// <summary>
            /// the message
            /// </summary>
            public string message;

            /// <summary>
            /// Hiding warning..
            /// </summary>
            public DateTime dtPutDateTime;

            /// <summary>
            /// obsolete - use messageId
            /// </summary>
            //[System.Obsolete("use messageId")]
            public byte[] id; //dtn9708 2006Dec28

            /// <summary>
            /// messageId - it was id
            /// </summary>
            public byte[] messageId; //dtn9708 2006Dec28

            /// <summary>
            /// queue name to reply to
            /// </summary>
            public string replyToQueue; //dtn9708 2006Dec18

            /// <summary>
            /// queue Manager to reply to
            /// </summary>
            public string replyToQueueManagerName; //dtn9708 2007Jan15

            /// <summary>
            /// message correlationId
            /// </summary>
            public byte[] correlationId; //dtn9708 2006Dec28
        }//MQMessageItem

        // ///////////////////////////////////////////////////////////////////////
        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="sService">1-5 character name of the service such as SB</param>
        /// <param name="sConn">SQL Connection string</param>
        public MQClient(string sService, string sConn)
        {
            //Debug.Assert(sService.Length > 0 && sService.Length < 6, "1st param error",
            //    "We need a service parameter of 1 to 5 characters.");
            //Debug.Assert(sConn.Length > 0, "2n param error",
            //    "We need a SQL connection string to open the configuration table.");

            _mqService = sService;
            _mqConn = sConn;
        }//constructor 1

        /// <summary>
        /// Constructor with no arguments. The developer needs to set all of the
        /// properties and initialize the object.
        /// </summary>
        public MQClient()
        {
        }//constructor 2

        /// <summary>
        /// Destructor closes MQ Queue, closes MQ Manager, then
        /// calls the Garbage Collector
        /// </summary>
        ~MQClient()
        {
            lock (this)// //dtn9708 2006Dec29
            {
                CloseQueue();
                _mqQueue = null;
                Close();
                _mqQMgr = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }//lock
        }//destructor

        /// <summary>
        /// PJ002133 - Overload to handle optional parameters in the main method
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {
            string sMQInName = String.Empty;
            string sMQOutName = String.Empty;

            return this.Initialize(sMQInName, sMQOutName);
        }

        /// <summary>
        /// Establish the SQL connection and populate the properties.
        /// </summary>
        /// <returns></returns>
        public bool Initialize(string sMQqueueInName, string sMQqueueOutName)
        {
            //Debug.Assert(false, "Initialize()"); //Test ********

            //PJ002133 - CAPT.SPMPReceiver/CAPT.SPMPSender need to pass queue name IN and OUT

            lock (this) //dtn9708 2006Dec29
            {
                try
                {
                    _hostName = ConfigurationManager.AppSettings["MQHostName"];
                    _mqManagerName = ConfigurationManager.AppSettings["MQQueueManager"];
                    if (sMQqueueInName != String.Empty)
                        _mqQueueInName = sMQqueueInName;
                    else
                        _mqQueueInName = ConfigurationManager.AppSettings["MQQueueIn"];
                    if (sMQqueueOutName != String.Empty)
                        _mqQueueOutName = sMQqueueOutName;
                    else
                        _mqQueueOutName = ConfigurationManager.AppSettings["MQQueueOut"];
                    _channelName = ConfigurationManager.AppSettings["MQChannel"];
                    _mqWaitInterval = Convert.ToInt32(ConfigurationManager.AppSettings["WaitInterval"]);
                    _MQMessageCharacterSet = Convert.ToInt32(ConfigurationManager.AppSettings["CharacterSet"]);
                    _MQMessageFormat = ConfigurationManager.AppSettings["Format"];
                }
                catch (Exception ex)
                {
                    FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - Initialize(string sMQqueueInName, string sMQqueueOutName) -Unable to Retrieve MQClient Configuration Settings " + ex.Message + " : " + ex.StackTrace, string.Empty);
                }

                //Debug.Assert(_hostName.Length > 1,
                //    "MQ HostName", "The MQ HostName was not set.");
                //Debug.Assert(_mqManagerName.Length > 1,
                //    "MQ Manager", "The MQ Manager was not set.");
                //Debug.Assert(_mqQueueInName.Length > 1,
                //    "MQ In Queue", "The MQ In Queue was not set.");
                //Debug.Assert(_mqQueueOutName.Length > 1,
                //    "MQ Out Queue", "The MQ Out Queue was not set.");
                //Debug.Assert(_channelName.Length > 1,
                //    "MQ Channel", "The MQ Channel was not set.");

                _mqHasBeenInitialized = true;

                if (_mqWaitInterval < 0)
                {
                    _mqWaitInterval = 1000; //1 second
                }
                if (_MQMessageCharacterSet < 0) //dtn9708 2007Jan31
                {
                    _MQMessageCharacterSet = 1200; //Unicode
                }
                if (_MQMessageFormat.Length < 1) //dtn9708 2007Jan31
                {
                    _MQMessageFormat = "MQSTR   "; //default string format
                }

                // /// create an MQRCText instance - string error messages.
                //mqrcText = new MQRCText();

                _que = new Queue();
                _messageQue = Queue.Synchronized(_que);

                bool ret = true;
                MQEnvironment.Hostname = _hostName;
                MQEnvironment.Channel = _channelName;

                try
                {
                    if (!ConnectQueueManager())
                        ret = false;
                }
                catch (MQException mqe)
                {
                    ret = false;
                    _errorType = mqErrorType.mqEXCEPTION;
                    _mqException = (Exception)mqe;
                    FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - Initialize(string sMQqueueInName, string sMQqueueOutName) - Unable to ConnectQueueManager " + mqe.Message + " : " + mqe.StackTrace, string.Empty);
                    return false;
                }
                return ret;
            }//lock
        }

        /// <summary>
        /// MQ Server name
        /// </summary>
        public string mqHostName
        {
            get
            {
                return _hostName;
            }
            set
            {
                _hostName = value;
            }
        }

        /// <summary>
        ///  MQ Manager name
        /// </summary>
        public string mqQueMgr
        {
            get
            {
                return _mqManagerName;
            }
            set
            {
                _mqManagerName = value;
            }
        }

        /// <summary>
        /// In queue name
        /// </summary>
        public string mqQueIn
        {
            get
            {
                return _mqQueueInName;
            }
            set
            {
                _mqQueueInName = value;
            }
        }

        /// <summary>
        /// Out queue name
        /// </summary>
        public string mqQueOut
        {
            get
            {
                return _mqQueueOutName;
            }
            set
            {
                _mqQueueOutName = value;
            }
        }

        /// <summary>
        /// Channel name
        /// </summary>
        public string mqChannel
        {
            get
            {
                return _channelName;
            }
            set
            {
                _channelName = value;
            }
        }

        /// <summary>
        /// .Net Queue output.
        /// Use this with QObjReturned.QueueOfStrings or QObjReturned.QueueOfMQMessageItems.
        /// </summary>
        public Queue outQue
        {
            get
            {
                return _messageQue;
            }
        }

        /// <summary>
        /// .Net string output.
        /// Use this with QObjReturned.OneString.
        /// </summary>
        public string outString
        {
            get
            {
                return _outString;
            }
        }

        /// <summary>
        /// error type
        /// </summary>
        public mqErrorType mqError
        {
            get
            {
                return _errorType;
            }
        }

        /// <summary>
        /// error string
        /// </summary>
        public string mqErrorString
        {
            get
            {
                return _mqError;
            }
        }

        /// <summary>
        /// Exception
        /// </summary>
        public Exception mqException
        {
            get
            {
                return _mqException;
            }
        }

        /// <summary>
        /// Error code
        /// </summary>
        public string mqReason
        {
            get
            {
                return _mqReason;
            }
        }

        /// <summary>
        /// This variable will be used to determine the maximum time to
        /// wait on an MQGet() before continuing. The value is in
        /// milliseconds. A suggested value would be
        /// 1000 ms which would be 1 second.
        /// 60000 ms would be 1 minute.
        /// </summary>
        public int mqWaitInterval
        {
            get
            {
                return _mqWaitInterval;
            }
            set
            {
                _mqWaitInterval = value;
            }
        }

        /// <summary>
        /// SyncPoint Availability. Compare to IBM.WMQ.MQC.MQSP_AVAILABLE.
        /// The queue manager must be open with ConnectQueueManager to set this.
        /// </summary>
        public int mqSyncPointAvailability
        {
            get
            {
                return _mqSyncPointAvailability;
            }
            set
            {
                _mqSyncPointAvailability = value;
            }
        }

        //[System.Obsolete("The queue environment must be set up for SyncPoint.",true)]
        //[System.Obsolete("The queue environment must be set up for SyncPoint.")]
        /// <summary>
        /// Boolean - can we do SyncPoint?
        /// The queue manager must be open with ConnectQueueManager to set this.
        /// </summary>
        public bool mqSyncPointAvailable
        {
            get
            {
                return _mqSyncPointAvailable;
            }
            set
            {
                _mqSyncPointAvailable = value;
            }
        }

        /// <summary>
        /// The MQ Message Format for either a Put() or a Get(). This will be used to set
        /// the property MQMessage.Format, just before use.
        /// For example, using the emun mqc.MQMessageFormat = MQC.MQFMT_STRING;
        /// The default is MQC.MQFMT_STRING.
        /// </summary>
        public string MQMessageFormat
        {
            get
            {
                return _MQMessageFormat;
            }
            set
            {
                _MQMessageFormat = value;
            }
        }

        /// <summary>
        /// The MQ Message CharacterSet for either a Put() or a Get(). This will be used to set
        /// the property MQMessage.CharacterSet, just before use.
        /// For example, mqc.MQMessageCharacterSet = 1200; //Unicode, 1208 is UTF-8. See
        /// the .Net documentation for the full table of Character set identifiers.
        /// Our default is 1200 - Unicode.
        /// </summary>
        public int MQMessageCharacterSet
        {
            get
            {
                return _MQMessageCharacterSet;
            }
            set
            {
                _MQMessageCharacterSet = value;
            }
        }

        //new 2007Jan16
        /// <summary>
        /// Open an MQQueue, Push a message, close the queue
        /// </summary>
        /// <param name="sPutMsg">string message that you are sending</param>
        /// <param name="correlationId">byte[] correlationId (GUID) that you are sending out</param>
        /// <param name="sReplyToQueueName">string containing the queue name you want to use to send the message</param>
        /// <param name="sReplyToQueueManagerName">string containing the queue manager name you want to use to send the message</param>
        /// <returns>message</returns>
        public bool MQPut(string sPutMsg, byte[] correlationId,
            string sReplyToQueueName, string sReplyToQueueManagerName) //200Jan18 dtn9708
        {
            //2007Feb07
            if (sPutMsg.Length < 1) //2007Feb08
            {
                return false; //nothing to do
            }//if

            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                bool ret = true;

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //dtn9708 2007Jan16
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }//if

                //for (; ; ) //2007Feb08
                //2007Feb08 retry after lost connection
                for (int ii = 0; ii < 2; ii++)
                {
                    // Try to open the queue
                    try
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT; //get the sealed constant from the base
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING; //get the sealed constant from the base

                        //The out que can be a dynamic queue which comes in as the
                        _mqQueue = _mqQMgr.AccessQueue(sReplyToQueueName, //dtn9708 2007Jan22
                                iOP | iFIQ,
                                sReplyToQueueManagerName, "",
                                "");

                        //2007Feb08
                        //}
                        //catch (MQException mqe)
                        //{
                        //    // stop if failed
                        //    ret = false;
                        //    _errorType = mqErrorType.mqEXCEPTION;
                        //    _mqException = (Exception)mqe;

                        //    //string mText = mqe.Message;
                        //    //throw new Exception("MQ AccessQueue MQException: " + mText, mqe);  //2007Feb07

                        //    return false;  //2007Feb07
                        //}

                        //2007Feb08
                        //if (sPutMsg.Length > 0)
                        //{
                        // put the next message to the queue
                        _mqMsg = new MQMessage();

                        //The queue where we are expecting to find messages or
                        //the queue name supplied by the original message
                        //_mqMsg.ReplyToQueueName = _mqQueueOutName; //dtn9708 2006Dec18
                        _mqMsg.ReplyToQueueName = _mqQueueInName; //dtn9708 2006Dec18

                        //_mqMsg.WriteString(sPutMsg); //2006Nov16 move after CharacterSet

                        //_mqMsg.Format = MQC.MQFMT_STRING; //2006Oct18
                        _mqMsg.Format = _MQMessageFormat;
                        _mqMsg.CharacterSet = _MQMessageCharacterSet; //2006Oct18

                        _mqMsg.WriteString(sPutMsg); //2006Nov16

                        _mqPutMsgOpts = new MQPutMessageOptions();
                        if (correlationId == null) //dtn9708 2007Jan18
                        {
                            byte[] correlId = { 0, 0, 0 }; //actually 24 long
                            correlationId = correlId;
                        }//if
                        _mqMsg.CorrelationId = correlationId; //GUID that you are sending dtn9708 2007Jan18

                        //2007Feb08
                        //try
                        //{
                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }//try
                    catch (MQException mqe)
                    {
                        //2007Feb08
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg, byte[] correlationId,string sReplyToQueueName, string sReplyToQueueManagerName) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //2007Feb07
                        //string mText = mqe.Message;
                        //throw new Exception("MQ Put MQException: " + mText, mqe);

                        break; //throw makes it unreachable

                        //return false; //2007Feb07
                    }//catch

                    //}//if

                    CloseQueue(); //release the queue handle - prevent error 2017

                    break;
                }//for
                return ret;
            }//lock
        }//MQPut

        /// <summary>
        /// Open an MQQueue, Push a message, close the queue
        /// </summary>
        /// <param name="sPutMsg">string message that you are sending</param>
        /// <param name="correlationId">byte[] correlationId (GUID) that you are sending out</param>
        /// <param name="sReplyToQueueName">string containing the queue name you want to use to send the message</param>
        /// <returns>message</returns>
        public bool MQPut(string sPutMsg, byte[] correlationId, string sReplyToQueueName) //2006Dec18 dtn9708

        //public bool MQPut(string sPutMsg) //2006Oct03 dtn9708
        {
            //2007Feb07
            if (sPutMsg.Length < 1) //2007Feb08
            {
                return false; //nothing to do
            }//if

            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                bool ret = true;

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //dtn9708 2007Jan16, 2007Jan18
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }//if

                //for (; ; ) //2007Feb08
                //2007Feb08 retry after lost connection
                for (int ii = 0; ii < 2; ii++)
                {
                    // Try to open the queue
                    try //2007Feb09
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT; //get the sealed constant from the base
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING; //get the sealed constant from the base

                        //The out que can be a dynamic queue which comes in as the
                        // Msg.ReplyToQueueName along with the correlationID to match it up.
                        //_mqQueue = _mqQMgr.AccessQueue(_mqQueueOutName, //dtn9708 2006Sep27
                        _mqQueue = _mqQMgr.AccessQueue(sReplyToQueueName, //dtn9708 2006Dec18
                                        iOP +
                                        iFIQ);

                        //2007Feb09
                        //}
                        //catch (MQException mqe)
                        //{
                        //    // stop if failed
                        //    ret = false;
                        //    _errorType = mqErrorType.mqEXCEPTION;
                        //    _mqException = (Exception)mqe;

                        //    //2007Feb07
                        //    //string mText = mqe.Message;
                        //    //throw new Exception("MQ AccessQueue MQException: " + mText, mqe); //2007Feb07

                        //    return false;
                        //}

                        //2007Feb09
                        //if (sPutMsg.Length > 0)
                        //{
                        // put the next message to the queue
                        _mqMsg = new MQMessage();

                        //The queue where we are expecting to find messages or
                        //the queue name supplied by the original message
                        //_mqMsg.ReplyToQueueName = _mqQueueOutName; //dtn9708 2006Dec18
                        _mqMsg.ReplyToQueueName = _mqQueueInName; //dtn9708 2006Dec18

                        //_mqMsg.WriteString(sPutMsg); //2006Nov16 move after CharacterSet

                        //_mqMsg.Format = MQC.MQFMT_STRING; //2006Oct18
                        _mqMsg.Format = _MQMessageFormat;
                        _mqMsg.CharacterSet = _MQMessageCharacterSet; //2006Oct18

                        _mqMsg.WriteString(sPutMsg); //2006Nov16

                        _mqPutMsgOpts = new MQPutMessageOptions();
                        if (correlationId == null)
                        {
                            byte[] correlId = { 0, 0, 0 }; //actually 24 long
                            correlationId = correlId;
                        }//if
                        _mqMsg.CorrelationId = correlationId; //GUID that you are sending 2006Oct03 dtn9708

                        //try //2007Feb08
                        //{
                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }//try
                    catch (MQException mqe)
                    {
                        //2007Feb07
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason
                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg, byte[] correlationId, string sReplyToQueueName) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //string mText = mqe.Message;
                        //throw new Exception("MQ Put MQException: " + mText, mqe); //2007Feb07

                        break; //throw makes it unreachable
                    }//catch

                    //}//if

                    CloseQueue(); //release the queue handle - prevent error 2017

                    //This loop is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    break;
                }//for
                return ret;
            }//lock
        }//MQPut

        /// <summary>
        /// Open an MQQueue, Push a message, close the queue
        /// </summary>
        /// <param name="sPutMsg">string message that you are sending</param>
        /// <param name="correlationId">byte[] correlationId (GUID) that you are sending out</param>
        /// <returns>message</returns>
        public bool MQPut(string sPutMsg, byte[] correlationId) //2006Oct03 dtn9708, 2006Dec28 dtn9708

        //public bool MQPut(string sPutMsg) //2006Oct03 dtn9708
        {
            //2007Feb07
            if (sPutMsg.Length < 1) //2007Feb08
            {
                return false; //nothing to do
            }//if

            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                bool ret = true;

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //dtn9708 2007Jan16, 2007Jan18
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }//if

                //for (; ; ) //2007Feb06
                //2007Feb07 retry after lost connection
                for (int ii = 0; ii < 2; ii++)
                {
                    // Try to open the queue
                    try
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT; //get the sealed constant from the base
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING; //get the sealed constant from the base

                        //The out que can be a dynamic queue which comes in as the
                        // Msg.ReplyToQueueName along with the correlationID to match it up.
                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueOutName, //dtn9708 2006Sep27
                                        iOP +
                                        iFIQ);

                        //2007Feb07
                        //}
                        //catch (MQException mqe)
                        //{
                        //    // stop if failed
                        //    ret = false;
                        //    _errorType = mqErrorType.mqEXCEPTION;
                        //    _mqException = (Exception)mqe;

                        //    string mText = mqe.Message;
                        //    throw new Exception("MQ AccessQueue MQException: " + mText, mqe);
                        //}

                        //if (sPutMsg.Length > 0)
                        //{
                        // put the next message to the queue
                        _mqMsg = new MQMessage();

                        //The queue where we are expecting to find messages or
                        //the queue name supplied by the original message
                        //_mqMsg.ReplyToQueueName = _mqQueueOutName; //dtn9708 2006Dec18
                        _mqMsg.ReplyToQueueName = _mqQueueInName; //dtn9708 2006Dec18

                        //_mqMsg.WriteString(sPutMsg); //2006Nov16 move after CharacterSet

                        //_mqMsg.Format = MQC.MQFMT_STRING; //2006Oct18
                        _mqMsg.Format = _MQMessageFormat;
                        _mqMsg.CharacterSet = _MQMessageCharacterSet; //2006Oct18

                        _mqMsg.WriteString(sPutMsg); //2006Nov16

                        _mqPutMsgOpts = new MQPutMessageOptions();
                        if (correlationId == null) //2006Dec28 dtn9708
                        {
                            byte[] correlId = { 0, 0, 0 }; //actually 24 long //2006Dec28 dtn9708
                            correlationId = correlId; //2006Dec28 dtn9708
                        }//if
                        _mqMsg.CorrelationId = correlationId; //GUID that you are sending 2006Oct03 dtn9708, //2006Dec28 dtn9708

                        //try 2007Feb07
                        //{
                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }//try
                    catch (MQException mqe)
                    {
                        //2007Feb07
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg, byte[] correlationId) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //string mText = mqe.Message;
                        //throw new Exception("MQ Put MQException: " + mText, mqe); //2007Feb07

                        //return false; //2007Feb07
                        break; //2007Feb07
                    }//catch

                    //}

                    CloseQueue(); //release the queue handle - prevent error 2017

                    //This is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    break;
                }//for
                return ret;
            }//lock
        }//MQPut

        /// <summary>
        /// Open an MQQueue, Push a message, close the queue.
        /// This overload sends back the messageID as an out byte[]
        /// </summary>
        /// <param name="sPutMsg">string message body</param>
        /// <param name="messageId">out byte[] messageId of sent message</param>
        /// <returns></returns>
        public bool MQPut(string sPutMsg, out byte[] messageId) //2006Dec27 dtn9708, 2006Dec28
        {
            lock (this) //dtn9708 2006Dec29
            {
                byte[] correlId = { 0, 0, 0 }; //actually 24 long  //2007Feb07
                messageId = correlId; //2007Feb07
                bool ret = true; //2007Feb07

                //2007Feb07
                if (sPutMsg.Length < 1) //2007Feb08
                {
                    return false; //nothing to do
                }//if

                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                //bool ret = true; //2007Feb07

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //dtn9708 2007Jan16, 2007Jan18
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        //take care of the out parameter
                        //byte[] correlId = { 0, 0, 0 }; //actually 24 long  //2007Feb07
                        //messageId = correlId; //2007Feb07

                        return false;
                    }
                }//if

                //for (; ; ) //2007Feb06
                //2007Feb07 retry after lost connection
                for (int ii = 0; ii < 2; ii++)
                {
                    // Try to open the queue and put a message
                    try
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT; //get the sealed constant from the base
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING; //get the sealed constant from the base

                        //The out que can be a dynamic queue which comes in as the
                        // Msg.ReplyToQueueName along with the correlationID to match it up.
                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueOutName, //dtn9708 2006Sep27
                                        iOP +
                                        iFIQ);

                        //} //2007Feb07
                        //catch (MQException mqe)
                        //{
                        //    // stop if failed
                        //    ret = false;
                        //    _errorType = mqErrorType.mqEXCEPTION;
                        //    _mqException = (Exception)mqe;

                        //    string mText = mqe.Message;
                        //    throw new Exception("MQ AccessQueue MQException: " + mText, mqe);
                        //}

                        //2007Feb07
                        //if (sPutMsg.Length > 0)
                        //{
                        // put the next message to the queue
                        _mqMsg = new MQMessage();

                        //The queue where we are expecting to find messages or
                        //the queue name supplied by the original message
                        _mqMsg.ReplyToQueueName = _mqQueueInName;
                        _mqMsg.Format = "MQSTR   ";
                        _mqMsg.CharacterSet = _MQMessageCharacterSet;

                        _mqMsg.WriteString(sPutMsg);

                        _mqPutMsgOpts = new MQPutMessageOptions();

                        //try //2007Feb07
                        //{
                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }//try
                    catch (MQException mqe)
                    {
                        //2007Feb07
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg, out byte[] messageId) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //string mText = mqe.Message;
                        //throw new Exception("MQ Put MQException: " + mText, mqe); //2007Feb07

                        //return false; //2007Feb07
                        break; //2007Feb07
                    }//catch

                    //}//if

                    messageId = _mqMsg.MessageId; //dtn9708 2006Dec27, 2006Dec28
                    CloseQueue(); //release the queue handle - prevent error 2017

                    break;
                }//for

                return ret;
            }//lock
        }//MQPut out byte[] overload

        /// <summary>
        /// Open an MQQueue, Push a message, close the queue.
        /// This overload accepts the messageID, iFlag as parameters for NMS service
        /// </summary>
        /// <param name="sPutMsg">string message body</param>
        /// <param name="messageId">byte[] messageId of sent message</param>
        /// <param name="iFlag">flag for NMS service</param>
        /// <returns></returns>
        public bool MQPut(string sPutMsg, byte[] messageId, int iFlag)
        {
            lock (this)
            {
                bool ret = true;
                byte[] correlId = { 0, 0, 0 };

                // Make sure Message Id is valid
                if (messageId == null)
                {
                    messageId = correlId;
                }

                // Make sure the Message is valid
                if (sPutMsg.Length < 1)
                {
                    return false;
                }

                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                // Make sure that the Queue Manager is open.
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }

                for (int ii = 0; ii < 2; ii++)
                {
                    // Open the queue and put a message
                    try
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT;
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING;

                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueOutName,
                                        iOP +
                                        iFIQ);

                        // Put the next message to the queue
                        _mqMsg = new MQMessage();

                        _mqMsg.ReplyToQueueName = _mqQueueInName;
                        _mqMsg.Format = "MQSTR   ";
                        _mqMsg.CharacterSet = _MQMessageCharacterSet;

                        _mqMsg.WriteString(sPutMsg);

                        _mqPutMsgOpts = new MQPutMessageOptions();

                        // Assign MessageId thats generated by CAPT
                        _mqMsg.MessageId = messageId;

                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }
                    catch (MQException mqe)
                    {
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager();
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue;
                        }

                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;
                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg, byte[] messageId, int iFlag) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);
                        break;
                    }

                    CloseQueue(); //release the queue handle

                    break;
                }

                return ret;
            }
        }

        /// <summary>
        /// Put a message to the outbound MQ Queue
        /// </summary>
        /// <param name="sPutMsg">The string message</param>
        /// <returns></returns>
        public bool MQPut(string sPutMsg)
        {
            //2007Feb07
            if (sPutMsg.Length < 1) //2007Feb08
            {
                return false; //nothing to do
            }//if

            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                bool ret = true;

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //if (!_mqQMgr.IsConnected) //dtn9708 2007Jan16
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected)) //dtn9708 2007Jan16, 2007Jan18
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }//if

                //for (; ; ) //2007Feb06
                //2007Feb07 retry after lost connection
                for (int ii = 0; ii < 2; ii++)
                {
                    // Try to open the queue
                    try
                    {
                        int iOP = IBM.WMQ.MQC.MQOO_OUTPUT; //get the sealed constant from the base
                        int iFIQ = IBM.WMQ.MQC.MQOO_FAIL_IF_QUIESCING; //get the sealed constant from the base
                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueOutName, //dtn9708 2006Sep27
                                        iOP +
                                        iFIQ);

                        //2007Feb07
                        //}
                        //catch (MQException mqe)
                        //{
                        //    // stop if failed
                        //    ret = false;
                        //    _errorType = mqErrorType.mqEXCEPTION;
                        //    _mqException = (Exception)mqe;

                        //    string mText = mqe.Message;

                        //    throw new Exception("MQ AccessQueue MQException: " + mText, mqe);

                        //    //break; //throw makes it unreachable
                        //}

                        //if (sPutMsg.Length > 0)
                        //{
                        // put the next message to the queue
                        _mqMsg = new MQMessage();

                        //The queue where we are expecting to find messages or
                        //the queue name supplied by the original message

                        //_mqMsg.ReplyToQueueName = _mqQueueOutName; //dtn9708 2006Dec18, 19
                        _mqMsg.ReplyToQueueName = _mqQueueInName; //dtn9708 2006Dec18, 19

                        //_mqMsg.WriteString(sPutMsg); //2006Nov16
                        //_mqMsg.Format = MQC.MQFMT_STRING; //2006Oct18
                        _mqMsg.Format = _MQMessageFormat;

                        //_mqMsg.CharacterSet = 1208; //UTF-8
                        //_mqMsg.CharacterSet = 1200; //Unicode  //2006Oct18
                        _mqMsg.CharacterSet = _MQMessageCharacterSet; //2006Oct18

                        _mqMsg.WriteString(sPutMsg); //2006Nov16

                        _mqPutMsgOpts = new MQPutMessageOptions();

                        //try //2007Feb07
                        //{
                        _mqQueue.Put(_mqMsg, _mqPutMsgOpts);
                    }//try
                    catch (MQException mqe)
                    {
                        //2007Feb07
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        ret = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;
                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQPut(string sPutMsg) - Unable to MQPut " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //string mText = mqe.Message; //2007Feb07
                        //throw new Exception("MQ Put MQException: " + mText, mqe); //2007Feb07

                        break; //throw makes it unreachable
                    }//catch

                    //}//if

                    CloseQueue(); //release the queue handle - prevent error 2017
                    break;
                }//for  retry logic

                return ret;
            }//lock
        }//MQPut - 1 parameter version

        /// <summary>
        /// Pop messages from MQ and enqueue them to a .Net Queue accessed
        /// by property outQue. outQueue is a .Net Queue of objects.
        /// This type will be QObjReturned.QueueOfStrings.
        /// </summary>
        /// <returns></returns>
        public bool MQGet()
        {
            lock (this) //dtn9708 2006Dec29
            {
                bool bRet = false;
                bRet = MQGet(QObjReturned.QueueOfStrings); //strings - the default

                return bRet;
            }
        }//MQGet

        /// <summary>
        /// Pop messages from MQ and enqueue them to a .Net Queue accessed
        /// by property outQue. outQueue is a .Net Queue of objects.
        /// Use QObjReturned to set the object types of the queue returned.
        /// </summary>
        /// <returns>bool success</returns>
        //public bool MQGet(int iObjectTypeReturned)
        public bool MQGet(QObjReturned eQOR)
        {
            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                bool ret = true;

                //the queue might still be open from the previous browse
                if (_mqQueue != null && _mqQueue.IsOpen) //2007Jan30
                {
                    CloseQueue(); //release the queue handle
                }

                //check to make sure that the Queue Manager is open.
                //In other words, make sure the connection has not been broken
                //if (!_mqQMgr.IsConnected) //dtn9708 2007Jan16
                if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected)) //dtn9708 2007Jan16
                {
                    bool bRet = ConnectQueueManager();
                    if (!bRet)
                    {
                        return false;
                    }
                }//if

                //------------------
                //2007Feb07
                for (int ii = 0; ii < 2; ii++) //retry loop
                {
                    try
                    {
                        int iIP = MQC.MQOO_INPUT_AS_Q_DEF;
                        int iFIQ = MQC.MQOO_FAIL_IF_QUIESCING; //dtn9708 2007Jan16

                        //int iFIQ = MQC.MQOO_INQUIRE; //dtn9708 2007Jan16
                        //int iFIQ = MQC.MQOO_SET;
                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueInName, iIP + iFIQ); //iIP+iFIQ); dtn9708 2006Sep27
                    }
                    catch (MQException mqe)
                    {
                        // stop if failed
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQGet(QObjReturned eQOR) - Unable to MQGet AccessQueue " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //put in some retry logic for when the connection is broken
                        //with the MQ Manager ******
                        //MQRC_CONNECTION_BROKEN , mqrcText
                        //MQC.MQRC_CONNECTION_BROKEN
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            return false;
                        }//if (mqe.Reason
                        else
                        {
                            //2007Feb07
                            //string mText = mqe.Message;
                            //throw new Exception("MQ MQQueue MQException: " + mText, mqe); //2007Feb07

                            return false; //2007Feb07
                        }

                        //return false;
                    }

                    // Get messages from the message queue
                    // Loop until there is a failure - generally because the
                    // queue is empty.
                    //
                    bool isContinue = true;

                    while (isContinue)
                    {
                        MQGetMessageOptions mqGetMsgOpts;    // MQGetMessageOptions instance

                        _mqMsg = new MQMessage();
                        mqGetMsgOpts = new MQGetMessageOptions();
                        mqGetMsgOpts.WaitInterval = _mqWaitInterval;  //maximum milliseconds limit for waiting on the Get()
                        mqGetMsgOpts.Options = MQC.MQGMO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING; //dtn9708 2006Sep19

                        try
                        {
                            _mqQueue.Get(_mqMsg, mqGetMsgOpts); //Get a message

                            string strFormat = _mqMsg.Format.ToString();
                            if ((_mqMsg.Format.CompareTo("String A") == 0) || (_mqMsg.Format.CompareTo(MQC.MQFMT_NONE) == 0) ||
                                (_mqMsg.Format.CompareTo(MQC.MQFMT_STRING) == 0) || //2006Dec19 dtn9708
                                (_mqMsg.Format.CompareTo("MQSTR   ") == 0)) //added the above condition by Sherry Mathews on 12/18/2006
                            {
                                // prp4825 - fix for empty messages - PJ002133
                                string strTemp = String.Empty;
                                if (_mqMsg.MessageLength > 0)
                                    strTemp = _mqMsg.ReadString(_mqMsg.MessageLength);

                                switch (eQOR)
                                {
                                    case QObjReturned.QueueOfStrings:
                                        QueInMessage(strTemp); //enqueue the string message objects
                                        break;

                                    case QObjReturned.QueueOfMQMessageItems:
                                        byte[] messageId = _mqMsg.MessageId; //Message GUID 2006Oct03, 2006Dec28
                                        string sReplyToQueue = _mqMsg.ReplyToQueueName; //dtn9708 2006Dec18
                                        string sReplyToQueueManagerName = _mqMsg.ReplyToQueueManagerName; //dtn9708 2007Jan15
                                        byte[] correlationId = _mqMsg.CorrelationId; //dtn9708 2006Dec18

                                        //Following code gets putdatetime(i.e. datetime when MQ got the Message) from MQ in GMT Format
                                        try
                                        {
                                            dtPutDateTime = _mqMsg.PutDateTime;
                                            dtPutDateTime = ConvertGMTtoCST(dtPutDateTime); //Converting from GMT to CST
                                        }
                                        catch
                                        {
                                            dtPutDateTime = DateTime.Now;
                                        }

                                        //QueInMessage(strTemp, msgId); //enqueue the MQMessageItem objects
                                        //QueInMessage(strTemp, messageId, sReplyToQueue, correlationId); //dtn9708 2006Dec18, 2006Dec28, 2007Jan15
                                        QueInMessage(strTemp, messageId,
                                            sReplyToQueue, sReplyToQueueManagerName,
                                            correlationId, dtPutDateTime); //dtn9708 2006Dec18, 2006Dec28, 2007Jan15
                                        break;

                                    case QObjReturned.OneString: //2006Oct18
                                        _outString = strTemp;
                                        isContinue = false; //one string/message only
                                        break;

                                    default:
                                        _outString = strTemp;
                                        isContinue = false; //one string/message only
                                        break;
                                }
                            }
                            else
                            {
                                _errorType = mqErrorType.mqERROR;
                                _mqError = "Non-text message";
                                FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQGet(QObjReturned eQOR) - Unable to MQGet - Message Format error, We did not get a message format that we are currently processing", string.Empty);
                                Debug.Fail("Message Format error.",
                                    "We did not get a message format that we are currently processing");
                            }
                        }
                        catch (MQException mqe)
                        {
                            if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                                FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQGet(QObjReturned eQOR) - Unable to MQGet " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                            //Retry logic for when the connection is broken
                            //with the MQ Manager
                            if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                            {
                                //2007Feb07
                                CloseManager(); //it still thinks its open
                                bool bRet = ConnectQueueManager();
                                if (!bRet)
                                {
                                    return false;
                                }

                                continue; //try again

                                //return false; //2007Feb07
                            }//if (mqe.Reason

                            // report reason, if any
                            int iMQRC_NO_MSG_AVAILABLE = MQC.MQRC_NO_MSG_AVAILABLE; //test**
                            if (mqe.Reason == iMQRC_NO_MSG_AVAILABLE)
                            {
                                //for normal end
                                isContinue = false;
                            }
                            else
                            {
                                // report for other reasons

                                // treat truncated message as a failure
                                int iMQRC_TRUNCATED_MSG_FAILED = MQC.MQRC_TRUNCATED_MSG_FAILED; //test
                                if (mqe.Reason == iMQRC_TRUNCATED_MSG_FAILED)
                                {
                                    isContinue = false;
                                }
                                _errorType = mqErrorType.mqEXCEPTION;
                                _mqException = (Exception)mqe;
                                return false;
                            }
                        }//catch

                        Thread.SpinWait(2); //Give other processes a break dtn9708 2006Dec29
                    }//while - get all of the messages

                    //This is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    if (isContinue) //2007Feb07

                        //after a reconnect
                        continue; //try again
                    else

                        //normal path
                        break; //2007Feb07
                }//for  - retry  //2007Feb07

                //----------------

                CloseQueue(); //release the queue handle - prevent error 2017

                return ret;
            }//lock
        }//bool MQGet()

        //new 2007Jan16 dtn9708
        /// <summary>
        /// Browse the first message in an MQ queue, enqueue it to a .Net Queue accessed
        /// by property outQue. outQueue is a .Net Queue of objects.
        /// Use QObjReturned to set the object type of the queue returned.
        /// </summary>
        /// <returns>bool success</returns>
        public bool MQBrowseFirst(QObjReturned eQOR)
        {
            //Debug.Assert(false, "MQBrowseFirst"); //Test *****

            lock (this)
            {
                //dtn9708 2007Feb01 move inside the try
                //if (!_mqHasBeenInitialized)
                //{
                //    Initialize();
                //}

                ////the queue might still be open from the previous browse
                //if (_mqQueue != null && _mqQueue.IsOpen) //2007Jan29
                //{
                //    CloseQueue(); //release the queue handle
                //}

                bool ret = true;

                ////check to make sure that the Queue Manager is open.
                ////In other words, make sure the connection has not been broken
                //if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                //{
                //    bool bRet = ConnectQueueManager();
                //    if (!bRet)
                //    {
                //        return false;
                //    }
                //}//if

                //we are using a loop so that we can do retry logic when
                //the MQ connection is lost and we do a retry. Try 2 times.
                //The connect is already trying 3 times.
                //bool isContinue = true;
                //while (isContinue)
                for (int ii = 0; ii < 2; ii++)
                {
                    try
                    {
                        //dtn9708 2007Feb01
                        if (!_mqHasBeenInitialized)
                        {
                            Initialize();
                        }

                        //the queue might still be open from the previous browse
                        if (_mqQueue != null && _mqQueue.IsOpen) //2007Jan29
                        {
                            CloseQueue(); //release the queue handle
                        }

                        //bool ret = true;

                        //check to make sure that the Queue Manager is open.
                        //In other words, make sure the connection has not been broken
                        if (_mqQMgr == null || (_mqQMgr != null && !_mqQMgr.IsConnected))
                        {
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }
                        }//if

                        //open for browse
                        int iBR = MQC.MQOO_BROWSE;  //dtn9708 2007Jan26
                        int iIP = MQC.MQOO_INPUT_AS_Q_DEF;
                        int iFIQ = MQC.MQOO_FAIL_IF_QUIESCING; //dtn9708 2007Jan16

                        //int iFIQ = MQC.MQOO_INQUIRE; //dtn9708 2007Jan16
                        int iSet = MQC.MQOO_SET;
                        _mqQueue = _mqQMgr.AccessQueue(_mqQueueInName,
                            iIP | iFIQ | iBR | iSet); //2007Jan29, 30
                    }
                    catch (MQException mqe)
                    {
                        if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQBrowseFirst(QObjReturned eQOR) - Unable to MQBrowseFirst AccessQueue " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        // stop if failed
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;

                        //Retry logic for when the connection is broken
                        //with the MQ Manager **
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false;
                        }//if (mqe.Reason
                        else
                        {
                            //2007Feb07
                            //string mText = mqe.Message;
                            //throw new Exception("MQ MQQueue MQException: " + mText, mqe); //2007Feb07

                            return false; //2007Feb07
                        }

                        //return false;
                    }

                    // Get messages from the message queue
                    // Loop until there is a failure - generally because the
                    // queue is empty.
                    //
                    //bool isContinue = true;
                    //while (isContinue)
                    //{
                    MQGetMessageOptions mqGetMsgOpts;    // MQGetMessageOptions instance

                    _mqMsg = new MQMessage();
                    mqGetMsgOpts = new MQGetMessageOptions();
                    mqGetMsgOpts.WaitInterval = _mqWaitInterval;  //maximum milliseconds limit for waiting on the Get()
                    mqGetMsgOpts.Options =
                        MQC.MQGMO_WAIT |
                        MQC.MQGMO_FAIL_IF_QUIESCING |
                        MQC.MQGMO_BROWSE_FIRST;  // |              //; // |

                    //MQC.MQGMO_LOCK; //2007Jan29, 30, Feb02

                    try
                    {
                        _mqQueue.Get(_mqMsg, mqGetMsgOpts); //Get a message

                        string strFormat = _mqMsg.Format.ToString();
                        if ((_mqMsg.Format.CompareTo("String A") == 0) || (_mqMsg.Format.CompareTo(MQC.MQFMT_NONE) == 0) ||
                            (_mqMsg.Format.CompareTo(MQC.MQFMT_STRING) == 0) || //2006Dec19 dtn9708
                            (_mqMsg.Format.CompareTo("MQSTR   ") == 0)) //added the above condition by Sherry Mathews on 12/18/2006
                        {
                            // prp4825 - fix for empty messages - PJ002133
                            string strTemp = String.Empty;
                            if (_mqMsg.MessageLength > 0)
                                strTemp = _mqMsg.ReadString(_mqMsg.MessageLength);

                            switch (eQOR)
                            {
                                case QObjReturned.QueueOfStrings:
                                    QueInMessage(strTemp); //enqueue the string message objects
                                    break;

                                case QObjReturned.QueueOfMQMessageItems:
                                    byte[] messageId = _mqMsg.MessageId; //Message GUID 2006Oct03, 2006Dec28
                                    string sReplyToQueue = _mqMsg.ReplyToQueueName; //dtn9708 2006Dec18
                                    string sReplyToQueueManagerName = _mqMsg.ReplyToQueueManagerName; //dtn9708 2007Jan15
                                    byte[] correlationId = _mqMsg.CorrelationId; //dtn9708 2006Dec18

                                    //Following code gets putdatetime(i.e. datetime when MQ got the Message) from MQ in GMT Format
                                    try
                                    {
                                        dtPutDateTime = _mqMsg.PutDateTime;
                                        dtPutDateTime = ConvertGMTtoCST(dtPutDateTime);//Converting from GMT to CST
                                    }
                                    catch
                                    {
                                        dtPutDateTime = DateTime.Now;
                                    }

                                    QueInMessage(strTemp, messageId,
                                        sReplyToQueue, sReplyToQueueManagerName,
                                        correlationId, dtPutDateTime); //dtn9708 2006Dec18, 2006Dec28, 2007Jan15
                                    break;

                                case QObjReturned.OneString: //2006Oct18
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;

                                default:
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;
                            }
                        }
                        else
                        {
                            _errorType = mqErrorType.mqERROR;
                            _mqError = "Non-text message";
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQBrowseFirst(QObjReturned eQOR) - Unable to MQBrowseFirst - Message Format error, We did not get a message format that we are currently processing", string.Empty);
                            Debug.Fail("Message Format error.",
                                "We did not get a message format that we are currently processing");
                        }
                    }
                    catch (MQException mqe)
                    {
                        if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQBrowseFirst(QObjReturned eQOR) - Unable to MQBrowseFirst " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //We need to put in some retry logic for when the connection is broken
                        //with the MQ Manager *****
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            //_mqQMgr.Disconnect(); //it still thinks its open
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false;
                        }//if (mqe.Reason

                        // report reason, if any
                        int iMQRC_NO_MSG_AVAILABLE = MQC.MQRC_NO_MSG_AVAILABLE; //test**
                        if (mqe.Reason == iMQRC_NO_MSG_AVAILABLE)
                        {
                            //for normal end
                            //isContinue = false;
                            break; //break the for loop
                        }
                        else
                        {
                            // report for other reasons

                            // treat truncated message as a failure
                            int iMQRC_TRUNCATED_MSG_FAILED = MQC.MQRC_TRUNCATED_MSG_FAILED; //test
                            if (mqe.Reason == iMQRC_TRUNCATED_MSG_FAILED)
                            {
                                //isContinue = false;
                                break; //break the for loop
                            }
                            _errorType = mqErrorType.mqEXCEPTION;
                            _mqException = (Exception)mqe;
                            return false;
                        }
                    }//catch

                    //Thread.SpinWait(2); //Give other processes a break dtn9708 2006Dec29

                    //This is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    break;
                }//for

                //Don't close the queue as this would close the browse cursor
                //CloseQueue(); //release the queue handle - prevent error 2017

                return ret;
            }//lock
        }//bool MQBrowseFirst()

        //new 2007Jan30 dtn9708
        /// <summary>
        /// Browse the next message in an MQ queue, enqueue it to a .Net Queue accessed
        /// by property outQue. outQueue is a .Net Queue of objects.
        /// Use QObjReturned to set the object type of the queue returned.
        /// </summary>
        /// <returns>bool success</returns>
        public bool MQBrowseNext(QObjReturned eQOR)
        {
            //Debug.Assert(false, "MQBrowseNext"); //Test *****

            lock (this)
            {
                //the queue must still be open from the previous browse
                //because the cursor will be used for position.
                //If the Queue Manager is closed, this will also be
                //closed.
                if (_mqQueue == null || !_mqQueue.IsOpen) //2007Jan30
                {
                    return false;
                }

                bool ret = true;

                //bool isContinue = true;
                //while (isContinue)
                //{
                //------------------

                //2007Feb07
                for (int ii = 0; ii < 2; ii++)
                {
                    try //2007Feb07
                    {
                        MQGetMessageOptions mqGetMsgOpts;    // MQGetMessageOptions instance

                        _mqMsg = new MQMessage();
                        mqGetMsgOpts = new MQGetMessageOptions();
                        mqGetMsgOpts.WaitInterval = _mqWaitInterval;  //maximum milliseconds limit for waiting on the Get()
                        mqGetMsgOpts.Options =
                            MQC.MQGMO_WAIT |
                            MQC.MQGMO_FAIL_IF_QUIESCING |
                            MQC.MQGMO_BROWSE_NEXT; // | 2007Feb07

                        //MQC.MQGMO_LOCK; //2007Jan30 | 2007Feb07

                        //try //2007Feb07
                        //{
                        _mqQueue.Get(_mqMsg, mqGetMsgOpts); //Get a message

                        string strFormat = _mqMsg.Format.ToString();
                        if ((_mqMsg.Format.CompareTo("String A") == 0) || (_mqMsg.Format.CompareTo(MQC.MQFMT_NONE) == 0) ||
                            (_mqMsg.Format.CompareTo(MQC.MQFMT_STRING) == 0) || //2006Dec19 dtn9708
                            (_mqMsg.Format.CompareTo("MQSTR   ") == 0)) //added the above condition by Sherry Mathews on 12/18/2006
                        {
                            // prp4825 - fix for empty messages - PJ002133
                            string strTemp = String.Empty;
                            if (_mqMsg.MessageLength > 0)
                                strTemp = _mqMsg.ReadString(_mqMsg.MessageLength);

                            switch (eQOR)
                            {
                                case QObjReturned.QueueOfStrings:
                                    QueInMessage(strTemp); //enqueue the string message objects
                                    break;

                                case QObjReturned.QueueOfMQMessageItems:
                                    byte[] messageId = _mqMsg.MessageId; //Message GUID 2006Oct03, 2006Dec28
                                    string sReplyToQueue = _mqMsg.ReplyToQueueName; //dtn9708 2006Dec18
                                    string sReplyToQueueManagerName = _mqMsg.ReplyToQueueManagerName; //dtn9708 2007Jan15
                                    byte[] correlationId = _mqMsg.CorrelationId; //dtn9708 2006Dec18

                                    //Following code gets putdatetime(i.e. datetime when MQ got the Message) from MQ in GMT Format
                                    try
                                    {
                                        dtPutDateTime = _mqMsg.PutDateTime;
                                        dtPutDateTime = ConvertGMTtoCST(dtPutDateTime); //Converting from GMT to CST
                                    }
                                    catch
                                    {
                                        dtPutDateTime = DateTime.Now;
                                    }

                                    QueInMessage(strTemp, messageId,
                                        sReplyToQueue, sReplyToQueueManagerName,
                                        correlationId, dtPutDateTime); //dtn9708 2006Dec18, 2006Dec28, 2007Jan15
                                    break;

                                case QObjReturned.OneString: //2006Oct18
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;

                                default:
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;
                            }
                        }
                        else
                        {
                            _errorType = mqErrorType.mqERROR;
                            _mqError = "Non-text message";
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQBrowseNext(QObjReturned eQOR) - Unable to MQBrowseNext - Message Format error, We did not get a message format that we are currently processing", string.Empty);
                            Debug.Fail("Message Format error.",
                                "We did not get a message format that we are currently processing");
                        }
                    }
                    catch (MQException mqe)
                    {
                        if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQBrowseNext(QObjReturned eQOR) - Unable to MQBrowseNext " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //Retry logic for when the connection is broken
                        //with the MQ Manager
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            //2007Feb07
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        // report reason, if any
                        int iMQRC_NO_MSG_AVAILABLE = MQC.MQRC_NO_MSG_AVAILABLE; //test**
                        if (mqe.Reason == iMQRC_NO_MSG_AVAILABLE)
                        {
                            //for normal end
                            //isContinue = false;
                        }
                        else
                        {
                            // report for other reasons

                            // treat truncated message as a failure
                            int iMQRC_TRUNCATED_MSG_FAILED = MQC.MQRC_TRUNCATED_MSG_FAILED; //test
                            if (mqe.Reason == iMQRC_TRUNCATED_MSG_FAILED)
                            {
                                //isContinue = false;
                            }
                            _errorType = mqErrorType.mqEXCEPTION;
                            _mqException = (Exception)mqe;
                            return false;
                        }
                    }//catch

                    //Thread.SpinWait(2); //Give other processes a break dtn9708 2006Dec29

                    //This is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    break; //2007Feb07

                    //}//while
                }//for  - retry

                //----------------

                //Don't close the queue as this would close the browse cursor
                //CloseQueue(); //release the queue handle - prevent error 2017

                return ret;
            }//lock
        }//bool MQBrowseNext()

        //new 2007Jan26 dtn9708
        /// <summary>
        /// Get the message after it has been browsed.
        ///
        /// outQueue is a .Net Queue of objects.
        /// Use QObjReturned to set the object type of the queue returned.
        /// </summary>
        /// <returns>bool success</returns>
        public bool MQGetAfterBrowseFirst(QObjReturned eQOR)
        {
            //Debug.Assert(false, "MQGetAfterBrowseFirst"); //Test *********

            lock (this)
            {
                //if the queue isn't open, we won't be able to do a
                //MQGMO_MSG_UNDER_CURSOR. Otherwise, we need to do get using
                //messageID
                if (!_mqQueue.IsOpen)
                {
                    return false;
                }

                bool ret = true;

                // Get messages from the message queue
                //
                //bool isContinue = true;
                //while (isContinue)
                //{
                //-------------
                //2007Feb07
                for (int ii = 0; ii < 2; ii++)
                {
                    try //2007Feb07
                    {
                        MQGetMessageOptions mqGetMsgOpts;    // MQGetMessageOptions instance

                        _mqMsg = new MQMessage();
                        mqGetMsgOpts = new MQGetMessageOptions();
                        mqGetMsgOpts.WaitInterval = _mqWaitInterval;  //maximum milliseconds limit for waiting on the Get()

                        mqGetMsgOpts.Options =
                            MQC.MQGMO_WAIT |
                            MQC.MQGMO_FAIL_IF_QUIESCING |
                            MQC.MQGMO_MSG_UNDER_CURSOR; //|

                        //MQC.MQGMO_UNLOCK; //unlock doesn't seem to work

                        //try //2007Feb07
                        //{
                        _mqQueue.Get(_mqMsg, mqGetMsgOpts); //Get a message

                        string strFormat = _mqMsg.Format.ToString();
                        if ((_mqMsg.Format.CompareTo("String A") == 0) || (_mqMsg.Format.CompareTo(MQC.MQFMT_NONE) == 0) ||
                            (_mqMsg.Format.CompareTo(MQC.MQFMT_STRING) == 0) || //2006Dec19 dtn9708
                            (_mqMsg.Format.CompareTo("MQSTR   ") == 0)) //added the above condition by Sherry Mathews on 12/18/2006
                        {
                            // prp4825 - fix for empty messages - PJ002133
                            string strTemp = String.Empty;
                            if (_mqMsg.MessageLength > 0)
                                strTemp = _mqMsg.ReadString(_mqMsg.MessageLength);

                            switch (eQOR)
                            {
                                case QObjReturned.QueueOfStrings:
                                    QueInMessage(strTemp); //enqueue the string message objects
                                    break;

                                case QObjReturned.QueueOfMQMessageItems:
                                    byte[] messageId = _mqMsg.MessageId; //Message GUID 2006Oct03, 2006Dec28
                                    string sReplyToQueue = _mqMsg.ReplyToQueueName; //dtn9708 2006Dec18

                                    //Following code gets putdatetime(i.e. datetime when MQ got the Message) from MQ in GMT Format
                                    try
                                    {
                                        dtPutDateTime = _mqMsg.PutDateTime;
                                        dtPutDateTime = ConvertGMTtoCST(dtPutDateTime); //Converting from GMT to CST
                                    }
                                    catch
                                    {
                                        dtPutDateTime = DateTime.Now;
                                    }

                                    string sReplyToQueueManagerName = _mqMsg.ReplyToQueueManagerName; //dtn9708 2007Jan15
                                    byte[] correlationId = _mqMsg.CorrelationId; //dtn9708 2006Dec18
                                    QueInMessage(strTemp, messageId,
                                        sReplyToQueue, sReplyToQueueManagerName,
                                        correlationId, dtPutDateTime); //dtn9708 2006Dec18, 2006Dec28, 2007Jan15
                                    break;

                                case QObjReturned.OneString: //2006Oct18
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;

                                default:
                                    _outString = strTemp;

                                    //isContinue = false; //one string/message only
                                    break;
                            }
                        }
                        else
                        {
                            _errorType = mqErrorType.mqERROR;
                            _mqError = "Non-text message";
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQGetAfterBrowseFirst(QObjReturned eQOR) - Unable to MQGetAfterBrowseFirst - Message Format error, We did not get a message format that we are currently processing", string.Empty);
                            Debug.Fail("Message Format error.",
                                "We did not get a message format that we are currently processing");
                        }
                    }
                    catch (MQException mqe)
                    {
                        if (mqe.Reason != MQC.MQRC_NO_MSG_AVAILABLE)
                            FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - MQGetAfterBrowseFirst(QObjReturned eQOR) - Unable to MQGetAfterBrowseFirst " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //We need to put in some retry logic for when the connection is broken
                        //with the MQ Manager *****
                        if (mqe.Reason == MQC.MQRC_CONNECTION_BROKEN)
                        {
                            //2007Feb07
                            CloseManager(); //it still thinks its open
                            bool bRet = ConnectQueueManager();
                            if (!bRet)
                            {
                                return false;
                            }

                            continue; //try again

                            //return false; //2007Feb07
                        }//if (mqe.Reason

                        // report reason, if any
                        int iMQRC_NO_MSG_AVAILABLE = MQC.MQRC_NO_MSG_AVAILABLE; //test**
                        if (mqe.Reason == iMQRC_NO_MSG_AVAILABLE)
                        {
                            //for normal end
                            //isContinue = false;
                        }
                        else
                        {
                            // report for other reasons

                            // treat truncated message as a failure
                            int iMQRC_TRUNCATED_MSG_FAILED = MQC.MQRC_TRUNCATED_MSG_FAILED; //test
                            if (mqe.Reason == iMQRC_TRUNCATED_MSG_FAILED)
                            {
                                //isContinue = false;
                            }
                            _errorType = mqErrorType.mqEXCEPTION;
                            _mqException = (Exception)mqe;
                            return false;
                        }//else
                    }//catch

                    //Thread.SpinWait(2); //Give other processes a break dtn9708 2006Dec29

                    //This is only meant to occur a maximum of 2 times.
                    //Normally, this will only happen once.
                    break; //2007Feb07

                    //}//while
                }//for

                //-----------

                //CloseQueue(); //release the queue handle - prevent error 2017

                return ret;
            }//lock
        }//bool MQGetAfterBrowseFirst()

        /// <summary>
        /// enqueue the MQMessageItem messages for the client app
        /// </summary>
        /// <param name="message">string message to enqueue</param>
        /// <param name="messageId">byte[] messageID</param>
        /// <param name="replyToQueue">msg.ReplyToQueueName of incoming message</param>
        /// <param name="replyToQueueManagerName">msg.ReplyToQueueManagerName of incoming message</param>
        /// <param name="correlationId">byte[] correlationId</param>
        /// <param name="dtputDateTime">DateTime dtputDateTime</param>
        /// <returns>bool</returns>
        private bool QueInMessage(string message, byte[] messageId,
            string replyToQueue, string replyToQueueManagerName,
            byte[] correlationId, DateTime dtputDateTime) //2006Dec18 dtn9708, 2006Dec28 dtn9708, 2007Jan15

        //string replyToQueue, byte[] correlationId) //2006Dec18 dtn9708, 2006Dec28 dtn9708
        {
            lock (this) //dtn9708 2006Dec29
            {
                bool ret = true;
                MQMessageItem item = new MQMessageItem(); //2006Oct03 dtn9708
                item.message = message; //2006Oct03 dtn9708
                item.id = messageId; //2006Oct03 dtn9708 -- the obsolete message is OK for now
                item.messageId = messageId; //2006Dec28 dtn9708
                item.replyToQueue = replyToQueue; //2006Dec18 dtn9708
                item.replyToQueueManagerName = replyToQueueManagerName; //dtn9708 2007Jan15
                item.correlationId = correlationId; //2006Dec28 dtn9708
                item.dtPutDateTime = dtputDateTime;
                lock (_messageQue.SyncRoot)
                {
                    _messageQue.Enqueue(item); //item object 2006Oct03 dtn9708

                    // We now have a message in the queue.
                    // signal the manual reset event to unblock the thread
                    //hasMessageEvent.Set();
                }

                return ret;
            }//lock
        }//QueInMessage

        /// <summary>
        /// enqueue the string messages for the client app
        /// </summary>
        /// <param name="message">string message to enqueue</param>
        /// <returns>bool</returns>
        private bool QueInMessage(string message)
        {
            lock (this) //dtn9708 2006Dec29
            {
                bool ret = true;

                lock (_messageQue.SyncRoot)
                {
                    _messageQue.Enqueue(message); //2006Oct03 dtn9708

                    //_messageQue.Enqueue(item); //item object 2006Oct03 dtn9708
                    // We now have a message in the queue.
                    // signal the manual reset event to unblock the thread
                    //hasMessageEvent.Set();
                }

                return ret;
            }//lock
        }//QueInMessage

        /// <summary>
        /// Try to establish or re-establish the connection with the queue manager.
        /// The method will try 5 times with an increasing sleep after each try.
        /// </summary>
        /// <returns>bool Success or Failure</returns>
        public bool ConnectQueueManager()
        {
            lock (this) //dtn9708 2006Dec29
            {
                if (!_mqHasBeenInitialized)
                {
                    Initialize();
                }

                //already connected.
                if (_mqQMgr != null && _mqQMgr.IsConnected)
                    return true;

                bool bSuccess = false;
                int iRetryCount = 0;

                while (!bSuccess && iRetryCount < 5)
                {
                    try
                    {
                        if (_mqQMgr != null)
                        {
                            //cleanup(); //dtn9708 2007Jan16
                            CloseManager(); //dtn9708 2007Jan16

                            //move below to CloseManager()
                            //_mqQMgr = null;  //dtn9708 2007Jan16

                            //GC.Collect();  //dtn9708 2007Jan16
                            //GC.WaitForPendingFinalizers();  //dtn9708 2007Jan16
                        }

                        //_mqQMgr = new MQQueueManager(); //dtn9708 2007Jan16
                        //The above parameterless constructor works.
                        //I would expect the one below to be better.
                        //The queue manager parameter will be necessary to
                        //implement replyToQueue switching.
                        _mqQMgr = new MQQueueManager(_mqManagerName); //dtn9708 2007Jan16

                        _mqSyncPointAvailability = _mqQMgr.SyncPointAvailability;

                        if (_mqSyncPointAvailability == MQC.MQSP_AVAILABLE)
                        {
                            _mqSyncPointAvailable = true;
                        }

                        //bSuccess = true;
                        return true;
                    }
                    catch (MQException mqe)
                    {
                        // stop if failed
                        bSuccess = false;
                        _errorType = mqErrorType.mqEXCEPTION;
                        _mqException = (Exception)mqe;
                        FL.WriteLogFile(string.Empty, FL.Event.Error, FL.MsgType.error, -1, string.Empty, "MQClient - ConnectQueueManager() - Unable to ConnectQueueManager " + mqe.Message + " : " + mqe.StackTrace, string.Empty);

                        //string mText = mqe.Message;

                        //throw new Exception("MQ MQQueueManager MQException: " + mText, mqe);
                    }

                    iRetryCount++;

                    //still in the loop
                    //Thread.Sleep(TimeSpan.FromSeconds(5)); //haven't determined a good interval yet. dtn9708 2007Feb01
                    Thread.Sleep(TimeSpan.FromSeconds(60 * iRetryCount)); //dtn9708 2007Feb07
                }//while

                return bSuccess;
            }//lock
        }//OpenQueueManager

        /// <summary>
        /// Begin a SyncPoint on the queue Manager.
        /// This is only possible if mqSyncPointAvailable
        /// </summary>
        public void mqBegin()
        {
            lock (this) //dtn9708 2006Dec29
            {
                //if (_mqSyncPointAvailability == MQC.MQSP_AVAILABLE)
                if (_mqSyncPointAvailable)
                {
                    _mqQMgr.Begin();
                    _mqSyncPointInProgress = true;
                }
            }//lock
        }//Begin

        /// <summary>
        /// Commit the message transactions after Begin().
        /// This will unlock the messages and pop them from
        /// the queue(s). This is a commit on the queue manager.
        /// This is only possible if mqSyncPointAvailable and
        /// you have previously done a Begin().
        /// </summary>
        public void mqCommit()
        {
            //Debug.Assert(_mqSyncPointInProgress, "No SyncPoint In Progress",
            //    "You must have done a Begin(), before you can do a mqCommit().");

            lock (this) //dtn9708 2006Dec29
            {
                //if (_mqSyncPointAvailability == MQC.MQSP_AVAILABLE)
                if (_mqSyncPointAvailable)
                {
                    //Debug.Assert(_mqSyncPointInProgress, "No SyncPoint In Progress",
                    //    "You must have done a Begin(), before you can do an mqCommit().");

                    _mqQMgr.Commit();
                    _mqSyncPointInProgress = false;
                }
            }//lock
        }//Commit

        /// <summary>
        /// Backout of a syncpoint.
        /// This will restore the messages and unlock the messages in
        /// the queue(s). This is a Backout on the queue manager.
        /// This is only possible if mqSyncPointAvailable and
        /// you have previously done a Begin().
        /// Something like a Rollback.
        /// </summary>
        public void mqBackout()
        {
            //Debug.Assert(_mqSyncPointInProgress, "No SyncPoint In Progress",
            //"You must have done a Begin(), before you can do a mqBackout().");

            lock (this) //dtn9708 2006Dec29
            {
                //if (_mqSyncPointAvailability == MQC.MQSP_AVAILABLE)
                if (_mqSyncPointAvailable)
                {
                    _mqQMgr.Backout();
                    _mqSyncPointInProgress = false;
                }
            }//lock
        }//mqBackout

        /// <summary>
        /// This method closes the MQ Queue.
        /// If you don't use this after each put or get, you will get
        /// error 2017 - MQRC_HANDLE_NOT_AVAILABLE
        /// </summary>
        public void CloseQueue()
        {
            lock (this)
            {
                if (_mqQueue != null && _mqQueue.IsOpen)
                    _mqQueue.Close(); //When finished with the queue
            }//lock
        }//CloseQueue

        /////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Close the MQ connection.
        /// </summary>
        public void Close()
        {
            //cleanup(); //dtn9708 2007Jan15
            CloseManager(); //dtn9708 2007Jan15
        }//Close

        /// <summary>
        /// disconnect
        /// </summary>
        //[System.Obsolete("use CloseManager()")] //dtn9708 2007Jan15
        private void cleanup()
        {
            lock (this)
            {
                if (_mqQMgr != null && _mqQMgr.IsOpen)
                {
                    _mqQMgr.Disconnect();	// MQQueueManager instance
                    _mqQMgr.Close();
                }
            }//lock
        }//Cleanup

        /// <summary>
        /// disconnect the queue manager
        /// </summary>
        private void CloseManager() //dtn9708 2007Jan15
        {
            lock (this)
            {
                if (_mqQMgr != null && _mqQMgr.IsOpen)
                {
                    try //dtn9708 2007Feb01
                    {
                        _mqQMgr.Disconnect(); // MQQueueManager instance
                        _mqQMgr.Close();

                        _mqQMgr = null;  //dtn9708 2007Jan16
                    }
                    catch (Exception Ex) //dtn9708 2007Feb01
                    {
                        string sTmp = Ex.Message;

                        //Don't do anything. The MQ connection is probably down.
                    }
                    GC.Collect();  //dtn9708 2007Jan16

                    //GC.WaitForPendingFinalizers();  //dtn9708 2007Jan16
                }
                if (_mqQMgr != null) //dtn9708 2007Jan16
                {
                    _mqQMgr = null; //dtn9708 2007Jan16

                    GC.Collect();  //dtn9708 2007Jan16

                    //GC.WaitForPendingFinalizers();  //dtn9708 2007Jan16
                }
            }//lock
        }//CloseManager

        //This function will convert GMR datetime to CST datetime
        private DateTime ConvertGMTtoCST(DateTime DtMQPutDate)
        {
            if (DtMQPutDate != null)
            {
                TimeZone tz = TimeZone.CurrentTimeZone;
                TimeSpan ts = tz.GetUtcOffset(DtMQPutDate);
                DtMQPutDate = DtMQPutDate.AddHours(ts.Hours);
            }
            return DtMQPutDate;
        }
    }//class
}//namespace