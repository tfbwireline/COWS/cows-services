﻿namespace AppConstants
{
    public enum ECPTCustomerType
    {
        ManagedData = 1,
        ManagedService,
        ManagedVoice,
        UnmanagedE2E,
        SPS
    }

    public enum ECPTServiceTier
    {
        MDSStrategic = 1,
        MDSComplete,
        MDSSupport,
        WholesaleCarrier,
        WholesaleVar
    }

    public enum ECPTHostedService
    {
        WPaaS = 1,
        SCC
    }

    public enum ECPTServiceType
    {
        IpDefender = 1,
        MssCompucom,
        MssAuthentication,
        MssSprint,
        MssOther
    }

    public enum ECPTProvisionType
    {
        Spectrum = 1,
        Voyancy,
        QIP,
        TACACS,
        ODIE
    }

    public enum ECPTPrimsite
    {
        FMNS = 1,
        IAAS,
        MNSD,
        MNSW,
        MVSC
    }

    public enum ECPTSupportTier
    {
        Design = 1,
        Implementation,
        Monitor
    }

    public enum ECPTProvisionStatus
    {
        Pending = 301,
        Picked,
        Success,
        Errored
    }

    public enum ECPTStatus
    {
        Submitted = 305,
        Provisioned,
        Assigned,
        Deleted,
        Completed,
        ReturnedToSDE,
        Cancelled,
        ReviewedByPM
    }

    public enum ECPTTransportType
    {
        IPSprintLink = 1,
        GMPLS,
        PIP,
        SprintLinkFR,
        IPSecVPN,
        GMPLSBroadband,
        WirelessWANWDLS
    }
}