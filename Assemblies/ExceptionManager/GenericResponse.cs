﻿using System;

namespace ExceptionManager
{
    public class GenericResponse<T>
    {
        private T myData;
        private bool myValid;
        private ErrorInfo myError;

        public GenericResponse(bool inValid)
        {
            myValid = inValid;
        }

        public GenericResponse()
        {
            // TODO: Complete member initialization
        }

        public T ReturnValue
        {
            get { return myData; }
            set { myData = value; }
        }

        public bool IsValid
        {
            get { return myValid; }
            set { myValid = value; }
        }

        public ErrorInfo Error
        {
            get
            {
                if (myError == null)
                    myError = new ErrorInfo(0, String.Empty);
                return myError;
            }
            set { myError = value; }
        }
    }
}