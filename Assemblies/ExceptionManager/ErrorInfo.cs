﻿namespace ExceptionManager
{
    public class ErrorInfo
    {
        private int errorNumber;
        private string errorMessage;
        private string userMessage;
        private string source;

        public ErrorInfo(int number, string message)
        {
            errorNumber = number;
            errorMessage = message;
        }

        public int ErrorNumber
        {
            get { return errorNumber; }
            set { errorNumber = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        public string UserMessage
        {
            get { return userMessage; }
            set { userMessage = value; }
        }

        public string Source
        {
            get { return source; }
            set { source = value; }
        }
    }
}