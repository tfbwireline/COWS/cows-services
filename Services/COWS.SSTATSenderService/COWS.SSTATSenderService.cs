﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using CL = LogManager.FileLogger;
using MQC = MQManager.MQClient;

namespace COWS.SSTATSenderService
{
    public partial class SSTATSenderService : ServiceBase
    {
        private string _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);
        private Thread _SenderThread = null;
        private MQC _mqcClient = null;
        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private ASCIIEncoding encoding = new ASCIIEncoding();

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new SSTATSenderService() };
            ServiceBase.Run(ServicesToRun);
        }

        public SSTATSenderService()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATSenderService is starting.", "");

                _bActive = true;
                _bStopThread = false;
                _bCanBeStoppedNow = true;
                _SenderThread = new Thread(new ThreadStart(this.SendRequests));
                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send SSTAT Requests)", string.Empty);
                _SenderThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.SSTATSenderService. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATSenderService", "COWS.SSTATSenderService Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                _bStopThread = true;
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.SSTATSenderService", string.Empty);

                _SenderThread.Join(TimeSpan.FromSeconds(30));
                if (!_bCanBeStoppedNow)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send SSTAT  Requests)", string.Empty);
                _SenderThread.Abort();
                _SenderThread.Join();

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.SSTATSenderService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.SSTATSenderService. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void SendRequests()
        {
            SSTATSenderIO _sIO = new SSTATSenderIO();
            try
            {
                _mqcClient = new MQC();
            }
            catch
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error connecting to the MQ Queue Manager", "Please check MQ Connection string in App Config.");
                return;
            }

            while (!_bStopThread)
            {
                if (_bActive)
                {
                    _sIO.GetSSTATRequests(ref _mqcClient);
                    Thread.Sleep(_iRequestSleepTime);
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                    return;
                }
            }

            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated ", "Please check.");
        }
    }
}