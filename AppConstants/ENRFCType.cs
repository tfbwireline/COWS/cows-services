﻿namespace AppConstants
{
    public enum ENRFCType
    {
        V21,
        V35,
        G703,
        RJ45,
        Other
    }
}