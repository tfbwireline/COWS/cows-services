﻿using DataManager;

namespace FSAHelper
{
    public class FSA_ORDR_CPE_LINE_ITEM_DB : MSSqlBase
    {
        #region "Fields And Properties"

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private string _SRVC_LINE_ITEM_CD;

        public string SRVC_LINE_ITEM_CD
        {
            get { return _SRVC_LINE_ITEM_CD; }
            set { _SRVC_LINE_ITEM_CD = value; }
        }

        private string _EQPT_TYPE_ID;

        public string EQPT_TYPE_ID
        {
            get { return _EQPT_TYPE_ID; }
            set { _EQPT_TYPE_ID = value; }
        }

        private string _EQPT_ID;

        public string EQPT_ID
        {
            get { return _EQPT_ID; }
            set { _EQPT_ID = value; }
        }

        private string _MFR_NME;

        public string MFR_NME
        {
            get { return _MFR_NME; }
            set { _MFR_NME = value; }
        }

        private string _CNTRC_TYPE_ID;

        public string CNTRC_TYPE_ID
        {
            get { return _CNTRC_TYPE_ID; }
            set { _CNTRC_TYPE_ID = value; }
        }

        private string _CNTRC_TERM_ID;

        public string CNTRC_TERM_ID
        {
            get { return _CNTRC_TERM_ID; }
            set { _CNTRC_TERM_ID = value; }
        }

        private string _INSTLN_CD;

        public string INSTLN_CD
        {
            get { return _INSTLN_CD; }
            set { _INSTLN_CD = value; }
        }

        private string _MNTC_CD;

        public string MNTC_CD
        {
            get { return _MNTC_CD; }
            set { _MNTC_CD = value; }
        }

        private string _DISC_PL_NBR;

        public string DISC_PL_NBR
        {
            get { return _DISC_PL_NBR; }
            set { _DISC_PL_NBR = value; }
        }

        private int _DISC_FMS_CKT_NBR;

        public int DISC_FMS_CKT_NBR
        {
            get { return _DISC_FMS_CKT_NBR; }
            set { _DISC_FMS_CKT_NBR = value; }
        }

        private string _DISC_NW_ADR;

        public string DISC_NW_ADR
        {
            get { return _DISC_NW_ADR; }
            set { _DISC_NW_ADR = value; }
        }

        private string _MDS_DES;

        public string MDS_DES
        {
            get { return _MDS_DES; }
            set { _MDS_DES = value; }
        }

        private int _CMPNT_ID;

        public int CMPNT_ID
        {
            get { return _CMPNT_ID; }
            set { _CMPNT_ID = value; }
        }

        private string _LINE_ITEM_CD;

        public string LINE_ITEM_CD
        {
            get { return _LINE_ITEM_CD; }
            set { _LINE_ITEM_CD = value; }
        }

        private int _FSA_ORGNL_INSTL_ORDR_ID;

        public int FSA_ORGNL_INSTL_ORDR_ID
        {
            get { return _FSA_ORGNL_INSTL_ORDR_ID; }
            set { _FSA_ORGNL_INSTL_ORDR_ID = value; }
        }

        private string _CXR_SRVC_ID;

        public string CXR_SRVC_ID
        {
            get { return _CXR_SRVC_ID; }
            set { _CXR_SRVC_ID = value; }
        }

        private string _CXR_CKT_ID;

        public string CXR_CKT_ID
        {
            get { return _CXR_CKT_ID; }
            set { _CXR_CKT_ID = value; }
        }

        private string _ACCS_TYPE_CD;

        public string ACCS_TYPE_CD
        {
            get { return _ACCS_TYPE_CD; }
            set { _ACCS_TYPE_CD = value; }
        }

        private string _ACCS_TYPE_DES;

        public string ACCS_TYPE_DES
        {
            get { return _ACCS_TYPE_DES; }
            set { _ACCS_TYPE_DES = value; }
        }

        private string _SPD_OF_SRVC;

        public string SPD_OF_SRVC
        {
            get { return _SPD_OF_SRVC; }
            set { _SPD_OF_SRVC = value; }
        }

        private int _VAS_ID;

        public int VAS_ID
        {
            get { return _VAS_ID; }
            set { _VAS_ID = value; }
        }

        #endregion "Fields And Properties"

        public FSA_ORDR_CPE_LINE_ITEM_DB(int _OrderID, string _LineItemCode)
        {
            ORDR_ID = _OrderID;
            LINE_ITEM_CD = _LineItemCode;
        }

        public int InsertFSALineItem(BILL_LINE_ITEM_DB _Bill, bool _HasBillItem)
        {
            setCommand("dbo.insertFSALineItem");
            addIntParam("@FSA_CPE_LINE_ITEM_ID", 0, true);
            addIntParam("@ORDR_ID", _ORDR_ID);
            addStringParam("@LINE_ITEM_CD", _LINE_ITEM_CD);

            addStringParam("@SRVC_LINE_ITEM_CD", _SRVC_LINE_ITEM_CD);
            addStringParam("@EQPT_TYPE_ID", _EQPT_TYPE_ID);
            addStringParam("@EQPT_ID", _EQPT_ID);
            addStringParam("@MFR_NME", _MFR_NME);
            addStringParam("@CNTRC_TYPE_ID", _CNTRC_TYPE_ID);
            addStringParam("@CNTRC_TERM_ID", _CNTRC_TERM_ID);
            addStringParam("@INSTLN_CD", _INSTLN_CD);
            addStringParam("@MNTC_CD", _MNTC_CD);
            addStringParam("@MDS_DES", _MDS_DES);
            addIntParam("@CMPNT_ID", _CMPNT_ID);
            addStringParam("@DISC_PL_NBR", _DISC_PL_NBR);
            addIntParam("@DISC_FMS_CKT_NBR", _DISC_FMS_CKT_NBR);
            addStringParam("@DISC_NW_ADR", _DISC_NW_ADR);

            addSmallIntParam("@BILL_ITEM_TYPE_ID", _Bill.BILL_ITEM_TYPE_ID);
            addStringParam("@LINE_ITEM_DES", _Bill.LINE_ITEM_DES);
            addSmallIntParam("@LINE_ITEM_QTY", _Bill.LINE_ITEM_QTY);
            addStringParam("@MRC_CHG_AMT", _Bill.MRC_CHG_AMT);
            addStringParam("@NRC_CHG_AMT", _Bill.NRC_CHG_AMT);
            addStringParam("@CHG_CD", _Bill.CHG_CD);
            addStringParam("@ACTN_CD", _Bill.ACTN_CD);
            addBooleanParam("@HasBillItem", _HasBillItem);

            addStringParam("@FSA_ORGNL_INSTL_ORDR_ID", _FSA_ORGNL_INSTL_ORDR_ID.ToString());
            addStringParam("@CXR_SRVC_ID", _CXR_SRVC_ID);
            addStringParam("@CXR_CKT_ID", _CXR_CKT_ID);
            addStringParam("@ACCS_TYPE_CD", _ACCS_TYPE_CD);
            addStringParam("@ACCS_TYPE_DES", _ACCS_TYPE_DES);
            addStringParam("@SPD_OF_SRVC", _SPD_OF_SRVC);

            if (_VAS_ID != 0)
                addIntParam("@VAS_ID", _VAS_ID);

            return execNoQueryOut("@FSA_CPE_LINE_ITEM_ID", true);
        }
    }
}