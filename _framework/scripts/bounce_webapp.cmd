@echo off
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM 
REM AVT framework script to bounce the IIS application pool for the @WEBSSITENAME@ webapp.
REM
REM Return codes:
REM   (pls see return codes for _recycle_app_pool.cmd)
REM
REM *******************************************************************************


REM
REM Start and then stop the IIS application pool for the webapp specified
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
CALL "%APP_HOME%\Scripts\_recycle_app_pool.cmd" @WEBSSITENAME@ STOP
IF NOT ERRORLEVEL 1 (
  CALL "%APP_HOME%\Scripts\_recycle_app_pool.cmd" @WEBSSITENAME@ START
)
