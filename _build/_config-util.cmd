@ECHO OFF
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to update and validate the application configuration. Calls 
REM the corresponding AVT framework targets to perform the required action.
REM
REM *******************************************************************************


REM
REM initialize some variables and do some validation
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
SET ACTION=%1
SET ENVNAME=%2
IF "%ACTION%" EQU "update" GOTO validated
IF "%ACTION%" EQU "validate" GOTO validated
ECHO ERROR: action argument must be 'update' or 'validate'
EXIT /B 2
:validated
IF "%ENVNAME%" EQU "." SET ENVNAME=
SET CFGFILE=%APP_HOME%\_framework\etc\config.properties
SET MASTERCFGFILE=%CFGFILE%.master
IF "%ENVNAME%" NEQ "" SET CFGFILE=%CFGFILE%.%ENVNAME%


REM
REM Call ant script
REM
SET ANT_HOME=%APP_HOME%\_framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\_framework\scripts\_admin.xml
"%ANT_PROG%" -f "%ANT_SCRIPT%" _config.%ACTION% -Dscripts.dir=scripts "-D_framework.dir=%APP_HOME%\_framework" "-Dcfgfile=%CFGFILE%" "-Dmaster.cfgfile=%MASTERCFGFILE%"
