﻿using AppConstants;
using System;
using System.Collections;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Xml;

//using fl = LogManager.FileLogger;

namespace UserManager
{
    [Serializable()]
    public class User
    {
        /////////////////////////////////////////////////////////////////////////

        #region "Data Members/Properties"

        private const string _affiliateDomain = "ssoaafl01";
        private const string _localDefaultMachine = "w4110484";
        private const string _localDefaultUser = "ad\\ssc3908";
        private const string _ldapURL = "LDAP://kcdirp02.corp.sprint.com/";
        private const string _rootPath = "LDAP://kcdirp02.corp.sprint.com/ou=People,o=Sprint,c=US";
        private const string _username = "cn=NMIS,ou=Machines,o=Sprint,c=US";

        private const string _password = "Green871";

        /////////////////////////////////////////////////////////////////////////////
        private Group _UserGrp = new Group();

        public Group UserGrp
        {
            get { return _UserGrp; }
            set { _UserGrp = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private Hashtable _GroupsAssigned;

        public Hashtable GroupsAssigned
        {
            get { return _GroupsAssigned; }
            set { _GroupsAssigned = value; }
        }

        private Hashtable _UserRoleHashTable;

        public Hashtable UserRoleHashTable
        {
            get { return _UserRoleHashTable; }
            set { _UserRoleHashTable = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserId;

        public string UserID
        {
            get { return _sUserId; }
            set { _sUserId = value; }
        }

        public int UserIDInt
        {
            get { try { return Int32.Parse(_sUserId); } catch { return 0; } }
            set { _sUserId = value.ToString(); }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserName;

        public string UserName
        {
            get { return _sUserName; }
            set { _sUserName = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sFirstName;

        public string FirstName
        {
            get { return _sFirstName; }
            set { _sFirstName = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sMiddleName;

        public string MiddleName
        {
            get { return _sMiddleName; }
            set { _sMiddleName = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sLastName;

        public string LastName
        {
            get { return _sLastName; }
            set { _sLastName = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sFullName;

        public string FullName
        {
            get { return _sFullName; }
            set { _sFullName = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserEmail;

        public string Email
        {
            get { return _sUserEmail; }
            set { _sUserEmail = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserPhone;

        public string Phone
        {
            get { return _sUserPhone; }
            set { _sUserPhone = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserCellPhone;

        public string CellPhone
        {
            get { return _sUserCellPhone; }
            set { _sUserCellPhone = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserPagerNumber;

        public string PagerNumber
        {
            get { return _sUserPagerNumber; }
            set { _sUserPagerNumber = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserPagerPin;

        public string PagerPin
        {
            get { return _sUserPagerPin; }
            set { _sUserPagerPin = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sUserDomain;

        public string Domain
        {
            get { return _sUserDomain; }
            set { _sUserDomain = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sAcf2Id;

        public string Acf2Id
        {
            get { return _sAcf2Id; }
            set { _sAcf2Id = value; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private bool _bIsValidUser = false;

        public bool IsValidUser
        {
            get { return _bIsValidUser; }
            set { _bIsValidUser = value; }
        }

        private string _UserStatus = "0";

        public string UserStatus
        {
            get { return _UserStatus; }
            set { _UserStatus = value; }
        }

        private string _ModifiedDT = string.Empty;

        public string ModifiedDT
        {
            get { return _ModifiedDT; }
            set { _ModifiedDT = value; }
        }

        private string _ModifiedBy = string.Empty;

        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }

        private string _sDispNme = string.Empty;

        public string DispNme
        {
            get { return _sDispNme; }
            set { _sDispNme = value; }
        }

        private string _sState = string.Empty;

        public string State
        {
            get { return _sState; }
            set { _sState = value; }
        }

        private string _sCity = string.Empty;

        public string City
        {
            get { return _sCity; }
            set { _sCity = value; }
        }

        private DataTable _dtUserCSG;

        public DataTable UserCSG
        {
            get { return _dtUserCSG; }
            set { _dtUserCSG = value; }
        }

        public bool isCSG2
        {
            get
            {
                foreach (DataRow r in _dtUserCSG.Rows)
                {
                    if (Convert.ToInt32(r["CSG_LVL_ID"]) <= 2)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public byte CSGLevelID
        {
            get
            {
                foreach (DataRow r in _dtUserCSG.Rows)
                {
                    if (Convert.ToInt32(r["CSG_LVL_ID"]) == 1)
                    {
                        return 1;
                    }
                    else if (Convert.ToInt32(r["CSG_LVL_ID"]) == 2)
                    {
                        return 2;
                    }
                    else if (Convert.ToInt32(r["CSG_LVL_ID"]) == 3)
                    {
                        return 3;
                    }
                    else if (Convert.ToInt32(r["CSG_LVL_ID"]) == 4)
                    {
                        return 4;
                    }
                }
                return 0;
            }
        }

        public bool isCSG1
        {
            get
            {
                foreach (DataRow r in _dtUserCSG.Rows)
                {
                    if (Convert.ToInt32(r["CSG_LVL_ID"]) == 1)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private bool _bIsSystemUser = false;

        public bool IsSystemUser
        {
            get { return _bIsSystemUser; }
            set { _bIsSystemUser = value; }
        }

        private bool _bIsRptUser = false;

        public bool IsRptUser
        {
            get
            {
                return (IsCannedRptUser || IsOnDemandRptUser || IsAdhocRptUser || UserGrp.bxNCIAm || UserGrp.bxNCIAsia || UserGrp.bxNCIEurope || UserGrp.bDCPE);
            }
            set { _bIsRptUser = value; }
        }

        private bool _bIsCannedRptUser = false;

        public bool IsCannedRptUser
        {
            get
            {
                var RptVal = (from x in UserRoleHashTable.Cast<DictionaryEntry>() where ((((string)x.Key).Contains("CannedRpt") && (((string)x.Value).Equals("1")))) select ((string)x.Value)).FirstOrDefault();

                if (RptVal == null)
                    return false;
                else if (RptVal.Equals(""))
                    return false;
                else if (RptVal.Equals("0"))
                    return false;
                else if (RptVal.Equals("1"))
                    return true;
                else return false;
            }
            set { _bIsCannedRptUser = value; }
        }

        private bool _bIsOnDemandRptUser = false;

        public bool IsOnDemandRptUser
        {
            get
            {
                var RptVal = (from x in UserRoleHashTable.Cast<DictionaryEntry>() where ((((string)x.Key).Contains("OnDemandRpt") && (((string)x.Value).Equals("1")))) select ((string)x.Value)).FirstOrDefault();

                if (RptVal == null)
                    return false;
                else if (RptVal.Equals(""))
                    return false;
                else if (RptVal.Equals("0"))
                    return false;
                else if (RptVal.Equals("1"))
                    return true;
                else return false;
            }
            set { _bIsOnDemandRptUser = value; }
        }

        private bool _bIsAdhocRptUser = false;

        public bool IsAdhocRptUser
        {
            get
            {
                var RptVal = (from x in UserRoleHashTable.Cast<DictionaryEntry>() where ((((string)x.Key).Contains("AdhocRpt") && (((string)x.Value).Equals("1")))) select ((string)x.Value)).FirstOrDefault();

                if (RptVal == null)
                    return false;
                else if (RptVal.Equals(""))
                    return false;
                else if (RptVal.Equals("0"))
                    return false;
                else if (RptVal.Equals("1"))
                    return true;
                else return false;
            }
            set { _bIsAdhocRptUser = value; }
        }

        #endregion "Data Members/Properties"

        /////////////////////////////////////////////////////////////////////////////

        #region "Constructors"

        public User(string sUserName, string sDomain, bool bIsAuthenticate)
        {
            InitializeUser(sUserName, "ad", bIsAuthenticate);
        }

        /////////////////////////////////////////////////////////////////////////////
        public User(int iUserId, bool bIsAuthenticate)
        {
            UserRoleHashTable = GetUserFromDB(iUserId);

            if (UserRoleHashTable.Count > 0)
            {
                InitializeUser(UserRoleHashTable["USERNAME"].ToString(), UserRoleHashTable["DOMAIN_NAME"].ToString(), bIsAuthenticate);
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public User()
        { }

        /////////////////////////////////////////////////////////////////////////////
        public User(string sUserId, string sUserName, string sAcf2Id, string sFullName, string sUserEmail, string sUserPhone, string sUserCellPhone, string sUserPagerNumber,
                    string sUserPagerPin, string sUserDomain, bool bIsAff, string sDispNme)
        {
            SetProperties(sUserId, sUserName, sFullName, sUserEmail, sUserPhone, sUserCellPhone, sUserPagerNumber, sUserPagerPin, sAcf2Id, sDispNme);
        }

        #endregion "Constructors"

        #region "General Operations"

        public int GetUserGroup()
        {
            //Assumption - One User would belong to only one User Group
            int groupID = 0;
            if (UserGrp.bGOM)
                groupID = (int)EGroups.GOM;
            else if (UserGrp.bMDS)
                groupID = (int)EGroups.MDS;
            else if (UserGrp.bCAND)
                groupID = (int)EGroups.CAND;
            else if (UserGrp.bSIPTnUCaaS)
                groupID = (int)EGroups.SIPTnUCaaS;
            else if (UserGrp.bCSC)
                groupID = (int)EGroups.CSC;
            else if (UserGrp.bSalesSupport)
                groupID = (int)EGroups.SalesSupport;
            else if (UserGrp.bxNCIAm)
                groupID = (int)EGroups.xNCIAmericas;
            else if (UserGrp.bxNCIAsia)
                groupID = (int)EGroups.xNCIAsia;
            else if (UserGrp.bxNCIEurope)
                groupID = (int)EGroups.xNCIEurope;
            else if (UserGrp.bDBB)
                groupID = (int)EGroups.DBB;
            else if (UserGrp.bCPETECH)
                groupID = (int)EGroups.CPETECH;
            return groupID;
        }

        public bool CheckUserOrderCSGLevel(byte _CSGLevel)
        {
            bool RetVal = true;

            if (_CSGLevel != 0)
            {
                if ((UserCSG == null) || (UserCSG.Rows.Count == 0))
                {
                    RetVal = false;
                }
                else
                {
                    string _Expr = "CSG_LVL_ID <> 0 and CSG_LVL_ID <= " + _CSGLevel;
                    DataRow[] DR = UserCSG.Select(_Expr);
                    if ((DR == null) || (DR.Length == 0))
                    {
                        RetVal = false;
                    }
                }
            }

            return RetVal;
        }

        public bool CheckUserWGAcess(int _WGID)
        {
            bool RetVal = false;

            switch (_WGID)
            {
                case (int)EGroups.GOM:
                    {
                        if (_UserGrp.GOM.OReviewer == "1" || _UserGrp.GOM.OUpdater == "1" || _UserGrp.GOM.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.MDS:
                    {
                        if (_UserGrp.MDS.OReviewer == "1" || _UserGrp.MDS.OUpdater == "1" || _UserGrp.MDS.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.SIPTnUCaaS:
                    {
                        if (_UserGrp.SIPTnUCaaS.ESReviewer == "1" || _UserGrp.SIPTnUCaaS.ESActivator == "1" || _UserGrp.SIPTnUCaaS.ESMember == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.CSC:
                    {
                        if (_UserGrp.CSC.OReviewer == "1" || _UserGrp.CSC.OUpdater == "1" || _UserGrp.CSC.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.xNCIAmericas:
                    {
                        if (_UserGrp.xNCIAm.OReviewer == "1" || _UserGrp.xNCIAm.OUpdater == "1" || _UserGrp.xNCIAm.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.xNCIAsia:
                    {
                        if (_UserGrp.xNCIAsia.OReviewer == "1" || _UserGrp.xNCIAsia.OUpdater == "1" || _UserGrp.xNCIAsia.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.xNCIEurope:
                    {
                        if (_UserGrp.xNCIEurope.OReviewer == "1" || _UserGrp.xNCIEurope.OUpdater == "1" || _UserGrp.xNCIEurope.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                case (int)EGroups.SalesSupport:
                    {
                        if (_UserGrp.SalesSupport.OReviewer == "1" || _UserGrp.SalesSupport.OUpdater == "1" || _UserGrp.SalesSupport.ORTS == "1")
                            RetVal = true;
                        break;
                    }

                case (int)EGroups.DBB:
                    {
                        if (_UserGrp.DBB.OReviewer == "1" || _UserGrp.DBB.OUpdater == "1" || _UserGrp.DBB.ORTS == "1")
                            RetVal = true;
                        break;
                    }
                default:
                    {
                        RetVal = true;
                        break;
                    }
            }
            return RetVal;
        }

        /////////////////////////////////////////////////////////////////////////////
        private void SetProperties(string sUserId, string sUserName, string sFullName, string sUserEmail, string sUserPhone, string sUserCellPhone, string sUserPagerNumber, string sUserPagerPin, string sAcf2Id, string sDispNme)
        {
            _sUserId = sUserId;
            _sUserName = sUserName;
            _sFullName = sFullName;
            _sUserEmail = sUserEmail;
            _sUserPhone = sUserPhone;
            _sUserCellPhone = sUserCellPhone;
            _sUserPagerNumber = sUserPagerNumber;
            _sUserPagerPin = sUserPagerPin;
            _sAcf2Id = sAcf2Id;
            _sDispNme = sDispNme;
        }

        /////////////////////////////////////////////////////////////////////////////
        private void SetProperties()
        {
            _UserGrp = new Group();
            _dtUserCSG = new DataTable();
            _GroupsAssigned = new Hashtable();
            /*_UserGrp.CAND = new Role();
            _UserGrp.CSC = new Role();
            _UserGrp.GOM = new Role();
            _UserGrp.MDS = new Role();
            _UserGrp.SalesEng = new Role();
            _UserGrp.SalesSupport = new Role();
            _UserGrp.WBO = new Role();
            _UserGrp.xNCIAm = new Role();
            _UserGrp.xNCIAsia = new Role();
            _UserGrp.xNCIEurope = new Role();*/

            _sUserName = UserRoleHashTable["UserName"].ToString();
            _sUserEmail = UserRoleHashTable["Email"].ToString();
            _sAcf2Id = UserRoleHashTable["Acf2Id"].ToString();
            _sUserPhone = UserRoleHashTable["Phone"].ToString();
            _sUserCellPhone = UserRoleHashTable["CellPhone"].ToString();
            _sUserPagerNumber = UserRoleHashTable["PagerNumber"].ToString();
            _sUserPagerPin = UserRoleHashTable["PagerPin"].ToString();
            _sUserId = UserRoleHashTable["WebUserID"].ToString();
            _sFullName = UserRoleHashTable["FullName"].ToString();
            _sDispNme = UserRoleHashTable["DispName"].ToString();
            _UserStatus = UserRoleHashTable["UserStatus"].ToString();
            _sState = UserRoleHashTable["State"].ToString();
            _sCity = UserRoleHashTable["City"].ToString();

            if (UserRoleHashTable["ReadOnly"] != null)
            {
                if (UserRoleHashTable["ReadOnly"].ToString().Equals("1"))
                    _UserGrp.bReadOnly = true;
                else
                    _UserGrp.bReadOnly = false;
            }
            else
                _UserGrp.bReadOnly = false;

            if (UserRoleHashTable["CANDEMember"] != null)
            {
                _UserGrp.CAND.ESMember = UserRoleHashTable["CANDEMember"].ToString();
                _UserGrp.CAND.ESReviewer = UserRoleHashTable["CANDEReviewer"].ToString();
                _UserGrp.CAND.ESActivator = UserRoleHashTable["CANDEActivator"].ToString();
                _UserGrp.CAND.OReviewer = UserRoleHashTable["CANDOReviewer"].ToString();
                _UserGrp.CAND.ORTS = UserRoleHashTable["CANDORTS"].ToString();
                _UserGrp.CAND.OUpdater = UserRoleHashTable["CANDOUpdater"].ToString();
                _UserGrp.CAND.Admin = UserRoleHashTable["CANDAdmin"].ToString();
                _UserGrp.CAND.WFMAssigner = UserRoleHashTable["CANDWFM"].ToString();
                _UserGrp.CAND.CannedRpt = UserRoleHashTable["CANDCannedRpt"].ToString();
                _UserGrp.CAND.OnDemandRpt = UserRoleHashTable["CANDOnDemandRpt"].ToString();
                _UserGrp.CAND.AdhocRpt = UserRoleHashTable["CANDAdhocRpt"].ToString();
                _UserGrp.CAND.SalesDesignEngineer = UserRoleHashTable["CANDSDE"].ToString();
                _UserGrp.CAND.NetworkTestEngineer = UserRoleHashTable["CANDNTE"].ToString();
                _UserGrp.CAND.SystemSecurityEngineer = UserRoleHashTable["CANDSSE"].ToString();

                if (UserRoleHashTable["CANDEMember"].ToString().Equals("1")
                    || UserRoleHashTable["CANDEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CANDEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["CANDOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CANDORTS"].ToString().Equals("1")
                    || UserRoleHashTable["CANDOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["CANDAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["CANDWFM"].ToString().Equals("1")
                    || UserRoleHashTable["CANDCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CANDOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CANDAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CANDSDE"].ToString().Equals("1")
                    || UserRoleHashTable["CANDNTE"].ToString().Equals("1")
                    || UserRoleHashTable["CANDSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bCAND = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.CAND), EGroups.CAND.ToString());
                }

                UserGrp.SIPTnUCaaS.ESMember = UserRoleHashTable["SIPTnUCaaSEMember"].ToString();
                _UserGrp.SIPTnUCaaS.ESReviewer = UserRoleHashTable["SIPTnUCaaSEReviewer"].ToString();
                _UserGrp.SIPTnUCaaS.ESActivator = UserRoleHashTable["SIPTnUCaaSEActivator"].ToString();
                _UserGrp.SIPTnUCaaS.OReviewer = UserRoleHashTable["SIPTnUCaaSOReviewer"].ToString();
                _UserGrp.SIPTnUCaaS.ORTS = UserRoleHashTable["SIPTnUCaaSORTS"].ToString();
                _UserGrp.SIPTnUCaaS.OUpdater = UserRoleHashTable["SIPTnUCaaSOUpdater"].ToString();
                _UserGrp.SIPTnUCaaS.Admin = UserRoleHashTable["SIPTnUCaaSAdmin"].ToString();
                _UserGrp.SIPTnUCaaS.WFMAssigner = UserRoleHashTable["SIPTnUCaaSWFM"].ToString();
                _UserGrp.SIPTnUCaaS.CannedRpt = UserRoleHashTable["SIPTnUCaaSCannedRpt"].ToString();
                _UserGrp.SIPTnUCaaS.OnDemandRpt = UserRoleHashTable["SIPTnUCaaSOnDemandRpt"].ToString();
                _UserGrp.SIPTnUCaaS.AdhocRpt = UserRoleHashTable["SIPTnUCaaSAdhocRpt"].ToString();
                _UserGrp.SIPTnUCaaS.SalesDesignEngineer = UserRoleHashTable["SIPTnUCaaSSDE"].ToString();
                _UserGrp.SIPTnUCaaS.NetworkTestEngineer = UserRoleHashTable["SIPTnUCaaSNTE"].ToString();
                _UserGrp.SIPTnUCaaS.SystemSecurityEngineer = UserRoleHashTable["SIPTnUCaaSSSE"].ToString();

                if (UserRoleHashTable["SIPTnUCaaSEMember"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSORTS"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSWFM"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSSDE"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSNTE"].ToString().Equals("1")
                    || UserRoleHashTable["SIPTnUCaaSSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bSIPTnUCaaS = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.SIPTnUCaaS), EGroups.SIPTnUCaaS.ToString());
                }

                _UserGrp.CSC.ESMember = UserRoleHashTable["CSCEMember"].ToString();
                _UserGrp.CSC.ESReviewer = UserRoleHashTable["CSCEReviewer"].ToString();
                _UserGrp.CSC.ESActivator = UserRoleHashTable["CSCEActivator"].ToString();
                _UserGrp.CSC.OReviewer = UserRoleHashTable["CSCOReviewer"].ToString();
                _UserGrp.CSC.ORTS = UserRoleHashTable["CSCORTS"].ToString();
                _UserGrp.CSC.OUpdater = UserRoleHashTable["CSCOUpdater"].ToString();
                _UserGrp.CSC.Admin = UserRoleHashTable["CSCAdmin"].ToString();
                _UserGrp.CSC.WFMAssigner = UserRoleHashTable["CSCWFM"].ToString();
                _UserGrp.CSC.CannedRpt = UserRoleHashTable["CSCCannedRpt"].ToString();
                _UserGrp.CSC.OnDemandRpt = UserRoleHashTable["CSCOnDemandRpt"].ToString();
                _UserGrp.CSC.AdhocRpt = UserRoleHashTable["CSCAdhocRpt"].ToString();
                _UserGrp.CSC.SalesDesignEngineer = UserRoleHashTable["CSCSDE"].ToString();
                _UserGrp.CSC.NetworkTestEngineer = UserRoleHashTable["CSCNTE"].ToString();
                _UserGrp.CSC.SystemSecurityEngineer = UserRoleHashTable["CSCSSE"].ToString();

                if (UserRoleHashTable["CSCEMember"].ToString().Equals("1")
                    || UserRoleHashTable["CSCEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CSCEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["CSCOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CSCORTS"].ToString().Equals("1")
                    || UserRoleHashTable["CSCOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["CSCAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["CSCWFM"].ToString().Equals("1")
                    || UserRoleHashTable["CSCCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CSCOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CSCAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CSCSDE"].ToString().Equals("1")
                    || UserRoleHashTable["CSCNTE"].ToString().Equals("1")
                    || UserRoleHashTable["CSCSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bCSC = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.CSC), EGroups.CSC.ToString());
                }

                _UserGrp.GOM.ESMember = UserRoleHashTable["GOMEMember"].ToString();
                _UserGrp.GOM.ESReviewer = UserRoleHashTable["GOMEReviewer"].ToString();
                _UserGrp.GOM.ESActivator = UserRoleHashTable["GOMEActivator"].ToString();
                _UserGrp.GOM.OReviewer = UserRoleHashTable["GOMOReviewer"].ToString();
                _UserGrp.GOM.ORTS = UserRoleHashTable["GOMORTS"].ToString();
                _UserGrp.GOM.OUpdater = UserRoleHashTable["GOMOUpdater"].ToString();
                _UserGrp.GOM.Admin = UserRoleHashTable["GOMAdmin"].ToString();
                _UserGrp.GOM.WFMAssigner = UserRoleHashTable["GOMWFM"].ToString();
                _UserGrp.GOM.CannedRpt = UserRoleHashTable["GOMCannedRpt"].ToString();
                _UserGrp.GOM.OnDemandRpt = UserRoleHashTable["GOMOnDemandRpt"].ToString();
                _UserGrp.GOM.AdhocRpt = UserRoleHashTable["GOMAdhocRpt"].ToString();
                _UserGrp.GOM.SalesDesignEngineer = UserRoleHashTable["GOMSDE"].ToString();
                _UserGrp.GOM.NetworkTestEngineer = UserRoleHashTable["GOMNTE"].ToString();
                _UserGrp.GOM.SystemSecurityEngineer = UserRoleHashTable["GOMSSE"].ToString();

                if (UserRoleHashTable["GOMEMember"].ToString().Equals("1")
                    || UserRoleHashTable["GOMEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["GOMEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["GOMOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["GOMORTS"].ToString().Equals("1")
                    || UserRoleHashTable["GOMOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["GOMAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["GOMWFM"].ToString().Equals("1")
                    || UserRoleHashTable["GOMCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["GOMOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["GOMAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["GOMSDE"].ToString().Equals("1")
                    || UserRoleHashTable["GOMNTE"].ToString().Equals("1")
                    || UserRoleHashTable["GOMSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bGOM = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.GOM), EGroups.GOM.ToString());
                }

                _UserGrp.MDS.ESMember = UserRoleHashTable["MDSEMember"].ToString();
                _UserGrp.MDS.ESReviewer = UserRoleHashTable["MDSEReviewer"].ToString();
                _UserGrp.MDS.ESActivator = UserRoleHashTable["MDSEActivator"].ToString();
                _UserGrp.MDS.OReviewer = UserRoleHashTable["MDSOReviewer"].ToString();
                _UserGrp.MDS.ORTS = UserRoleHashTable["MDSORTS"].ToString();
                _UserGrp.MDS.OUpdater = UserRoleHashTable["MDSOUpdater"].ToString();
                _UserGrp.MDS.Admin = UserRoleHashTable["MDSAdmin"].ToString();
                _UserGrp.MDS.WFMAssigner = UserRoleHashTable["MDSWFM"].ToString();
                _UserGrp.MDS.MNSActivator = UserRoleHashTable["MNSActivator"].ToString();
                _UserGrp.MDS.MNSPreConfigEng = UserRoleHashTable["MNSPreConfigEng"].ToString();
                _UserGrp.MDS.LCTPM = UserRoleHashTable["MDSLCTPM"].ToString();
                _UserGrp.MDS.MDSPM = UserRoleHashTable["MDSPM"].ToString();
                _UserGrp.MDS.CannedRpt = UserRoleHashTable["MDSCannedRpt"].ToString();
                _UserGrp.MDS.OnDemandRpt = UserRoleHashTable["MDSOnDemandRpt"].ToString();
                _UserGrp.MDS.AdhocRpt = UserRoleHashTable["MDSAdhocRpt"].ToString();
                _UserGrp.MDS.SalesDesignEngineer = UserRoleHashTable["MDSSDE"].ToString();
                _UserGrp.MDS.NetworkTestEngineer = UserRoleHashTable["MDSNTE"].ToString();
                _UserGrp.MDS.SystemSecurityEngineer = UserRoleHashTable["MDSSSE"].ToString();
                _UserGrp.MDS.NOS = UserRoleHashTable["MDSNOS"].ToString();
                _UserGrp.MDS.Gatekeeper = UserRoleHashTable["MDSGatekeeper"].ToString();
                _UserGrp.MDS.Manager = UserRoleHashTable["MDSManager"].ToString();

                if (UserRoleHashTable["MDSEMember"].ToString().Equals("1")
                    || UserRoleHashTable["MDSEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["MDSEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["MDSOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["MDSORTS"].ToString().Equals("1")
                    || UserRoleHashTable["MDSOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["MDSAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["MDSAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["MNSActivator"].ToString().Equals("1")
                    || UserRoleHashTable["MNSPreConfigEng"].ToString().Equals("1")
                    || UserRoleHashTable["MDSLCTPM"].ToString().Equals("1")
                    || UserRoleHashTable["MDSPM"].ToString().Equals("1")
                    || UserRoleHashTable["MDSCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["MDSOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["MDSAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["MDSSDE"].ToString().Equals("1")
                    || UserRoleHashTable["MDSNTE"].ToString().Equals("1")
                    || UserRoleHashTable["MDSSSE"].ToString().Equals("1")
                    || UserRoleHashTable["MDSNOS"].ToString().Equals("1")
                    || UserRoleHashTable["MDSGatekeeper"].ToString().Equals("1")
                    || UserRoleHashTable["MDSManager"].ToString().Equals("1"))
                {
                    _UserGrp.bMDS = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.MDS), EGroups.MDS.ToString());
                }

                _UserGrp.SalesSupport.ESMember = UserRoleHashTable["SalesSEMember"].ToString();
                _UserGrp.SalesSupport.ESReviewer = UserRoleHashTable["SalesSEReviewer"].ToString();
                _UserGrp.SalesSupport.ESActivator = UserRoleHashTable["SalesSEActivator"].ToString();
                _UserGrp.SalesSupport.OReviewer = UserRoleHashTable["SalesSOReviewer"].ToString();
                _UserGrp.SalesSupport.ORTS = UserRoleHashTable["SalesSORTS"].ToString();
                _UserGrp.SalesSupport.OUpdater = UserRoleHashTable["SalesSOUpdater"].ToString();
                _UserGrp.SalesSupport.Admin = UserRoleHashTable["SalesSAdmin"].ToString();
                _UserGrp.SalesSupport.WFMAssigner = UserRoleHashTable["SalesSWFM"].ToString();
                _UserGrp.SalesSupport.CannedRpt = UserRoleHashTable["SSCannedRpt"].ToString();
                _UserGrp.SalesSupport.OnDemandRpt = UserRoleHashTable["SSOnDemandRpt"].ToString();
                _UserGrp.SalesSupport.AdhocRpt = UserRoleHashTable["SSAdhocRpt"].ToString();
                _UserGrp.SalesSupport.SalesDesignEngineer = UserRoleHashTable["SSSDE"].ToString();
                _UserGrp.SalesSupport.NetworkTestEngineer = UserRoleHashTable["SSNTE"].ToString();
                _UserGrp.SalesSupport.SystemSecurityEngineer = UserRoleHashTable["SSSSE"].ToString();

                if (UserRoleHashTable["SalesSEMember"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSORTS"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["SalesSWFM"].ToString().Equals("1")
                    || UserRoleHashTable["SSCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SSOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SSAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["SSSDE"].ToString().Equals("1")
                    || UserRoleHashTable["SSNTE"].ToString().Equals("1")
                    || UserRoleHashTable["SSSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bSalesSupport = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.SalesSupport), EGroups.SalesSupport.ToString());
                }

                _UserGrp.WBO.ESMember = UserRoleHashTable["WBOEMember"].ToString();
                _UserGrp.WBO.ESReviewer = UserRoleHashTable["WBOEReviewer"].ToString();
                _UserGrp.WBO.ESActivator = UserRoleHashTable["WBOEActivator"].ToString();
                _UserGrp.WBO.OReviewer = UserRoleHashTable["WBOOReviewer"].ToString();
                _UserGrp.WBO.ORTS = UserRoleHashTable["WBOORTS"].ToString();
                _UserGrp.WBO.OUpdater = UserRoleHashTable["WBOOUpdater"].ToString();
                _UserGrp.WBO.Admin = UserRoleHashTable["WBOAdmin"].ToString();
                _UserGrp.WBO.WFMAssigner = UserRoleHashTable["WBOWFM"].ToString();
                _UserGrp.WBO.CannedRpt = UserRoleHashTable["WBOCannedRpt"].ToString();
                _UserGrp.WBO.OnDemandRpt = UserRoleHashTable["WBOOnDemandRpt"].ToString();
                _UserGrp.WBO.AdhocRpt = UserRoleHashTable["WBOAdhocRpt"].ToString();
                _UserGrp.WBO.SalesDesignEngineer = UserRoleHashTable["WBOSDE"].ToString();
                _UserGrp.WBO.NetworkTestEngineer = UserRoleHashTable["WBONTE"].ToString();
                _UserGrp.WBO.SystemSecurityEngineer = UserRoleHashTable["WBOSSE"].ToString();

                if (UserRoleHashTable["WBOEMember"].ToString().Equals("1")
                    || UserRoleHashTable["WBOEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["WBOEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["WBOOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["WBOORTS"].ToString().Equals("1")
                    || UserRoleHashTable["WBOOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["WBOAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["WBOWFM"].ToString().Equals("1")
                    || UserRoleHashTable["WBOCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["WBOOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["WBOAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["WBOSDE"].ToString().Equals("1")
                    || UserRoleHashTable["WBONTE"].ToString().Equals("1")
                    || UserRoleHashTable["WBOSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bWBO = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.WBO), EGroups.WBO.ToString());
                }

                _UserGrp.xNCIAm.ESMember = UserRoleHashTable["xNCIAmEMember"].ToString();
                _UserGrp.xNCIAm.ESReviewer = UserRoleHashTable["xNCIAmEReviewer"].ToString();
                _UserGrp.xNCIAm.ESActivator = UserRoleHashTable["xNCIAmEActivator"].ToString();
                _UserGrp.xNCIAm.OReviewer = UserRoleHashTable["xNCIAmOReviewer"].ToString();
                _UserGrp.xNCIAm.ORTS = UserRoleHashTable["xNCIAmORTS"].ToString();
                _UserGrp.xNCIAm.OUpdater = UserRoleHashTable["xNCIAmOUpdater"].ToString();
                _UserGrp.xNCIAm.Admin = UserRoleHashTable["xNCIAmAdmin"].ToString();
                _UserGrp.xNCIAm.WFMAssigner = UserRoleHashTable["xNCIAmWFM"].ToString();
                _UserGrp.xNCIAm.CannedRpt = UserRoleHashTable["xNCIAmCannedRpt"].ToString();
                _UserGrp.xNCIAm.OnDemandRpt = UserRoleHashTable["xNCIAmOnDemandRpt"].ToString();
                _UserGrp.xNCIAm.AdhocRpt = UserRoleHashTable["xNCIAmAdhocRpt"].ToString();
                _UserGrp.xNCIAm.SalesDesignEngineer = UserRoleHashTable["xNCIAmSDE"].ToString();
                _UserGrp.xNCIAm.NetworkTestEngineer = UserRoleHashTable["xNCIAmNTE"].ToString();
                _UserGrp.xNCIAm.SystemSecurityEngineer = UserRoleHashTable["xNCIAmSSE"].ToString();

                if (UserRoleHashTable["xNCIAmEMember"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmORTS"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmWFM"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmSDE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmNTE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAmSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bxNCIAm = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.xNCIAmericas), EGroups.xNCIAmericas.ToString());
                }

                _UserGrp.xNCIAsia.ESMember = UserRoleHashTable["xNCIAEMember"].ToString();
                _UserGrp.xNCIAsia.ESReviewer = UserRoleHashTable["xNCIAEReviewer"].ToString();
                _UserGrp.xNCIAsia.ESActivator = UserRoleHashTable["xNCIAEActivator"].ToString();
                _UserGrp.xNCIAsia.OReviewer = UserRoleHashTable["xNCIAOReviewer"].ToString();
                _UserGrp.xNCIAsia.ORTS = UserRoleHashTable["xNCIAORTS"].ToString();
                _UserGrp.xNCIAsia.OUpdater = UserRoleHashTable["xNCIAOUpdater"].ToString();
                _UserGrp.xNCIAsia.Admin = UserRoleHashTable["xNCIAAdmin"].ToString();
                _UserGrp.xNCIAsia.WFMAssigner = UserRoleHashTable["xNCIAWFM"].ToString();
                _UserGrp.xNCIAsia.CannedRpt = UserRoleHashTable["xNCIACannedRpt"].ToString();
                _UserGrp.xNCIAsia.OnDemandRpt = UserRoleHashTable["xNCIAOnDemandRpt"].ToString();
                _UserGrp.xNCIAsia.AdhocRpt = UserRoleHashTable["xNCIAAdhocRpt"].ToString();
                _UserGrp.xNCIAsia.SalesDesignEngineer = UserRoleHashTable["xNCIASDE"].ToString();
                _UserGrp.xNCIAsia.NetworkTestEngineer = UserRoleHashTable["xNCIANTE"].ToString();
                _UserGrp.xNCIAsia.SystemSecurityEngineer = UserRoleHashTable["xNCIASSE"].ToString();

                if (UserRoleHashTable["xNCIAEMember"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAORTS"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAWFM"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIACannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIAAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIASDE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIANTE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIASSE"].ToString().Equals("1"))
                {
                    _UserGrp.bxNCIAsia = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.xNCIAsia), EGroups.xNCIAsia.ToString());
                }

                _UserGrp.xNCIEurope.ESMember = UserRoleHashTable["xNCIEEMember"].ToString();
                _UserGrp.xNCIEurope.ESReviewer = UserRoleHashTable["xNCIEEReviewer"].ToString();
                _UserGrp.xNCIEurope.ESActivator = UserRoleHashTable["xNCIEEActivator"].ToString();
                _UserGrp.xNCIEurope.OReviewer = UserRoleHashTable["xNCIEOReviewer"].ToString();
                _UserGrp.xNCIEurope.ORTS = UserRoleHashTable["xNCIEORTS"].ToString();
                _UserGrp.xNCIEurope.OUpdater = UserRoleHashTable["xNCIEOUpdater"].ToString();
                _UserGrp.xNCIEurope.Admin = UserRoleHashTable["xNCIEAdmin"].ToString();
                _UserGrp.xNCIEurope.WFMAssigner = UserRoleHashTable["xNCIEWFM"].ToString();
                _UserGrp.xNCIEurope.CannedRpt = UserRoleHashTable["xNCIECannedRpt"].ToString();
                _UserGrp.xNCIEurope.OnDemandRpt = UserRoleHashTable["xNCIEOnDemandRpt"].ToString();
                _UserGrp.xNCIEurope.AdhocRpt = UserRoleHashTable["xNCIEAdhocRpt"].ToString();
                _UserGrp.xNCIEurope.NetworkTestEngineer = UserRoleHashTable["xNCIENTE"].ToString();
                _UserGrp.xNCIEurope.SalesDesignEngineer = UserRoleHashTable["xNCIESDE"].ToString();
                _UserGrp.xNCIEurope.SystemSecurityEngineer = UserRoleHashTable["xNCIESSE"].ToString();

                if (UserRoleHashTable["xNCIEEMember"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEORTS"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEWFM"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIECannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIEAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIESDE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIENTE"].ToString().Equals("1")
                    || UserRoleHashTable["xNCIESSE"].ToString().Equals("1"))
                {
                    _UserGrp.bxNCIEurope = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.xNCIEurope), EGroups.xNCIEurope.ToString());
                }

                _UserGrp.DCPE.ESMember = UserRoleHashTable["DCPEEMember"].ToString();
                _UserGrp.DCPE.ESReviewer = UserRoleHashTable["DCPEEReviewer"].ToString();
                _UserGrp.DCPE.ESActivator = UserRoleHashTable["DCPEEActivator"].ToString();
                _UserGrp.DCPE.OReviewer = UserRoleHashTable["DCPEOReviewer"].ToString();
                _UserGrp.DCPE.ORTS = UserRoleHashTable["DCPEORTS"].ToString();
                _UserGrp.DCPE.OUpdater = UserRoleHashTable["DCPEOUpdater"].ToString();
                _UserGrp.DCPE.Admin = UserRoleHashTable["DCPEAdmin"].ToString();
                _UserGrp.DCPE.WFMAssigner = UserRoleHashTable["DCPEWFM"].ToString();
                _UserGrp.DCPE.CannedRpt = UserRoleHashTable["DCPECannedRpt"].ToString();
                _UserGrp.DCPE.OnDemandRpt = UserRoleHashTable["DCPEOnDemandRpt"].ToString();
                _UserGrp.DCPE.AdhocRpt = UserRoleHashTable["DCPEAdhocRpt"].ToString();
                _UserGrp.DCPE.NetworkTestEngineer = UserRoleHashTable["DCPENTE"].ToString();
                _UserGrp.DCPE.SalesDesignEngineer = UserRoleHashTable["DCPESDE"].ToString();
                _UserGrp.DCPE.SystemSecurityEngineer = UserRoleHashTable["DCPESSE"].ToString();

                if (UserRoleHashTable["DCPEEMember"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEORTS"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEWFM"].ToString().Equals("1")
                    || UserRoleHashTable["DCPECannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DCPEAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DCPESDE"].ToString().Equals("1")
                    || UserRoleHashTable["DCPENTE"].ToString().Equals("1")
                    || UserRoleHashTable["DCPESSE"].ToString().Equals("1"))
                {
                    _UserGrp.bDCPE = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.DCPE), EGroups.DCPE.ToString());
                }

                //_UserGrp.VCPE.ESMember = UserRoleHashTable["VCPEEMember"].ToString();
                //_UserGrp.VCPE.ESReviewer = UserRoleHashTable["VCPEEReviewer"].ToString();
                //_UserGrp.VCPE.ESActivator = UserRoleHashTable["VCPEEActivator"].ToString();
                //_UserGrp.VCPE.OReviewer = UserRoleHashTable["VCPEOReviewer"].ToString();
                //_UserGrp.VCPE.ORTS = UserRoleHashTable["VCPEORTS"].ToString();
                //_UserGrp.VCPE.OUpdater = UserRoleHashTable["VCPEOUpdater"].ToString();
                //_UserGrp.VCPE.Admin = UserRoleHashTable["VCPEAdmin"].ToString();
                //_UserGrp.VCPE.WFMAssigner = UserRoleHashTable["VCPEWFM"].ToString();
                //_UserGrp.VCPE.CannedRpt = UserRoleHashTable["VCPECannedRpt"].ToString();
                //_UserGrp.VCPE.OnDemandRpt = UserRoleHashTable["VCPEOnDemandRpt"].ToString();
                //_UserGrp.VCPE.AdhocRpt = UserRoleHashTable["VCPEAdhocRpt"].ToString();
                //_UserGrp.VCPE.NetworkTestEngineer = UserRoleHashTable["VCPENTE"].ToString();
                //_UserGrp.VCPE.SalesDesignEngineer = UserRoleHashTable["VCPESDE"].ToString();
                //_UserGrp.VCPE.SystemSecurityEngineer = UserRoleHashTable["VCPESSE"].ToString();

                //if (UserRoleHashTable["VCPEEMember"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEEReviewer"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEEActivator"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEOReviewer"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEORTS"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEOUpdater"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEAdmin"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEWFM"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPECannedRpt"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEOnDemandRpt"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPEAdhocRpt"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPESDE"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPENTE"].ToString().Equals("1")
                //    || UserRoleHashTable["VCPESSE"].ToString().Equals("1"))
                //{
                //    _UserGrp.bVCPE = true;
                //    _GroupsAssigned.Add(Convert.ToByte(EGroups.VCPE), EGroups.VCPE.ToString());
                //}

                _UserGrp.CPETECH.ESMember = UserRoleHashTable["CPETECHEMember"].ToString();
                _UserGrp.CPETECH.ESReviewer = UserRoleHashTable["CPETECHEReviewer"].ToString();
                _UserGrp.CPETECH.ESActivator = UserRoleHashTable["CPETECHEActivator"].ToString();
                _UserGrp.CPETECH.OReviewer = UserRoleHashTable["CPETECHOReviewer"].ToString();
                _UserGrp.CPETECH.ORTS = UserRoleHashTable["CPETECHORTS"].ToString();
                _UserGrp.CPETECH.OUpdater = UserRoleHashTable["CPETECHOUpdater"].ToString();
                _UserGrp.CPETECH.Admin = UserRoleHashTable["CPETECHAdmin"].ToString();
                _UserGrp.CPETECH.WFMAssigner = UserRoleHashTable["CPETECHWFM"].ToString();
                _UserGrp.CPETECH.CannedRpt = UserRoleHashTable["CPETECHCannedRpt"].ToString();
                _UserGrp.CPETECH.OnDemandRpt = UserRoleHashTable["CPETECHOnDemandRpt"].ToString();
                _UserGrp.CPETECH.AdhocRpt = UserRoleHashTable["CPETECHAdhocRpt"].ToString();
                _UserGrp.CPETECH.NetworkTestEngineer = UserRoleHashTable["CPETECHNTE"].ToString();
                _UserGrp.CPETECH.SalesDesignEngineer = UserRoleHashTable["CPETECHSDE"].ToString();
                _UserGrp.CPETECH.SystemSecurityEngineer = UserRoleHashTable["CPETECHSSE"].ToString();

                if (UserRoleHashTable["CPETECHEMember"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHORTS"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHWFM"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHSDE"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHNTE"].ToString().Equals("1")
                    || UserRoleHashTable["CPETECHSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bCPETECH = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.CPETECH), EGroups.CPETECH.ToString());
                }

                _UserGrp.DBB.ESMember = UserRoleHashTable["DBBEMember"].ToString();
                _UserGrp.DBB.ESReviewer = UserRoleHashTable["DBBEReviewer"].ToString();
                _UserGrp.DBB.ESActivator = UserRoleHashTable["DBBEActivator"].ToString();
                _UserGrp.DBB.OReviewer = UserRoleHashTable["DBBOReviewer"].ToString();
                _UserGrp.DBB.ORTS = UserRoleHashTable["DBBORTS"].ToString();
                _UserGrp.DBB.OUpdater = UserRoleHashTable["DBBOUpdater"].ToString();
                _UserGrp.DBB.Admin = UserRoleHashTable["DBBAdmin"].ToString();
                _UserGrp.DBB.WFMAssigner = UserRoleHashTable["DBBWFM"].ToString();
                _UserGrp.DBB.CannedRpt = UserRoleHashTable["DBBCannedRpt"].ToString();
                _UserGrp.DBB.OnDemandRpt = UserRoleHashTable["DBBOnDemandRpt"].ToString();
                _UserGrp.DBB.AdhocRpt = UserRoleHashTable["DBBAdhocRpt"].ToString();
                _UserGrp.DBB.NetworkTestEngineer = UserRoleHashTable["DBBNTE"].ToString();
                _UserGrp.DBB.SalesDesignEngineer = UserRoleHashTable["DBBSDE"].ToString();
                _UserGrp.DBB.SystemSecurityEngineer = UserRoleHashTable["DBBSSE"].ToString();

                if (UserRoleHashTable["DBBEMember"].ToString().Equals("1")
                    || UserRoleHashTable["DBBEReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["DBBEActivator"].ToString().Equals("1")
                    || UserRoleHashTable["DBBOReviewer"].ToString().Equals("1")
                    || UserRoleHashTable["DBBORTS"].ToString().Equals("1")
                    || UserRoleHashTable["DBBOUpdater"].ToString().Equals("1")
                    || UserRoleHashTable["DBBAdmin"].ToString().Equals("1")
                    || UserRoleHashTable["DBBWFM"].ToString().Equals("1")
                    || UserRoleHashTable["DBBCannedRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DBBOnDemandRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DBBAdhocRpt"].ToString().Equals("1")
                    || UserRoleHashTable["DBBSDE"].ToString().Equals("1")
                    || UserRoleHashTable["DBBNTE"].ToString().Equals("1")
                    || UserRoleHashTable["DBBSSE"].ToString().Equals("1"))
                {
                    _UserGrp.bDBB = true;
                    _GroupsAssigned.Add(Convert.ToByte(EGroups.DBB), EGroups.DBB.ToString());
                }

                _bIsSystemUser = (UserRoleHashTable["IsSystemUser"]).ToString().Equals("1");
            }
            if (UserRoleHashTable["UserCSGTbl"] != null)
                _dtUserCSG = (DataTable)UserRoleHashTable["UserCSGTbl"];
        }

        /////////////////////////////////////////////////////////////////////////////
        public static string LogonHost()
        {
            string sHttpHost = (string)HttpContext.Current.Request.ServerVariables["HTTP_HOST"];

            if (sHttpHost != null)
            {
                return sHttpHost.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public static string LogonUser()
        {
            try
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "LogonHost : " + LogonHost(), string.Empty);
                if (LogonHost() == _localDefaultMachine || LogonHost() == "localhost")
                {
                    return _localDefaultUser;
                }

                string sLogonUser = HttpContext.Current.Request.ServerVariables["LOGON_USER"];

                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "sLogonUser : " + sLogonUser, string.Empty);
                if (sLogonUser != null && sLogonUser != string.Empty)
                {
                    return sLogonUser;
                }
                else
                {
                    string sIdentityUser = HttpContext.Current.User.Identity.Name;

                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "sIdentityUser : " + sIdentityUser, string.Empty);
                    if (sIdentityUser != null && sIdentityUser != string.Empty)
                    {
                        return sIdentityUser;
                    }
                    else
                    {
                        string sPrincipalName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        if (sPrincipalName != null && sPrincipalName != string.Empty)
                        {
                            return sPrincipalName;
                        }
                        else
                        {
                            string sLogOnIdentity = HttpContext.Current.Request.LogonUserIdentity.Name;
                            if (sLogOnIdentity != null && sLogOnIdentity != string.Empty)
                            {
                                return sLogOnIdentity;
                            }
                            else { return string.Empty; }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "LogonUser() : " + ex.StackTrace + " : " + ex.Message, string.Empty);
                return string.Empty;
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        private void InitializeUser(string sUserName, string sUserDomain, bool bIsAuthenticate)
        {
            try
            {
                if (sUserName != string.Empty)
                {
                    _sUserName = sUserName;

                    //_sUserDomain	= sUserDomain;
                    //  _bIsAff = (sUserDomain.ToLower().Trim() == _affiliateDomain) ? true : false;

                    if (SearchUserInDB(sUserName, "ad"))
                    {
                        // search user in ldap
                        if (bIsAuthenticate && (!_bIsValidUser))
                        {
                            //Hashtable ht = GetUserFromLDAP(sUserName, "ad");

                            LDAPUser._LDAPXmlPath = HttpContext.Current.Request.PhysicalApplicationPath + "LDAPConfig.xml";

                            LDAPUser objLDAP = new LDAPUser();

                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Before getting user from db : " + sUserName, string.Empty);
                            Hashtable ht = objLDAP.GetUserFromLDAP(sUserName, "ad");

                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "After getting user from db : " + sUserName, string.Empty);
                            if (ht.Count > 0)
                            {
                                try
                                {
                                    if (Add_New_User(sUserName, "ad", ht["FIRST_NAME"].ToString(), ht["MIDDLE_NAME"].ToString(), ht["LAST_NAME"].ToString(), ht["EMAIL"].ToString().Replace("&nbsp;", string.Empty).Trim(), ht["PHONE"].ToString().Replace("&nbsp;", string.Empty).Trim(), sUserName, ht["DISPLAYNAME"].ToString()) > 0)
                                    {
                                        SearchUserInDB(sUserName, "ad");
                                    }
                                }
                                catch (Exception)
                                {
                                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "InitializeUser1 : " + ex.StackTrace + " : " + ex.Message, string.Empty);
                                    System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                                    info.Add("sUserName", sUserName);

                                    //info.Add("sUserDom", sUserDomain);

                                    //ExceptionManager.Publish(ex, info)
                                }
                            }

                            //else { //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "InitializeUser : User Not found in LDAP", string.Empty); }
                        }
                    }
                    else
                    {
                        LDAPUser._LDAPXmlPath = HttpContext.Current.Request.PhysicalApplicationPath + "LDAPConfig.xml";

                        LDAPUser objLDAP = new LDAPUser();

                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Before getting user from db : " + sUserName, string.Empty);
                        Hashtable ht = objLDAP.GetUserFromLDAP(sUserName, "ad");

                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "After getting user from db : " + sUserName, string.Empty);
                        if (ht.Count > 0)
                            Add_New_User(sUserName, "ad", ht["FIRST_NAME"].ToString(), ht["MIDDLE_NAME"].ToString(), ht["LAST_NAME"].ToString(), ht["EMAIL"].ToString().Replace("&nbsp;", string.Empty).Trim(), ht["PHONE"].ToString().Replace("&nbsp;", string.Empty).Trim(), sUserName, ht["DISPLAYNAME"].ToString());

                        _sUserName = string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "InitializeUser2 : " + ex.StackTrace + " : " + ex.Message, string.Empty);
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public string SearchUserInDB(string sUserName, int UserCnt)
        {
            string sResp = string.Empty;

            if (UserCnt > 1)
            {
                UserRoleHashTable = GetUserFromDB(sUserName, true);

                if (UserRoleHashTable.Count > 0)
                {
                    try
                    {
                        try
                        {
                            SetProperties();
                            _bIsValidUser = true;
                        }
                        catch
                        {
                            // _bIsCAPT = false;
                            _bIsValidUser = false;
                        }
                    }
                    catch (Exception)
                    {
                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "searchuserindb: " + ex.StackTrace + " : " + ex.Message, string.Empty);
                        System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                        info.Add("sUserName", sUserName);

                        //ExceptionManager.Publish(ex, info)
                    }
                }
                else
                {
                    UserRoleHashTable = GetUserFromDB(sUserName, "ad");
                    if (UserRoleHashTable.Count > 0)
                    {
                        sResp = "User not in Activators Group";
                    }
                    else
                    {
                        sResp = "User not in DB";
                    }
                }
            }
            else
            {
                if (!SearchUserInDB(sUserName, "ad"))
                    sResp = "User not in DB";
            }

            return sResp;
        }

        /////////////////////////////////////////////////////////////////////////////
        public bool SearchUserInDB(string sUserName, string sUserDom)
        {
            bool bReturnVal = false;

            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Before Searching user in db : " + sUserName, string.Empty);
            UserRoleHashTable = GetUserFromDB(sUserName, sUserDom);

            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "after Searching user in db : " + sUserName + " htcount:" + ht.Count.ToString(), string.Empty);

            if (UserRoleHashTable.Count > 0)
            {
                try
                {
                    try
                    {
                        SetProperties();

                        _bIsValidUser = true;

                        bReturnVal = true;
                    }
                    catch
                    {
                        // _bIsCAPT = false;
                        _bIsValidUser = false;
                    }
                }
                catch (Exception)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "searchuserindb: " + ex.StackTrace + " : " + ex.Message, string.Empty);
                    System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                    info.Add("sUserName", sUserName);
                    info.Add("sUserDom", sUserDom);

                    //ExceptionManager.Publish(ex, info)
                }
            }
            else
            {
                _bIsValidUser = false;
                bReturnVal = false;
            }

            return bReturnVal;
        }

        public bool LoadUserInfo(int userId)
        {
            bool ret = false;
            UserRoleHashTable = GetUserFromDB(userId);

            if (UserRoleHashTable.Count > 0)
            {
                try
                {
                    SetProperties();
                    ret = true;
                }
                catch (Exception)
                {
                    //ExceptionManager.Publish(ex, info)
                }
            }

            return ret;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static Hashtable GetUserFromDB(string sUserName, bool bActv)
        {
            Hashtable ht = new Hashtable();
            UserDB ud = new UserDB();
            DataSet ds = new DataSet();

            try
            {
                if (true)
                {
                    ds = ud.GetUserFromDB(sUserName, bActv);

                    ht = LoadHashTable(ds);
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "getuserfromdb: " + ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sUserName);

                //ExceptionManager.Publish(ex, info)
            }

            return ht;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static Hashtable GetUserFromDB(string sUserName, string sUserDom)
        {
            Hashtable ht = new Hashtable();
            UserDB ud = new UserDB();
            DataSet ds = new DataSet();

            try
            {
                if (true)
                {
                    ds = ud.GetUserFromDB(sUserName);

                    ht = LoadHashTable(ds);
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "getuserfromdb: " + ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sUserName);
                info.Add("sUserDom", sUserDom);

                //ExceptionManager.Publish(ex, info)
            }

            return ht;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static Hashtable GetUserFromDB(int iUserId)
        {
            Hashtable ht = new Hashtable();
            UserDB ud = new UserDB();
            DataSet ds = new DataSet();

            try
            {
                if (true)
                {
                    ds = ud.GetUserFromDB(iUserId);

                    ht = LoadHashTable(ds);
                }
            }
            catch
            {
            }

            return ht;
        }

        protected static Hashtable LoadHashTable(DataSet ds)
        {
            Hashtable ht = new Hashtable();

            if (ds != null)
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("1")) && (ds.Tables[1].Rows.Count > 0))
                            ht["UserStatus"] = "1";
                        else if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("1")) && (ds.Tables[1].Rows.Count <= 0))
                            ht["UserStatus"] = "2";
                        else if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("0")) && (ds.Tables[1].Rows.Count <= 0))
                            ht["UserStatus"] = "0";
                        else if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("0")) && (ds.Tables[1].Rows.Count > 0))
                            ht["UserStatus"] = "2";
                        else if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("2")) && (ds.Tables[1].Rows.Count <= 0))
                            ht["UserStatus"] = "1";
                        else if ((ds.Tables[0].Rows[0]["REC_STUS_ID"].ToString().Equals("2")) && (ds.Tables[1].Rows.Count > 0))
                            ht["UserStatus"] = "1";
                    }
                    else
                        ht["UserStatus"] = "0";

                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        DataTable dt = ds.Tables[0];

                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "getuserfromdb dt count " + dt.Rows.Count, string.Empty);
                        ht["UserName"] = dt.Rows[0]["USER_ADID"].ToString();
                        ht["Email"] = dt.Rows[0]["EMAIL_ADR"].ToString();
                        ht["Acf2Id"] = dt.Rows[0]["USER_ACF2_ID"].ToString();
                        ht["Phone"] = dt.Rows[0]["PHN_NBR"].ToString();
                        ht["CellPhone"] = dt.Rows[0]["CELL_PHN_NBR"].ToString();
                        ht["PagerNumber"] = dt.Rows[0]["PGR_NBR"].ToString();
                        ht["PagerPin"] = dt.Rows[0]["PGR_PIN_NBR"].ToString();
                        ht["WebUserID"] = dt.Rows[0]["USER_ID"].ToString();
                        ht["FullName"] = dt.Rows[0]["FULL_NME"].ToString();
                        ht["DispName"] = dt.Rows[0]["DSPL_NME"].ToString();
                        ht["State"] = dt.Rows[0]["STT_CD"].ToString();
                        ht["City"] = dt.Rows[0]["CTY_NME"].ToString();
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[1];

                        if ((dt.Select("GRP_ID =" + ((int)EGroups.ReadOnly).ToString())).Length > 0)
                        {
                            ht["ReadOnly"] = "1";

                            ht["CANDEMember"] = "1";
                            ht["CANDEReviewer"] = "1";
                            ht["CANDEActivator"] = "1";
                            ht["CANDOReviewer"] = "0";
                            ht["CANDORTS"] = "0";
                            ht["CANDOUpdater"] = "0";
                            ht["CANDAdmin"] = "0";
                            ht["CANDWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CANDCannedRpt"] = "1";
                            else ht["CANDCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CANDOnDemandRpt"] = "1";
                            else ht["CANDOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CANDAdhocRpt"] = "1";
                            else ht["CANDAdhocRpt"] = "0";
                            ht["CANDSDE"] = "0";
                            ht["CANDNTE"] = "0";
                            ht["CANDSSE"] = "0";

                            ht["SIPTnUCaaSEMember"] = "1";
                            ht["SIPTnUCaaSEReviewer"] = "1";
                            ht["SIPTnUCaaSEActivator"] = "1";
                            ht["SIPTnUCaaSOReviewer"] = "0";
                            ht["SIPTnUCaaSORTS"] = "0";
                            ht["SIPTnUCaaSOUpdater"] = "0";
                            ht["SIPTnUCaaSAdmin"] = "0";
                            ht["SIPTnUCaaSWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSCannedRpt"] = "1";
                            else ht["SIPTnUCaaSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSOnDemandRpt"] = "1";
                            else ht["SIPTnUCaaSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSAdhocRpt"] = "1";
                            else ht["SIPTnUCaaSAdhocRpt"] = "0";
                            ht["SIPTnUCaaSSDE"] = "0";
                            ht["SIPTnUCaaSNTE"] = "0";
                            ht["SIPTnUCaaSSSE"] = "0";

                            ht["CSCEMember"] = "1";
                            ht["CSCEReviewer"] = "1";
                            ht["CSCEActivator"] = "1";
                            ht["CSCOReviewer"] = "0";
                            ht["CSCORTS"] = "0";
                            ht["CSCOUpdater"] = "0";
                            ht["CSCAdmin"] = "0";
                            ht["CSCWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CSCCannedRpt"] = "1";
                            else ht["CSCCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CSCOnDemandRpt"] = "1";
                            else ht["CSCOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CSCAdhocRpt"] = "1";
                            else ht["CSCAdhocRpt"] = "0";
                            ht["CSCSDE"] = "0";
                            ht["CSCNTE"] = "0";
                            ht["CSCSSE"] = "0";

                            ht["GOMEMember"] = "1";
                            ht["GOMEReviewer"] = "1";
                            ht["GOMEActivator"] = "1";
                            ht["GOMOReviewer"] = "0";
                            ht["GOMORTS"] = "0";
                            ht["GOMOUpdater"] = "0";
                            ht["GOMAdmin"] = "0";
                            ht["GOMWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["GOMCannedRpt"] = "1";
                            else ht["GOMCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["GOMOnDemandRpt"] = "1";
                            else ht["GOMOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["GOMAdhocRpt"] = "1";
                            else ht["GOMAdhocRpt"] = "0";
                            ht["GOMSDE"] = "0";
                            ht["GOMNTE"] = "0";
                            ht["GOMSSE"] = "0";

                            ht["MDSEMember"] = "1";
                            ht["MDSEReviewer"] = "1";
                            ht["MDSEActivator"] = "1";
                            ht["MDSOReviewer"] = "0";
                            ht["MDSORTS"] = "0";
                            ht["MDSOUpdater"] = "0";
                            ht["MDSAdmin"] = "0";
                            ht["MDSWFM"] = "0";
                            ht["MNSActivator"] = "0";
                            ht["MNSPreConfigEng"] = "0";
                            ht["MDSLCTPM"] = "0";
                            ht["MDSPM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["MDSCannedRpt"] = "1";
                            else ht["MDSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["MDSOnDemandRpt"] = "1";
                            else ht["MDSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["MDSAdhocRpt"] = "1";
                            else ht["MDSAdhocRpt"] = "0";
                            ht["MDSSDE"] = "0";
                            ht["MDSNTE"] = "0";
                            ht["MDSSSE"] = "0";
                            ht["MDSNOS"] = "0";
                            ht["MDSGatekeeper"] = "0";
                            ht["MDSManager"] = "0";

                            ht["SalesSEMember"] = "1";
                            ht["SalesSEReviewer"] = "1";
                            ht["SalesSEActivator"] = "1";
                            ht["SalesSOReviewer"] = "0";
                            ht["SalesSORTS"] = "0";
                            ht["SalesSOUpdater"] = "0";
                            ht["SalesSAdmin"] = "0";
                            ht["SalesSWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["SSCannedRpt"] = "1";
                            else ht["SSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["SSOnDemandRpt"] = "1";
                            else ht["SSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["SSAdhocRpt"] = "1";
                            else ht["SSAdhocRpt"] = "0";
                            ht["SSSDE"] = "0";
                            ht["SSNTE"] = "0";
                            ht["SSSSE"] = "0";

                            ht["WBOEMember"] = "1";
                            ht["WBOEReviewer"] = "1";
                            ht["WBOEActivator"] = "1";
                            ht["WBOOReviewer"] = "0";
                            ht["WBOORTS"] = "0";
                            ht["WBOOUpdater"] = "0";
                            ht["WBOAdmin"] = "0";
                            ht["WBOWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["WBOCannedRpt"] = "1";
                            else ht["WBOCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["WBOOnDemandRpt"] = "1";
                            else ht["WBOOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["WBOAdhocRpt"] = "1";
                            else ht["WBOAdhocRpt"] = "0";
                            ht["WBOSDE"] = "0";
                            ht["WBONTE"] = "0";
                            ht["WBOSSE"] = "0";

                            ht["xNCIAmEMember"] = "1";
                            ht["xNCIAmEReviewer"] = "1";
                            ht["xNCIAmEActivator"] = "1";
                            ht["xNCIAmOReviewer"] = "0";
                            ht["xNCIAmORTS"] = "0";
                            ht["xNCIAmOUpdater"] = "0";
                            ht["xNCIAmAdmin"] = "0";
                            ht["xNCIAmWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIAmCannedRpt"] = "1";
                            else ht["xNCIAmCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIAmOnDemandRpt"] = "1";
                            else ht["xNCIAmOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIAmAdhocRpt"] = "1";
                            else ht["xNCIAmAdhocRpt"] = "0";
                            ht["xNCIAmSDE"] = "0";
                            ht["xNCIAmNTE"] = "0";
                            ht["xNCIAmSSE"] = "0";

                            ht["xNCIAEMember"] = "1";
                            ht["xNCIAEReviewer"] = "1";
                            ht["xNCIAEActivator"] = "1";
                            ht["xNCIAOReviewer"] = "0";
                            ht["xNCIAORTS"] = "0";
                            ht["xNCIAOUpdater"] = "0";
                            ht["xNCIAAdmin"] = "0";
                            ht["xNCIAWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIACannedRpt"] = "1";
                            else ht["xNCIACannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIAOnDemandRpt"] = "1";
                            else ht["xNCIAOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIAAdhocRpt"] = "1";
                            else ht["xNCIAAdhocRpt"] = "0";
                            ht["xNCIASDE"] = "0";
                            ht["xNCIANTE"] = "0";
                            ht["xNCIASSE"] = "0";

                            ht["xNCIEEMember"] = "1";
                            ht["xNCIEEReviewer"] = "1";
                            ht["xNCIEEActivator"] = "1";
                            ht["xNCIEOReviewer"] = "0";
                            ht["xNCIEORTS"] = "0";
                            ht["xNCIEOUpdater"] = "0";
                            ht["xNCIEAdmin"] = "0";
                            ht["xNCIEWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIECannedRpt"] = "1";
                            else ht["xNCIECannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIEOnDemandRpt"] = "1";
                            else ht["xNCIEOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIEAdhocRpt"] = "1";
                            else ht["xNCIEAdhocRpt"] = "0";
                            ht["xNCIESDE"] = "0";
                            ht["xNCIENTE"] = "0";
                            ht["xNCIESSE"] = "0";

                            ht["DCPEEMember"] = "1";
                            ht["DCPEEReviewer"] = "1";
                            ht["DCPEEActivator"] = "1";
                            ht["DCPEOReviewer"] = "0";
                            ht["DCPEORTS"] = "0";
                            ht["DCPEOUpdater"] = "0";
                            ht["DCPEAdmin"] = "0";
                            ht["DCPEWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["DCPECannedRpt"] = "1";
                            else ht["DCPECannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["DCPEOnDemandRpt"] = "1";
                            else ht["DCPEOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["DCPEAdhocRpt"] = "1";
                            else ht["DCPEAdhocRpt"] = "0";
                            ht["DCPESDE"] = "0";
                            ht["DCPENTE"] = "0";
                            ht["DCPESSE"] = "0";

                            //ht["VCPEEMember"] = "1";
                            //ht["VCPEEReviewer"] = "1";
                            //ht["VCPEEActivator"] = "1";
                            //ht["VCPEOReviewer"] = "0";
                            //ht["VCPEORTS"] = "0";
                            //ht["VCPEOUpdater"] = "0";
                            //ht["VCPEAdmin"] = "0";
                            //ht["VCPEWFM"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                            //    ht["VCPECannedRpt"] = "1";
                            //else ht["VCPECannedRpt"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                            //    ht["VCPEOnDemandRpt"] = "1";
                            //else ht["VCPEOnDemandRpt"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                            //    ht["VCPEAdhocRpt"] = "1";
                            //else ht["VCPEAdhocRpt"] = "0";
                            //ht["VCPESDE"] = "0";
                            //ht["VCPENTE"] = "0";
                            //ht["VCPESSE"] = "0";

                            ht["CPETECHEMember"] = "1";
                            ht["CPETECHEReviewer"] = "1";
                            ht["CPETECHEActivator"] = "1";
                            ht["CPETECHOReviewer"] = "0";
                            ht["CPETECHORTS"] = "0";
                            ht["CPETECHOUpdater"] = "0";
                            ht["CPETECHAdmin"] = "0";
                            ht["CPETECHWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CPETECHCannedRpt"] = "1";
                            else ht["CPETECHCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CPETECHOnDemandRpt"] = "1";
                            else ht["CPETECHOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CPETECHAdhocRpt"] = "1";
                            else ht["CPETECHAdhocRpt"] = "0";
                            ht["CPETECHSDE"] = "0";
                            ht["CPETECHNTE"] = "0";
                            ht["CPETECHSSE"] = "0";

                            ht["DBBEMember"] = "1";
                            ht["DBBEReviewer"] = "1";
                            ht["DBBEActivator"] = "1";
                            ht["DBBOReviewer"] = "0";
                            ht["DBBORTS"] = "0";
                            ht["DBBOUpdater"] = "0";
                            ht["DBBAdmin"] = "0";
                            ht["DBBWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["DBBCannedRpt"] = "1";
                            else ht["DBBCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["DBBOnDemandRpt"] = "1";
                            else ht["DBBOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["DBBAdhocRpt"] = "1";
                            else ht["DBBAdhocRpt"] = "0";
                            ht["DBBSDE"] = "0";
                            ht["DBBNTE"] = "0";
                            ht["DBBSSE"] = "0";

                            ht["IsSystemUser"] = "0";
                        }
                        else
                        {
                            ht["ReadOnly"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["CANDEMember"] = "1";
                            else ht["CANDEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["CANDEReviewer"] = "1";
                            else ht["CANDEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["CANDEActivator"] = "1";
                            else ht["CANDEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["CANDOReviewer"] = "1";
                            else ht["CANDOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["CANDORTS"] = "1";
                            else ht["CANDORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["CANDOUpdater"] = "1";
                            else ht["CANDOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["CANDAdmin"] = "1";
                            else ht["CANDAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["CANDWFM"] = "1";
                            else ht["CANDWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CANDCannedRpt"] = "1";
                            else ht["CANDCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CANDOnDemandRpt"] = "1";
                            else ht["CANDOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CANDAdhocRpt"] = "1";
                            else ht["CANDAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["CANDSDE"] = "1";
                            else ht["CANDSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["CANDNTE"] = "1";
                            else ht["CANDNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CAND).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["CANDSSE"] = "1";
                            else ht["CANDSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["SIPTnUCaaSEMember"] = "1";
                            else ht["SIPTnUCaaSEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["SIPTnUCaaSEReviewer"] = "1";
                            else ht["SIPTnUCaaSEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["SIPTnUCaaSEActivator"] = "1";
                            else ht["SIPTnUCaaSEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["SIPTnUCaaSOReviewer"] = "1";
                            else ht["SIPTnUCaaSOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["SIPTnUCaaSORTS"] = "1";
                            else ht["SIPTnUCaaSORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["SIPTnUCaaSOUpdater"] = "1";
                            else ht["SIPTnUCaaSOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["SIPTnUCaaSAdmin"] = "1";
                            else ht["SIPTnUCaaSAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["SIPTnUCaaSWFM"] = "1";
                            else ht["SIPTnUCaaSWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSCannedRpt"] = "1";
                            else ht["SIPTnUCaaSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSOnDemandRpt"] = "1";
                            else ht["SIPTnUCaaSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["SIPTnUCaaSAdhocRpt"] = "1";
                            else ht["SIPTnUCaaSAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["SIPTnUCaaSSDE"] = "1";
                            else ht["SIPTnUCaaSSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["SIPTnUCaaSNTE"] = "1";
                            else ht["SIPTnUCaaSNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SIPTnUCaaS).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["SIPTnUCaaSSSE"] = "1";
                            else ht["SIPTnUCaaSSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["CSCEMember"] = "1";
                            else ht["CSCEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["CSCEReviewer"] = "1";
                            else ht["CSCEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["CSCEActivator"] = "1";
                            else ht["CSCEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["CSCOReviewer"] = "1";
                            else ht["CSCOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["CSCORTS"] = "1";
                            else ht["CSCORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["CSCOUpdater"] = "1";
                            else ht["CSCOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["CSCAdmin"] = "1";
                            else ht["CSCAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["CSCWFM"] = "1";
                            else ht["CSCWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CSCCannedRpt"] = "1";
                            else ht["CSCCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CSCOnDemandRpt"] = "1";
                            else ht["CSCOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CSCAdhocRpt"] = "1";
                            else ht["CSCAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["CSCSDE"] = "1";
                            else ht["CSCSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["CSCNTE"] = "1";
                            else ht["CSCNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CSC).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["CSCSSE"] = "1";
                            else ht["CSCSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["GOMEMember"] = "1";
                            else ht["GOMEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["GOMEReviewer"] = "1";
                            else ht["GOMEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["GOMEActivator"] = "1";
                            else ht["GOMEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["GOMOReviewer"] = "1";
                            else ht["GOMOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["GOMORTS"] = "1";
                            else ht["GOMORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["GOMOUpdater"] = "1";
                            else ht["GOMOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["GOMAdmin"] = "1";
                            else ht["GOMAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["GOMWFM"] = "1";
                            else ht["GOMWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["GOMCannedRpt"] = "1";
                            else ht["GOMCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["GOMOnDemandRpt"] = "1";
                            else ht["GOMOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["GOMAdhocRpt"] = "1";
                            else ht["GOMAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["GOMSDE"] = "1";
                            else ht["GOMSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["GOMNTE"] = "1";
                            else ht["GOMNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.GOM).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["GOMSSE"] = "1";
                            else ht["GOMSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["MDSEMember"] = "1";
                            else ht["MDSEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["MDSEReviewer"] = "1";
                            else ht["MDSEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["MDSEActivator"] = "1";
                            else ht["MDSEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["MDSOReviewer"] = "1";
                            else ht["MDSOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["MDSORTS"] = "1";
                            else ht["MDSORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["MDSOUpdater"] = "1";
                            else ht["MDSOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["MDSAdmin"] = "1";
                            else ht["MDSAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["MDSWFM"] = "1";
                            else ht["MDSWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.MNSActivator).ToString()).Length > 0)
                                ht["MNSActivator"] = "1";
                            else ht["MNSActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.MNSPreConfigEng).ToString()).Length > 0)
                                ht["MNSPreConfigEng"] = "1";
                            else ht["MNSPreConfigEng"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.LCTPM).ToString()).Length > 0)
                                ht["MDSLCTPM"] = "1";
                            else ht["MDSLCTPM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.MDSPM).ToString()).Length > 0)
                                ht["MDSPM"] = "1";
                            else ht["MDSPM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["MDSCannedRpt"] = "1";
                            else ht["MDSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["MDSOnDemandRpt"] = "1";
                            else ht["MDSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["MDSAdhocRpt"] = "1";
                            else ht["MDSAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["MDSSDE"] = "1";
                            else ht["MDSSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["MDSNTE"] = "1";
                            else ht["MDSNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["MDSSSE"] = "1";
                            else ht["MDSSSE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CPTNOS).ToString()).Length > 0)
                                ht["MDSNOS"] = "1";
                            else ht["MDSNOS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CPTGatekeeper).ToString()).Length > 0)
                                ht["MDSGatekeeper"] = "1";
                            else ht["MDSGatekeeper"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.MDS).ToString() + " AND ROLE_ID =" + ((int)ERoles.CPTManager).ToString()).Length > 0)
                                ht["MDSManager"] = "1";
                            else ht["MDSManager"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["SalesSEMember"] = "1";
                            else ht["SalesSEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["SalesSEReviewer"] = "1";
                            else ht["SalesSEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["SalesSEActivator"] = "1";
                            else ht["SalesSEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["SalesSOReviewer"] = "1";
                            else ht["SalesSOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["SalesSORTS"] = "1";
                            else ht["SalesSORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["SalesSOUpdater"] = "1";
                            else ht["SalesSOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["SalesSAdmin"] = "1";
                            else ht["SalesSAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["SalesSWFM"] = "1";
                            else ht["SalesSWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["SSCannedRpt"] = "1";
                            else ht["SSCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["SSOnDemandRpt"] = "1";
                            else ht["SSOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["SSAdhocRpt"] = "1";
                            else ht["SSAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["SSSDE"] = "1";
                            else ht["SSSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["SSNTE"] = "1";
                            else ht["SSNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.SalesSupport).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["SSSSE"] = "1";
                            else ht["SSSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["WBOEMember"] = "1";
                            else ht["WBOEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["WBOEReviewer"] = "1";
                            else ht["WBOEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["WBOEActivator"] = "1";
                            else ht["WBOEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["WBOOReviewer"] = "1";
                            else ht["WBOOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["WBOORTS"] = "1";
                            else ht["WBOORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["WBOOUpdater"] = "1";
                            else ht["WBOOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["WBOAdmin"] = "1";
                            else ht["WBOAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["WBOWFM"] = "1";
                            else ht["WBOWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["WBOCannedRpt"] = "1";
                            else ht["WBOCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["WBOOnDemandRpt"] = "1";
                            else ht["WBOOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["WBOAdhocRpt"] = "1";
                            else ht["WBOAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["WBOSDE"] = "1";
                            else ht["WBOSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["WBONTE"] = "1";
                            else ht["WBONTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.WBO).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["WBOSSE"] = "1";
                            else ht["WBOSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["xNCIAmEMember"] = "1";
                            else ht["xNCIAmEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["xNCIAmEReviewer"] = "1";
                            else ht["xNCIAmEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["xNCIAmEActivator"] = "1";
                            else ht["xNCIAmEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["xNCIAmOReviewer"] = "1";
                            else ht["xNCIAmOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["xNCIAmORTS"] = "1";
                            else ht["xNCIAmORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["xNCIAmOUpdater"] = "1";
                            else ht["xNCIAmOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["xNCIAmAdmin"] = "1";
                            else ht["xNCIAmAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["xNCIAmWFM"] = "1";
                            else ht["xNCIAmWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIAmCannedRpt"] = "1";
                            else ht["xNCIAmCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIAmOnDemandRpt"] = "1";
                            else ht["xNCIAmOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIAmAdhocRpt"] = "1";
                            else ht["xNCIAmAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["xNCIAmSDE"] = "1";
                            else ht["xNCIAmSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["xNCIAmNTE"] = "1";
                            else ht["xNCIAmNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAmericas).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["xNCIAmSSE"] = "1";
                            else ht["xNCIAmSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["xNCIAEMember"] = "1";
                            else ht["xNCIAEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["xNCIAEReviewer"] = "1";
                            else ht["xNCIAEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["xNCIAEActivator"] = "1";
                            else ht["xNCIAEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["xNCIAOReviewer"] = "1";
                            else ht["xNCIAOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["xNCIAORTS"] = "1";
                            else ht["xNCIAORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["xNCIAOUpdater"] = "1";
                            else ht["xNCIAOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["xNCIAAdmin"] = "1";
                            else ht["xNCIAAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["xNCIAWFM"] = "1";
                            else ht["xNCIAWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIACannedRpt"] = "1";
                            else ht["xNCIACannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIAOnDemandRpt"] = "1";
                            else ht["xNCIAOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIAAdhocRpt"] = "1";
                            else ht["xNCIAAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["xNCIASDE"] = "1";
                            else ht["xNCIASDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["xNCIANTE"] = "1";
                            else ht["xNCIANTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIAsia).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["xNCIASSE"] = "1";
                            else ht["xNCIASSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["xNCIEEMember"] = "1";
                            else ht["xNCIEEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["xNCIEEReviewer"] = "1";
                            else ht["xNCIEEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["xNCIEEActivator"] = "1";
                            else ht["xNCIEEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["xNCIEOReviewer"] = "1";
                            else ht["xNCIEOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["xNCIEORTS"] = "1";
                            else ht["xNCIEORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["xNCIEOUpdater"] = "1";
                            else ht["xNCIEOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["xNCIEAdmin"] = "1";
                            else ht["xNCIEAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["xNCIEWFM"] = "1";
                            else ht["xNCIEWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["xNCIECannedRpt"] = "1";
                            else ht["xNCIECannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["xNCIEOnDemandRpt"] = "1";
                            else ht["xNCIEOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["xNCIEAdhocRpt"] = "1";
                            else ht["xNCIEAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["xNCIESDE"] = "1";
                            else ht["xNCIESDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["xNCIENTE"] = "1";
                            else ht["xNCIENTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.xNCIEurope).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["xNCIESSE"] = "1";
                            else ht["xNCIESSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["DCPEEMember"] = "1";
                            else ht["DCPEEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["DCPEEReviewer"] = "1";
                            else ht["DCPEEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["DCPEEActivator"] = "1";
                            else ht["DCPEEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["DCPEOReviewer"] = "1";
                            else ht["DCPEOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["DCPEORTS"] = "1";
                            else ht["DCPEORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["DCPEOUpdater"] = "1";
                            else ht["DCPEOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["DCPEAdmin"] = "1";
                            else ht["DCPEAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["DCPEWFM"] = "1";
                            else ht["DCPEWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["DCPECannedRpt"] = "1";
                            else ht["DCPECannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["DCPEOnDemandRpt"] = "1";
                            else ht["DCPEOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["DCPEAdhocRpt"] = "1";
                            else ht["DCPEAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["DCPESDE"] = "1";
                            else ht["DCPESDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["DCPENTE"] = "1";
                            else ht["DCPENTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["DCPESSE"] = "1";
                            else ht["DCPESSE"] = "0";

                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                            //    ht["VCPEEMember"] = "1";
                            //else ht["VCPEEMember"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                            //    ht["VCPEEReviewer"] = "1";
                            //else ht["VCPEEReviewer"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                            //    ht["VCPEEActivator"] = "1";
                            //else ht["VCPEEActivator"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                            //    ht["VCPEOReviewer"] = "1";
                            //else ht["VCPEOReviewer"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                            //    ht["VCPEORTS"] = "1";
                            //else ht["VCPEORTS"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                            //    ht["VCPEOUpdater"] = "1";
                            //else ht["VCPEOUpdater"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                            //    ht["VCPEAdmin"] = "1";
                            //else ht["VCPEAdmin"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                            //    ht["VCPEWFM"] = "1";
                            //else ht["VCPEWFM"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                            //    ht["VCPECannedRpt"] = "1";
                            //else ht["VCPECannedRpt"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                            //    ht["VCPEOnDemandRpt"] = "1";
                            //else ht["VCPEOnDemandRpt"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                            //    ht["VCPEAdhocRpt"] = "1";
                            //else ht["VCPEAdhocRpt"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                            //    ht["VCPESDE"] = "1";
                            //else ht["VCPESDE"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                            //    ht["VCPENTE"] = "1";
                            //else ht["VCPENTE"] = "0";
                            //if (dt.Select("GRP_ID =" + ((int)EGroups.VCPE).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                            //    ht["VCPESSE"] = "1";
                            //else ht["VCPESSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["CPETECHEMember"] = "1";
                            else ht["CPETECHEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["CPETECHEReviewer"] = "1";
                            else ht["CPETECHEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["CPETECHEActivator"] = "1";
                            else ht["CPETECHEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["CPETECHOReviewer"] = "1";
                            else ht["CPETECHOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["CPETECHORTS"] = "1";
                            else ht["CPETECHORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["CPETECHOUpdater"] = "1";
                            else ht["CPETECHOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["CPETECHAdmin"] = "1";
                            else ht["CPETECHAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["CPETECHWFM"] = "1";
                            else ht["CPETECHWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["CPETECHCannedRpt"] = "1";
                            else ht["CPETECHCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["CPETECHOnDemandRpt"] = "1";
                            else ht["CPETECHOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["CPETECHAdhocRpt"] = "1";
                            else ht["CPETECHAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["CPETECHSDE"] = "1";
                            else ht["CPETECHSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["CPETECHNTE"] = "1";
                            else ht["CPETECHNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.CPETECH).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["CPETECHSSE"] = "1";
                            else ht["CPETECHSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventMember).ToString()).Length > 0)
                                ht["DBBEMember"] = "1";
                            else ht["DBBEMember"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventReviewer).ToString()).Length > 0)
                                ht["DBBEReviewer"] = "1";
                            else ht["DBBEReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.EventActivator).ToString()).Length > 0)
                                ht["DBBEActivator"] = "1";
                            else ht["DBBEActivator"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderReviewer).ToString()).Length > 0)
                                ht["DBBOReviewer"] = "1";
                            else ht["DBBOReviewer"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderRTS).ToString()).Length > 0)
                                ht["DBBORTS"] = "1";
                            else ht["DBBORTS"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.OrderUpdater).ToString()).Length > 0)
                                ht["DBBOUpdater"] = "1";
                            else ht["DBBOUpdater"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.Admin).ToString()).Length > 0)
                                ht["DBBAdmin"] = "1";
                            else ht["DBBAdmin"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.WFM).ToString()).Length > 0)
                                ht["DBBWFM"] = "1";
                            else ht["DBBWFM"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.CannedRpt).ToString()).Length > 0)
                                ht["DBBCannedRpt"] = "1";
                            else ht["DBBCannedRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.OnDemandRpt).ToString()).Length > 0)
                                ht["DBBOnDemandRpt"] = "1";
                            else ht["DBBOnDemandRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.AdhocRpt).ToString()).Length > 0)
                                ht["DBBAdhocRpt"] = "1";
                            else ht["DBBAdhocRpt"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.SalesDesignEngineer).ToString()).Length > 0)
                                ht["DBBSDE"] = "1";
                            else ht["DBBSDE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.NetworkTestEngineer).ToString()).Length > 0)
                                ht["DBBNTE"] = "1";
                            else ht["DBBNTE"] = "0";
                            if (dt.Select("GRP_ID =" + ((int)EGroups.DBB).ToString() + " AND ROLE_ID =" + ((int)ERoles.SystemSecurityEngineer).ToString()).Length > 0)
                                ht["DBBSSE"] = "1";
                            else ht["DBBSSE"] = "0";

                            if (dt.Select("GRP_ID =" + ((int)EGroups.System).ToString()).Length > 0)
                                ht["IsSystemUser"] = "1";
                            else ht["IsSystemUser"] = "0";
                        }
                    }
                    else
                    {
                        ht["CANDEMember"] = "0";
                        ht["CANDEReviewer"] = "0";
                        ht["CANDEActivator"] = "0";
                        ht["CANDOReviewer"] = "0";
                        ht["CANDORTS"] = "0";
                        ht["CANDOUpdater"] = "0";
                        ht["CANDAdmin"] = "0";
                        ht["CANDWFM"] = "0";
                        ht["CANDCannedRpt"] = "0";
                        ht["CANDOnDemandRpt"] = "0";
                        ht["CANDAdhocRpt"] = "0";
                        ht["CANDSDE"] = "0";
                        ht["CANDNTE"] = "0";
                        ht["CANDSSE"] = "0";

                        ht["SIPTnUCaaSEMember"] = "0";
                        ht["SIPTnUCaaSEReviewer"] = "0";
                        ht["SIPTnUCaaSEActivator"] = "0";
                        ht["SIPTnUCaaSOReviewer"] = "0";
                        ht["SIPTnUCaaSORTS"] = "0";
                        ht["SIPTnUCaaSOUpdater"] = "0";
                        ht["SIPTnUCaaSAdmin"] = "0";
                        ht["SIPTnUCaaSWFM"] = "0";
                        ht["SIPTnUCaaSCannedRpt"] = "0";
                        ht["SIPTnUCaaSOnDemandRpt"] = "0";
                        ht["SIPTnUCaaSAdhocRpt"] = "0";
                        ht["SIPTnUCaaSSDE"] = "0";
                        ht["SIPTnUCaaSNTE"] = "0";
                        ht["SIPTnUCaaSSSE"] = "0";

                        ht["CSCEMember"] = "0";
                        ht["CSCEReviewer"] = "0";
                        ht["CSCEActivator"] = "0";
                        ht["CSCOReviewer"] = "0";
                        ht["CSCORTS"] = "0";
                        ht["CSCOUpdater"] = "0";
                        ht["CSCAdmin"] = "0";
                        ht["CSCWFM"] = "0";
                        ht["CSCCannedRpt"] = "0";
                        ht["CSCOnDemandRpt"] = "0";
                        ht["CSCAdhocRpt"] = "0";
                        ht["CSCSDE"] = "0";
                        ht["CSCNTE"] = "0";
                        ht["CSCSSE"] = "0";

                        ht["GOMEMember"] = "0";
                        ht["GOMEReviewer"] = "0";
                        ht["GOMEActivator"] = "0";
                        ht["GOMOReviewer"] = "0";
                        ht["GOMORTS"] = "0";
                        ht["GOMOUpdater"] = "0";
                        ht["GOMAdmin"] = "0";
                        ht["GOMWFM"] = "0";
                        ht["GOMCannedRpt"] = "0";
                        ht["GOMOnDemandRpt"] = "0";
                        ht["GOMAdhocRpt"] = "0";
                        ht["GOMSDE"] = "0";
                        ht["GOMNTE"] = "0";
                        ht["GOMSSE"] = "0";

                        ht["MDSEMember"] = "0";
                        ht["MDSEReviewer"] = "0";
                        ht["MDSEActivator"] = "0";
                        ht["MDSOReviewer"] = "0";
                        ht["MDSORTS"] = "0";
                        ht["MDSOUpdater"] = "0";
                        ht["MDSAdmin"] = "0";
                        ht["MDSWFM"] = "0";
                        ht["MNSActivator"] = "0";
                        ht["MNSPreConfigEng"] = "0";
                        ht["MDSLCTPM"] = "0";
                        ht["MDSPM"] = "0";
                        ht["MDSCannedRpt"] = "0";
                        ht["MDSOnDemandRpt"] = "0";
                        ht["MDSAdhocRpt"] = "0";
                        ht["MDSSDE"] = "0";
                        ht["MDSNTE"] = "0";
                        ht["MDSSSE"] = "0";
                        ht["MDSNOS"] = "0";
                        ht["MDSGatekeeper"] = "0";
                        ht["MDSManager"] = "0";

                        ht["MVSEMember"] = "0";
                        ht["MVSEReviewer"] = "0";
                        ht["MVSEActivator"] = "0";
                        ht["MVSOReviewer"] = "0";
                        ht["MVSORTS"] = "0";
                        ht["MVSOUpdater"] = "0";
                        ht["MVSAdmin"] = "0";
                        ht["MVSWFM"] = "0";
                        ht["MNSActivator"] = "0";
                        ht["MNSPreConfigEng"] = "0";
                        ht["MVSLCTPM"] = "0";
                        ht["MVSPM"] = "0";
                        ht["MVSCannedRpt"] = "0";
                        ht["MVSOnDemandRpt"] = "0";
                        ht["MVSAdhocRpt"] = "0";
                        ht["MVSSDE"] = "0";
                        ht["MVSNTE"] = "0";
                        ht["MVSSSE"] = "0";

                        ht["SalesSEMember"] = "0";
                        ht["SalesSEReviewer"] = "0";
                        ht["SalesSEActivator"] = "0";
                        ht["SalesSOReviewer"] = "0";
                        ht["SalesSORTS"] = "0";
                        ht["SalesSOUpdater"] = "0";
                        ht["SalesSAdmin"] = "0";
                        ht["SalesSWFM"] = "0";
                        ht["SSCannedRpt"] = "0";
                        ht["SSOnDemandRpt"] = "0";
                        ht["SSAdhocRpt"] = "0";
                        ht["SSSDE"] = "0";
                        ht["SSNTE"] = "0";
                        ht["SSSSE"] = "0";

                        ht["WBOEMember"] = "0";
                        ht["WBOEReviewer"] = "0";
                        ht["WBOEActivator"] = "0";
                        ht["WBOOReviewer"] = "0";
                        ht["WBOORTS"] = "0";
                        ht["WBOOUpdater"] = "0";
                        ht["WBOAdmin"] = "0";
                        ht["WBOWFM"] = "0";
                        ht["WBOCannedRpt"] = "0";
                        ht["WBOOnDemandRpt"] = "0";
                        ht["WBOAdhocRpt"] = "0";
                        ht["WBOSDE"] = "0";
                        ht["WBONTE"] = "0";
                        ht["WBOSSE"] = "0";

                        ht["xNCIAmEMember"] = "0";
                        ht["xNCIAmEReviewer"] = "0";
                        ht["xNCIAmEActivator"] = "0";
                        ht["xNCIAmOReviewer"] = "0";
                        ht["xNCIAmORTS"] = "0";
                        ht["xNCIAmOUpdater"] = "0";
                        ht["xNCIAmAdmin"] = "0";
                        ht["xNCIAmWFM"] = "0";
                        ht["xNCIAmCannedRpt"] = "0";
                        ht["xNCIAmOnDemandRpt"] = "0";
                        ht["xNCIAmAdhocRpt"] = "0";
                        ht["xNCIAmSDE"] = "0";
                        ht["xNCIAmNTE"] = "0";
                        ht["xNCIAmSSE"] = "0";

                        ht["xNCIAEMember"] = "0";
                        ht["xNCIAEReviewer"] = "0";
                        ht["xNCIAEActivator"] = "0";
                        ht["xNCIAOReviewer"] = "0";
                        ht["xNCIAORTS"] = "0";
                        ht["xNCIAOUpdater"] = "0";
                        ht["xNCIAAdmin"] = "0";
                        ht["xNCIAWFM"] = "0";
                        ht["xNCIACannedRpt"] = "0";
                        ht["xNCIAOnDemandRpt"] = "0";
                        ht["xNCIAAdhocRpt"] = "0";
                        ht["xNCIASDE"] = "0";
                        ht["xNCIANTE"] = "0";
                        ht["xNCIASSE"] = "0";

                        ht["xNCIEEMember"] = "0";
                        ht["xNCIEEReviewer"] = "0";
                        ht["xNCIEEActivator"] = "0";
                        ht["xNCIEOReviewer"] = "0";
                        ht["xNCIEORTS"] = "0";
                        ht["xNCIEOUpdater"] = "0";
                        ht["xNCIEAdmin"] = "0";
                        ht["xNCIEWFM"] = "0";
                        ht["xNCIECannedRpt"] = "0";
                        ht["xNCIEOnDemandRpt"] = "0";
                        ht["xNCIEAdhocRpt"] = "0";
                        ht["xNCIESDE"] = "0";
                        ht["xNCIENTE"] = "0";
                        ht["xNCIESSE"] = "0";

                        ht["DCPEEMember"] = "0";
                        ht["DCPEEReviewer"] = "0";
                        ht["DCPEEActivator"] = "0";
                        ht["DCPEOReviewer"] = "0";
                        ht["DCPEORTS"] = "0";
                        ht["DCPEOUpdater"] = "0";
                        ht["DCPEAdmin"] = "0";
                        ht["DCPEWFM"] = "0";
                        ht["DCPECannedRpt"] = "0";
                        ht["DCPEOnDemandRpt"] = "0";
                        ht["DCPEAdhocRpt"] = "0";
                        ht["DCPESDE"] = "0";
                        ht["DCPENTE"] = "0";
                        ht["DCPESSE"] = "0";

                        ht["VCPEEMember"] = "0";
                        ht["VCPEEReviewer"] = "0";
                        ht["VCPEEActivator"] = "0";
                        ht["VCPEOReviewer"] = "0";
                        ht["VCPEORTS"] = "0";
                        ht["VCPEOUpdater"] = "0";
                        ht["VCPEAdmin"] = "0";
                        ht["VCPEWFM"] = "0";
                        ht["VCPECannedRpt"] = "0";
                        ht["VCPEOnDemandRpt"] = "0";
                        ht["VCPEAdhocRpt"] = "0";
                        ht["VCPESDE"] = "0";
                        ht["VCPENTE"] = "0";
                        ht["VCPESSE"] = "0";

                        ht["CPETECHEMember"] = "0";
                        ht["CPETECHEReviewer"] = "0";
                        ht["CPETECHEActivator"] = "0";
                        ht["CPETECHOReviewer"] = "0";
                        ht["CPETECHORTS"] = "0";
                        ht["CPETECHOUpdater"] = "0";
                        ht["CPETECHAdmin"] = "0";
                        ht["CPETECHWFM"] = "0";
                        ht["CPETECHCannedRpt"] = "0";
                        ht["CPETECHOnDemandRpt"] = "0";
                        ht["CPETECHAdhocRpt"] = "0";
                        ht["CPETECHSDE"] = "0";
                        ht["CPETECHNTE"] = "0";
                        ht["CPETECHSSE"] = "0";

                        ht["DBBEMember"] = "0";
                        ht["DBBEReviewer"] = "0";
                        ht["DBBEActivator"] = "0";
                        ht["DBBOReviewer"] = "0";
                        ht["DBBORTS"] = "0";
                        ht["DBBOUpdater"] = "0";
                        ht["DBBAdmin"] = "0";
                        ht["DBBWFM"] = "0";
                        ht["DBBCannedRpt"] = "0";
                        ht["DBBOnDemandRpt"] = "0";
                        ht["DBBAdhocRpt"] = "0";
                        ht["DBBSDE"] = "0";
                        ht["DBBNTE"] = "0";
                        ht["DBBSSE"] = "0";

                        ht["IsSystemUser"] = "0";
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        ht["UserCSGTbl"] = ds.Tables[2];
                    }
                }

            return ht;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static Hashtable GetUserFromLDAP(string sUserName, string sUserDom)
        {
            Hashtable ht = new Hashtable();

            DirectoryEntry _root = null;
            DirectorySearcher _searcher = null;

            try
            {
                _root = new DirectoryEntry(_rootPath);
                _root.Username = _username;
                _root.Password = _password;
                _root.AuthenticationType = AuthenticationTypes.ServerBind;

                _searcher = new DirectorySearcher(_root);

                _searcher.Filter = "(" + "zcAnyAttribute" + "=" + sUserName + ")";
                _searcher.PropertiesToLoad.Add("sn");
                _searcher.PropertiesToLoad.Add("mail");
                _searcher.PropertiesToLoad.Add("fonNickName");
                _searcher.PropertiesToLoad.Add("givenName");
                _searcher.PropertiesToLoad.Add("telephonenumber");
                _searcher.PropertiesToLoad.Add("uid");
                _searcher.PropertiesToLoad.Add("ADsPath");
                _searcher.PropertiesToLoad.Add("fonacf2id");
                _searcher.PropertiesToLoad.Add("street");
                _searcher.PropertiesToLoad.Add("postalcode");
                _searcher.PropertiesToLoad.Add("l");
                _searcher.PropertiesToLoad.Add("st");
                _searcher.PropertiesToLoad.Add("roomnumber");

                Hashtable htResult = new Hashtable();

                SearchResult searchResult = null;

                try
                {
                    searchResult = _searcher.FindOne();
                }
                catch
                {
                }

                if (searchResult != null)
                {
                    foreach (string props in _searcher.PropertiesToLoad)
                    {
                        try
                        {
                            string sTemp = (string)searchResult.Properties[props][0];
                            htResult[props] = sTemp;
                        }
                        catch (Exception)
                        {
                            try
                            {
                                htResult[props] = System.Text.Encoding.UTF7.GetString((byte[])searchResult.Properties[props][0]);
                            }
                            catch
                            {
                                htResult[props] = string.Empty;
                            }
                        }
                    }

                    searchResult = null;

                    if (htResult.Count > 0)
                    {
                        ht["USERNAME"] = sUserName;
                        ht["DOMAIN_NAME"] = sUserDom;

                        if (!object.ReferenceEquals(htResult["fonNickName"], string.Empty))
                        {
                            ht["FIRST_NAME"] = htResult["fonNickName"];
                        }
                        else
                        {
                            ht["FIRST_NAME"] = htResult["givenName"];
                        }
                        ht["MIDDLE_NAME"] = htResult["middleName"];
                        ht["LAST_NAME"] = htResult["sn"];
                        ht["EMAIL"] = htResult["mail"];
                        ht["PHONE"] = htResult["telephonenumber"];

                        //ht["LDAP_ID"] = htResult["uid"];
                        ht["ACF2_ID"] = htResult["fonacf2id"];
                        ht["STREET"] = htResult["street"];
                        ht["CITY"] = htResult["l"];
                        ht["STATE"] = htResult["st"];
                        ht["ZIP"] = htResult["postalcode"];
                        ht["CUBE"] = htResult["roomnumber"];
                    }

                    _root.Close();
                }
            }
            catch (Exception)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sUserName);

                //info.Add("sUserDom", sUserDom);

                //ExceptionManager.Publish(ex, info)
            }

            _root = null;
            _searcher = null;

            return ht;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static int Add_New_User(string _sUserName, string _sUserDom, string _sFirstName, string _sMiddleName, string _sLastName, string _sEmail, string _sPhone, string _sAcf2Id, string _sDispNme)
        {
            int iReturnVal = -100;
            string _sUserID = "[unknown]";
            UserDB ud = new UserDB();

            try
            {
                if (true)
                {
                    iReturnVal = ud.AddNewUser(_sUserName, _sUserDom, _sFirstName, _sMiddleName, _sLastName, _sEmail, _sPhone, _sAcf2Id, _sDispNme);
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "add_new_user : " + ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("USERNAME", _sUserName);
                info.Add("O_USER_ID", _sUserID);

                //ExceptionManager.Publish(ex, info)
            }

            return iReturnVal;
        }

        public int AddNewUser(string _sUserName, string _sUserDom, string _sFirstName, string _sMiddleName, string _sLastName, string _sEmail, string _sPhone, string _sAcf2Id, string _sDispNme)
        {
            int iReturnVal = -100;
            string _sUserID = "[unknown]";
            UserDB ud = new UserDB();

            try
            {
                if (true)
                {
                    iReturnVal = ud.AddNewUser(_sUserName, _sUserDom, _sFirstName, _sMiddleName, _sLastName, _sEmail, _sPhone, _sAcf2Id, _sDispNme);
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "add_new_user : " + ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("USERNAME", _sUserName);
                info.Add("O_USER_ID", _sUserID);

                //ExceptionManager.Publish(ex, info)
            }

            return iReturnVal;
        }

        public static int Delete_User(string _sUserName, int _iModUserUserID)
        {
            int bReturnVal = 0;
            UserDB ud = new UserDB();
            try
            {
                if (true)
                {
                    if (ud.DeleteUser(_sUserName, _iModUserUserID))
                    {
                        bReturnVal = 1;
                    }
                    else
                    {
                        bReturnVal = 3;
                    }
                }
            }
            catch (Exception)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("USERNAME", _sUserName);

                //ExceptionManager.Publish(ex, info)
            }
            return bReturnVal;
        }

        /////////////////////////////////////////////////////////////////////////////

        public static string GetUserDetails(string sUserName)
        {
            string NameString = String.Empty;
            UserDB ud = new UserDB();

            DataTable dt = new DataTable();

            try
            {
                if (true)
                {
                    dt = ud.GetUserDetails(sUserName).Tables[0];
                    if (dt != null)
                    {
                        NameString = dt.Rows[0]["FULL_NME"].ToString();
                    }
                    else
                    {
                        NameString = string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("USERNAME", sUserName);

                //ExceptionManager.Publish(ex, info)
            }
            return NameString;
        }

        // (sbg9814) PRTT 13799, 04/25/2006  ---   End

        #endregion "General Operations"
    }

    ///////////////////////////////////////////////////////////////////////////
    public class UserLibrary
    {
        ///////////////////////////////////////////////////////////////////////////

        #region "CheckOutUser"

        public static User CheckOutUser(string sKey, bool bIsAuthenticate, bool bIsCache, string sSessionID, string sIPAddr)
        {
            try
            {
                if (HttpContext.Current.Cache["iu_" + sKey] == null)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Success2", string.Empty);

                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "sKey : " + sKey, string.Empty);
                    if (sKey.IndexOf("\\") > 0)
                    {
                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Before Initializing user", string.Empty);
                        User usr = new User(sKey.Substring(sKey.IndexOf("\\") + 1), sKey.Substring(0, sKey.IndexOf("\\")), bIsAuthenticate);

                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Success13", string.Empty);
                        UserDB ud = new UserDB();

                        if ((!sSessionID.Equals(string.Empty)) && (!(sKey.Substring(sKey.IndexOf("\\") + 1).Contains("$"))))
                            ud.UpdateSession(sKey.Substring(sKey.IndexOf("\\") + 1), sSessionID, sIPAddr);

                        if (usr.IsValidUser && bIsCache)
                        {
                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Success14", string.Empty);

                            HttpContext.Current.Cache.Insert("iu_" + sKey, usr, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(30));

                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Success15", string.Empty);

                            // add into CAPT SQL Server database if we have to
                            if (usr.IsValidUser)
                            {
                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Before Adding User", string.Empty);

                                ud.AddNewUser(usr.UserName, usr.Domain, usr.FirstName, usr.MiddleName, usr.LastName, usr.Email, usr.Phone, usr.Acf2Id, usr.DispNme);
                            }
                        }

                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Successful : UserAuthenticated and Returned to GUI", string.Empty);
                        return usr;
                    }
                }
                else
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "Successful : User Returned to GUI from Cache", string.Empty);
                    return (User)HttpContext.Current.Cache["iu_" + sKey];
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "checkoutuser: " + ex.StackTrace + " : " + ex.Message, string.Empty);

                //HttpContext.Current.Response.Write("<br><br>" + ex.ToString() + "<br><br>");
            }

            return null;
        }

        /////////////////////////////////////////////////////////////////////////////
        public static User CheckOutUser(int iRefresh)
        {
            try
            {
                if (iRefresh == 1)
                    RemoveUserFromCache();
                return CheckOutUser(User.LogonUser(), true, true, string.Empty, string.Empty);
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, exp.StackTrace + " : " + exp.Message, string.Empty);
                return null;
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public static User CheckOutUser(int iRefresh, string sSessionID, string sIPAddr)
        {
            try
            {
                if (iRefresh == 1)
                    RemoveUserFromCache();
                return CheckOutUser(User.LogonUser(), true, true, sSessionID, sIPAddr);
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, exp.StackTrace + " : " + exp.Message, string.Empty);
                return null;
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public static User CheckOutUser(bool bIsCache)
        {
            return CheckOutUser(User.LogonUser(), true, bIsCache, string.Empty, string.Empty);
        }

        /////////////////////////////////////////////////////////////////////////////
        public static User CheckOutUser(bool bIsAuthenticate, bool bIsCache)
        {
            return CheckOutUser(User.LogonUser(), bIsAuthenticate, bIsCache, string.Empty, string.Empty);
        }

        /////////////////////////////////////////////////////////////////////////////
        public static User CheckOutUser(int iUserId, bool bIsCache)
        {
            User usr = new User(iUserId, false);
            return CheckOutUser(usr.Domain + "\\" + usr.UserName, true, bIsCache, string.Empty, string.Empty);
        }

        #endregion "CheckOutUser"

        #region "Cache Operations"

        /////////////////////////////////////////////////////////////////////////////
        public static void RemoveUserFromCache(string sKey)
        {
            HttpContext.Current.Cache.Remove("iu_" + sKey);
        }

        /////////////////////////////////////////////////////////////////////////////
        public static void RemoveUserFromCache()
        {
            HttpContext.Current.Cache.Remove("iu_" + User.LogonUser());
        }

        /////////////////////////////////////////////////////////////////////////////
        public static void ClearUserCache()
        {
            foreach (DictionaryEntry objItem in HttpContext.Current.Cache)
            {
                if (objItem.Key.ToString().Substring(0, 2).ToLower() == "iu_")
                {
                    HttpContext.Current.Cache.Remove(objItem.Key.ToString());
                }
            }
        }

        #endregion "Cache Operations"
    }

    //////////////////////////////////////////////////////////////////////////

    public class LDAPUser
    {
        #region "Data Members"

        public string _rootPath = string.Empty;
        public string _userName = string.Empty;
        public string _password = string.Empty;

        public static string _LDAPXmlPath = string.Empty;

        #endregion "Data Members"

        #region "Constructor"

        public LDAPUser()
        {
            setConfig();
        }

        #endregion "Constructor"

        #region "public methods"

        public void setConfig()
        {
            XmlDocument xdoc = new XmlDocument();

            try
            {
                xdoc.Load(_LDAPXmlPath);

                _rootPath = xdoc.SelectSingleNode("/ConfigSettings/rootPath").InnerText;
                _userName = xdoc.SelectSingleNode("/ConfigSettings/userName").InnerText;
                _password = xdoc.SelectSingleNode("/ConfigSettings/password").InnerText;

                xdoc = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable GetUserFromLDAP(string sUserName, string sUserDom)
        {
            Hashtable ht = new Hashtable();

            DirectoryEntry _root = null;
            DirectorySearcher _searcher = null;

            try
            {
                _root = new DirectoryEntry(_rootPath);
                _root.Username = _userName;
                _root.Password = _password;
                _root.AuthenticationType = AuthenticationTypes.ServerBind;

                _searcher = new DirectorySearcher(_root);

                //_searcher.Filter = "(" + "zcAnyAttribute" + "=" + sUserName + ")";
                //_searcher.Filter = "(" + "sAMAccountName" + "=" + sSearchString + ")";
                StringBuilder sb = new StringBuilder();
                sb.Append("(|(sAMAccountName=");
                sb.Append(sUserName);
                sb.Append(")(mail=");
                sb.Append(sUserName);
                sb.Append("))");

                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sb.ToString(), string.Empty);
                _searcher.Filter = sb.ToString();
                _searcher.PropertiesToLoad.Add("sn");
                _searcher.PropertiesToLoad.Add("mail");
                _searcher.PropertiesToLoad.Add("fonNickName");
                _searcher.PropertiesToLoad.Add("givenName");
                _searcher.PropertiesToLoad.Add("telephonenumber");
                _searcher.PropertiesToLoad.Add("displayName");
                _searcher.PropertiesToLoad.Add("cn");
                _searcher.PropertiesToLoad.Add("sAMAccountName");
                _searcher.PropertiesToLoad.Add("middleName");
                _searcher.PropertiesToLoad.Add("initials");

                //_searcher.PropertiesToLoad.Add("uid");
                //_searcher.PropertiesToLoad.Add("fonUnixID");
                //_searcher.PropertiesToLoad.Add("ADsPath");
                //_searcher.PropertiesToLoad.Add("fonacf2id");
                //_searcher.PropertiesToLoad.Add("street");
                //_searcher.PropertiesToLoad.Add("postalcode");
                //_searcher.PropertiesToLoad.Add("l");
                //_searcher.PropertiesToLoad.Add("st");
                //_searcher.PropertiesToLoad.Add("roomnumber");

                Hashtable htResult = new Hashtable();

                SearchResult searchResult = null;

                try
                {
                    searchResult = _searcher.FindOne();
                }
                catch (Exception ex)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                    string str = ex.Message;
                }

                if (searchResult != null)
                {
                    foreach (string props in _searcher.PropertiesToLoad)
                    {
                        try
                        {
                            string sTemp = (string)searchResult.Properties[props][0];
                            htResult[props] = sTemp;

                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sTemp, string.Empty);
                        }
                        catch (Exception)
                        {
                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                            try
                            {
                                htResult[props] = System.Text.Encoding.UTF7.GetString((byte[])searchResult.Properties[props][0]);
                            }
                            catch
                            {
                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                                htResult[props] = string.Empty;
                            }
                        }
                    }

                    searchResult = null;

                    if (htResult.Count > 0)
                    {
                        //ht["USERNAME"] = sUserName;
                        //ht["DOMAIN_NAME"] = sUserDom;

                        if (!object.ReferenceEquals(htResult["fonNickName"], string.Empty))
                        {
                            ht["FIRST_NAME"] = htResult["fonNickName"];
                        }
                        else
                        {
                            ht["FIRST_NAME"] = htResult["givenName"];
                        }
                        ht["MIDDLE_NAME"] = (htResult["middleName"] == null) ? ((htResult["initials"] == null) ? string.Empty : htResult["initials"]) : htResult["middleName"];
                        ht["LAST_NAME"] = htResult["sn"];
                        ht["EMAIL"] = htResult["mail"];

                        //ht["LDAP_ID"] = htResult["fonUnixID"];
                        //ht["ACF2_ID"] = htResult["fonacf2id"];
                        //ht["STREET"] = htResult["street"];
                        //ht["CITY"] = htResult["l"];
                        //ht["STATE"] = htResult["st"];
                        //ht["ZIP"] = htResult["postalcode"];
                        //ht["CUBE"] = htResult["roomnumber"];
                        ht["DISPLAYNAME"] = htResult["displayName"];
                        ht["PHONE"] = htResult["telephonenumber"];
                        ht["ADID"] = htResult["sAMAccountName"];
                    }

                    _root.Close();
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sUserName);

                //info.Add("sUserDom", sUserDom);

                //ExceptionManager.Publish(ex, info)
            }

            _root = null;
            _searcher = null;

            return ht;
        }

        public DataTable GetPickerUsersFromLDAP(string sSearchString, string sUserDom)
        {
            Hashtable ht = new Hashtable();
            DataTable dt = new DataTable();

            DirectoryEntry _root = null;
            DirectorySearcher _searcher = null;

            try
            {
                _root = new DirectoryEntry(_rootPath);
                _root.Username = _userName;
                _root.Password = _password;
                _root.AuthenticationType = AuthenticationTypes.ServerBind;

                _searcher = new DirectorySearcher(_root);

                string FName = string.Empty;
                string LName = string.Empty;
                StringBuilder sb = new StringBuilder();

                if (sSearchString.Contains(","))
                {
                    FName = sSearchString.Split(new Char[] { ',' })[1].Trim();
                    LName = sSearchString.Split(new Char[] { ',' })[0].Trim();
                    sb.Append("(&(|(givenName=");
                    sb.Append(FName + "*");
                    sb.Append(")(fonNickName=");
                    sb.Append(FName + "*");
                    sb.Append("))(sn=");
                    sb.Append(LName);
                    sb.Append("))");
                }
                else
                {
                    //_searcher.Filter = "(" + "sAMAccountName" + "=" + sSearchString + ")";

                    sb.Append("(|(sAMAccountName=");
                    sb.Append(sSearchString);
                    sb.Append(")(givenName=");
                    sb.Append(sSearchString);
                    sb.Append(")(fonNickName=");
                    sb.Append(sSearchString);
                    sb.Append(")(sn=");
                    sb.Append(sSearchString);
                    sb.Append(")(mail=");
                    sb.Append(sSearchString);
                    sb.Append("))");
                }

                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sb.ToString(), string.Empty);
                _searcher.Filter = sb.ToString();
                _searcher.PropertiesToLoad.Add("sn");
                _searcher.PropertiesToLoad.Add("mail");
                _searcher.PropertiesToLoad.Add("fonNickName");
                _searcher.PropertiesToLoad.Add("givenName");
                _searcher.PropertiesToLoad.Add("telephonenumber");
                _searcher.PropertiesToLoad.Add("displayName");
                _searcher.PropertiesToLoad.Add("cn");
                _searcher.PropertiesToLoad.Add("sAMAccountName");
                _searcher.PropertiesToLoad.Add("middleName");
                _searcher.PropertiesToLoad.Add("initials");

                //_searcher.PropertiesToLoad.Add("uid");
                //_searcher.PropertiesToLoad.Add("fonUnixID");
                //_searcher.PropertiesToLoad.Add("ADsPath");
                //_searcher.PropertiesToLoad.Add("fonacf2id");
                //_searcher.PropertiesToLoad.Add("street");
                //_searcher.PropertiesToLoad.Add("postalcode");
                //_searcher.PropertiesToLoad.Add("l");
                //_searcher.PropertiesToLoad.Add("st");
                //_searcher.PropertiesToLoad.Add("roomnumber");

                Hashtable htResult = new Hashtable();

                SearchResultCollection searchResultColl = null;

                try
                {
                    searchResultColl = _searcher.FindAll();
                }
                catch (Exception ex)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                    string str = ex.Message;
                }

                if (searchResultColl != null)
                {
                    dt.Columns.Add(new DataColumn("FIRSTNAME"));
                    dt.Columns.Add(new DataColumn("LASTNAME"));
                    dt.Columns.Add(new DataColumn("MIDDLENAME"));
                    dt.Columns.Add(new DataColumn("EMAIL"));
                    dt.Columns.Add(new DataColumn("PHONE"));
                    dt.Columns.Add(new DataColumn("DISPLAYNAME"));
                    dt.Columns.Add(new DataColumn("ADID"));

                    foreach (SearchResult searchResult in searchResultColl)
                    {
                        foreach (string props in _searcher.PropertiesToLoad)
                        {
                            try
                            {
                                string sTemp = (string)searchResult.Properties[props][0];
                                htResult[props] = sTemp;

                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sTemp, string.Empty);
                            }
                            catch (Exception)
                            {
                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                                try
                                {
                                    htResult[props] = System.Text.Encoding.UTF7.GetString((byte[])searchResult.Properties[props][0]);
                                }
                                catch
                                {
                                    htResult[props] = string.Empty;
                                }
                            }
                        }

                        //searchResult = null;

                        if ((htResult.Count > 0) && (htResult["sAMAccountName"].ToString().Length > 0))
                        {
                            //ht["USERNAME"] = sUserName;
                            //ht["DOMAIN_NAME"] = sUserDom;
                            DataRow dr = dt.NewRow();

                            if (!object.ReferenceEquals(htResult["fonNickName"], string.Empty))
                            {
                                dr["FIRSTNAME"] = htResult["fonNickName"];
                            }
                            else
                            {
                                dr["FIRSTNAME"] = htResult["givenName"];
                            }
                            dr["MIDDLENAME"] = (htResult["middleName"] == null) ? ((htResult["initials"] == null) ? string.Empty : htResult["initials"]) : htResult["middleName"];
                            dr["LASTNAME"] = htResult["sn"];
                            dr["EMAIL"] = htResult["mail"];

                            //ht["LDAP_ID"] = htResult["fonUnixID"];
                            //ht["ACF2_ID"] = htResult["fonacf2id"];
                            //ht["STREET"] = htResult["street"];
                            //ht["CITY"] = htResult["l"];
                            //ht["STATE"] = htResult["st"];
                            //ht["ZIP"] = htResult["postalcode"];
                            //ht["CUBE"] = htResult["roomnumber"];
                            dr["DISPLAYNAME"] = htResult["displayName"];
                            dr["PHONE"] = htResult["telephonenumber"];
                            dr["ADID"] = htResult["sAMAccountName"];
                            dt.Rows.Add(dr);
                        }
                    }

                    _root.Close();
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sSearchString);
            }

            _root = null;
            _searcher = null;

            return dt;
        }

        #endregion "public methods"
    }
}