﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>


          <tr>
            <td style="text-align: left">The following International order requires a Billing Only order keyed into Mach5:</td>
          </tr>
          <br></br>
        <br></br>
          <tr>
          <td width="35%" style="text-align: left; font-size=12px">
            M5#: <xsl:value-of select="BillOnlyEmail/FTN" />
          </td>
            </tr>
          <br></br>
          <tr>
            <td width="35%" style="text-align: left; font-size=12px">
              H6#: <xsl:value-of select="BillOnlyEmail/H5_H6_CUST_ID" />
            </td>
          </tr>


        <br></br>
        <br></br>
        <br></br>

       
        <tr>
            <td colspan="1">
              <table border="0"  width="98%" style="word-wrap: break-word; font-size=12px">
                    <tr>
                      <td style="text-align: left">Items to be ordered as Bill-Only:</td>
                    </tr>
                    <tr>
                      <td width="35%" style="text-align: left; font-size=12px">
                        ==================================================================================
                      </td>
                    </tr>
                    <xsl:for-each select="/BillOnlyEmail/Items">
                      <tr>  
                        <td width="35%" style="text-align: left; font-size=12px">
                          Type of Charge:  <xsl:value-of select="CKT_CHG_TYPE_DES" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Addl Charge NRC: <xsl:value-of select="CHG_NRC_AMT" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Tax Rate (%):  <xsl:value-of select="TAX_RT_PCT_QTY" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Currency:  <xsl:value-of select="CUR_NME" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Total Cost (USD):  <xsl:value-of select="CHG_NRC_IN_USD_AMT" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Note:   <xsl:value-of select="NTE_TXT" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          Sales Status:    <xsl:value-of select="STUS_DES" />
                        </td>
                      </tr>
                      <tr>
                        <td width="35%" style="text-align: left; font-size=12px">
                          ==================================================================================   
                        </td>
                      </tr>
                    </xsl:for-each>
               </table>
            </td>
          </tr>
          
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>