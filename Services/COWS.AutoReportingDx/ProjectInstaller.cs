﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Runtime.InteropServices;

namespace AutoReportingDx
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;
        private System.ComponentModel.Container components = null;

        public ProjectInstaller()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null) { components.Dispose(); }
            }
            base.Dispose(disposing);
        }

        private void ProjectInstaller_AfterInstall(object sender, System.Configuration.Install.InstallEventArgs e)
        {
            int iSCManagerHandle = 0;
            int iSCManagerLockHandle = 0;
            int iServiceHandle = 0;
            bool bChangeServiceConfig2 = false;
            modAPI.SERVICE_DESCRIPTION ServiceDescription;
            modAPI.SERVICE_FAILURE_ACTIONS ServiceFailureActions;
            modAPI.SC_ACTION[] ScActions = new modAPI.SC_ACTION[3];
            bool bCloseService = false;
            bool bUnlockSCManager = false;
            bool bCloseSCManager = false;
            IntPtr iScActionsPointer = new IntPtr();

            try
            {
                iSCManagerHandle = modAPI.OpenSCManagerA(null, null, modAPI.ServiceControlManagerType.SC_MANAGER_ALL_ACCESS);
                if (iSCManagerHandle < 1) { throw new Exception("Unable to open Services Manager."); }

                iSCManagerLockHandle = modAPI.LockServiceDatabase(iSCManagerHandle);
                if (iSCManagerLockHandle < 1) { throw new Exception("Unable to lock Services Manager."); }

                iServiceHandle = modAPI.OpenServiceA(iSCManagerHandle, "COWS.AutoReportingDx", modAPI.ACCESS_TYPE.SERVICE_ALL_ACCESS);
                if (iServiceHandle < 1) { throw new Exception("Unable to open Windows Service for modification."); }

                ServiceDescription.lpDescription = "Report Automation Dx (RAD)";
                bChangeServiceConfig2 = modAPI.ChangeServiceConfig2A(iServiceHandle, modAPI.InfoLevel.SERVICE_CONFIG_DESCRIPTION, ref ServiceDescription);
                if (bChangeServiceConfig2 == false) { throw new Exception("Unable to set Windows Service description."); }

                ScActions[0].Delay = 20000;
                ScActions[0].SCActionType = modAPI.SC_ACTION_TYPE.SC_ACTION_NONE;
                ScActions[1].Delay = 20000;
                ScActions[1].SCActionType = modAPI.SC_ACTION_TYPE.SC_ACTION_NONE;
                ScActions[2].Delay = 20000;
                ScActions[2].SCActionType = modAPI.SC_ACTION_TYPE.SC_ACTION_NONE;
                iScActionsPointer = Marshal.AllocHGlobal(Marshal.SizeOf(new modAPI.SC_ACTION()) * 3);
                modAPI.CopyMemory(iScActionsPointer, ScActions, Marshal.SizeOf(new modAPI.SC_ACTION()) * 3);

                ServiceFailureActions.dwResetPeriod = 600;
                ServiceFailureActions.lpRebootMsg = "";
                ServiceFailureActions.lpCommand = "";
                ServiceFailureActions.cActions = ScActions.Length;
                ServiceFailureActions.lpsaActions = iScActionsPointer.ToInt32();
                bChangeServiceConfig2 = modAPI.ChangeServiceConfig2A(iServiceHandle, modAPI.InfoLevel.SERVICE_CONFIG_FAILURE_ACTIONS, ref ServiceFailureActions);
                if (bChangeServiceConfig2 == false) { throw new Exception("Unable to set Windows Service Failure Actions."); }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Marshal.FreeHGlobal(iScActionsPointer);
                if (iServiceHandle > 0) { bCloseService = modAPI.CloseServiceHandle(iServiceHandle); }
                if (iSCManagerLockHandle > 0) { bUnlockSCManager = modAPI.UnlockServiceDatabase(iSCManagerLockHandle); }
                if (iSCManagerHandle != 0) { bCloseSCManager = modAPI.CloseServiceHandle(iSCManagerHandle); }
            }
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            //
            // serviceProcessInstaller1
            //
            this.serviceProcessInstaller1.Password = "eBC3jrj9Bah?k2u";
            this.serviceProcessInstaller1.Username = "AD\\$CORS";
            this.serviceProcessInstaller1.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
            //
            // serviceInstaller1
            //
            this.serviceInstaller1.Description = "Report Automation Dx (RAD)";
            this.serviceInstaller1.DisplayName = "COWS.AutoReportingDx";
            this.serviceInstaller1.ServiceName = "COWS.AutoReportingDx";
            this.serviceInstaller1.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            //
            // ProjectInstaller
            //
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1,
            this.serviceInstaller1});
            this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);
        }

        #endregion Component Designer generated code

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
        }
    }
}