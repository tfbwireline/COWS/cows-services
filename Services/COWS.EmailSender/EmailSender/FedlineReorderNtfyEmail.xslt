﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:include href="style.xslt" />
	<xsl:template match="/">
		<html>
			<body>
				<table width="900" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
					<tr>
						<td width="10%" style="text-align: left; font-weight: 600">Serial Number:  </td>
						<td width="40%">
              <a>
								<xsl:value-of select="FedlineReOrderEmailEntity/SerialNum" />
							</a>
						</td>
					</tr>
					<tr>
						<td width="10%" style="text-align: left; font-weight: 600">Device Name:  </td>
            <td width="40%">
							<xsl:value-of select="FedlineReOrderEmailEntity/DeviceName" />
						</td>
					</tr>
					<tr>
						<td width="10%" style="text-align: left; font-weight: 600">Install Order:   </td>
            <td width="40%">
							<a>
								<xsl:value-of select="FedlineReOrderEmailEntity/OrdrId" />
							</a>
						</td>
					</tr>
					<tr>
						<td width="10%" style="text-align: left; font-weight: 600">Order Type:   </td>
            <td width="40%">
							<xsl:value-of select="FedlineReOrderEmailEntity/OrdrTyp" />
						</td>
					</tr>
					<tr>
						<td width="10%" style="text-align: left; font-weight: 600">COWS Event:   </td>
            <td width="40%">
              <a>
                <xsl:value-of select="FedlineReOrderEmailEntity/EventId" />
              </a>
						</td>
					</tr>
          <tr></tr>

          <tr>
            <td colspan="2"> Installation For

              <a>
                <xsl:value-of select="FedlineReOrderEmailEntity/OrdrTyp" />
              </a>
                 Device Has Changed To In Progress Status - Original Device Cert Needs To Be Removed Manually.
            </td>
          </tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>