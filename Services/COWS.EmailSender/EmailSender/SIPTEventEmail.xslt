<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/EventID" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event Title : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/EventTitle" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event Type : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/EventType" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Product Type : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/ProdType" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Activity Type : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/ActyType" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Have the TNs been mapped to the appropriate TG? : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/TNsTGCd" />
            </td>
          </tr>
		  <xsl:if test="/EventEmail/SIPTEvent/ProdType[.='Enhanced Toll Free']">
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Toll Type : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/TollType" />
            </td>
          </tr>
		  </xsl:if>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Related Mach5 Order#'s : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/RelM5Ordrs" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">H6 : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/H6" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site ID : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/SiteID" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Address : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/SiteAdr" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/Floor" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">City : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/City" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">State/Providence : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/StProv" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Country/Region : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/Ctry" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Zip : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/ZipCode" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">US/International : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/USIntlCd" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site Contact Name : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/SiteCntctNme" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site Contact # : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/SiteCntctNbr" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Site Contact Hrs : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/SiteCntctHrs" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Team PDL : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/TeamPDL" />
            </td>
          </tr>
		  <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Design Document : </td>
            <td width="80%">
              <xsl:value-of select="/EventEmail/SIPTEvent/DesignDoc" />
            </td>
          </tr>
		  <tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Documents:</td>
						<td width="80%"></td>
					</tr>
						<tr>
							<td colspan="2">
								<table border="1">
									<tr bgcolor="#D8D8D8">
										<th>FileName</th>
										<th>FileSize</th>
									</tr>
									<xsl:for-each select="/EventEmail/SIPTDoc">
										<tr>
											<td>
												<a>
													<xsl:attribute name="href">
                            <xsl:choose>
                              <xsl:when test="contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cows.') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsdev2')">
                                <xsl:value-of select="concat(/EventEmail/SIPTEvent/DocURL, DocID)" />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="concat(/EventEmail/SIPTEvent/DocURL, '?docID=', DocID, '&amp;email=1')" />
                              </xsl:otherwise>
                            </xsl:choose>
													</xsl:attribute>
													<xsl:value-of select="DocName" />
												</a>
											</td>
											<td>
												<xsl:value-of select="DocSize" />
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
		  <tr>
            <td width="25%" style="text-align: left; font-weight: 600">Customer Request Start Date : </td>
            <td width="75%">
              <xsl:value-of select="concat(/EventEmail/SIPTEvent/CustReqStDt, ' CT')" />
            </td>
          </tr>
		  <tr>
            <td width="25%" style="text-align: left; font-weight: 600">Customer Request End Date : </td>
            <td width="75%">
              <xsl:value-of select="concat(/EventEmail/SIPTEvent/CustReqEndDt, ' CT')" />
            </td>
          </tr>
          <xsl:if test="/EventEmail/SIPTEvent/EndEStatusDes[.!='Rework']">
            <tr>
              <td width="20%" style="text-align: left; font-weight: 600">Assigned Activators : </td>
              <td width="80%">
                <xsl:value-of select="/EventEmail/SIPTEvent/AssignAct" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td colspan="2">
              <xsl:value-of select="concat('Event set to ', translate(/EventEmail/SIPTEvent/EndEStatusDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Status from ', translate(/EventEmail/SIPTEvent/StartEStatusDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Status using ', translate(/EventEmail/SIPTEvent/CommandDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Command by ', /EventEmail/SIPTEvent/ModUserName ,' with the following comments')" /> :
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <xsl:value-of select="/EventEmail/SIPTEvent/Comments" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
					        <xsl:choose>
										<xsl:when test="contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cows.') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/SIPTEvent/ViewEventURL,'cowsdev2')">
											<xsl:value-of select="/EventEmail/SIPTEvent/ViewEventURL" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="concat(/EventEmail/SIPTEvent/ViewEventURL, '?eid=', /EventEmail/SIPTEvent/EventID, '&amp;email=1')" />
										</xsl:otherwise>
									</xsl:choose>
                </xsl:attribute>
                Click here to view Event
              </a>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>