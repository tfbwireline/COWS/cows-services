﻿namespace AppConstants
{
    public enum EProdType
    {
        IPLCanada = 1,
        IPLMexico,
        IPLOffshore,
        IPLBilateral,
        IPLLeased,
        IPLResale,
        E2E,
        ICBH,
        CPEStandalone,
        DIAOnnet,
        DIAOffnet,
        GenericProduct,
        DedicatedIP,
        ManagedNetworkServices,
        MPLSOnnet,
        ManagedSecurityServices,
        SLFROnnet,
        SLFROffnet,
        MPLSOffnet,
        ATM,
        GFR,
        IP,
        SLFR,
        MPLS,
        DPL,
    }
}