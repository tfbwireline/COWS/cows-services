﻿namespace AppConstants
{
    public enum ELineCodingType
    {
        AMI,
        B8ZS,
        CCS,
        NSA_O,
        NSA_S
    }
}