﻿using DataManager;
using System.Data;

namespace COWS.Mach5SenderService
{
    public class Mach5SenderDB : MSSqlBase
    {
        public DataTable GetMach5Responses()
        {
            setCommand("dbo.getMach5Responses");
            return getDataTable();
        }

        public DataTable GetMach5RedesignMsgs()
        {
            setCommand("dbo.getMach5RedesignMsgs");
            return getDataTable();
        }

        public DataTable GetMach5BillDsptchMsgs()
        {
            setCommand("dbo.getM5BillDsptchMsgs");
            return getDataTable();
        }

        public int UpdateBillDsptchMsgs(int TRAN_ID, int BillDsptchID, int _iStat, string errMsg)
        {
            setCommand("dbo.updateM5BillDsptchMsgs");
            addIntParam("@TRAN_ID", TRAN_ID);
            addIntParam("@BillDisptchID", BillDsptchID);
            addIntParam("@STUS_ID", _iStat);
            addStringParam("@ERRMSG", errMsg);

            return execNoQuery();
        }

        public int UpdateMach5RedesignMsgs(int TRAN_ID, int _iStat, string errMsg)
        {
            setCommand("dbo.updateMach5RedesignMsgs");
            addIntParam("@TRAN_ID", TRAN_ID);
            addIntParam("@STUS_ID", _iStat);
            addStringParam("@ERRMSG", errMsg);

            return execNoQuery();
        }

        public int UpdateMach5Response(int TRAN_ID, int ORDR_ID, int _iStat, string errMsg)
        {
            setCommand("dbo.updateMach5Response");
            addIntParam("@TRAN_ID", TRAN_ID);
            addIntParam("@ORDR_ID", ORDR_ID);
            addIntParam("@STUS_ID", _iStat);
            addStringParam("@ERRMSG", errMsg);
            
            return execNoQuery();
        }

        public int InsertCustomerSecuredLog(int SCRD_OBJ_ID, int SCRD_OBJ_TYPE_ID, string LogMsg)
        {
            setCommand("dbo.insertCustScrdLog");
            addIntParam("@SCRD_OBJ_ID", SCRD_OBJ_ID);
            addIntParam("@SCRD_OBJ_TYPE_ID", SCRD_OBJ_TYPE_ID);
            addStringParam("@LogMsg", LogMsg);

            return execNoQuery();
        }
    }
}