﻿using DataManager;
using System.Data;

namespace COWS.EmailSender
{
    public class EmailSenderDB : MSSqlBase
    {
        public DataSet GetEmailRequests()
        {
            setCommand("dbo.getEmailRequests");
            return getDataSet();
        }

        public int UpdateEmailRequest(int _Email_Req_ID, int _OrderID, int _Status, int _EventID, int _RedesignID = 0)
        {
            setCommand("dbo.updateEmailRequest");
            addIntParam("@EMAIL_REQ_ID", _Email_Req_ID);
            addIntParam("@ORDR_ID", _OrderID);
            addIntParam("@STUS_ID", _Status);
            addIntParam("@EVENT_ID", _EventID);
            addIntParam("@REDESIGN_ID", _RedesignID);

            return execNoQuery();
        }

        public DataSet GetMDSEventData(int _iEventID)
        {
            setCommand("web.GetMDSEventEmailData");
            addIntParam("@iEventID", _iEventID);

            return getDataSet();
        }

        public DataSet GetMDSEventDataDetail(int _H5_H6, int _iEventID, string _iHeader, int _iFSAEventID, int _iID)
        {
            setCommand("[dbo].[getMDSEventEmailData]");
            addIntParam("@H5_H6_CUST_ID", 0);
            addIntParam("@EVENT_ID", _iEventID);
            addStringParam("@TAB_NME", _iHeader);
            addIntParam("@FSA_MDS_EVENT_ID", _iFSAEventID);
            addIntParam("@Tab_Seq_Nbr", _iID);

            return getDataSet();
        }

        public DataTable getMDSEventTabs(int _iEventID)
        {
            setCommand("dbo.getMDSEventTabs");
            addIntParam("@EVENT_ID", _iEventID);

            return getDataTable();
        }

        public DataTable GetRetractEvents()
        {
            setCommand("dbo.getRetractEvents");
            return getDataTable();
        }

        public int InsertReworkEmail(int _EventID, int _Status, string sTo, string sCC, string sSubj, string sBody)
        {
            setCommand("dbo.InsertEventEmail");
            addIntParam("@EVENT_ID", _EventID);
            addIntParam("@STUS_ID", _Status);
            addStringParam("sTo", sTo);
            addStringParam("sCC", sCC);
            addStringParam("sSubj", sSubj);
            addStringParam("sBody", sBody);

            return execNoQuery();
        }

        public int UpdateReworkEmail(int _EventID, int _Status)
        {
            setCommand("dbo.UpdateReworkEmail");
            addIntParam("@EVENT_ID", _EventID);
            addIntParam("@STUS_ID", _Status);

            return execNoQuery();
        }

        public int updateCANDReworkEvent(int _EventID, int _EventTypeID)
        {
            setCommand("dbo.updateCANDReworkEvent");
            addIntParam("@EventID", _EventID);
            addIntParam("@EventTypeID", _EventTypeID);

            return execNoQuery();
        }

        public DataSet GetGomPLCompData(string _OrderID)
        {
            setCommand("dbo.GetGomPLCompEmailData");
            addStringParam("@OrderID", _OrderID);

            return getDataSet();
        }

        public DataTable GetGomIPLIPASRData(int _OrderID)
        {
            setCommand("dbo.GetGomIPLIPASREmailData");
            addIntParam("@OrderID", _OrderID);
            return getDataTable();
        }

        public DataTable GetASREmailData(int _OrderID)
        {
            setCommand("dbo.getASREmailData");
            addIntParam("@orderID", _OrderID);
            return getDataTable();
        }

        public int UpdateASREmailStatus(int ASR_ID, int status)
        {
            setCommand("dbo.updateASREmailStatus");
            addIntParam("@ASR_ID", ASR_ID);
            addIntParam("@Status", status);
            return execNoQuery();
        }

        public int UpdateCPTEmailStatus(int emailReqID, int cptID, int status)
        {
            setCommand("dbo.UpdateCPTEmailStatus");
            addIntParam("@EMAIL_REQ_ID", emailReqID);
            addIntParam("@CPT_ID", cptID);
            addIntParam("@Status", status);
            return execNoQuery();
        }

        public int InsertCPTEmailRequest(int cptID, int emailReqTypeID, int status, string emailList, string cc, string subject, string body)
        {
            setCommand("dbo.InsertCPTEmailRequest");
            addIntParam("@CPT_ID", cptID);
            addIntParam("@EMAIL_REQ_TYPE_ID", emailReqTypeID);
            addIntParam("@STUS_ID", status);
            addStringParam("@EMAIL_LIST_TXT", emailList);
            addStringParam("@EMAIL_CC_TXT", cc);
            addStringParam("@EMAIL_SUBJ_TXT", subject);
            addStringParam("@EMAIL_BODY_TXT", body);
            return execNoQuery();
        }

        public int UpdateSDEEmailStatus(int emailReqID, int status)
        {
            setCommand("dbo.UpdateSDEEmailStatus");
            addIntParam("@EMAIL_REQ_ID", emailReqID);
            addIntParam("@STUS_ID", status);
            return execNoQuery();
        }

        public DataSet GetZscalerEmailData(int _OrderID)
        {
            setCommand("dbo.getZsclrDiscInfo");
            addIntParam("@ORDR_ID", _OrderID);
            return getDataSet();
        }

        public DataSet GetBillOnlyInfoData(int _OrderID)
        {
            setCommand("dbo.getISBillOnlyEmailInfo");
            addIntParam("@ORDR_ID", _OrderID);
            return getDataSet();
        }
    }
}