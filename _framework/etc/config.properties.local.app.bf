################################################################################
# Copyright (c) 2010 by Sprint Nextel
#
# All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
# and is the sole and exclusive property of the copyright owner. 
#
################################################################################
#
# Optional host-specific local application configuration file. Used to store 
# host-specific config settings, including those to support host and environment
# sanity checking. Initialize the HOST_CHECK with the value of the string returned
# by the 'hostname' command. The JOB_FILTER setting is necessary on applications
# that deploy to multiple hosts. The LOCAL_SERVER_ROLE setting is used to trim
# the build for applications differentiated across multiple server types. The 
# WEB_SVC_INDEX setting can be used to differentiate configurations for apps on 
# multiple web servers. The ENVIRONMENT_CHECK setting can be used for sanity 
# check of the environment (to make sure the wrong configuration is not deployed).
# 
################################################################################

# host name for this config file (what is returned by the hostname command)
HOST_CHECK = tvmxd971

# filter to indicate what ^M jobs should be generated for this host
JOB_FILTER = b

# filter to trim staged code depending on server type (should be WEB, APP, or BOTH)
LOCAL_SERVER_ROLE = APP

# index for web service domain (1, 2, 3, ...)
WEB_SVC_INDEX =

# environment validation (this must match value of ENV_NAME in config.properties)
ENVIRONMENT_CHECK = BREAKFIX

# startup mode for windows services (must be Automatic, Manual, or Disabled)
FSA_SVC_STARTUP_TYPE = Disabled
ODIESENDER_SVC_STARTUP_TYPE = Automatic
ODIERECEIVER_SVC_STARTUP_TYPE = Automatic
EMAILSENDER_SVC_STARTUP_TYPE = Automatic
TADPOLE_SVC_STARTUP_TYPE = Manual
RECURRINGAPPTPARSER_SVC_STARTUP_TYPE = Automatic
AUTOREPORTINGDX_SVC_STARTUP_TYPE = Manual
SSTATRECEIVER_SVC_STARTUP_TYPE = Manual
MACH5SENDER_SVC_STARTUP_TYPE = Automatic
SSTATSENDER_SVC_STARTUP_TYPE = Manual
PSOFTBATCH_SVC_STARTUP_TYPE = Automatic
RULESETPROCESSOR_SVC_STARTUP_TYPE = Disabled