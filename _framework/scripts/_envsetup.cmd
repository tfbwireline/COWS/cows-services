@echo off
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to setup the environment. Initializes environment variables
REM APP_HOME and JAVA_HOME. APP_HOME is derived from context and JAVA_HOME is read 
REM from the application configuration. Must be called in-process. Prints message 
REM and exits with non-zero exit code if there is an error. Just sets variables and
REM returns on success.
REM 
REM *******************************************************************************

REM
REM if setup has already been done just return
REM
IF "%_@AVT_FW_INIT@_%" EQU "1" EXIT /B 0


REM
REM initialize script context and APP_HOME
REM
SET _@=%~dp0
IF /I "%_@:~-9%" NEQ "\Scripts\" (
  ECHO ERROR: invalid context, framework must be deployed
  EXIT /B 1
)
SET ANT_HOME=%_@:~0,-9%\Framework\tools\ant
IF EXIST %ANT_HOME%\bin\ant.bat (
  IF EXIST %_@%_admin.xml (
    GOTO :contextok
  )
)
ECHO ERROR: framework not setup properly
EXIT /B 2
:contextok
SET _SCRIPT_HOME=%_@:~0,-1%
SET _@=%_@:~0,-9%
IF /I "%_@:~-8%" EQU "\Staging" SET _@=%_@:~0,-8%
IF /I "%_@:~-7%" EQU "\Backup" SET _@=%_@:~0,-7%
SET APP_HOME=%_@%


REM
REM make sure config file is available
REM
IF NOT EXIST %APP_HOME%\Framework\etc\config.properties (
  ECHO ERROR: application configuration file not found
  EXIT /B 3
)


REM
REM make sure parsing script is available
REM
SET _@=%APP_HOME%\Scripts\_parseCfg.js
IF NOT EXIST %_@% SET _@=%APP_HOME%\Staging\Scripts\_parseCfg.js
IF NOT EXIST %_@% (
  ECHO ERROR: framework parsing script not found
  EXIT /B 4
)


REM
REM pull JAVA_HOME out of configuration (trimming if necessary) and validate it
REM
SET JAVA_HOME=
FOR /F "usebackq delims== tokens=1,2" %%i IN (`cscript /NoLogo "%_@%" JAVA_HOME "%APP_HOME%\Framework\etc\config.properties.local"`) DO (
  SET JAVA_HOME=%%j
)
IF NOT DEFINED JAVA_HOME (
  FOR /F "usebackq delims== tokens=1,2" %%i IN (`cscript /NoLogo "%_@%" JAVA_HOME "%APP_HOME%\Framework\etc\config.properties"`) DO (
    SET JAVA_HOME=%%j
  )
)

SET JAVA_HOME=E:\Program Files (x86)\Java\jre1.8.0_201
IF EXIST "%JAVA_HOME%\" GOTO JAVA_HOME_OK
SET JAVA_HOME=E:\Program Files (x86)\Java\jre6

:JAVA_HOME_OK
IF NOT DEFINED JAVA_HOME (
  ECHO ERROR: configuration setting 'JAVA_HOME' not found in configuration
  EXIT /B 5
)
SET JAVA_HOME=%JAVA_HOME:/=\%
IF NOT EXIST "%JAVA_HOME%\bin\java.exe" (
  ECHO ERROR: configuration setting 'JAVA_HOME' has an invalid value
  EXIT /B 6
)
ECHO JAVA_HOME:'%JAVA_HOME%' is found
SET _@AVT_FW_INIT@_=1
EXIT /B 0