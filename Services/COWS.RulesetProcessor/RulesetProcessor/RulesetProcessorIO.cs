﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using CL = LogManager.FileLogger;

namespace COWS.RulesetProcessor
{
    public class RulesetProcessorIO
    {
        public int _iSleepTime = 0;

        private string _fileName = string.Empty;

        private string recHdr = string.Empty;
        private string _auditFilePath = ConfigurationManager.AppSettings["AuditFilePath"].ToString();
        private string _archiveAuditFilePath = ConfigurationManager.AppSettings["ArchiveAuditFilePath"].ToString();
        private string _edwDirectory = ConfigurationManager.AppSettings["EDWDirectory"].ToString();

        private RulesetProcessorDB _SenderDB = new RulesetProcessorDB();

        public void Process30RRequests(string _sExecApp)
        {
            DataTable DT = new DataTable();
            int ColumnCount = 0;
            int iCnt = 0;
            string FileDelimiter = "|"; //You can provide comma or pipe or whatever you like
            try
            {
                if (!CheckIfFileExists("fed.qda.pvmxe095." + DateTime.Now.ToString("MMddyyyy") + "*", _archiveAuditFilePath + _sExecApp + "\\"))
                {
                    DT = _SenderDB.getLogDetails();

                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        _fileName = string.Empty;
                        CreateFilename("fed.qda.pvmxe095.", ".log", "MMddyyyyHHmmss");
                        string bPath = _auditFilePath + _sExecApp + "\\" + _fileName;
                        StreamWriter sw = new StreamWriter(bPath, false);
                        string bPathArch = _archiveAuditFilePath + _sExecApp + "\\" + _fileName;
                        StreamWriter swArch = new StreamWriter(bPathArch, false);

                        iCnt = DT.Rows.Count;
                        ColumnCount = DT.Columns.Count;
                        CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " User(s) Log found to send to 30R", string.Empty);
                        for (int i = 0; i < ColumnCount; i++)
                        {
                            sw.Write(DT.Columns[i]);
                            swArch.Write(DT.Columns[i]);
                            if (i < ColumnCount - 1)
                            {
                                sw.Write(FileDelimiter);
                                swArch.Write(FileDelimiter);
                            }
                        }
                        sw.Write(sw.NewLine);
                        swArch.Write(swArch.NewLine);

                        // Write All Rows to the File
                        foreach (DataRow dr in DT.Rows)
                        {
                            for (int ir = 0; ir < ColumnCount; ir++)
                            {
                                if (!Convert.IsDBNull(dr[ir]))
                                {
                                    sw.Write(dr[ir].ToString());
                                    swArch.Write(dr[ir].ToString());
                                }
                                if (ir < ColumnCount - 1)
                                {
                                    sw.Write(FileDelimiter);
                                    swArch.Write(FileDelimiter);
                                }
                            }
                            sw.Write(sw.NewLine);
                            swArch.Write(swArch.NewLine);
                        }
                        sw.Close();
                        swArch.Close();
                    }
                    else
                        CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) User(s) Log found", string.Empty);
                }
                else
                    CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "There is already a 30R audit file created for today", string.Empty);
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error getting User(s) Log " + recHdr, e.Message.ToString() + e.InnerException.ToString());
            }
        }

        public void ProcessEDWRequests(string _sExecApp)
        {
            DataTable DT;
            try
            {
                if (!CheckIfFileExists("edw_qda_" + DateTime.Now.ToString("yyyyMMdd") + "*", _archiveAuditFilePath + _sExecApp + "\\"))
                {
                    DT = _SenderDB.getEDWDetails();

                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        _fileName = string.Empty;
                        CreateFilename("edw_qda_", ".txt", "yyyyMMddHHmmss");
                        string bPath = _auditFilePath + _sExecApp + "\\" + _fileName;
                        StreamWriter sw = new StreamWriter(bPath, false);
                        string bPathArch = _archiveAuditFilePath + _sExecApp + "\\" + _fileName;
                        StreamWriter swArch = new StreamWriter(bPathArch, false);

                        CL.WriteLogFile("Request", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " records found to send to EDW", string.Empty);
                        //Create Headers
                        sw.Write("applid:qda");
                        swArch.Write("applid:qda");
                        sw.Write(sw.NewLine);
                        swArch.Write(swArch.NewLine);
                        sw.Write("timestamp:" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                        swArch.Write("timestamp:" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                        sw.Write(sw.NewLine);
                        swArch.Write(swArch.NewLine);
                        sw.Write("directory:" + _edwDirectory);
                        swArch.Write("directory:" + _edwDirectory);
                        sw.Write(sw.NewLine);
                        swArch.Write(swArch.NewLine);
                        sw.Write("counts:" + DT.Rows.Count.ToString());
                        swArch.Write("counts:" + DT.Rows.Count.ToString());
                        sw.Write(sw.NewLine);
                        swArch.Write(swArch.NewLine);
                        sw.Write("login_id|employee_name|role_name|entity|entity_type|last_login_timestamp|discovery_timestamp|active_flag");
                        swArch.Write("login_id|employee_name|role_name|entity|entity_type|last_login_timestamp|discovery_timestamp|active_flag");
                        if (DT.Rows.Count > 0)
                        {
                            sw.Write(sw.NewLine);
                            swArch.Write(swArch.NewLine);
                        }
                        // Write All Rows to the File
                        foreach (DataRow dr in DT.Rows)
                        {
                            if (!Convert.IsDBNull(dr[0]))
                            {
                                sw.Write(dr[0].ToString());
                                swArch.Write(dr[0].ToString());
                                sw.Write(sw.NewLine);
                                swArch.Write(swArch.NewLine);
                            }
                        }
                        sw.Close();
                        swArch.Close();
                    }
                    else
                        CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) records found", string.Empty);
                }
                else
                    CL.WriteLogFile("Request", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "There is already a EDW audit file created for today", string.Empty);
            }
            catch (Exception e)
            {
                CL.WriteLogFile("Request", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error getting EDW records " + recHdr, e.Message.ToString() + e.InnerException.ToString());
            }
        }

        #region "Private Methods"

        private bool CheckIfFileExists(string sFilePattern, string sDestDir)
        {
            string[] fileArr = Directory.GetFiles(sDestDir, sFilePattern);
            if ((fileArr != null) && (fileArr.Length > 0))
                return true;
            else return false;
        }

        private void CreateFilename(string sFileName, string sFileExt, string sDateFormat)
        {
            DateTime date = DateTime.Now;
            StringBuilder sb = new StringBuilder();

            sb.Append(sFileName);
            sb.Append(date.ToString(sDateFormat));
            sb.Append(sFileExt);

            _fileName = sb.ToString();
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        #endregion "Private Methods"
    }
}