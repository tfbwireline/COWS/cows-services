﻿using AppConstants;
using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;

namespace UserManager
{
    public class UserDB : DBBase
    {
        public UserDB()
        {
        }

        public DataSet GetUserDetails(string sUserName)
        {
            return GetUsers(sUserName, -1, -1, -1);
        }

        public DataSet GetUserFromDB(string sUserName)
        {
            return GetUsers(sUserName, -1, -1, -1);
        }

        public DataSet GetUserFromDB(string sUserName, bool bActv)
        {
            if (bActv == true)
                return GetUsers(sUserName, -1, -1, ((int)ERoles.EventActivator));
            else
                return GetUsers(sUserName, -1, -1, -1);
        }

        public DataSet GetUsers(string sUserName, int iUserID, int iUserStatus, int iRoleID)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                DataSet ds = new DataSet("UserDataSet");

                if ((sUserName.Length <= 0) && (iUserID == -1) && (iUserStatus == -1) && (iRoleID == -1))
                {
                    var x = (from u in UserDBCntxt.LK_USER
                             select new { u.USER_ACF2_ID, u.USER_ADID, u.USER_ID, u.FULL_NME, u.EMAIL_ADR, u.DSPL_NME, u.PGR_NBR, u.PGR_PIN_NBR, u.PHN_NBR, u.CELL_PHN_NBR, u.REC_STUS_ID, u.STT_CD, u.CTY_NME });
                    ds.Tables.Add(LinqHelper.CopyToDataTable(x, null, null));

                    var y = (from ug in UserDBCntxt.USER_GRP_ROLE
                             where (ug.REC_STUS_ID == ((Byte)ERecStatus.Active))
                             select new { ug.GRP_ID, ug.ROLE_ID });
                    ds.Tables.Add(LinqHelper.CopyToDataTable(y, null, null));

                    var z = (from ucl in UserDBCntxt.USER_CSG_LVL
                             join lc in UserDBCntxt.LK_CSG_LVL on ucl.CSG_LVL_ID equals lc.CSG_LVL_ID
                             select new { ucl.USER_ID, ucl.CSG_LVL_ID, lc.CSG_LVL_CD });
                    ds.Tables.Add(LinqHelper.CopyToDataTable(z, null, null));
                }
                else if ((sUserName.Length <= 0) && (iUserID != -1) && (iUserStatus == -1))
                {
                    var x = (from u in UserDBCntxt.LK_USER
                             where (u.USER_ID == iUserID)
                             //&& (u.REC_STUS_ID == ((Byte)ERecStatus.Active)))
                             select new { u.USER_ACF2_ID, u.USER_ADID, u.USER_ID, u.FULL_NME, u.EMAIL_ADR, u.DSPL_NME, u.PGR_NBR, u.PGR_PIN_NBR, u.PHN_NBR, u.CELL_PHN_NBR, u.REC_STUS_ID, u.STT_CD, u.CTY_NME }).ToList();

                    var yug = (from ug in UserDBCntxt.USER_GRP_ROLE
                               where ((ug.USER_ID == iUserID) &&
                                      (ug.REC_STUS_ID == ((Byte)ERecStatus.Active)))
                               select new { ug.GRP_ID, ug.ROLE_ID, ug.REC_STUS_ID });

                    if (iRoleID != -1)
                        yug = yug.Where("ROLE_ID == " + iRoleID.ToString() + " && REC_STUS_ID == " + (((int)ERecStatus.Active).ToString()));
                    //{
                    //    string strWhr = "ROLE_ID == " + iRoleID.ToString() + " && REC_STUS_ID == " + (((int)ERecStatus.Active).ToString());
                    //    y = DynamicQueryable.Where(y, strWhr, null);
                    //}

                    var y = yug.ToList();

                    var z = (from ucl in UserDBCntxt.USER_CSG_LVL
                             join lc in UserDBCntxt.LK_CSG_LVL on ucl.CSG_LVL_ID equals lc.CSG_LVL_ID
                             where (ucl.USER_ID == iUserID)
                             select new { ucl.CSG_LVL_ID });

                    /*if (y.Count > 0)
                    {
                        ds.Tables.Add(LinqHelper.CopyToDataTable(x, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(y, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(z, null, null));
                    }
                    else
                    {
                        ds = null;
                    }*/

                    if (x.Count > 0)
                    {
                        ds.Tables.Add(LinqHelper.CopyToDataTable(x, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(y, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(z, null, null));
                    }
                }
                else
                {
                    var xlu = (from u in UserDBCntxt.LK_USER
                               where (u.USER_ADID == sUserName)
                               //&& (u.REC_STUS_ID == ((Byte)ERecStatus.Active)))
                               select new { u.USER_ACF2_ID, u.USER_ADID, u.USER_ID, u.FULL_NME, u.EMAIL_ADR, u.DSPL_NME, u.PGR_NBR, u.PGR_PIN_NBR, u.PHN_NBR, u.CELL_PHN_NBR, u.REC_STUS_ID, u.STT_CD, u.CTY_NME });

                    var x = xlu.ToList();

                    int UID = -1;
                    if (x.Count > 0)
                    {
                        UID = x[0].USER_ID;
                    }

                    var yug = (from ug in UserDBCntxt.USER_GRP_ROLE
                               where ((ug.USER_ID == UID) &&
                                     (ug.REC_STUS_ID == ((Byte)ERecStatus.Active)))
                               select new { ug.GRP_ID, ug.ROLE_ID, ug.REC_STUS_ID });

                    if (iRoleID != -1)
                        yug = yug.Where("ROLE_ID == " + iRoleID.ToString() + " && REC_STUS_ID == " + (((int)ERecStatus.Active).ToString()));
                    //{
                    //    string strWhr = "ROLE_ID == " + iRoleID.ToString() + " && REC_STUS_ID == " + (((int)ERecStatus.Active).ToString());
                    //    y = DynamicQueryable.Where(y, strWhr, null);
                    //}

                    var y = yug.ToList();

                    var z = (from ucl in UserDBCntxt.USER_CSG_LVL
                             join lc in UserDBCntxt.LK_CSG_LVL on ucl.CSG_LVL_ID equals lc.CSG_LVL_ID
                             where (ucl.USER_ID == UID)
                             select new { lc.CSG_LVL_ID });

                    /*if (y.Count > 0)
                    {
                        ds.Tables.Add(LinqHelper.CopyToDataTable(x, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(y, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(z, null, null));
                    }
                    else
                    {
                        ds = null;
                    }*/

                    if (x.Count > 0)
                    {
                        ds.Tables.Add(LinqHelper.CopyToDataTable(x, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(y, null, null));
                        ds.Tables.Add(LinqHelper.CopyToDataTable(z, null, null));
                    }
                }

                return ds;
            }
        }

        public DataSet GetUserFromDB(int iUserId)
        {
            return GetUsers(string.Empty, iUserId, -1, -1);
        }

        public int AddNewUser(string sUserName, string sUserDom, string sFirstName, string sMiddleName, string sLastName, string sEmail, string sPhone, string sAcf2Id, string sDispNme)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                int nextId = -1;

                lock (this)
                {
                    LK_USER lu = new LK_USER();

                    if ((from u in UserDBCntxt.LK_USER where u.USER_ADID == sUserName select new { u.USER_ADID }).Count() <= 0)
                    {
                        nextId = (int)(from u in UserDBCntxt.LK_USER select u.USER_ID).Max();

                        lu.USER_ID = (int)++nextId;
                        lu.USER_ADID = sUserName;
                        lu.USER_ACF2_ID = sAcf2Id;
                        if (sMiddleName.Trim().Length > 0)
                            lu.FULL_NME = sFirstName + " " + sMiddleName + " " + sLastName;
                        else
                            lu.FULL_NME = sFirstName + " " + sLastName;
                        lu.PHN_NBR = (sPhone == null) ? string.Empty : ((sPhone == String.Empty) ? string.Empty : sPhone);
                        lu.EMAIL_ADR = sEmail;
                        lu.REC_STUS_ID = (byte)ERecStatus.InActive;
                        lu.CREAT_BY_USER_ID = 1;
                        lu.CREAT_DT = DateTime.Now;
                        lu.DSPL_NME = sDispNme;

                        UserDBCntxt.LK_USER.InsertOnSubmit(lu);
                        UserDBCntxt.SubmitChanges();
                    }
                }

                return nextId;
            }
        }

        public void UpdateUserInfo(string sUserID, string sPhone, string sEmail, string sCellPhone, string sPagerPhone, string sPagerPin, int iLoggedInUser)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                LK_USER lu = (from u in UserDBCntxt.LK_USER
                              where u.USER_ID == Int32.Parse(sUserID)
                              select u).Single();
                if (sPhone.Trim().Length > 0)
                    lu.PHN_NBR = sPhone;
                if (sEmail.Trim().Length > 0)
                    lu.EMAIL_ADR = sEmail;
                if (sCellPhone.Trim().Length > 0)
                    lu.CELL_PHN_NBR = sCellPhone;
                if (sPagerPhone.Trim().Length > 0)
                    lu.PGR_NBR = sPagerPhone;
                if (sPagerPin.Trim().Length > 0)
                    lu.PGR_PIN_NBR = sPagerPin;
                lu.MODFD_BY_USER_ID = iLoggedInUser;
                lu.MODFD_DT = DateTime.Now;

                UserDBCntxt.SubmitChanges();
            }
        }

        public void UpdateUserStatus(string sUserName, string sLoggedInUser, string sUserStatus)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                LK_USER lu = (from u in UserDBCntxt.LK_USER
                              where u.USER_ADID == sUserName
                              select u).Single();

                lu.REC_STUS_ID = Byte.Parse(sUserStatus);
                lu.MODFD_BY_USER_ID = Int16.Parse(sLoggedInUser);
                lu.MODFD_DT = DateTime.Now;

                UserDBCntxt.SubmitChanges();
            }
        }

        public bool DeleteUser(string sUserName, int iModUserID)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                try
                {
                    var ul = (from u in UserDBCntxt.LK_USER
                              where u.USER_ADID == sUserName
                              select u);

                    foreach (var u in ul)
                    {
                        u.REC_STUS_ID = Byte.Parse("0");
                        u.MODFD_BY_USER_ID = iModUserID;
                        u.MODFD_DT = DateTime.Now;
                    }

                    UserDBCntxt.SubmitChanges();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool UpdateSession(string sUserADID, string sSessionID, string sIP)
        {
            using (COWS UserDBCntxt = new COWS(ConnStr))
            {
                try
                {
                    setCommand("web.UpdateWebSession");
                    addStringParam("@UADID", sUserADID);
                    addStringParam("@IPAdr", sIP);
                    addStringParam("@SID", sSessionID);

                    execNoQuery();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public List<Group> GetGroupList()
        {
            List<Group> retList = new List<Group>();

            return retList;
        }

        public bool SaveSecurity(User objUser, int iSelUserID, string sUserID)
        {
            string sGrpStatus = string.Empty;
            try
            {
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}