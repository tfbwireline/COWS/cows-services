﻿namespace AppConstants
{
    public enum EMDSNtwkActyType
    {
        MDSOnly = 1,
        NetworkOnly,
        NetworkAndMDS,
        VASNetworkOnly,
        VASNetworkAndMDS
    }
}