﻿using System.Configuration;

namespace DataManager
{
    public abstract class DBBase : MSSqlBase
    {
        private bool _bDetailLog = false;

        public DBBase()
        {
            _connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                _bDetailLog = ConfigurationManager.AppSettings["DetailLog"].ToString().Equals("1");
            }
            catch { }
        }

        public string ConnStr
        {
            get { return _connStr; }
        }

        protected bool IsDetailLog
        {
            get { return _bDetailLog; }
        }
    }
}