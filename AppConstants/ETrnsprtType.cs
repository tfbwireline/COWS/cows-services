﻿namespace AppConstants
{
    public enum ETrnsprtType
    {
        Wired = 1,
        SpAE,
        SprintLink,
        DSL
    }
}