@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to stop the "emailsender_svc" application component. An ERRORLEVEL
REM of 0 will be returned if the component is stopped successfully, and a non-zero 
REM ERRORLEVEL will be returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM retrieve the service environment prefix and prepend to name
REM
CALL "%APP_HOME%\Scripts\_getconfig.cmd" SVC_ENV_PREFIX
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_emailsender_svc.cmd 5 "ERROR: unable to retrieve service env prefix from configuration"
  EXIT /b 5
)
SET SERVICE_NAME=%SVC_ENV_PREFIX%COWS.EmailSender


REM
REM invoke framework service stop script
REM
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_stopService.js %SERVICE_NAME%
