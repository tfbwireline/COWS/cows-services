@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to retrieve a setting from the application configuration
REM for other framework scripts. Expects first argument to be the name of the 
REM setting to retrieve. Must be 'CALL'ed in-process. If successful, sets the 
REM environment variable with the same name to the value retrieved. If an optional
REM second argument is given it will be used for the environment variable name. If 
REM there are any errors (including the setting not being in the configuration), a
REM message will be printed and a non-zero ERRORLEVEL will be returned. 
REM
REM *******************************************************************************

REM
REM initialize some variables
REM
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%
IF "%1" EQU "" (
  ECHO ERROR: _getconfig.cmd called without any arguments
  EXIT /B 9
)
SET _@=%1
IF "%2" NEQ "" SET _@=%2


REM
REM now we are ready to parse and (hopefully) retrieve the value
REM
SET %_@%=
FOR /F "usebackq tokens=1,2* delims==" %%i IN (
  `CALL "%ANT_HOME%"\bin\ant.bat -f "%_SCRIPT_HOME%"\_admin.xml "-Dcfgfile=%APP_HOME%\Framework\etc\config.properties" "-Dcfg.var.name=%1" _get.config`
) DO (
  IF "%%i" EQU "     [echo] INFO: %1" (
    SET %_@%=%%j
    EXIT /B 0)
  )
)


REM
REM if we get here, the variable does not exist (or script failed)
REM
REM echo ERROR: unable to retrieve '%1' from configuration
EXIT /B 9
