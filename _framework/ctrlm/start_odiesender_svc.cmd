@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to start the "odiesender_svc" application component. An ERRORLEVEL
REM of 0 will be returned if the component is started successfully, and a non-zero 
REM ERRORLEVEL will be returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM retrieve and validate the startup mode for the service
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd ODIESENDER_SVC_STARTUP_TYPE START_MODE
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_odiesender_svc.cmd 5 "ERROR: unable to retrieve ODIESENDER_SVC_STARTUP_TYPE from configuration"
  EXIT /b 5
)
IF "%START_MODE%" NEQ "Automatic" (
  IF "%START_MODE%" NEQ "Manual" (
    echo ERROR: cannot start service unless start mode is Automatic or Manual
    CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_odiesender_svc.cmd 6 "ERROR: cannot start service unless start mode is Automatic or Manual"
    EXIT /b 6
  )
)
CALL "%APP_HOME%\Scripts\_getconfig.cmd" SVC_ENV_PREFIX
SET SERVICE_NAME=%SVC_ENV_PREFIX%COWS.ODIESenderService


REM
REM perform the host check
REM 
CALL "%APP_HOME%"\Scripts\perform-host-check.cmd
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_@SERVICE_ID@.cmd 10 "ERROR: host validation failed"
  EXIT /b 10
)


REM
REM make sure service is not already running, make sure service is defined
REM
CALL "%APP_HOME%"\Scripts\status_odiesender_svc.cmd
IF NOT ERRORLEVEL 1 (
  ECHO ERROR: service %SERVICE_NAME% is already running
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_odiesender_svc.cmd 7 "ERROR: service %SERVICE_NAME% is already running"
  EXIT /b 7
)
IF %ERRORLEVEL% EQU 1 (
  ECHO ERROR: service %SERVICE_NAME% is not installed
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_odiesender_svc.cmd 8 "ERROR: service %SERVICE_NAME% is not installed"
  EXIT /b 8
)


REM
REM perform application configuration before starting
REM
CALL "%APP_HOME%"\Scripts\configure.cmd -NOLOG
IF ERRORLEVEL 1 (
  ECHO ERROR: application configuration failed
  CALL "%APP_HOME%"\Scripts\_logJobMsg.cmd start_odiesender_svc.cmd::configure.cmd 9 "ERROR: application configuration failed"
  EXIT /b 9
)


REM
REM invoke service start script
REM
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_startService.js %SERVICE_NAME%
