<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Scheduled Date &amp; Time : </td>
            <td width="60%">
              <xsl:value-of select="concat(/EventEmail/UCaaSEvent/STRT_TMST, ' CT')" />
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Conference Bridge Number and Pin : </td>
            <td width="60%">
              <xsl:if test="/EventEmail/UCaaSEvent/CNFRC_BRDG_NBR[.!='']">
                <xsl:value-of select="concat(/EventEmail/UCaaSEvent/CNFRC_BRDG_NBR, ' / ', /EventEmail/UCaaSEvent/CNFRC_PIN_NBR)" />
              </xsl:if>
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Online Meeting URL : </td>
            <td width="60%">
              <xsl:if test="/EventEmail/UCaaSEvent/ONLINE_MEETING_ADR[.!='']">
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="/EventEmail/UCaaSEvent/ONLINE_MEETING_ADR" />
                  </xsl:attribute>
                  <xsl:value-of select="/EventEmail/UCaaSEvent/ONLINE_MEETING_ADR" />
                </a>
              </xsl:if>
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event Reviewer : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/RevUserName" />
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Phone Number : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/REV_PHN_NBR" />
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event Assigned MNS Resource : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/AssignUsers" />
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Phone Number : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/AssignPhoneNumbers" />
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event Author : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/REQOR_USER" />
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event Author Contact # : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/REQOR_USER_PHN" />
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Last Note : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/CMNT_TXT" />
            </td>
          </tr>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Link to the Event : </td>
            <td width="60%">
              <a>
                <xsl:attribute name="href">
                  <xsl:choose>
                    <xsl:when test="contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cows.') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/UCaaSEvent/ViewEventURL,'cowsdev2')">
                      <xsl:value-of select="concat(/EventEmail/UCaaSEvent/ViewEventURL, '/', /EventEmail/UCaaSEvent/EVENT_ID)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="concat(/EventEmail/UCaaSEvent/ViewEventURL, '?eid=', /EventEmail/UCaaSEvent/EVENT_ID, '&amp;email=1')" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                Click Here
              </a>
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Add to Calendar : </td>
            <td width="60%">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(/EventEmail/UCaaSEvent/CalenderEntryURL, '?eid=', /EventEmail/UCaaSEvent/EVENT_ID, '&amp;mode=2')" />
                </xsl:attribute>
                Click Here
              </a>
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/EVENT_ID" />
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Event Title : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/EVENT_TITLE_TXT" />
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">H1 : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/H1" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Activity Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Product Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_PROD_TYPE_DES" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Plan Type : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/UCaaS_PLAN_TYPE_DES" />
            </td>
          </tr>
		  <xsl:if test="/EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES[.!='Disconnect']">
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">H6 : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/H6" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">CCD : </td>
				<td width="60%">
				  <xsl:value-of select="concat(/EventEmail/UCaaSEvent/CCD, ' CT')" />
				</td>
			  </tr>
		  </xsl:if>
          <tr />
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">
              Here is a link to the Customer's SOWS folder in Docshare. Click here to access the Scope of Work for this activity Reference the Redesign Number Listed above. :
            </td>
            <td width="60%">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="/EventEmail/UCaaSEvent/CUST_SOW_LOC_TXT" />
                </xsl:attribute>
                Click Here
              </a>
            </td>
          </tr>
          <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Short Description : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/SHRT_DES" />
            </td>
          </tr>
          <tr />
		  <xsl:if test="/EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES[.!='Disconnect']">
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Address : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/STREET_ADR" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/FLR_BLDG_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">City : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/CTY_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">State/Providence : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/STT_PRVN_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Country/Region : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/CTRY_RGN_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Zip : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/ZIP_CD" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">US/International : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/US_INTL_CD" />
				</td>
			  </tr>
			  <xsl:if test="EventEmail/UCaaSRedesign">
			  <tr>
				<td colspan="2" width="100%" style="font-style:italic">
				  <span>---------- ODIE DEVICE/REDESIGN INFORMATION -----------</span>
				</td>
			  </tr>
			  <tr><td colspan="2">
			  <table border="1"  width="95%">
				  <tr bgcolor="#D8D8D8">
					<td style="text-align: left; font-weight: 600">Redesign #</td>
					<td style="text-align: left; font-weight: 600">Redesign Exp. Date</td>
					<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
					<td style="text-align: left; font-weight: 600">Vendor</td>
					<td style="text-align: left; font-weight: 600">Model</td>
				   </tr>
			  <xsl:for-each select="EventEmail/UCaaSRedesign">
				<tr>
				  <td width="15%">
					<xsl:value-of select="RDSN_NBR" />
				  </td>
				  <td width="15%">
					<xsl:value-of select="RDSN_EXP_DT" />
				  </td>
				  <td width="40%">
					<xsl:value-of select="ODIE_DEV_NME" />
				  </td>
				  <td width="15%">
					<xsl:value-of select="MANF_NME" />
				  </td>
				  <td width="15%">
					<xsl:value-of select="DEV_MODEL_NME" />
				  </td>
				</tr>
			  </xsl:for-each>
			  </table></td></tr>
			  </xsl:if>
						<xsl:if test="/EventEmail/UCaaSDevCmpltTbl">
								<tr>
								<td colspan="2" width="100%" style="font-style:italic">
								  <span>---------- Device Completion Table -----------</span>
								</td>
							  </tr>
							  <tr><td colspan="2">
							<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
								  <tr bgcolor="#D8D8D8">
									<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
									<td style="text-align: left; font-weight: 600">ODIE H6</td>
									<td style="text-align: left; font-weight: 600">Activator Completion</td>
								   </tr>
							<xsl:for-each select="/EventEmail/UCaaSDevCmpltTbl">
								<tr>
									<td width="60%">
										<xsl:value-of select="ODIEDevNme" />
									</td>
									<td width="20%">
										<xsl:value-of select="ODIEH6" />
									</td>
									<td width="20%">
										<xsl:value-of select="ActvCmpltCd" />
									</td>
								</tr>
							</xsl:for-each>
							</table></td></tr>
							</xsl:if>
			  <xsl:if test="EventEmail/UCaaSCPEDev">
			  <tr>
				<td colspan="2" width="100%" style="font-style:italic">
				  <span>---------- CPE DEVICE INFORMATION -----------</span>
				</td>
			  </tr>
			  <tr><td colspan="2">
			  <table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
				  <tr bgcolor="#D8D8D8">
				  <td style="text-align: left; font-weight: 600">CPE Order #</td>
				  <td style="text-align: left; font-weight: 600">Device ID</td>
				  <td style="text-align: left; font-weight: 600">Assoc. H6</td>
				  <td style="text-align: left; font-weight: 600">Requisition #</td>
				  <td style="text-align: left; font-weight: 600">ODIE Device Name</td>
				   </tr>
			  <xsl:for-each select="EventEmail/UCaaSCPEDev">
				<tr>
				  <td width="10%">
					<xsl:value-of select="CPE_ORDR_NBR" />
				  </td>
				  <td width="15%">
					<xsl:value-of select="DEVICE_ID" />
				  </td>
				  <td width="15%">
					<xsl:value-of select="ASSOC_H6" />
				  </td>
				  <td width="20%">
					<xsl:value-of select="REQSTN_NBR" />
				  </td>
				  <td width="40%">
					<xsl:value-of select="ODIE_DEV_NME" />
				  </td>
				</tr>
			  </xsl:for-each>
			  </table></td></tr>
			  </xsl:if>
			  <xsl:if test="/EventEmail/UCaaSDevMgmtTbl">
								<tr>
								<td colspan="2" width="100%" style="font-style:italic">
								  <span>---------- Device Based MNS Management Table -----------</span>
								</td>
							  </tr>
							  <tr><td colspan="2">
							<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
								  <tr bgcolor="#D8D8D8">
									<td style="text-align: left; font-weight: 600">MNS Order #</td>
									<td style="text-align: left; font-weight: 600">Device ID</td>
									<td style="text-align: left; font-weight: 600">Service Tier</td>
									<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
								   </tr>
							<xsl:for-each select="/EventEmail/UCaaSDevMgmtTbl">
								<tr>
									<td width="15%">
										<xsl:value-of select="MNSOrdrNbr" />
									</td>
									<td width="25%">
										<xsl:value-of select="DeviceID" />
									</td>
									<td width="15%">
										<xsl:value-of select="ServiceTier" />
									</td>
									<td width="45%">
										<xsl:value-of select="ODIEDevNme" />
									</td>
								</tr>
							</xsl:for-each>
							</table></td></tr>
							</xsl:if>
							<xsl:if test="EventEmail/UCaaSBilling">
			  <tr>
				<td colspan="2" width="100%" style="font-style:italic">
				  <span>---------- BILLING INFORMATION -----------</span>
				</td>
			  </tr>
			  <tr><td colspan="2">
			  <table border="1"  width="95%">
				  <tr bgcolor="#D8D8D8">
				  <xsl:choose>
					<xsl:when test="contains(EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES, 'Change')">
						<td style="text-align: left; font-weight: 600">Increase Quantity</td>
						<td style="text-align: left; font-weight: 600">Decrease Quantity</td>
					</xsl:when>
					<xsl:otherwise>
						<td style="text-align: left; font-weight: 600">Quantity</td>
					</xsl:otherwise>
				  </xsl:choose>
				  <td style="text-align: left; font-weight: 600">Description</td>
				  <td style="text-align: left; font-weight: 600">BIC</td>
				  <td style="text-align: left; font-weight: 600">MRC/NRC</td>
				  </tr>
			  <xsl:for-each select="EventEmail/UCaaSBilling">
			  <tr>
				<xsl:choose>
					<xsl:when test="contains(//EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES, 'Change')">
						<td>
							<xsl:value-of select="INC_QTY" />
						</td>
						<td>
							<xsl:value-of select="DCR_QTY" />
								   </td>
					</xsl:when>
					<xsl:otherwise>
						<td>
							<xsl:value-of select="INC_QTY" />
						</td>
					</xsl:otherwise>
				</xsl:choose>
				  <td>
					<xsl:value-of select="UCaaS_BILL_ACTY_DES" />
				  </td>
				  <td>
					<xsl:value-of select="UCaaS_BIC_TYPE" />
				  </td>
				  <td>
					<xsl:value-of select="MRC_NRC_CD" />
				  </td>
				</tr>
			  </xsl:for-each>
			  </table></td></tr>
			  </xsl:if>
			  <tr />
			  <xsl:if test="/EventEmail/UCaaSEvent/CPE_DSPCH_EMAIL_ADR[.!='']">
				<tr>
				  <td width="40%" style="text-align: left; font-weight: 600">CPE Dispatch Email Addr : </td>
				  <td width="60%">
					<xsl:value-of select="/EventEmail/UCaaSEvent/CPE_DSPCH_EMAIL_ADR" />
				  </td>
				</tr>
			  </xsl:if>
			  <xsl:if test="/EventEmail/UCaaSEvent/CPE_DSPCH_CMNT_TXT[.!='']">
				<tr>
				  <td width="40%" style="text-align: left; font-weight: 600">CPE Dispatch Comments : </td>
				  <td width="60%">
					<xsl:value-of select="/EventEmail/UCaaSEvent/CPE_DSPCH_CMNT_TXT" />
				  </td>
				</tr>
			  </xsl:if>
			  <tr />
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Install Site POC : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/INSTL_SITE_POC_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/INSTL_SITE_POC_PHN_NBR" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Cell : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/INSTL_SITE_POC_CELL_PHN_NBR" />
				</td>
			  </tr>
			  <tr />
			  <tr />
			  <tr />
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/SRVC_ASSRN_POC_NME" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Phone : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/SRVC_ASSRN_POC_PHN_NBR" />
				</td>
			  </tr>
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Cell : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/SRVC_ASSRN_POC_CELL_NBR" />
				</td>
			  </tr>
		  </xsl:if>
		  <xsl:if test="/EventEmail/UCaaSEvent/UCaaS_ACTY_TYPE_DES[.='Disconnect']">
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Disconnect of Management Only ? : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/DISC_MGMT_CD" />
            </td>
          </tr>
		  <tr>
            <td width="40%" style="text-align: left; font-weight: 600">Total Customer Disconnect ? : </td>
            <td width="60%">
              <xsl:value-of select="/EventEmail/UCaaSEvent/FULL_CUST_DISC_CD" />
            </td>
          </tr>
		  <xsl:if test="/EventEmail/UCaaSEvent/FULL_CUST_DISC_CD[.='Full Disconnect']">
			  <tr>
				<td width="40%" style="text-align: left; font-weight: 600">Reason for Total Customer Disconnect : </td>
				<td width="60%">
				  <xsl:value-of select="/EventEmail/UCaaSEvent/FULL_CUST_DISC_REAS_TXT" />
				</td>
			  </tr>
		  </xsl:if>
		  <xsl:if test="EventEmail/UCaaSDiscODIEDev">
			  <tr>
				<td colspan="2" width="100%" style="font-style:italic">
				  <span>---------- ODIE DEVICE/REDESIGN INFORMATION -----------</span>
				</td>
			  </tr>
			  <tr><td colspan="2">
			  <table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
				  <tr bgcolor="#D8D8D8">
					<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
					<td style="text-align: left; font-weight: 600">H5/H6</td>
					<td style="text-align: left; font-weight: 600">Site ID</td>
					<td style="text-align: left; font-weight: 600">Device ID</td>
					<td style="text-align: left; font-weight: 600">Vendor</td>
					<td style="text-align: left; font-weight: 600">Model</td>
					<td style="text-align: left; font-weight: 600">Serial #</td>
					<td style="text-align: left; font-weight: 600">Third Party Contract</td>
				   </tr>
			  <xsl:for-each select="EventEmail/UCaaSDiscODIEDev">
				<tr>
				  <td width="30%">
					<xsl:value-of select="ODIE_DEV_NME" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="H5_H6" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="SITE_ID" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="DEVICE_ID" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="MANF_NME" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="DEV_MODEL_NME" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="SERIAL_NBR" />
				  </td>
				  <td width="10%">
					<xsl:value-of select="THRD_PARTY_CTRCT" />
				  </td>
				</tr>
			  </xsl:for-each>
			  </table></td></tr>
			  </xsl:if>
		  </xsl:if>
          <tr />
          <tr />
          <tr />
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>