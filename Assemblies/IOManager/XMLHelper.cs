﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace IOManager
{
    public class XMLHelper
    {
        
        public string createHTML(DataTable dt, string sUserExtInt)
        {
            string str = string.Empty;

            DataRow dr = null;
            dr = dt.Rows[0];

            XElement xe = new XElement(string.Empty);

            str = xe.ToString();

            return transformXMLtoHTML(str, "xsltfile", 1);
        }

        public string transformXMLtoHTML(string sVal, string sTrgt, int iFlg)
        {
            string trgtfile = ConfigurationManager.AppSettings[sTrgt];

            //Dim sV As String = String.Empty
            //This is our XSL template.
            XslCompiledTransform xslDoc = new XslCompiledTransform();
            var settings = new XsltSettings();
            settings.EnableScript = true;
            if ((iFlg == 1))
            {
                string strPath = System.Web.HttpContext.Current.Server.MapPath(trgtfile);
                xslDoc.Load(strPath, settings, new XmlUrlResolver());
            }
            else
            {
                xslDoc.Load(trgtfile, settings, new XmlUrlResolver());
            }

            XsltArgumentList xslArgs = new XsltArgumentList();
            StringWriter writer = new StringWriter();
            //Dim writer As XmlWriter = XmlWriter.Create(sV, xslDoc.OutputSettings)

            XmlReader oXmlReader = XmlReader.Create(new StringReader(sVal));
            XPathDocument oXmlDoc = new XPathDocument(oXmlReader);
            XPathNavigator oXmlNav = oXmlDoc.CreateNavigator();

            // Merge XSLT document with data XML document
            //(writer will hold resulted transformation).
            xslDoc.Transform(oXmlNav, xslArgs, writer);

            return writer.ToString();
        }
    }

    public static class StringBuilderExtns
    {
        public static StringBuilder AppendNoDup(this StringBuilder strb, string str)
        {
            return new StringBuilder(strb.ToString().Contains(str) ? strb.ToString() : strb.Append(str).ToString());
        }

        public static StringBuilder AppendNoDup(this StringBuilder strb, StringBuilder str)
        {
            return new StringBuilder(strb.ToString().Contains(str.ToString()) ? strb.ToString() : strb.Append(str).ToString());
        }
    }

    public static class StringExtns
    {
        /// <summary>
        /// Writes this XML to string while allowing invalid XML chars to either be
        /// simply removed during the write process, or else encoded into entities, 
        /// instead of having an exception occur, as the standard XmlWriter.Create 
        /// XmlWriter does (which is the default writer used by XElement).
        /// </summary>
        /// <param name="xml">XElement.</param>
        /// <param name="deleteInvalidChars">True to have any invalid chars deleted, else they will be entity encoded.</param>
        /// <param name="indent">Indent setting.</param>
        /// <param name="indentChar">Indent char (leave null to use default)</param>
        public static string ToStringIgnoreInvalidChars(this XElement xml, bool deleteInvalidChars = true, bool indent = true, char? indentChar = null)
        {
            if (xml == null) return null;

            StringWriter swriter = new StringWriter();
            using (XmlTextWriterIgnoreInvalidChars writer = new XmlTextWriterIgnoreInvalidChars(swriter, deleteInvalidChars))
            {

                // -- settings --
                // unfortunately writer.Settings cannot be set, is null, so we can't specify: bool newLineOnAttributes, bool omitXmlDeclaration
                writer.Formatting = indent ? Formatting.Indented : Formatting.None;

                if (indentChar != null)
                    writer.IndentChar = (char)indentChar;

                // -- write --
                xml.WriteTo(writer);
            }

            return swriter.ToString();
        }

        public static string ToQueryString(this NameValueCollection collection)
        {
            var array = (from key in collection.AllKeys
                         from value in collection.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value))).ToArray();
            return "?" + string.Join("&", array);
        }

        public static string RemoveInvalidXmlChars(this string text)
        {
            char[] validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }

        public static string TidyUpEmailAddr(this string sEmail)
        {
            StringBuilder sbout = new StringBuilder();
            sEmail = sEmail.Replace(';', ',');
            string[] Emails = sEmail.Split(new char[] { ',' });
            foreach (string s in Emails)
            {
                if (s.Trim().Length > 0)
                {
                    if (((!(sbout.ToString().IndexOf(s.Trim(), 0, StringComparison.CurrentCultureIgnoreCase) != -1)) || (sbout.Length <= 0))
                        && (s.Trim().Contains("@")) && (s.Trim().Contains(".")))
                    {
                        if (sbout.Length <= 0)
                            sbout.Append(s.Trim());
                        else
                            sbout.Append(',').Append(s.Trim());
                    }
                }
            }
            return sbout.ToString();
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            if (source == null) return false;
            return source.IndexOf(toCheck, comp) >= 0;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        public static string Serialize(object dataToSerialize)
        {
            if (dataToSerialize == null) return null;

            using (StringWriter stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(dataToSerialize.GetType());
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
        }

        public static T Deserialize<T>(string xmlText)
        {
            if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

            using (StringReader stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }
    }

    public static class DateExtensions
    {
        public static bool IsBusinessDay(this DateTime date)
        {
            return
                date.DayOfWeek != DayOfWeek.Saturday &&
                date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static bool IsDateTime(this string txtDate)
        {
            DateTime dtOut;
            return DateTime.TryParse(txtDate, out dtOut);
        }

        public static int GetNthBusinessDayOfMonth(this DateTime fromDate, int nTh)
        {
            int noOfBusDays = 0;
            for(int i=1; i<=31; i++)
            {
                DateTime nwdt = new DateTime(fromDate.Year, fromDate.Month, i, 0, 0, 0);
                if ((nwdt.ToShortDateString().IsDateTime()) && (nwdt.IsBusinessDay())) noOfBusDays++;
                if (noOfBusDays == nTh)
                    return i;
            }
            return -1;
        }

        public static int BusinessDaysTo(this DateTime fromDate, DateTime toDate,
                                         int maxAllowed = 0)
        {
            int ret = 0;
            DateTime dt = fromDate;
            while (dt < toDate)
            {
                if (dt.IsBusinessDay()) ret++;
                if (maxAllowed > 0 && ret == maxAllowed) return ret;
                dt = dt.AddDays(1);
            }
            return ret;
        }
    }

    public class XmlTextWriterIgnoreInvalidChars : XmlTextWriter
    {
        public bool DeleteInvalidChars { get; set; }

        public XmlTextWriterIgnoreInvalidChars(TextWriter w, bool deleteInvalidChars = true) : base(w)
        {
            DeleteInvalidChars = deleteInvalidChars;
        }

        public override void WriteString(string text)
        {
            if (text != null && DeleteInvalidChars)
                text = ToValidXmlCharactersString(text);
            base.WriteString(text);
        }

        /// <summary>
        /// Determines if any invalid XML 1.0 characters exist within the string,
        /// and if so it returns a new string with the invalid chars removed, else 
        /// the same string is returned (with no wasted StringBuilder allocated, etc).
        /// </summary>
        /// <param name="s">Xml string.</param>
        /// <param name="startIndex">The index to begin checking at.</param>
        public string ToValidXmlCharactersString(string s, int startIndex = 0)
        {
            int firstInvalidChar = IndexOfFirstInvalidXMLChar(s, startIndex);
            if (firstInvalidChar < 0)
                return s;

            startIndex = firstInvalidChar;

            int len = s.Length;
            var sb = new StringBuilder(len);

            if (startIndex > 0)
                sb.Append(s, 0, startIndex);

            for (int i = startIndex; i < len; i++)
                if (System.Xml.XmlConvert.IsXmlChar(s[i]))
                    sb.Append(s[i]);

            return sb.ToString();
        }

        /// <summary>
        /// Gets the index of the first invalid XML 1.0 character in this string, else returns -1.
        /// </summary>
        /// <param name="s">Xml string.</param>
        /// <param name="startIndex">Start index.</param>
        public int IndexOfFirstInvalidXMLChar(string s, int startIndex = 0)
        {
            if (s != null && s.Length > 0 && startIndex < s.Length)
            {

                if (startIndex < 0) startIndex = 0;
                int len = s.Length;

                for (int i = startIndex; i < len; i++)
                    if (!System.Xml.XmlConvert.IsXmlChar(s[i]))
                        return i;
            }
            return -1;
        }

        /// <summary>
        /// Indicates whether a given character is valid according to the XML 1.0 spec.
        /// </summary>
        //public static bool IsLegalXmlChar(char c)
        //{
        //    if (c > 31 && c <= 55295)
        //        return true;
        //    if (c < 32)
        //        return c == 9 || c == 10 || c == 13;
        //    return (c >= 57344 && c <= 65533) || c > 65535;
        //    // final comparison is useful only for integral comparison, if char c -> int c, useful for utf-32 I suppose
        //    //c <= 1114111 */ // impossible to get a code point bigger than 1114111 because Char.ConvertToUtf32 would have thrown an exception
        //}
    }
}