﻿using System;
using System.Xml.Serialization;

namespace COWS.ODIESenderService
{
    #region "ODIECustomerNameRequest"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "ODIECustomerNameRequest")]
    [XmlRootAttribute(Namespace = "ODIECustomerNameRequest", IsNullable = false)]
    public partial class ODIECustomerNameRequest
    {
        private string tranID;
        private string h1;
        private DateTime requestDateTime;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string H1
        {
            get { return this.h1; }
            set { this.h1 = value; }
        }

        public DateTime RequestDateTime
        {
            get { return this.requestDateTime; }
            set { this.requestDateTime = value; }
        }
    }

    #endregion "ODIECustomerNameRequest"

    #region "ODIECustomerInfoRequest"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "ODIECustomerInfoRequest")]
    [XmlRootAttribute(Namespace = "ODIECustomerInfoRequest", IsNullable = false)]
    public partial class ODIECustomerInfoRequest
    {
        private string tranID;
        private string custID;
        private DateTime requestDateTime;
        private ODIECustomerInfoRequestCategoryType[] categoryField;
        private ODIECustomerInfoRequestDeviceFilter[] deviceFilter;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string CustID
        {
            get { return this.custID; }
            set { this.custID = value; }
        }

        [XmlIgnore]
        public string CustomerName { get; set; }

        [XmlElement("CustomerName")]
        public System.Xml.XmlCDataSection CustomerNameCDATA
        {
            get
            {
                return new System.Xml.XmlDocument().CreateCDataSection(CustomerName);
            }
            set
            {
                CustomerName = value.Value;
            }
        }

        public DateTime RequestDateTime
        {
            get { return this.requestDateTime; }
            set { this.requestDateTime = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Type", typeof(ODIECustomerInfoRequestCategoryType))]
        public ODIECustomerInfoRequestCategoryType[] Category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Filter", typeof(ODIECustomerInfoRequestDeviceFilter))]
        public ODIECustomerInfoRequestDeviceFilter[] DeviceFilter
        {
            get
            {
                return this.deviceFilter;
            }
            set
            {
                this.deviceFilter = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "ODIECustomerInfoRequest")]
    public partial class ODIECustomerInfoRequestCategoryType
    {
        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "ODIECustomerInfoRequest")]
    public partial class ODIECustomerInfoRequestDeviceFilter
    {
        private string value;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }
    }

    #endregion "ODIECustomerInfoRequest"

    #region "ODIEDiscoRequest"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "ODIEDiscoRequest")]
    [XmlRootAttribute(Namespace = "ODIEDiscoRequest", IsNullable = false)]
    public partial class ODIEDiscoRequest
    {
        private string tranID;
        private DateTime requestDateTime;
        private Devices[] deviceList;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public DateTime Req_Dt
        {
            get { return this.requestDateTime; }
            set { this.requestDateTime = value; }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Devices", IsNullable = false)]
        public Devices[] DeviceList
        {
            get { return this.deviceList; }
            set { this.deviceList = value; }
        }
    }

    public partial class Devices
    {
        private string device_Name;
        private string dev_Req_ID;

        public string Device_Name
        {
            get { return this.device_Name; }
            set { this.device_Name = value; }
        }

        public string Dev_Req_ID
        {
            get { return this.dev_Req_ID; }
            set { this.dev_Req_ID = value; }
        }
    }

    #endregion "ODIEDiscoRequest"

    #region "ODIEShortNameValidationRequest"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "ODIEShortNameValidationRequest")]
    [XmlRootAttribute(Namespace = "ODIEShortNameValidationRequest", IsNullable = false)]
    public partial class ODIEShortNameValidationRequest
    {
        private string tranID;
        private string h1;
        private string customerName;
        private DateTime requestDateTime;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string H1
        {
            get { return this.h1; }
            set { this.h1 = value; }
        }

        public string CustomerName
        {
            get { return this.customerName; }
            set { this.customerName = value; }
        }

        public DateTime RequestDateTime
        {
            get { return this.requestDateTime; }
            set { this.requestDateTime = value; }
        }
    }

    #endregion "ODIEShortNameValidationRequest"
}