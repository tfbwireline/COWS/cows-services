﻿namespace AppConstants
{
    public enum EMPLSActyType
    {
        CarrierEthernet = 20,
        CarrierEthernetChange = 21,
        SpecialProject = 26
    }
}