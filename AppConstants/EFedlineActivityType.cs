﻿namespace AppConstants
{
    public enum EFedlineActivityType
    {
        Install,
        Disconnect,
        HeadendInstall,
        HeadendMAC,
        HeadendDisconnect,
        HeadendCreate,
        Refresh,
    }
}