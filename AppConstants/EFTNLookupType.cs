﻿namespace AppConstants
{
    public enum EFTNLookupType
    {
        AD = 1,
        NGVN,
        MPLS,
        SprintLink,
        MDS,
        SIPT = 10
    }
}