﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace FSAHelper
{
    public class FSAServiceIO
    {
        public int _OrderID;
        public string _OrderFTN;
        public Order _Order = new Order();
        public FSA_ORDR_DB _FSA_ORDR = new FSA_ORDR_DB();

        public string createOrderResponse(string sError)
        {
            string sXML = string.Empty;
            OrderResponse _Response = new OrderResponse();

            _Response.TimeStamp = String.Format("{0:s}", DateTime.Now);
            if (_OrderID != 0)
            {
                _Response.OrderID = _OrderID.ToString();
            }
            else
            {
                try
                {
                    _Response.OrderID = FixNull(_Order.OrderInformation.OrderID).ToString();
                }
                catch
                {
                    _Response.OrderID = "0";
                }
            }
            _Response.ResponseTypeCode = sError == string.Empty ? ResponseType.ACKNOWLEDGMENT : ResponseType.NEGATIVEACKNOWLEDGMENT;
            _Response.CompletionDate = string.Empty;
            _Response.NegativeAcknowledgmentText = sError;

            XmlSerializer _Serializer = new XmlSerializer(typeof(OrderResponse));
            sXML = SerializeObject<OrderResponse>(Encoding.UTF8, _Response);
            return sXML;
        }

        public string CreateOrderResponse(string _FTN, ResponseType _ResponseType, string sError)
        {
            string sXML = string.Empty;
            OrderResponse _Response = new OrderResponse();

            _Response.TimeStamp = String.Format("{0:s}", DateTime.Now);
            _Response.OrderID = _FTN;
            _Response.ResponseTypeCode = _ResponseType;

            string _CmplDT = String.Format("{0:s}", DateTime.Now);
            _CmplDT = _CmplDT.Substring(0, 10);

            switch (_ResponseType)
            {
                case ResponseType.NEGATIVEACKNOWLEDGMENT:
                    _Response.NegativeAcknowledgmentText = sError;
                    break;

                case ResponseType.ACTIVATION:
                    _Response.CompletionDate = _CmplDT;
                    break;

                case ResponseType.CHANGECOMPLETE:
                    _Response.CompletionDate = _CmplDT;
                    break;

                case ResponseType.DISCONNECTCOMPLETE:
                    _Response.CompletionDate = _CmplDT;
                    break;

                case ResponseType.REJECT:
                    _Response.RejectText = sError;
                    break;
            }

            XmlSerializer _Serializer = new XmlSerializer(typeof(OrderResponse));
            sXML = SerializeObject<OrderResponse>(Encoding.UTF8, _Response);
            return sXML;
        }

        public string ProcessXML(string sXML)
        {
            string sError = ProcessXMLData(sXML);

            if (sError != string.Empty)
            {
                _FSA_ORDR.ORDR_ID = _FSA_ORDR.ORDR_ID == 0 ? _OrderID : _FSA_ORDR.ORDR_ID;
                _FSA_ORDR.DeletePartialFSAOrder();
            }
            return sError;
        }

        public string ProcessXMLData(string sXML)
        {
            string sError = string.Empty;
            try
            {
                _Order = (Order)DeserializeXML(sXML);
            }
            catch (Exception Ex)
            {
                sError = Ex.InnerException.Message.ToString();
                return sError;
            }

            sError = ValidateAndReadOrderInfo();
            if (sError == string.Empty)
            {
                _FSA_ORDR.ORDR_ID = _FSA_ORDR.GetFSAOrderID();
                if (_FSA_ORDR.ORDR_ID == 0)
                {
                    sError = "M5 # and OrderAction Type combination already exists in the COWS database.";
                    return sError;
                }
                else if (_FSA_ORDR.ORDR_ID == -1)
                {
                    sError = "DELETE is valid only for pre-submitted orders.";
                    return sError;
                }
            }
            else
            {
                return sError;
            }
            _OrderID = _FSA_ORDR.ORDR_ID;
            _OrderFTN = _FSA_ORDR.FTN;

            sError = Load_FSA_ORDR();
            if (sError != string.Empty)
            {
                return sError;
            }

            if (_Order.CustomerInfo != null)
            {
                sError = Load_FSA_ORDR_CUST();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            if (_Order.AccountTeam != null)
            {
                sError = Load_Account_Team_Info();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            if (_Order.InstallInfo != null)
            {
                if (_Order.InstallInfo.MDSInstallationInformation != null)
                {
                    sError = Load_MDS_Install_Info();
                    if (sError != string.Empty)
                    {
                        return sError;
                    }
                }

                if (_Order.InstallInfo.CPEInstallationInformation != null)
                {
                    if (_Order.InstallInfo.CPEInstallationInformation.CPELineItems != null)
                    {
                        sError = Load_CPE_Line_Item_Info();
                        if (sError != string.Empty)
                        {
                            return sError;
                        }
                    }
                }

                if (_Order.InstallInfo.ValueAddedServices != null)
                {
                    sError = Load_VAS_Info();
                    if (sError != string.Empty)
                    {
                        return sError;
                    }
                }

                if (_Order.InstallInfo.BillingLineItems != null)
                {
                    sError = Load_Install_Billing_Info();
                    if (sError != string.Empty)
                    {
                        return sError;
                    }
                }

                if (_Order.InstallInfo.TransportInstallInformation != null)
                {
                    if (_Order.InstallInfo.TransportInstallInformation.SupportInformation != null)
                    {
                        sError = Load_Transport_Install_Support_Info();
                        if (sError != string.Empty)
                        {
                            return sError;
                        }
                    }

                    if (_Order.InstallInfo.TransportInstallInformation.Transport != null)
                    {
                        sError = Load_Transport_Install_Transport_Info();
                        if (sError != string.Empty)
                        {
                            return sError;
                        }
                    }

                    if (_Order.InstallInfo.TransportInstallInformation.Port != null)
                    {
                        sError = Load_Transport_Install_Port_Info();
                        if (sError != string.Empty)
                        {
                            return sError;
                        }
                    }
                }

                if (_Order.InstallInfo.SIPTrunkInstallInformation != null)
                {
                    sError = Load_SIP_Trunk_Group_Info(_Order.InstallInfo.SIPTrunkInstallInformation.SIPTrunkGroups, true);
                    if (sError != string.Empty)
                    {
                        return sError;
                    }
                }
            }

            if (_Order.BillingOnlyInformation != null)
            {
                sError = Load_Billing_Info();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            if (_Order.DisconnectInfo != null)
            {
                sError = Load_Disconnect_Info();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            if (_Order.ChangeInformation != null)
            {
                sError = Load_Change_Info();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            if (_Order.Notes != null)
            {
                sError = Load_Notes_Info();
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            Load_Initial_Tasks();

            return sError;
        }

        #region "Read & Load Main Forms"

        private string Load_FSA_ORDR()
        {
            string sError = string.Empty;
            if (_Order.InstallInfo != null)
            {
                sError += ReadOrderMDSCPEInstallInfo();

                if (_Order.InstallInfo.TransportInstallInformation != null)
                {
                    sError += ReadOrderTransportInstallInfo();
                }
            }

            ///////////////////////////////////////////////////////////////////////
            //Disconnect Specific Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.DisconnectInfo != null)
            {
                try
                {
                    _FSA_ORDR.DISC_REAS_CD = FixNull(_Order.DisconnectInfo.ReasonCode).ToString();
                    _FSA_ORDR.DISC_CNCL_BEF_STRT_REAS_CD = FixNull(_Order.DisconnectInfo.CancelBeforeStartReasonCode).ToString();
                    _FSA_ORDR.DISC_CNCL_BEF_STRT_REAS_DETL_TXT = FixNull(_Order.DisconnectInfo.CancelBeforeStartReasonDetail).ToString();
                }
                catch
                {
                    sError += "Error reading Order.DisconnectInfo Data.";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            //Change Specific Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.ChangeInformation != null)
            {
                try
                {
                    _FSA_ORDR.ORDR_CHNG_DES = FixNull(_Order.ChangeInformation.ChangeDescription).ToString();
                    _FSA_ORDR.CXR_SRVC_ID = FixNull(_Order.ChangeInformation.CarrierServiceID).ToString();
                    _FSA_ORDR.CXR_CKT_ID = FixNull(_Order.ChangeInformation.CarrierCircuitID).ToString();
                    _FSA_ORDR.NW_USER_ADR = FixNull(_Order.ChangeInformation.NetworkUserAddress).ToString();
                    _FSA_ORDR.PORT_RT_TYPE_CD = FixNull(_Order.ChangeInformation.PortRateTypeCode.ToString()).ToString();
                    try
                    {
                        _FSA_ORDR.FSA_ORGNL_INSTL_ORDR_ID = Convert.ToInt32(FixNull(_Order.ChangeInformation.OriginalInstallOrderID).ToString());
                    }
                    catch
                    { }
                }
                catch
                {
                    sError += "Error reading Order.ChangeInfo Data.";
                }
            }

            if (sError == string.Empty)
            {
                if (_FSA_ORDR.InsertFSAOrder() != -1)
                {
                    sError += "Error insert FSA_ORDR related information into the COWS database.";
                }
            }

            return sError;
        }

        private string Load_FSA_ORDR_CUST()
        {
            string sError = string.Empty;
            int iCisHLvlCnt = 0;

            try
            {
                iCisHLvlCnt = _Order.CustomerInfo.Length;
            }
            catch
            {
                return sError;
            }

            string _ModuleID = "CIS";
            OrderCISHierarchyInfo _CIS = new OrderCISHierarchyInfo();

            for (int i = 0; i < iCisHLvlCnt; i++)
            {
                _CIS = _Order.CustomerInfo[i];
                string _CISLevelType = FixNull(_CIS.LevelType.ToString());

                if (_CISLevelType != string.Empty)
                {
                    ORDR_ADR_DB _ORDR_ADR = new ORDR_ADR_DB(_OrderID, _ModuleID);
                    ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);
                    FSA_ORDR_CUST_DB _FSA_ORDR_CUST = new FSA_ORDR_CUST_DB(_OrderID);

                    switch (_CISLevelType)
                    {
                        case "ES":
                            _CISLevelType = "H5";
                            break;

                        case "GP":
                            _CISLevelType = "H6";
                            break;

                        case "IP":
                            _CISLevelType = "H6";
                            break;

                        case "CP":
                            _CISLevelType = "H6";
                            break;

                        default:
                            break;
                    }
                    // ***********************Order Contact & Address Info*******************************
                    if (_CIS.ContactInfo != null)
                    {
                        _ORDR_CNTCT = Load_ORDR_CNTCT(_ORDR_CNTCT, _CIS.ContactInfo);
                        _ORDR_ADR = Load_ORDR_ADR(_ORDR_ADR, _CIS.ContactInfo);
                        _ORDR_CNTCT.CIS_LVL_TYPE = _CISLevelType;
                        _ORDR_ADR.CIS_LVL_TYPE = _CISLevelType;
                    }

                    // ***********************Order Customer Info*******************************
                    _FSA_ORDR_CUST.CIS_LVL_TYPE = _CISLevelType;
                    try
                    {
                        _FSA_ORDR_CUST.CUST_ID = Convert.ToInt32(FixNull(_CIS.CustomerID).ToString());
                    }
                    catch
                    {
                        sError = "Error readin the CustomerID for CIS Level Type " + _CISLevelType;
                        break;
                    }
                    _FSA_ORDR_CUST.CUST_NME = FixNull(_CIS.CustomerName).ToString();
                    _FSA_ORDR_CUST.BR_CD = FixNull(_CIS.BranchCode).ToString();
                    _FSA_ORDR_CUST.CURR_BILL_CYC_CD = FixNull(_CIS.CurrentBillCycleCode).ToString();
                    _FSA_ORDR_CUST.FUT_BILL_CYC_CD = FixNull(_CIS.FutureBillCycleCode).ToString();
                    _FSA_ORDR_CUST.SRVC_SUB_TYPE_ID = FixNull(_CIS.ServiceSubType.ToString());
                    _FSA_ORDR_CUST.SOI_CD = FixNull(_CIS.SalesOfficeIDCode).ToString();
                    _FSA_ORDR_CUST.SALS_PERSN_PRIM_CID = FixNull(_CIS.SalesPersonPrimaryCID).ToString();
                    _FSA_ORDR_CUST.SALS_PERSN_SCNDY_CID = FixNull(_CIS.SalesPersonSecondaryCID).ToString();
                    _FSA_ORDR_CUST.CLLI_CD = FixNull(_CIS.CLLICode).ToString();

                    int j = _FSA_ORDR_CUST.InsertFSAOrderCustInfo(_ORDR_CNTCT, _ORDR_ADR);
                    if (j == 0)
                    {
                        sError = "Error inserting CustomerInfo Data for CIS Level Type " + _CISLevelType;
                        break;
                    }

                    ORDR_ADR_DB _ADR = new ORDR_ADR_DB(_OrderID, _ModuleID);
                    ORDR_CNTCT_DB _CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);

                    if (_CIS.Address != null)
                    {
                        switch (FixNull(_CIS.Address.Type.ToString()))
                        {
                            case "Billing":
                                _ADR.ADR_TYPE_ID = (Byte)OrderAddressType.Billing;
                                break;

                            case "Site":
                                _ADR.ADR_TYPE_ID = (Byte)OrderAddressType.Site;
                                break;

                            default:
                                _ADR.ADR_TYPE_ID = 17;
                                break;
                        }
                        _ADR.CIS_LVL_TYPE = _CISLevelType;
                        _ADR.STREET_ADR_1 = FixNull(_CIS.Address.AddressLine1).ToString();
                        _ADR.STREET_ADR_2 = FixNull(_CIS.Address.AddressLine2).ToString();
                        _ADR.CTY_NME = FixNull(_CIS.Address.City).ToString();
                        _ADR.PRVN_NME = FixNull(_CIS.Address.ProvinceMuncipality).ToString();
                        _ADR.STT_CD = FixNull(_CIS.Address.StateCode).ToString();
                        _ADR.ZIP_PSTL_CD = FixNull(_CIS.Address.ZIPCode).ToString()
                                                + FixNull(_CIS.Address.ZIPCodePlusFour).ToString()
                                                + FixNull(_CIS.Address.PostalCode).ToString();
                        _ADR.CTRY_CD = FixNull(_CIS.Address.CountryCode).ToString();
                        _ADR.HIER_LVL_CD = true;

                        int iA = _ADR.InsertOrderAddressInfo();
                        if (iA == 0)
                        {
                            sError = "Error inserting Address section of the CustomerInfo Data for CIS Level Type " + _CISLevelType;
                            break;
                        }
                    }

                    if (_CIS.PhoneInfo != null)
                    {
                        _CNTCT = Load_ORDR_CNTCT_PHN(_CNTCT, _CIS.PhoneInfo);
                        _CNTCT.CNTCT_TYPE_ID = 17;
                        _CNTCT.ROLE_ID = 17;
                        _CNTCT.CIS_LVL_TYPE = _CISLevelType;

                        int iP = _CNTCT.InsertOrderContactInfo();
                        if (iP == 0)
                        {
                            sError = "Error inserting PhoneInfo section of the CustomerInfo Data for CIS Level Type " + _CISLevelType;
                            break;
                        }
                    }
                }
            }
            return sError;
        }

        private string Load_Account_Team_Info()
        {
            string sError = string.Empty;
            int iTeamCnt = 0;

            try
            {
                iTeamCnt = _Order.AccountTeam.Length;
            }
            catch
            {
                return sError;
            }

            string _ModuleID = "ATM";
            OrderAccountTeamMembers _AccountTeam = new OrderAccountTeamMembers();

            for (int i = 0; i < iTeamCnt; i++)
            {
                _AccountTeam = _Order.AccountTeam[i];
                ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);

                switch (FixNull(_AccountTeam.RoleCode.ToString()))
                {
                    case "SalespersonPrimary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SalespersonPrimary;
                        break;

                    case "SISPrimary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SISPrimary;
                        break;

                    case "SDEPrimary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SDEPrimary;
                        break;

                    case "TACPrimary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.TACPrimary;
                        break;

                    case "ATSM":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.ATSM;
                        break;

                    case "SalespersonSecondary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SalespersonSecondary;
                        break;

                    case "SDESecondary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SDESecondary;
                        break;

                    case "SISSecondary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SISSecondary;
                        break;

                    case "TACSecondary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.TACSecondary;
                        break;

                    case "SSEPrimary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SSEPrimary;
                        break;

                    case "SSESecondary":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.SSESecondary;
                        break;

                    case "AEM":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.AEM;
                        break;

                    case "IE":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.IE;
                        break;

                    case "QAS":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.QAS;
                        break;

                    case "Aggregate":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.Aggregate;
                        break;

                    case "OMG":
                        _ORDR_CNTCT.ROLE_ID = (Byte)OrderAccountTeamMembersRoleCode.OMG;
                        break;
                }

                if (_ORDR_CNTCT.ROLE_ID != 0)
                {
                    _ORDR_CNTCT = Load_ORDR_CNTCT_NME(_ORDR_CNTCT, _AccountTeam.ContactNameInfo);

                    if (_AccountTeam.PhoneInfo != null)
                    {
                        _ORDR_CNTCT = Load_ORDR_CNTCT_PHN(_ORDR_CNTCT, _AccountTeam.PhoneInfo);
                    }

                    int RetVal = _ORDR_CNTCT.InsertOrderContactInfo();
                    if (RetVal == 0)
                    {
                        sError = "Error insert Account Team Member data in the COWS database for Role Code: " + _AccountTeam.RoleCode.ToString();
                        break;
                    }
                }
            }

            return sError;
        }

        private string Load_VAS_Info()
        {
            string sError = string.Empty;
            int iVASCnt = 0;

            try
            {
                iVASCnt = _Order.InstallInfo.ValueAddedServices.Length;
            }
            catch
            {
                return sError;
            }

            if (iVASCnt == 0)
            {
                sError = "Error reading the VAS Data from the ValueAddedService section.";
                return sError;
            }

            OrderInstallInfoValueAddedService _VAS = new OrderInstallInfoValueAddedService();
            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.ValueAddedService;

            for (int i = 0; i < iVASCnt; i++)
            {
                _VAS = _Order.InstallInfo.ValueAddedServices[i];

                FSA_ORDR_VAS_DB _ORDR_VAS = new FSA_ORDR_VAS_DB(_OrderID);
                BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);
                bool _HasBillItem = false;

                _ORDR_VAS.VAS_TYPE_CD = FixNull(_VAS.VASType.ToString());
                if (_ORDR_VAS.VAS_TYPE_CD == string.Empty)
                {
                    sError = "VAS_TYPE not provided in the ValueAddedServices section.";
                    return sError;
                }
                else
                {
                    _ORDR_VAS.SRVC_LVL_ID = FixNull(_VAS.Level).ToString();
                    try
                    {
                        _ORDR_VAS.NW_ADR = FixNull(_VAS.NetworkAddress).ToString();
                    }
                    catch
                    {
                        _ORDR_VAS.NW_ADR = string.Empty;
                    }
                    try
                    {
                        _ORDR_VAS.VAS_QTY = Convert.ToInt16(FixNull(_VAS.Quantity).ToString());
                    }
                    catch
                    {
                        _ORDR_VAS.VAS_QTY = 0;
                    }

                    if (_VAS.BillingLineItems != null)
                    {
                        try
                        {
                            _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _VAS.BillingLineItems[0]);
                            _HasBillItem = true;
                        }
                        catch
                        { }
                    }

                    int RetVal = _ORDR_VAS.InsertFSAOrderVASInfo(_BILL_LINE_ITEM, _HasBillItem);

                    if (RetVal == 0)
                    {
                        sError = "Error inserting VAS data in the COWS database for VAS Type :" + _ORDR_VAS.VAS_TYPE_CD;
                        return sError;
                    }
                }
            }
            return sError;
        }

        private string Load_Billing_Info()
        {
            string sError = string.Empty;
            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.OrderBillingLineItem;
            int iBillCnt = 0;

            if (_Order.BillingOnlyInformation.BillingLineItems == null)
                return sError;

            try
            {
                iBillCnt = _Order.BillingOnlyInformation.BillingLineItems.Length;
            }
            catch
            {
                sError = "Error reading the Billing Only Info from the BillingOnlyInformation section.";
                return sError;
            }

            for (int i = 0; i < iBillCnt; i++)
            {
                BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);
                _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.BillingOnlyInformation.BillingLineItems[i]);

                int RetVal = _BILL_LINE_ITEM.InsertBillingLineItem();
                if (RetVal == 0)
                {
                    sError = "Error inserting Billing only info in the COWS database.";
                    return sError;
                }
            }
            return sError;
        }

        private string Load_Transport_Install_Support_Info()
        {
            string sError = string.Empty;
            int RetVal = -1;

            if (_Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos != null)
            {
                int iCnt = _Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos.Length;
                if (iCnt > 0)
                {
                    string _ModuleID = "SUP";

                    for (int i = 0; i < iCnt; i++)
                    {
                        ORDR_ADR_DB _ORDR_ADR = new ORDR_ADR_DB(_OrderID, _ModuleID);
                        ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);

                        _ORDR_CNTCT = Load_ORDR_CNTCT(_ORDR_CNTCT, _Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i]);
                        _ORDR_ADR = Load_ORDR_ADR(_ORDR_ADR, _Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i]);

                        _ORDR_CNTCT.INTPRTR_CD = (FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i].InterpreterNeededFlag).ToString() == "Y");
                        _ORDR_CNTCT.CNTCT_HR_TXT = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i].AvailabilityHours).ToString();
                        _ORDR_CNTCT.SUPPD_LANG_NME = string.Empty;// FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i].LanguageSupported).ToString();
                        _ORDR_CNTCT.FSA_TME_ZONE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.ContactInfos[i].TimeZone).ToString();

                        RetVal = _ORDR_CNTCT.InsertOrderContactInfo();
                        if (RetVal == 0)
                        {
                            sError = "Error insert Order Contact Info from the Support section of the Transport Installation Data!";
                        }
                        else
                        {
                            RetVal = _ORDR_ADR.InsertOrderAddressInfo();
                            if (RetVal == 0)
                            {
                                sError = "Error insert Order Address Info from the Support section of the Transport Installation Data!";
                            }
                        }
                    }
                }
            }
            return sError;
        }

        private string Load_Transport_Install_Transport_Info()
        {
            string sError = string.Empty;
            int RetVal = -1;

            // Billing Line Items from Transport.
            Int16 _BModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.TransportInstall;
            int iBillCnt = 0;
            try
            {
                iBillCnt = _Order.InstallInfo.TransportInstallInformation.Transport.BillingLineItems.Length;
            }
            catch
            {
                return sError;
            }

            for (int i = 0; i < iBillCnt; i++)
            {
                BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _BModuleID);
                _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.InstallInfo.TransportInstallInformation.Transport.BillingLineItems[i]);

                RetVal = _BILL_LINE_ITEM.InsertBillingLineItem();
                if (RetVal == 0)
                {
                    sError = "Error inserting Billing info from the Install.TransportInfo.Transport section in the COWS database.";
                    return sError;
                }
            }

            return sError;
        }

        private string Load_Transport_Install_Port_Info()
        {
            string sError = string.Empty;
            int RetVal = -1;
            // Order VLANs Info from the Port section.
            if (_Order.InstallInfo.TransportInstallInformation.Port.VLANS != null)
            {
                int VLANCnt = 0;
                try
                {
                    VLANCnt = _Order.InstallInfo.TransportInstallInformation.Port.VLANS.Length;
                }
                catch
                { }

                if (VLANCnt > 0)
                {
                    for (int i = 0; i < VLANCnt; i++)
                    {
                        string _VLAN_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.VLANS[i].VLANID).ToString();
                        string _VLAN_PCT_QTY = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.VLANS[i].AllocationPercentage).ToString();
                        int k = _FSA_ORDR.InsertOrderVLAN(_VLAN_ID, _VLAN_PCT_QTY);
                        if (k == 0)
                        {
                            sError = "Error inserting the VLAN Data from the Install.TransportInfo.Port.IPAddressInformation.VLANS section in the COWS database.";
                            return sError;
                        }
                    }
                }
            }

            // Order IP Address Info Items from Port.
            if (_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation != null)
            {
                if (_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPAddresses != null)
                {
                    int iIPCnt = 0;
                    try
                    {
                        iIPCnt = _Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPAddresses.Length;
                    }
                    catch
                    { }
                    if (iIPCnt > 0)
                    {
                        for (int i = 0; i < iIPCnt; i++)
                        {
                            FSA_ORDR_DB _IP_DB = new FSA_ORDR_DB();
                            _IP_DB.ORDR_ID = _OrderID;
                            _IP_DB.IP_ADR = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPAddresses[i].IPAddress).ToString();
                            _IP_DB.IP_ADR_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPAddresses[i].IPAddressTypeCode).ToString();

                            int _IPVal = _IP_DB.InsertFSAIPAddress();
                            if (_IPVal == 0)
                            {
                                sError = "Error inserting IP Address info from the Install.TransportInfo.Port.IPAddressInformation.IPAddresses section in the COWS database.";
                                return sError;
                            }
                        }
                    }
                }
            }

            // Order Contact Info Items from Port.
            if (_Order.InstallInfo.TransportInstallInformation.Port.ContactInfo != null)
            {
                string _ModuleID = "PRT";
                ORDR_ADR_DB _ORDR_ADR = new ORDR_ADR_DB(_OrderID, _ModuleID);
                ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);

                _ORDR_CNTCT = Load_ORDR_CNTCT(_ORDR_CNTCT, _Order.InstallInfo.TransportInstallInformation.Port.ContactInfo);
                _ORDR_ADR = Load_ORDR_ADR(_ORDR_ADR, _Order.InstallInfo.TransportInstallInformation.Port.ContactInfo);

                RetVal = _ORDR_CNTCT.InsertOrderContactInfo();
                if (RetVal == 0)
                {
                    sError = "Error insert Order Contact Info from the Support section of the Transport Installation Data!";
                }
                else
                {
                    RetVal = _ORDR_ADR.InsertOrderAddressInfo();
                    if (RetVal == 0)
                    {
                        sError = "Error insert Order Address Info from the Support section of the Transport Installation Data!";
                    }
                }
            }

            // Billing Line Items from Port.
            Int16 _BModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.PortInstall;
            int iBillCnt = 0;
            try
            {
                iBillCnt = _Order.InstallInfo.TransportInstallInformation.Port.BillingLineItems.Length;
            }
            catch
            {
                return sError; // Assume no Billing Line Items
            }

            for (int i = 0; i < iBillCnt; i++)
            {
                BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _BModuleID);
                _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.InstallInfo.TransportInstallInformation.Port.BillingLineItems[i]);

                RetVal = _BILL_LINE_ITEM.InsertBillingLineItem();
                if (RetVal == 0)
                {
                    sError = "Error inserting Billing info from the Install.TransportInfo.Port section in the COWS database.";
                    return sError;
                }
            }

            return sError;
        }

        private string Load_MDS_Install_Info()
        {
            string sError = string.Empty;
            int RetVal = -1;

            if (_Order.InstallInfo.MDSInstallationInformation.ContactInfos != null)
            {
                int iCnt = _Order.InstallInfo.MDSInstallationInformation.ContactInfos.Length;
                if (iCnt > 0)
                {
                    for (int i = 0; i < iCnt; i++)
                    {
                        string _ModuleID = "MDS";

                        ORDR_ADR_DB _ORDR_ADR = new ORDR_ADR_DB(_OrderID, _ModuleID);
                        ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);

                        _ORDR_CNTCT = Load_ORDR_CNTCT(_ORDR_CNTCT, _Order.InstallInfo.MDSInstallationInformation.ContactInfos[i]);
                        _ORDR_ADR = Load_ORDR_ADR(_ORDR_ADR, _Order.InstallInfo.MDSInstallationInformation.ContactInfos[i]);

                        RetVal = _ORDR_CNTCT.InsertOrderContactInfo();
                        if (RetVal == 0)
                        {
                            sError = "Error insert Order Contact Info from the MDS Installation Data!";
                        }
                        else
                        {
                            RetVal = _ORDR_ADR.InsertOrderAddressInfo();
                            if (RetVal == 0)
                            {
                                sError = "Error insert Order Address Info from the MDS Installation Data!";
                            }
                        }
                    }
                }
            }

            if (RetVal == -1)
            {
                if (_Order.InstallInfo.MDSInstallationInformation.MDSLineItems != null)
                {
                    sError = Load_MDS_Line_Item_Info();
                }
            }
            return sError;
        }

        private string Load_Install_Billing_Info()
        {
            string sError = string.Empty;
            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.InstallBillingLineItem;
            int iBillCnt = 0;

            try
            {
                iBillCnt = _Order.InstallInfo.BillingLineItems.Length;
            }
            catch
            {
                sError = "Error reading the Billing Only Info from the InstallInfo.BillingLineItems section.";
                return sError;
            }

            for (int i = 0; i < iBillCnt; i++)
            {
                BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);
                _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.InstallInfo.BillingLineItems[i]);

                int RetVal = _BILL_LINE_ITEM.InsertBillingLineItem();
                if (RetVal == 0)
                {
                    sError = "Error inserting Billing only info in the COWS database.";
                    return sError;
                }
            }
            return sError;
        }

        private string Load_MDS_Line_Item_Info()
        {
            string sError = string.Empty;
            int iMDSCnt = 0;

            try
            {
                iMDSCnt = _Order.InstallInfo.MDSInstallationInformation.MDSLineItems.Length;
            }
            catch
            {
                return sError;
            }

            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.MDSLineItem;
            for (int i = 0; i < iMDSCnt; i++)
            {
                FSA_ORDR_CPE_LINE_ITEM_DB _MDS = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "MDS");
                BILL_LINE_ITEM_DB _Bill = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);

                _MDS.SRVC_LINE_ITEM_CD = FixNull(_Order.InstallInfo.MDSInstallationInformation.MDSLineItems[i].ServiceLineItemCode).ToString();
                _Bill.LINE_ITEM_DES = FixNull(_Order.InstallInfo.MDSInstallationInformation.MDSLineItems[i].Description).ToString();
                try
                {
                    _Bill.LINE_ITEM_QTY = Convert.ToInt16(FixNull(_Order.InstallInfo.MDSInstallationInformation.MDSLineItems[i].Quantity).ToString());
                }
                catch
                {
                    _Bill.LINE_ITEM_QTY = 0;
                }
                _Bill.MRC_CHG_AMT = FixNull(_Order.InstallInfo.MDSInstallationInformation.MDSLineItems[i].MonthlyRecurringChargeAmount).ToString();
                _Bill.NRC_CHG_AMT = FixNull(_Order.InstallInfo.MDSInstallationInformation.MDSLineItems[i].NonRecurringChargeAmount).ToString();

                int RetVal = _MDS.InsertFSALineItem(_Bill, true);
                if (RetVal == 0)
                {
                    sError = "Error inserting MDS Line Items info in the COWS database.";
                    return sError;
                }
            }

            return sError;
        }

        private string Load_CPE_Line_Item_Info()
        {
            string sError = string.Empty;
            int iCPECnt = 0;

            try
            {
                iCPECnt = _Order.InstallInfo.CPEInstallationInformation.CPELineItems.Length;
            }
            catch
            {
                return sError;
            }

            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.CPELineItem;
            for (int i = 0; i < iCPECnt; i++)
            {
                FSA_ORDR_CPE_LINE_ITEM_DB _CPE = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "CPE");
                BILL_LINE_ITEM_DB _Bill = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);

                _CPE.EQPT_TYPE_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].EquipmentTypeCode.ToString());
                _CPE.EQPT_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].EquipmentID).ToString();
                _CPE.MFR_NME = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].Manufacturer).ToString();
                _CPE.CNTRC_TYPE_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].ContractType.ToString());
                _CPE.CNTRC_TERM_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].ContractTerm).ToString();

                _CPE.INSTLN_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].InstallationFlagCode).ToString();
                _CPE.MNTC_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].MaintenanceFlagCode).ToString();

                _Bill.LINE_ITEM_DES = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].Description).ToString();
                try
                {
                    _Bill.LINE_ITEM_QTY = Convert.ToInt16(FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].Quantity).ToString());
                }
                catch
                {
                    _Bill.LINE_ITEM_QTY = 0;
                }

                //On ICD v27, we now get multiple Financial sections, requiring inserting multiple Billing Lines.
                //Plan is to insert the first one along with the CPE section, then insert any additional bililng lines seperately
                int iFINCnt = 0;
                try
                {
                    iFINCnt = _Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials.Length;
                }
                catch { }

                if (iFINCnt > 0)
                {
                    _Bill.MRC_CHG_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[0].MonthlyRecurringChargeAmount).ToString();
                    _Bill.NRC_CHG_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[0].NonRecurringChargeAmount).ToString();
                    _Bill.CHG_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[0].ChargeCode).ToString();
                }

                int _FSA_CPE_LINE_ITEM_ID = _CPE.InsertFSALineItem(_Bill, true);
                if (_FSA_CPE_LINE_ITEM_ID == 0)
                {
                    sError = "Error inserting CPE Line Items info in the COWS database.";
                    return sError;
                }
                else
                {
                    if (iFINCnt > 0)
                    {
                        for (int j = 1; j < iFINCnt; j++)
                        {
                            BILL_LINE_ITEM_DB _newBill = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);
                            _newBill.LINE_ITEM_DES = _Bill.LINE_ITEM_DES;
                            _newBill.LINE_ITEM_QTY = _Bill.LINE_ITEM_QTY;
                            _newBill.MRC_CHG_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[j].MonthlyRecurringChargeAmount).ToString();
                            _newBill.NRC_CHG_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[j].NonRecurringChargeAmount).ToString();
                            _newBill.CHG_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPELineItems[i].CPEFinancials[j].ChargeCode).ToString();
                            _newBill.FSA_CPE_LINE_ITEM_ID = _FSA_CPE_LINE_ITEM_ID;

                            int RetValInner = _newBill.InsertBillingLineItem();
                            if (RetValInner == 0)
                            {
                                sError = "Error inserting CPE Line Items Financial info in the COWS database.";
                                return sError;
                            }
                        }
                    }
                }
            }

            return sError;
        }

        private string Load_SIP_Trunk_Group_Info(SIPTrunkGroups[] _Trunk, bool _IsInstallCode)
        {
            string sError = string.Empty;
            int _SIPCnt = 0;
            try
            {
                _SIPCnt = _Trunk.Length;
            }
            catch
            { }

            if (_SIPCnt > 0)
            {
                for (int i = 0; i < _SIPCnt; i++)
                {
                    SIP_TRNK_GRP_DB _TrunkDB = new SIP_TRNK_GRP_DB();

                    _TrunkDB.ORDR_ID = _OrderID;
                    _TrunkDB.IS_INSTL_CD = _IsInstallCode;
                    try
                    {
                        _TrunkDB.CMPNT_ID = Convert.ToInt32(FixNull(_Trunk[i].ComponentID).ToString());
                    }
                    catch
                    { }
                    try
                    {
                        _TrunkDB.SIP_TRNK_QTY = Convert.ToInt32(FixNull(_Trunk[i].SIPTrunkQuantity).ToString());
                    }
                    catch
                    { }
                    _TrunkDB.NW_USER_ADR = FixNull(_Trunk[i].NetworkUserAddress).ToString();
                    _TrunkDB.SIP_TRNK_GRP_ID = 0;

                    int _SIP_TRNK_GRP_ID = 0;
                    _SIP_TRNK_GRP_ID = _TrunkDB.InsertSIPTrunkGroup();

                    if (_SIP_TRNK_GRP_ID <= 0)
                    {
                        sError = "Error inserting the SIP Trunk Group info into the COWS database for record # " + (i + 1).ToString();
                        return sError;
                    }
                    else
                    {
                        if (_Trunk[i].MPLSAccesses != null && _IsInstallCode)
                        {
                            int _MPLSCnt = 0;
                            try
                            {
                                _MPLSCnt = _Trunk[i].MPLSAccesses.Length;
                            }
                            catch
                            { }

                            if (_MPLSCnt > 0)
                            {
                                for (int j = 0; j < _MPLSCnt; j++)
                                {
                                    MPLS_ACCS _AccessDB = new MPLS_ACCS();
                                    _AccessDB.SIP_TRNK_GRP_ID = _SIP_TRNK_GRP_ID;
                                    _AccessDB.CKT_ID = FixNull(_Trunk[i].MPLSAccesses[j].CircuitID).ToString();
                                    _AccessDB.NW_USER_ADR = FixNull(_Trunk[i].MPLSAccesses[j].NetworkUserAddress).ToString();

                                    int k = _AccessDB.InsertMPLSAccess();
                                    if (k == 0)
                                    {
                                        sError = "Error inserting MPLS Access Info in the COWS database for Record # " + (j + 1).ToString();
                                        return sError;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                sError = "No SIP Trunk Groups records exist inside the SIPGroupInfo section.";
            }
            return sError;
        }

        private string Load_Disconnect_Info()
        {
            string sError = string.Empty;
            int RetVal = -1;
            string _ModuleID = "DSC";

            ORDR_CNTCT_DB _ORDR_CNTCT = new ORDR_CNTCT_DB(_OrderID, _ModuleID);
            _ORDR_CNTCT.ROLE_ID = 15;

            _ORDR_CNTCT = Load_ORDR_CNTCT_NME(_ORDR_CNTCT, _Order.DisconnectInfo.ContactNameInfo);
            _ORDR_CNTCT = Load_ORDR_CNTCT_PHN(_ORDR_CNTCT, _Order.DisconnectInfo.PhoneInfo);

            RetVal = _ORDR_CNTCT.InsertOrderContactInfo();
            if (RetVal == 0)
            {
                sError = "Error insert Order Contact Info from the MDS Installation Data!";
            }

            if (RetVal == -1)
            {
                int iCnt = 0;
                BILL_LINE_ITEM_DB _Bill = new BILL_LINE_ITEM_DB(_OrderID, 0);

                if (_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation != null)
                {
                    try
                    {
                        iCnt = _Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems.Length;
                    }
                    catch
                    { }

                    for (int i = 0; i < iCnt; i++)
                    {
                        FSA_ORDR_VAS_DB _VAS = new FSA_ORDR_VAS_DB(_OrderID);
                        FSA_ORDR_CPE_LINE_ITEM_DB _FSA_VAS_ITEM = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "DVL");

                        try
                        {
                            _VAS.NW_ADR = FixNull(_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems[i].NetworkAddress).ToString();
                        }
                        catch
                        { }
                        _VAS.VAS_TYPE_CD = FixNull(_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems[i].Type.ToString());

                        RetVal = _VAS.InsertFSAOrderVASInfo(_Bill, false);
                        if (RetVal == 0)
                        {
                            sError = "Error inserting the Disconnect VAS info into the COWS database.";
                            return sError;
                        }
                        else
                            _FSA_VAS_ITEM.VAS_ID = RetVal;

                        try
                        {
                            _FSA_VAS_ITEM.FSA_ORGNL_INSTL_ORDR_ID = Convert.ToInt32(FixNull(_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems[i].OriginalInstallOrderID).ToString());
                        }
                        catch { }

                        if (_FSA_VAS_ITEM.FSA_ORGNL_INSTL_ORDR_ID != 0)
                        {
                            _FSA_VAS_ITEM.CXR_CKT_ID = FixNull(_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems[i].CarrierCircuitID).ToString();
                            _FSA_VAS_ITEM.CXR_SRVC_ID = FixNull(_Order.DisconnectInfo.ValueAddedServicesDisconnectInformation.ValueAddedServicesDisconnectLineItems[i].CarrierServiceID).ToString();

                            RetVal = _FSA_VAS_ITEM.InsertFSALineItem(_Bill, false);
                            if (RetVal == 0)
                            {
                                sError = "Error inserting the Disconnect VAS Line Items info into the COWS database.";
                                return sError;
                            }
                        }
                    }
                    iCnt = 0;
                }

                if (_Order.DisconnectInfo.MDSDisconnectInformation != null)
                {
                    try
                    {
                        iCnt = _Order.DisconnectInfo.MDSDisconnectInformation.MDSLineItems.Length;
                    }
                    catch
                    { }

                    for (int i = 0; i < iCnt; i++)
                    {
                        FSA_ORDR_CPE_LINE_ITEM_DB _FSA = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "DML");
                        _FSA.SRVC_LINE_ITEM_CD = FixNull(_Order.DisconnectInfo.MDSDisconnectInformation.MDSLineItems[i].ServiceLineItemCode).ToString();
                        _FSA.MDS_DES = FixNull(_Order.DisconnectInfo.MDSDisconnectInformation.MDSLineItems[i].Description).ToString();
                        try
                        {
                            _FSA.FSA_ORGNL_INSTL_ORDR_ID = Convert.ToInt32(FixNull(_Order.DisconnectInfo.MDSDisconnectInformation.MDSLineItems[i].OriginalInstallOrderID).ToString());
                        }
                        catch { }

                        RetVal = _FSA.InsertFSALineItem(_Bill, false);
                        if (RetVal == 0)
                        {
                            sError = "Error inserting the Disconnect MDS Line Items info into the COWS database.";
                            return sError;
                        }
                    }
                    iCnt = 0;
                }

                if (_Order.DisconnectInfo.CPEDisconnectInformation != null)
                {
                    try
                    {
                        iCnt = _Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems.Length;
                    }
                    catch
                    { }

                    for (int i = 0; i < iCnt; i++)
                    {
                        FSA_ORDR_CPE_LINE_ITEM_DB _FSA = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "DCL");
                        try
                        {
                            _FSA.CMPNT_ID = Convert.ToInt32(FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].ComponentID.ToString()));
                        }
                        catch
                        {
                            _FSA.CMPNT_ID = 0;
                        }

                        try
                        {
                            _FSA.FSA_ORGNL_INSTL_ORDR_ID = Convert.ToInt32(FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].OriginalInstallOrderID.ToString()));
                        }
                        catch
                        { }

                        _FSA.EQPT_TYPE_ID = FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].EquipmentTypeCode).ToString();
                        _FSA.EQPT_ID = FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].EquipmentID).ToString();
                        _FSA.CNTRC_TYPE_ID = FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].ContractType.ToString());
                        _FSA.CNTRC_TERM_ID = FixNull(_Order.DisconnectInfo.CPEDisconnectInformation.CPELineItems[i].ContractTerm).ToString();

                        RetVal = _FSA.InsertFSALineItem(_Bill, false);
                        if (RetVal == 0)
                        {
                            sError = "Error inserting the Disconnect CPE Line Items info into the COWS database.";
                            return sError;
                        }
                    }
                    iCnt = 0;
                }

                if (_Order.DisconnectInfo.TransportDisconnectInformation != null)
                {
                    try
                    {
                        iCnt = _Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems.Length;
                    }
                    catch
                    { }

                    for (int i = 0; i < iCnt; i++)
                    {
                        FSA_ORDR_CPE_LINE_ITEM_DB _FSA = new FSA_ORDR_CPE_LINE_ITEM_DB(_OrderID, "DSC");
                        _FSA.DISC_PL_NBR = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].PrivateLineNumber).ToString();
                        try
                        {
                            _FSA.DISC_FMS_CKT_NBR = Convert.ToInt32(FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].FMSCircuitNumber.ToString()));
                        }
                        catch
                        { }
                        _FSA.DISC_NW_ADR = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].NetworkAddress).ToString();

                        try
                        {
                            _FSA.FSA_ORGNL_INSTL_ORDR_ID = Convert.ToInt32(FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].OriginalInstallOrderID.ToString()));
                        }
                        catch { }
                        _FSA.CXR_CKT_ID = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].CarrierCircuitID).ToString();
                        _FSA.CXR_SRVC_ID = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].CarrierServiceID).ToString();
                        _FSA.ACCS_TYPE_CD = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].AccessTypeCode).ToString();
                        _FSA.ACCS_TYPE_DES = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].AccessTypeDescription).ToString();
                        _FSA.SPD_OF_SRVC = FixNull(_Order.DisconnectInfo.TransportDisconnectInformation.TransportDisconnectLineItems[i].SpeedOfService).ToString();

                        RetVal = _FSA.InsertFSALineItem(_Bill, false);
                        if (RetVal == 0)
                        {
                            sError = "Error inserting the Disconnect Transport Disconnect Line Items info into the COWS database.";
                            return sError;
                        }
                    }
                }
            }

            if (_Order.DisconnectInfo.BillingLineItems != null)
            {
                Int16 _BillModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.DisconnectBillingLineItem;
                int iBillCnt = 0;

                try
                {
                    iBillCnt = _Order.DisconnectInfo.BillingLineItems.Length;
                }
                catch
                {
                    sError = "Error reading the Billing Only Info from the DisconnectInfo.BillingLineItems section.";
                    return sError;
                }

                for (int i = 0; i < iBillCnt; i++)
                {
                    BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _BillModuleID);
                    _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.DisconnectInfo.BillingLineItems[i]);

                    if (_BILL_LINE_ITEM.InsertBillingLineItem() == 0)
                    {
                        sError = "Error inserting Billing only info (Disconnect) in the COWS database.";
                        return sError;
                    }
                }
            }

            if (_Order.DisconnectInfo.SIPTrunkDisconnectInformation != null)
            {
                sError = Load_SIP_Trunk_Group_Info(_Order.DisconnectInfo.SIPTrunkDisconnectInformation.SIPTrunkGroups, false);
                if (sError != string.Empty)
                {
                    return sError;
                }
            }

            return sError;
        }

        private string Load_Change_Info()
        {
            string sError = string.Empty;
            Int16 _ModuleID = (Int16)BILL_LINE_ITEM_DB.BILL_LINE_ITEM_TYPE.ChangeBillingLineItem;
            int iBillCnt = 0;

            if (_Order.ChangeInformation.BillingLineItems != null)
            {
                try
                {
                    iBillCnt = _Order.ChangeInformation.BillingLineItems.Length;
                }
                catch
                {
                    sError = "Error reading the Billing Only Info from the ChangeInformation.BillingLineItems section.";
                    return sError;
                }

                for (int i = 0; i < iBillCnt; i++)
                {
                    BILL_LINE_ITEM_DB _BILL_LINE_ITEM = new BILL_LINE_ITEM_DB(_OrderID, _ModuleID);
                    _BILL_LINE_ITEM = Load_BILL_LINE_ITEM(_BILL_LINE_ITEM, _Order.ChangeInformation.BillingLineItems[i]);

                    int RetVal = _BILL_LINE_ITEM.InsertBillingLineItem();
                    if (RetVal == 0)
                    {
                        sError = "Error inserting Billing only info (Change) in the COWS database.";
                        return sError;
                    }
                }
            }
            return sError;
        }

        private string Load_Notes_Info()
        {
            string sError = string.Empty;
            int iNotesCnt = 0;

            try
            {
                iNotesCnt = _Order.Notes.Length;
            }
            catch
            {
                return sError;
            }

            for (int i = 0; i < iNotesCnt; i++)
            {
                ORDR_NTE_DB _Note = new ORDR_NTE_DB(_OrderID);

                switch (FixNull(_Order.Notes[i].NoteType.ToString()))
                {
                    case "FMSES":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.FMSES;
                        break;

                    case "NetworkEngineer":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.NetworkEngineer;
                        break;

                    case "CPE":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.CPE;
                        break;

                    case "Pricing":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.Pricing;
                        break;

                    case "International":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.International;
                        break;

                    case "Cancel":
                        _Note.NTE_TYPE_ID = (Byte)OrderNotesNoteNoteType.Cancel;
                        break;

                    default:
                        continue;
                }

                _Note.NTE_TXT = FixNull(_Order.Notes[i].NoteText).ToString();

                int RetVal = _Note.InsertOrderNotes();
                if (RetVal == 0)
                {
                    sError = "Error inserting Notes Info in the COWS database.";
                    return sError;
                }
            }
            return sError;
        }

        private void Load_Initial_Tasks()
        {
            _FSA_ORDR.LoadInitialTask();
        }

        #endregion "Read & Load Main Forms"

        #region "Read & Load Sub or Partial Form Data"

        private string ValidateAndReadOrderInfo()
        {
            string sError = string.Empty;

            switch (_Order.OrderInformation.OrderAction.ToString().ToUpper())
            {
                case "SUBMIT":
                    _FSA_ORDR.ORDR_ACTN_ID = (Byte)OrderOrderInformationOrderAction.Submit;
                    break;

                case "PRESUBMIT":
                    _FSA_ORDR.ORDR_ACTN_ID = (Byte)OrderOrderInformationOrderAction.Presubmit;
                    break;

                case "DELETE":
                    _FSA_ORDR.ORDR_ACTN_ID = (Byte)OrderOrderInformationOrderAction.Delete;
                    break;

                default:
                    sError += "Order Action Type,";
                    break;
            }

            try
            {
                _FSA_ORDR.FTN = _Order.OrderInformation.OrderID;
                sError = _FSA_ORDR.FTN != "" ? sError : sError + "OrderID,";
            }
            catch
            {
                sError += "OrderID,";
            }

            _FSA_ORDR.ORDR_TYPE_CD = FixNull(_Order.OrderInformation.OrderTypeCode.ToString());
            if (_FSA_ORDR.ORDR_TYPE_CD == string.Empty && _FSA_ORDR.ORDR_ACTN_ID != 3)
            {
                sError += "Order Type Code,";
            }

            _FSA_ORDR.ORDR_SUB_TYPE_CD = FixNull(_Order.OrderInformation.OrderSubTypeCode.ToString());

            _FSA_ORDR.PROD_TYPE_CD = FixNull(_Order.OrderInformation.ProductTypeCode.ToString());
            if (_FSA_ORDR.PROD_TYPE_CD == string.Empty && _FSA_ORDR.ORDR_ACTN_ID != 3)
            {
                sError += "Product Type Code,";
            }

            _FSA_ORDR.PRNT_FTN = FixNull(_Order.OrderInformation.ParentOrderID).ToString();
            _FSA_ORDR.RELTD_FTN = FixNull(_Order.OrderInformation.RelatedOrderID).ToString();
            _FSA_ORDR.SCA_NBR = GetSCANumbers();
            _FSA_ORDR.TSP_CD = FixNull(_Order.OrderInformation.TelecomServicePriorityCode).ToString();
            _FSA_ORDR.CPW_TYPE = FixNull(_Order.OrderInformation.CarrierPartnerWholeSaleType.ToString());

            try
            {
                _FSA_ORDR.CUST_CMMT_DT = _Order.OrderInformation.CustomerCommitDate;
            }
            catch
            { }

            if ((_FSA_ORDR.ORDR_ACTN_ID == 2) && (_FSA_ORDR.ORDR_TYPE_CD != "CN") && ((_FSA_ORDR.CUST_CMMT_DT.ToString() == "1/1/0001 12:00:00 AM") || (_FSA_ORDR.CUST_CMMT_DT == null)))
                sError += "CustomerCommitDate,";

            try
            {
                _FSA_ORDR.CUST_WANT_DT = Convert.ToDateTime(FixNull(_Order.OrderInformation.CustomerWantDate).ToString());
            }
            catch
            { }
            try
            {
                _FSA_ORDR.CUST_ORDR_SBMT_DT = Convert.ToDateTime(FixNull(_Order.OrderInformation.CustomerOrderSubmitDate).ToString());
            }
            catch
            { }
            try
            {
                _FSA_ORDR.CUST_SIGNED_DT = Convert.ToDateTime(FixNull(_Order.OrderInformation.CustomerSignedDate).ToString());
            }
            catch
            { }
            try
            {
                _FSA_ORDR.ORDR_SBMT_DT = Convert.ToDateTime(FixNull(_Order.OrderInformation.OrderSubmitDate).ToString());
            }
            catch
            { }

            _FSA_ORDR.CUST_PRMS_OCPY_CD = FixNull(_Order.OrderInformation.CustomerPremiseCurrentlyOccupiedFlag).ToString();
            _FSA_ORDR.CUST_ACPT_ERLY_SRVC_CD = FixNull(_Order.OrderInformation.WillCustomerAcceptServiceEarlyFlag).ToString();
            _FSA_ORDR.MULT_CUST_ORDR_CD = FixNull(_Order.OrderInformation.MultipleOrderFlag).ToString();

            if (_Order.OrderInformation.GovernmentInformation != null)
            {
                _FSA_ORDR.GVRMNT_TYPE_ID = FixNull(_Order.OrderInformation.GovernmentInformation.TypeCode).ToString();
                _FSA_ORDR.SOTS_ID = FixNull(_Order.OrderInformation.GovernmentInformation.SOTSID).ToString();
                _FSA_ORDR.CHARS_ID = FixNull(_Order.OrderInformation.GovernmentInformation.CHARSID).ToString();
            }

            if (_Order.OrderInformation.ExpediteInformation != null)
            {
                _FSA_ORDR.FSA_EXP_TYPE_CD = FixNull(_Order.OrderInformation.ExpediteInformation.TypeCode).ToString();
            }

            _FSA_ORDR.INSTL_ESCL_CD = FixNull(_Order.OrderInformation.EscalatedFlag.ToString());
            _FSA_ORDR.INSTL_VNDR_CD = FixNull(_Order.OrderInformation.VendorCode).ToString();

            _FSA_ORDR.VNDR_VPN_CD = FixNull(_Order.OrderInformation.VendorVPNCode).ToString();
            try
            {
                _FSA_ORDR.MULT_ORDR_INDEX_NBR = Convert.ToInt32(FixNull(_Order.OrderInformation.MultipleOrderIndex).ToString());
            }
            catch
            { }

            try
            {
                _FSA_ORDR.MULT_ORDR_TOT_CNT = Convert.ToInt32(FixNull(_Order.OrderInformation.MultipleOrderTotal).ToString());
            }
            catch
            { }

            sError = sError == string.Empty ? sError : "Invalid " + ReturnTrimmedString(sError) + ".";
            return sError;
        }

        private string GetSCANumbers()
        {
            string _SCA_NBR = String.Empty;
            int iSCACnt = 0;

            try
            {
                iSCACnt = _Order.OrderInformation.SpecialCustomerArrangements.Length;
            }
            catch
            {
                return _SCA_NBR;
            }

            OrderOrderInformationSpecialCustomerArrangement _SCAItem = new OrderOrderInformationSpecialCustomerArrangement();

            for (int i = 0; i < iSCACnt; i++)
            {
                _SCAItem = _Order.OrderInformation.SpecialCustomerArrangements[i];
                if (_SCAItem != null)
                {
                    _SCA_NBR += FixNull(_SCAItem.Number).ToString() + "|";
                }
            }

            return _SCA_NBR;
        }

        private string GetCustomerIPAddresses()
        {
            string _IPAddresses = String.Empty;
            int iIPCnt = 0;
            try
            {
                iIPCnt = _Order.InstallInfo.TransportInstallInformation.Port.CustomerIPAddresses.Length;
            }
            catch
            {
                return _IPAddresses;
            }

            OrderInstallInfoTransportInstallInformationPortCustomerIPAddress _IPItem = new OrderInstallInfoTransportInstallInformationPortCustomerIPAddress();
            for (int i = 0; i < iIPCnt; i++)
            {
                _IPItem = _Order.InstallInfo.TransportInstallInformation.Port.CustomerIPAddresses[i];
                if (_IPItem != null)
                {
                    _IPAddresses += FixNull(_IPItem.IPAddress.ToString()) + "|";
                }
            }

            return _IPAddresses;
        }

        private string ReadOrderMDSCPEInstallInfo()
        {
            string sError = string.Empty;

            try
            {
                _FSA_ORDR.INSTL_DSGN_DOC_NBR = FixNull(_Order.InstallInfo.DesignDocumentNumber).ToString();
            }
            catch
            { }

            ///////////////////////////////////////////////////////////////////////
            //MDS Install Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.InstallInfo.MDSInstallationInformation != null)
            {
                try
                {
                    _FSA_ORDR.INSTL_SOLU_SRVC_DES = FixNull(_Order.InstallInfo.MDSInstallationInformation.SolutionService).ToString();
                    _FSA_ORDR.INSTL_NW_TYPE_CD = FixNull(_Order.InstallInfo.MDSInstallationInformation.NetworkTypeCode).ToString();
                    _FSA_ORDR.INSTL_SRVC_TIER_DES = FixNull(_Order.InstallInfo.MDSInstallationInformation.ServiceTierCode).ToString();

                    switch (FixNull(_Order.InstallInfo.MDSInstallationInformation.ServiceTierCode).ToString().Replace(" ", ""))
                    {
                        case "MNSStrategic":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 1;
                            break;

                        case "MNSComplete":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 2;
                            break;

                        case "MNSCollaborate":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 3;
                            break;

                        case "MNSSupportDesign":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 4;
                            break;

                        case "MNSSupportImplementation":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 5;
                            break;

                        case "MNSSupportM&N":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 6;
                            break;

                        case "MNSSupportDesign+Imp":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 7;
                            break;

                        case "MNSSupportDesign+M&N":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 8;
                            break;

                        case "MNSSupportDesign+Imp+M&N":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 9;
                            break;

                        case "MNSSupportImp+M&N":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 10;
                            break;

                        case "LegacyComplete":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 11;
                            break;

                        case "LegacyM&TR":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 12;
                            break;

                        case "LegacyM&N":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 13;
                            break;

                        case "LegacyDSOC":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 14;
                            break;

                        case "LegacyAimnet":
                            _FSA_ORDR.INSTL_SRVC_TIER_CD = 15;
                            break;

                        default:
                            break;
                    }

                    _FSA_ORDR.INSTL_TRNSPRT_TYPE_CD = FixNull(_Order.InstallInfo.MDSInstallationInformation.TransportOrderTypeCode).ToString();
                }
                catch
                {
                    sError += "Error reading Order.InstallInfo.MDSInstallation Data.";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            //CPE Install Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.InstallInfo.CPEInstallationInformation != null)
            {
                if (_Order.InstallInfo.CPEInstallationInformation.CPEPhoneNumber != null)
                {
                    _FSA_ORDR.CPE_PHN_NBR = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPEPhoneNumber.PhoneNumber).ToString();
                    _FSA_ORDR.CPE_PHN_NBR_TYPE_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPEPhoneNumber.Type).ToString();
                }
                try
                {
                    _FSA_ORDR.CPE_CPE_ORDR_TYPE_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.CPEOrderTypeCode.ToString());
                    _FSA_ORDR.CPE_EQPT_ONLY_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.EquipmentOnlyFlagCode).ToString();
                    _FSA_ORDR.CPE_REC_ONLY_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.RecordsOnlyOrderFlagCode).ToString();
                    _FSA_ORDR.CPE_ACCS_PRVDR_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.AccessProvideCode).ToString();
                    _FSA_ORDR.CPE_ECCKT_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.ECCKTIdentifier).ToString();
                    _FSA_ORDR.CPE_MSCP_CD = FixNull(_Order.InstallInfo.CPEInstallationInformation.ManagedServicesChannelProgramCode).ToString();
                    _FSA_ORDR.CPE_DLVRY_DUTY_ID = FixNull(_Order.InstallInfo.CPEInstallationInformation.DeliveryDuties).ToString();
                    _FSA_ORDR.CPE_DLVRY_DUTY_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.DeliveryDutyAmount).ToString();
                    _FSA_ORDR.CPE_SHIP_CHG_AMT = FixNull(_Order.InstallInfo.CPEInstallationInformation.ShippingChargeAmount).ToString();
                }
                catch
                {
                    sError += "Error reading Order.InstallInfo.CPEInstallation Data.";
                }
            }

            return sError;
        }

        private string ReadOrderTransportInstallInfo()
        {
            string sError = string.Empty;
            //_FSA_ORDR.ORANGE_CMNTY_NME = FixNull(_Order.InstallInfo.TransportInstallInformation.OrangeCommunityName).ToString();
            //_FSA_ORDR.ORANGE_SITE_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.OrangeSiteID).ToString();
            _FSA_ORDR.ORANGE_OFFR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.OrangeOfferCode).ToString();
            _FSA_ORDR.CXR_SRVC_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.CarrierServiceID).ToString();
            _FSA_ORDR.CXR_CKT_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.CarrierCircuitID).ToString();
            _FSA_ORDR.CUSTOMER_FIREWALL_FLAG = FixNull(_Order.InstallInfo.TransportInstallInformation.CustomerFirewallFlag.ToString()).ToString();
            ///////////////////////////////////////////////////////////////////////
            //Transport Install - Support Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.InstallInfo.TransportInstallInformation.SupportInformation != null)
            {
                try
                {
                    _FSA_ORDR.TSUP_TELCO_DEMARC_BLDG_NME = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.TelcoDemarcationBuilding).ToString();
                    _FSA_ORDR.TSUP_TELCO_DEMARC_FLR_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.TelcoDemarcationFloor).ToString();
                    _FSA_ORDR.TSUP_TELCO_DEMARC_RM_NBR = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.TelcoDemarcationRoom).ToString();
                    _FSA_ORDR.TSUP_NEAREST_CROSS_STREET_NME = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.NearestCrossStreet).ToString();
                    _FSA_ORDR.TSUP_PRS_QOT_NBR = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.PRSQuoteNumber).ToString();
                    _FSA_ORDR.TSUP_GCS_NBR = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.GCSNumber).ToString();
                    _FSA_ORDR.TSUP_RFQ_NBR = FixNull(_Order.InstallInfo.TransportInstallInformation.SupportInformation.RequestForQuoteNumber).ToString();
                }
                catch
                {
                    sError += "Error reading Order.InstallInfo.TransportInstallationInformation.SupportInformation Data.";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            //Transport Install - Transport Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.InstallInfo.TransportInstallInformation.Transport != null)
            {
                _FSA_ORDR.CKT_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.CircuitID).ToString();
                _FSA_ORDR.VNDO_CNTRC_TERM_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.VendorContractTerm).ToString();

                try
                {
                    _FSA_ORDR.TTRPT_ACCS_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AccessTypeCode).ToString();
                    _FSA_ORDR.TTRPT_ACCS_TYPE_DES = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AccessTypeDescription);
                    _FSA_ORDR.TTRPT_ACCS_PRVDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AccessProviderCode).ToString();
                    _FSA_ORDR.TTRPT_ACCS_ARNGT_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AccessArrangementCode).ToString();
                    _FSA_ORDR.TTRPT_ACCS_CNTRC_TERM_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AccessContractTerm).ToString();
                    _FSA_ORDR.TTRPT_SPD_OF_SRVC_BDWD_DES = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.SpeedOfService).ToString();
                    _FSA_ORDR.TTRPT_LINK_PRCOL_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.LinkProtocolCode).ToString();
                    _FSA_ORDR.TTRPT_ENCAP_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.EncapsulationCode).ToString();

                    _FSA_ORDR.TTRPT_ROUTG_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.RoutingTypeCode).ToString();
                    _FSA_ORDR.TTRPT_MPLS_VPN_OVR_SPLK_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.MPLSVPNOverSprintLinkFlag).ToString();
                    _FSA_ORDR.TTRPT_SRVC_TYPE_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.ServiceType).ToString();
                    _FSA_ORDR.TTRPT_FRMNG_DES = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.Framing).ToString();
                    _FSA_ORDR.TTRPT_NCDE_DES = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.Encoding).ToString();
                    _FSA_ORDR.TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.CSUDSUProvidedByVendorFlag).ToString();
                    _FSA_ORDR.TTRPT_JACK_NRFC_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.JackInterfaceTypeCode).ToString();
                    _FSA_ORDR.TTRPT_STRATUM_LVL_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.StratumLevelCode).ToString();
                    _FSA_ORDR.TTRPT_CLOCK_SRC_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.ClockSourceCode).ToString();
                    _FSA_ORDR.TTRPT_IS_OCHRONOUS_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.IsochronousFlag).ToString();
                    _FSA_ORDR.TTRPT_PRLEL_PRCS_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.ParallelProcessTypeCode).ToString();
                    _FSA_ORDR.TTRPT_PRLEL_PRCS_CKT_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.ParallelProcessCircuitID).ToString();
                    _FSA_ORDR.TTRPT_RELTD_FCTY_REUSE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.RelatedFacilityReuseFlag).ToString();
                    _FSA_ORDR.TTRPT_RELTD_FCTY_CKT_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.RelatedFacilityCircuitID).ToString();
                    _FSA_ORDR.TTRPT_NW_ADR = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.NetworkAddress).ToString();
                    _FSA_ORDR.TTRPT_SHRD_TNANT_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.SharedTenantFlag).ToString();
                    _FSA_ORDR.TTRPT_ALT_ACCS_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AlternateAccessFlag).ToString();
                    _FSA_ORDR.TTRPT_ALT_ACCS_PRVDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.AlternateAccessProviderCode).ToString();
                    _FSA_ORDR.TTRPT_FBR_HAND_OFF_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.FiberHandOffCode).ToString();
                    _FSA_ORDR.TTRPT_MNGD_DATA_SRVC_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.ManagedDataServicesFlag.ToString());
                    _FSA_ORDR.TTRPT_OSS_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.OneStopShopFlag).ToString();
                    _FSA_ORDR.TTRPT_MPLS_BACKHAUL_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.MPLSBackHaulFlag).ToString();
                    _FSA_ORDR.CXR_ACCS_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.CarrierAccessCode).ToString();
                    _FSA_ORDR.INTL_PL_SRVC_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.InternationalPrivateLineServiceTypeCode).ToString();
                    _FSA_ORDR.INTL_PL_CKT_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.InternationalPrivateLineCircuitTypeCode).ToString();
                    _FSA_ORDR.INTL_PL_PLUS_IP_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.InternationalPrivateLinePlusIPFlag.ToString());
                    _FSA_ORDR.INTL_PL_CAT_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.InternationalPrivateLineCategory.ToString());
                    _FSA_ORDR.OSS_OPT_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.OneStopShopOptionCode.ToString());
                }
                catch
                {
                    sError += "Error reading Order.InstallInfo.TransportInstallationInformation.Transport Data.";
                }

                try
                {
                    _FSA_ORDR.TTRPT_TME_SOLT_CNT = Convert.ToInt16(FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.NumberofTimeSlots.ToString()));
                    _FSA_ORDR.TTRPT_STRT_TME_SLOT_NBR = Convert.ToInt16(FixNull(_Order.InstallInfo.TransportInstallInformation.Transport.StartingTimeSlot.ToString()));
                }
                catch
                { }
            }

            ///////////////////////////////////////////////////////////////////////
            //Transport Install - Port Information Properties
            ///////////////////////////////////////////////////////////////////////
            if (_Order.InstallInfo.TransportInstallInformation.Port != null)
            {
                if (_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation != null)
                {
                    _FSA_ORDR.TPORT_IP_VER_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPVersionTypeCode).ToString();
                    _FSA_ORDR.TPORT_IPV4_ADR_PRVDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPv4AddressProviderCode).ToString();
                    _FSA_ORDR.TPORT_IPV4_ADR_QTY = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPv4AddressQuantity).ToString();
                    _FSA_ORDR.TPORT_IPV6_ADR_PRVDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPv6AddressProviderCode).ToString();
                    _FSA_ORDR.TPORT_IPV6_ADR_QTY = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPv6AddressQuantity).ToString();

                    _FSA_ORDR.DMN_NME = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.DomainName).ToString();
                    try
                    {
                        _FSA_ORDR.ADDL_IP_ADR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.AdditionalIPAddressFlag.ToString());
                    }
                    catch
                    { }
                    _FSA_ORDR.IPV4_SUBNET_MASK_ADR = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.IPv4SubnetMask).ToString();
                    _FSA_ORDR.TPORT_XST_IP_CNCTN_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.ExistingIPConnectionFlag).ToString();
                    _FSA_ORDR.TPORT_CURR_INET_PRVDR_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.IPAddressInformation.CurrentInternetProviderCode).ToString();
                }

                _FSA_ORDR.TPORT_USER_TYPE_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.UserType).ToString();
                _FSA_ORDR.TPORT_MULTI_MEG_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.MultiMegFlag).ToString();
                _FSA_ORDR.TPORT_ETHRNT_NRFC_INDCR = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.EthernetInterfaceIndicator).ToString();
                _FSA_ORDR.TPORT_SCRMB_TXT = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.Scrambling).ToString();
                _FSA_ORDR.TPORT_PORT_TYPE_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.PortTypePreference).ToString();
                _FSA_ORDR.TPORT_CNCTR_TYPE_ID = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.ConnectorTypeCode).ToString();
                _FSA_ORDR.TPORT_CUST_ROUTR_TAG_TXT = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.ConnectorRouterTagging).ToString();
                _FSA_ORDR.TPORT_CUST_ROUTR_AUTO_NEGOT_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.CustomerRouterAutoNegotiation).ToString();
                _FSA_ORDR.TPORT_CUST_PRVDD_IP_ADR_INDCR = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.CustomerProvidedIPAddressIndicator).ToString();
                _FSA_ORDR.TPORT_CUST_IP_ADR = GetCustomerIPAddresses();
                _FSA_ORDR.TPORT_BRSTBL_PRICE_DISC_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.BurstableInternetPricingCode).ToString();
                _FSA_ORDR.TPORT_BRSTBL_USAGE_TYPE_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.BurstableUsageTypeCode).ToString();
                _FSA_ORDR.TPORT_PPLVLS = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.PPLVLS).ToString();

                try
                {
                    _FSA_ORDR.SPAE_ACCS_INDCTR = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.SPAEAccessIndicator.ToString());
                }
                catch
                { }

                try
                {
                    _FSA_ORDR.NRFC_PRCOL_CD = FixNull(_Order.InstallInfo.TransportInstallInformation.Port.InterfaceProtocolCode.ToString());
                }
                catch
                { }

                try
                {
                    _FSA_ORDR.TPORT_VLAN_QTY = Convert.ToInt16(FixNull(_Order.InstallInfo.TransportInstallInformation.Port.VLANQuantity).ToString());
                }
                catch
                { }
            }
            return sError;
        }

        #endregion "Read & Load Sub or Partial Form Data"

        #region

        #endregion
        #region "Contact, Billing & Address Objects"

        private ORDR_CNTCT_DB Load_ORDR_CNTCT(ORDR_CNTCT_DB _Contact, OrderContactInfo _ContactInfo)
        {
            _Contact = Load_ORDR_CNTCT_NME(_Contact, _ContactInfo.ContactNameInfo);
            _Contact = Load_ORDR_CNTCT_PHN(_Contact, _ContactInfo.PhoneInfo);

            switch (FixNull(_ContactInfo.Type.ToString()))
            {
                case "Service":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.Service;
                    break;

                case "Primary":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.Primary;
                    break;

                case "Billing":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.Billing;
                    break;

                case "Secondary":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.Secondary;
                    break;

                case "Support":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.Support;
                    break;

                case "CustomerOnsite":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.CustomerOnsite;
                    break;

                case "AlternateSite":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.AlternateSite;
                    break;

                case "ServiceAssurance":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.ServiceAssurance;
                    break;

                case "DNSAdministrator":
                    _Contact.ROLE_ID = (Byte)OrderAddressType.DNSAdministrator;
                    break;

                default:
                    break;
            }
            return _Contact;
        }

        private ORDR_CNTCT_DB Load_ORDR_CNTCT_NME(ORDR_CNTCT_DB _Contact, OrderContactInfoData _ContactInfo)
        {
            if (_ContactInfo != null)
            {
                switch (FixNull(_ContactInfo.ContactType.ToString()))
                {
                    case "Primary":
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Primary;
                        break;

                    case "Secondary":
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Secondary;
                        break;

                    case "Service":
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Service;
                        break;

                    case "Onsite":
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Onsite;
                        break;

                    case "Requestor":
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Requestor;
                        break;

                    default:
                        _Contact.CNTCT_TYPE_ID = (Byte)OrderContactType.Primary;
                        break;
                }

                _Contact.FRST_NME = FixNull(_ContactInfo.FirstName).ToString();
                _Contact.LST_NME = FixNull(_ContactInfo.LastName).ToString();
                _Contact.NME = FixNull(_ContactInfo.Name).ToString();
                _Contact.EMAIL_ADR = FixNull(_ContactInfo.EmailAddress).ToString();
            }
            return _Contact;
        }

        private ORDR_CNTCT_DB Load_ORDR_CNTCT_PHN(ORDR_CNTCT_DB _Contact, OrderPhoneInfoData _ContactInfo)
        {
            if (_ContactInfo != null)
            {
                if (FixNull(_ContactInfo.PhoneType.ToString()).ToUpper() == "PHONE" || FixNull(_ContactInfo.PhoneType.ToString()).ToUpper() == "BILLING")
                {
                    _Contact.PHN_NBR = FixNull(_ContactInfo.Number).ToString();
                }
                else
                {
                    _Contact.FAX_NBR = FixNull(_ContactInfo.Number).ToString();
                }

                _Contact.NPA = FixNull(_ContactInfo.NPA).ToString();
                _Contact.NXX = FixNull(_ContactInfo.NXX).ToString();
                _Contact.STN_NBR = FixNull(_ContactInfo.Station).ToString();
                _Contact.CTY_CD = FixNull(_ContactInfo.CityCode).ToString();
                _Contact.ISD_CD = FixNull(_ContactInfo.CountryCode).ToString();
                _Contact.PHN_EXT_NBR = FixNull(_ContactInfo.Extension).ToString();
            }
            return _Contact;
        }

        private ORDR_ADR_DB Load_ORDR_ADR(ORDR_ADR_DB _Address, OrderContactInfo _ContactInfo)
        {
            switch (FixNull(_ContactInfo.Type.ToString()))
            {
                case "Service":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.Service;
                    break;

                case "Primary":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.Primary;
                    break;

                case "Billing":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.Billing;
                    break;

                case "Secondary":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.Secondary;
                    break;

                case "Support":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.Support;
                    break;

                case "CustomerOnsite":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.CustomerOnsite;
                    break;

                case "AlternateSite":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.AlternateSite;
                    break;

                case "ServiceAssurance":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.ServiceAssurance;
                    break;

                case "DNSAdministrator":
                    _Address.ADR_TYPE_ID = (Byte)OrderAddressType.DNSAdministrator;
                    break;

                default:
                    break;
            }

            _Address.STREET_ADR_1 = FixNull(_ContactInfo.AddressLine1).ToString();
            _Address.STREET_ADR_2 = FixNull(_ContactInfo.AddressLine2).ToString();
            _Address.CTY_NME = FixNull(_ContactInfo.City).ToString();
            _Address.PRVN_NME = FixNull(_ContactInfo.ProvinceMuncipality).ToString();
            _Address.STT_CD = FixNull(_ContactInfo.StateCode).ToString();
            _Address.ZIP_PSTL_CD = FixNull(_ContactInfo.ZIPCode).ToString()
                                    + FixNull(_ContactInfo.ZIPCodePlusFour).ToString()
                                    + FixNull(_ContactInfo.PostalCode).ToString();
            _Address.CTRY_CD = FixNull(_ContactInfo.CountryCode).ToString();

            return _Address;
        }

        private BILL_LINE_ITEM_DB Load_BILL_LINE_ITEM(BILL_LINE_ITEM_DB _Bill, OrderBillingLineItems _BillItem)
        {
            _Bill.BIC_CD = FixNull(_BillItem.BICCode).ToString();
            _Bill.LINE_ITEM_DES = FixNull(_BillItem.Description).ToString();
            try
            {
                _Bill.LINE_ITEM_QTY = Convert.ToInt16(FixNull(_BillItem.Quantity).ToString());
            }
            catch
            {
                _Bill.LINE_ITEM_QTY = 0;
            }
            _Bill.ITEM_CD = FixNull(_BillItem.ItemCode).ToString();
            _Bill.ITEM_DES = FixNull(_BillItem.ItemDescription).ToString();
            _Bill.MRC_CHG_AMT = FixNull(_BillItem.MonthlyRecurringChargeAmount).ToString();
            _Bill.NRC_CHG_AMT = FixNull(_BillItem.NonRecurringChargeAmount).ToString();
            _Bill.ACTN_CD = FixNull(_BillItem.ActionCode.ToString());

            return _Bill;
        }

        #endregion

        #region "Convertors"

        private Object DeserializeXML(string sXML)
        {
            XmlSerializer _XS = new XmlSerializer(typeof(Order));
            MemoryStream _MS = new MemoryStream(StringToUTF8ByteArray(sXML));
            //XmlTextWriter _XW = new XmlTextWriter(_MS, Encoding.UTF8);
            return _XS.Deserialize(_MS);
        }

        private string ReturnTrimmedString(string _String)
        {
            if (_String.Length > 0)
            {
                _String = _String.Substring(0, _String.Length - 1);
            }

            return _String;
        }

        private Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        private bool ConvertStringToBool(string sInput)
        {
            sInput = sInput.ToUpper();
            return ((sInput == "Y") || (sInput == "YES") || (sInput == "ON")) ? true : false;
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        #endregion
    }
}