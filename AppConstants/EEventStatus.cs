﻿namespace AppConstants
{
    public enum EEventStatus
    {
        Visible = 1,
        Pending,
        Rework,
        Published,
        InProgress,
        Completed,
        OnHold,
        Delete,
        Fulfilling,
        Shipped,
        Cancelled = 13,
        PendingDeviceReturn = 16,
        Submitted = 17,
        CompletePendingUAT = 18,
        InProgressUAT = 19,
        OnHoldUAT = 20
    }
}