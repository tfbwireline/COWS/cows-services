﻿namespace AppConstants
{
    public enum EEmailStatus
    {
        OnHold = 9,
        Pending = 10,
        Success = 11,
        Failure = 12
    }
}