################################################################################
#
# Copyright (c) 2011 Sprint Communications Company
#
# The content of this document is PROPRIETARY INFORMATION, and is the sole
# and exclusive property of Sprint. Information provided in this document is
# classified as SPRINT RESTRICTED. Originator must grant permission before
# reproductions of this document can be made. If you need to dispose of any
# hard copies of this document, please shred it.
#
################################################################################
#
# This is the application configuration file. All configuration settings for the
# application are consolidated into this one place, and the various config files
# are initialized from values in this configuration. This permits the app to be
# environment-neutral. 
#
# Configuration settings are key/value pairs. The key is a unique identifier. 
# Values are text strings. Values may contain nested config settings. For example
# if you have defined a config setting named XYZ you can use it in another config
# setting (ex: ABC = 123.%XYZ%.456).
#
# It is recommended to use the forward slash when possible in file paths. Note 
# that backslashes must be escaped (ex: c:\\bea) if they are used.  
#
# Each application has a master configuration file. This config file has all the
# settings and has default values where appropriate. The master config file is
# used to validate the local config file. If there are settings in the master 
# config file that aren't in the local config file the configuration will fail. 
# Whenever the application adds a new configuration setting it should always be
# added to the master config file.
#
# There is script to validate a local configuration against the master. It will 
# report any inconsistencies it finds.
#
# There is another script to update a local configuration from the master. If 
# the local configuration needs updating the current configuration will be saved
# and annotated with the deltas and a new configuration will be written with the
# updates from the master. 
# 
################################################################################


################################################################################
#
# The following settings are general settings for all components.
#
################################################################################
# location where Java is installed (where bin/java or bin\\java.exe is expected)
JAVA_HOME = 
# writable folder for writing log files
LOGFILE_HOME = 
SERVICES_HOME =
WEB_HOME =
WEBSITE_HOME =
#Data files folder
DATA_HOME = 
# environment name for performing environment checks etc (ex: PROD, PRODSUPT, DEV1, etc)
ENV_NAME = 
# environment-specific prefix for service names (usually %ENV_NAME%_ when used)
SVC_ENV_PREFIX = %ENV_NAME%_

# dirs/files (if any) to persist during deploy process (space-delimited list in ant format)
DEPLOY_PERSISTENT_PATHS = Logs/** Data/** Application/COWS/Services Application/COWS Web/* Web/COWS/aspnet_client/** Application/COWS/QDAUSR/mqs** Application/COWS/Services/COWS.ODIESenderService Application/COWS/Services/COWS.ODIEReceiverService Application/COWS/Services/COWS.FSAService Application/COWS/Services/COWS.EmailSender Application/COWS/Services/COWS.TadpoleService Application/COWS/Services/COWS.RecurringApptParser
Application/COWS/Services/COWS.Mach5SenderService Application/COWS/Services/COWS.PeopleSoftBatchService Application/COWS/Services/COWS.RulesetProcessor Application/COWS/Services/COWS.SSTATSenderService Application/COWS/Services/COWS.SSTATReceiverService
Application/COWS/Services/COWS.AutoReportingDx

# list of dirs/files (if any) to ignore when backing up current build
BACKUP_IGNORE_PATHS = Logs/** Data/** Web/COWS/aspnet_client/**

# MQ connectivity setttings
DEFAULT_MQ_CHANNEL = 
DEFAULT_MQ_HOST_NAME =  
DEFAULT_MQ_QUEUE_MANAGER = 

# IIS App pool name
COWS_APP_POOL_NAME = 

################################################################################
#
# Application Wide Settings
#
################################################################################
# For log_cleaner.cmd Script.
LOGS_DAYS_TO_PURGE = 60

# Global database connection string.
COWS_DB_CONNECT_STRING = 

################################################################################
#
# The following settings are for the COWS SNAPSHOT Jobs.
#
################################################################################
DB_JOB_CONNECT_STRING =
DB_JOB_START_READONLY =
DB_JOB_STOP_READONLY = 

################################################################################
#
# The following settings are for the Certificate Monitor Jobs.
#
################################################################################

CERT_LOCATION = 

################################################################################
#
# The following settings are for the Staging Folder Monitor Jobs.
#
################################################################################
DEPLOY_EMAIL_ADDRESS = "WholesaleServiceProductionSupport-DeploymentTeam@sprint.com"

################################################################################
#
# The following settings are for the COWS web app.
#
################################################################################
COWS_CONNECT_STRING = 
COWS_SVCS_CONNECT_STRING =
COWS_APP_SVCS_CONNECT_STRING = data source=.\\SQLEXPRESS;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\\aspnetdb.mdf;User Instance=true
COWS_REPORTING_CONNECT_STRING = 
COWS_ON_REPORTING_CONNECT_STRING =
COWS_CACHE_TIMEOUT =
COWS_CALENDAR_ENTRY_URL =
COWS_VIEW_EVENT_URL =
COWS_MAIL_SERVER_NAME = mailhost.corp.sprint.com
COWS_DEFAULT_FROM_EMAIL = COWSDev@sprint.com
COWS_ADHOC_REPORT_NOTIFY = 
COWS_EVENT_MAIL = 
COWS_MDS_SHIPPED_EMAIL = 
COWS_RETRACT_MAIL = 
COWS_CPEORDR_XSLT = 
COWS_DESIGNDOC_XSLT = 

COWS_LOG_APP_DIR = %LOGFILE_HOME%\\COWS
COWS_LOG_DIRECTORY_LIST = %LOGFILE_HOME%\\COWS\\Main
COWS_DETAIL_LOG = 
COWS_ERROR_MAIL = jagannath.r.gangi@sprint.com
COWS_MDSRedesignLkp = 1
COWS_REJECT_MAIL = 
COWS_FEDLINE_REJECT_MAIL = 
COWS_REGION = 

COWS_OSXslt = E:\\TEST3\\QDA\\Web\\COWS\\xslt\\OpenSlotsDisplay.xslt

COWS_PAGESIZE = 
COWS_EXPORT_FOLDER = 
COWS_VENDORCERTOrange = 
COWS_Orange_EOC = 
COWS_VENDORCERTNAVEGA = 
COWS_NAVEGA_EOC = 
COWS_EOCPDL = 
COWS_VNDRENCRYPTFLG = 
COWS_CHARTIMAGE_HANDLER = 
COWS_MSMGR_FILE_NAME = 
COWS_CPEMNS_FILE_NAME = 

COWS_BLOCK_UI = false
COWS_BLOCK_UI_MESSAGE = COWS\ Read\ Only\ -\ Maintenance\ Mode
COWS_BLOCKED_IDS = btnSave,btnSave2,lkbDelete,btnDelete,btnCalculate,btnUpdate,btnUpdateSched,btnAddADID,btnUpdateRoles,btnAddAssignment,lkbDelete_Click,btnAlterMan,lbAdd,lbRemove,btnNewEvent,btnEditView,btnOk,btnAdd,btnLookupFTN,lkbRemove,btnCheckCurrentSlot,btnLaunchPicker,imgbtnClear,btnPplList,btnCheckPpl,btnLookUp,btnRDInfo,btnMDAdd,btnOEAdd,btnADInfo,btnMDAddData,btnWTAdd,btnSlotPicker,btnActivatorPicker,lkbOERemove,lkbWTRemove,btnDeleteFolder,btnEditFolder,btnSubmit,btnH5New,btnACCIADD,btnVenOrdNew,btnAttach,btnADDCM,btnVLANAddt,btnHoldUnHold,btnCIADD,btnSaveChanges,btnCalculateftr,btnMove,btnComplete,btnRTS,btnRTSBottom,btnACRComplete,btnACRComplete2,btngrvAssign,btngrvUnAssign,btnAllSlots,btnUploadFiles,btnCreateOrder,btnUpdateOrder,btnUploadForm,btnAddEmail,ibDelete,btnSOWGo,lbEventHist,UploadForm,imgGeneralReset,imgGeneralSave,fuAttachment1,fuAttachment2,fuAttachment3,fuAttachment4,fuAttachment5,btnAttachEmailDoc,btnAddVendorForm,btnSendEmail,btnSaveEmail,ibDeleteAttch,btnSaveAck,ContentPlaceHolder1_pnlUpload,imbDelete

COWS_ASPNET_MAXHTTPCOLLECTIONKEYS = 

COWS_XSD_PATH =
COWS_ARCHIVED_REDESIGNS =
COWS_VIEW_CPT_URL =
COWS_VIEW_CPE_CLLI_URL =
COWS_SIPT_DOC_URL =

    
################################################################################
#
# The following settings are for the COWS Odie Sender windows service.
#
################################################################################
ODIE_SENDER_SVC_STARTUP_TYPE = Automatic
ODIE_SENDER_SVC_MQ_HOST = 
ODIE_SENDER_SVC_MQ_QUEUE_MGR = 
ODIE_SENDER_SVC_MQ_CHANNEL = 
ODIE_SENDER_SVC_MQ_QUEUE_IN = 
ODIE_SENDER_SVC_MQ_QUEUE_OUT = 
ODIE_SENDER_SVC_MQ_CHARSET = 
ODIE_SENDER_SVC_MQ_FORMAT = 
ODIE_SENDER_SVC_MQ_WAIT_INTERVAL = 
ODIE_SENDER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%
ODIE_SENDER_SVC_LOG_USER = 
ODIE_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
ODIE_SENDER_SVC_LOG_TIMER = 180000
ODIE_SENDER_SVC_LOG_TYPE = Detail
ODIE_SENDER_SVC_LOG_QUEUE_SIZE = 200
ODIE_SENDER_SVC_LOG_XSL = 
ODIE_SENDER_SVC_LOG_NAME_LIST = Main
ODIE_SENDER_SVC_LOG_MODE = Text
ODIE_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

    
################################################################################
#
# The following settings are for the COWS Odie Receiver windows service.
#
################################################################################
ODIE_RECEIVER_SVC_STARTUP_TYPE = Automatic
ODIE_RECEIVER_SVC_MQ_HOST = 
ODIE_RECEIVER_SVC_MQ_QUEUE_MGR = 
ODIE_RECEIVER_SVC_MQ_CHANNEL = 
ODIE_RECEIVER_SVC_MQ_QUEUE_IN = 
ODIE_RECEIVER_SVC_MQ_QUEUE_OUT = 
ODIE_RECEIVER_SVC_MQ_CHARSET = 
ODIE_RECEIVER_SVC_MQ_FORMAT = 
ODIE_RECEIVER_SVC_MQ_WAIT_INTERVAL = 
ODIE_RECEIVER_SVC_LOG_DIR_LIST = %LOGFILE_HOME%
ODIE_RECEIVER_SVC_LOG_USER = 
ODIE_RECEIVER_SVC_LOG_QUEUE_GROW_FACTOR = 4
ODIE_RECEIVER_SVC_LOG_TIMER = 180000
ODIE_RECEIVER_SVC_LOG_TYPE = Detail
ODIE_RECEIVER_SVC_LOG_QUEUE_SIZE = 200
ODIE_RECEIVER_SVC_LOG_XSL = 
ODIE_RECEIVER_SVC_LOG_NAME_LIST = Main
ODIE_RECEIVER_SVC_LOG_MODE = Text
ODIE_RECEIVER_SVC_LOG_QUEUE_FLUSH_SIZE = 200


################################################################################
#
# The following settings are for the COWS EmailSender windows service.
#
################################################################################
Email_Sender_SVC_AdhocReportNotify = 
Email_Sender_SVC_CalenderEntryURL = 
Email_Sender_SVC_CancelEmailCCList = 
Email_Sender_SVC_CANDEventEmail = %SERVICES_HOME%\\COWS.EmailSender\\CANDEventEmail.xslt
Email_Sender_SVC_CANDRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\CANDRetractEmail.xslt
Email_Sender_SVC_SIPTEventEmail = %SERVICES_HOME%\\COWS.EmailSender\\SIPTEventEmail.xslt
Email_Sender_SVC_SIPTRetractEmail = %SERVICES_HOME%\\COWS.EmailSender\\SIPTRetractEmail.xslt
Email_Sender_SVC_MDSCPEFTEmail =
Email_Sender_SVC_MDSCPEEmail = 
Email_Sender_SVC_UCaaSCPEEmail = 
Email_Sender_SVC_CPEFTEmail = 
Email_Sender_SVC_CustIDUserAssignEmail = 
Email_Sender_SVC_DefaultFromEmail = 
Email_Sender_SVC_EmailThreadActive = 
Email_Sender_SVC_EOCThreadActive = 
Email_Sender_SVC_FedlineEmail =
Email_Sender_SVC_GomPLCompEmail = 
Email_Sender_SVC_GomPLCompPDL = 
Email_Sender_SVC_GomPLCompPDLcc = 
Email_Sender_SVC_LOG_DIR_LIST = %LOGFILE_HOME%\\COWS.EmailSender
Email_Sender_SVC_LOG_MODE = 
Email_Sender_SVC_LOG_NAME_LIST = 
Email_Sender_SVC_LOG_QUEUE_FLUSH_SIZE =
Email_Sender_SVC_LOG_QUEUE_GROW_FACTOR = 
Email_Sender_SVC_LOG_QUEUE_SIZE = 
Email_Sender_SVC_LOG_TIMER = 
Email_Sender_SVC_LOG_TYPE = 
Email_Sender_SVC_LOG_USER = 
Email_Sender_SVC_LOG_XSL = 
Email_Sender_SVC_MDSFTEmail = 
Email_Sender_SVC_MDSRetractEmail = 
Email_Sender_SVC_UCaaSRetractEmail = 
Email_Sender_SVC_MDSShippedEmail = 
Email_Sender_SVC_MDSXLST =
Email_Sender_SVC_UCaaSEmail =
Email_Sender_SVC_NCCOOrderEmail = 
Email_Sender_SVC_ReworkEmail = 
Email_Sender_SVC_ReworkThreadActive = 
Email_Sender_SVC_Server_Name = 
Email_Sender_SVC_SleepTime = 
Email_Sender_SVC_STARTUP_TYPE = 

Email_Sender_SVC_ViewEventURL =
Email_Sender_SVC_XSLTPath = 
Email_Sender_SVC_GOM_IPLIPASR =
Email_Sender_SVC_ASREmail = 
Email_Sender_SVC_ASREmailTo = 
Email_Sender_SVC_ASREmailToDmstc =
Email_Sender_SVC_ASREmailCC = 
Email_Sender_SVC_MDSSrvcActvnEmail =
Email_Sender_SVC_MDSSrvcCommentsEmail =
Email_Sender_SVC_RedesignEmail =  
Email_Sender_SVC_RedesignURL =
Email_Sender_SVC_CPTNotificationEmail =  
Email_Sender_SVC_CPTCompleteEmail = 
Email_Sender_SVC_FedlineReorderNtfyEmail =
Email_Sender_SVC_FedlineReorderEmailTo =
Email_Sender_SVC_FedlineReorderEmailCC = 
Email_Sender_SVC_SDEOpportunityEmail = 
Email_Sender_SVC_ViewSDEUrl = 
Email_Sender_SVC_DownloadSDEURL = 
Email_Sender_SVC_ZscalerDiscEmail =
Email_Sender_SVC_CradlePointShippedEmail =
Email_Sender_SVC_CLLIRequestEmail =
Email_Sender_SVC_ISInitiateBillOnlyEmail =

################################################################################
#
# The following settings are for the COWS Tadpole windows service.
#
################################################################################
TADPOLE_SSL_CIPHER_SPEC = 
TADPOLE_SSL_KEY_REPOSITORY = 
TADPOLE_SVC_CHARACTER_SET =
TADPOLE_SVC_DEFAULT_FROM_EMAIL =
TADPOLE_SVC_DETAIL_LOG =
TADPOLE_SVC_ERROR_EMAIL_LIST = 
TADPOLE_SVC_FORMAT =
TADPOLE_SVC_LOG_DIRECTORY_LIST =
TADPOLE_SVC_LOG_MODE =
TADPOLE_SVC_LOG_NAME_LIST =
TADPOLE_SVC_LOG_QUEUE_FLUSHSIZE =
TADPOLE_SVC_LOG_QUEUE_GROW_FACTOR =
TADPOLE_SVC_LOG_QUEUE_SIZE =
TADPOLE_SVC_LOG_TIMER =
TADPOLE_SVC_LOG_TYPE =
TADPOLE_SVC_LOG_USER =
TADPOLE_SVC_LOG_XSL =
TADPOLE_SVC_MQ_CHANNEL =
TADPOLE_SVC_MQ_HOST_NAME =
TADPOLE_SVC_MQ_QUEUE_IN =
TADPOLE_SVC_MQ_QUEUE_MANAGER =
TADPOLE_SVC_MQ_QUEUE_OUT =
TADPOLE_SVC_RECEIVER_SLEEP_TIME =
TADPOLE_SVC_SENDER_SLEEP_TIME =
TADPOLE_SVC_WAIT_INTERVAL =
TADPOLE_SVC_XSD_PATH =
TADPOLE_SVC_SERVER_NAME =

################################################################################
#
# The following settings are for the COWS RecurringApptParser windows service.
#
################################################################################
RECURRING_APPT_PARSER_SVC_SLEEP_TIME = 
RECURRING_APPT_PARSER_SVC_SERVER_NAME = 
RECURRING_APPT_PARSER_SVC_DEFAULT_FROM_EMAIL = 
RECURRING_APPT_PARSER_SVC_COWS_REGION = 
RECURRING_APPT_PARSER_SVC_LOG_DIRECTORY_LIST = 
RECURRING_APPT_PARSER_SVC_LOG_USER = 
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_GROW_FACTOR = 
RECURRING_APPT_PARSER_SVC_LOG_TIMER = 
RECURRING_APPT_PARSER_SVC_LOG_TYPE = 
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_SIZE = 
RECURRING_APPT_PARSER_SVC_LOG_XSL = 
RECURRING_APPT_PARSER_SVC_LOG_NAME_LIST = 
RECURRING_APPT_PARSER_SVC_LOG_MODE = 
RECURRING_APPT_PARSER_SVC_LOG_QUEUE_FLUSH_SIZE = 

################################################################################
#
# The following settings are for the COWS AutoReportingDx windows service.
#
################################################################################
AUTO_REPORTING_DX_SVC_ClientSettingsProvider_ServiceURI = 
AUTO_REPORTING_DX_SVC_TimerCycle = 
AUTO_REPORTING_DX_SVC_HeartbeatCycle = 
AUTO_REPORTING_DX_SVC_LogFileName = 
AUTO_REPORTING_DX_SVC_LogFileDelAfterDays = 
AUTO_REPORTING_DX_SVC_LogFileWidth = 
AUTO_REPORTING_DX_SVC_DataCurrMins = 
AUTO_REPORTING_DX_SVC_SMTPHost = 
AUTO_REPORTING_DX_SVC_SMTPPort = 
AUTO_REPORTING_DX_SVC_SMTPUID = 
AUTO_REPORTING_DX_SVC_SMTPPassword = ?k2u
AUTO_REPORTING_DX_SVC_TempFilePath =

AUTO_REPORTING_DX_SVC_ENVIRONMENT = 
AUTO_REPORTING_DX_SVC_APP_FILE_SERVER = 
AUTO_REPORTING_DX_SVC_LOG_FILE_PATH = 
AUTO_REPORTING_DX_SVC_LOG_FILE_PATH_ALT =
AUTO_REPORTING_DX_SVC_SQL_SERVER = 
AUTO_REPORTING_DX_SVC_SQL_CONN = 
AUTO_REPORTING_DX_SVC_SQLOLEDB = 
AUTO_REPORTING_DX_SVC_FTP_SERVER = 
AUTO_REPORTING_DX_SVC_APP_FILE_SERVER = 
AUTO_REPORTING_DX_SVC_SMTP_TO =
AUTO_REPORTING_DX_SVC_SMTP_FROM = 
AUTO_REPORTING_DX_SVC_SMTP_DEFAULT_CC =
AUTO_REPORTING_DX_SVC_SMTP_DEFAULT_BCC =
AUTO_REPORTING_DX_SVC_DEVEMAIL_FROM = 
AUTO_REPORTING_DX_SVC_DEVEMAIL_TO = 
AUTO_REPORTING_DX_SVC_DEVEMAIL_CC =
AUTO_REPORTING_DX_SVC_DEVEMAIL_BCC =
AUTO_REPORTING_DX_SVC_FTP_FILE_PATH =

################################################################################
#
# The following settings are for the COWS PeopleSoft Batch windows service.
#
################################################################################
PSOFT_BATCH_SVC_STARTUP_TYPE = Automatic
PSOFT_BATCH_SVC_REQ_SLEEP_TIME = 
PSOFT_BATCH_SVC_EXC_MINUTE_TIME = 
PSOFT_BATCH_SVC_REQ_FILE_PATH = 
PSOFT_BATCH_SVC_REC_FILE_PATH = 
PSOFT_BATCH_SVC_ARCHIVE_REQ_FILE_PATH = 
PSOFT_BATCH_SVC_ARCHIVE_REC_FILE_PATH = 
PSOFT_BATCH_SVC_LOG_DIR_LIST = 
PSOFT_BATCH_SVC_LOG_USER = 
PSOFT_BATCH_SVC_LOG_QUEUE_GROW_FACTOR = 4
PSOFT_BATCH_SVC_LOG_TIMER = 180000
PSOFT_BATCH_SVC_LOG_TYPE = Detail
PSOFT_BATCH_SVC_LOG_QUEUE_SIZE = 200
PSOFT_BATCH_SVC_LOG_XSL = 
PSOFT_BATCH_SVC_LOG_NAME_LIST = Main
PSOFT_BATCH_SVC_LOG_MODE = Text
PSOFT_BATCH_SVC_LOG_QUEUE_FLUSH_SIZE = 200

PSOFT_BATCH_SVC_LOCALFILEPATH =
PSOFT_BATCH_SVC_FILENAMEREQUEST =
PSOFT_BATCH_SVC_FILENAMERECEIPTS =
PSOFT_BATCH_SVC_MYBATCHFILE =
PSOFT_BATCH_SVC_TARGETDIR =
PSOFT_BATCH_SVC_SPATHREQUEST =
PSOFT_BATCH_SVC_SPATHRECEIPT =
PSOFT_BATCH_SVC_SSYS =
PSOFT_BATCH_SVC_DSYS =
PSOFT_BATCH_SVC_TNAME  =
PSOFT_BATCH_SVC_DELETE =
PSOFT_BATCH_SVC_EMALADDR =
PSOFT_BATCH_SVC_EMAIL =


################################################################################
#
# The following settings are for the COWS Mach5 Sender windows service.
#
#################################################################################
MACH5SENDER_SVC_STARTUP_TYPE = Automatic
MACH5_SENDER_SVC_REQ_SLEEP_TIME = 
MACH5_SENDER_SVC_LOG_DIR_LIST = 
MACH5_SENDER_PS_KEY_NAME = 
MACH5_SENDER_RCS_KEY_NAME = 
MACH5_SENDER_URL_VALUE = 
MACH5_SENDER_SVC_LOG_USER = 
MACH5_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
MACH5_SENDER_SVC_LOG_TIMER = 180000
MACH5_SENDER_SVC_LOG_TYPE = Detail
MACH5_SENDER_SVC_LOG_QUEUE_SIZE = 200
MACH5_SENDER_SVC_LOG_XSL = 
MACH5_SENDER_SVC_LOG_NAME_LIST = Main
MACH5_SENDER_SVC_LOG_MODE = Text
MACH5_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 200
MACH5_SENDER_SVC_SHAREDSECRET =
MACH5_SENDER_SVC_URL =
MACH5_SENDER_SVC_REDESIGNSHAREDSECRET = 
MACH5_SENDER_REDESIGN_URL_VALUE = 
################################################################################
#
# The following settings are for the COWS SSTAT Receiver windows service.
#
################################################################################
SSTAT_RECEIVER_SVC_STARTUP_TYPE = Automatic
SSTAT_RECEIVER_SVC_SLEEP_TIME = 
SSTAT_RECEIVER_SVC_MQ_HOST = 
SSTAT_RECEIVER_SVC_MQ_QUEUE_MGR = 
SSTAT_RECEIVER_SVC_MQ_CHANNEL = 
SSTAT_RECEIVER_SVC_MQ_QUEUE_IN = 
SSTAT_RECEIVER_SVC_MQ_QUEUE_OUT = 
SSTAT_RECEIVER_SVC_MQ_CHARSET = 1208
SSTAT_RECEIVER_SVC_MQ_FORMAT = 
SSTAT_RECEIVER_SVC_MQ_WAIT_INTERVAL = 2000
SSTAT_RECEIVER_SVC_LOG_DIR_LIST = 
SSTAT_RECEIVER_SVC_LOG_USER = 
SSTAT_RECEIVER_SVC_LOG_QUEUE_GROW_FACTOR = 4
SSTAT_RECEIVER_SVC_LOG_TIMER = 180000
SSTAT_RECEIVER_SVC_LOG_TYPE = Detail
SSTAT_RECEIVER_SVC_LOG_QUEUE_SIZE = 200
SSTAT_RECEIVER_SVC_LOG_XSL = 
SSTAT_RECEIVER_SVC_LOG_NAME_LIST = Main
SSTAT_RECEIVER_SVC_LOG_MODE = Text
SSTAT_RECEIVER_SVC_LOG_QUEUE_FLUSH_SIZE = 200

################################################################################
#
# The following settings are for the COWS SSTAT Sender windows service.
#
################################################################################
SSTAT_SENDER_SVC_STARTUP_TYPE = Automatic
SSTAT_SENDER_SVC_SLEEP_TIME = 
SSTAT_SENDER_SVC_MQ_HOST = 
SSTAT_SENDER_SVC_MQ_QUEUE_MGR = 
SSTAT_SENDER_SVC_MQ_CHANNEL = 
SSTAT_SENDER_SVC_MQ_QUEUE_IN = 
SSTAT_SENDER_SVC_MQ_QUEUE_OUT = 
SSTAT_SENDER_SVC_MQ_CHARSET = 1208
SSTAT_SENDER_SVC_MQ_FORMAT = 
SSTAT_SENDER_SVC_MQ_WAIT_INTERVAL = 2000
SSTAT_SENDER_SVC_LOG_DIR_LIST = 
SSTAT_SENDER_SVC_LOG_USER = 
SSTAT_SENDER_SVC_LOG_QUEUE_GROW_FACTOR = 4
SSTAT_SENDER_SVC_LOG_TIMER = 180000
SSTAT_SENDER_SVC_LOG_TYPE = Detail
SSTAT_SENDER_SVC_LOG_QUEUE_SIZE = 200
SSTAT_SENDER_SVC_LOG_XSL = 
SSTAT_SENDER_SVC_LOG_NAME_LIST = Main
SSTAT_SENDER_SVC_LOG_MODE = Text
SSTAT_SENDER_SVC_LOG_QUEUE_FLUSH_SIZE = 

################################################################################
#
# The following settings are for the COWS RulesetProcessor Service
#
################################################################################
#
# ENVIRONMENT CONFIGURATION FOR BPM
#
# https://appiandev0.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService				
# https://appiandev1.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService				
# https://appiandev2.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiandev4.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest2.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appiantest3.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://appianbreakfix.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
# https://bpmcore.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
#
# CoWs.D@1!4NsF --> Dev1
# CoWs.D@2!4NsF --> Dev2
# CoWs.D@3!4NsF --> Dev3
# CoWs.R@1!4NsF --> RTB1
# CoWs.R@2!4NsF --> RTB2
# CoWs.R@3!4NsF --> RTB3
#
################################################################################
RULESETPROCESSOR_SVC_STARTUP_TYPE = Automatic
RULESETPROCESSOR_SVC_SLEEP_TIME = 
RULESETPROCESSOR_SVC_EXECAPPLIST = 
RULESETPROCESSOR_SVC_EXECHOURTIMER = 
RULESETPROCESSOR_SVC_EXECMINUTETIMER = 
RULESETPROCESSOR_SVC_EXECSECONDTIMER = 
RULESETPROCESSOR_SVC_AUDITFILEPATH = 
RULESETPROCESSOR_SVC_ARCHIVEAUDITFILEPATH = 
RULESETPROCESSOR_SVC_EDWDIRECTORY = 
RULESETPROCESSOR_SVC_LOG_DIR_LIST = 
RULESETPROCESSOR_SVC_LOG_USER = 
RULESETPROCESSOR_SVC_LOG_QUEUE_GROW_FACTOR = 4
RULESETPROCESSOR_SVC_LOG_TIMER = 180000
RULESETPROCESSOR_SVC_LOG_TYPE = Detail
RULESETPROCESSOR_SVC_LOG_QUEUE_SIZE = 200
RULESETPROCESSOR_SVC_LOG_XSL = 
RULESETPROCESSOR_SVC_LOG_NAME_LIST = Main
RULESETPROCESSOR_SVC_LOG_MODE = Text
RULESETPROCESSOR_SVC_LOG_QUEUE_FLUSH_SIZE = 200
RULESETPROCESSOR_BPM_REDESIGN_URL = https://appiantest3.sprint.com/suite/webservice/processmodel/NSF_MNS_Redsgn_WebService
RULESETPROCESSOR_BPM_REDESIGN_USERNAME = qda-appian
RULESETPROCESSOR_BPM_REDESIGN_PASSWORD = CoWs.R@3!4NsF
RULESETPROCESSOR_EXECACTIVETHREAD = false,false,true