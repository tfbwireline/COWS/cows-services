@echo off
REM *******************************************************************************
REM Copyright (c) 2015 by Sprint Nextel
REM  Modified By : Jagannath Gangi
REM Added ANT script call to Unzip and copy/merge the contents to application home.
REM If package .zip is not present then skip and deploy&configure the contents from staging.
REM *******************************************************************************


REM
REM initialize some variables and do some validation
REM
SETLOCAL
SET CMDARG=%~dp0
IF /I "%CMDARG:~-8%" EQU "scripts\" (
  IF /I "%CMDARG:~-17%" NEQ "\staging\scripts\" IF /I "%CMDARG:~-16%" NEQ "\backup\scripts\" GOTO location_ok
  ECHO ERROR: deploy.cmd must not be run under the staging or backup directory
  EXIT /B 12
)
ECHO ERROR: deploy.cmd must be run in the application codebase
EXIT /B 11
:location_ok
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
SET CFG_FILE=%APP_HOME%\Framework\etc\config.properties
SET BACKUP_CFG_FILE=%APP_HOME%\Backup\Framework\etc\config.properties
IF NOT EXIST "%CFG_FILE%" (
  REM If it doesn't exist, check in the backup location
  IF NOT EXIST "%BACKUP_CFG_FILE%" (
    ECHO ERROR: Configuration file '%CFG_FILE%' not found
    EXIT /B 13
  )
  ECHO Copying config file from backup.
  COPY "%BACKUP_CFG_FILE%" "%CFG_FILE%"
) 
IF NOT EXIST "%CFG_FILE%.local" (
  REM If it doesn't exist, check in the backup location
  IF EXIST "%BACKUP_CFG_FILE%.local" (
    ECHO Copying local config file from backup.
    COPY "%BACKUP_CFG_FILE%.local" "%CFG_FILE%.local"
  )
)

REM
REM pull JAVA_HOME out of configuration (trimming if necessary) and validate it
REM
SET JAVA_HOME=
FOR /F "usebackq delims== tokens=1,2" %%i IN (`cscript /NoLogo "%_@%" JAVA_HOME "%APP_HOME%\Framework\etc\config.properties.local"`) DO (
  SET JAVA_HOME=%%j
)
IF NOT DEFINED JAVA_HOME (
  FOR /F "usebackq delims== tokens=1,2" %%i IN (`cscript /NoLogo "%_@%" JAVA_HOME "%APP_HOME%\Framework\etc\config.properties"`) DO (
    SET JAVA_HOME=%%j
  )
)

SET JAVA_HOME=E:\Program Files (x86)\Java\jre1.8.0_201
IF EXIST "%JAVA_HOME%\" GOTO JAVA_HOME_OK
SET JAVA_HOME=E:\Program Files (x86)\Java\jre6

:JAVA_HOME_OK
IF NOT DEFINED JAVA_HOME (
  ECHO ERROR: configuration setting 'JAVA_HOME' not found in configuration
  EXIT /B 5
)
SET JAVA_HOME=%JAVA_HOME:/=\%
IF NOT EXIST "%JAVA_HOME%\bin\java.exe" (
  ECHO ERROR: configuration setting 'JAVA_HOME' has an invalid value
  EXIT /B 6
)

REM
REM Call ANT script to unzip package zip file to application home
REM
SET ANT_HOME=%APP_HOME%\Framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET UNZIP_SCRIPT=%APP_HOME%\Scripts\_unzipbuild.xml
IF EXIST "%ANT_PROG%" (
  IF NOT EXIST "%UNZIP_SCRIPT%" GOTO deploy-configure
)
CALL "%ANT_PROG%" -f "%UNZIP_SCRIPT%" -q _unzip.build
IF ERRORLEVEL 1 EXIT /B 24

SET ANT_SCRIPT=%APP_HOME%\Scripts\_admin.xml
REM
REM we are ready for stage 1, call ant to check if ant-contrib is available
REM
CALL "%ANT_PROG%" -f "%ANT_SCRIPT%" -q _check.ant-contrib
IF ERRORLEVEL 1 EXIT /B 19

:deploy-configure
ECHO Calling deploy.cmd now...
REM
REM Call deploy.cmd
REM
CALL %APP_HOME%\Scripts\deploy.cmd

REM
REM Call configure.cmd
REM
CALL %APP_HOME%\Scripts\configure.cmd

CALL "%ANT_PROG%" -f "%UNZIP_SCRIPT%" -q _build.delete

PAUSE

