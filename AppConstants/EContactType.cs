﻿namespace AppConstants
{
    public enum EContactType
    {
        Primary = 1,
        Secondary,
        Service,
        OnSite,
        Requestor,
        IPLOrgnAdrDrop1SiteContact,
        IPLOrgnAdrDrop1AlternateSiteContact,
        IPLOrgnAdrDrop1ServiceAssuranceContact,
        IPLTrmtnAdrDrop1SiteContact,
        IPLTrmtnAdrDrop1AlternateSiteContact,
        IPLTrmtnAdrDrop1ServiceAssuranceContact
    }
}