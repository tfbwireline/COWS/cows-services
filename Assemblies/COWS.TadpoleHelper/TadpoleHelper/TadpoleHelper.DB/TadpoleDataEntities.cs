﻿using System;
using System.Collections.Generic;

namespace TadpoleHelper
{
    public class TadpoleEventDataEntity
    {
        public int FedlineEventID { get; set; }
        public int EventID { get; set; }
        public int FRBRequestID { get; set; }
        public int SequenceNumber { get; set; }
        public string Title { get; set; }
        public string FRBOrderType { get; set; }
        public ActionType OrderType { get; set; }
        public string DesignType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? FRBSentDate { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationID { get; set; }
        public string DeviceID { get; set; }
        public string DeviceSerialNumber { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceVendor { get; set; }
        public List<TadpoleContact> SiteContacts { get; set; }
        public List<TadpoleContact> ShippingContacts { get; set; }
        public List<TadpoleAddress> Addresses { get; set; }
        public List<TadpoleConfig> Configs { get; set; }
        public byte Status { get; set; }
        public string ChangeLog { get; set; }
        public bool isMove { get; set; }
        public bool isExpedite { get; set; }
        public string ExistingDevice { get; set; }
        public string OrderSubType { get; set; }
        public string FRBInstallType { get; set; }
        public string ReplacementReason { get; set; }
        public byte ActionID { get; set; }
        public string OrigDeviceID { get; set; }
        public string OrigDeviceSerialNumber { get; set; }
        public string OrigDeviceModel { get; set; }
        public int OrigFRBRequestID { get; set; }

        public TadpoleEventDataEntity()
        {
            SiteContacts = new List<TadpoleContact>();
            ShippingContacts = new List<TadpoleContact>();
            Addresses = new List<TadpoleAddress>();
            Configs = new List<TadpoleConfig>();
            ActionID = 1;
        }
    }

    public class TadpoleContact
    {
        public byte RoleID { get; set; }
        public byte ContactTypeID { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string EmailAddress { get; set; }
    }

    public class TadpoleAddress
    {
        public byte AddressTypeID { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipPostalCode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
    }

    public class TadpoleConfig
    {
        public int Fedline_Event_ID { get; set; }
        public string PortName { get; set; }
        public string IPAddress { get; set; }
        public string SubnetMask { get; set; }
        public string DefaultGateway { get; set; }
        public string SPEED { get; set; }
        public string Duplex { get; set; }
        public string MACAddress { get; set; }
    }

    public class TimeSlot
    {
        public byte TimeSlotID { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }

    public class ResponseEntity
    {
        public int MessageID { get; set; }
        public int FRBRequestID { get; set; }
        public int SequenceNumber { get; set; }
        public string FRBRequestType { get; set; }
        public string ResponseType { get; set; }
        public string Status { get; set; }
        public string ResponseDescription { get; set; }
        public DateTime CreateDate { get; set; }
        public string ShipTrackingNumber { get; set; }
        public string ShipCarrier { get; set; }
        public DateTime ShippedDate { get; set; }
        public DateTime ShipDeliveryDate { get; set; }
    }

    public class ResponseContainer
    {
        public string ACKResponse { get; set; }
        //7-10-2012 Combining ACK and Confirm into one message
        //public string Confirm { get; set; }

        public ResponseContainer()
        {
        }

        public ResponseContainer(string NACK)
        {
            ACKResponse = NACK;
        }
    }

    public enum ActionType
    {
        /// <summary>
        /// New Customer Install
        /// </summary>
        NCI,

        /// <summary>
        /// New Customer Cancel
        /// </summary>
        NCC,

        /// <summary>
        /// New Customer Modify
        /// </summary>
        NCM,

        /// <summary>
        /// New Internal Modify
        /// </summary>
        NIM,

        /// <summary>
        /// New Date Modification
        /// </summary>
        NDM,

        /// <summary>
        /// New Internal Cancel
        /// </summary>
        NIC,

        /// <summary>
        /// Disconnect Customer Cancel
        /// </summary>
        DCC,

        /// <summary>
        /// Disconnect Customer Service
        /// </summary>
        DCS,
    }
}