﻿using System.Xml.Serialization;

namespace COWS.ODIEReceiverService
{
    #region "COWSODIEOrderResponse"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class COWSODIEOrderResponse
    {
        private COWSODIEOrderResponseAcknowledgement acknowledgementField;
        private COWSODIEOrderResponseNegativeAcknowledgement negativeAcknowledgementField;
        private bool negativeAcknowledgementFieldSpecified;
        private string tranID;
        private string requestXMLField;
        private string errorMessageField;
        private string dateTimeField;

        public COWSODIEOrderResponseAcknowledgement Acknowledgement
        {
            get
            {
                return this.acknowledgementField;
            }
            set
            {
                this.acknowledgementField = value;
            }
        }

        public COWSODIEOrderResponseNegativeAcknowledgement NegativeAcknowledgement
        {
            get
            {
                return this.negativeAcknowledgementField;
            }
            set
            {
                this.negativeAcknowledgementField = value;
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NegativeAcknowledgementSpecified
        {
            get
            {
                return this.negativeAcknowledgementFieldSpecified;
            }
            set
            {
                this.negativeAcknowledgementFieldSpecified = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string RequestXML
        {
            get
            {
                return this.requestXMLField;
            }
            set
            {
                this.requestXMLField = value;
            }
        }

        public string errorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public string DateTime
        {
            get
            {
                return this.dateTimeField;
            }
            set
            {
                this.dateTimeField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    public enum COWSODIEOrderResponseAcknowledgement
    {
        QQQ,
        X,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    public enum COWSODIEOrderResponseNegativeAcknowledgement
    {
        QQQ,
        X,
    }

    #endregion "COWSODIEOrderResponse"

    #region "ODIECustomerNameResponse"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ODIECustomerNameResponse
    {
        private string tranID;
        private string h1;
        private CustomerNameList[] customerName;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string H1
        {
            get { return this.h1; }
            set { this.h1 = value; }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("CustomerNameList", IsNullable = false)]
        public CustomerNameList[] CustomerName
        {
            get { return this.customerName; }
            set { this.customerName = value; }
        }
    }

    public partial class CustomerNameList
    {
        private string custID;
        private string customerName;
        private string mNSPMId;
        private string nTEAssigned;
        private string customerTeamPDL;
        private string documentationURL;
        private string sdeAssigned;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string CustID
        {
            get { return this.custID; }
            set { this.custID = value; }
        }

        public string CustomerName
        {
            get { return this.customerName; }
            set { this.customerName = value; }
        }

        public string MNSPMId
        {
            get { return this.mNSPMId; }
            set { this.mNSPMId = value; }
        }

        public string CustomerTeamPDL
        {
            get { return this.customerTeamPDL; }
            set { this.customerTeamPDL = value; }
        }

        public string DocumentationURL
        {
            get { return this.documentationURL; }
            set { this.documentationURL = value; }
        }

        public string NTEAssigned
        {
            get { return this.nTEAssigned; }
            set { this.nTEAssigned = value; }
        }

        public string SDEAssigned
        {
            get { return this.sdeAssigned; }
            set { this.sdeAssigned = value; }
        }
    }

    #endregion "ODIECustomerNameResponse"

    #region "ODIECustomerInfoResponse"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ODIECustomerInfoResponse
    {
        private string tranID;
        private string customerName;
        private CustomerNameList[] customerNameList;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string CustomerName
        {
            get { return this.customerName; }
            set { this.customerName = value; }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Info", IsNullable = false)]
        public CustomerNameList[] CustomerNameList
        {
            get { return this.customerNameList; }
            set { this.customerNameList = value; }
        }
    }

    public partial class CustomerNameList
    {
        private string modelId;
        private string deviceId;
        private string mfrID;
        private string serialNo;
        private string redesignNbr;
        private string fastTrackFlag;
        private string dispatchReadyFlag;
        private string h6;

        public string ModelId
        {
            get { return this.modelId; }
            set { this.modelId = value; }
        }

        public string DeviceId
        {
            get { return this.deviceId; }
            set { this.deviceId = value; }
        }

        public string MfrID
        {
            get { return this.mfrID; }
            set { this.mfrID = value; }
        }

        public string SerialNo
        {
            get { return this.serialNo; }
            set { this.serialNo = value; }
        }

        public string RedesignNbr
        {
            get { return this.redesignNbr; }
            set { this.redesignNbr = value; }
        }

        public string FastTrackFlag
        {
            get { return this.fastTrackFlag; }
            set { this.fastTrackFlag = value; }
        }

        public string DispatchReadyFlag
        {
            get { return this.dispatchReadyFlag; }
            set { this.dispatchReadyFlag = value; }
        }

        public string H6
        {
            get { return h6; }
            set { h6 = value; }
        }
    }

    public enum EnumBooleanYNFlag
    {
        QQQ,
        Y,
        N,
    }

    #endregion "ODIECustomerInfoResponse"

    #region "ODIEDiscoResponse"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ODIEDiscoResponse
    {
        private string tranID;
        private Devices[] deviceList;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("Devices", IsNullable = false)]
        public Devices[] DevicesList
        {
            get { return this.deviceList; }
            set { this.deviceList = value; }
        }
    }

    public partial class Devices
    {
        private string device_Name;
        private string dev_Req_ID;
        private string h5_H6;
        private string vendor;
        private string model;
        private string serial_No;
        private string mach5DeviceID;
        private string siteID;
        private string contractNumber;

        public string Device_Name
        {
            get { return this.device_Name; }
            set { this.device_Name = value; }
        }

        public string Dev_Req_ID
        {
            get { return this.dev_Req_ID; }
            set { this.dev_Req_ID = value; }
        }

        public string H5_H6
        {
            get { return this.h5_H6; }
            set { this.h5_H6 = value; }
        }

        public string Vendor
        {
            get { return this.vendor; }
            set { this.vendor = value; }
        }

        public string Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        public string Serial_No
        {
            get { return this.serial_No; }
            set { this.serial_No = value; }
        }

        public string Mach5DeviceID
        {
            get { return this.mach5DeviceID; }
            set { this.mach5DeviceID = value; }
        }

        public string SiteID
        {
            get { return this.siteID; }
            set { this.siteID = value; }
        }

        public string ContractNumber
        {
            get { return this.contractNumber; }
            set { this.contractNumber = value; }
        }
    }

    #endregion "ODIEDiscoResponse"

    #region "ODIEShortNameValidationResponse"

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "")]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ODIEShortNameValidationResponse
    {
        private string tranID;
        private string isValid;

        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TranID
        {
            get { return this.tranID; }
            set { this.tranID = value; }
        }

        public string IsValid
        {
            get { return this.isValid; }
            set { this.isValid = value; }
        }
    }

    #endregion "ODIEShortNameValidationResponse"
}