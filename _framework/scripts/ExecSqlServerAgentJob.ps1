## ExecSqlServerAgentJob.ps1 v1.0ps1
##   Paul Williams (pawill@microsoft.com) Microsoft Services Sep. 2011
##
## Simple script, based on snippets scattered across social.technet.microsoft.com
## that invokes a SQL Server Agent job.
##
## Two optional parameters can be used:
##  -HostName supplies the name of the SQL Server where the job exists.  The expected
##   formats for this are either the alias name, hostname or hostname\instance name.
##
##  -JobName supplies the name of the job to run.  Defined as optional, but actually
##  rather necessary if you want to invoke a job.  If the job contains spaces in the
##  name enclose in either single or double quotes.
##
## The script makes use of SQL Server Management Objects (SMO) thus must be run from
## a computer that has SQL SMO installed.  This script was written to be run locally on
## a SQL server to invoke agent jobs for the purpose of FIM deployments.  The author is
## not a SQL Server expert, rather an occasional user for the greater good of FIM and ILM
## projects.
##

PARAM
(
    [Parameter(Mandatory = $false)]
    [String]$ConnectString = (gc Env:\COMPUTERNAME),

    [Parameter(Mandatory = $false)]
    [String]$JobName = [String]::Empty
);

# $SB = "Data Source=PVMX0669\\MS2008_PROD,2777;Initial Catalog=msdb;Trusted_Connection=true;MultipleActiveResultSets=True;"
$SQLcon = New-object system.data.sqlclient.SqlConnection
$SQLcon.ConnectionString = $ConnectString
$SelectCMD = New-object system.data.sqlclient.SqlCommand
$SelectCMD.CommandTimeout = 30
$SelectCMD.Connection = $SQLCon
$SelectCMD.CommandText = "exec sp_start_job '$JobName'"
$da = new-object System.Data.SqlClient.SQLDataAdapter($SelectCMD)
$ds = new-object System.Data.dataset
$da.fill($ds) 

# Finally invoke the job.  Use the job history (in SSMS) to verify the success of the job.
Write-Host "Executing job (the script doesn't wait for the job to finish)...`n`n";
$job.Start();