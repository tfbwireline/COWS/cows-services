﻿using System;

namespace TadpoleHelper
{
    public class OutOfSequence : Exception
    {
        public string SequenceError { get; set; }

        public OutOfSequence()
        {
        }

        public OutOfSequence(string message)
        {
            SequenceError = message;
        }
    }

    public class IgnoreMessage : Exception
    {
    }

    public class XMLNotDatabased : Exception
    {
        public Exception exception { get; set; }

        public XMLNotDatabased(Exception ex)
        {
            exception = ex;
        }
    }
}