﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">PL# : </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/PLN_NME" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">PL SEQ : </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/PLN_SEQ_NBR" />
            </td>
          </tr>
          <tr>
              <td width="20%" style="text-align: left; font-weight: 600">Router/Port :  </td>
              <td width="80%">
                <xsl:value-of select="GomPLComp/PORT_ASMT_DES" />
              </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Toc Location :  </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/TOC_NME" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Ckt Id :  </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/FMS_CKT_ID" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Net Address : </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/NUA_ADR" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">FTN # : </td>
            <td width="80%">
              <xsl:value-of select="GomPLComp/FTN" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>