@echo off
REM *******************************************************************************
REM Copyright (c) 2011 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to deploy the application. It must be called from the 
REM application home directory, and the main deploy script must be called (not one 
REM of the copies under the staging or backup directories). The application home 
REM path must not have any spaces. 
REM 
REM Before deploying the application, a new build must be staged. The application
REM must not be running during deployment. The first operation is to move the
REM current build (if any) to the backup directory. Then the new staged build is 
REM moved to the application directory.
REM 
REM Most of the work is performed using Java scripts. This functionality is cross-
REM platform.
REM
REM *******************************************************************************


REM
REM initialize some variables and do some validation
REM
SETLOCAL
SET CMDARG=%~dp0
IF /I "%CMDARG:~-8%" EQU "scripts\" (
  IF /I "%CMDARG:~-17%" NEQ "\staging\scripts\" IF /I "%CMDARG:~-16%" NEQ "\backup\scripts\" GOTO location_ok
  ECHO ERROR: deploy.cmd must not be run under the staging or backup directory
  EXIT /B 12
)
ECHO ERROR: deploy.cmd must be run in the application codebase
EXIT /B 11
:location_ok
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
SET CFG_FILE=%APP_HOME%\Framework\etc\config.properties
SET BACKUP_CFG_FILE=%APP_HOME%\Backup\Framework\etc\config.properties
IF NOT EXIST "%CFG_FILE%" (
  REM If it doesn't exist, check in the backup location
  IF NOT EXIST "%BACKUP_CFG_FILE%" (
    ECHO ERROR: Configuration file '%CFG_FILE%' not found
    EXIT /B 13
  )
  ECHO Copying config file from backup.
  COPY "%BACKUP_CFG_FILE%" "%CFG_FILE%"
) 
IF NOT EXIST "%CFG_FILE%.local" (
  REM If it doesn't exist, check in the backup location
  IF EXIST "%BACKUP_CFG_FILE%.local" (
    ECHO Copying local config file from backup.
    COPY "%BACKUP_CFG_FILE%.local" "%CFG_FILE%.local"
  )
)


REM
REM perform the host check
REM 
CALL "%~dp0"_envsetup.cmd
CALL %APP_HOME%\Staging\Scripts\perform-host-check.cmd
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%\Staging\Scripts\_logJobMsg.cmd" deploy.cmd 14 "ERROR: host validation failed"
  EXIT /b 14
)



REM
REM check for a tactical deployment
REM
IF NOT EXIST "%APP_HOME%\Staging\tactical" GOTO :no_tactical
SET ANT_HOME=%APP_HOME%\Framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\Scripts\_admin.xml
IF NOT EXIST "%ANT_SCRIPT%" (
  ECHO ERROR: tactical deployment only valid for existing build
  EXIT /B 15
)
CALL "%ANT_PROG%" -f "%ANT_SCRIPT%" -q _tactical.deploy
IF ERRORLEVEL 1 EXIT /B 16
EXIT /B 0
:no_tactical


REM
REM set some more variables and make sure we are running in correct context
REM
SET ANT_HOME=%APP_HOME%\Staging\Framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\Staging\Scripts\_admin.xml
IF EXIST "%ANT_PROG%" (
  IF EXIST "%ANT_SCRIPT%" GOTO build_staged
)
ECHO ERROR: new build must be staged before deployment
EXIT /B 17
:build_staged
IF EXIST "%APP_HOME%\Scripts\deploy.cmd" (
  IF EXIST "%APP_HOME%\Staging\Scripts\deploy.cmd" GOTO context_ok
)
ECHO ERROR: deploy script must be run from application home directory
EXIT /B 18
:context_ok


REM
REM we are ready for stage 1, call ant to do the heavy lifting
REM
CALL "%ANT_PROG%" -f "%ANT_SCRIPT%" -q _deploy.stage1
IF ERRORLEVEL 1 EXIT /B 20


REM
REM we made it through first stage of deploy, now do second stage
REM
SET ANT_HOME=%APP_HOME%\Backup\Framework\tools\ant
SET ANT_PROG=%ANT_HOME%\bin\ant.bat
SET ANT_SCRIPT=%APP_HOME%\Backup\Scripts\_admin.xml
"%ANT_PROG%" -f "%ANT_SCRIPT%" %1 -q _deploy.stage2
IF ERRORLEVEL 1 EXIT /B 21
