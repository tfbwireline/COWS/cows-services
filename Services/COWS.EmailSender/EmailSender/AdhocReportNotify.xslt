﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:template match="/">
		<html>
			<body>
				<table width="900" border="0" align="left" style="WORD-BREAK:BREAK-WORD;table-layout: fixed;" cellpadding="1">
					<tr>
						<td>

							<span>
								COWS Dev Team,<br></br><br></br>

								Please create an Adhoc Report for ADHOC_RPT_ACTY_ID : <xsl:value-of select="/AdhocRpt/ID" /> in dbo.ADHOC_RPT_ACTY table, requested by
								<xsl:value-of select="/AdhocRpt/ReqUsr" /> on <xsl:value-of select="/AdhocRpt/CreatDt" />
							</span>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>