﻿namespace AppConstants
{
    public enum EOrderType
    {
        Install,
        Upgrade,
        Downgrade,
        Move,
        Change,
        BillingChange,
        Disconnect,
        Cancel
    }
}