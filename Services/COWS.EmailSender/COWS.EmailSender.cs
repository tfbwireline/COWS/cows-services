﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWS.EmailSender
{
    public partial class EmailSender : ServiceBase
    {
        private Thread _EmailThread = null;
        private Thread _ReworkThread = null;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private int _iSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);

        private int _bEmailThreadActive = Convert.ToInt32(ConfigurationManager.AppSettings["_EmailThreadActive"]);
        private int _bReworkThreadActive = Convert.ToInt32(ConfigurationManager.AppSettings["_ReworkThreadActive"]);

        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new EmailSender()
            };
            ServiceBase.Run(ServicesToRun);
        }

        public EmailSender()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.EmailSender service is starting.", "");

                _bStopThread = false;
                _bCanBeStoppedNow = true;
                _EmailThread = new Thread(new ThreadStart(this.SendRequests));
                _ReworkThread = new Thread(new ThreadStart(this.SendRetractEMails));
                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Send Email Requests)", string.Empty);
                _EmailThread.Start();
                _ReworkThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.EmailSender. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.EmailSender", "COWS.EmailSender Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                _bStopThread = true;
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.EmailSender", string.Empty);

                _EmailThread.Join(TimeSpan.FromSeconds(30));
                _ReworkThread.Join(TimeSpan.FromSeconds(30));

                if (!_bCanBeStoppedNow)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Send Email  Requests)", string.Empty);
                _EmailThread.Abort();
                _ReworkThread.Abort();

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.EmailSender Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.EmailSender. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void SendRequests()
        {
            EmailSenderIO _SenderIO = new EmailSenderIO();
            while (!_bStopThread)
            {
                try
                {
                    if (_bEmailThreadActive == 1)
                    {
                        _SenderIO.SendEmailRequests();
                    }
                    else
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Service Terminated from DB", "Please check DB Connection string in App Config.");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error out in SendRequests() : " + ex.Message + " ; " + ex.StackTrace, string.Empty);
                }
                Thread.Sleep(_iSleepTime);
            }
        }

        private void SendRetractEMails()
        {
            EmailSenderIO _SenderIO = new EmailSenderIO();
            while (!_bStopThread)
            {
                try
                {
                    if ((DateTime.Now.Hour == 4)
                    && (DateTime.Now.Minute == 59)
                    && (DateTime.Now.Second > 50)
                    && (_bReworkThreadActive == 1)
                     )
                        _SenderIO.SendRetractEMails();
                    //else
                    //  CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "SendRetractEMails Thread still waiting on appropriate time to process", string.Empty);
                }
                catch (Exception ex)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error out in SendRetractEMails() : " + ex.Message + " ; " + ex.StackTrace, string.Empty);
                }

                Thread.Sleep(_iSleepTime);
            }
        }
    }
}