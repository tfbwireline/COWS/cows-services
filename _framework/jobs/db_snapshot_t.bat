@echo off
REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave - initial implementation
REM
REM *** Database Script ***
REM 
REM  Job to STOP the SQL Server Snapshot of for the Readonly and backup purposes.
REM
REM
REM *******************************************************************************


REM
REM STOP the SQL Server Snapshot of for the Readonly and backup purposes
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\Scripts\stop_db_snapshot.cmd"
