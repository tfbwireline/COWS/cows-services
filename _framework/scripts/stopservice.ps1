param([string]$service)
$arrService = Get-Service -Name $service
If ($arrService.Status -eq "Running"){
Stop-Service $service
}
Start-Sleep -s 10
If ($arrService.Status -eq "Stopped"){
Write-Host "$service service successfully stopped."
}
ElseIf ($arrService.Status -ne "Running"){
$Srvc = Get-WmiObject win32_service -filter "name='$service'"
Stop-Process -Id $Srvc.processid -Force -PassThru -ErrorAction Stop
Write-Host "$service service has successfully been forced stopped."
}