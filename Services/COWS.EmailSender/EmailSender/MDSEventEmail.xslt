<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:key name="tabids" match="EventEmail/Tab[@TABID]" use="@TABID" />
	<xsl:template match="/">
		<html>
			<body>
				<table width="1000" border="0" align="left" style="word-wrap: break-word; font-size=12px" cellpadding="1">
					<xsl:if test="/EventEmail/MDSEvent/ESCL_BUS_JUSTN_TXT[.!='']">
					  <tr>
						  <td width="40%" style="text-align: left; font-weight: 600">Escalation Business Justification : </td>
						  <td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/ESCL_BUS_JUSTN_TXT" />
						  </td>
					  </tr>
					</xsl:if>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Scheduled Date &amp; Time : </td>
						<td width="60%">
							<xsl:value-of select="concat(/EventEmail/MDSEvent/STRT_TMST, ' CT')" />
						</td>
					</tr>
					<tr />
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Conference Bridge Number and Pin : </td>
						<td width="60%">
							<xsl:if test="/EventEmail/MDSEvent/CNFRC_BRDG_NBR[.!='']">
								<xsl:value-of select="concat(/EventEmail/MDSEvent/CNFRC_BRDG_NBR, ' / ', /EventEmail/MDSEvent/CNFRC_PIN_NBR)" />
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Online Meeting URL : </td>
						<td width="60%">
							<xsl:if test="/EventEmail/MDSEvent/OnlineMeetingURL[.!='']">
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="/EventEmail/MDSEvent/OnlineMeetingURL" />
									</xsl:attribute>
									<xsl:value-of select="/EventEmail/MDSEvent/OnlineMeetingURL" />
								</a>
							</xsl:if>
						</td>
					</tr>
					<tr />
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Reviewer : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/RevUserName" />
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Phone Number : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/REV_PHN_NBR" />
						</td>
					</tr>
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Assigned MNS Resource : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/AssignUsers" />
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Phone Number : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/AssignPhoneNumbers" />
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Soft Assigned : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/SoftAssignCd" />
						</td>
					</tr>
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Type : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/NTWK_ACTY_TYPE" />
						</td>
					</tr>
					<xsl:if test="contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'Network') or contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'VAS')">
						<xsl:if test="/EventEmail/MDSEvent/CEChngFlg[.!='True']">
							<tr>
								<td width="40%" style="text-align: left; font-weight: 600">Network H6 : </td>
								<td width="60%">
									<xsl:value-of select="/EventEmail/MDSEvent/NTWK_H6" />
								</td>
							</tr>
						</xsl:if>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Network H1 : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/NTWK_H1" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Network Customer Name : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/NTWK_CUST_NME" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Network Activity Type : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/MDS_NTWK_ACTY_TYPE" />
							</td>
						</tr>
						<xsl:if test="/EventEmail/MDSEvent/RltdCmpNCRCd[.='1']">
							<tr>
								<td width="40%" style="text-align: left; font-weight: 600">Related Compass NCR : </td>
								<td width="60%">
									<xsl:value-of select="/EventEmail/MDSEvent/RltdCmpNCR" />
								</td>
							</tr>
						</xsl:if>
						<tr>
							<td colspan="2" width="100%" style="font-style:italic">
								<span>---------- Network Customer Contacts -----------</span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
									<tr bgcolor="#D8D8D8">
										<td style="text-align: left; font-weight: 600">Contact Name</td>
										<td style="text-align: left; font-weight: 600">Contact Role</td>
										<td style="text-align: left; font-weight: 600">Contact Email</td>
										<td style="text-align: left; font-weight: 600">Contact Phone</td>
									</tr>
									<xsl:for-each select="/EventEmail/MDSNtwkCust">
										<tr>
											<td width="25%">
												<xsl:value-of select="CustCntct" />
											</td>
											<td width="25%">
												<xsl:value-of select="CntctRole" />
											</td>
											<td width="30%">
												<xsl:value-of select="CntctEmail" />
											</td>
											<td width="20%">
												<xsl:value-of select="CntctPhn" />
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="100%" style="font-style:italic">
								<span>---------- Network Transport/VAS Table -----------</span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
									<tr bgcolor="#D8D8D8">
										<td style="text-align: left; font-weight: 600">Associated H6</td>
										<td style="text-align: left; font-weight: 600">Design Doc #</td>
										<td style="text-align: left; font-weight: 600">Transport Type</td>
										<td style="text-align: left; font-weight: 600">LEC Provider Name</td>
										<td style="text-align: left; font-weight: 600">M5 Order #</td>
										<td style="text-align: left; font-weight: 600">Network NUA</td>
										<td style="text-align: left; font-weight: 600">Port Bandwidth</td>
										<xsl:if test="/EventEmail/MDSEvent/CEFlg[.='True'] or /EventEmail/MDSEvent/CEChngFlg[.='True']">
                                          <td style="text-align: left; font-weight: 600">CE Service ID</td>
											<td style="text-align: left; font-weight: 600">NID Serial #</td>
											<td style="text-align: left; font-weight: 600">NID IP</td>
											<td style="text-align: left; font-weight: 600">NID Hostname</td>
										</xsl:if>
										<!--<td style="text-align: left; font-weight: 600">Location City</td>
										<td style="text-align: left; font-weight: 600">Location State</td>-->
										<td style="text-align: left; font-weight: 600">Location Country</td>
										<xsl:if test="contains(/EventEmail/MDSEvent/VASCECd,'1') or contains(/EventEmail/MDSEvent/VASCECd,'2')
						or contains(/EventEmail/MDSEvent/VASCECd,'4') or contains(/EventEmail/MDSEvent/VASCECd,'5')">
											<td style="text-align: left; font-weight: 600">VAS Type</td>
										</xsl:if>
										<td style="text-align: left; font-weight: 600">SCA #</td>
										<!--<td style="text-align: left; font-weight: 600">IP Version</td>-->
									</tr>
									<xsl:for-each select="/EventEmail/MDSNtwkTrpt">
										<tr>
											<td width="6%">
												<xsl:value-of select="AssocH6" />
											</td>
											<td width="10%">
												<xsl:value-of select="DD_APRVL_NBR" />
											</td>
											<td width="8%">
												<xsl:value-of select="TrptType" />
											</td>
											<td width="5%">
												<xsl:value-of select="LECPrvdrNme" />
											</td>
											<td width="5%">
												<xsl:value-of select="M5OrdrNbr" />
											</td>
											<td width="4%">
												<xsl:value-of select="NtwkNUA" />
											</td>
											<td width="4%">
												<xsl:value-of select="BDWD_NME" />
											</td>
											<xsl:if test="/EventEmail/MDSEvent/CEFlg[.='True'] or /EventEmail/MDSEvent/CEChngFlg[.='True']">
                                              <td width="4%">
                                                <xsl:value-of select="CE_SRVC_ID" />
                                              </td>
											  <td width="4%">
												  <xsl:value-of select="NID_SERIAL_NBR" />
											  </td>
											  <td width="5%">
												  <xsl:value-of select="NID_IP" />
											  </td>
											  <td width="10%">
												  <xsl:value-of select="NID_HOSTNAME" />
											  </td>
											</xsl:if>
											<!--<td width="5%">
												<xsl:value-of select="LocCity" />
											</td>
											<td width="4%">
												<xsl:value-of select="LocState" />
											</td>-->
											<td width="3%">
												<xsl:value-of select="LocCtry" />
											</td>
											<xsl:if test="contains(/EventEmail/MDSEvent/VASCECd,'1') or contains(/EventEmail/MDSEvent/VASCECd,'2')
						or contains(/EventEmail/MDSEvent/VASCECd,'4') or contains(/EventEmail/MDSEvent/VASCECd,'5')">
												<td width="5%">
													<xsl:value-of select="VasType" />
												</td>
											</xsl:if>
											<td width="4%">
												<xsl:value-of select="SCANbr" />
											</td>
											<!--<td width="4%">
												<xsl:value-of select="IPVer" />
											</td>-->
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</xsl:if>
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Author : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER" />
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Author Contact # : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_PHN" />
						</td>
					</tr>
					<tr />
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Last Note : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/DES_CMNT_TXT" />
						</td>
					</tr>
					<tr />
					<tr />
					<tr />
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Link to the Event : </td>
						<td width="60%">
							<a>
								<xsl:attribute name="href">
									<xsl:choose>
										<xsl:when test="contains(/EventEmail/MDSEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/MDSEvent/ViewEventURL,'cows.') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsdev2')">
											<xsl:value-of select="concat(/EventEmail/MDSEvent/ViewEventURL, 'mds/', /EventEmail/MDSEvent/EVENT_ID)" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="concat(/EventEmail/MDSEvent/ViewEventURL, '?eid=', /EventEmail/MDSEvent/EVENT_ID, '&amp;email=1')" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								Click Here
							</a>
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Add to Calendar : </td>
						<td width="60%">
							<a>
								<xsl:attribute name="href">
                                  <xsl:value-of select="concat(/EventEmail/MDSEvent/CalenderEntryURL, '?eid=', /EventEmail/MDSEvent/EVENT_ID, '&amp;mode=2')" />
								</xsl:attribute>
								Click Here
							</a>
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event ID : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/EVENT_ID" />
						</td>
					</tr>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Event Title : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/EVENT_TITLE_TXT" />
						</td>
					</tr>
					<xsl:if test="/EventEmail/MDSEvent/WRKFLW_STUS_DES[.='Retract']">
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">IPM Description : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/IPMDesc" />
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td width="40%" style="text-align: left; font-weight: 600">Short Description : </td>
						<td width="60%">
							<xsl:value-of select="/EventEmail/MDSEvent/SHRT_DES" />
						</td>
					</tr>
					<xsl:if test="contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'MDS')">
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">H1 : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/H1" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Customer Name : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/CUST_NME" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Account Team PDL : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/CUST_EMAIL_ADR" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">Redesign Number : </td>
							<td width="60%">
								<xsl:value-of select="/EventEmail/MDSEvent/RDSGN_NBR" />
							</td>
						</tr>
						<tr>
							<td width="40%" style="text-align: left; font-weight: 600">
                Here is a link to the Customer's SOWS folder in Docshare. Click here to access the Scope of Work for this activity Reference the Redesign Number Listed above. :
							</td>
							<td width="60%">
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="/EventEmail/MDSEvent/DSGN_DOC_LOC_TXT" />
									</xsl:attribute>
                  Click Here
								</a>
							</td>
						</tr>
						<tr />
					</xsl:if>
					<tr />
					<tr />
					<xsl:if test="/EventEmail/Tab">
						<xsl:for-each select="EventEmail/Tab">
							<xsl:for-each select="MDSAddr">
								<tr />
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Address : </td>
									<td width="60%">
										<xsl:value-of select="STREET_ADR" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
									<td width="60%">
										<xsl:value-of select="FLR_BLDG_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">City : </td>
									<td width="60%">
										<xsl:value-of select="CTY_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">State/Providence : </td>
									<td width="60%">
										<xsl:value-of select="STT_PRVN_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Country/Region : </td>
									<td width="60%">
										<xsl:value-of select="CTRY_RGN_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Zip : </td>
									<td width="60%">
										<xsl:value-of select="ZIP_CD" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">US/International : </td>
									<td width="60%">
										<xsl:value-of select="US_INTL_ID" />
									</td>
								</tr>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:if>
					<tr />
					<xsl:if test="/EventEmail/Tab">
						<xsl:for-each select="EventEmail/Tab">
							<tr />
							<tr />
							<tr />
							<xsl:for-each select="MDSMISC">
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Tab Sequence : </td>
									<td width="60%">
										<xsl:value-of select="concat(TAB_SEQ_NBR,' : ',TAB_NME)" />
									</td>
								</tr>
							</xsl:for-each>
							<tr />
							<tr />
							<xsl:for-each select="MDSOE">
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">MNS FTN : </td>
									<td width="60%">
										<xsl:value-of select="MNS_OE_FTN" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Description : </td>
									<td width="60%">
										<xsl:value-of select="MDS_SRVC_TIER_DES" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Enrollment Description : </td>
									<td width="60%">
										<xsl:value-of select="MDS_ENTLMNT_DES" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
							</xsl:for-each>

							<xsl:for-each select="MDSMagDevices">
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">FTN (CPE) : </td>
									<td width="60%">
										<xsl:value-of select="FTN_CPE" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Managed Device : </td>
									<td width="60%">
										<xsl:value-of select="DEV_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Vendor : </td>
									<td width="60%">
										<xsl:value-of select="MANF_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Model : </td>
									<td width="60%">
										<xsl:value-of select="DEV_MODEL_NME" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
							</xsl:for-each>

							<xsl:for-each select="MDSMISC">
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Transport FTN : </td>
									<td width="60%">
										<xsl:value-of select="TRPT_FTN" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Is Wired Transport Required? : </td>
									<td width="60%">
										<xsl:value-of select="WIRED_DEV_TRNSPRT_REQR_CD" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Is Wireless Transport Required? : </td>
									<td width="60%">
										<xsl:value-of select="WRLS_TRNSPRT_REQR_CD" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Any Virtual Connections : </td>
									<td width="60%">
										<xsl:value-of select="VRTL_CNCTN_CD" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Install Site POC : </td>
									<td width="60%">
										<xsl:value-of select="INSTL_SITE_POC_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
									<td width="60%">
										<xsl:value-of select="INSTL_SITE_POC_PHN_NBR" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Cell : </td>
									<td width="60%">
										<xsl:value-of select="INSTL_SITE_POC_CELL_PHN_NBR" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC : </td>
									<td width="60%">
										<xsl:value-of select="SRVC_ASSRN_POC_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Phone : </td>
									<td width="60%">
										<xsl:value-of select="SRVC_ASSRN_POC_PHN_NBR" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Cell : </td>
									<td width="60%">
										<xsl:value-of select="SRVC_ASSRN_POC_CELL_NBR" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Contact Hours : </td>
									<td width="60%">
										<xsl:value-of select="SA_POC_HR_NME" />
									</td>
								</tr>
								<tr>
									<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC TimeZone : </td>
									<td width="60%">
										<xsl:value-of select="TME_ZONE_ID" />
									</td>
								</tr>
								<tr />
								<tr />
								<tr />
							</xsl:for-each>
						</xsl:for-each>
					</xsl:if>
					<xsl:if test="not(/EventEmail/Tab)">
						<xsl:choose>
							<xsl:when test="contains(/EventEmail/MDSEvent/MDS_ACTY_TYPE_DES, 'Disconnect')">
								<xsl:if test="/EventEmail/MDSDiscODIEDev">
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Disconnect of Management Only ? : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/DISC_MGMT_CD" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Total Customer Disconnect ? : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/FULL_CUST_DISC_CD" />
										</td>
									</tr>
									<xsl:if test="/EventEmail/MDSEvent/FULL_CUST_DISC_CD[.='Full Disconnect']">
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Reason for Total Customer Disconnect : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/FULL_CUST_DISC_REAS_TXT" />
											</td>
										</tr>
									</xsl:if>
									<tr>
										<td colspan="2" width="100%" style="font-style:italic">
											<span>---------- Disconnect ODIE Device/Redesign Table -----------</span>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
												<tr bgcolor="#D8D8D8">
													<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													<td style="text-align: left; font-weight: 600">H5/H6</td>
													<td style="text-align: left; font-weight: 600">Site ID</td>
													<td style="text-align: left; font-weight: 600">Device ID</td>
													<td style="text-align: left; font-weight: 600">Vendor</td>
													<td style="text-align: left; font-weight: 600">Model</td>
													<td style="text-align: left; font-weight: 600">Serial Number</td>
													<td style="text-align: left; font-weight: 600">Third Party Contract #</td>
												</tr>
												<xsl:for-each select="/EventEmail/MDSDiscODIEDev">
													<tr>
														<td width="20%">
															<xsl:value-of select="ODIEDevNme" />
														</td>
														<td width="10%">
															<xsl:value-of select="H5H6" />
														</td>
														<td width="15%">
															<xsl:value-of select="SiteID" />
														</td>
														<td width="15%">
															<xsl:value-of select="DeviceID" />
														</td>
														<td width="10%">
															<xsl:value-of select="DevManfName" />
														</td>
														<td width="10%">
															<xsl:value-of select="DevModelName" />
														</td>
														<td width="10%">
															<xsl:value-of select="SerialNbr" />
														</td>
														<td width="10%">
															<xsl:value-of select="ThirdPartyCtrct" />
														</td>
													</tr>
												</xsl:for-each>
											</table>
										</td>
									</tr>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<tr />
								<xsl:if test="contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'MDS') or contains(/EventEmail/MDSEvent/MDS_NTWK_ACTY_TYPE,'Carrier Ethernet')">
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Address : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/STREET_ADR" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/FLR_BLDG_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">City : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/CTY_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">State/Providence : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/STT_PRVN_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Country/Region : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/CTRY_RGN_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Zip : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/ZIP_CD" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">US/International : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/US_INTL_ID" />
										</td>
									</tr>
									<xsl:if test="/EventEmail/MDSEvent/ALT_SHIP_STREET_ADR[.!='']">
										<tr />
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To Address : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_STREET_ADR" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To Floor/Building Name : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To City : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_CTY_NME" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To State/Providence : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_STT_PRVN_NME" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To Country/Region : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_CTRY_RGN_NME" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To Zip : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_ZIP_CD" />
											</td>
										</tr>
									</xsl:if>
								</xsl:if>
								<tr />
								<xsl:if test="/EventEmail/MDSODIEDev">
									<tr>
										<td colspan="2" width="100%" style="font-style:italic">
											<span>---------- ODIE Device/Redesign Table -----------</span>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
												<tr bgcolor="#D8D8D8">
													<td style="text-align: left; font-weight: 600">Redesign #</td>
													<td style="text-align: left; font-weight: 600">Redesign Exp. Date</td>
													<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													<td style="text-align: left; font-weight: 600">Vendor</td>
													<td style="text-align: left; font-weight: 600">Model</td>
												</tr>
												<xsl:for-each select="/EventEmail/MDSODIEDev">
													<tr>
														<td width="15%">
															<xsl:value-of select="RdsnNbr" />
														</td>
														<td width="15%">
															<xsl:value-of select="RdsnExpDt" />
														</td>
														<td width="40%">
															<xsl:value-of select="ODIEDevNme" />
														</td>
														<td width="15%">
															<xsl:value-of select="DevManfName" />
														</td>
														<td width="15%">
															<xsl:value-of select="DevModelName" />
														</td>
													</tr>
												</xsl:for-each>
											</table>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="/EventEmail/MDSDevCmpltTbl">
									<tr>
										<td colspan="2" width="100%" style="font-style:italic">
											<span>---------- Device Completion Table -----------</span>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
												<tr bgcolor="#D8D8D8">
													<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													<td style="text-align: left; font-weight: 600">ODIE H6</td>
													<td style="text-align: left; font-weight: 600">Activator Completion</td>
												</tr>
												<xsl:for-each select="/EventEmail/MDSDevCmpltTbl">
													<tr>
														<td width="60%">
															<xsl:value-of select="ODIEDevNme" />
														</td>
														<td width="20%">
															<xsl:value-of select="ODIEH6" />
														</td>
														<td width="20%">
															<xsl:value-of select="ActvCmpltCd" />
														</td>
													</tr>
												</xsl:for-each>
											</table>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="/EventEmail/MDSCPEDev">
									<tr>
										<td colspan="2" width="100%" style="font-style:italic">
											<span>---------- CPE Table -----------</span>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
												<tr bgcolor="#D8D8D8">
													<td style="text-align: left; font-weight: 600">CPE Order #</td>
													<td style="text-align: left; font-weight: 600">Device ID</td>
													<td style="text-align: left; font-weight: 600">Assoc. H6</td>
													<td style="text-align: left; font-weight: 600">CCD</td>
													<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													<td style="text-align: left; font-weight: 600">Component Status</td>
													<td style="text-align: left; font-weight: 600">Receipt Status</td>
													<td style="text-align: left; font-weight: 600">Contract Type</td>
												</tr>
												<xsl:for-each select="/EventEmail/MDSCPEDev">
													<tr>
														<td width="10%">
															<xsl:value-of select="CPEOrdrNbr" />
														</td>
														<td width="12%">
															<xsl:value-of select="DeviceID" />
														</td>
														<td width="8%">
															<xsl:value-of select="AssocH6" />
														</td>
														<td width="5%">
															<xsl:value-of select="CCD" />
														</td>
														<td width="30%">
															<xsl:value-of select="OdieDevNme" />
														</td>
														<td width="10%">
															<xsl:value-of select="CmpntStus" />
														</td>
														<td width="15%">
															<xsl:value-of select="RcptStus" />
														</td>
														<td width="10%">
															<xsl:value-of select="CtrctType" />
														</td>
													</tr>
												</xsl:for-each>
											</table>
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'MDS')">
									<xsl:if test="/EventEmail/MDSCustTrptInfo">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- DSL/SBIC/CUST Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left;  font-weight: 600">Transport Type</td>
														<td style="text-align: left; font-weight: 600">Primary/Backup</td>
														<td style="text-align: left; font-weight: 600">
                              Vendor/Provider<br />Transport Circuit ID
														</td>
														<td style="text-align: left; font-weight: 600">
                              Vendor/Provider<br />Name
														</td>
														<td style="text-align: left; font-weight: 600">
                              Vendor/Provider<br />Callout
														</td>
														<td style="text-align: left; font-weight: 600">Wireless</td>
														<td style="text-align: left; font-weight: 600">
                              Speed/BW ESN/3G<br />MEID ID 4G
														</td>
														<td style="text-align: left; font-weight: 600">SCA</td>
														<td style="text-align: left; font-weight: 600">IP Address</td>
														<td style="text-align: left; font-weight: 600">Subnet Mask</td>
														<td style="text-align: left; font-weight: 600">
                              Next-Hop Gateway<br />Address
														</td>
														<td style="text-align: left; font-weight: 600">T-Mobile Managed</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSCustTrptInfo">
														<tr>
															<td width="6%">
																<xsl:value-of select="MDSTrptType" />
															</td>
															<td width="6%">
																<xsl:value-of select="PrimBkpCD" />
															</td>
															<td width="6%">
																<xsl:value-of select="VndrPrvdrTrptCktID" />
															</td>
															<td width="6%">
																<xsl:value-of select="VndrPrvdrNme" />
															</td>
															<td width="6%">
																<xsl:value-of select="Telco" />
															</td>
															<td width="6%">
																<xsl:value-of select="WrlsCD" />
															</td>
															<td width="6%">
																<xsl:value-of select="BwdEsn" />
															</td>
															<td width="6%">
																<xsl:value-of select="SCANbr" />
															</td>
															<td width="6%">
																<xsl:value-of select="IPAdr" />
															</td>
															<td width="6%">
																<xsl:value-of select="SubnetMask" />
															</td>
															<td width="6%">
																<xsl:value-of select="NhopGwy" />
															</td>
															<td width="6%">
																<xsl:value-of select="MngdCD" />
															</td>
															<td width="28%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="/EventEmail/MDSSrvcTbl">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- Service Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left; font-weight: 600">M5 Service Order ID</td>
														<td style="text-align: left; font-weight: 600">Service Type</td>
														<td style="text-align: left; font-weight: 600">Third Party Vendor</td>
														<td style="text-align: left; font-weight: 600">Third Party</td>
														<td style="text-align: left; font-weight: 600">Third Party Service Level</td>
														<td style="text-align: left; font-weight: 600">Activation Date</td>
														<td style="text-align: left; font-weight: 600">Comments</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSSrvcTbl">
														<tr>
															<td width="10%">
																<xsl:value-of select="Mach5H6SrvcID" />
															</td>
															<td width="10%">
																<xsl:value-of select="SrvcType" />
															</td>
															<td width="10%">
																<xsl:value-of select="ThirdPartyVndr" />
															</td>
															<td width="10%">
																<xsl:value-of select="ThirdParty" />
															</td>
															<td width="10%">
																<xsl:value-of select="ThirdPartySrvcLvl" />
															</td>
															<td width="10%">
																<xsl:value-of select="substring(ActvnDt, 1, 10)" />
															</td>
															<td width="25%">
																<xsl:value-of select="Comments" />
															</td>
															<td width="15%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="/EventEmail/MDSSlnkWrdTrptInfo">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- Sprintlink FR/Wired Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1"  width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left; font-weight: 600">Transport Type</td>
														<td style="text-align: left; font-weight: 600">Primary/Backup</td>
														<td style="text-align: left; font-weight: 600">
                              Vendor/Provider<br />Transport Circuit ID
														</td>
														<td style="text-align: left; font-weight: 600">Vendor/Provider Name</td>
														<td style="text-align: left; font-weight: 600">Bandwidth</td>
														<td style="text-align: left; font-weight: 600">NUA</td>
														<td style="text-align: left; font-weight: 600">Old Circuit</td>
														<td style="text-align: left; font-weight: 600">VLAN</td>
														<td style="text-align: left; font-weight: 600">T-Mobile Managed</td>
														<td style="text-align: left; font-weight: 600">Ready Monitor</td>
														<td style="text-align: left; font-weight: 600">Opt-In-H1</td>
														<td style="text-align: left; font-weight: 600">Proactive Monitoring(Opt-In Ckt)</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSSlnkWrdTrptInfo">
														<tr>
															<td width="10%">
																<xsl:value-of select="MDSTrptType" />
															</td>
															<td width="5%">
																<xsl:value-of select="PrimBkpCD" />
															</td>
															<td width="10%">
																<xsl:value-of select="VndrPrvdrTrptCktID" />
															</td>
															<td width="10%">
																<xsl:value-of select="VndrPrvdrNme" />
															</td>
															<td width="5%">
																<xsl:value-of select="BdwChnlNme" />
															</td>
															<td width="5%">
																<xsl:value-of select="IPNUA" />
															</td>
															<td width="5%">
																<xsl:value-of select="OldCktID" />
															</td>
															<td width="5%">
																<xsl:value-of select="VLANNbr" />
															</td>
															<td width="5%">
																<xsl:value-of select="MngdCD" />
															</td>
															<td width="5%">
																<xsl:value-of select="ReadyBeginFlag" />
															</td>
															<td width="5%">
																<xsl:value-of select="OptInHCd" />
															</td>
															<td width="5%">
																<xsl:value-of select="OptInCktCd" />
															</td>
															<td width="25%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="/EventEmail/MDSDevMgmtTbl">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- Device Based MNS Management Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left; font-weight: 600">MNS Order #</td>
														<td style="text-align: left; font-weight: 600">Device ID</td>
														<td style="text-align: left; font-weight: 600">Component ID</td>
														<td style="text-align: left; font-weight: 600">Component Status</td>
														<td style="text-align: left; font-weight: 600">Service Tier</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSDevMgmtTbl">
														<tr>
															<td width="5%">
																<xsl:value-of select="MNSOrdrNbr" />
															</td>
															<td width="15%">
																<xsl:value-of select="DeviceID" />
															</td>
															<td width="15%">
																<xsl:value-of select="CmpntID" />
															</td>
															<td width="15%">
																<xsl:value-of select="CmpntStus" />
															</td>
															<td width="20%">
																<xsl:value-of select="ServiceTier" />
															</td>
															<td width="30%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
										<xsl:if test="/EventEmail/MDSRelDevMgmtTbl">
											<tr>
												<td colspan="2" width="100%" style="font-style:italic">
													<span>---------- Related Components -----------</span>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
														<tr bgcolor="#D8D8D8">
															<td style="text-align: left; font-weight: 600">MNS Order #</td>
															<td style="text-align: left; font-weight: 600">Device ID</td>
															<td style="text-align: left; font-weight: 600">Component ID</td>
															<td style="text-align: left; font-weight: 600">Component Status</td>
															<td style="text-align: left; font-weight: 600">Component Name</td>
															<td style="text-align: left; font-weight: 600">Related Component ID</td>
															<td style="text-align: left; font-weight: 600">Service Tier</td>
															<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
														</tr>
														<xsl:for-each select="/EventEmail/MDSRelDevMgmtTbl">
															<tr>
																<td width="5%">
																	<xsl:value-of select="MNSOrdrNbr" />
																</td>
																<td width="10%">
																	<xsl:value-of select="DeviceID" />
																</td>
																<td width="10%">
																	<xsl:value-of select="CmpntID" />
																</td>
																<td width="10%">
																	<xsl:value-of select="CmpntStus" />
																</td>
																<td width="15%">
																	<xsl:value-of select="CmpntNme" />
																</td>
																<td width="10%">
																	<xsl:value-of select="RltdCmpntID" />
																</td>
																<td width="20%">
																	<xsl:value-of select="ServiceTier" />
																</td>
																<td width="20%">
																	<xsl:value-of select="ODIEDevNme" />
																</td>
															</tr>
														</xsl:for-each>
													</table>
												</td>
											</tr>
										</xsl:if>
									</xsl:if>
									<xsl:if test="/EventEmail/MDSWrlsTrptTbl">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- Wireless Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left; font-weight: 600">3G/4G</td>
														<td style="text-align: left; font-weight: 600">Primary/Backup</td>
														<td style="text-align: left; font-weight: 600">ESN/3G MEID/4G</td>
														<td style="text-align: left; font-weight: 600">Billing Account Name</td>
														<td style="text-align: left; font-weight: 600">Billing Account #</td>
														<td style="text-align: left; font-weight: 600">PIN</td>
														<td style="text-align: left; font-weight: 600">Sales Code</td>
														<td style="text-align: left; font-weight: 600">SOC</td>
														<td style="text-align: left; font-weight: 600">Static IP</td>
														<td style="text-align: left; font-weight: 600">IMEI</td>
														<td style="text-align: left; font-weight: 600">UICC ID#</td>
														<td style="text-align: left; font-weight: 600">Price Plan</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSWrlsTrptTbl">
														<tr>
															<td width="6%">
																<xsl:value-of select="WrlsTypeCD" />
															</td>
															<td width="6%">
																<xsl:value-of select="PrimBkpCD" />
															</td>
															<td width="6%">
																<xsl:value-of select="ESN" />
															</td>
															<td width="6%">
																<xsl:value-of select="BillAcctNme" />
															</td>
															<td width="6%">
																<xsl:value-of select="BillAcctNbr" />
															</td>
															<td width="6%">
																<xsl:value-of select="AcctPin" />
															</td>
															<td width="6%">
																<xsl:value-of select="SalsCD" />
															</td>
															<td width="6%">
																<xsl:value-of select="SOC" />
															</td>
															<td width="6%">
																<xsl:value-of select="StaticIPAdr" />
															</td>
															<td width="6%">
																<xsl:value-of select="IMEI" />
															</td>
															<td width="6%">
																<xsl:value-of select="UICCIDNbr" />
															</td>
															<td width="6%">
																<xsl:value-of select="PricePln" />
															</td>
															<td width="24%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="/EventEmail/MDSPortBndwd">
										<tr>
											<td colspan="2" width="100%" style="font-style:italic">
												<span>---------- Port Bandwidth Table -----------</span>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<table border="1" width="98%" style="word-wrap: break-word; font-size=12px">
													<tr bgcolor="#D8D8D8">
														<td style="text-align: left; font-weight: 600">Mach5 Order #</td>
														<td style="text-align: left; font-weight: 600">Mach5 Order Component ID</td>
														<td style="text-align: left; font-weight: 600">NUA</td>
														<td style="text-align: left; font-weight: 600">Port Bandwidth</td>
														<td style="text-align: left; font-weight: 600">ODIE Device Name</td>
													</tr>
													<xsl:for-each select="/EventEmail/MDSPortBndwd">
														<tr>
															<td width="15%">
																<xsl:value-of select="M5OrdrNbr" />
															</td>
															<td width="15%">
																<xsl:value-of select="M5CmpntID" />
															</td>
															<td width="15%">
																<xsl:value-of select="NUA" />
															</td>
															<td width="15%">
																<xsl:value-of select="PortBndwd" />
															</td>
															<td width="40%">
																<xsl:value-of select="ODIEDevNme" />
															</td>
														</tr>
													</xsl:for-each>
												</table>
											</td>
										</tr>
									</xsl:if>
								</xsl:if>
								<xsl:if test="contains(/EventEmail/MDSEvent/NTWK_ACTY_TYPE,'MDS') or contains(/EventEmail/MDSEvent/MDS_NTWK_ACTY_TYPE,'Carrier Ethernet')">
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Install Site POC : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/INSTL_SITE_POC_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/INSTL_SITE_POC_PHN_NBR" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Install Site POC Cell : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/INSTL_SITE_POC_CELL_PHN_NBR" />
										</td>
									</tr>
									<tr />
									<tr />
									<tr />
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/SRVC_ASSRN_POC_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Phone : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/SRVC_ASSRN_POC_PHN_NBR" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Cell : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/SRVC_ASSRN_POC_CELL_NBR" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC Contact Hours : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/SA_POC_HR_NME" />
										</td>
									</tr>
									<tr>
										<td width="40%" style="text-align: left; font-weight: 600">Service Assurance POC TimeZone : </td>
										<td width="60%">
											<xsl:value-of select="/EventEmail/MDSEvent/TME_ZONE_ID" />
										</td>
									</tr>
									<xsl:if test="/EventEmail/MDSEvent/ALT_SHIP_POC_NME[.!='']">
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To POC : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_POC_NME" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To POC Phone : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_POC_PHN_NBR" />
											</td>
										</tr>
										<tr>
											<td width="40%" style="text-align: left; font-weight: 600">Alternate Ship To POC Email : </td>
											<td width="60%">
												<xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_POC_EMAIL" />
											</td>
										</tr>
									</xsl:if>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
