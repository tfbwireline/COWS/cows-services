﻿using System.ComponentModel;

namespace AppConstants
{
    public enum ESearchField
    {
        [Description("Event ID")]
        EventID,

        [Description("M5 #")]
        M5,

        [Description("AD Type")]
        ADType,

        [Description("Customer Name")]
        CustomerName,

        [Description("Assigned Activator")]
        AssignedActivator,

        [Description("SOWS EventID")]
        SOWSID,

        [Description("Requestor Name")]
        RequestorName,

        [Description("Event Type")]
        EventType,

        [Description("FMS Circuit")]
        FMSCkt,

        [Description("FRB ID")]
        FRBID,
    }
}