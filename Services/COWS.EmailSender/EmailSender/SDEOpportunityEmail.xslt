﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:include href="style.xslt" />
	<xsl:template match="/">
		<html>
			<body style="font-family: Verdana; font-size: 10pt; margin: 5; padding: 5;">
				<!-- Email Message -->
				<p>
					MDS Design Opportunity Submitted for <b><xsl:value-of select="/SDEEmail/SDEInfo/CompanyName" /></b>
					<br></br><br></br>
					A new IP Security Services Opportunity has been submitted.<br></br>
					For your reference below is a copy of the information submitted.
				</p>

				<p>
					<xsl:value-of select="/SDEEmail/SDEInfo/AssignedText" />
				</p>

				<p>
					Please click
					<a>
						<xsl:attribute name="href">
							<xsl:value-of select="concat(/SDEEmail/SDEInfo/SDEURL, '?id=', /SDEEmail/SDEInfo/OpportunityID)" />
						</xsl:attribute>
						here</a>
					to open the Managed Security Engagement Request Form.
				</p>

				<table border="0" align="left" style="font-family: Verdana; font-size: 10pt; WORD-BREAK: break-word;" cellpadding="5">
					<!-- SDE Info -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>NEW SALES OPPORTUNITY - <xsl:value-of select="/SDEEmail/SDEInfo/OpportunityID" />
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>ACCOUNT TEAM
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Requestor:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/RequestorName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Email:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/RequestorEmail" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Info:</td>
						<td width="80%">
							Phone: <xsl:value-of select="/SDEEmail/SDEInfo/RequestorPhone" />
							Location: <xsl:value-of select="/SDEEmail/SDEInfo/RequestorLocation" />
						</td>
					</tr>

					<!-- Customer Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>CUSTOMER INFORMATION
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Name:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/CompanyName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Address:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/Address" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Region:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/Region" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">State:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/State" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">City:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/City" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Contact:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/ContactName" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Email:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/ContactEmail" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Phone:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/ContactPhone" />
						</td>
					</tr>

					<!-- Sale Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>SALE INFORMATION
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Type of Sale?</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/SaleType" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Is this an RFP?</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/IsRFP" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Is this an existing T-Mobile Wireline customer?</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/IsWireline" />
						</td>
					</tr>
					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Is this an existing T-Mobile Managed Services customer?</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/IsManagedService" />
						</td>
					</tr>

					<!-- Product Information -->
					<tr>
						<td colspan="2">
							<div style="text-align: left; font-weight: bold;">
								<br></br>PRODUCT INFORMATION
							</div>
							<div>
								======================================================
							</div>
						</td>
					</tr>

					<xsl:for-each select="SDEEmail">
						<tr>
							<td colspan="2">
								<table border="1">
									<tr bgcolor="#D8D8D8">
										<th>Product Type</th>
										<th>QTY</th>
									</tr>
									<xsl:for-each select="SDEProductInfo">
										<tr>
											<td><xsl:value-of select="ProductTypeName" /></td>
											<td><xsl:value-of select="ProductTypeQty" /></td>
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</xsl:for-each>

					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Share File:</td>
						<td width="80%"></td>
					</tr>

					<xsl:for-each select="SDEEmail">
						<tr>
							<td colspan="2">
								<table border="1">
									<tr bgcolor="#D8D8D8">
										<th>FileName</th>
										<th>FileSize</th>
									</tr>
									<xsl:for-each select="SDEDocInfo">
										<tr>
											<td>
												<a>
													<xsl:attribute name="href">
														<xsl:value-of select="concat(/SDEEmail/SDEInfo/DOCURL, '?docID=', DocID, '&amp;email=1')" />
													</xsl:attribute>
													<xsl:value-of select="DocName" />
												</a>
												<xsl:value-of select="ProductTypeName" />
											</td>
											<td>
												<xsl:value-of select="DocSize" />
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</td>
						</tr>
					</xsl:for-each>

					<tr>
						<td width="20%" style="text-align: left; font-weight: bold;">Comments:</td>
						<td width="80%">
							<xsl:value-of select="/SDEEmail/SDEInfo/Comments" />
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>