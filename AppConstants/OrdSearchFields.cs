﻿using System.ComponentModel;

namespace AppConstants
{
    public enum OrdSearchFields
    {
        [Description("M5 #")]
        M5,

        [Description("H1 Cust Name")]
        H1CustName,

        [Description("H5 Cust Name")]
        H5CustName,

        [Description("Order Type")]
        OrderType,

        [Description("Order Sub Type")]
        OrderSubType,

        [Description("CCD")]
        CCD,
    }
}