﻿using System;
using System.Configuration;

//using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWSRecurringApptParser
{
    public partial class RecurringApptParser : ServiceBase
    {
        private Thread _RecDataParserThread = null;
        private bool _bActive = true;
        private bool _bStopThread = false;
        private bool _bCanBeStoppedNow = true;
        private int _iSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);

        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new RecurringApptParser()
            };
            ServiceBase.Run(ServicesToRun);
        }

        public RecurringApptParser()
        {
            InitializeComponent();
            this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.RecurringApptParser service is starting.", "");

                _bActive = true;
                _bStopThread = false;
                _bCanBeStoppedNow = true;
                _RecDataParserThread = new Thread(new ThreadStart(this.ParseRecAppntmnts));

                CL.WriteLogFile("Main", CL.Event.BeginThread, CL.MsgType.text, -1, string.Empty, "Beginning Thread: (Parse Rec Appointments)", string.Empty);

                _RecDataParserThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.RecurringApptParser. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.RecurringApptParser", "COWS.RecurringApptParser Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                _bStopThread = true;
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.RecurringApptParser", string.Empty);

                _RecDataParserThread.Join(TimeSpan.FromSeconds(30));

                if (!_bCanBeStoppedNow)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }

                CL.WriteLogFile("Main", CL.Event.EndThread, CL.MsgType.text, -1, string.Empty, "Aborting Thread: (Parse Rec Appointments)", string.Empty);

                _RecDataParserThread.Abort();

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.RecurringApptParser Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.RecurringApptParser. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void ParseRecAppntmnts()
        {
            RecurringApptParserBF rbf = new RecurringApptParserBF();
            while (!_bStopThread)
            {
                if (_bActive)
                {
                    //Debug.Assert(false, "test");
                    _bCanBeStoppedNow = false;
                    //if ((DateTime.Now.Hour == 23) && (DateTime.Now.Minute == 14))
                    rbf.ParseRecApptmnts();
                    if ((DateTime.Now.Hour == 0) && (DateTime.Now.Minute == 59))
                        rbf.ProcessNoEndRecAppntmnts();
                    Thread.Sleep(_iSleepTime);
                    _bCanBeStoppedNow = true;
                }
            }
        }
    }
}