@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Culbertson - initial implementation
REM   Scott Musgrave - added option to configure app pool name
REM
REM Script to start or stop the IIS application pool for the website passed in 
REM as the first argument.  The second argument specifies the action to take.
REM An ERRORLEVEL of 0 will be returned if the scripts runs successfully, and a 
REM non-zero ERRORLEVEL will be returned if there are any errors.
REM
REM NOTE: The logical website name is passed in the first parameter. This is both 
REM the name of the website's folder and by default the website's app pool name. If
REM the app pool name is different, add a setting in config.properties named with 
REM the logical website name appended by _APP_POOL_NAME (ex: if you have a website 
REM named 'MyWebsite' but it's app pool is named 'MyAppPool', add the following 
REM setting to config.properties: MyWebsite_APP_POOL_NAME = MyAppPool). 
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM validate correct website was specified
REM
SET WEBSITE=%~1
IF NOT EXIST "%APP_HOME%\Web\%WEBSITE%" (
  ECHO ERROR: invalid website: %WEBSITE%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" recycle_app_pool.cmd 1 "ERROR: invalid website: %WEBSITE%"
  EXIT /b 1
)


REM
REM validate correct action was specified
REM
SET ACTION=%~2
IF /I "%ACTION%" NEQ "START" (
  IF /I "%ACTION%" NEQ "STOP" (
    ECHO ERROR: invalid action: %ACTION%
    CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" recycle_app_pool.cmd 2 "ERROR: invalid action: %ACTION%"
    EXIT /b 2
  )
)


REM
REM validate %COMPUTERNAME% matches local config
REM 
CALL "%APP_HOME%\Scripts\perform-host-check.cmd"
IF ERRORLEVEL 1 (
  ECHO ERROR: host validation failed
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" recycle_app_pool.cmd 3 "ERROR: host validation failed"
  EXIT /b 3
)


REM
REM retrieve WEB_SVC_INDEX for the current server
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd WEB_SVC_INDEX INDEX
IF ERRORLEVEL 1 (
  SET INDEX=
)
SET WEBSITE_NAME=%WEBSITE%%INDEX%



REM
REM initialize app pool name (may be specified in configuration). if not, 
REM name is derived from website name.
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd %WEBSITE%_APP_POOL_NAME APP_POOL_NAME
IF ERRORLEVEL 1 (
SET APP_POOL_NAME=%WEBSITE_NAME%
)



REM
REM Start or Stop the IIS application pool for the website specified
REM
CALL "%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_recycle_app_pool.vbs %APP_POOL_NAME% %ACTION%
IF ERRORLEVEL 1 (
  ECHO ERROR: _recycle_app_pool.vbs failed with exit code %ERRORLEVEL%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" recycle_app_pool.cmd 5 "ERROR: _recycle_app_pool.vbs failed"
  EXIT /b 5
)
