<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="1000" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">H1 : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/H1" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">FTN : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/FTN" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">H5/H6 : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/H5_H6" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Customer Name : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/CUST_NME" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Location : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/Location" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Order Type : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/OrderType" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Customer Commit Date : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/CCD" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Account Team Contact : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/ATC" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Date Cancel Received : </td>
            <td width="80%">
              <xsl:value-of select="/ATCEmail/DCD" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>