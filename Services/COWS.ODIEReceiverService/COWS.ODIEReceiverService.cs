﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWS.ODIEReceiverService
{
    public partial class ODIEReceiverService : ServiceBase
    {
        private Thread _ODIEThread = null;
        private ODIEReceiver _ODIEReceiver = null;
        private string _ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new ODIEReceiverService() };
            ServiceBase.Run(ServicesToRun);
        }

        public ODIEReceiverService()
        {
            InitializeComponent();
            this.ServiceName = _ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.ODIEInService is starting.", "");
                _ODIEThread = new Thread(new ThreadStart(this.GetODIEMessages));
                _ODIEThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.ODIEInService. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.ODIEInService", "COWS.ODIEInService Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.ODIEInService", string.Empty);

                if (_ODIEThread != null)
                {
                    _ODIEThread.Join(TimeSpan.FromSeconds(30));
                    if ((_ODIEReceiver != null) && (!_ODIEReceiver.CanBeStoppedNow))
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                    }

                    _ODIEThread.Abort();
                    _ODIEThread.Join();
                }

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.ODIEInService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.ODIEInService. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void GetODIEMessages()
        {
            _ODIEReceiver = new ODIEReceiver();
            _ODIEReceiver.GetODIEMessages();
        }
    }
}