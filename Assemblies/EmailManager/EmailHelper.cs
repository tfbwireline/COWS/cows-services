﻿using AppConstants;
using IOManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using UserManager;
using fl = LogManager.FileLogger;

namespace EmailManager
{
    /////////////////////////////////////////////////////////////////////////////
    public class EmailHelper
    {
        private static string sServerName = (ConfigurationManager.AppSettings["ServerName"] == null) ? ConfigurationManager.AppSettings["SMTPHost"] : ConfigurationManager.AppSettings["ServerName"];
        private static string sDefaultFrom = ConfigurationManager.AppSettings["DefaultFromEmail"];
        /////////////////////////////////////////////////////////////////////////////

        private static string _sMailHost;

        public static string MailHost
        {
            get { return _sMailHost; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private string _sFrom;

        public string From
        {
            get { return _sFrom; }
        }

        /////////////////////////////////////////////////////////////////////////////
        private bool _bIsHTML;

        public bool IsHTML
        {
            get { return _bIsHTML; }
        }

        /////////////////////////////////////////////////////////////////////////////
        public EmailHelper()
        {
            SetProperties(sServerName, sDefaultFrom, true);
        }

        /////////////////////////////////////////////////////////////////////////////
        public EmailHelper(string sFrom, bool bIsHtml)
        {
            SetProperties(sServerName, sFrom, bIsHtml);
        }

        /////////////////////////////////////////////////////////////////////////////
        public EmailHelper(string sMailHost, string sFrom, bool bIsHtml)
        {
            SetProperties(sMailHost, sFrom, bIsHtml);
        }

        /////////////////////////////////////////////////////////////////////////////
        protected void SetProperties(string sMailHost, string sFrom, bool bIsHtml)
        {
            if (sMailHost != string.Empty)
            {
                _sMailHost = sMailHost;
            }

            if (sFrom != string.Empty)
            {
                _sFrom = sFrom;
            }

            _bIsHTML = bIsHtml;
        }

        /////////////////////////////////////////////////////////////////////////////
        public void Wait(int iNumSecs)
        {
            DateTime finish = DateTime.Now.AddSeconds(iNumSecs);
            while (DateTime.Now < finish)
            {
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public void SEND_MAIL(SendType type, User usr, string sSubject, string sBody)
        {
            ArrayList uList = new ArrayList(1);
            uList.Add(usr);
            SEND_MAIL(type, uList, sSubject, sBody, 1, 1);
        }

        /////////////////////////////////////////////////////////////////////////////
        public string SEND_MAIL(SendType type, ArrayList uList, string sSubject, string sBody, int iInterval, int iWaitLength)
        {
            try
            {
                string sEmailList = string.Empty;
                string sNoEmails = string.Empty;
                int iCount = 0;
                int iRecipCount = uList.Count;

                SmtpClient sc = new SmtpClient(MailHost);

                if (sEmailList != string.Empty)
                {
                    MailMessage mymail = new MailMessage(sDefaultFrom, sEmailList, sSubject, sBody);

                    mymail.Priority = MailPriority.High;

                    mymail.IsBodyHtml = IsHTML;

                    if (iInterval > iRecipCount)
                    {
                        iInterval = iRecipCount;
                    }

                    IEnumerator enumer = uList.GetEnumerator();
                    while (enumer.MoveNext())
                    {
                        iCount += 1;

                        User uinfo = (User)enumer.Current;

                        if (uinfo.Email != string.Empty)
                        {
                            sEmailList += (uinfo.Email + ";");
                        }
                        else
                        {
                            sNoEmails += (uinfo.Email + "; ");
                        }

                        if (iCount >= iInterval)
                        {
                            if (sEmailList != string.Empty)
                            {
                                sc.Send(mymail);
                            }

                            sEmailList = string.Empty;
                            iCount = 0;

                            Wait(iWaitLength);
                        }
                    }
                }
                return sNoEmails;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write("<br><br>" + ex.ToString() + "<br><br>");
            }
            return string.Empty;
        }

        /////////////////////////////////////////////////////////////////////////////
        public void SendSingleEmail(SendType type, string sEmailTo, string sSubject, string sBody)
        {
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            //!! sendtype not used!!!
            if (sEmailTo.Length > 0)
                SendSingleEmail(sEmailTo, string.Empty, string.Empty, sSubject, sBody);
        }

        public static string _SendEmailInLoop(bool bIsHtml, MailPriority priority, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody)
        {
            StringBuilder errEmail = new StringBuilder();

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();

            if (sEmailTo.Length > 0)
            {
                Array arr = sEmailTo.Split(new Char[] { ',' });

                foreach (string singleEmail in arr)
                {
                    if (singleEmail != null)
                    {
                        if (!singleEmail.Trim().Equals(string.Empty))
                        {
                            /*if (!sUploadFilePath.Trim().Equals(string.Empty))
                            {
                                if (!(_SendSingleEmail(bIsHtml, priority, sEmailFrom, singleEmail, string.Empty, string.Empty, sSubject.ToString(), sBody.ToString(), sUploadFilePath)))
                                {
                                    errEmail.Append(singleEmail);
                                }
                            }
                            else
                            {*/
                            if (!(_SendSingleEmail(bIsHtml, priority, sEmailFrom, singleEmail, string.Empty, string.Empty, sSubject.ToString(), sBody.ToString())))
                            {
                                errEmail.Append(singleEmail);
                            }

                            //}
                        }
                    }
                }
            }
            else
                errEmail.Append("Empty To: Email Address");

            return errEmail.ToString();
        }

        /////////////////////////////////////////////////////////////////////////////
        public static bool _SendSingleEmail(bool bIsHtml, MailPriority priority, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            MailMessage myMail = new MailMessage();
            try
            {
                if (sEmailTo.Length > 0)
                {
                    myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);

                    SmtpClient sc = new SmtpClient(sServerName);
                    myMail.IsBodyHtml = bIsHtml;
                    myMail.Priority = priority;
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;

                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    sc.Send(myMail);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", myMail.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                info.Add("Subject", myMail.Subject);
                info.Add("Body", myMail.Body);
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            return bResult;
        }

        // NMV1547, CSB9923 - 01/04/06  - D5435 PACKAGE CODE 7  - BEGIN
        //To Send Email with Attatchment.
        public static bool _SendSingleEmail(bool bIsHtml, MailPriority priority, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody, string sAttach)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            MailMessage myMail = new MailMessage();
            try
            {
                if (sEmailTo.Length > 0)
                {
                    myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);

                    SmtpClient sc = new SmtpClient(sServerName);

                    myMail.IsBodyHtml = bIsHtml;
                    myMail.Priority = priority;
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;
                    myMail.Attachments.Add(new Attachment(sAttach));

                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    sc.Send(myMail);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", myMail.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                info.Add("Subject", myMail.Subject);
                info.Add("Body", myMail.Body);
                info.Add("Attach", myMail.Attachments[0].ToString());
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            return bResult;
        }

        //To Send Email with Attatchment as Stream
        public static bool SendSingleEmail(bool bIsHtml, MailPriority priority, string sEmailFrom, string sEmailTo,
                                            string sEmailCC, string sEmailBCC, string sSubject, string sBody,
                                            List<KeyValuePair<string, Stream>> attachments)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            MailMessage myMail = new MailMessage();
            try
            {
                if (sEmailTo.Length > 0)
                {
                    myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);

                    SmtpClient sc = new SmtpClient(sServerName);

                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    myMail.IsBodyHtml = bIsHtml;
                    myMail.Priority = priority;
                    myMail.Subject = sSubject;
                    myMail.Body = Regex.Replace(sBody, @"(?<!\t)((?<!\r)(?=\n)|(?=\r\n))", "\t", RegexOptions.Multiline);
                    if (attachments != null)
                    {
                        if (attachments.Count > 0)
                        {
                            foreach (KeyValuePair<string, Stream> attch in attachments)
                            {
                                myMail.Attachments.Add(new Attachment((Stream)attch.Value, attch.Key));
                            }
                        }
                    }
                    sc.Send(myMail);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", myMail.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                info.Add("Subject", myMail.Subject);
                info.Add("Body", myMail.Body);
                info.Add("Attach", myMail.Attachments[0].ToString());
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            return bResult;
        }

        public static bool SendSingleEmail(string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody, List<string> attachments)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            MailMessage myMail = new MailMessage();
            SmtpClient sc = new SmtpClient(sServerName);
            try
            {
                if (sEmailTo.Length > 0)
                {
                    myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);

                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    myMail.IsBodyHtml = false;
                    myMail.Priority = MailPriority.Normal;
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;
                    myMail.Body = Regex.Replace(sBody, @"(?<!\t)((?<!\r)(?=\n)|(?=\r\n))", "\t", RegexOptions.Multiline);
                    if (attachments != null)
                    {
                        if (attachments.Count > 0)
                        {
                            foreach (string attch in attachments)
                            {
                                //s = new MemoryStream(attch.Value);
                                //myMail.Attachments.Add(new Attachment(s, attch.Key));
                                //FileInfo fi = new FileInfo(attch);
                                myMail.Attachments.Add(new Attachment(attch));
                            }
                        }
                    }
                    sc.Send(myMail);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", myMail.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                info.Add("Subject", myMail.Subject);
                info.Add("Body", myMail.Body);
                info.Add("Attach", myMail.Attachments[0].ToString());
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            finally
            {
                if ((myMail.Attachments != null) && (myMail.Attachments.Count > 0))
                {
                    foreach (Attachment att in myMail.Attachments)
                    {
                        att.Dispose();
                    }
                }
                myMail.Dispose();
                sc.Dispose();
                if ((attachments != null) && (attachments.Count > 0))
                {
                    foreach (string path in attachments)
                    {
                        int i = 0;
                        while ((File.Exists(path)) && (i < 10))
                        {
                            try
                            {
                                File.Delete(path);
                            }
                            catch (Exception ex) { fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty); }
                            i++;
                        }
                    }
                }
            }
            return bResult;
        }

        public static bool SendSingleEncryptEmail(X509Certificate2 EncryptCert, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody, List<string> attachments)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            SmtpClient sc = new SmtpClient(sServerName);
            MailMessage message = new MailMessage();
            try
            {
                if (sEmailTo.Length > 0)
                {
                    string[] sFrom = sEmailFrom.Split(new Char[] { ',' });
                    string[] sTo = sEmailTo.Split(new Char[] { ',' });
                    string[] sCC = sEmailCC.Split(new Char[] { ',' });

                    //string[] sBCC = sEmailBCC.Split(new Char[] { ',' });
                    /*Stream s;
                    StringBuilder Message = new StringBuilder();
                    MailMessage myMail = new MailMessage(sEmailFrom, sEmailTo);
                    SmtpClient sc = new SmtpClient(sServerName);
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;
                    Message.AppendLine("content-type: multipart/mixed;");
                    Message.AppendLine(" boundary=\"----=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX\"");
                    Message.AppendLine();
                    Message.AppendLine("This is a multi-part message in MIME format.");
                    Message.AppendLine();
                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");
                    Message.AppendLine("content-type: text/html; charset=\"us-ascii\"");
                    Message.AppendLine("content-transfer-encoding: 7bit");
                    Message.AppendLine();
                    Message.AppendLine(sBody);
                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");
                    Message.AppendLine("content-disposition: attachment; filename=\"" + "x.txt" + "\"");
                    Message.AppendLine("content-transfer-encoding: base64");
                    Message.AppendLine("content-type: application/edine;\n name=\"" + "E_x.htm" + "\"");
                    Message.AppendLine();

                    if (attachments != null)
                    {
                        if (attachments.Count > 0)
                        {
                            foreach (KeyValuePair<string, byte[]> attch in attachments)
                            {
                                //myMail.Attachments.Add(new Attachment((Stream)attch.Value, attch.Key));
                                //Message.AppendLine(Convert.ToBase64String(attch.Value));
                                s = new MemoryStream(attch.Value);
                                myMail.Attachments.Add(new Attachment(s, attch.Key));
                            }
                        }
                    }

                    //Message.AppendLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(data)));
                    string data = string.Empty;
                    try
                    {
                        data = File.ReadAllText(sFilePath);
                    }
                    catch
                    {
                        data = " Failed to add attachment to Mail. [CYP]";
                    }

                    Message.AppendLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(data)));
                    Message.AppendLine();

                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX--");

                    byte[] BodyBytes = Encoding.ASCII.GetBytes(Message.ToString());

                    EnvelopedCms ECms = new EnvelopedCms(new ContentInfo(BodyBytes));

                    CmsRecipient Recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, EncryptCert);

                    ECms.Encrypt(Recipient);

                    byte[] EncryptedBytes = ECms.Encode();

                    MemoryStream ms1 = new MemoryStream(EncryptedBytes);
                    AlternateView av1 = new AlternateView(ms1, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m; content-transfer-encoding=Base64; content-disposition=attachment; fileName=smime.p7m;");

                    myMail.AlternateViews.Add(av1);

                    myMail.IsBodyHtml = true;
                    myMail.Priority = MailPriority.Normal;

                    sc.Send(myMail);*/

                    message.From = new MailAddress(sEmailFrom);
                    message.Subject = sSubject;

                    if (attachments != null && attachments.Count > 0)
                    {
                        StringBuilder buffer = new StringBuilder();
                        buffer.Append("MIME-Version: 1.0\r\n");
                        buffer.Append("Content-Type: multipart/mixed; boundary=unique-boundary-1\r\n");
                        buffer.Append("\r\n");
                        buffer.Append("This is a multi-part message in MIME format.\r\n");
                        buffer.Append("--unique-boundary-1\r\n");
                        buffer.Append("Content-Type: text/plain\r\n");  //could use text/html as well here if you want a html message
                        buffer.Append("Content-Transfer-Encoding: 7Bit\r\n\r\n");
                        buffer.Append(sBody);
                        if (!sBody.EndsWith("\r\n"))
                            buffer.Append("\r\n");
                        buffer.Append("\r\n\r\n");

                        foreach (string filename in attachments)
                        {
                            FileInfo fileInfo = new FileInfo(filename);
                            buffer.Append("--unique-boundary-1\r\n");
                            buffer.Append("Content-Type: application/octet-stream; file=" + fileInfo.Name + "\r\n");
                            buffer.Append("Content-Transfer-Encoding: base64\r\n");
                            buffer.Append("Content-Disposition: attachment; filename=" + fileInfo.Name + "\r\n");
                            buffer.Append("\r\n");
                            byte[] binaryData = File.ReadAllBytes(filename);

                            string base64Value = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                            int position = 0;
                            while (position < base64Value.Length)
                            {
                                int chunkSize = 100;
                                if (base64Value.Length - (position + chunkSize) < 0)
                                    chunkSize = base64Value.Length - position;
                                buffer.Append(base64Value.Substring(position, chunkSize));
                                buffer.Append("\r\n");
                                position += chunkSize;
                            }
                            buffer.Append("\r\n");
                        }

                        sBody = buffer.ToString();
                    }
                    else
                    {
                        sBody = "Content-Type: text/plain\r\nContent-Transfer-Encoding: 7Bit\r\n\r\n" + sBody;
                    }

                    byte[] messageData = Encoding.ASCII.GetBytes(sBody);
                    ContentInfo content = new ContentInfo(messageData);
                    EnvelopedCms envelopedCms = new EnvelopedCms(content);
                    CmsRecipientCollection toCollection = new CmsRecipientCollection();
                    foreach (string address in sTo)
                    {
                        if (address != string.Empty)
                        {
                            message.To.Add(new MailAddress(address));
                            X509Certificate2 certificate = EncryptCert;
                            CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                            toCollection.Add(recipient);
                        }
                    }

                    foreach (string address in sCC)
                    {
                        if (address != string.Empty)
                        {
                            message.CC.Add(new MailAddress(address));
                            X509Certificate2 certificate = EncryptCert;
                            CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                            toCollection.Add(recipient);
                        }
                    }

                    //foreach (string address in sBCC)
                    //{
                    //    message.Bcc.Add(new MailAddress(address));
                    //    X509Certificate2 certificate = EncryptCert;
                    //    CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                    //    toCollection.Add(recipient);
                    //}

                    envelopedCms.Encrypt(toCollection);
                    byte[] encryptedBytes = envelopedCms.Encode();

                    //add digital signature:
                    /*SignedCms signedCms = new SignedCms(new ContentInfo(encryptedBytes));
                    X509Certificate2 signerCertificate = EncryptCert;
                    CmsSigner signer = new CmsSigner(SubjectIdentifierType.SubjectKeyIdentifier, signerCertificate);
                    signedCms.ComputeSignature(signer);
                    encryptedBytes = signedCms.Encode();*/

                    //end digital signature section

                    MemoryStream stream = new MemoryStream(encryptedBytes);
                    AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m; content-transfer-encoding=Base64; content-disposition=attachment; fileName=smime.p7m;");

                    //AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
                    message.AlternateViews.Add(view);

                    sc.Send(message);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((message.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", message.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(message.From) + "]");
                info.Add("To", "[" + Convert.ToString(message.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(message.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(message.Bcc) + "]");
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            finally
            {
                if ((message.Attachments != null) && (message.Attachments.Count > 0))
                {
                    foreach (Attachment att in message.Attachments)
                    {
                        att.Dispose();
                    }
                }
                message.Dispose();
                sc.Dispose();
                if ((attachments != null) && (attachments.Count > 0))
                {
                    foreach (string path in attachments)
                    {
                        int i = 0;
                        while ((File.Exists(path)) && (i < 10))
                        {
                            try
                            {
                                File.Delete(path);
                            }
                            catch (Exception ex) { fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty); }
                            i++;
                        }
                    }
                }
            }
            return bResult;
        }

        // NMV1547, CSB9923 - 01/04/06  - D5435 PACKAGE CODE 7  - END
        /////////////////////////////////////////////////////////////////////////////
        public static bool _SendSingleEmail(string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody)
        {
            return _SendSingleEmail(true, MailPriority.High, sEmailFrom, sEmailTo, sEmailCC, sEmailCC, sSubject, sBody);
        }

        /////////////////////////////////////////////////////////////////////////////
        public static bool _SendSingleEmail(string sEmailFrom, string sEmailTo, string sSubject, string sBody)
        {
            return _SendSingleEmail(true, MailPriority.High, sEmailFrom, sEmailTo, string.Empty, string.Empty, sSubject, sBody);
        }

        /////////////////////////////////////////////////////////////////////////////
        public bool SendSingleEmail(string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody)
        {
            return _SendSingleEmail(IsHTML, MailPriority.High, From, sEmailTo, sEmailCC, sEmailCC, sSubject, sBody);
        }

        /////////////////////////////////////////////////////////////////////////////
        public bool SendMultipleEmails(SendType sndType, string sMailList, string sSubject, string sBody)
        {
            bool bResult = true;

            try
            {
                string[] sArrResult = sMailList.Split(';');

                if (sArrResult.Length > 0)
                {
                    for (int i = 0; i <= sArrResult.Length - 1; i++)
                    {
                        if (bResult && (sArrResult[i].Trim() != string.Empty))
                        {
                            switch (sndType)
                            {
                                case SendType.NORMAL:
                                    if (!_SendSingleEmail(IsHTML, MailPriority.High, From, sArrResult[i], string.Empty, string.Empty, sSubject, sBody))
                                    {
                                        bResult = false;
                                    }
                                    break; // TODO: might not be correct. Was : Exit Select

                                case SendType.CC:
                                    if (!_SendSingleEmail(IsHTML, MailPriority.High, From, string.Empty, sArrResult[i], string.Empty, sSubject, sBody))
                                    {
                                        bResult = false;
                                    }
                                    break; // TODO: might not be correct. Was : Exit Select

                                case SendType.BCC:
                                    if (!_SendSingleEmail(IsHTML, MailPriority.High, From, string.Empty, string.Empty, sArrResult[i], sSubject, sBody))
                                    {
                                        bResult = false;
                                    }
                                    break; // TODO: might not be correct. Was : Exit Select
                            }
                        }
                    }
                }
            }
            catch
            {
                bResult = false;
            }

            return bResult;
        }

        /////////////////////////////////////////////////////////////////////////////
        public void SendErrorEmail(SendType type, string sSendTo, string sWebUser, Exception ex)
        {
            try
            {
                string sBody = "An error has occured on one of the COWS webpages.  See error description below: <br><br>";

                if (sWebUser != string.Empty)
                {
                    sBody += "User: " + sWebUser + "<br>";
                }
                else
                {
                    try
                    {
                        sBody += "User: " + User.LogonUser() + "<br>";
                    }
                    catch (Exception)
                    {
                        sBody += "User: unknown<br>";
                    }
                }

                sBody += "Page: " + HttpContext.Current.Request.ServerVariables["PATH_INFO"].ToString() + "<br>";
                sBody += "Error: " + ex.Message.ToString() + "<br>";
                sBody += "StackTrace: " + ex.StackTrace.ToString() + "<br>";

                SendSingleEmail(type, "jagannath.r.gangi@sprint.com; " + sSendTo, "ASPNET WebPage Error", sBody.ToString());
            }
            catch (Exception)
            {
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public static string GetADNotice()
        {
            return "If you get prompted for a username/password please use your Sprint Active Directory (AD) user name (in lower case letters) and password to log in. The domain name will be (AD). It is the same user name and password that you use to log in to your Windows 2000 desktop or your Microsoft Outlook email account.<br><br>";
        }

        /////////////////////////////////////////////////////////////////////////////
        public static string GetDefaultFrom()
        {
            return sDefaultFrom;
        }
    }
}