@echo off
REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave 
REM
REM Script to Monitor the Staging Folder 
REM
REM
REM
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%


REM
REM validate %COMPUTERNAME% matches local config
REM 

CALL "%APP_HOME%\Scripts\perform-host-check.cmd"
IF ERRORLEVEL 1 (
  ECHO ERROR: host validation failed
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 3 "ERROR: host validation failed"
  EXIT /b 3
)




REM
REM initialize HOST_CHECK from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd HOST_CHECK HOST_NAME
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve HOST_CHECK from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve HOST_CHECK from Configuration"
  EXIT /b 2
)

REM
REM initialize EMAIL_ADDRESS from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd DEPLOY_EMAIL_ADDRESS EMAIL_ADDRESS
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve DEPLOY_EMAIL_ADDRESS from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve DEPLOY_EMAIL_ADDRESS from Configuration"
  EXIT /b 2
)


%WINDIR%\System32\WindowsPowerShell\v1.0\powershell.exe "%APP_HOME%"\Scripts\MonitorStaging.ps1 -LOCATION '"%APP_HOME%"\Staging' -EMAIL_ADDRESS '%EMAIL_ADDRESS%' -SERVER '%HOST_NAME%'

IF ERRORLEVEL 1 (
  ECHO ERROR: MonitorStaging.ps1 failed with exit code %ERRORLEVEL%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitorStaging.ps1 6 "ERROR: monitorStaging.ps1 failed"
  EXIT /b 6
) ELSE (
  ECHO INFO: MonitorStaging.ps1 Staging Folder Monitor Completed Successfully
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" Monitor_cert.ps1 0 "INFO: Staging Folder Monitor Completed Successfully"
  EXIT /b 0
)