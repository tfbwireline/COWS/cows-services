﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using CL = LogManager.FileLogger;

namespace COWS.SSTATReceiverService
{
    public partial class SSTATReceiverService : ServiceBase
    {
        private Thread _SSTATThread = null;
        private SSTATReceiver _SSTATReceiver = null;
        private string _ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
        private int _iRequestSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SleepTime"]);

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new SSTATReceiverService() };
            ServiceBase.Run(ServicesToRun);
        }

        public SSTATReceiverService()
        {
            InitializeComponent();
            this.ServiceName = _ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CL.WriteLogFile("Main", CL.Event.BeginStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATInService is starting.", "");
                _SSTATThread = new Thread(new ThreadStart(this.GetSSTATMessages));
                _SSTATThread.Start();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to start COWS.SSTATInService. Error: " + e.ToString(), string.Empty);
                }
                catch { }
            }
            CL.WriteLogFile("Main", CL.Event.EndStart, CL.MsgType.text, -1, string.Empty, "COWS.SSTATInService", "COWS.SSTATInService Started at :" + DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            try
            {
                base.OnStop();
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Stopping COWS.SSTATInService", string.Empty);

                if (_SSTATThread != null)
                {
                    _SSTATThread.Join(TimeSpan.FromSeconds(30));
                    if ((_SSTATReceiver != null) && (!_SSTATReceiver.CanBeStoppedNow))
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(30));
                    }

                    _SSTATThread.Abort();
                    _SSTATThread.Join();
                }

                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "COWS.SSTATInService Stopped at: " + DateTime.Now.ToString(), string.Empty);
                CL.DequeueMessages();
            }
            catch (Exception e)
            {
                try
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Unable to stop COWS.SSTATInService. Error: " + e.ToString(), string.Empty);
                    CL.DequeueMessages();
                }
                catch { }
            }
        }

        private void GetSSTATMessages()
        {
            _SSTATReceiver = new SSTATReceiver();
            _SSTATReceiver.GetSSTATMessages();
            Thread.Sleep(_iRequestSleepTime);
        }
    }
}