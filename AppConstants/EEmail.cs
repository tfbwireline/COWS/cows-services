﻿namespace AppConstants
{
    public enum SendType
    {
        NORMAL = 1,
        CC = 2,
        BCC = 3
    }
}