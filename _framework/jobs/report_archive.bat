@echo off
REM *******************************************************************************
REM Copyright (c) 2009 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave - initial implementation
REM
REM Application job copies files to archive folder removing old files.
REM 
REM *******************************************************************************


REM
REM initialize application home and call install script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%

%WINDIR%\System32\WindowsPowerShell\v1.0\powershell.exe "%APP_HOME%\Scripts\MoveCOWSRpts.ps1"