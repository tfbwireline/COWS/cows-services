# *******************************************************************************
# Date: 11/15/2013
# Author:  Frank Luna 
#
# Application: Script to move files from report directories to archive directory
# 
# *******************************************************************************
Try
{
    # This sets up date for log name
    $path = 'E:\PROD_E\QDA'
    $date = get-date 
    $StrDate = $date.tostring("MM-dd-yyyy")

    # This moves reports from report directory to archive directory. /e copys subdirectories, 
    #/r:2 number retries, /is keeps job from failing if duplicate report is already out in folder, minage set to 365 days
    # creates log named (current date) log.tx
    robocopy $path\Data\COWS\Reports $path\Data\COWS\Archive\Reports /e /mov /r:2 /is /minage:365 /xx /np /log:$path\Logs\COWS\MoveCowsRpts\$StrDate' 'Log.txt 

    # Deletes any log reports older than 30 days
    Get-ChildItem -Path $path\Logs\COWS\MoveCowsRpts\*.txt |
    Where-object {$_.CreationTime -lt (get-date).AddDays(-30)} | remove-Item
}

Catch [exception]
{
    $ErrorMessage =  $_.Exception.Message
    $FailedItem = $_.Exception.ItemName |
    out-file $path\Logs\COWS\MoveCowsRpts\$StrDate' 'Log.txt -append
}

