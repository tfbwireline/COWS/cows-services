﻿namespace AppConstants
{
    public enum EUCaaSProdType
    {
        CISCOHCS = 1,
        MIPT = 2,
        SmartUC = 3,
        SmartUCTollFree = 4,
        SIPT = 5,
        SDV = 6
    }
}