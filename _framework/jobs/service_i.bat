@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM *** WINDOWS SERVICE SCRIPT ***
REM 
REM AVT framework job to install the "@SERVICE_TAG@" application component as a Windows 
REM service. An ERRORLEVEL of 0 will be returned if the service is installed
REM successfully, and a non-zero ERRORLEVEL will be returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize application home and call install script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\Scripts\install_@SERVICE_TAG@.cmd"
