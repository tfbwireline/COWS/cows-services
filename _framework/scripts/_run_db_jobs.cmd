REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave - initial implementation
REM
REM *** Database Script ***
REM 
REM  Script to Run Job the SQL Server Snapshot of for the Readonly and backup purposes.
REM
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM Get Connection String
REM
SET CONN_STRING=%~1

REM
REM Get Job Name
REM
SET DB_JOB=%~2


REM
REM Exicute the job on the SQL Server Database
REM
CALL osql -S %CONN_STRING% -Q"exec msdb.dbo.sp_start_job ' %DB_JOB% ' "

IF ERRORLEVEL 1 (
  ECHO ERROR:_run_db_jobs.cmd failed with exit code %ERRORLEVEL%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" _run_db_jobs.cmd 5 "ERROR: _run_db_jobs.cmd failed"
  EXIT /b 5
)


