﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using FL = LogManager.FileLogger;
using o = TadpoleOrder;
using r = TadpoleResponse;

namespace TadpoleHelper
{
    public class TadpoleServiceIO
    {
        private TadpoleEventDB teDB = new TadpoleEventDB();
        private o.tadpoleToCows Order;
        private List<ActionType> installOrderTypes = new List<ActionType> { ActionType.NCI, ActionType.NCC, ActionType.NCM, ActionType.NIM, ActionType.NDM, ActionType.NIC };
        private List<ActionType> disconnectOrderTypes = new List<ActionType> { ActionType.DCC, ActionType.DCS };
        private string _strError = string.Empty;

        public string strError
        {
            get { return _strError; }
            set { _strError = value; }
        }

        #region Incoming messages

        public ResponseContainer ProcessXML(string xmlString, bool toLog)
        {
            ResponseContainer rc = new ResponseContainer();
            Order = new o.tadpoleToCows();

            string validationResult = ValidateXML(xmlString);
            if (validationResult == string.Empty)
            {
                //Until validation is fully turned back on, have to add a try/catch around the deserialization section
                //7/10/2012 - Turning validation back on, but no reason to remove the try/catch block.
                try
                {
                    Order = (o.tadpoleToCows)DeserializeXML(xmlString);
                }
                catch (Exception ex)
                {
                    //Don't NACK their ACKs since we don't take any action on the ACK
                    string regexACK = "<acknowledgment[^>]*>";
                    Match ReqACKMatch = Regex.Match(xmlString, regexACK);
                    if (ReqACKMatch != null && ReqACKMatch.ToString() != string.Empty)
                    {
                        FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "ACK/NACK received but unable to convert to object.  Ignoring message - "
                            + xmlString + Environment.NewLine + "Exception - " + ex.ToString(), string.Empty);
                        return rc;
                    }

                    GetNACKInfo(xmlString);
                    if (toLog)
                        teDB.LogRejectXML(xmlString, validationResult + " " + ex.ToString(), Order.requestInformation.requestID);
                    rc.ACKResponse = CreateACKNACKResponse(r.Status.Rejected, "Unable to deserialize xml string to an object");
                    _strError += "Unable to deserialize xml string to an object";
                    return rc;
                }

                //Combining ACK/Confirm into one...
                //rc.ACKResponse = CreateACKNACKResponse(r.Status.Accepted, string.Empty);
            }
            else
            {
                //Don't NACK their ACKs since we don't take any action on the ACK
                string regexACK = "<acknowledgment[^>]*>";
                Match ReqACKMatch = Regex.Match(xmlString, regexACK);
                if (ReqACKMatch != null && ReqACKMatch.ToString() != string.Empty)
                {
                    FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "ACK/NACK received but failed xsd validation.  Ignoring message - "
                        + xmlString + Environment.NewLine + validationResult, string.Empty);
                    return rc;
                }

                GetNACKInfo(xmlString);
                if (toLog)
                    teDB.LogRejectXML(xmlString, validationResult, Order.requestInformation.requestID);
                rc.ACKResponse = CreateACKNACKResponse(r.Status.Rejected, validationResult);
                return rc;
            }

            //Convert to BusinessEntity and Insert
            try
            {
                List<string> errors = ConvertToBusinessEntityAndInsertToDB(Order, xmlString, toLog);
                if (errors.Count > 0)
                {
                    if (toLog)
                    {
                        string errorLog = "";
                        foreach (string e in errors)
                            errorLog += e + ";";

                        teDB.LogRejectXML(xmlString, errorLog, Order.requestInformation.requestID);
                    }
                    rc.ACKResponse = CreateACKNACKResponse(r.Status.Rejected, errors);
                }
                else
                {
                    FL.WriteLogFile("Receiver", FL.Event.GenericMessage, FL.MsgType.text, -1, string.Empty, "Event Successfully processed and databased, RequestID/RequestType/RequestVersion : " + Order.requestInformation.requestID.ToString() + "/" + Order.requestInformation.requestType.ToString() + "/" + Order.requestVersion.ToString(), string.Empty);
                    rc.ACKResponse = CreateACKNACKResponse(r.Status.Accepted);
                }
                return rc;
            }
            catch (IgnoreMessage im)
            {
                if (im.Message.Length > 0)
                    FL.WriteLogFile("Receiver", FL.Event.GenericMessage, FL.MsgType.text, -1, string.Empty, "Receiver Error - " + im.Message + "; " + im.StackTrace, string.Empty);
                return new ResponseContainer();
            }
        }

        public void GetNACKInfo(string xmlString)
        {
            if (Order == null)
                Order = new o.tadpoleToCows();
            if (Order.requestInformation == null)
                Order.requestInformation = new o.RequestInformation();

            string regexReqID = "<requestID[^>]*>[0-9-]+</requestID>";
            string regexFRBType = "<requestType[^>]*>[a-zA-Z]+</requestType>";
            string regexType = "<responseDescription[^>]*>[a-zA-Z]+</responseDescription>";
            string regexVer = "<requestVersion[^>]*>[0-9]+</requestVersion>";
            Match ReqIDMatch = Regex.Match(xmlString, regexReqID);
            Match ReqFRBTypMatch = Regex.Match(xmlString, regexFRBType);
            Match ReqTypMatch = Regex.Match(xmlString, regexType);
            Match ReqVerMatch = Regex.Match(xmlString, regexVer);

            if (ReqIDMatch != null && ReqIDMatch.ToString() != string.Empty)
            {
                int rID = 0;
                Int32.TryParse(ReqIDMatch.ToString().Remove(0, ReqIDMatch.ToString().IndexOf(">") + 1).Replace("</requestID>", ""), out rID);
                Order.requestInformation.requestID = rID;
            }

            if (ReqFRBTypMatch != null && ReqFRBTypMatch.ToString() != string.Empty)
                Order.requestInformation.requestType = MapFRBType(MapFRBType(ReqFRBTypMatch.ToString().Remove(0, ReqFRBTypMatch.ToString().IndexOf(">") + 1).Replace("</requestType>", "")));

            if (ReqTypMatch != null && ReqTypMatch.ToString() != string.Empty)
                Order.responseDescription = MapDescription(MapDescription(ReqTypMatch.ToString().Remove(0, ReqTypMatch.ToString().IndexOf(">") + 1).Replace("</responseDescription>", "")));

            if (ReqVerMatch != null && ReqVerMatch.ToString() != string.Empty)
            {
                int verID = 0;
                Int32.TryParse(ReqVerMatch.ToString().Remove(0, ReqVerMatch.ToString().IndexOf(">") + 1).Replace("</requestVersion>", ""), out verID);
                Order.requestVersion = verID;
            }
        }

        public string CreateACKNACKResponse(r.Status status)
        {
            List<string> emptyList = null;
            return CreateACKNACKResponse(status, emptyList);
        }

        public string CreateACKNACKResponse(r.Status status, string error)
        {
            if (error == null || error == string.Empty)
                return CreateACKNACKResponse(status, new List<string>());
            else
                return CreateACKNACKResponse(status, new List<string> { error });
        }

        public string CreateACKNACKResponse(r.Status status, List<string> errorList)
        {
            r.cowsToTadpole response = new r.cowsToTadpole();
            response.requestInformation = new r.RequestInformation();

            if (Order.requestInformation != null)
            {
                response.requestInformation.requestID = Order.requestInformation.requestID;
                response.requestVersion = Order.requestVersion;
                response.requestInformation.requestType = MapFRBType(Order.requestInformation.requestType);
            }
            else
            {
                response.requestInformation.requestID = 0;
                response.requestVersion = 0;
                response.requestInformation.requestType = r.RequestInformationRequestType.NewOrderInstall;
            }
            response.requestInformation.timestamp = DateTime.Now.ToUniversalTime();
            response.responseDescription = MapDescription(Order.responseDescription);

            r.AcknowledgmentInformation ACKNode = new r.AcknowledgmentInformation();
            ACKNode.status = status;
            if (errorList != null)
                ACKNode.errorDescriptionList = errorList.ToArray();
            response.Item = ACKNode;

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(r.cowsToTadpole));
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, response);
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        private object DeserializeXML(string sXML)
        {
            XmlSerializer XS = new XmlSerializer(typeof(o.tadpoleToCows));
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(sXML);
            MemoryStream MS = new MemoryStream(byteArray);
            return XS.Deserialize(MS);
        }

        private List<string> ConvertToBusinessEntityAndInsertToDB(o.tadpoleToCows Order, string xmlString, bool toLog)
        {
            TadpoleEventDataEntity EventData = new TadpoleEventDataEntity();
            TadpoleEventDataEntity oldEvent = null;
            List<string> errors = new List<string>();
            int oldstatus = 0;

            #region XML Common Region

            EventData.SequenceNumber = Order.requestVersion;
            EventData.OrderType = GetCOWSCodeForRequestType(Order.responseDescription);
            if (CleanNull(Order.requestReceiveDate) != string.Empty)
            {
                DateTimeOffset dt;
                DateTimeOffset.TryParse(Order.requestReceiveDate, out dt);
                if (dt != new DateTimeOffset())
                    EventData.FRBSentDate = dt.LocalDateTime;
                else
                    errors.Add("Invalid Request Received Date, unable to convert to DateTime.");
            }

            if (Order.requestInformation != null)
            {
                EventData.FRBRequestID = Order.requestInformation.requestID;
                EventData.FRBOrderType = GetCOWSCodeForRequestType(Order.requestInformation.requestType);
            }
            else
                errors.Add("Request Information tag not included.");

            #endregion XML Common Region

            #region ACK

            if (Order.Item.GetType() == typeof(o.AcknowledgmentInformation))
            {
                o.AcknowledgmentInformation ack = (o.AcknowledgmentInformation)Order.Item;
                if (ack.status == o.Status.Rejected)
                {
                    //Update status to errored when a NACK is sent to us
                    string requestType = GetMessageName(Order.responseDescription);

                    if (requestType != string.Empty && Order.requestInformation != null)
                    {
                        teDB.UpdateMessageStatusToErrored(EventData.FRBRequestID, EventData.SequenceNumber, requestType);
                    }
                }
                FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "ACK/NACK received - " + xmlString, string.Empty);
                throw new IgnoreMessage();
            }

            #endregion ACK

            #region NewOrderInstall

            if (Order.Item.GetType() == typeof(o.tadpoleToCowsNewOrderInstall))
            {
                o.tadpoleToCowsNewOrderInstall InstallNode = (o.tadpoleToCowsNewOrderInstall)Order.Item;

                if (Order.responseDescription != o.ResponseDescription.NewOrderInstall)
                {
                    errors.Add("NewOrderInstall tag is populated, but request description is not New Install.");
                }
                int seq = teDB.GetPreviousSequence(EventData.FRBRequestID);
                if (seq != 0)
                {
                    errors.Add("NewOrderInstall received, but there is already a pending order on version " + seq.ToString());
                }
                EventData.Status = 2;   //Pending
                EventData.ActionID = 1;

                #region Base Node

                EventData.ExistingDevice = InstallNode.originalDeviceId;
                if (InstallNode.moveOrder == o.MoveOrder.Y)
                {
                    EventData.isMove = true;
                    if (CleanNull(InstallNode.originalDeviceId) == string.Empty)
                        errors.Add("Move flag is populated but original device information is not populated.");
                }
                switch (InstallNode.orderType)
                {
                    case o.OrderType.NewOrder:
                        EventData.OrderSubType = "New Order";
                        break;

                    case o.OrderType.ReOrder:
                        EventData.OrderSubType = "Reorder";
                        break;

                    case o.OrderType.ReplacementOrder:
                        EventData.OrderSubType = "Replacement Order";
                        break;

                    case o.OrderType.RefreshOrder:
                        EventData.OrderSubType = "Refresh Order";
                        break;
                }
                if (InstallNode.migrationReasonSpecified)
                {
                    switch (InstallNode.migrationReason)
                    {
                        case o.MigrationReason.MigratingParallelBuild:
                            EventData.FRBInstallType = "Migrating Parallel Build";
                            break;

                        case o.MigrationReason.MigratingReplacementDevice:
                            EventData.FRBInstallType = "Migrating Replacement Device";
                            break;

                        case o.MigrationReason.NewDevice:
                            EventData.FRBInstallType = "New Device";
                            break;
                    }
                }
                if (InstallNode.replacementReasonSpecified)
                {
                    switch (InstallNode.replacementReason)
                    {
                        case o.ReplacementReason.CustomerCausedConfigProblem:
                            EventData.ReplacementReason = "Customer Config Issue";
                            break;

                        case o.ReplacementReason.DeviceLostInTransit:
                            EventData.ReplacementReason = "Device Lost In Transit";
                            break;

                        case o.ReplacementReason.HardwareFailure:
                            EventData.ReplacementReason = "Hardware Issue";
                            break;

                        case o.ReplacementReason.SprintCausedConfigProblem:
                            EventData.ReplacementReason = "Sprint Issue";
                            break;

                        case o.ReplacementReason.HWUpgrade:
                            EventData.ReplacementReason = "Hardware Upgrade";
                            break;

                        case o.ReplacementReason.FacilityMove:
                            EventData.ReplacementReason = "Facility Move";
                            break;
                    }
                }
                EventData.isExpedite = InstallNode.expeditedShipping;

                #endregion Base Node

                #region Contacts and Start/End Times

                if (InstallNode.installationInformation != null)
                {
                    if (CleanNull(InstallNode.installationInformation.installationSlotDateTimeStart) != "")
                    {
                        DateTimeOffset dt;
                        DateTimeOffset.TryParse(InstallNode.installationInformation.installationSlotDateTimeStart, out dt);
                        if (dt != new DateTimeOffset())
                            EventData.StartTime = dt.LocalDateTime;
                        else
                            errors.Add("Invalid Start Time, unable to convert to DateTime.");
                    }
                    if (CleanNull(InstallNode.installationInformation.installationSlotDateTimeEnd) != "")
                    {
                        DateTimeOffset dt;
                        DateTimeOffset.TryParse(InstallNode.installationInformation.installationSlotDateTimeEnd, out dt);
                        if (dt != new DateTimeOffset())
                            EventData.EndTime = dt.LocalDateTime;
                        else
                            errors.Add("Invalid End Time, unable to convert to DateTime.");
                    }

                    if (InstallNode.installationInformation.installationContactPerson != null)
                    {
                        TadpoleContact tcPrimaryInstall = new TadpoleContact();
                        tcPrimaryInstall.Name = CleanNull(InstallNode.installationInformation.installationContactPerson.name);
                        if ((InstallNode.installationInformation.installationContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                            tcPrimaryInstall.PhoneNumber = CleanNull(InstallNode.installationInformation.installationContactPerson.phoneNumberDomestic);
                        else
                            tcPrimaryInstall.PhoneNumber = CleanNull(InstallNode.installationInformation.installationContactPerson.phoneNumberInternational);
                        tcPrimaryInstall.PhoneExtension = CleanNull(InstallNode.installationInformation.installationContactPerson.phoneNumberExtension);
                        tcPrimaryInstall.EmailAddress = CleanNull(InstallNode.installationInformation.installationContactPerson.emailAddress);
                        tcPrimaryInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                        tcPrimaryInstall.ContactTypeID = 1;   //1 for Primary, 2 for secondary
                        EventData.SiteContacts.Add(tcPrimaryInstall);
                    }

                    if (InstallNode.installationInformation.installationBackupContactPerson != null)
                    {
                        TadpoleContact tcBackupInstall = new TadpoleContact();
                        tcBackupInstall.Name = CleanNull(InstallNode.installationInformation.installationBackupContactPerson.name);
                        if ((InstallNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                            tcBackupInstall.PhoneNumber = CleanNull(InstallNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic);
                        else
                            tcBackupInstall.PhoneNumber = CleanNull(InstallNode.installationInformation.installationBackupContactPerson.phoneNumberInternational);
                        tcBackupInstall.PhoneExtension = CleanNull(InstallNode.installationInformation.installationBackupContactPerson.phoneNumberExtension);
                        tcBackupInstall.EmailAddress = CleanNull(InstallNode.installationInformation.installationBackupContactPerson.emailAddress);
                        tcBackupInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                        tcBackupInstall.ContactTypeID = 2;   //1 for Primary, 2 for secondary
                        EventData.SiteContacts.Add(tcBackupInstall);
                    }
                }

                #endregion Contacts and Start/End Times

                #region Org ID/Name

                if (InstallNode.organizationInformation != null)
                {
                    EventData.OrganizationName = CleanNull(InstallNode.organizationInformation.organizationName);
                    EventData.OrganizationID = CleanNull(InstallNode.organizationInformation.organizationID);
                }

                #endregion Org ID/Name

                #region Shipping Contact/Address

                if (InstallNode.shippingContactAddressInformation != null)
                {
                    if (InstallNode.shippingContactAddressInformation.shippingContactPerson != null)
                    {
                        TadpoleContact tcShippingInstall = new TadpoleContact();
                        tcShippingInstall.Name = CleanNull(InstallNode.shippingContactAddressInformation.shippingContactPerson.name);
                        if ((InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                            tcShippingInstall.PhoneNumber = CleanNull(InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberDomestic);
                        else
                            tcShippingInstall.PhoneNumber = CleanNull(InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberInternational);
                        tcShippingInstall.PhoneExtension = CleanNull(InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberExtension);
                        tcShippingInstall.EmailAddress = CleanNull(InstallNode.shippingContactAddressInformation.shippingContactPerson.emailAddress);
                        tcShippingInstall.RoleID = 74;  //1 for Installation, 74 for Shipping
                        tcShippingInstall.ContactTypeID = 1;   //1 for Primary, 2 for secondary
                        EventData.ShippingContacts.Add(tcShippingInstall);
                    }

                    if (InstallNode.shippingContactAddressInformation.shippingAddress != null)
                    {
                        TadpoleAddress taSite = new TadpoleAddress();
                        taSite.AddressTypeID = 19;  //18 for Site, 19 for Shipping
                        taSite.StreetAddress1 = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.streetAddress1);
                        taSite.StreetAddress2 = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.streetAddress2);
                        taSite.City = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.city);
                        taSite.State = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.state);
                        if ((InstallNode.shippingContactAddressInformation.shippingAddress.ZIP ?? "") != string.Empty)
                            taSite.ZipPostalCode = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.ZIP);
                        else
                            taSite.ZipPostalCode = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.intlPostalCode);
                        taSite.Province = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.province);
                        taSite.Country = CleanNull(InstallNode.shippingContactAddressInformation.shippingAddress.country);
                        EventData.Addresses.Add(taSite);
                    }
                }

                #endregion Shipping Contact/Address

                #region Site Address, Title (dependant on address)

                if (InstallNode.siteInformation != null && InstallNode.siteInformation.siteAddress != null)
                {
                    TadpoleAddress taSite = new TadpoleAddress();
                    taSite.AddressTypeID = 18;  //18 for Site, 19 for Shipping
                    taSite.StreetAddress1 = CleanNull(InstallNode.siteInformation.siteAddress.streetAddress1);
                    taSite.StreetAddress2 = CleanNull(InstallNode.siteInformation.siteAddress.streetAddress2);
                    taSite.City = CleanNull(InstallNode.siteInformation.siteAddress.city);
                    taSite.State = CleanNull(InstallNode.siteInformation.siteAddress.state);
                    if ((InstallNode.siteInformation.siteAddress.ZIP ?? "") != string.Empty)
                        taSite.ZipPostalCode = CleanNull(InstallNode.siteInformation.siteAddress.ZIP);
                    else
                        taSite.ZipPostalCode = CleanNull(InstallNode.siteInformation.siteAddress.intlPostalCode);
                    taSite.Province = CleanNull(InstallNode.siteInformation.siteAddress.province);
                    taSite.Country = CleanNull(InstallNode.siteInformation.siteAddress.country);
                    EventData.Addresses.Add(taSite);
                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                               EventData.OrganizationName ?? "", taSite.City, taSite.State, taSite.Country, EventData.StartTime);
                }
                else
                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                               EventData.OrganizationName ?? "", "NA", "NA", "NA", EventData.StartTime);

                #endregion Site Address, Title (dependant on address)

                #region Device

                if (InstallNode.deviceInformation != null)
                {
                    EventData.DeviceID = CleanNull(InstallNode.deviceInformation.deviceID);
                    EventData.DeviceSerialNumber = CleanNull(InstallNode.deviceInformation.deviceSerialNumber);
                    EventData.DeviceModel = CleanNull(InstallNode.deviceInformation.model);
                    EventData.DeviceVendor = CleanNull(InstallNode.deviceInformation.vendor);
                }

                #endregion Device

                #region OriginalDevice

                if (InstallNode.originalDeviceInformation != null)
                {
                    EventData.OrigDeviceID = CleanNull(InstallNode.originalDeviceInformation.deviceID);
                    EventData.OrigDeviceSerialNumber = CleanNull(InstallNode.originalDeviceInformation.deviceSerialNumber);
                    EventData.OrigDeviceModel = CleanNull(InstallNode.originalDeviceInformation.model);
                    EventData.OrigFRBRequestID = CleanNull(InstallNode.originalDeviceInformation.originalRequestID);
                }

                #endregion OriginalDevice

                #region Configuration Information

                if (InstallNode.configurationInformation != null)
                {
                    TadpoleConfig tcExt = new TadpoleConfig();
                    tcExt.PortName = "WAN1";
                    if (InstallNode.configurationInformation.externalNetwork != null)
                    {
                        tcExt.IPAddress = InstallNode.configurationInformation.externalNetwork.wanIPAddress;
                        tcExt.SubnetMask = InstallNode.configurationInformation.externalNetwork.wanIPSubnetMask;
                        tcExt.DefaultGateway = InstallNode.configurationInformation.externalNetwork.wanDefaultGateway;
                        tcExt.SPEED = GetPortSpeed(InstallNode.configurationInformation.externalNetwork.wanSpeed);
                        tcExt.Duplex = InstallNode.configurationInformation.externalNetwork.wanDuplex.ToString();
                        tcExt.MACAddress = InstallNode.configurationInformation.externalNetwork.macAddressForDefaultGateway;
                    }
                    EventData.Configs.Add(tcExt);

                    if (InstallNode.configurationInformation.Item != null)
                    {
                        if (InstallNode.configurationInformation.typeGroup.configurationType == o.TypeGroupTypeConfigurationType.Item1)
                        {
                            if (InstallNode.configurationInformation.Item != null && InstallNode.configurationInformation.Item.GetType() != typeof(o.InternalNetworkInfoType1))
                            {
                                errors.Add("Type 1 config code received, but configuration provided is not type 1.");
                            }
                            else
                            {
                                EventData.DesignType = "1";
                                o.InternalNetworkInfoType1 ConfigNode = (o.InternalNetworkInfoType1)InstallNode.configurationInformation.Item;
                                TadpoleConfig tcInt = new TadpoleConfig();
                                tcInt.PortName = "PORT1";
                                tcInt.IPAddress = ConfigNode.lanIpAddress;
                                tcInt.SubnetMask = ConfigNode.lanSubnetMask;
                                EventData.Configs.Add(tcInt);

                                if (ConfigNode.subscriberPCList != null)
                                {
                                    ConfigNode.subscriberPCList.OrderBy(a => a.ipAddress);
                                    List<o.InternalPortNumberType> usedPortNumbers = new List<o.InternalPortNumberType>();

                                    foreach (o.SubscriberPCType1 pc in ConfigNode.subscriberPCList)
                                    {
                                        if (usedPortNumbers.Contains(pc.portNumber))
                                            errors.Add(string.Format("Duplicate Port Number ({0}) received for configuration type 1 subscriber PC list", pc.portNumber));
                                        usedPortNumbers.Add(pc.portNumber);

                                        TadpoleConfig tc = new TadpoleConfig();
                                        tc.PortName = pc.portNumber.ToString();
                                        tc.IPAddress = pc.ipAddress;
                                        tc.SPEED = GetPortSpeed(pc.internalPortSpeed);
                                        tc.Duplex = pc.internalDuplexSetting.ToString();
                                        EventData.Configs.Add(tc);
                                    }
                                }
                            }
                        }
                        else if (InstallNode.configurationInformation.typeGroup.configurationType == o.TypeGroupTypeConfigurationType.Item2)
                        {
                            if (InstallNode.configurationInformation.Item.GetType() != typeof(o.InternalNetworkInfoType2))
                            {
                                errors.Add("Type 2 config code received, but configuration provided is not type 2");
                            }
                            else
                            {
                                EventData.DesignType = "2";
                                o.InternalNetworkInfoType2 ConfigNode = (o.InternalNetworkInfoType2)InstallNode.configurationInformation.Item;
                                if ((ConfigNode.subscriberNetworkList != null && ConfigNode.subscriberNetworkList.Count() > 0)
                                  || (ConfigNode.subscriberPCList != null && ConfigNode.subscriberPCList.Count() > 0))
                                {
                                    if (ConfigNode.subscriberNetworkList != null)
                                    {
                                        ConfigNode.subscriberNetworkList.OrderBy(a => a.ipAddress);

                                        for (int i = 0; i < ConfigNode.subscriberNetworkList.Count(); i++)
                                        {
                                            TadpoleConfig tc = new TadpoleConfig();
                                            tc.PortName = string.Format("NETWORK{0}", i + 1);
                                            tc.IPAddress = ConfigNode.subscriberNetworkList[i].ipAddress;
                                            tc.SubnetMask = ConfigNode.subscriberNetworkList[i].subnetMask;
                                            tc.DefaultGateway = ConfigNode.subscriberNetworkList[i].gateway;
                                            tc.MACAddress = ConfigNode.subscriberNetworkList[i].multicastMacAddress;
                                            EventData.Configs.Add(tc);
                                        }
                                    }

                                    if (ConfigNode.subscriberPCList != null)
                                    {
                                        ConfigNode.subscriberPCList.OrderBy(a => a.ipAddress);

                                        for (int i = 0; i < ConfigNode.subscriberPCList.Count(); i++)
                                        {
                                            TadpoleConfig tc = new TadpoleConfig();
                                            tc.PortName = string.Format("SUBSCRIBER{0}", i + 1);
                                            tc.IPAddress = ConfigNode.subscriberPCList[i].ipAddress;
                                            tc.DefaultGateway = ConfigNode.subscriberPCList[i].gateway;
                                            tc.MACAddress = ConfigNode.subscriberPCList[i].multicastMacAddress;
                                            EventData.Configs.Add(tc);
                                        }
                                    }
                                }
                                else
                                    errors.Add("No Subscriber Networks or Subscriber PC supplied for type 2 configuration.");
                            }
                        }
                        else if (InstallNode.configurationInformation.typeGroup.configurationType == o.TypeGroupTypeConfigurationType.Item3)
                        {
                            if (InstallNode.configurationInformation.Item.GetType() != typeof(o.InternalNetworkInfoType3))
                            {
                                errors.Add("Type 3 config code received, but configuration provided is not type 3");
                            }
                            else
                            {
                                EventData.DesignType = "3";
                                o.InternalNetworkInfoType3 ConfigNode = (o.InternalNetworkInfoType3)InstallNode.configurationInformation.Item;
                                TadpoleConfig tcInt = new TadpoleConfig();
                                tcInt.PortName = "PORT1";
                                tcInt.IPAddress = ConfigNode.lanIpAddress;
                                tcInt.SubnetMask = ConfigNode.lanSubnetMask;
                                tcInt.SPEED = GetPortSpeed(ConfigNode.lanPortSpeed);
                                tcInt.Duplex = ConfigNode.lanDuplex.ToString();
                                EventData.Configs.Add(tcInt);

                                if ((ConfigNode.subscriberNetworkList != null && ConfigNode.subscriberNetworkList.Count() > 0)
                                  || (ConfigNode.subscriberPCList != null && ConfigNode.subscriberPCList.Count() > 0))
                                {
                                    if (ConfigNode.subscriberNetworkList != null)
                                    {
                                        ConfigNode.subscriberNetworkList.OrderBy(a => a.ipAddress);

                                        for (int i = 0; i < ConfigNode.subscriberNetworkList.Count(); i++)
                                        {
                                            TadpoleConfig tc = new TadpoleConfig();
                                            tc.PortName = string.Format("NETWORK{0}", i + 1);
                                            tc.IPAddress = ConfigNode.subscriberNetworkList[i].ipAddress;
                                            tc.SubnetMask = ConfigNode.subscriberNetworkList[i].subnetMask;
                                            tc.DefaultGateway = ConfigNode.subscriberNetworkList[i].gateway;
                                            tc.MACAddress = ConfigNode.subscriberNetworkList[i].multicastMacAddress;
                                            EventData.Configs.Add(tc);
                                        }
                                    }

                                    if (ConfigNode.subscriberPCList != null)
                                    {
                                        ConfigNode.subscriberPCList.OrderBy(a => a.ipAddress);

                                        for (int i = 0; i < ConfigNode.subscriberPCList.Count(); i++)
                                        {
                                            TadpoleConfig tc = new TadpoleConfig();
                                            tc.PortName = string.Format("SUBSCRIBER{0}", i + 1);
                                            tc.IPAddress = ConfigNode.subscriberPCList[i].ipAddress;
                                            tc.DefaultGateway = ConfigNode.subscriberPCList[i].gateway;
                                            tc.MACAddress = ConfigNode.subscriberPCList[i].multicastMacAddress;
                                            EventData.Configs.Add(tc);
                                        }
                                    }
                                }
                                else
                                    errors.Add("No Subscriber Networks or Subscriber PC supplied for type 3 configuration.");
                            }
                        }
                    }
                }

                #endregion Configuration Information

                EventData.ChangeLog = "New Install Event request received";
            }
            else if (Order.requestInformation.requestType == o.RequestInformationRequestType.NewOrderInstall)
            {
                errors.Add("Request Type of New Install received, but NewOrderInstall tag is not populated.");
            }

            #endregion NewOrderInstall

            #region NewOrderModify

            if (Order.Item.GetType() == typeof(o.tadpoleToCowsNewOrderModify))
            {
                o.tadpoleToCowsNewOrderModify ModifyNode = (o.tadpoleToCowsNewOrderModify)Order.Item;

                if (Order.responseDescription != o.ResponseDescription.NewOrderModify)
                {
                    errors.Add("NewOrderModify tag is populated, but request description is not Modify Install.");
                }
                try
                {
                    EventData.ActionID = 20;    //20 = Event Data Updated.  Set to Reschedule below if the time changed.

                    #region Data from previous Sequence request....

                    oldEvent = teDB.GetPreviousRequest(EventData.FRBRequestID, EventData.SequenceNumber);
                    if (!installOrderTypes.Contains(oldEvent.OrderType))
                        errors.Add("Install Modify request received for a request which is not an install.");
                    EventData.Status = oldEvent.Status;
                    oldstatus = oldEvent.Status;
                    EventData.Addresses = oldEvent.Addresses;
                    EventData.ShippingContacts = oldEvent.ShippingContacts;
                    EventData.DesignType = oldEvent.DesignType;
                    EventData.DeviceID = oldEvent.DeviceID;
                    EventData.DeviceModel = oldEvent.DeviceModel;
                    EventData.DeviceSerialNumber = oldEvent.DeviceSerialNumber;
                    EventData.DeviceVendor = oldEvent.DeviceVendor;
                    EventData.EventID = oldEvent.EventID;
                    EventData.OrganizationName = oldEvent.OrganizationName;
                    EventData.OrganizationID = oldEvent.OrganizationID;
                    EventData.isMove = oldEvent.isMove;
                    EventData.isExpedite = oldEvent.isExpedite;
                    EventData.ExistingDevice = oldEvent.ExistingDevice;
                    EventData.OrderSubType = oldEvent.OrderSubType;
                    EventData.FRBInstallType = oldEvent.FRBInstallType;
                    EventData.ReplacementReason = oldEvent.ReplacementReason;
                    EventData.OrigDeviceID = oldEvent.OrigDeviceID;
                    EventData.OrigDeviceModel = oldEvent.OrigDeviceModel;
                    EventData.OrigDeviceSerialNumber = oldEvent.OrigDeviceSerialNumber;
                    EventData.OrigFRBRequestID = oldEvent.OrigFRBRequestID;

                    #endregion Data from previous Sequence request....

                    #region Updated Information

                    if (ModifyNode.installationInformation != null)
                    {
                        TadpoleContact tcPrimaryInstall = new TadpoleContact();
                        TadpoleContact tcBackupInstall = new TadpoleContact();

                        #region ModifyNode

                        switch (ModifyNode.change)
                        {
                            //New Time Slot only
                            case o.tadpoleToCowsNewOrderModifyChange.Item1:
                                if (CleanNull(ModifyNode.installationInformation.installationSlotDateTimeStart) != "")
                                {
                                    DateTimeOffset dt;
                                    DateTimeOffset.TryParse(ModifyNode.installationInformation.installationSlotDateTimeStart, out dt);
                                    if (dt != new DateTimeOffset())
                                        EventData.StartTime = dt.LocalDateTime;
                                    else
                                        errors.Add("Invalid Start Time, unable to convert to DateTime.");
                                }
                                if (CleanNull(ModifyNode.installationInformation.installationSlotDateTimeEnd) != "")
                                {
                                    DateTimeOffset dt;
                                    DateTimeOffset.TryParse(ModifyNode.installationInformation.installationSlotDateTimeEnd, out dt);
                                    if (dt != new DateTimeOffset())
                                        EventData.EndTime = dt.LocalDateTime;
                                    else
                                        errors.Add("Invalid End Time, unable to convert to DateTime.");
                                }
                                TadpoleAddress taSite1 = oldEvent.Addresses.Where(a => a.AddressTypeID == 18).FirstOrDefault();
                                if (taSite1 != null)
                                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                                        EventData.OrganizationName ?? "", taSite1.City, taSite1.State, taSite1.Country, EventData.StartTime);
                                else
                                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                                        EventData.OrganizationName ?? "", "NA", "NA", "NA", EventData.StartTime);
                                EventData.SiteContacts = oldEvent.SiteContacts;
                                break;

                            //New Contacts Only
                            case o.tadpoleToCowsNewOrderModifyChange.Item2:
                                EventData.StartTime = oldEvent.StartTime;
                                EventData.EndTime = oldEvent.EndTime;
                                EventData.Title = oldEvent.Title;

                                #region Get New Contacts

                                if (ModifyNode.installationInformation.installationContactPerson != null)
                                {
                                    tcPrimaryInstall.Name = CleanNull(ModifyNode.installationInformation.installationContactPerson.name);
                                    if ((ModifyNode.installationInformation.installationContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                                        tcPrimaryInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberDomestic);
                                    else
                                        tcPrimaryInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberInternational);
                                    tcPrimaryInstall.PhoneExtension = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberExtension);
                                    tcPrimaryInstall.EmailAddress = CleanNull(ModifyNode.installationInformation.installationContactPerson.emailAddress);
                                }
                                else
                                {
                                    tcPrimaryInstall.Name = tcPrimaryInstall.PhoneNumber = tcPrimaryInstall.EmailAddress = "";
                                }
                                tcPrimaryInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                                tcPrimaryInstall.ContactTypeID = 1;   //1 for Primary, 2 for secondary
                                EventData.SiteContacts.Add(tcPrimaryInstall);

                                if (ModifyNode.installationInformation.installationBackupContactPerson != null)
                                {
                                    tcBackupInstall.Name = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.name);
                                    if ((ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                                        tcBackupInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic);
                                    else
                                        tcBackupInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberInternational);
                                    tcBackupInstall.PhoneExtension = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberExtension);
                                    tcBackupInstall.EmailAddress = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.emailAddress);
                                }
                                else
                                {
                                    tcBackupInstall.Name = tcBackupInstall.PhoneNumber = tcBackupInstall.EmailAddress = "";
                                }
                                tcBackupInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                                tcBackupInstall.ContactTypeID = 2;   //1 for Primary, 2 for secondary
                                EventData.SiteContacts.Add(tcBackupInstall);

                                #endregion Get New Contacts

                                break;

                            //New Time Slot and new Contacts
                            case o.tadpoleToCowsNewOrderModifyChange.Item3:
                                if (CleanNull(ModifyNode.installationInformation.installationSlotDateTimeStart) != "")
                                {
                                    DateTimeOffset dt;
                                    DateTimeOffset.TryParse(ModifyNode.installationInformation.installationSlotDateTimeStart, out dt);
                                    if (dt != new DateTimeOffset())
                                        EventData.StartTime = dt.LocalDateTime;
                                    else
                                        errors.Add("Invalid Start Time, unable to convert to DateTime.");
                                }
                                if (CleanNull(ModifyNode.installationInformation.installationSlotDateTimeEnd) != "")
                                {
                                    DateTimeOffset dt;
                                    DateTimeOffset.TryParse(ModifyNode.installationInformation.installationSlotDateTimeEnd, out dt);
                                    if (dt != new DateTimeOffset())
                                        EventData.EndTime = dt.LocalDateTime;
                                    else
                                        errors.Add("Invalid End Time, unable to convert to DateTime.");
                                }
                                TadpoleAddress taSite3 = oldEvent.Addresses.Where(a => a.AddressTypeID == 18).FirstOrDefault();
                                if (taSite3 != null)
                                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                                        EventData.OrganizationName ?? "", taSite3.City, taSite3.State, taSite3.Country, EventData.StartTime);
                                else
                                    EventData.Title = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                                        EventData.OrganizationName ?? "", "NA", "NA", "NA", EventData.StartTime);

                                #region Get New Contacts

                                if (ModifyNode.installationInformation.installationContactPerson != null)
                                {
                                    tcPrimaryInstall.Name = CleanNull(ModifyNode.installationInformation.installationContactPerson.name);
                                    if ((ModifyNode.installationInformation.installationContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                                        tcPrimaryInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberDomestic);
                                    else
                                        tcPrimaryInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberInternational);
                                    tcPrimaryInstall.PhoneExtension = CleanNull(ModifyNode.installationInformation.installationContactPerson.phoneNumberExtension);
                                    tcPrimaryInstall.EmailAddress = CleanNull(ModifyNode.installationInformation.installationContactPerson.emailAddress);
                                }
                                else
                                {
                                    tcPrimaryInstall.Name = tcPrimaryInstall.PhoneNumber = tcPrimaryInstall.EmailAddress = "";
                                }
                                tcPrimaryInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                                tcPrimaryInstall.ContactTypeID = 1;   //1 for Primary, 2 for secondary
                                EventData.SiteContacts.Add(tcPrimaryInstall);

                                if (ModifyNode.installationInformation.installationBackupContactPerson != null)
                                {
                                    tcBackupInstall.Name = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.name);
                                    if ((ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic ?? "") != string.Empty)
                                        tcBackupInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberDomestic);
                                    else
                                        tcBackupInstall.PhoneNumber = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberInternational);
                                    tcBackupInstall.PhoneExtension = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberExtension);
                                    tcBackupInstall.EmailAddress = CleanNull(ModifyNode.installationInformation.installationBackupContactPerson.emailAddress);
                                }
                                else
                                {
                                    tcBackupInstall.Name = tcBackupInstall.PhoneNumber = tcBackupInstall.EmailAddress = "";
                                }
                                tcBackupInstall.RoleID = 1;  //1 for Installation, 74 for Shipping
                                tcBackupInstall.ContactTypeID = 2;   //1 for Primary, 2 for secondary
                                EventData.SiteContacts.Add(tcBackupInstall);

                                #endregion Get New Contacts

                                break;

                            //Serial number only
                            case o.tadpoleToCowsNewOrderModifyChange.Item4:
                                if (CleanNull(ModifyNode.deviceSerialNumber) == string.Empty)
                                    errors.Add("Device Serial Number not included for a modify install change type 4 request.");
                                else
                                {
                                    EventData.StartTime = oldEvent.StartTime;
                                    EventData.EndTime = oldEvent.EndTime;
                                    EventData.Title = oldEvent.Title;
                                    EventData.SiteContacts = oldEvent.SiteContacts;
                                    EventData.DeviceSerialNumber = ModifyNode.deviceSerialNumber;

                                    //SLA tracking updates - Seperate Internal modifies vs. external modifies.
                                    //Only update the date sent on external modifies.
                                    EventData.OrderType = ActionType.NIM;
                                }
                                break;
                        }

                        #endregion ModifyNode

                        //Chop seconds off here as well so we don't incorrectly find a change.
                        EventData.StartTime = ChopTimeSlot(EventData.StartTime);
                        EventData.EndTime = ChopTimeSlot(EventData.EndTime);

                        #region Change Log

                        EventData.ChangeLog = "Modify Event request received<br>";
                        if (EventData.StartTime != oldEvent.StartTime)
                        {
                            EventData.ActionID = 15;
                            EventData.OrderType = ActionType.NDM;
                            EventData.ChangeLog += string.Format("Event Start Time changed from {0:MMM/dd/yy hh:mm tt} to {1:MMM/dd/yy hh:mm tt}<br>", oldEvent.StartTime, EventData.StartTime);
                        }

                        if (EventData.EndTime != oldEvent.EndTime)
                        {
                            EventData.ActionID = 15;
                            EventData.OrderType = ActionType.NDM;
                            EventData.ChangeLog += string.Format("Event End Time changed from {0:MMM/dd/yy hh:mm tt} to {1:MMM/dd/yy hh:mm tt}<br>", oldEvent.EndTime, EventData.EndTime);
                        }

                        if (ModifyNode.change == o.tadpoleToCowsNewOrderModifyChange.Item2 || ModifyNode.change == o.tadpoleToCowsNewOrderModifyChange.Item3)
                        {
                            TadpoleContact tcOld = oldEvent.SiteContacts.Where(a => a.ContactTypeID == 1).SingleOrDefault();
                            if (tcOld.Name != tcPrimaryInstall.Name || tcOld.PhoneNumber != tcPrimaryInstall.PhoneNumber || tcOld.EmailAddress != tcPrimaryInstall.EmailAddress || tcOld.PhoneExtension != tcPrimaryInstall.PhoneExtension)
                                EventData.ChangeLog += string.Format("Primary contact information has changed<br>");
                            tcOld = oldEvent.SiteContacts.Where(a => a.ContactTypeID == 2).SingleOrDefault();
                            if (tcOld.Name != tcBackupInstall.Name || tcOld.PhoneNumber != tcBackupInstall.PhoneNumber || tcOld.EmailAddress != tcBackupInstall.EmailAddress || tcOld.PhoneExtension != tcBackupInstall.PhoneExtension)
                                EventData.ChangeLog += string.Format("Backup contact information has changed<br>");
                        }
                        if (EventData.DeviceSerialNumber != oldEvent.DeviceSerialNumber)
                            EventData.ChangeLog += string.Format("Device Serial Number changed from {0} to {1}<br>", oldEvent.DeviceSerialNumber, EventData.DeviceSerialNumber);
                        if (EventData.OrderType == ActionType.NDM && !EventData.FRBSentDate.HasValue)
                        {
                            EventData.ChangeLog += "FRB Order Submission Date not received from Tadpole, using the current time for the Last Update to Requested Activity Date & Time time stamp<br>";
                            EventData.FRBSentDate = DateTime.Now;
                        }

                        #endregion Change Log
                    }

                    #endregion Updated Information
                }
                catch (OutOfSequence oos)
                {
                    errors.Add(oos.SequenceError);
                }
            }
            else if (Order.requestInformation.requestType == o.RequestInformationRequestType.NewOrderModify)
            {
                errors.Add("Request Type of Modify Install received, but NewOrderModify tag is not populated.");
            }

            #endregion NewOrderModify

            #region NewOrderCancel

            if (Order.Item.GetType() == typeof(o.tadpoleToCowsNewOrderCancel))
            {
                o.tadpoleToCowsNewOrderCancel CancelNode = (o.tadpoleToCowsNewOrderCancel)Order.Item;

                if (Order.responseDescription != o.ResponseDescription.NewOrderCancel)
                {
                    errors.Add("NewOrderCancel tag is populated, but request description is not Cancel Install.");
                }

                try
                {
                    EventData.ActionID = 26;    //26	Cancel Recieved

                    //Data from previous Sequence request....
                    oldEvent = teDB.GetPreviousRequest(EventData.FRBRequestID, EventData.SequenceNumber);
                    if (!installOrderTypes.Contains(oldEvent.OrderType))
                        errors.Add("Install Cancel request received for a request which is not an install.");
                    EventData.Status = oldEvent.Status;
                    oldstatus = oldEvent.Status;
                    EventData.Addresses = oldEvent.Addresses;
                    EventData.SiteContacts = oldEvent.SiteContacts;
                    EventData.ShippingContacts = oldEvent.ShippingContacts;
                    EventData.DesignType = oldEvent.DesignType;
                    EventData.DeviceID = oldEvent.DeviceID;
                    EventData.DeviceModel = oldEvent.DeviceModel;
                    EventData.DeviceSerialNumber = oldEvent.DeviceSerialNumber;
                    EventData.DeviceVendor = oldEvent.DeviceVendor;
                    EventData.EventID = oldEvent.EventID;
                    EventData.OrganizationName = oldEvent.OrganizationName;
                    EventData.OrganizationID = oldEvent.OrganizationID;
                    EventData.StartTime = oldEvent.StartTime;
                    EventData.EndTime = oldEvent.EndTime;
                    EventData.Title = oldEvent.Title;
                    EventData.isMove = oldEvent.isMove;
                    EventData.isExpedite = oldEvent.isExpedite;
                    EventData.ExistingDevice = oldEvent.ExistingDevice;
                    EventData.OrderSubType = oldEvent.OrderSubType;
                    EventData.FRBInstallType = oldEvent.FRBInstallType;
                    EventData.ReplacementReason = oldEvent.ReplacementReason;
                    EventData.OrigDeviceID = oldEvent.OrigDeviceID;
                    EventData.OrigDeviceModel = oldEvent.OrigDeviceModel;
                    EventData.OrigDeviceSerialNumber = oldEvent.OrigDeviceSerialNumber;
                    EventData.OrigFRBRequestID = oldEvent.OrigFRBRequestID;

                    if (CancelNode.internalCancel == o.tadpoleToCowsNewOrderCancelInternalCancel.Y)
                    {
                        EventData.OrderType = ActionType.NIC;
                        EventData.ChangeLog = "Internal cancel received from Tadpole, replacement request received.<br>";
                    }
                    else
                        EventData.ChangeLog = "Cancel request received from FRM<br>";
                }
                catch (OutOfSequence oos)
                {
                    errors.Add(oos.SequenceError);
                }
            }

            #endregion NewOrderCancel

            #region DisconnectService

            if (Order.Item.GetType() == typeof(o.tadpoleToCowsDisconnectService))
            {
                o.tadpoleToCowsDisconnectService disconnectNode = (o.tadpoleToCowsDisconnectService)Order.Item;

                if (Order.responseDescription != o.ResponseDescription.DisconnectService)
                {
                    errors.Add("Disconnect Service tag is populated, but request description is not New Disconnect.");
                }
                int seq = teDB.GetPreviousSequence(EventData.FRBRequestID);
                if (seq != 0)
                {
                    errors.Add("New Disconnect Service received, but there is already a pending order on version " + seq.ToString());
                }
                EventData.Status = 15;   //Disco Pending
                EventData.ActionID = 1;

                if (disconnectNode.disconnectInformation != null)
                {
                    if (CleanNull(disconnectNode.disconnectInformation.requestedDisconnectDate) != "")
                    {
                        DateTimeOffset dt;
                        DateTimeOffset.TryParse(disconnectNode.disconnectInformation.requestedDisconnectDate, out dt);
                        if (dt != new DateTimeOffset())
                            EventData.StartTime = dt.LocalDateTime;
                        else
                            errors.Add("Invalid Request Date, unable to convert to DateTime.");
                    }
                    EventData.DeviceID = disconnectNode.disconnectInformation.deviceID;
                    EventData.DeviceSerialNumber = disconnectNode.disconnectInformation.deviceSerialNumber;
                }
                if (disconnectNode.organizationInformation != null)
                {
                    EventData.OrganizationID = disconnectNode.organizationInformation.organizationID;
                    EventData.OrganizationName = disconnectNode.organizationInformation.organizationName;
                }
                if (disconnectNode.moveOrder == o.MoveOrder.Y)
                    EventData.isMove = true;

                EventData.Title = string.Format("Disconnect -- {0} -- {1:MMM/dd/yy}", EventData.OrganizationName ?? "", EventData.StartTime);
            }

            #endregion DisconnectService

            #region DisconnectServiceCancel

            if (Order.responseDescription == o.ResponseDescription.DisconnectServiceCancel)
            {
                try
                {
                    EventData.ActionID = 26;

                    //Data from previous Sequence request....
                    oldEvent = teDB.GetPreviousRequest(EventData.FRBRequestID, EventData.SequenceNumber);
                    if (!disconnectOrderTypes.Contains(oldEvent.OrderType))
                        errors.Add("Disconnect Cancel request received for a request which is not a disconnect.");
                    if (oldEvent.StartTime.HasValue && oldEvent.StartTime.Value.Date <= DateTime.Now.Date)
                        errors.Add("Disconnect Cancel request not allowed on or after the requested disconnect date.");
                    EventData.Status = oldEvent.Status;
                    oldstatus = oldEvent.Status;
                    EventData.Addresses = oldEvent.Addresses;
                    EventData.SiteContacts = oldEvent.SiteContacts;
                    EventData.ShippingContacts = oldEvent.ShippingContacts;
                    EventData.DesignType = oldEvent.DesignType;
                    EventData.DeviceID = oldEvent.DeviceID;
                    EventData.DeviceModel = oldEvent.DeviceModel;
                    EventData.DeviceSerialNumber = oldEvent.DeviceSerialNumber;
                    EventData.DeviceVendor = oldEvent.DeviceVendor;
                    EventData.EventID = oldEvent.EventID;
                    EventData.OrganizationName = oldEvent.OrganizationName;
                    EventData.OrganizationID = oldEvent.OrganizationID;
                    EventData.StartTime = oldEvent.StartTime;
                    EventData.EndTime = oldEvent.EndTime;
                    EventData.Title = oldEvent.Title;
                    EventData.isMove = oldEvent.isMove;
                    EventData.isExpedite = oldEvent.isExpedite;
                    EventData.ExistingDevice = oldEvent.ExistingDevice;
                    EventData.ChangeLog = "Cancel request received from FRM<br>";
                }
                catch (OutOfSequence oos)
                {
                    errors.Add(oos.SequenceError);
                }
            }

            #endregion DisconnectServiceCancel

            GetNewStatus(EventData); //Set the EventData.Status field according to Business Rules, after changelog is set up.

            #region Insert Event, Update Time Slots on successful databasing

            if (errors.Count == 0)
            {
                //Database rounds 1:59:30 up to 2:00.  This can cause a discrepency in the time slot values produced here and the date/times
                //that get databased.  So cut off the seconds value so it can't round up.

                #region chop off seconds

                EventData.StartTime = ChopTimeSlot(EventData.StartTime);
                EventData.EndTime = ChopTimeSlot(EventData.EndTime);
                if (oldEvent != null)
                {
                    oldEvent.StartTime = ChopTimeSlot(oldEvent.StartTime);
                    oldEvent.EndTime = ChopTimeSlot(oldEvent.EndTime);
                }

                #endregion chop off seconds

                if (teDB.InsertEvent(EventData, xmlString, toLog))
                {
                    //TimeSlots...
                    //if old status is 11,12,13, or 16 then the order was previously cancelled.  Don't decrease time slot if it was previously cancelled.
                    if (EventData.OrderType == ActionType.NDM)   //NDM is the only one with time slot updates
                    {
                        if (oldEvent != null && oldstatus != 11 && oldstatus != 12 && oldstatus != 13 && oldstatus != 16)
                            UpdateTimeSlots(oldEvent.EventID, ActionType.NIC, oldEvent.StartTime, oldEvent.EndTime);    //Pass NIC as the event type so we decrease the old time slot
                        UpdateTimeSlots(EventData.EventID, EventData.OrderType, EventData.StartTime, EventData.EndTime);
                        teDB.UpdateReqHist(EventData.EventID, oldEvent.StartTime, EventData.StartTime);
                    }
                    else if (EventData.OrderType == ActionType.NCC || EventData.OrderType == ActionType.NIC)
                    {
                        if (oldEvent != null && oldstatus != 11 && oldstatus != 12 && oldstatus != 13 && oldstatus != 16)
                            UpdateTimeSlots(EventData.EventID, EventData.OrderType, EventData.StartTime, EventData.EndTime);
                    }
                    else if (EventData.OrderType == ActionType.NCI)
                        UpdateTimeSlots(EventData.EventID, EventData.OrderType, EventData.StartTime, EventData.EndTime);
                }
            }

            #endregion Insert Event, Update Time Slots on successful databasing

            return errors;
        }

        public string GetGenericMessage(o.RequestInformationRequestType type, int FRMOrderID, int version, int DesignCode, bool isMove, bool isExp)
        {
            o.tadpoleToCows or = new o.tadpoleToCows();
            or.requestInformation = new o.RequestInformation();
            or.requestInformation.requestID = FRMOrderID;
            or.requestInformation.requestType = type;
            or.requestInformation.timestamp = string.Format("{0:o}", DateTime.Now.ToUniversalTime());
            or.requestVersion = version;
            or.requestReceiveDate = string.Format("{0:o}", DateTime.Now.ToUniversalTime());
            or.requestReceiveDateSpecified = true;

            #region NewOrderInstall

            if (type == o.RequestInformationRequestType.NewOrderInstall)
            {
                or.responseDescription = o.ResponseDescription.NewOrderInstall;
                o.tadpoleToCowsNewOrderInstall InstallNode = new o.tadpoleToCowsNewOrderInstall();
                InstallNode.deviceInformation = new o.DeviceInformation();
                InstallNode.deviceInformation.deviceID = "A";
                InstallNode.deviceInformation.deviceSerialNumber = "B";
                InstallNode.deviceInformation.model = "C";
                InstallNode.deviceInformation.vendor = "D";
                InstallNode.installationInformation = new o.InstallationInformation();
                InstallNode.installationInformation.installationContactPerson = new o.ContactPerson();
                InstallNode.installationInformation.installationContactPerson.emailAddress = "primary@contact.per";
                InstallNode.installationInformation.installationContactPerson.name = "Install Contact";
                InstallNode.installationInformation.installationContactPerson.phoneNumberDomestic = "1231231234";
                InstallNode.installationInformation.installationContactPerson.phoneNumberExtension = "ext 123";
                InstallNode.installationInformation.installationBackupContactPerson = new o.ContactPerson();
                InstallNode.installationInformation.installationBackupContactPerson.emailAddress = "backup@contact.per";
                InstallNode.installationInformation.installationBackupContactPerson.name = "Backup Contact";
                InstallNode.installationInformation.installationBackupContactPerson.phoneNumberInternational = "+1 2312341234";
                InstallNode.installationInformation.installationBackupContactPerson.phoneNumberExtension = "ext 456";
                InstallNode.installationInformation.installationSlotDateTimeEnd = string.Format("{0:o}", DateTime.Now.AddHours(2).ToUniversalTime());
                InstallNode.installationInformation.installationSlotDateTimeEndSpecified = true;
                InstallNode.installationInformation.installationSlotDateTimeStart = string.Format("{0:o}", DateTime.Now.ToUniversalTime());
                InstallNode.installationInformation.installationSlotDateTimeStartSpecified = true;
                InstallNode.organizationInformation = new o.OrganizationInformation();
                InstallNode.organizationInformation.organizationID = "000000100";
                InstallNode.organizationInformation.organizationName = "Org Name";
                InstallNode.siteInformation = new o.SiteInformation();
                InstallNode.siteInformation.siteAddress = new o.AddressInformation();
                InstallNode.siteInformation.siteAddress.city = "City";
                InstallNode.siteInformation.siteAddress.country = "US";
                InstallNode.siteInformation.siteAddress.state = "KS";
                InstallNode.siteInformation.siteAddress.streetAddress1 = "123 Main Address";
                InstallNode.siteInformation.siteAddress.streetAddress2 = "Apt Blahblah";
                InstallNode.siteInformation.siteAddress.ZIP = "12345";
                InstallNode.siteInformation.locationID = 1;
                InstallNode.siteInformation.locationName = "TestLoc";
                InstallNode.configurationInformation = new o.ConfigurationInformation();
                InstallNode.configurationInformation.typeGroup = new o.TypeGroupType();
                switch (DesignCode)
                {
                    case 1:
                        InstallNode.configurationInformation.typeGroup.configurationType = o.TypeGroupTypeConfigurationType.Item1;
                        InstallNode.configurationInformation.typeGroup.designTypeDescription = "Type Desc";
                        InstallNode.configurationInformation.externalNetwork = new o.ExternalNetworkType();
                        InstallNode.configurationInformation.externalNetwork.wanIPAddress = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanIPSubnetMask = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanSpeed = o.PortSpeedType.Item100;
                        InstallNode.configurationInformation.externalNetwork.wanDuplex = o.DuplexType.FULL;
                        InstallNode.configurationInformation.externalNetwork.wanDefaultGateway = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.macAddressForDefaultGateway = "01:23:45:67:89:AC";
                        o.InternalNetworkInfoType1 NetworkType1Node = new o.InternalNetworkInfoType1();
                        NetworkType1Node.lanIpAddress = "250.250.250.2";
                        NetworkType1Node.lanSubnetMask = "250.250.250.5";
                        List<o.SubscriberPCType1> myList = new List<o.SubscriberPCType1>();
                        o.SubscriberPCType1 s1 = new o.SubscriberPCType1();
                        s1.internalDuplexSetting = o.DuplexType.AUTO;
                        s1.ipAddress = "250.250.250.3";
                        s1.internalPortSpeed = o.PortSpeedType.AUTO;
                        s1.portNumber = o.InternalPortNumberType.Internal1;

                        o.SubscriberPCType1 s2 = new o.SubscriberPCType1();
                        s2.internalDuplexSetting = o.DuplexType.FULL;
                        s2.ipAddress = "250.250.250.4";
                        s2.internalPortSpeed = o.PortSpeedType.Item10;
                        s2.portNumber = o.InternalPortNumberType.Internal2;
                        myList.Add(s1);
                        myList.Add(s2);
                        NetworkType1Node.subscriberPCList = myList.ToArray();
                        InstallNode.configurationInformation.Item = NetworkType1Node;
                        break;

                    case 2:
                        InstallNode.configurationInformation.typeGroup.configurationType = o.TypeGroupTypeConfigurationType.Item2;
                        InstallNode.configurationInformation.typeGroup.designTypeDescription = "Type Desc";
                        InstallNode.configurationInformation.externalNetwork = new o.ExternalNetworkType();
                        InstallNode.configurationInformation.externalNetwork.wanIPAddress = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanIPSubnetMask = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanSpeed = o.PortSpeedType.Item100;
                        InstallNode.configurationInformation.externalNetwork.wanDuplex = o.DuplexType.FULL;
                        InstallNode.configurationInformation.externalNetwork.wanDefaultGateway = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.macAddressForDefaultGateway = "01:23:45:67:89:AC";
                        o.InternalNetworkInfoType2 NetworkType2Node = new o.InternalNetworkInfoType2();
                        List<o.SubscriberPCType2> myList2 = new List<o.SubscriberPCType2>();
                        o.SubscriberPCType2 s1b = new o.SubscriberPCType2();
                        s1b.gateway = "250.250.250.10";
                        s1b.ipAddress = "250.250.250.3";
                        s1b.multicastMacAddress = "01:23:45:67:89:AC";

                        o.SubscriberPCType2 s2b = new o.SubscriberPCType2();

                        //s2b.gateway = "250.250.250.11";
                        s2b.ipAddress = "250.250.250.4";
                        s2b.multicastMacAddress = "01:23:45:67:89:AC";
                        myList2.Add(s1b);
                        myList2.Add(s2b);
                        NetworkType2Node.subscriberPCList = myList2.ToArray();

                        List<o.SubscriberNetworkType> myNetlist = new List<o.SubscriberNetworkType>();
                        o.SubscriberNetworkType sn = new o.SubscriberNetworkType();
                        sn.ipAddress = "250.250.250.5";
                        sn.multicastMacAddress = "01:23:45:67:89:AC";
                        sn.gateway = "250.250.250.12";
                        sn.subnetMask = "250.250.250.13";
                        o.SubscriberNetworkType sn2 = new o.SubscriberNetworkType();
                        sn2.ipAddress = "250.250.250.6";
                        sn2.subnetMask = "250.250.250.14";
                        sn2.multicastMacAddress = "01:23:45:67:89:AC";

                        //sn2.gateway = "250.250.250.15";
                        myNetlist.Add(sn);
                        myNetlist.Add(sn2);
                        NetworkType2Node.subscriberNetworkList = myNetlist.ToArray();
                        InstallNode.configurationInformation.Item = NetworkType2Node;
                        break;

                    case 3:
                        InstallNode.configurationInformation.typeGroup.configurationType = o.TypeGroupTypeConfigurationType.Item3;
                        InstallNode.configurationInformation.typeGroup.designTypeDescription = "Type Desc";
                        InstallNode.configurationInformation.externalNetwork = new o.ExternalNetworkType();
                        InstallNode.configurationInformation.externalNetwork.wanIPAddress = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanIPSubnetMask = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.wanSpeed = o.PortSpeedType.Item1000;
                        InstallNode.configurationInformation.externalNetwork.wanDuplex = o.DuplexType.FULL;
                        InstallNode.configurationInformation.externalNetwork.wanDefaultGateway = "250.250.250.1";
                        InstallNode.configurationInformation.externalNetwork.macAddressForDefaultGateway = "01:23:45:67:89:AC";
                        o.InternalNetworkInfoType3 NetworkType3Node = new o.InternalNetworkInfoType3();
                        NetworkType3Node.lanDuplex = o.DuplexType.AUTO;
                        NetworkType3Node.lanIpAddress = "250.250.250.2";
                        NetworkType3Node.lanPortSpeed = o.PortSpeedType.AUTO;
                        NetworkType3Node.lanSubnetMask = "250.250.250.3";
                        List<o.SubscriberPCType2> myList3 = new List<o.SubscriberPCType2>();
                        o.SubscriberPCType2 s1c = new o.SubscriberPCType2();
                        s1c.gateway = "250.250.250.10";
                        s1c.ipAddress = "250.250.250.7";
                        s1c.multicastMacAddress = "01:23:45:67:89:AC";

                        o.SubscriberPCType2 s2c = new o.SubscriberPCType2();

                        //s2c.gateway = "250.250.250.11";
                        s2c.ipAddress = "250.250.250.4";
                        s2c.multicastMacAddress = "01:23:45:67:89:AC";
                        myList3.Add(s1c);
                        myList3.Add(s2c);
                        NetworkType3Node.subscriberPCList = myList3.ToArray();

                        List<o.SubscriberNetworkType> myNetlistc = new List<o.SubscriberNetworkType>();
                        o.SubscriberNetworkType snc = new o.SubscriberNetworkType();
                        snc.ipAddress = "250.250.250.5";
                        snc.multicastMacAddress = "01:23:45:67:89:AC";

                        //snc.gateway = "250.250.250.12";
                        snc.subnetMask = "250.250.250.13";
                        o.SubscriberNetworkType sn2c = new o.SubscriberNetworkType();
                        sn2c.ipAddress = "250.250.250.6";
                        sn2c.multicastMacAddress = "01:23:45:67:89:AC";
                        sn2c.subnetMask = "250.250.250.15";
                        sn2c.gateway = "250.250.250.14";
                        myNetlistc.Add(snc);
                        myNetlistc.Add(sn2c);
                        NetworkType3Node.subscriberNetworkList = myNetlistc.ToArray();
                        InstallNode.configurationInformation.Item = NetworkType3Node;
                        break;
                }
                InstallNode.shippingContactAddressInformation = new o.ShippingContactAddressInformation();
                InstallNode.shippingContactAddressInformation.shippingAddress = new o.AddressInformation();
                InstallNode.shippingContactAddressInformation.shippingAddress.streetAddress1 = "619 Cliff Ave";
                InstallNode.shippingContactAddressInformation.shippingAddress.city = "Enderby";
                InstallNode.shippingContactAddressInformation.shippingAddress.province = "British Columbia";
                InstallNode.shippingContactAddressInformation.shippingAddress.country = "CA";
                InstallNode.shippingContactAddressInformation.shippingAddress.intlPostalCode = "1V0";
                InstallNode.shippingContactAddressInformation.shippingContactPerson = new o.ContactPerson();
                InstallNode.shippingContactAddressInformation.shippingContactPerson.name = "Shippping";
                InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberDomestic = "1231231234";
                InstallNode.shippingContactAddressInformation.shippingContactPerson.phoneNumberExtension = "ext 789";
                InstallNode.shippingContactAddressInformation.shippingContactPerson.emailAddress = "shipping.person@shipping.com";
                if (isMove)
                {
                    InstallNode.moveOrder = o.MoveOrder.Y;
                    InstallNode.originalDeviceId = "originaldevid";
                }
                else
                    InstallNode.moveOrder = o.MoveOrder.N;
                if (isExp)
                    InstallNode.expeditedShipping = true;

                InstallNode.orderType = o.OrderType.ReOrder;
                InstallNode.migrationReasonSpecified = true;
                InstallNode.migrationReason = o.MigrationReason.MigratingParallelBuild;
                InstallNode.replacementReasonSpecified = true;
                InstallNode.replacementReason = o.ReplacementReason.CustomerCausedConfigProblem;

                or.Item = InstallNode;
            }

            #endregion NewOrderInstall

            #region NerOrderModify

            else if (type == o.RequestInformationRequestType.NewOrderModify)
            {
                or.responseDescription = o.ResponseDescription.NewOrderModify;
                o.tadpoleToCowsNewOrderModify ModifyNode = new o.tadpoleToCowsNewOrderModify();
                ModifyNode.change = o.tadpoleToCowsNewOrderModifyChange.Item3;
                ModifyNode.installationInformation = new o.InstallationInformation();

                //ModifyNode.deviceSerialNumber = "newserial";
                ModifyNode.installationInformation.installationContactPerson = new o.ContactPerson();
                ModifyNode.installationInformation.installationContactPerson.emailAddress = "new.primary@contact.per";
                ModifyNode.installationInformation.installationContactPerson.name = "Install ChangeContact";
                ModifyNode.installationInformation.installationContactPerson.phoneNumberDomestic = "3213213214";
                ModifyNode.installationInformation.installationContactPerson.phoneNumberExtension = "ext 123";
                ModifyNode.installationInformation.installationBackupContactPerson = new o.ContactPerson();
                ModifyNode.installationInformation.installationBackupContactPerson.emailAddress = "new.backup@contact.per";
                ModifyNode.installationInformation.installationBackupContactPerson.name = "Backup ChangeContact";
                ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberInternational = "+2 3213213214";
                ModifyNode.installationInformation.installationBackupContactPerson.phoneNumberExtension = "ext 789";
                ModifyNode.installationInformation.installationSlotDateTimeEnd = string.Format("{0:o}", DateTime.Now.AddHours(2).ToUniversalTime());
                ModifyNode.installationInformation.installationSlotDateTimeEndSpecified = true;
                ModifyNode.installationInformation.installationSlotDateTimeStart = string.Format("{0:o}", DateTime.Now.ToUniversalTime());
                ModifyNode.installationInformation.installationSlotDateTimeStartSpecified = true;
                or.Item = ModifyNode;
            }

            #endregion NerOrderModify

            #region NewOrderCancel

            else if (type == o.RequestInformationRequestType.NewOrderCancel)
            {
                or.responseDescription = o.ResponseDescription.NewOrderCancel;
                or.Item = new o.tadpoleToCowsNewOrderCancel();
            }

            #endregion NewOrderCancel

            #region Disconnect

            else if (type == o.RequestInformationRequestType.DisconnectService)
            {
                or.responseDescription = o.ResponseDescription.DisconnectService;
                o.tadpoleToCowsDisconnectService DisconnectNode = new o.tadpoleToCowsDisconnectService();
                DisconnectNode.organizationInformation = new o.OrganizationInformation();
                DisconnectNode.organizationInformation.organizationID = "0000012345";
                DisconnectNode.organizationInformation.organizationName = "Disconnect Org Name";
                DisconnectNode.disconnectInformation = new o.DisconnectInformation();
                DisconnectNode.disconnectInformation.deviceID = "DeviceID";
                DisconnectNode.disconnectInformation.deviceSerialNumber = "Serial Number";
                DisconnectNode.disconnectInformation.requestedDisconnectDate = String.Format("{0:o}", DateTime.Now.Date.ToUniversalTime().AddDays(7));
                if (isMove)
                    DisconnectNode.moveOrder = o.MoveOrder.Y;
                else
                    DisconnectNode.moveOrder = o.MoveOrder.N;
                or.Item = DisconnectNode;
            }

            #endregion Disconnect

            #region DisconnectModify

            //Removed from xsd
            //else if (type == o.RequestInformationRequestType.DisconnectServiceModify)
            //{
            //    or.responseDescription = o.ResponseDescription.DisconnectServiceModify;
            //    o.DisconnectInformation DisconnectModNode = new o.DisconnectInformation();
            //    DisconnectModNode.deviceID = "cantchange";
            //    DisconnectModNode.deviceSerialNumber = "Serial Number (not allowed to change)";
            //    DisconnectModNode.requestedDisconnectDate = DateTime.Now.Date.AddDays(14);
            //    or.Item = DisconnectModNode;
            //}

            #endregion DisconnectModify

            #region DisconnectCancel

            else if (type == o.RequestInformationRequestType.DisconnectServiceCancel)
            {
                or.responseDescription = o.ResponseDescription.DisconnectServiceCancel;
                or.Item = new o.tadpoleToCowsDisconnectServiceCancel();
            }

            #endregion DisconnectCancel

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(o.tadpoleToCows));
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, or);
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        private string ValidateXML(string xmlString)
        {
            XmlSchemaSet schemas = new XmlSchemaSet();
            string path = ConfigurationManager.AppSettings["XSDPath"].ToString();
            schemas.Add(null, path);

            string result = "";
            try
            {
                XDocument custOrdDoc = XDocument.Parse(xmlString);
                custOrdDoc.Validate(schemas, (o, e) =>
                {
                    result = e.Message;
                });
            }
            catch (Exception)
            {
                result = "Malformed XML, unable to validate xml against xsd";
                _strError += "Malformed XML, unable to validate xml against xsd";
            }

            FL.WriteLogFile("Receiver", FL.Event.GenericMessage, FL.MsgType.text, -1, "", string.Format("XML-xsd Validation Result: {0}", result), string.Empty);

            return result;
        }

        private void UpdateTimeSlots(int eventID, ActionType MessageType, DateTime? StartTime, DateTime? EndTime)
        {
            if (StartTime == null || EndTime == null)
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, false);
                return;
            }

            //Zero out any minutes for the purpose of setting the time slots.  Have to change this approach
            //if time slots ever stray from the per-hour model.
            DateTime st = new DateTime(StartTime.Value.Year, StartTime.Value.Month, StartTime.Value.Day, StartTime.Value.Hour, 0, 0);
            DateTime et = new DateTime(EndTime.Value.Year, EndTime.Value.Month, EndTime.Value.Day, EndTime.Value.Hour, 0, 0);
            if (EndTime.Value > et)
                et = et.AddHours(1);

            List<TimeSlot> timeSlots = teDB.GetFedTimeSlots();

            //Wrap in loop in case order encloses multiple days
            for (DateTime day = st.Date; day <= et.Date; day = day.AddDays(1))
            {
                //Don't insert time slots for weekends, those get flagged as invalid timeslot below
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                {
                    foreach (TimeSlot t in timeSlots)
                    {
                        if ((st.Hour <= t.Start.Hours || day > st.Date) && (day < et.Date || et.Hour >= t.End.Hours))
                        {
                            //For Cancels, -1, for Installs +1, for Modify add or take one based on logic used to call this method
                            if (MessageType == ActionType.NCC || MessageType == ActionType.NIC)
                                teDB.IncrementTimeSlot(day, t.TimeSlotID, -1);
                            else if (MessageType == ActionType.NCI || MessageType == ActionType.NDM)
                                teDB.IncrementTimeSlot(day, t.TimeSlotID, 1);
                        }
                    }
                }
            }

            //Check for invalid Time Slots
            //Set up Fed Holidays..
            List<DateTime> fedHolidays = new List<DateTime>()
            {
                new DateTime(2012, 2, 20), new DateTime(2012, 10, 8), new DateTime(2012, 11, 12),
                new DateTime(2013, 2, 18), new DateTime(2013, 10, 14), new DateTime(2013, 11, 11),
                new DateTime(2014, 2, 17), new DateTime(2014, 10, 13), new DateTime(2014, 11, 11),
                new DateTime(2015, 2, 16), new DateTime(2015, 10, 12), new DateTime(2015, 11, 11),
                new DateTime(2016, 2, 15), new DateTime(2016, 10, 10), new DateTime(2016, 11, 11),
                new DateTime(2017, 2, 20), new DateTime(2017, 10, 9), new DateTime(2017, 11, 10),
                new DateTime(2018, 2, 19), new DateTime(2018, 10, 8), new DateTime(2018, 11, 12),
                new DateTime(2019, 2, 18), new DateTime(2019, 10, 14), new DateTime(2019, 11, 11),
                new DateTime(2020, 2, 17), new DateTime(2020, 10, 12), new DateTime(2020, 11, 11)
            };

            if (MessageType == ActionType.NCC || MessageType == ActionType.NIC)
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, true);
            }
            else if (st.DayOfWeek == DayOfWeek.Saturday || st.DayOfWeek == DayOfWeek.Sunday || st.Date != et.Date || teDB.IsSprintHoliday(st.Date) || fedHolidays.Contains(st.Date))
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, false);
            }
            else if (DateTime.Now.AddDays(7 * 6) < st)  //Time slots are only allowed up to 42 days out
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, false);
            }
            else if (st < DateTime.Now.Date)
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, false);
            }
            else if (StartTime.Value >= EndTime.Value)
            {
                teDB.UpdateInvalidTimeSlotTable(eventID, false);
            }
            else
            {
                //Assumptions made for this to work: Timeslots are all 1 hour each, on the hour.
                List<int> hoursList = new List<int>();
                for (int i = st.Hour; i < et.Hour; i++)
                    hoursList.Add(i);

                foreach (TimeSlot t in timeSlots)
                {
                    if (hoursList.Contains(t.Start.Hours))
                        hoursList.Remove(t.Start.Hours);
                }

                if (hoursList.Count > 0)    //there is an hour that isn't covered by a time slot
                    teDB.UpdateInvalidTimeSlotTable(eventID, false);
                else                        //All time slots are covered, event is good.
                    teDB.UpdateInvalidTimeSlotTable(eventID, true);
            }
        }

        /// <summary>
        /// Implements business rules for new status based on the old status.  Old status is set
        /// when previous event data is retrieved.
        /// </summary>
        /// <param name="tadpoleEvent"></param>
        private void GetNewStatus(TadpoleEventDataEntity tadpoleEvent)
        {
            int CurrentStatus = tadpoleEvent.Status;

            if (tadpoleEvent.OrderType == ActionType.NCI)    //New Installs
            {
                tadpoleEvent.Status = 2;    //Pending
            }
            else if (tadpoleEvent.OrderType == ActionType.NCC || tadpoleEvent.OrderType == ActionType.NIC)    //Cancel Install
            {
                //2012-07-23 - Users have requested all events move to Pending Device Return when cancelled unless
                //the event was in Pending status (that goes straight to Cancelled)
                switch (CurrentStatus)
                {
                    //1c8v) #1 - 1.	If the COWS event is ‘pending’ when the cancel request
                    //is received, COWS will systematically change the event status to ‘cancelled’.
                    //COWS will send an asynchronous message to TADPOLE with the status = ‘cancelled’
                    case 2:
                        tadpoleEvent.Status = 13;
                        tadpoleEvent.ChangeLog += "Event Status changed from Pending to Cancelled";
                        break;

                    case 3:
                        tadpoleEvent.Status = 16;
                        tadpoleEvent.ChangeLog += "Event Status changed from Rework to Pending Device Return";
                        break;

                    case 5:
                        tadpoleEvent.Status = 16;
                        tadpoleEvent.ChangeLog += "Event Status changed from In Progress to Pending Device Return";
                        break;

                    //1c8iii) If Completed, move to cancel pending
                    case 6:
                        tadpoleEvent.Status = 16;
                        tadpoleEvent.ChangeLog += "Event Status changed from Completed to Pending Device Return";
                        break;

                    case 9:
                        tadpoleEvent.Status = 16;
                        tadpoleEvent.ChangeLog += "Event Status changed from Fulfilling to Pending Device Return";
                        break;

                    case 10:
                        tadpoleEvent.Status = 16;
                        tadpoleEvent.ChangeLog += "Event Status changed from Shipped to Pending Device Return";
                        break;

                    case 11:
                        tadpoleEvent.Status = 16;
                        break;

                    case 12:
                        tadpoleEvent.Status = 16;
                        break;

                    case 13:
                        tadpoleEvent.Status = 13;
                        break;

                    case 16:
                        tadpoleEvent.Status = 16;
                        break;
                }
            }
            else if (tadpoleEvent.OrderType == ActionType.NCM || tadpoleEvent.OrderType == ActionType.NDM || tadpoleEvent.OrderType == ActionType.NIM)
            {
                //1b6iii) If the COWS event is ‘completed’ and TADPOLE sends a ‘new customer modify’
                //order type message, COWS will ACK and database the message.  COWS will change the
                //event status from ‘complete’ to ‘pending’ and update the event history with the
                //details of fields that have been modified.

                switch (CurrentStatus)
                {
                    case 3:
                        if (tadpoleEvent.OrderType == ActionType.NDM)
                        {
                            tadpoleEvent.Status = 10;
                            tadpoleEvent.ChangeLog += "Events status changed from Rework to Shipped, no replacement device needed";
                        }
                        break;

                    case 6:
                        tadpoleEvent.Status = 2;
                        tadpoleEvent.ChangeLog += "Event Status changed from Completed to Pending";
                        break;

                    case 11:
                    case 12:
                    case 16:
                        tadpoleEvent.Status = 2;
                        tadpoleEvent.ChangeLog += "Event Status changed from Pending Device Return to Pending";
                        break;

                    case 13:
                        tadpoleEvent.Status = 2;
                        tadpoleEvent.ChangeLog += "Event Status changed from Cancelled to Pending";
                        break;
                }
            }
            else if (tadpoleEvent.OrderType == ActionType.DCC)
            {
                switch (CurrentStatus)
                {
                    //Only case 3 or 15 should ever be hit now.  Added validation to the service that cancels must happen before the due date.
                    //Added validation to the UI that user can not move the order to In Progress until on the due date.
                    case 6:
                        tadpoleEvent.Status = 12;
                        tadpoleEvent.ChangeLog += "Event Status changed from Completed to Cancel Pending";
                        break;

                    case 3:
                        tadpoleEvent.Status = 13;
                        tadpoleEvent.ChangeLog += "Event Status changed from Rework to Cancelled";
                        break;

                    case 2:
                        tadpoleEvent.Status = 13;
                        tadpoleEvent.ChangeLog += "Event Status changed from Pending to Cancelled";
                        break;

                    case 15:
                        tadpoleEvent.Status = 13;
                        tadpoleEvent.ChangeLog += "Event Status changed from Disconnect Request to Cancelled";
                        break;

                    case 5:
                        tadpoleEvent.Status = 12;
                        tadpoleEvent.ChangeLog += "Event Status changed from In Progress to Cancel Pending";
                        break;

                    case 14:
                        tadpoleEvent.Status = 12;
                        tadpoleEvent.ChangeLog += "Event Status changed from Completed-Failed to Clean Device to Cancel Pending";
                        break;
                }
            }
            else if (tadpoleEvent.OrderType == ActionType.DCS)
            {
                tadpoleEvent.Status = 15;
            }
        }

        #endregion Incoming messages

        #region Outgoing Thread

        public List<ResponseEntity> GetResponses()
        {
            return teDB.GetResponses();
        }

        public string GetXMLForResponse(ResponseEntity responseEntity)
        {
            r.cowsToTadpole TadpoleResponseClass = new r.cowsToTadpole();

            TadpoleResponseClass.requestVersion = responseEntity.SequenceNumber;
            TadpoleResponseClass.requestInformation = new r.RequestInformation();
            TadpoleResponseClass.requestInformation.requestID = responseEntity.FRBRequestID;
            TadpoleResponseClass.requestInformation.timestamp = DateTime.Now.ToUniversalTime();
            TadpoleResponseClass.requestInformation.requestType = MapFRBType(responseEntity.FRBRequestType);

            switch (responseEntity.ResponseType)
            {
                case "ShippingConf":

                    //defect 544115 - always send new order install on shipping confirmation, Install complete.  Rework is only applicable
                    //to installs now, so send it there as well.  On status messages, send New Install or New Disconnect
                    //depending on what base type is.
                    TadpoleResponseClass.requestInformation.requestType = r.RequestInformationRequestType.NewOrderInstall;
                    r.cowsToTadpoleNewOrderInstallShipping ShippingNode = new r.cowsToTadpoleNewOrderInstallShipping();
                    ShippingNode.shippingTrackingInformation = new r.ShippingTrackingInformation();
                    ShippingNode.shippingTrackingInformation.shipCarrier = responseEntity.ShipCarrier;
                    ShippingNode.shippingTrackingInformation.shipDate = responseEntity.ShippedDate.ToUniversalTime();
                    ShippingNode.shippingTrackingInformation.shipEstimatedDeliveryDate = responseEntity.ShipDeliveryDate.ToUniversalTime();
                    ShippingNode.shippingTrackingInformation.shipTrackingNumber = responseEntity.ShipTrackingNumber;

                    TadpoleResponseClass.responseDescription = r.ResponseDescription.NewOrderInstallShipping;
                    TadpoleResponseClass.Item = ShippingNode;
                    break;

                case "InstallComplete":
                    TadpoleResponseClass.requestInformation.requestType = r.RequestInformationRequestType.NewOrderInstall;
                    r.cowsToTadpoleNewOrderInstallComplete ICNode = new r.cowsToTadpoleNewOrderInstallComplete();
                    ICNode.actualInstallDate = responseEntity.CreateDate.ToUniversalTime();

                    TadpoleResponseClass.responseDescription = r.ResponseDescription.NewOrderInstallComplete;
                    TadpoleResponseClass.Item = ICNode;
                    break;

                case "StatusMessage":
                    switch (TadpoleResponseClass.requestInformation.requestType)
                    {
                        case r.RequestInformationRequestType.NewOrderInstall:
                        case r.RequestInformationRequestType.NewOrderModify:
                        case r.RequestInformationRequestType.NewOrderCancel:
                            TadpoleResponseClass.requestInformation.requestType = r.RequestInformationRequestType.NewOrderInstall;
                            break;

                        case r.RequestInformationRequestType.DisconnectService:
                        case r.RequestInformationRequestType.DisconnectServiceCancel:
                        case r.RequestInformationRequestType.DisconnectServiceModify:
                            TadpoleResponseClass.requestInformation.requestType = r.RequestInformationRequestType.DisconnectService;
                            break;
                    }
                    r.StatusUpdate StatusNode = new r.StatusUpdate();
                    StatusNode.statusDescription = responseEntity.ResponseDescription;

                    TadpoleResponseClass.responseDescription = r.ResponseDescription.StatusUpdate;
                    TadpoleResponseClass.Item = StatusNode;
                    break;

                case "Rework":
                    TadpoleResponseClass.requestInformation.requestType = r.RequestInformationRequestType.NewOrderRework;
                    r.NewOrderRework ReworkNode = new r.NewOrderRework();
                    List<r.NewOrderReworkReworkActivity> reworkList = new List<r.NewOrderReworkReworkActivity>();
                    switch (responseEntity.ResponseDescription)
                    {
                        case "Customer No Show":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.CustomerNoShow;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.RescheduleNotification;
                            break;

                        case "Customer Network/Circuit Issue":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.CustomerNetworkCircuitIssue;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.RescheduleNotification;
                            break;

                        case "Sprint Issue (e.g., Sprint No Show, Sprint network issue)":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.SprintIssue;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.RescheduleNotification;
                            break;

                        case "Defective Device - Customer Mis-config":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.CustomerCausedConfigProblem;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.BadDeviceNotification;
                            break;

                        case "Defective Device - Sprint Mis-config":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.SprintCausedConfigProblem;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.BadDeviceNotification;
                            break;

                        case "Defective Device - Hardware":
                            ReworkNode.reworkCode = r.NewOrderReworkReworkCode.HardwareFailure;
                            ReworkNode.reworkActivity = r.NewOrderReworkReworkActivity.BadDeviceNotification;
                            break;
                    }
                    TadpoleResponseClass.responseDescription = r.ResponseDescription.NewOrderRework;
                    TadpoleResponseClass.Item = ReworkNode;
                    break;

                case "DiscCompleteExc":
                case "DiscoComplete":
                    r.cowsToTadpoleDisconnectServiceComplete DiscoCompleteNode = new r.cowsToTadpoleDisconnectServiceComplete();
                    if (responseEntity.ResponseDescription != string.Empty)
                        DiscoCompleteNode.errorDescriptionList = new string[] { responseEntity.ResponseDescription };
                    else
                        DiscoCompleteNode.errorDescriptionList = new string[] { };
                    TadpoleResponseClass.responseDescription = r.ResponseDescription.DisconnectServiceComplete;
                    TadpoleResponseClass.Item = DiscoCompleteNode;
                    break;

                case "ACK":
                    r.AcknowledgmentInformation AckNode = new r.AcknowledgmentInformation();
                    AckNode.status = r.Status.Accepted;
                    TadpoleResponseClass.responseDescription = MapDescription(TadpoleResponseClass.requestInformation.requestType);
                    TadpoleResponseClass.Item = AckNode;
                    break;
            }

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(r.cowsToTadpole));
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, TadpoleResponseClass);
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        public bool UpdateMessageStatus(int MessageID, byte StatusID)
        {
            return teDB.UpdateMessageStatus(MessageID, StatusID);
        }

        #endregion Outgoing Thread

        #region General Methods

        private string CleanNull(string obj)
        {
            return obj == null ? "" : obj.Trim();
        }

        private int CleanNull(int? obj)
        {
            return obj == null ? 0 : (int)obj;
        }

        private DateTime? ChopTimeSlot(DateTime? dt)
        {
            if (dt.HasValue)
                return dt.Value.Date.AddHours(dt.Value.Hour).AddMinutes(dt.Value.Minute);
            else
                return null;
        }

        private r.ResponseDescription MapDescription(o.ResponseDescription obj)
        {
            switch (obj)
            {
                case o.ResponseDescription.DisconnectService:
                    return r.ResponseDescription.DisconnectService;

                case o.ResponseDescription.DisconnectServiceCancel:
                    return r.ResponseDescription.DisconnectServiceCancel;

                case o.ResponseDescription.DisconnectServiceComplete:
                    return r.ResponseDescription.DisconnectServiceComplete;

                case o.ResponseDescription.NewOrderCancel:
                    return r.ResponseDescription.NewOrderCancel;

                case o.ResponseDescription.NewOrderCancelConfirm:
                    return r.ResponseDescription.NewOrderCancelConfirm;

                case o.ResponseDescription.NewOrderInstall:
                    return r.ResponseDescription.NewOrderInstall;

                case o.ResponseDescription.NewOrderInstallComplete:
                    return r.ResponseDescription.NewOrderInstallComplete;

                case o.ResponseDescription.NewOrderInstallConfirm:
                    return r.ResponseDescription.NewOrderInstallConfirm;

                case o.ResponseDescription.NewOrderInstallShipping:
                    return r.ResponseDescription.NewOrderInstallShipping;

                case o.ResponseDescription.NewOrderModify:
                    return r.ResponseDescription.NewOrderModify;

                case o.ResponseDescription.NewOrderModifyConfirm:
                    return r.ResponseDescription.NewOrderModifyConfirm;

                case o.ResponseDescription.NewOrderRework:
                    return r.ResponseDescription.NewOrderRework;

                case o.ResponseDescription.NewOrderReworkConfirm:
                    return r.ResponseDescription.NewOrderReworkConfirm;

                case o.ResponseDescription.StatusUpdate:
                    return r.ResponseDescription.StatusUpdate;

                default:
                    return r.ResponseDescription.NewOrderInstall;
            }
        }

        private o.ResponseDescription MapDescription(r.ResponseDescription obj)
        {
            switch (obj)
            {
                case r.ResponseDescription.DisconnectService:
                    return o.ResponseDescription.DisconnectService;

                case r.ResponseDescription.DisconnectServiceCancel:
                    return o.ResponseDescription.DisconnectServiceCancel;

                case r.ResponseDescription.DisconnectServiceComplete:
                    return o.ResponseDescription.DisconnectServiceComplete;

                case r.ResponseDescription.NewOrderCancel:
                    return o.ResponseDescription.NewOrderCancel;

                case r.ResponseDescription.NewOrderCancelConfirm:
                    return o.ResponseDescription.NewOrderCancelConfirm;

                case r.ResponseDescription.NewOrderInstall:
                    return o.ResponseDescription.NewOrderInstall;

                case r.ResponseDescription.NewOrderInstallComplete:
                    return o.ResponseDescription.NewOrderInstallComplete;

                case r.ResponseDescription.NewOrderInstallConfirm:
                    return o.ResponseDescription.NewOrderInstallConfirm;

                case r.ResponseDescription.NewOrderInstallShipping:
                    return o.ResponseDescription.NewOrderInstallShipping;

                case r.ResponseDescription.NewOrderModify:
                    return o.ResponseDescription.NewOrderModify;

                case r.ResponseDescription.NewOrderModifyConfirm:
                    return o.ResponseDescription.NewOrderModifyConfirm;

                case r.ResponseDescription.NewOrderRework:
                    return o.ResponseDescription.NewOrderRework;

                case r.ResponseDescription.NewOrderReworkConfirm:
                    return o.ResponseDescription.NewOrderReworkConfirm;

                case r.ResponseDescription.StatusUpdate:
                    return o.ResponseDescription.StatusUpdate;

                default:
                    return o.ResponseDescription.NewOrderInstall;
            }
        }

        private r.ResponseDescription MapDescription(string obj)
        {
            switch (obj)
            {
                case "DisconnectService":
                    return r.ResponseDescription.DisconnectService;

                case "DisconnectServiceCancel":
                    return r.ResponseDescription.DisconnectServiceCancel;

                case "DisconnectServiceComplete":
                    return r.ResponseDescription.DisconnectServiceComplete;

                case "NewOrderCancel":
                    return r.ResponseDescription.NewOrderCancel;

                case "NewOrderCancelConfirm":
                    return r.ResponseDescription.NewOrderCancelConfirm;

                case "NewOrderInstall":
                    return r.ResponseDescription.NewOrderInstall;

                case "NewOrderInstallComplete":
                    return r.ResponseDescription.NewOrderInstallComplete;

                case "NewOrderInstallConfirm":
                    return r.ResponseDescription.NewOrderInstallConfirm;

                case "NewOrderInstallShipping":
                    return r.ResponseDescription.NewOrderInstallShipping;

                case "NewOrderModify":
                    return r.ResponseDescription.NewOrderModify;

                case "NewOrderModifyConfirm":
                    return r.ResponseDescription.NewOrderModifyConfirm;

                case "NewOrderRework":
                    return r.ResponseDescription.NewOrderRework;

                case "NewOrderReworkConfirm":
                    return r.ResponseDescription.NewOrderReworkConfirm;

                case "StatusUpdate":
                    return r.ResponseDescription.StatusUpdate;

                default:
                    return r.ResponseDescription.NewOrderInstall;
            }
        }

        /// <summary>
        /// Order Request Type to Response Request type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private r.RequestInformationRequestType MapFRBType(o.RequestInformationRequestType obj)
        {
            switch (obj)
            {
                case o.RequestInformationRequestType.NewOrderInstall:
                    return r.RequestInformationRequestType.NewOrderInstall;

                case o.RequestInformationRequestType.NewOrderCancel:
                    return r.RequestInformationRequestType.NewOrderCancel;

                case o.RequestInformationRequestType.NewOrderModify:
                    return r.RequestInformationRequestType.NewOrderModify;

                case o.RequestInformationRequestType.DisconnectService:
                    return r.RequestInformationRequestType.DisconnectService;

                case o.RequestInformationRequestType.DisconnectServiceCancel:
                    return r.RequestInformationRequestType.DisconnectServiceCancel;

                default:
                    return r.RequestInformationRequestType.NewOrderInstall;
            }
        }

        /// <summary>
        /// Response Request Type to Order Request Type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private o.RequestInformationRequestType MapFRBType(r.RequestInformationRequestType obj)
        {
            switch (obj)
            {
                case r.RequestInformationRequestType.NewOrderInstall:
                    return o.RequestInformationRequestType.NewOrderInstall;

                case r.RequestInformationRequestType.NewOrderCancel:
                    return o.RequestInformationRequestType.NewOrderCancel;

                case r.RequestInformationRequestType.NewOrderModify:
                    return o.RequestInformationRequestType.NewOrderModify;

                case r.RequestInformationRequestType.DisconnectService:
                    return o.RequestInformationRequestType.DisconnectService;

                case r.RequestInformationRequestType.DisconnectServiceCancel:
                    return o.RequestInformationRequestType.DisconnectServiceCancel;

                default:
                    return o.RequestInformationRequestType.NewOrderInstall;
            }
        }

        /// <summary>
        /// String to Response Request Type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private r.RequestInformationRequestType MapFRBType(string obj)
        {
            switch (obj)
            {
                case "NewOrderInstall":
                    return r.RequestInformationRequestType.NewOrderInstall;

                case "NewOrderCancel":
                    return r.RequestInformationRequestType.NewOrderCancel;

                case "NewOrderModify":
                    return r.RequestInformationRequestType.NewOrderModify;

                case "DisconnectService":
                    return r.RequestInformationRequestType.DisconnectService;

                case "DisconnectServiceCancel":
                    return r.RequestInformationRequestType.DisconnectServiceCancel;

                case "DisconnectServiceModify":
                    return r.RequestInformationRequestType.DisconnectServiceModify;

                case "NCI":
                    return r.RequestInformationRequestType.NewOrderInstall;

                case "NCC":
                case "NIC":
                    return r.RequestInformationRequestType.NewOrderCancel;

                case "NCM":
                case "NIM":
                case "NDM":
                    return r.RequestInformationRequestType.NewOrderModify;

                case "DCS":
                    return r.RequestInformationRequestType.DisconnectService;

                case "DCC":
                    return r.RequestInformationRequestType.DisconnectServiceCancel;

                default:
                    return r.RequestInformationRequestType.NewOrderInstall;
            }
        }

        private r.ResponseDescription MapDescription(r.RequestInformationRequestType obj)
        {
            switch (obj)
            {
                case r.RequestInformationRequestType.DisconnectService:
                    return r.ResponseDescription.DisconnectService;

                case r.RequestInformationRequestType.DisconnectServiceCancel:
                    return r.ResponseDescription.DisconnectServiceCancel;

                case r.RequestInformationRequestType.NewOrderCancel:
                    return r.ResponseDescription.NewOrderCancel;

                case r.RequestInformationRequestType.NewOrderInstall:
                    return r.ResponseDescription.NewOrderInstall;

                case r.RequestInformationRequestType.NewOrderModify:
                    return r.ResponseDescription.NewOrderModify;

                default:
                    return r.ResponseDescription.NewOrderInstall;
            }
        }

        private string GetCOWSCodeForRequestType(o.RequestInformationRequestType obj)
        {
            switch (obj)
            {
                case o.RequestInformationRequestType.NewOrderInstall:
                    return "NCI";

                case o.RequestInformationRequestType.NewOrderModify:
                    return "NCM";

                case o.RequestInformationRequestType.NewOrderCancel:
                    return "NCC";

                case o.RequestInformationRequestType.DisconnectService:
                    return "DCS";

                case o.RequestInformationRequestType.DisconnectServiceCancel:
                    return "DCC";

                default:
                    return "NCI";
            }
        }

        private ActionType GetCOWSCodeForRequestType(o.ResponseDescription obj)
        {
            switch (obj)
            {
                case o.ResponseDescription.NewOrderInstall:
                    return ActionType.NCI;

                case o.ResponseDescription.NewOrderModify:

                    //Return NCM here.  Then set to NDM if there is a change in the actual time slot.
                    return ActionType.NCM;

                case o.ResponseDescription.NewOrderCancel:
                    return ActionType.NCC;

                case o.ResponseDescription.DisconnectService:
                    return ActionType.DCS;

                case o.ResponseDescription.DisconnectServiceCancel:
                    return ActionType.DCC;

                default:
                    return ActionType.NCI;
            }
        }

        private string GetMessageName(o.ResponseDescription obj)
        {
            switch (obj)
            {
                case o.ResponseDescription.NewOrderInstallComplete:
                    return "InstallComplete";

                case o.ResponseDescription.NewOrderInstallShipping:
                    return "ShippingConf";

                case o.ResponseDescription.StatusUpdate:
                    return "StatusMessage";

                case o.ResponseDescription.NewOrderReworkConfirm:
                    return "Rework";

                case o.ResponseDescription.DisconnectServiceComplete:
                    return "DiscComplete";

                default:
                    return "";
            }
        }

        private string GetPortSpeed(o.PortSpeedType obj)
        {
            switch (obj)
            {
                case o.PortSpeedType.AUTO:
                    return "AUTO";

                case o.PortSpeedType.Item10:
                    return "10";

                case o.PortSpeedType.Item100:
                    return "100";

                case o.PortSpeedType.Item1000:
                    return "1000";

                default:
                    return "AUTO";
            }
        }

        #endregion General Methods
    }
}