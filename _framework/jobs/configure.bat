@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework job to configure the application. An ERRORLEVEL of 0 will be 
REM returned if the operation is successful, and a non-zero ERRORLEVEL will be 
REM returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize application home and call configure script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\scripts\configure.cmd"
