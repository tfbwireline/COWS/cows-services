﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:key name="tabids" match="EventEmail/Tab[@TABID]" use="@TABID" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="1000" border="0" align="left" style="font-size=12px;" cellpadding="1">
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">Escalation Reason : <br/><br/>
            </td>
              <td width="700" style="text-align: left;">
                <xsl:value-of select="/EventEmail/MDSEvent/ESCL_REAS_DES" />
                <br/>
				<br/>
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">
                Primary Requested Date/Time : <br/><br/> </td>
              <td width="700" style="text-align: left;">
                <xsl:value-of select="/EventEmail/MDSEvent/PRIM_REQ_DT" />
                <br/>
				<br/>
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">Secondary Requested Date/Time : <br/><br/>
            </td>
              <td width="700" style="text-align: left;">
                <xsl:value-of select="/EventEmail/MDSEvent/SCNDY_REQ_DT" />
                <br/>
				<br/>
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600; vertical-align: top;">Business Justification : <br/><br/><br/><br/>
            </td>
              <td width="700" style="text-align: left;">
				<span style="text-align: left; word-break: break-all;">
					<xsl:value-of select="/EventEmail/MDSEvent/ESCL_BUS_JUSTN_TXT" />
				</span>
                <br/>
                <br/>
                <br/>
				<br/>
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">Requestor Name : </td>
              <td width="700">
                <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER" />
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">Requestor Phone Number : </td>
              <td width="700">
                <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_PHN" />
              </td>
            </tr>
            <tr>
              <td width="300" style="text-align: left; font-weight: 600">Requestor Email : </td>
              <td width="700">
                <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_EMAIL" />
              </td>
            </tr>
			<tr>
              <td width="100%" colspan="2">
                <br/>
                <br/>
                <span>
                  1. Generic H1 (999999999) events do not have an MDS Reviewer automatically assigned. <b style="color: #dc3545;">The CSM/IM must place the assigned account MNS NIM in the Contact List on the event.</b>
                </span>
              </td>
            </tr>
            <tr>
              <td width="100%"  colspan="2">
                <br/>
                <span>
                  2. Escalation emails are generated through COWS, not Outlook. There is <u style="color: #dc3545;"><b>no out of office functionality</b></u> available.
                </span>
              </td>
            </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
