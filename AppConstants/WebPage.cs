﻿namespace AppConstants
{
    public enum WebPageID
    {
        None = 0,
        xNCIOrderDetail = 1000,
        VendorOrderAdd = 1010,
        VendorOrderUpdate = 1020,
        CCD = 1050,
        H5Folder = 2000,
        IPL = 2333,
        DPL = 2334,
        DBBOrderDetail = 1000
    }
}