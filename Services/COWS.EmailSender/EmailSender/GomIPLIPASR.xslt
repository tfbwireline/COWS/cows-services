﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="80%">
              <xsl:value-of disable-output-escaping="yes"  select="GomIPLIPASR/notes" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>