﻿using AppConstants;
using EmailManager;
using IOManager;
using System;
using System.Configuration;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Linq;
using static LogManager.FileLogger;
using CL = LogManager.FileLogger;

namespace COWS.EmailSender
{
    public class EmailSenderIO
    {
        public void SendEmailRequests()
        {
            CheckEmailRequests();
        }

        private void CheckEmailRequests()
        {
            try
            {
                EmailSenderDB _SenderDB = new EmailSenderDB();
                DataSet DS = new DataSet();
                DataTable DT = new DataTable();
                DS = _SenderDB.GetEmailRequests();

                if ((DS != null) && (DS.Tables.Count > 0))
                {
                    //Process the Orders First.
                    DT = DS.Tables[0];
                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " Orders found to send Emails", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            if (i["EMAIL_REQ_TYPE_ID"].ToString() == "9")
                            {
                                CreateAndSendGomPLCompleteEmail(i);
                            }
                            else if (i["EMAIL_REQ_TYPE_ID"].ToString() == "17")
                            {
                                CreateAndSendGomIPLIPASREmail(i);
                            }
                            else
                            {
                                CreateAndSendEmail(i);
                            }
                        }
                    }

                    ////Once Orders are done, get to the Events Next.
                    try
                    {
                        DT = DS.Tables[1];
                    }
                    catch
                    {
                        DT = null;
                    }

                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " Events found to send Emails", string.Empty);

                        //TODO. MDSEvent Email code.

                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendEventEmail(i, i["EMAIL_REQ_TYPE_ID"].ToString());
                        }
                    }

                    try
                    {
                        DT = DS.Tables[2];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " NCCO Orders found to send Emails", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendNCCOOrders(i);
                        }
                    }

                    try
                    {
                        DT = DS.Tables[3];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " Adhoc Report Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendAdhocRptNtfy(i);
                        }
                    }
                    try
                    {
                        DT = DS.Tables[4];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " H5 Cust Assign Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendH5AssignUserNtfy(i);
                        }
                    }

                    try
                    {
                        DT = DS.Tables[5];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if ((DT != null) && (DT.Rows.Count > 0))
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " MDS Guest Wi-fi Activation/Comments Emails", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendMDSSrvc(i);
                        }
                    }

                    try
                    {
                        DT = null;
                        DT = DS.Tables[6];
                    }
                    catch
                    {
                        DT = null;
                    }

                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " ASR Email notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            SendASRRequests(i);
                        }
                    }

                    try
                    {
                        DT = DS.Tables[7];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " Redesign Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendRedesignEmail(i);
                        }
                    }

                    // For CPT 24 hour notification - Shortname and IP Address
                    try
                    {
                        DT = DS.Tables[8];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        foreach (DataRow i in DT.Rows)
                        {
                            // Log the number of CPT notifications ready to be sent out.
                            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                                DT.Rows.Count.ToString() + " CPT 24-hour Notifications for Shortname or IP Address", string.Empty);
                            CreateAndSendCPT24HourNotification(i);
                        }
                    }

                    try
                    {
                        DT = DS.Tables[9]; // CPE Return to sales email notifications.
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " CPT Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendCPErtsEmail(i);
                        }
                    }

                    // For CPT Email Notification - Submitted, Updated, Cancelled, Completed Requests
                    try
                    {
                        DT = DS.Tables[10];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        foreach (DataRow i in DT.Rows)
                        {
                            CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1,
                                string.Empty, DT.Rows.Count.ToString() + " CPT Email", string.Empty);
                            CreateAndSendCPTEmail(i);
                        }
                    }

                    try
                    {
                        DT = DS.Tables[11];  // Send CPE order cancellation notifications to PeopleSoft.
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, DT.Rows.Count.ToString() + " CPT Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendPSEmail(i);
                        }
                    }

                    // Added by qi931353 - 08092017
                    // Get SDE Opportunity Email Request
                    try
                    {
                        DT = DS.Tables[12];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " SDE Opportunity Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendSDEEmail(i);
                        }
                    }

                    // Get SDE Account Setup Notification Email
                    try
                    {
                        DT = DS.Tables[13];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " SDE Account Setup Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendSDEAccountEmail(i);
                        }
                    }

                    // CPE CLLI Request Email
                    try
                    {
                        DT = DS.Tables[14];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " CLLI Request Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendCLLIEmails(i);
                        }
                    }

                    // CPE Zscaler Disconnect Email
                    try
                    {
                        DT = DS.Tables[15];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " Zscaler Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendZscalerOrders(i);
                        }
                    }
                    // IS Bill Only Email notification for orders in task 110
                    try
                    {
                        DT = DS.Tables[16];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " IS Bill Only Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendBillOnlyOrderEmail(i);
                        }
                    }
                    // IPM shipping Required Email (PJ022373)
                    try
                    {
                        DT = DS.Tables[17];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " IPM shipping Required Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendIPMEmail(i);
                        }
                    }

                    // CPE Disconnect Request Email (PJ022373)
                    try
                    {
                        DT = DS.Tables[18];
                    }
                    catch
                    {
                        DT = null;
                    }
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                            DT.Rows.Count.ToString() + " CPE Disconnect Request Email Notifications", string.Empty);
                        foreach (DataRow i in DT.Rows)
                        {
                            CreateAndSendCPEDiscEmail(i);
                        }
                    }
                }
                else
                {
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Emails", string.Empty);
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile(CL.Event.GenericMessage, CL.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
            }
        }

        private void CreateAndSendAdhocRptNtfy(DataRow _dr)
        {
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

            string _EmailBody = string.Empty;

            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_objDB, _Email_Req_ID.ToString(), _Email_Req_ID, _Status, MsgClass.EmailRequests);
                return;
            }
           
            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent with Subject: " + _Subj + " To these email addresses: " + _EmailAddress, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, string.Empty, string.Empty)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for EmailReqID : " + _Email_Req_ID.ToString(), string.Empty);
            }
        }

        private void CreateAndSendMDSSrvc(DataRow _dr)
        {
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _EventID = _dr["EVENT_ID"].ToString();

            string _EmailBody = string.Empty;


            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_objDB, _Email_Req_ID.ToString(), _Email_Req_ID, _Status, MsgClass.EmailRequests);
                return;
            }


            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent with Subject: " + _Subj + " To these email addresses: " + _EmailAddress, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _EventID, _CCEmail)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for EmailReqID : " + _Email_Req_ID.ToString(), string.Empty);
            }
        }

        private void CreateAndSendH5AssignUserNtfy(DataRow _dr)
        {
            //bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            //string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

            string _EmailBody = string.Empty;

            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_objDB, _Email_Req_ID.ToString(), _Email_Req_ID, _Status, MsgClass.EmailRequests);
                return;
            }

            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent with Subject: " + _Subj + " To these email addresses: " + _EmailAddress, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, string.Empty, string.Empty)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for EmailReqID : " + _Email_Req_ID.ToString(), string.Empty);
            }
        }

        private void CreateAndSendEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = i["ATC_EMAIL"].ToString();
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();

            string _Subject = CreateOrderSubject(i);
            string _EmailBody = CreateOrderEmailBody(i);
            string _EmailCC = "";
            if ((i["EMAIL_REQ_TYPE_ID"].ToString() == "2") && (Convert.ToBoolean(i["MDS_CD"]) == true))
            {
                _EmailCC = ConfigurationManager.AppSettings["CancelEmailCCList"].ToString();
            }

            if ((i["EMAIL_REQ_TYPE_ID"].ToString() == "1") && (i["PRIM_TAC_EMAIL"].ToString() != null))
            {
                _EmailCC = i["PRIM_TAC_EMAIL"].ToString();
            }

            if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
            {
                _Status = 12;
            }

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
            }
        }

        private string CreateOrderEmailBody(DataRow DR)
        {
            bool _isIPL = Convert.ToBoolean(DR["ISIPL"].ToString());
            XMLHelper _XHelper = new XMLHelper();
            XElement _Email = new XElement("ATCEmail",
                               new XElement("H1", DR["H1_CUST_ID"].ToString()),
                               new XElement("FTN", DR["FTN"].ToString()),
                               new XElement("H5_H6", DR["H5_H6_CUST_ID"].ToString()),
                               new XElement("CUST_NME", DR["CUST_NME"].ToString()),
                               new XElement("Location", DR["H5_H6_ADR"].ToString()),
                               new XElement("OrderType", DR["FSA_ORDR_TYPE_DES"].ToString()),
                               new XElement("CCD", DR["CUST_CMMT_DT"].ToString()),
                               new XElement("ATC", DR["ATC_EMAIL"].ToString()),
                               new XElement("DCD", DR["DCD"].ToString())
                               );

            string html = _XHelper.transformXMLtoHTML(_Email.ToString(), DR["EMAIL_REQ_TYPE"].ToString(), 0);
            if (_isIPL)
                html = html.Replace("FTN :", "CTN :");
            return html;
        }

        private string CreateOrderSubject(DataRow DR)
        {
            string _Subject = string.Empty;
            bool _isIPL = Convert.ToBoolean(DR["ISIPL"].ToString());
            string ftnCtn = "> FTN # ";

            if (_isIPL)
                ftnCtn = "> CTN # ";

            if (DR["EMAIL_REQ_TYPE_ID"].ToString() == "1")
            {
                _Subject = "RTS: <" + DR["CUST_NME"].ToString() + "> <" + DR["H5_H6_ADR"].ToString() + ftnCtn + DR["FTN"].ToString();
            }
            else if (DR["EMAIL_REQ_TYPE_ID"].ToString() == "2")
            {
                _Subject = "CANCELLED: <" + DR["CUST_NME"].ToString() + "> <" + DR["H5_H6_ADR"].ToString() + ftnCtn + DR["FTN"].ToString();
            }

            return _Subject;
        }

        private bool SendEmail(string _EmailAddress, string _Subject, string _EmailBody, string _ID, string _EmailCC)
        {
            bool _RetVal = true;
            string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

            try
            {
                EmailHelper._SendSingleEmail(_FromEmail, _EmailAddress, _EmailCC, string.Empty, _Subject, _EmailBody);
                if (_ID != string.Empty)
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email request created and sent for Order/Event ID: " + _ID, _Subject);
                else
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email request created and sent for Adhoc Report Notification", _Subject);
            }
            catch (Exception Ex)
            {
                if (_ID != string.Empty)
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Errored out while sending email out for Order/Event ID: " + _ID, Ex.Message.ToString());
                else
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Errored out while sending email for Adhoc Report", Ex.Message.ToString());
                _RetVal = false;
            }

            return _RetVal;
        }

        private bool CreateAndSendEventEmail(DataRow _dr, string sReqTyp)
        {
            string sCalenderEntryURL = ConfigurationManager.AppSettings["CalenderEntryURL"].ToString();
            string sViewEventURL = ConfigurationManager.AppSettings["ViewEventURL"].ToString();
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _EventID = Convert.ToInt32(_dr["Event_ID"].ToString());
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            string _EmailAddress = _dr["ATC_MAIL"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();
            string _EmailReqType = _dr["EMAIL_REQ_TYPE"].ToString();

            string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

            string _EmailBody = string.Empty;

            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _EmailReqType, 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_objDB, _Email_Req_ID.ToString(), Convert.ToInt32(_EventID), _Status, MsgClass.Events);
                return false;
            }


            if (sReqTyp.Equals("30"))
            {
                _EmailAddress = ConfigurationManager.AppSettings["FedlineReorderEmailTo"].ToString();
                _CCEmail = ConfigurationManager.AppSettings["FedlineReorderEmailCC"].ToString();
            }

            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent for Event_ID: " + _EventID.ToString() + " with Subject: " + _Subj + " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _EventID.ToString(), _CCEmail)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, _EventID) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for Event_ID: " + _EventID, string.Empty);
            }

            return _RetVal;
        }

        internal void SendRetractEMails()
        {
            DataTable DT = new DataTable();
            int _Status = 11;
            int _EventID = -1;
            int _EventTypeID;
            int _IsNewMDS;
            EmailSenderDB _objDB = new EmailSenderDB();
            string sCalenderEntryURL = ConfigurationManager.AppSettings["CalenderEntryURL"].ToString();
            string sViewEventURL = ConfigurationManager.AppSettings["ViewEventURL"].ToString();
            string sChatURL = ConfigurationManager.AppSettings["ViewEventURL"].ToString() + "ChatLinks.aspx";
            DataSet ds = new DataSet();
            DataTable dtMDSEvent = new DataTable();
            StringBuilder sbEmailAddress = new StringBuilder();

            try
            {
                DT = _objDB.GetRetractEvents();
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error while getting records for nightly rework emails : " + ex.Message + " ; " + ex.StackTrace, string.Empty);
            }

            try
            {
                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    foreach (DataRow dr in DT.Rows)
                    {
                        try
                        {
                            _EventID = Int32.Parse(dr["EVENT_ID"].ToString());
                            _EventTypeID = Int32.Parse(dr["EVENT_TYPE_ID"].ToString());
                            _IsNewMDS = Int32.Parse(dr["IsNewMDS"].ToString());
                            sbEmailAddress.Clear();

                            if ((_EventTypeID == 5) && (_IsNewMDS == 0))
                            {
                                DataTable dtTabs = new DataTable();

                                ds = _objDB.GetMDSEventData(_EventID);

                                if (ds != null)
                                {
                                    dtMDSEvent = ds.Tables[0];
                                    dtTabs = ds.Tables[1];
                                }

                                if ((dtMDSEvent != null) && (dtMDSEvent.Rows.Count > 0))
                                {
                                    string _Subject = "(VISIBLE) " + dtMDSEvent.Rows[0]["EVENT_TITLE_TXT"].ToString() + " EventID: " + _EventID;

                                    if ((ds.Tables[4] != null) && (ds.Tables[4].Rows.Count > 0))
                                        if (!ds.Tables[4].Rows[0]["AssignEmails"].ToString().Equals(string.Empty))
                                            if ((sbEmailAddress.ToString().ToLower().IndexOf(ds.Tables[4].Rows[0]["AssignEmails"].ToString().ToLower())) < 0)
                                            {
                                                if (ds.Tables[4].Rows[0]["AssignEmails"].ToString().Length > 0)
                                                {
                                                    if (sbEmailAddress.Length > 0)
                                                    {
                                                        sbEmailAddress = sbEmailAddress.Append(",");
                                                        sbEmailAddress = sbEmailAddress.Append(ds.Tables[4].Rows[0]["AssignEmails"].ToString());
                                                    }
                                                    else
                                                        sbEmailAddress = sbEmailAddress.Append(ds.Tables[4].Rows[0]["AssignEmails"].ToString());
                                                }
                                            }

                                    if ((ds.Tables[2] != null) && (ds.Tables[2].Rows.Count > 0))
                                        if (!ds.Tables[2].Rows[0]["MOD_USR_EMAIL"].ToString().Equals(string.Empty))
                                            if ((sbEmailAddress.ToString().ToLower().IndexOf(ds.Tables[2].Rows[0]["MOD_USR_EMAIL"].ToString().ToLower())) < 0)
                                            {
                                                if (ds.Tables[2].Rows[0]["MOD_USR_EMAIL"].ToString().Length > 0)
                                                {
                                                    if (sbEmailAddress.Length > 0)
                                                    {
                                                        sbEmailAddress = sbEmailAddress.Append(",");
                                                        sbEmailAddress = sbEmailAddress.Append(ds.Tables[2].Rows[0]["MOD_USR_EMAIL"].ToString());
                                                    }
                                                    else
                                                        sbEmailAddress = sbEmailAddress.Append(ds.Tables[2].Rows[0]["MOD_USR_EMAIL"].ToString());
                                                }
                                            }

                                    XMLHelper _XHelper = new XMLHelper();

                                    XElement _EventEmail = new XElement("EventEmail");

                                    XElement _xa = new XElement("MDSEvent",
                                    new XElement("EVENT_ID", _EventID),
                                    new XElement("EVENT_STUS_DES", dtMDSEvent.Rows[0]["EVENT_STUS_DES"]),
                                    new XElement("WRKFLW_STUS_DES", dtMDSEvent.Rows[0]["WRKFLW_STUS_DES"]),
                                    new XElement("OLD_EVENT_STUS_DES", dtMDSEvent.Rows[0]["OLD_EVENT_STUS_DES"]),
                                    new XElement("EVENT_TITLE_TXT", dtMDSEvent.Rows[0]["EVENT_TITLE_TXT"]),
                                    new XElement("MDS_FAST_TRK_CD", dtMDSEvent.Rows[0]["MDS_FAST_TRK_CD"]),
                                    new XElement("MDS_FAST_TRK_TYPE_DES", dtMDSEvent.Rows[0]["MDS_FAST_TRK_TYPE_DES"]),
                                    new XElement("H1", dtMDSEvent.Rows[0]["H1"]),
                                    new XElement("CHARS_ID", dtMDSEvent.Rows[0]["CHARS_ID"]),
                                    new XElement("CUST_NME", dtMDSEvent.Rows[0]["CUST_NME"]),
                                    new XElement("CUST_EMAIL_ADR", dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"]),
                                    new XElement("DSGN_DOC_LOC_TXT", dtMDSEvent.Rows[0]["DSGN_DOC_LOC_TXT"]),
                                    new XElement("SHRT_DES", dtMDSEvent.Rows[0]["SHRT_DES"]),
                                    new XElement("DES_CMNT_TXT", ((ds.Tables[2] != null) && (ds.Tables[2].Rows.Count > 0)) ? ds.Tables[2].Rows[0]["CMNT_TXT"].ToString() : string.Empty),
                                    new XElement("MDS_ACTY_TYPE_DES", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_DES"]),
                                    new XElement("MDS_ACTY_TYPE_ID", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"]),
                                    new XElement("MDS_INSTL_ACTY_TYPE_DES", dtMDSEvent.Rows[0]["MDS_INSTL_ACTY_TYPE_DES"]),
                                    new XElement("FULL_CUST_DISC_CD", dtMDSEvent.Rows[0]["FULL_CUST_DISC_CD"]),
                                    new XElement("DEV_CNT", dtMDSEvent.Rows[0]["DEV_CNT"]),
                                    new XElement("DISC_NTFY_PDL_NME", dtMDSEvent.Rows[0]["DISC_NTFY_PDL_NME"]),
                                    new XElement("MDS_BRDG_NEED_CD", dtMDSEvent.Rows[0]["MDS_BRDG_NEED_CD"]),
                                    new XElement("STRT_TMST", (DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("MM-dd-yyyy")) + " @ " + (DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("t"))),
                                    new XElement("END_TMST", dtMDSEvent.Rows[0]["END_TMST"]),
                                    new XElement("EXTRA_DRTN_TME_AMT", dtMDSEvent.Rows[0]["EXTRA_DRTN_TME_AMT"]),
                                    new XElement("CREAT_BY_USER_ID", dtMDSEvent.Rows[0]["CREAT_BY_USER_ID"]),
                                    new XElement("MODFD_BY_USER_ID", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"]),
                                    new XElement("CREAT_DT", dtMDSEvent.Rows[0]["CREAT_DT"]),
                                    new XElement("CNFRC_BRDG_NBR", dtMDSEvent.Rows[0]["CNFRC_BRDG_NBR"]),
                                    new XElement("CNFRC_PIN_NBR", dtMDSEvent.Rows[0]["CNFRC_PIN_NBR"]),
                                    new XElement("OnlineMeetingURL", dtMDSEvent.Rows[0]["ONLINE_MEETING_ADR"]),
                                    new XElement("TME_SLOT_ID", dtMDSEvent.Rows[0]["TME_SLOT_ID"]),
                                    new XElement("CalenderEntryURL", sCalenderEntryURL),
                                    new XElement("ViewEventURL", sViewEventURL),
                                    new XElement("RDSGN_NBR", ((ds.Tables[7] != null) && (ds.Tables[7].Rows.Count > 0)) ? ds.Tables[7].Rows[0]["RDSN_NBR"] : string.Empty),
                                    new XElement("ModUserName", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"]),
                                    new XElement("AssignUsers", ((ds.Tables[3] != null) && (ds.Tables[3].Rows.Count > 0)) ? ds.Tables[3].Rows[0]["AssignUsers"] : string.Empty),
                                    new XElement("RevUserName", ((ds.Tables[6] != null) && (ds.Tables[6].Rows.Count > 0)) ? ds.Tables[6].Rows[0]["REV_USR_NME"] : string.Empty),
                                    new XElement("ESCL_CD", dtMDSEvent.Rows[0]["ESCL_CD"]),
                                    new XElement("ESCL_REAS_DES", dtMDSEvent.Rows[0]["ESCL_REAS_DES"]),
                                    new XElement("REQOR_USER", dtMDSEvent.Rows[0]["REQOR_USER"]),
                                    new XElement("REQOR_USER_PHN", dtMDSEvent.Rows[0]["REQOR_USER_PHN"]),
                                    new XElement("CREAT_BY_ADID", dtMDSEvent.Rows[0]["CREAT_BY_ADID"]),
                                    new XElement("REV_USR_ADID", ((ds.Tables[6] != null) && (ds.Tables[6].Rows.Count > 0)) ? ds.Tables[6].Rows[0]["REV_USR_ADID"] : string.Empty),
                                    new XElement("FT_TIME_BLOCK", dtMDSEvent.Rows[0]["TME_SLOT_ID"]),
                                    new XElement("SOWS_EVENT_ID", dtMDSEvent.Rows[0]["SOWS_EVENT_ID"])
                                    );

                                    _EventEmail.Add(_xa);

                                    if (dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim().Length > 0)
                                    {
                                        if (dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim().Length > 0)
                                        {
                                            if (sbEmailAddress.Length > 0)
                                            {
                                                sbEmailAddress = sbEmailAddress.Append(",");
                                                sbEmailAddress = sbEmailAddress.Append(dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim());
                                            }
                                            else
                                                sbEmailAddress = sbEmailAddress.Append(dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim());
                                        }
                                    }

                                    if ((ds.Tables[6] != null) && (ds.Tables[6].Rows.Count > 0))
                                    {
                                        if (ds.Tables[6].Rows[0]["REV_USR_EMAIL"].ToString().Trim().Length > 0)
                                        {
                                            if (sbEmailAddress.Length > 0)
                                            {
                                                sbEmailAddress = sbEmailAddress.Append(",");
                                                sbEmailAddress = sbEmailAddress.Append(ds.Tables[6].Rows[0]["REV_USR_EMAIL"].ToString().Trim());
                                            }
                                            else
                                                sbEmailAddress = sbEmailAddress.Append(ds.Tables[6].Rows[0]["REV_USR_EMAIL"].ToString().Trim());
                                        }
                                    }

                                    if ((dtTabs != null) && (dtTabs.Rows.Count > 0))
                                    {
                                        int _ID;
                                        string _Header;
                                        int _FSA_Event_ID;
                                        int _H5_H6 = 0;

                                        DataTable dtMDSMNSOE = new DataTable();
                                        DataTable dtMDSManagedDevices = new DataTable();
                                        DataTable dtMDSWiredTransport = new DataTable();
                                        DataTable dtMDSWirelessTransport = new DataTable();
                                        DataTable dtMDSVirtualConnPVC = new DataTable();
                                        DataTable dtMDSMisc = new DataTable();
                                        DataTable dtMDSAddr = new DataTable();
                                        DataTable dtMDSLOC = new DataTable();

                                        DataSet ds1 = new DataSet();

                                        for (int i = 0; i < dtTabs.Rows.Count; i++)
                                        {
                                            _ID = Convert.ToInt32(dtTabs.Rows[i]["TAB_SEQ_NBR"].ToString());
                                            _Header = dtTabs.Rows[i]["TAB_NME"].ToString();
                                            _FSA_Event_ID = Convert.ToInt32(dtTabs.Rows[i]["FSA_MDS_EVENT_ID"].ToString());

                                            ds1 = _objDB.GetMDSEventDataDetail(_H5_H6, _EventID, _Header, _FSA_Event_ID, _ID);

                                            XElement _EventTab = new XElement("Tab");
                                            _EventTab.SetAttributeValue("TABID", _ID);

                                            if ((ds1 != null) && (ds1.Tables.Count > 2))
                                            {
                                                dtMDSMNSOE = ds1.Tables[0];
                                                dtMDSManagedDevices = ds1.Tables[1];
                                                dtMDSWiredTransport = ds1.Tables[2];
                                                dtMDSWirelessTransport = ds1.Tables[3];
                                                dtMDSVirtualConnPVC = ds1.Tables[4];
                                                dtMDSMisc = ds1.Tables[5];
                                                dtMDSAddr = ds1.Tables[6];
                                                dtMDSLOC = ds1.Tables[7];

                                                for (int b = 0; b < dtMDSMNSOE.Rows.Count; b++)
                                                {
                                                    XElement _xb = new XElement("MDSOE",
                                                      new XElement("MNS_OE_FTN", dtMDSMNSOE.Rows[b]["MDS_MNS_FTN"]),
                                                      new XElement("MDS_SRVC_TIER_DES", dtMDSMNSOE.Rows[b]["MDS_SRVC_TIER_DES"]),
                                                      new XElement("MDS_ENTLMNT_DES", dtMDSMNSOE.Rows[b]["MDS_ENTLMNT_DES"])
                                                       );

                                                    _EventTab.Add(_xb);
                                                }

                                                for (int c = 0; c < dtMDSManagedDevices.Rows.Count; c++)
                                                {
                                                    XElement _xc = new XElement("MDSMagDevices",
                                                      new XElement("FTN_CPE", dtMDSManagedDevices.Rows[c]["FTN_CPE"]),
                                                      new XElement("DEV_NME", dtMDSManagedDevices.Rows[c]["DEV_NME"]),
                                                      new XElement("MANF_NME", dtMDSManagedDevices.Rows[c]["MANF_NME"]),
                                                      new XElement("DEV_MODEL_NME", dtMDSManagedDevices.Rows[c]["DEV_MODEL_NME"])
                                                       );

                                                    _EventTab.Add(_xc);
                                                }

                                                for (int e = 0; e < dtMDSWiredTransport.Rows.Count; e++)
                                                {
                                                    XElement _xe = new XElement("MDSWirTrans",
                                                      new XElement("TRPT_FTN", dtMDSWiredTransport.Rows[e]["ACCESS_FTN"]),
                                                      new XElement("WdTransportType", dtMDSWiredTransport.Rows[e]["WdTransportType"]),
                                                      new XElement("WdPrimaryBackup", dtMDSWiredTransport.Rows[e]["WdPrimaryBackup"]),
                                                      new XElement("FMS_CKT_ID", dtMDSWiredTransport.Rows[e]["FMS_CKT_ID"]),
                                                      new XElement("PL_NBR", dtMDSWiredTransport.Rows[e]["PL_NBR"]),
                                                      new XElement("NUA_449_ADR", dtMDSWiredTransport.Rows[e]["NUA_449_ADR"]),
                                                      new XElement("OLD_CKT_ID", dtMDSWiredTransport.Rows[e]["OLD_CKT_ID"]),
                                                      new XElement("MULTI_LINK_CKT_CD", dtMDSWiredTransport.Rows[e]["MULTI_LINK_CKT"]),
                                                      new XElement("FOC_DT", dtMDSWiredTransport.Rows[e]["FOC_DT"]),
                                                      new XElement("NEW_NUA", dtMDSWiredTransport.Rows[e]["NEW_NUA"]),
                                                      new XElement("NEW_FMS_CKT_ID", dtMDSWiredTransport.Rows[e]["NEW_FMS_CKT_ID"]),
                                                      new XElement("WdSprintManaged", dtMDSWiredTransport.Rows[e]["WdSprintManaged"]),
                                                      new XElement("WdTelcoCallout", dtMDSWiredTransport.Rows[e]["WdTelcoCallout"])
                                                      );

                                                    _EventTab.Add(_xe);
                                                }

                                                for (int f = 0; f < dtMDSWirelessTransport.Rows.Count; f++)
                                                {
                                                    XElement _xf = new XElement("MDSWirlessTran",
                                                      new XElement("WlPrimaryBackup", dtMDSWirelessTransport.Rows[f]["WlPrimaryBackup"]),
                                                      new XElement("ESN", dtMDSWirelessTransport.Rows[f]["ESN"])
                                                       );

                                                    _EventTab.Add(_xf);
                                                }

                                                for (int g = 0; g < dtMDSVirtualConnPVC.Rows.Count; g++)
                                                {
                                                    XElement _xg = new XElement("MDSPVC",
                                                      new XElement("PVC_ID", dtMDSVirtualConnPVC.Rows[g]["DLCI_VPI_CD"])
                                                       );

                                                    _EventTab.Add(_xg);
                                                }

                                                for (int h = 0; h < dtMDSMisc.Rows.Count; h++)
                                                {
                                                    XElement _xh = new XElement("MDSMISC",
                                                      new XElement("TAB_SEQ_NBR", dtMDSMisc.Rows[h]["TAB_SEQ_NBR"]),
                                                      new XElement("TAB_NME", _Header),
                                                      new XElement("SPRINT_CPE_NCR_DES", dtMDSMisc.Rows[h]["SPRINT_CPE_NCR_DES"]),
                                                      new XElement("SRVC_ASSRN_SITE_SUPP_DES", dtMDSMisc.Rows[h]["SRVC_ASSRN_SITE_SUPP_DES"]),
                                                      new XElement("CPE_DSPCH_EMAIL_ADR", dtMDSMisc.Rows[h]["CPE_DSPCH_EMAIL_ADR"]),
                                                      new XElement("CPE_DSPCH_CMNT_TXT", dtMDSMisc.Rows[h]["CPE_DSPCH_CMNT_TXT"]),
                                                      new XElement("TRPT_FTN", dtMDSWiredTransport == null ? string.Empty : dtMDSWiredTransport.Rows.Count > 0 ? dtMDSWiredTransport.Rows[0]["ACCESS_FTN"] : string.Empty),
                                                      new XElement("WIRED_DEV_TRNSPRT_REQR_CD", dtMDSMisc.Rows[h]["WIRED_DEV_TRPT_REQR_CD"]),
                                                      new XElement("WRLS_TRNSPRT_REQR_CD", dtMDSMisc.Rows[h]["WRLS_TRNSPRT_REQR_CD"]),
                                                      new XElement("VRTL_CNCTN_CD", dtMDSMisc.Rows[h]["VRTL_CNCTN_CD"]),
                                                      new XElement("INSTL_SITE_POC_NME", dtMDSMisc.Rows[h]["INSTL_SITE_POC_NME"]),
                                                      new XElement("INSTL_SITE_POC_PHN_NBR", dtMDSMisc.Rows[h]["INSTL_SITE_POC_PHN_NBR"]),
                                                      new XElement("INSTL_SITE_POC_CELL_PHN_NBR", dtMDSMisc.Rows[h]["INSTL_SITE_POC_CELL_PHN_NBR"]),
                                                      new XElement("SRVC_ASSRN_POC_NME", dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_NME"]),
                                                      new XElement("SRVC_ASSRN_POC_PHN_NBR", dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_PHN_NBR"]),
                                                      new XElement("SRVC_ASSRN_POC_CELL_NBR", dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_CELL_NBR"]),
                                                      new XElement("SA_POC_HR_NME", dtMDSMisc.Rows[h]["SA_POC_HR_NME"]),
                                                      new XElement("TME_ZONE_ID", dtMDSMisc.Rows[h]["TME_ZONE_ID"])
                                                      );

                                                    _EventTab.Add(_xh);
                                                }

                                                for (int l = 0; l < dtMDSAddr.Rows.Count; l++)
                                                {
                                                    XElement _xl = new XElement("MDSAddr",
                                                      new XElement("ADR", dtMDSAddr.Rows[l]["ADR"]),
                                                      new XElement("FLR_BLDG_NME", dtMDSAddr.Rows[l]["FLR_BLDG_NME"]),
                                                      new XElement("CTY_NME", dtMDSAddr.Rows[l]["CTY_NME"]),
                                                      new XElement("STT_PRVN_NME", dtMDSAddr.Rows[l]["STT_PRVN_NME"]),
                                                      new XElement("CTRY_RGN_NME", dtMDSAddr.Rows[l]["CTRY_RGN_NME"]),
                                                      new XElement("ZIP_CD", dtMDSAddr.Rows[l]["ZIP_CD"]),
                                                      new XElement("US_INTL_ID", dtMDSAddr.Rows[l]["US_INTL_ID"])
                                                       );

                                                    _EventTab.Add(_xl);
                                                }

                                                for (int d = 0; d < dtMDSLOC.Rows.Count; d++)
                                                {
                                                    XElement _xd = new XElement("MDSLOC",
                                                      new XElement("MDS_LOC_TYPE_DES", dtMDSLOC.Rows[d]["MDS_LOC_TYPE_DES"]),
                                                      new XElement("MDS_LOC_CAT_DES", dtMDSLOC.Rows[d]["MDS_LOC_CAT_DES"]),
                                                      new XElement("MDS_LOC_NBR", dtMDSLOC.Rows[d]["MDS_LOC_NBR"]),
                                                      new XElement("MDS_LOC_RAS_DT", dtMDSLOC.Rows[d]["MDS_LOC_RAS_DT"])
                                                       );

                                                    _EventTab.Add(_xd);
                                                }

                                                _EventEmail.Add(_EventTab);
                                            }
                                        }
                                    }

                                    string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

                                    string _EmailBody = string.Empty;

                                    _objDB.InsertReworkEmail(_EventID, _Status, dtMDSEvent.Rows[0]["CREAT_BY_EMAIL"].ToString(), sbEmailAddress.ToString(), _Subject, _EventEmail.ToString());

                                    _EmailBody = _XHelper.transformXMLtoHTML(_EventEmail.ToString(), "ReworkEmail", 0);

                                    if (_EventEmail != null)
                                    {
                                        if (!(SendEmail(dtMDSEvent.Rows[0]["CREAT_BY_EMAIL"].ToString(), _Subject, _EmailBody, _EventID.ToString(), sbEmailAddress.ToString())))
                                        {
                                            _Status = 12;
                                        }
                                    }
                                }

                                if (_objDB.UpdateReworkEmail(_EventID, _Status) == 0)
                                {
                                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for Event_ID: " + _EventID, string.Empty);
                                }
                            }
                            else if ((_EventTypeID == 5) && (_IsNewMDS == 1))
                            {
                                _Status = 11;
                                XElement _EventEmail = new XElement("EventEmail");
                                bool bSensitiveCust = false;

                                ds = _objDB.GetMDSEventData(_EventID);

                                if (ds != null)
                                {
                                    dtMDSEvent = ds.Tables[0];
                                    DataTable dtMDSDisc = ds.Tables[8];
                                    DataTable dtMDSODIEDev = ds.Tables[9];
                                    DataTable dtMDSDevCmplt = ds.Tables[10];
                                    DataTable dtMDSCPEDev = ds.Tables[11];
                                    DataTable dtMDSCustTrptTbl = ds.Tables[12];
                                    DataTable dtMDSSlnkWrdTrptTbl = ds.Tables[13];
                                    DataTable dtMDSSrvcTbl = ds.Tables[14];
                                    DataTable dtMDSDevMgmtSrvc = ds.Tables[15];
                                    DataTable dtMDSWrlsTrptTbl = ds.Tables[16];
                                    DataTable dtMDSPortBndwd = ds.Tables[18];

                                    if ((dtMDSEvent != null) && (dtMDSEvent.Rows.Count > 0))
                                    {
                                        sbEmailAddress.Clear();
                                        string _Subject = "(VISIBLE) " + dtMDSEvent.Rows[0]["EVENT_TITLE_TXT"].ToString() + " EventID: " + _EventID;

                                        if ((ds.Tables[3] != null) && (ds.Tables[3].Rows.Count > 0))
                                            if (!ds.Tables[3].Rows[0]["AssignEmails"].ToString().Equals(string.Empty))
                                                if ((sbEmailAddress.ToString().ToLower().IndexOf(ds.Tables[3].Rows[0]["AssignEmails"].ToString().ToLower())) < 0)
                                                {
                                                    if (ds.Tables[3].Rows[0]["AssignEmails"].ToString().Length > 0)
                                                    {
                                                        if (sbEmailAddress.Length > 0)
                                                        {
                                                            sbEmailAddress = sbEmailAddress.Append(",");
                                                            sbEmailAddress = sbEmailAddress.Append(ds.Tables[3].Rows[0]["AssignEmails"].ToString());
                                                        }
                                                        else
                                                            sbEmailAddress = sbEmailAddress.Append(ds.Tables[3].Rows[0]["AssignEmails"].ToString());
                                                    }
                                                }

                                        if ((ds.Tables[1] != null) && (ds.Tables[1].Rows.Count > 0))
                                            if (!ds.Tables[1].Rows[0]["MOD_USR_EMAIL"].ToString().Equals(string.Empty))
                                                if ((sbEmailAddress.ToString().ToLower().IndexOf(ds.Tables[1].Rows[0]["MOD_USR_EMAIL"].ToString().ToLower())) < 0)
                                                {
                                                    if (ds.Tables[1].Rows[0]["MOD_USR_EMAIL"].ToString().Length > 0)
                                                    {
                                                        if (sbEmailAddress.Length > 0)
                                                        {
                                                            sbEmailAddress = sbEmailAddress.Append(",");
                                                            sbEmailAddress = sbEmailAddress.Append(ds.Tables[1].Rows[0]["MOD_USR_EMAIL"].ToString());
                                                        }
                                                        else
                                                            sbEmailAddress = sbEmailAddress.Append(ds.Tables[1].Rows[0]["MOD_USR_EMAIL"].ToString());
                                                    }
                                                }

                                        XMLHelper _XHelper = new XMLHelper();

                                        bSensitiveCust = (byte.Parse(dtMDSEvent.Rows[0]["CSG_LVL_ID"].ToString()) > 0);

                                        XElement _xa = new XElement("MDSEvent",
                                                    new XElement("EVENT_ID", _EventID),
                                                    new XElement("EVENT_STUS_DES", dtMDSEvent.Rows[0]["EVENT_STUS_DES"].ToString()),
                                                    new XElement("WRKFLW_STUS_DES", dtMDSEvent.Rows[0]["WRKFLW_STUS_DES"].ToString()),
                                                    new XElement("OLD_EVENT_STUS_DES", dtMDSEvent.Rows[0]["OLD_EVENT_STUS_DES"].ToString()),
                                                    new XElement("EVENT_TITLE_TXT", dtMDSEvent.Rows[0]["EVENT_TITLE_TXT"].ToString()),
                                                    new XElement("MDS_FAST_TRK_CD", dtMDSEvent.Rows[0]["MDS_FAST_TRK_CD"].ToString()),
                                                    new XElement("MDS_FAST_TRK_TYPE_DES", dtMDSEvent.Rows[0]["MDS_FAST_TRK_TYPE_DES"].ToString()),
                                                    new XElement("H1", dtMDSEvent.Rows[0]["H1"].ToString()),
                                                    new XElement("CHARS_ID", dtMDSEvent.Rows[0]["CHARS_ID"].ToString()),
                                                    new XElement("CUST_NME", bSensitiveCust ? CustomerMaskedData.customerName : dtMDSEvent.Rows[0]["CUST_NME"].ToString()),
                                                    new XElement("CUST_EMAIL_ADR", dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString()),
                                                    new XElement("DSGN_DOC_LOC_TXT", dtMDSEvent.Rows[0]["CUST_SOW_LOC_TXT"].ToString()),
                                                    new XElement("SHRT_DES", dtMDSEvent.Rows[0]["SHRT_DES"].ToString()),
                                                    new XElement("DES_CMNT_TXT", dtMDSEvent.Rows[0]["CMNT_TXT"].ToString()),
                                                    new XElement("MDS_ACTY_TYPE_DES", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_DES"].ToString()),
                                                    new XElement("MDS_ACTY_TYPE_ID", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"].ToString()),
                                                    new XElement("DISC_MGMT_CD", (dtMDSEvent.Rows[0]["DISC_MGMT_CD"].ToString().Length <= 0) ? "" : (dtMDSEvent.Rows[0]["DISC_MGMT_CD"].ToString().Equals("Y") ? "Yes" : "No")),
                                                    new XElement("FULL_CUST_DISC_CD", dtMDSEvent.Rows[0]["FULL_CUST_DISC_CD"].ToString()),
                                                    new XElement("FULL_CUST_DISC_REAS_TXT", (dtMDSEvent.Rows[0]["FULL_CUST_DISC_REAS_TXT"].ToString() ?? "")),
                                                    new XElement("DEV_CNT", ((ds.Tables[12] == null) ? 0 : ds.Tables[12].Rows.Count)),
                                                    new XElement("DISC_NTFY_PDL_NME", dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString()),
                                                    new XElement("MDS_BRDG_NEED_CD", dtMDSEvent.Rows[0]["MDS_BRDG_NEED_CD"].ToString()),
                                                    new XElement("STRT_TMST", DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("MM-dd-yyyy") + " @ " + DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("t")),
                                                    new XElement("END_TMST", DateTime.Parse(dtMDSEvent.Rows[0]["END_TMST"].ToString()).ToString("MM-dd-yyyy") + " @ " + DateTime.Parse(dtMDSEvent.Rows[0]["END_TMST"].ToString()).ToString("t")),
                                                    new XElement("EXTRA_DRTN_TME_AMT", dtMDSEvent.Rows[0]["EXTRA_DRTN_TME_AMT"].ToString()),
                                                    new XElement("CREAT_BY_USER_ID", dtMDSEvent.Rows[0]["CREAT_BY_USER_ID"].ToString()),
                                                    new XElement("MODFD_BY_USER_ID", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"].ToString()),
                                                    new XElement("CREAT_DT", DateTime.Parse(dtMDSEvent.Rows[0]["CREAT_DT"].ToString()).ToString("MM-dd-yyyy") + " @ " + DateTime.Parse(dtMDSEvent.Rows[0]["CREAT_DT"].ToString()).ToString("t")),
                                                    new XElement("CNFRC_BRDG_NBR", dtMDSEvent.Rows[0]["CNFRC_BRDG_NBR"].ToString()),
                                                    new XElement("CNFRC_PIN_NBR", dtMDSEvent.Rows[0]["CNFRC_PIN_NBR"].ToString()),
                                                    new XElement("OnlineMeetingURL", dtMDSEvent.Rows[0]["ONLINE_MEETING_ADR"].ToString()),
                                                    new XElement("TME_SLOT_ID", dtMDSEvent.Rows[0]["TME_SLOT_ID"].ToString()),
                                                    new XElement("CalenderEntryURL", sCalenderEntryURL),
                                                    new XElement("ViewEventURL", sViewEventURL),
                                                    new XElement("ChatURL", sChatURL),
                                                    new XElement("RDSGN_NBR", (ds.Tables[6] == null) ? "" : ((ds.Tables[6].Rows[0][0] == null) ? "" : ds.Tables[6].Rows[0][0].ToString())),
                                                    new XElement("ModUserName", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"].ToString()),
                                                    new XElement("AssignUsers", (ds.Tables[2] == null) ? "" : ((ds.Tables[2].Rows[0][0] == null) ? "" : ds.Tables[2].Rows[0][0].ToString())),
                                                    new XElement("AssignPhoneNumbers", (ds.Tables[7] == null) ? "" : ((ds.Tables[7].Rows[0][0] == null) ? "" : ds.Tables[7].Rows[0][0].ToString())),
                                                    new XElement("RevUserName", ((ds.Tables[5] != null) && (ds.Tables[5].Rows.Count > 0)) ? ds.Tables[5].Rows[0]["REV_USR_NME"] : string.Empty),
                                                    new XElement("REV_PHN_NBR", ((ds.Tables[5] != null) && (ds.Tables[5].Rows.Count > 0)) ? ds.Tables[5].Rows[0]["REV_PHN_NBR"] : string.Empty),
                                                    new XElement("ESCL_CD", dtMDSEvent.Rows[0]["ESCL_CD"].ToString()),
                                                    new XElement("ESCL_REAS_DES", dtMDSEvent.Rows[0]["ESCL_REAS_DES"].ToString()),
                                                    new XElement("REQOR_USER", dtMDSEvent.Rows[0]["REQOR_USER"].ToString()),
                                                    new XElement("REQOR_USER_PHN", dtMDSEvent.Rows[0]["REQOR_USER_PHN"].ToString()),
                                                    new XElement("FT_TIME_BLOCK", string.Empty),
                                                    new XElement("SOWS_EVENT_ID", string.Empty),
                                                    new XElement("REQOR_USER_EMAIL", dtMDSEvent.Rows[0]["REQOR_USER_EMAIL"].ToString()),
                                                    new XElement("SHIP_TRK_REFR_NBR", dtMDSEvent.Rows[0]["SHIP_TRK_REFR_NBR"].ToString()),
                                                    new XElement("SHIP_CXR_NME", string.Empty),
                                                    new XElement("DEV_SERIAL_NBR", string.Empty),
                                                    new XElement("SPRINT_CPE_NCR_DES", string.Empty),
                                                    new XElement("CPE_DSPCH_EMAIL_ADR", string.Empty),
                                                    new XElement("CPE_DSPCH_CMNT_TXT", string.Empty),
                                                    new XElement("TRPT_FTN", (ds.Tables[17] == null) ? "" : ((ds.Tables[17].Rows[0][0] == null) ? "" : ds.Tables[17].Rows[0][0].ToString())),
                                                    new XElement("INSTL_SITE_POC_NME", bSensitiveCust ? CustomerMaskedData.installSitePOC : dtMDSEvent.Rows[0]["INSTL_SITE_POC_NME"].ToString()),
                                                    new XElement("INSTL_SITE_POC_PHN_NBR", (bSensitiveCust ? CustomerMaskedData.installSitePOCPhone : String.Format("{0} - {1}", dtMDSEvent.Rows[0]["INSTL_SITE_POC_INTL_PHN_CD"].ToString(), dtMDSEvent.Rows[0]["INSTL_SITE_POC_PHN_NBR"].ToString()))),
                                                    new XElement("INSTL_SITE_POC_CELL_PHN_NBR", (bSensitiveCust ? CustomerMaskedData.installSitePOCCell : String.Format("{0} - {1}", dtMDSEvent.Rows[0]["INSTL_SITE_POC_INTL_CELL_PHN_CD"].ToString(), dtMDSEvent.Rows[0]["INSTL_SITE_POC_CELL_PHN_NBR"].ToString()))),
                                                    new XElement("SRVC_ASSRN_POC_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOC : dtMDSEvent.Rows[0]["SRVC_ASSRN_POC_NME"].ToString()),
                                                    new XElement("SRVC_ASSRN_POC_PHN_NBR", (bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCPhone : String.Format("{0} - {1}", dtMDSEvent.Rows[0]["SRVC_ASSRN_POC_INTL_PHN_CD"].ToString(), dtMDSEvent.Rows[0]["SRVC_ASSRN_POC_PHN_NBR"].ToString()))),
                                                    new XElement("SRVC_ASSRN_POC_CELL_NBR", (bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCCell : String.Format("{0} - {1}", dtMDSEvent.Rows[0]["SRVC_ASSRN_POC_INTL_CELL_PHN_CD"].ToString(), dtMDSEvent.Rows[0]["SRVC_ASSRN_POC_CELL_PHN_NBR"].ToString()))),
                                                    new XElement("SA_POC_HR_NME", dtMDSEvent.Rows[0]["SRVC_AVLBLTY_HRS"].ToString()),
                                                    new XElement("TME_ZONE_ID", dtMDSEvent.Rows[0]["SRVC_TME_ZN_CD"].ToString()),
                                                    new XElement("ADR", bSensitiveCust ? CustomerMaskedData.address : dtMDSEvent.Rows[0]["STREET_ADR"].ToString()),
                                                    new XElement("FLR_BLDG_NME", bSensitiveCust ? CustomerMaskedData.floor : dtMDSEvent.Rows[0]["FLR_BLDG_NME"].ToString()),
                                                    new XElement("CTY_NME", bSensitiveCust ? CustomerMaskedData.city : dtMDSEvent.Rows[0]["CTY_NME"].ToString()),
                                                    new XElement("STT_PRVN_NME", bSensitiveCust ? CustomerMaskedData.state : dtMDSEvent.Rows[0]["STT_PRVN_NME"].ToString()),
                                                    new XElement("CTRY_RGN_NME", bSensitiveCust ? CustomerMaskedData.country : dtMDSEvent.Rows[0]["CTRY_RGN_NME"].ToString()),
                                                    new XElement("ZIP_CD", bSensitiveCust ? CustomerMaskedData.zipCode : dtMDSEvent.Rows[0]["ZIP_CD"].ToString()),
                                                    new XElement("US_INTL_ID", dtMDSEvent.Rows[0]["US_INTL_CD"].ToString().Equals("D") ? "US" : "International"),
                                                    new XElement("CPE_ORDR_CD", ""),
                                                    new XElement("MNS_ORDR_CD", "")
                                                    );

                                        _EventEmail.Add(_xa);

                                        if (dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim().Length > 0)
                                        {
                                            if (dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim().Length > 0)
                                            {
                                                if (sbEmailAddress.Length > 0)
                                                {
                                                    sbEmailAddress = sbEmailAddress.Append(",");
                                                    sbEmailAddress = sbEmailAddress.Append(dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim());
                                                }
                                                else
                                                    sbEmailAddress = sbEmailAddress.Append(dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim());
                                            }
                                        }

                                        if ((ds.Tables[5] != null) && (ds.Tables[5].Rows.Count > 0))
                                        {
                                            if (ds.Tables[5].Rows[0]["REV_USR_EMAIL"].ToString().Trim().Length > 0)
                                            {
                                                if (sbEmailAddress.Length > 0)
                                                {
                                                    sbEmailAddress = sbEmailAddress.Append(",");
                                                    sbEmailAddress = sbEmailAddress.Append(ds.Tables[5].Rows[0]["REV_USR_EMAIL"].ToString().Trim());
                                                }
                                                else
                                                    sbEmailAddress = sbEmailAddress.Append(ds.Tables[5].Rows[0]["REV_USR_EMAIL"].ToString().Trim());
                                            }
                                        }

                                        if ((dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"].ToString() == "3") && (dtMDSDisc != null) && (dtMDSDisc.Rows.Count > 0))
                                        {
                                            foreach (DataRow drMDSDisc in dtMDSDisc.Rows)
                                            {
                                                XElement _xmdo = new XElement("MDSDiscODIEDev",
                                                              new XElement("ODIEDevNme", drMDSDisc["ODIE_DEV_NME"]),
                                                              new XElement("H5H6", drMDSDisc["H5_H6"]),
                                                              new XElement("SiteID", drMDSDisc["SITE_ID"]),
                                                              new XElement("DeviceID", drMDSDisc["DEVICE_ID"]),
                                                              new XElement("DevManfName", drMDSDisc["MANF_NME"]),
                                                              new XElement("DevModelName", drMDSDisc["DEV_MODEL_NME"]),
                                                              new XElement("SerialNbr", drMDSDisc["SERIAL_NBR"]),
                                                              new XElement("ThirdPartyCtrct", drMDSDisc["THRD_PARTY_CTRCT"])
                                                              );
                                                _EventEmail.Add(_xmdo);
                                            }
                                        }
                                        else if (dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"].ToString() != "3")
                                        {
                                            foreach (DataRow drODIE in dtMDSODIEDev.Rows)
                                            {
                                                XElement _xmod = new XElement("MDSODIEDev",
                                                          new XElement("RdsnNbr", drODIE["RDSN_NBR"]),
                                                          new XElement("RdsnExpDt", drODIE["RDSN_EXP_DT"]),
                                                          new XElement("ODIEDevNme", drODIE["ODIE_DEV_NME"]),
                                                          new XElement("DevManfName", drODIE["MANF_NME"]),
                                                          new XElement("DevModelName", drODIE["DEV_MODEL_NME"]),
                                                          new XElement("FrwlSctyCD", drODIE["FRWL_PROD_CD"]),
                                                          new XElement("FastTrkCD", drODIE["FAST_TRK_CD"]),
                                                          new XElement("OptOutCD", drODIE["OPTOUT_CD"]),
                                                          new XElement("IntlCD", drODIE["INTL_CTRY_CD"]),
                                                          new XElement("DediTN", drODIE["PHN_NBR"]),
                                                          new XElement("WOOBCD", drODIE["WOOB_CD"]),
                                                          new XElement("EntityMaint", drODIE["SRVC_ASSRN_SITE_SUPP_DES"])
                                                          );
                                                _EventEmail.Add(_xmod);
                                            }

                                            foreach (DataRow drCmplt in dtMDSDevCmplt.Rows)
                                            {
                                                XElement _xmst = new XElement("MDSDevCmpltTbl",
                                                          new XElement("ODIEDevNme", drCmplt["ODIE_DEV_NME"]),
                                                          new XElement("ODIEH6", drCmplt["H6"]),
                                                          new XElement("ActvCmpltCd", drCmplt["CMPLTD_CD"])
                                                          );
                                                _EventEmail.Add(_xmst);
                                            }

                                            foreach (DataRow drCPE in dtMDSCPEDev.Rows)
                                            {
                                                XElement _xmcd = new XElement("MDSCPEDev",
                                                          new XElement("CPEOrdrID", drCPE["CPE_ORDR_ID"]),
                                                          new XElement("CPEOrdrNbr", drCPE["CPE_ORDR_NBR"]),
                                                          new XElement("DeviceID", drCPE["DEVICE_ID"]),
                                                          new XElement("AssocH6", drCPE["ASSOC_H6"]),
                                                          new XElement("MntVndrPO", drCPE["MAINT_VNDR_PO"]),
                                                          new XElement("CCD", drCPE["CCD"]),
                                                          new XElement("CmpntStus", drCPE["ORDR_STUS"]),
                                                          new XElement("RcptStus", drCPE["RCPT_STUS"]),
                                                          new XElement("OdieDevNme", drCPE["ODIE_DEV_NME"]),
                                                          new XElement("CtrctType", "")
                                                          );
                                                _EventEmail.Add(_xmcd);
                                            }

                                            foreach (DataRow drCust in dtMDSCustTrptTbl.Rows)
                                            {
                                                XElement _xmct = new XElement("MDSCustTrptInfo",
                                                          new XElement("MDSTrptType", drCust["MDS_TRNSPRT_TYPE"]),
                                                          new XElement("PrimBkpCD", (drCust["PRIM_BKUP_CD"].Equals("P") ? "Primary" : (drCust["PRIM_BKUP_CD"].Equals("B") ? "Backup" : string.Empty))),
                                                          new XElement("VndrPrvdrTrptCktID", drCust["VNDR_PRVDR_TRPT_CKT_ID"]),
                                                          new XElement("VndrPrvdrNme", drCust["VNDR_PRVDR_NME"]),
                                                          new XElement("WrlsCD", drCust["IS_WIRELESS_CD"]),
                                                          new XElement("BwdEsn", drCust["BW_ESN_MEID"]),
                                                          new XElement("SCANbr", drCust["SCA_NBR"]),
                                                          new XElement("IPAdr", drCust["IP_ADR"]),
                                                          new XElement("SubnetMask", drCust["SUBNET_MASK_ADR"]),
                                                          new XElement("NhopGwy", drCust["NXTHOP_GTWY_ADR"]),
                                                          new XElement("MngdCD", (drCust["SPRINT_MNGD_CD"].Equals("S") ? "Sprint" : (drCust["SPRINT_MNGD_CD"].Equals("C") ? "Customer" : string.Empty))),
                                                          new XElement("ODIEDevNme", drCust["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmct);
                                            }

                                            foreach (DataRow drWrd in dtMDSSlnkWrdTrptTbl.Rows)
                                            {
                                                XElement _xmwt = new XElement("MDSSlnkWrdTrptInfo",
                                                          new XElement("MDSTrptType", drWrd["MDS_TRNSPRT_TYPE"]),
                                                          new XElement("PrimBkpCD", (drWrd["PRIM_BKUP_CD"].Equals("P") ? "Primary" : (drWrd["PRIM_BKUP_CD"].Equals("B") ? "Backup" : string.Empty))),
                                                          new XElement("VndrPrvdrTrptCktID", drWrd["VNDR_PRVDR_TRPT_CKT_ID"]),
                                                          new XElement("VndrPrvdrNme", drWrd["VNDR_PRVDR_NME"]),
                                                          new XElement("Telco", ""),
                                                          new XElement("BdwChnlNme", drWrd["BDWD_CHNL_NME"]),
                                                          new XElement("PLNbr", drWrd["PL_NBR"]),
                                                          new XElement("IPNUA", drWrd["IP_NUA_ADR"]),
                                                          new XElement("OldCktID", drWrd["OLD_CKT_ID"]),
                                                          new XElement("MultiLnkCktCD", drWrd["MULTI_LINK_CKT_CD"]),
                                                          new XElement("DedAccsCD", drWrd["DED_ACCS_CD"]),
                                                          new XElement("FMSNbr", drWrd["FMS_NBR"]),
                                                          new XElement("DLCIVPIVCI", drWrd["DLCI_VPI_VCI"]),
                                                          new XElement("VLANNbr", drWrd["VLAN_NBR"]),
                                                          new XElement("SPANUA", drWrd["SPA_NUA"]),
                                                          new XElement("MngdCD", (drWrd["SPRINT_MNGD_CD"].Equals("S") ? "Sprint" : (drWrd["SPRINT_MNGD_CD"].Equals("C") ? "Customer" : string.Empty))),
                                                          new XElement("ODIEDevNme", drWrd["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmwt);
                                            }

                                            foreach (DataRow drSrvc in dtMDSSrvcTbl.Rows)
                                            {
                                                XElement _xmst = new XElement("MDSSrvcTbl",
                                                          new XElement("Mach5H6SrvcID", drSrvc["MACH5_SRVC_ORDR_ID"]),
                                                          new XElement("SrvcType", drSrvc["SRVC_TYPE_DES"]),
                                                          new XElement("ThirdPartyVndr", drSrvc["THRD_PARTY_VNDR_DES"]),
                                                          new XElement("ThirdParty", drSrvc["THRD_PARTY_ID"]),
                                                          new XElement("ThirdPartySrvcLvl", drSrvc["THRD_PARTY_SRVC_LVL_DES"]),
                                                          new XElement("ActvnDt", drSrvc["ACTV_DT"]),
                                                          new XElement("Comments", drSrvc["CMNT_TXT"]),
                                                          new XElement("EmailFlg", ""),
                                                          new XElement("ODIEDevNme", drSrvc["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmst);
                                            }

                                            foreach (DataRow drDevMgmt in dtMDSDevMgmtSrvc.Rows)
                                            {
                                                XElement _xmst = new XElement("MDSDevMgmtTbl",
                                                          new XElement("MNSOrdrNbr", drDevMgmt["MNS_ORDR_NBR"]),
                                                          new XElement("DeviceID", drDevMgmt["DEVICE_ID"]),
                                                          new XElement("CmpntID", drDevMgmt["M5_ORDR_CMPNT_ID"]),
                                                          new XElement("CmpntStus", drDevMgmt["CMPNT_STUS"]),
                                                          new XElement("CmpntType", drDevMgmt["CMPNT_TYPE_CD"]),
                                                          new XElement("CmpntNme", drDevMgmt["CMPNT_NME"]),
                                                          new XElement("RltdCmpntID", drDevMgmt["M5_ORDR_CMPNT_ID"]),
                                                          new XElement("ServiceTier", drDevMgmt["MDS_SRVC_TIER_DES"]),
                                                          new XElement("ODIEDevNme", drDevMgmt["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmst);
                                            }

                                            foreach (DataRow drWrls in dtMDSWrlsTrptTbl.Rows)
                                            {
                                                XElement _xmwl = new XElement("MDSWrlsTrptTbl",
                                                          new XElement("WrlsTypeCD", drWrls["WRLS_TYPE_CD"]),
                                                          new XElement("PrimBkpCD", (drWrls["PRIM_BKUP_CD"].Equals("P") ? "Primary" : (drWrls["PRIM_BKUP_CD"].Equals("B") ? "Backup" : string.Empty))),
                                                          new XElement("ESN", drWrls["ESN_MAC_ID"]),
                                                          new XElement("BillAcctNme", drWrls["BILL_ACCT_NME"]),
                                                          new XElement("BillAcctNbr", drWrls["BILL_ACCT_NBR"]),
                                                          new XElement("AcctPin", drWrls["ACCT_PIN"]),
                                                          new XElement("SalsCD", drWrls["SALS_CD"]),
                                                          new XElement("SOC", drWrls["SOC"]),
                                                          new XElement("StaticIPAdr", drWrls["STATIC_IP_ADR"]),
                                                          new XElement("IMEI", drWrls["IMEI"]),
                                                          new XElement("UICCIDNbr", drWrls["UICC_ID_NBR"]),
                                                          new XElement("PricePln", drWrls["PRICE_PLN"]),
                                                          new XElement("ODIEDevNme", drWrls["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmwl);
                                            }

                                            foreach (DataRow drMDSPort in dtMDSPortBndwd.Rows)
                                            {
                                                XElement _xmpb = new XElement("MDSPortBndwd",
                                                          new XElement("M5OrdrNbr", drMDSPort["M5_ORDR_NBR"]),
                                                          new XElement("M5CmpntID", drMDSPort["M5_ORDR_CMPNT_ID"]),
                                                          new XElement("NUA", drMDSPort["NUA"]),
                                                          new XElement("PortBndwd", drMDSPort["PORT_BNDWD"]),
                                                          new XElement("ODIEDevNme", drMDSPort["ODIE_DEV_NME"])
                                                          );
                                                _EventEmail.Add(_xmpb);
                                            }
                                        }

                                        string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

                                        string _EmailBody = string.Empty;

                                        _objDB.InsertReworkEmail(_EventID, _Status, dtMDSEvent.Rows[0]["CREAT_BY_EMAIL"].ToString(), sbEmailAddress.ToString(), _Subject, _EventEmail.ToString());

                                        _EmailBody = _XHelper.transformXMLtoHTML(_EventEmail.ToString(), "MDSEventEmail", 0);


                                        if (_EventEmail != null)
                                        {
                                            if (!(SendEmail(dtMDSEvent.Rows[0]["CREAT_BY_EMAIL"].ToString(), _Subject, _EmailBody, _EventID.ToString(), sbEmailAddress.ToString())))
                                                _Status = 12;
                                        }
                                    }
                                    else
                                    {
                                        CL.WriteLogFile(CL.Event.GenericMessage, CL.MsgType.error, 0, string.Empty, "dtMDSEvent is NULL or with no Rows, EventID : " + _EventID.ToString(), string.Empty);
                                        _Status = 12;
                                    }
                                }

                                if (_objDB.UpdateReworkEmail(_EventID, _Status) == 0)
                                {
                                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for Event_ID: " + _EventID, string.Empty);
                                }
                            }
                            else
                            {
                                if (_objDB.updateCANDReworkEvent(_EventID, _EventTypeID) == 0)
                                {
                                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the visible status from rework status for CAND Event_ID: " + _EventID, string.Empty);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error while sending the nightly rework email for EventID : " + _EventID.ToString() + "; " + ex.Message + " ; " + ex.StackTrace, string.Empty);
                        }
                    }
                }
                else
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send nightly rework Emails", string.Empty);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error while sending the nightly rework emails : " + ex.Message + " ; " + ex.StackTrace, string.Empty);
            }
        }

        private void CreateAndSendGomPLCompleteEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            DataSet ds = new DataSet();
            ds = _SenderDB.GetGomPLCompData(_OrderID);

            if (ds != null && (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0))
            {
                if (ds.Tables[0].Rows[0]["EMAIL_CC"].ToString() != string.Empty)
                    _EmailCC = ds.Tables[0].Rows[0]["PAC"] + "," + ds.Tables[0].Rows[0]["SIS"] + "," + ConfigurationManager.AppSettings["GomPLCompPDLcc"].ToString() + "," + ds.Tables[0].Rows[0]["EMAIL_CC"].ToString();
                else
                    _EmailCC = ds.Tables[0].Rows[0]["PAC"] + "," + ds.Tables[0].Rows[0]["SIS"] + "," + ConfigurationManager.AppSettings["GomPLCompPDLcc"].ToString();

                _EmailAddress = ConfigurationManager.AppSettings["GomPLCompPDL"].ToString();
                _Subject = "GOM Private Line Complete - FTN: " + ds.Tables[0].Rows[0]["FTN"].ToString() +
                    " PL/SEQ: " + ds.Tables[0].Rows[0]["PLN_NME"].ToString() + "/" + ds.Tables[0].Rows[0]["PLN_SEQ_NBR"].ToString();
                XMLHelper _XHelper = new XMLHelper();
                XElement _Email = new XElement("GomPLComp",
                                                   new XElement("FTN", ds.Tables[0].Rows[0]["FTN"]),
                                                   new XElement("PORT_ASMT_DES", ConcatenateValues(ds.Tables[0], "PORT_ASMT_DES")),
                                                   new XElement("TOC_NME", ConcatenateValues(ds.Tables[0], "TOC_NME")),
                                                   new XElement("FMS_CKT_ID", ConcatenateValues(ds.Tables[0], "FMS_CKT_ID")),
                                                   new XElement("NUA_ADR", ConcatenateValues(ds.Tables[0], "NUA_ADR")),
                                                   new XElement("PLN_NME", ConcatenateValues(ds.Tables[0], "PLN_NME")),
                                                   new XElement("PLN_SEQ_NBR", ConcatenateValues(ds.Tables[0], "PLN_SEQ_NBR"))
                                                    );

                try
                {
                    _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
                }
                catch
                {
                    this.UpdateCommonEmailRequestWithError(_SenderDB, _Email_Req_ID, Convert.ToInt32(_OrderID), _Status, MsgClass.Orders);
                    return;
                }


                if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
                {
                    _Status = 12;
                }

                if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
                }
            }
        }

        private void CreateAndSendGomIPLIPASREmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            int _OrderID = Convert.ToInt32(i["ORDR_ID"].ToString());
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            DataTable dt = new DataTable();
            dt = _SenderDB.GetGomIPLIPASRData(_OrderID);

            if (dt != null && dt.Rows.Count > 0)
            {
                _EmailAddress = dt.Rows[0]["EmailTo"].ToString();
                _Subject = dt.Rows[0]["EmailSubject"].ToString();
                _EmailBody = dt.Rows[0]["EmailBody"].ToString();
                _EmailCC = dt.Rows[0]["EmailCC"].ToString();
                XMLHelper _XHelper = new XMLHelper();
                XElement _Email = new XElement("GomIPLIPASR",
                                                   new XElement("notes", _EmailBody)
                                                   );



                try
                {
                    _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
                }
                catch
                {
                    this.UpdateCommonEmailRequestWithError(_SenderDB, _Email_Req_ID, _OrderID, _Status, MsgClass.Orders);
                    return;
                }

                if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID.ToString(), _EmailCC)))
                {
                    _Status = 12;
                }

                if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
                }
            }
        }


        private void CreateAndSendNCCOOrders(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            _EmailAddress = i["EMAIL_ADR"].ToString();
            _Subject = "NCCO Order ID:" + i["ORDR_ID"].ToString() + "is submitted and ready to be reviewed.";
            XMLHelper _XHelper = new XMLHelper();
            XElement _Email = new XElement("NCCOOrderEmail",
                                               new XElement("Order_ID", i["ORDR_ID"]),
                                               new XElement("Product_Type", i["PROD_TYPE_DES"]),
                                               new XElement("Order_Type", i["ORDR_TYPE_DESC"]),
                                               new XElement("Vendor_Name", i["VNDR_NME"]),
                                               new XElement("CWD", i["CWD_DT"]),
                                               new XElement("CCD", i["CCS_DT"]),
                                               new XElement("H5_Account_Number", i["H5_ACCT_NBR"]),
                                               new XElement("Site_City", i["SITE_CITY_NME"]),
                                               new XElement("Site_Country", i["CTRY_NME"]),
                                               new XElement("SOL", i["SOL_NME"]),
                                               new XElement("OE", i["OE_NME"]),
                                               new XElement("SOTS_Number", i["SOTS_NBR"]),
                                               new XElement("Bill_Cycle_Number", i["BILL_CYC_NBR"]),
                                               new XElement("Comments", i["CMNT_TXT"]),
                                               new XElement("Created_By", i["FULL_NME"])
                                                );


            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_SenderDB, _Email_Req_ID, Convert.ToInt32(_OrderID), _Status, MsgClass.Orders);
                return;
            }


            if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
            {
                _Status = 12;
            }

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
            }
        }

        private void SendASRRequests(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            int _OrderID = Convert.ToInt32(i["ORDR_ID"].ToString());
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            DataTable dt = new DataTable();
            dt = _SenderDB.GetASREmailData(_OrderID);

            if (dt != null && dt.Rows.Count == 1)
            {
                if (i["EMAIL_LIST_TXT"].ToString() != string.Empty)
                    if (dt.Rows[0]["INTL_DOM_EMAIL_CD"].ToString() == "0")
                    {
                        _EmailAddress = i["EMAIL_LIST_TXT"].ToString() + " , " + ConfigurationManager.AppSettings["ASREmailToDmstc"].ToString();
                    }
                    else
                    {
                        _EmailAddress = i["EMAIL_LIST_TXT"].ToString() + " , " + ConfigurationManager.AppSettings["ASREmailTo"].ToString();
                    }
                else
                    if (dt.Rows[0]["INTL_DOM_EMAIL_CD"].ToString() == "0")
                {
                    _EmailAddress = ConfigurationManager.AppSettings["ASREmailToDmstc"].ToString();
                }
                else
                {
                    _EmailAddress = ConfigurationManager.AppSettings["ASREmailTo"].ToString();
                }

                if (i["EMAIL_CC_TXT"].ToString() != string.Empty)
                    _EmailCC = i["EMAIL_CC_TXT"].ToString() + " , " + ConfigurationManager.AppSettings["ASREmailCC"].ToString();
                else
                    _EmailCC = ConfigurationManager.AppSettings["ASREmailCC"].ToString();

                if (_EmailAddress.LastIndexOf(',') == _EmailAddress.Length - 2)
                    _EmailAddress = _EmailAddress.Substring(0, _EmailAddress.Length - 2);
                if (_EmailCC.LastIndexOf(',') == _EmailCC.Length - 2)
                    _EmailCC = _EmailCC.Substring(0, _EmailCC.Length - 2);

                _Subject = i["EMAIL_SUBJ_TXT"].ToString();
                _EmailBody = i["EMAIL_BODY_TXT"].ToString();
                //_EmailCC = i["EmailCC"].ToString();
                XMLHelper _XHelper = new XMLHelper();
                XElement _Email = new XElement("ASREmailData",
                                                   new XElement("ASR_TYPE_ID", dt.Rows[0]["ASR_TYPE_ID"].ToString()),
                                                   new XElement("IP_NODE_TXT", dt.Rows[0]["IP_NODE_TXT"].ToString()),
                                                   new XElement("ACCS_BDWD_DES", dt.Rows[0]["ACCS_BDWD_DES"].ToString()),
                                                   new XElement("ACCS_CTY_NME_SITE_CD", dt.Rows[0]["ACCS_CTY_NME_SITE_CD"].ToString()),
                                                   new XElement("TRNSPNT_CD", dt.Rows[0]["TRNSPNT_CD"].ToString()),
                                                   new XElement("LEC_NNI_NBR", dt.Rows[0]["LEC_NNI_NBR"].ToString()),
                                                   new XElement("ENTRNC_ASMT_TXT", dt.Rows[0]["ENTRNC_ASMT_TXT"].ToString()),
                                                   new XElement("MSTR_FTN_CD", dt.Rows[0]["MSTR_FTN_CD"].ToString()),
                                                   new XElement("DLCI_DES", dt.Rows[0]["DLCI_DES"].ToString()),
                                                   new XElement("H1_MATCH_MSTR_VAS_CD", dt.Rows[0]["H1_MATCH_MSTR_VAS_CD"].ToString()),
                                                   new XElement("ASR_NOTES_TXT", dt.Rows[0]["ASR_NOTES_TXT"].ToString()),
                                                   new XElement("PTNR_CXR_CD", dt.Rows[0]["PTNR_CXR_CD"].ToString()),
                                                   new XElement("VLAN_IDs", dt.Rows[0]["VLAN_IDs"].ToString())
                                                   );
                try
                {
                    _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
                }
                catch
                {
                    _Status = 12;
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error transforming XML to HTML ASR_ID: " + dt.Rows[0]["ASR_ID"].ToString(), string.Empty);
                    if (_SenderDB.UpdateASREmailStatus(Convert.ToInt32(dt.Rows[0]["ASR_ID"].ToString()), _Status) == 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the ASR table for ASR_ID: " + dt.Rows[0]["ASR_ID"].ToString(), string.Empty);
                    }
                    return;
                }


                if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID.ToString(), _EmailCC)))
                {
                    _Status = 12;
                }

                if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID.ToString(), string.Empty);
                }

                if (_SenderDB.UpdateASREmailStatus(Convert.ToInt32(dt.Rows[0]["ASR_ID"].ToString()), _Status) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the ASR table for ASR ID: " + dt.Rows[0]["ASR_ID"].ToString(), string.Empty);
                }
            }
        }

        private bool CreateAndSendRedesignEmail(DataRow _dr)
        {
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _RedesignID = Convert.ToInt32(_dr["REDSGN_ID"].ToString());
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();

            string _EmailBody = string.Empty;
            string redesignURL = string.Empty;
            try
            {
                redesignURL = "<RedesignURL>" + ConfigurationManager.AppSettings["ViewRedesignURL"].ToString() + "</RedesignURL>";
            }
            catch
            {
                redesignURL = "<RedesignURL/>";
            }

            // Commented by Sarah Sandoval [20200810]
            // Updated to cater redesign url on rewrite
            //if (_Body != string.Empty && _Body.IndexOf("RedesignURL") > 0)
            //    _Body = _Body.Replace("<RedesignURL/>", redesignURL);
            if (_Body != string.Empty && _Body.IndexOf("<RedesignURL/>") > 0)
                _Body = _Body.Replace("<RedesignURL/>", redesignURL);

            


         
            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch
            {
                this.UpdateCommonEmailRequestWithError(_objDB, _Email_Req_ID.ToString(), _RedesignID, _Status, MsgClass.Redesigns);
                return false;
            }

            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent for Redesign Number: " + _RedesignID.ToString() + " with Subject: " + _Subj + " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _RedesignID.ToString(), _CCEmail)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, 0, _RedesignID) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for Redesign_ID: " + _RedesignID, string.Empty);
            }

            return _RetVal;
        }

        private bool CreateAndSendCPT24HourNotification(DataRow _dr)
        {
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _CPTID = Convert.ToInt32(_dr["CPT_ID"].ToString());
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();
            DateTime _CreatedDate = DateTime.Parse(_dr["CREAT_DT"].ToString());

            string _EmailBody = string.Empty;
            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty,
                    "Errored out while transforming XML to HTML for CPTID: " + _CPTID, ex.Message.ToString());

                _Status = 12;
                if (_objDB.UpdateCPTEmailStatus(_Email_Req_ID, _CPTID, _Status) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                        "Error updating the status in the EMAIL_REQ table for CPT_ID: " + _CPTID, string.Empty);
                }
                return false;
            }

            if (_CreatedDate.AddHours(24) <= DateTime.Now)
            {
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _CPTID.ToString(), _CCEmail)))
                {
                    // If sending email fails change status to failed.
                    _Status = 12;
                }
                else
                {
                    // Log activity for successfully sending of email message.
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                        "Email sent for CPT ID: " + _CPTID.ToString() + " with Subject: " + _Subj +
                        " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
                }

                // Update email status to irregardless of email sending successful or failed.
                if (_objDB.UpdateCPTEmailStatus(_Email_Req_ID, _CPTID, _Status) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                        "Error updating the status in the EMAIL_REQ table for CPT_ID: " + _CPTID, string.Empty);
                }

                // Insert a new record to send another notification after 24 hours.
                if (_objDB.InsertCPTEmailRequest(_CPTID, _Email_Req_Type_ID,
                        (int)EEmailStatus.Pending, _EmailAddress, _CCEmail, _Subj, _Body) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                        "Error in creating new email request in the EMAIL_REQ table for CPT_ID: " + _CPTID, string.Empty);
                }
            }

            return _RetVal;
        }

        private void CreateAndSendCPErtsEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = i["EMAIL_LIST_TXT"].ToString();
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();

            string _Subject = i["EMAIL_SUBJ_TXT"].ToString();
            string _EmailBody = i["EMAIL_BODY_TXT"].ToString();
            string _EmailCC = i["EMAIL_CC_TXT"].ToString();

            if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
            {
                _Status = 12;
            }

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
            }
        }

        private bool CreateAndSendCPTEmail(DataRow _dr)
        {
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            string _CPTID = _dr["CPT_ID"].ToString();
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            string _EmailBody = string.Empty;
            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty,
                    "Errored out while transforming XML to HTML for CPTID: " + _CPTID, ex.Message.ToString());

                _Status = 12;
                if (_objDB.UpdateCPTEmailStatus(_Email_Req_ID, Int32.Parse(_CPTID), _Status) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                        "Error updating the status in the EMAIL_REQ table for CPT_ID: " + _CPTID, string.Empty);
                }

                return false;
            }

            if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _CPTID, _CCEmail)))
            {
                // If sending email fails change status to failed.
                _Status = 12;
            }
            else
            {
                // Log activity for successfully sending of email message.
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                    "Email sent for CPT ID: " + _CPTID + " with Subject: " + _Subj +
                    " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
            }

            // Update email irregardless of sending email succeeded or failed.
            if (_objDB.UpdateCPTEmailStatus(_Email_Req_ID, Int32.Parse(_CPTID), _Status) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                    "Error updating the status in the EMAIL_REQ table for CPT_ID: " + _CPTID, string.Empty);
            }

            return _RetVal;
        }

        private void CreateAndSendPSEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = i["EMAIL_LIST_TXT"].ToString();
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();

            string _Subject = i["EMAIL_SUBJ_TXT"].ToString();
            string _EmailBody = i["EMAIL_BODY_TXT"].ToString();
            string _EmailCC = i["EMAIL_CC_TXT"].ToString();

            if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
            {
                _Status = 12;
            }

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
            }
        }

        private bool CreateAndSendCLLIEmails(DataRow _dr)
        {
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            string CLLI_ID = _dr["EMAIL_SUBJ_TXT"].ToString().Substring(_dr["EMAIL_SUBJ_TXT"].ToString().LastIndexOf(':') + 2);
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            string _EmailBody = string.Empty;
            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty,
                    "Errored out while transforming XML to HTML for CLLI: " + CLLI_ID, ex.Message.ToString());

                _Status = 12;
                if (_objDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), 0, _Status, 0) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for CLLI_ID: " + CLLI_ID, string.Empty);
                }
                return false;

            }

            if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, "", _CCEmail)))
            {
                // If sending email fails change status to failed.
                _Status = 12;
            }
            else
            {
                // Log activity for successfully sending of email message.
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                    "Email sent for CLLI: " + CLLI_ID + " with Subject: " + _Subj +
                    " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
            }

            if (_objDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), 0, _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for CLLI_ID: " + CLLI_ID, string.Empty);
            }

            return _RetVal;
        }

        private void CreateAndSendSDEEmail(DataRow _dr)
        {
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            //string _SDEID = _dr["CPT_ID"].ToString();
            string _SDEID = string.Empty;
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            string _EmailBody = string.Empty;
            string _sdeURL = string.Empty;
            try
            {
                _sdeURL = "<SDEURL>" + ConfigurationManager.AppSettings["ViewSDEURL"].ToString() + "</SDEURL>";
            }
            catch
            {
                _sdeURL = "<SDEURL/>";
            }

            if (_Body != string.Empty && _Body.IndexOf("SDEURL") > 0)
                _Body = _Body.Replace("<SDEURL/>", _sdeURL);

            string _docURL = string.Empty;
            try
            {
                _docURL = "<DOCURL>" + ConfigurationManager.AppSettings["DownloadSDEURL"].ToString() + "</DOCURL>";
            }
            catch
            {
                _docURL = "<DOCURL/>";
            }

            if (_Body != string.Empty && _Body.IndexOf("DOCURL") > 0)
                _Body = _Body.Replace("<DOCURL/>", _docURL);

            try
            {
                _EmailBody = _XHelper.transformXMLtoHTML(_Body, _dr["EMAIL_REQ_TYPE"].ToString(), 0);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error transforming XML to HTML for SDEID: " + _SDEID.ToString(), string.Empty);

                _Status = 12;
                if (_objDB.UpdateSDEEmailStatus(_Email_Req_ID, _Status) == 0)
                {
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                        "Error updating the status in the EMAIL_REQ table for SDEID: " + _SDEID, string.Empty);
                }
                return;
            }

            if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _SDEID, _CCEmail)))
            {
                // If sending email fails change status to failed.
                _Status = 12;
            }
            else
            {
                // Log activity for successfully sending of email message.
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty,
                    "Email sent for SDE ID: " + _SDEID + " with Subject: " + _Subj +
                    " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
            }

            // Update email irregardless of sending email succeeded or failed.
            if (_objDB.UpdateSDEEmailStatus(_Email_Req_ID, _Status) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                    "Error updating the status in the EMAIL_REQ table for SDEID: " + _SDEID, string.Empty);
            }
        }

        private void CreateAndSendSDEAccountEmail(DataRow _dr)
        {
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            int _Email_Req_Type_ID = Convert.ToInt32(_dr["EMAIL_REQ_TYPE_ID"].ToString());
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();

            if (!(SendEmail(_EmailAddress, _Subj, _Body, string.Empty, _CCEmail)))
            {
                _Status = 12;
            }

            if (_objDB.UpdateSDEEmailStatus(_Email_Req_ID, _Status) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty,
                    "Error updating the status in the EMAIL_REQ table for EMAIL_REQ_ID: " + _Email_Req_ID, string.Empty);
            }
        }

        private void CreateAndSendZscalerOrders(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            int _OrderID = Convert.ToInt32(i["ORDR_ID"].ToString());
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            DataSet ds = new DataSet();

            ds = _SenderDB.GetZscalerEmailData(_OrderID);

            DataTable dtOrdr = ds.Tables[0];
            DataTable dtItem = ds.Tables[1];

            if (ds != null)
            {
                if (dtOrdr != null && dtOrdr.Rows.Count == 1)
                {
                    _EmailAddress = i["EMAIL_LIST_TXT"].ToString();

                    if (_EmailAddress.LastIndexOf(',') == _EmailAddress.Length - 2)
                        _EmailAddress = _EmailAddress.Substring(0, _EmailAddress.Length - 2);

                    _Subject = i["EMAIL_SUBJ_TXT"].ToString();
                    _EmailCC = i["Email_CC_TXT"].ToString();
                    XMLHelper _XHelper = new XMLHelper();
                    XElement _Email = new XElement("ZscalerEmail",
                                                       new XElement("Customer", dtOrdr.Rows[0]["Customer"].ToString()),
                                                       new XElement("City", dtOrdr.Rows[0]["City"].ToString()),
                                                       new XElement("State", dtOrdr.Rows[0]["State"].ToString()),
                                                       new XElement("CCD", dtOrdr.Rows[0]["CCD"].ToString()),
                                                       new XElement("IPM_NME", dtOrdr.Rows[0]["IPM"].ToString()),
                                                       new XElement("IPM_Phone", dtOrdr.Rows[0]["Phone"].ToString()),
                                                       new XElement("QuoteID", dtOrdr.Rows[0]["QuoteID"].ToString())
                                                       );

                    for (int f = 0; f < dtItem.Rows.Count; f++)
                    {
                        XElement _xmsitm = new XElement("Items",
                                        new XElement("ItemDesc", dtItem.Rows[f]["MDS_DES"]),
                                        new XElement("ItemQty", dtItem.Rows[f]["ORDR_QTY"])

                                  );
                        _Email.Add(_xmsitm);
                    }

                    try
                    {
                        _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
                    }
                    catch
                    {

                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error transforming XML to HTML for ORDR_ID: " + _OrderID.ToString(), string.Empty);

                        _Status = 12;
                        if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                        {
                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID.ToString(), string.Empty);
                        }
                        return;
                    }
                    

                    //This is for testing prior to getting certificate from Zscaler
                    if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, string.Empty, _EmailCC)))
                    {
                        _Status = 12;
                    }

                    //X509Certificate2 _EncryptCert = new X509Certificate2(ConfigurationManager.AppSettings["Zscaler_Cert"].ToString());

                    //if (!(SendEncryptedEmail(_EncryptCert, _EmailAddress, _Subject, _EmailBody, _OrderID.ToString(), _EmailCC)))
                    //{
                    //    _Status = 12;
                    //}

                    if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID.ToString(), string.Empty);
                    }
                }
            }
        }

        private void CreateAndSendBillOnlyOrderEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            int _OrderID = Convert.ToInt32(i["ORDR_ID"].ToString());
            string _EmailAddress = "";
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();
            string _EmailCC = "";
            string _Subject = "";
            string _EmailBody = "";

            DataSet ds = new DataSet();

            ds = _SenderDB.GetBillOnlyInfoData(_OrderID);

            DataTable dtOrdr = ds.Tables[0];
            DataTable dtItem = ds.Tables[1];

            if (ds != null)
            {
                if (dtOrdr != null && dtOrdr.Rows.Count == 1)
                {
                    _EmailAddress = i["EMAIL_LIST_TXT"].ToString();

                    if (_EmailAddress.LastIndexOf(',') == _EmailAddress.Length - 2)
                        _EmailAddress = _EmailAddress.Substring(0, _EmailAddress.Length - 2);

                    _Subject = i["EMAIL_SUBJ_TXT"].ToString();
                    _EmailCC = i["Email_CC_TXT"].ToString();
                    XMLHelper _XHelper = new XMLHelper();
                    XElement _Email = new XElement("BillOnlyEmail",
                                                       new XElement("FTN", dtOrdr.Rows[0]["FTN"].ToString()),
                                                       new XElement("H5_H6_CUST_ID", dtOrdr.Rows[0]["H5_H6_CUST_ID"].ToString())
                                                       );

                    for (int f = 0; f < dtItem.Rows.Count; f++)
                    {
                        XElement _xmsitm = new XElement("Items",
                                        new XElement("CKT_CHG_TYPE_DES", dtItem.Rows[f]["CKT_CHG_TYPE_DES"]),
                                        new XElement("CHG_NRC_AMT", dtItem.Rows[f]["CHG_NRC_AMT"]),
                                        new XElement("TAX_RT_PCT_QTY", dtItem.Rows[f]["TAX_RT_PCT_QTY"]),
                                        new XElement("CUR_NME", dtItem.Rows[f]["CUR_NME"]),
                                        new XElement("CHG_NRC_IN_USD_AMT", dtItem.Rows[f]["CHG_NRC_IN_USD_AMT"]),
                                        new XElement("NTE_TXT", dtItem.Rows[f]["NTE_TXT"]),
                                        new XElement("STUS_DES", dtItem.Rows[f]["STUS_DES"])

                                  );
                        _Email.Add(_xmsitm);
                    }
                    try
                    {
                        _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);
                    }
                    catch
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error transforming XML to HTML for ORDR_ID: " + _OrderID.ToString(), string.Empty);

                        _Status = 12;
                        if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                        {
                            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID.ToString(), string.Empty);
                        }
                    }
                    

                    if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, string.Empty, _EmailCC)))
                    {
                        _Status = 12;
                    }

                    if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
                    {
                        CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID.ToString(), string.Empty);
                    }
                }
            }
        }


        private void CreateAndSendIPMEmail(DataRow i)
        {
            EmailSenderDB _SenderDB = new EmailSenderDB();
            int _Status = 11;
            string _OrderID = i["ORDR_ID"].ToString();
            string _EmailAddress = i["EMAIL_LIST_TXT"].ToString();
            string _Email_Req_ID = i["EMAIL_REQ_ID"].ToString();

            string _Subject = i["EMAIL_SUBJ_TXT"].ToString();
            string _EmailBody = i["EMAIL_BODY_TXT"].ToString();
            string _EmailCC = i["EMAIL_CC_TXT"].ToString();

            if (!(SendEmail(_EmailAddress, _Subject, _EmailBody, _OrderID, _EmailCC)))
            {
                _Status = 12;
            }

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_OrderID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for ORDR_ID: " + _OrderID, string.Empty);
            }
        }
        private bool CreateAndSendCPEDiscEmail(DataRow _dr)
        {
            bool _RetVal = true;
            XMLHelper _XHelper = new XMLHelper();
            EmailSenderDB _objDB = new EmailSenderDB();
            int _Status = 11;
            int _OrderID = Convert.ToInt32(_dr["ORDR_ID"].ToString());
            int _Email_Req_ID = Convert.ToInt32(_dr["EMAIL_REQ_ID"].ToString());
            string _Email_Req_Type = _dr["EMAIL_REQ_TYPE"].ToString();
            string _EmailAddress = _dr["EMAIL_LIST_TXT"].ToString();
            string _CCEmail = _dr["EMAIL_CC_TXT"].ToString();
            string _Subj = _dr["EMAIL_SUBJ_TXT"].ToString();
            string _Body = _dr["EMAIL_BODY_TXT"].ToString();
            string _EmailBody = _XHelper.transformXMLtoHTML(_Body, _Email_Req_Type.ToString(), 0);

            if (_Body != null)
            {
                CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email sent for OrderID: " + _OrderID.ToString() + " with Subject: " + _Subj + " To these email addresses: " + _EmailAddress + "," + _CCEmail, string.Empty);
                if (!(SendEmail(_EmailAddress, _Subj, _EmailBody, _OrderID.ToString(), _CCEmail)))
                {
                    _Status = 12;
                }
            }

            if (_objDB.UpdateEmailRequest(_Email_Req_ID, 0, _Status, 0, _OrderID) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error updating the status in the EMAIL_REQ table for OrderID: " + _OrderID, string.Empty);
            }

            return _RetVal;
        }





        private bool SendEncryptedEmail(X509Certificate2 _EncryptCert, string _EmailAddress, string _Subject, string _EmailBody, string _ID, string _EmailCC)
        {
            bool _RetVal = true;
            string _FromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();
            try
            {
                EmailHelper.SendSingleEncryptEmail(_EncryptCert, _FromEmail, _EmailAddress, _EmailCC, string.Empty, _Subject, _EmailBody, null);
                if (_ID != string.Empty)
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email request created and sent for Order/Event ID: " + _ID, _Subject);
                else
                    CL.WriteLogFile("Main", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Email request created and sent for Adhoc Report Notification", _Subject);
            }
            catch (Exception Ex)
            {
                if (_ID != string.Empty)
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Errored out while sending email out for Order/Event ID: " + _ID, Ex.Message.ToString());
                else
                    CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Errored out while sending email for Adhoc Report", Ex.Message.ToString());
                _RetVal = false;
            }

            return _RetVal;
        }

        private string ConcatenateValues(DataTable dt, string columnName)
        {
            string _value = string.Empty;
            try
            {
                foreach (DataRow item in dt.Rows)
                {
                    _value += (item[columnName] == null ? string.Empty : item[columnName].ToString()) + ", ";
                }
                if (_value.LastIndexOf(",") > 0)
                {
                    _value = _value.Remove(_value.LastIndexOf(","));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _value;
        }


        private void UpdateCommonEmailRequestWithError(EmailSenderDB _SenderDB, string _Email_Req_ID, int _ID, int _Status, MsgClass msgClass)
        {
            var key = string.Empty;

            switch (msgClass)
            {
                case MsgClass.Orders:
                    key = "ORDR_ID";
                    break;
                case MsgClass.Events:
                    key = "EVENT_ID";
                    break;
                case MsgClass.Redesigns:
                    key = "REDSGN_ID ";
                    break;
                case MsgClass.EmailRequests:
                    key = "EMAIL_REQ_ID";
                    break;
                default:
                    break;
            }

            var updateMessage = $"Error updating the status in the EMAIL_REQ table for {key}: {_ID.ToString()}";
            var transformMessage = $"Error transforming XML to HTML for {key}: {_ID.ToString()}";      

            _Status = 12;
            CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.error, -1, string.Empty, transformMessage, string.Empty);

            if (_SenderDB.UpdateEmailRequest(Convert.ToInt32(_Email_Req_ID), Convert.ToInt32(_ID), _Status, 0) == 0)
            {
                CL.WriteLogFile("Main", CL.Event.Error, CL.MsgType.text, -1, string.Empty, updateMessage, string.Empty);
            }
        }
    }
}