@echo off
REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave - initial implementation
REM
REM *** APPLICATION SCRIPT ***
REM This Script will need to be updated anytime we create a new service or remove
REM an existing service
REM
REM Return codes:
REM   (pls see return codes for _recycle_app_pool.cmd)
REM
REM *******************************************************************************


REM
REM Start all Services on the COWS Application
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%

call "%APP_HOME%\Scripts\start_emailsender_svc.cmd"
call "%APP_HOME%\Scripts\start_fsa_svc.cmd"
call "%APP_HOME%\Scripts\start_odiereceiver_svc.cmd"
call "%APP_HOME%\Scripts\start_odiesender_svc.cmd"
call "%APP_HOME%\Scripts\start_recurringapptparser_svc.cmd"
call "%APP_HOME%\Scripts\start_tadpole_svc.cmd"
call "%APP_HOME%\Scripts\start_autoreportingdx_svc.cmd"
