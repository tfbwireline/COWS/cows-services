﻿namespace COWS.SSTATReceiverService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SSTATOrderStus
    {
        private string timeStampField;

        private string tRANIDField;

        private string orderIDField;

        private string deviceIDField;

        private SSTATOrderStusOrderAction orderActionField;

        private string completionDateField;

        private string noteField;

        private string errorMsgField;

        private string jprdCd;

        /// <remarks/>
        public string TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string TRANID
        {
            get
            {
                return this.tRANIDField;
            }
            set
            {
                this.tRANIDField = value;
            }
        }

        /// <remarks/>
        public string OrderID
        {
            get
            {
                return this.orderIDField;
            }
            set
            {
                this.orderIDField = value;
            }
        }

        /// <remarks/>
        public string DeviceID
        {
            get
            {
                return this.deviceIDField;
            }
            set
            {
                this.deviceIDField = value;
            }
        }

        /// <remarks/>
        public SSTATOrderStusOrderAction OrderAction
        {
            get
            {
                return this.orderActionField;
            }
            set
            {
                this.orderActionField = value;
            }
        }

        /// <remarks/>
        public string CompletionDate
        {
            get
            {
                return this.completionDateField;
            }
            set
            {
                this.completionDateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string ErrorMsg
        {
            get
            {
                return this.errorMsgField;
            }
            set
            {
                this.errorMsgField = value;
            }
        }

        /// <remarks/>
        public string JprdCd
        {
            get
            {
                return this.jprdCd;
            }
            set
            {
                this.jprdCd = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum SSTATOrderStusOrderAction
    {
        /// <remarks/>
        Complete,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("RTS-Hold")]
        RTSHold,

        /// <remarks/>
        Cancel,

        /// <remarks/>
        ACK,

        /// <remarks/>
        NACK,
    }
}