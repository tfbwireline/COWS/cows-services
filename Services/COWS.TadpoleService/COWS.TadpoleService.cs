﻿using EmailManager;
using IOManager;
using MQManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using TadpoleHelper;
using FL = LogManager.FileLogger;

/*
 * Created by:  Kyle Wichert
 * Create Date: 2012-04-11
 * Start three threads, an MQ Connector, a Receiver thread, and a Sender thread.
 * Sender and receiver threads go into a hold pattern until the MQ Connector thread
 * establishes a connection to the MQ channel.
 * Receiver reads one message at a time until there are 0 messages on the queue, then sleeps before restarting loop.
 * Receiver reads the message first without taking it off of the queue, and only deques it once the
 * message has finished processing.
 * Sender retrieves data from the DB, then sleeps before restarting the loop.
 * On stop, send interupts to stop any sleeping, then join all three threads.
 * If any of
 */

namespace COWS.TadpoleService
{
    public partial class TadpoleService : ServiceBase
    {
        public TadpoleService()
        {
            InitializeComponent();
            try
            {
                this.ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();
            }
            catch
            {
                this.ServiceName = "COWS.TadpoleService";
            }
        }

        private bool StopSignalled = false;

        private Thread ReceiverThread = null;
        private Thread SenderThread = null;
        private Thread MQConnector = null;
        private MQClient mqClient = null;

        public string COWSConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private int ReceiverSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["ReceiverSleepTime"]);
        private int SenderSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["SenderSleepTime"]);
        private string DefaultFromEmail = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();
        private string ErrorEmailList = ConfigurationManager.AppSettings["ErrorEmailList"].ToString();
        private const byte EmailSuccessStatus = 11;
        private const byte EmailErrorStatus = 12;

        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new TadpoleService() };
            try { ServiceBase.Run(ServicesToRun); }
            catch (Exception ex)
            {
                try
                {
                    FL.WriteLogFile("Main", FL.Event.Error, FL.MsgType.text, -1, string.Empty, "Unhandled exception returned to Main - " + ex.ToString(), "");
                }
                catch { }
            }
        }

        protected override void OnStart(string[] args)
        {
            //Debug.Assert(false);

            try
            {
                MQConnector = new Thread(new ThreadStart(this.EstablishMQConnection));
                FL.WriteLogFile("Main", FL.Event.BeginThread, FL.MsgType.text, -1, string.Empty, "Beginning Thread to establish MQ Connection", string.Empty);
                MQConnector.Start();

                ReceiverThread = new Thread(new ThreadStart(this.RunReceiver));
                FL.WriteLogFile("Main", FL.Event.BeginThread, FL.MsgType.text, -1, string.Empty, "Beginning Thread: Tadpole Receiver", string.Empty);
                ReceiverThread.Start();

                SenderThread = new Thread(new ThreadStart(this.RunSender));
                FL.WriteLogFile("Main", FL.Event.BeginThread, FL.MsgType.text, -1, string.Empty, "Beginning Thread: Tadpole Sender", string.Empty);
                SenderThread.Start();
            }
            catch (Exception ex)
            {
                try
                {
                    FL.WriteLogFile("Main", FL.Event.Error, FL.MsgType.error, -1, string.Empty, "Error starting COWS.TadpoleService. Error: " + ex.ToString(), string.Empty);
                }
                catch { }
            }
        }

        protected override void OnStop()
        {
            StopSignalled = true;
            base.OnStop();      //Does nothing here since we are inheriting ServiceBase (which has an empty onstop method).  Leave here in case we ever inherit another base class
            FL.WriteLogFile("Main", FL.Event.GenericMessage, FL.MsgType.text, -1, string.Empty, "Stop Signal received, interupting/joining threads (30 second timeout each for Sender/Receiver service join)", string.Empty);

            //End sleep cycles.
            if (MQConnector != null)
                MQConnector.Interrupt();
            if (ReceiverThread != null)
                ReceiverThread.Interrupt();
            if (SenderThread != null)
                SenderThread.Interrupt();

            //Wait for Threads to complete
            if (MQConnector != null)
                MQConnector.Join(10 * 1000);
            if (ReceiverThread != null)
                ReceiverThread.Join(30 * 1000);
            if (SenderThread != null)
                SenderThread.Join(30 * 1000);

            //Be extra super special patient
            if ((ReceiverThread != null && ReceiverThread.IsAlive) || (SenderThread != null && SenderThread.IsAlive))
            {
                FL.WriteLogFile("Main", FL.Event.EndThread, FL.MsgType.text, -1, string.Empty, "One or both thread is still alive, giving it 30 seconds to complete", string.Empty);
                Thread.Sleep(30 * 1000);
            }

            //Fine, kill the threads
            if (ReceiverThread != null && ReceiverThread.IsAlive)
            {
                FL.WriteLogFile("Main", FL.Event.EndThread, FL.MsgType.text, -1, string.Empty, "Receiver Thread still alive, aborting", string.Empty);
                ReceiverThread.Abort();
                ReceiverThread.Join();
            }
            if (SenderThread != null && SenderThread.IsAlive)
            {
                FL.WriteLogFile("Main", FL.Event.EndThread, FL.MsgType.text, -1, string.Empty, "Sender Thread still alive, aborting", string.Empty);
                SenderThread.Abort();
                SenderThread.Join();
            }

            FL.WriteLogFile("Main", FL.Event.EndSession, FL.MsgType.text, -1, string.Empty, "Tadpole Service Stopped", string.Empty);
        }

        private void EstablishMQConnection()
        {
            mqClient = new MQClient();
            try
            {
                while (!mqClient.Initialize())
                {
                    FL.WriteLogFile("Main", FL.Event.Error, FL.MsgType.text, -1, string.Empty, String.Format("MQ connection not established, trying again in 2 minutes"), string.Empty);
                    Thread.Sleep(2 * 60 * 1000);
                }
            }
            catch (ThreadInterruptedException)
            {
                FL.WriteLogFile("Main", FL.Event.Error, FL.MsgType.text, -1, string.Empty, String.Format("MQ received interupt before connection established"), string.Empty);
            }
        }

        private void RunReceiver()
        {
            string strError = string.Empty;
            FL.WriteLogFile("Receiver", FL.Event.BeginStart, FL.MsgType.text, -1, string.Empty, String.Format("Receiver Started, begin wait for MQ Connection."), string.Empty);
            System.Text.ASCIIEncoding encoding = new ASCIIEncoding();

            //Wait for connection to be established
            try { MQConnector.Join(); }
            catch (ThreadInterruptedException) { }

            while (!StopSignalled)
            {
                try
                {
                    FL.WriteLogFile("Receiver", FL.Event.BeginMQRead, FL.MsgType.text, -1, string.Empty, "Begin Receiver Cycle", string.Empty);

                    if (mqClient.MQGet(MQClient.QObjReturned.QueueOfMQMessageItems))
                    {
                        Queue messageQue = mqClient.outQue;
                        int MsgCnt = messageQue.Count;

                        if (messageQue.Count > 0)
                        {
                            while (messageQue.Count > 0)
                            {
                                MQClient.MQMessageItem MQI = (MQClient.MQMessageItem)messageQue.Dequeue();
                                string msg = MQI.message;
                                byte[] correlationID = MQI.correlationId;
                                byte[] messageID = MQI.messageId;
                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                DataRow dr;
                                TadpoleEventDB _SenderDB = new TadpoleEventDB();
                                ds = _SenderDB.getEncryptionTKey("ZA9");
                                dt = ds.Tables[0];
                                dr = dt.Rows[0];
                                string key = dr["KEY"].ToString();
                                string decryptedData = Decrypt(msg, key);

                                FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "MQ Get complete, XML Retrieved with Correlation ID: " + HexEncoding.ToString(correlationID) + ", Message ID: " + HexEncoding.ToString(messageID), string.Empty);

                                TadpoleServiceIO ts = new TadpoleServiceIO();
                                ResponseContainer rc = ts.ProcessXML(decryptedData, true);
                                strError += ts.strError;

                                FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "MQ Remove complete", string.Empty);

                                if ((rc.ACKResponse ?? "") != string.Empty)
                                {
                                    if (rc.ACKResponse.Contains("<status>Accepted"))
                                    {
                                        string encryptedData = Encrypt(rc.ACKResponse, key);
                                        mqClient.MQPut(encryptedData, correlationID);
                                        FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "ACK/NACK put complete - " + rc.ACKResponse, string.Empty);
                                    }
                                    else
                                    {
                                        FL.WriteLogFile("Receiver", FL.Event.EndMQRead, FL.MsgType.text, -1, "", "Tadpole Service NACK Response - " + rc.ACKResponse, string.Empty);
                                        if (EmailHelper._SendSingleEmail(DefaultFromEmail, ErrorEmailList, string.Empty, string.Empty, "Tadpole Service NACK Response", rc.ACKResponse))
                                            FL.WriteLogFile("Receiver", FL.Event.GenericMessage, FL.MsgType.text, -1, string.Empty, "Tadpole Service NACK Response sent to " + ErrorEmailList, string.Empty);
                                    }
                                }
                            }
                        }
                        Thread.SpinWait(2);
                    }
                    FL.WriteLogFile("Receiver", FL.Event.BeginMQRead, FL.MsgType.text, -1, string.Empty, "End Receiver Cycle", string.Empty);
                }
                catch (XMLNotDatabased ex)
                {
                    FL.WriteLogFile("Receiver", FL.Event.Error, FL.MsgType.error, -1, string.Empty, "XML not databased, sleeping then attempting message again." + Environment.NewLine + ex.exception.Message + Environment.NewLine + ex.exception.StackTrace, string.Empty);
                    strError += "XML not databased, sleeping then attempting message again." + Environment.NewLine + ex.exception.Message + Environment.NewLine + ex.exception.StackTrace;
                }
                catch (Exception ex)
                {
                    FL.WriteLogFile("Receiver", FL.Event.Error, FL.MsgType.error, -1, string.Empty, "Receiver Error - " + ex.Message + "; " + ex.StackTrace, string.Empty);
                    strError += "Receiver Error - " + ex.Message + "; " + ex.StackTrace;
                }

                try { Thread.Sleep(ReceiverSleepTime); }
                catch (ThreadInterruptedException) { }

                if (strError.Length > 0)
                {
                    try
                    {
                        if (EmailHelper._SendSingleEmail(DefaultFromEmail, ErrorEmailList, string.Empty, string.Empty, "Tadpole Service Error", strError))
                            FL.WriteLogFile("Receiver", FL.Event.GenericMessage, FL.MsgType.text, -1, string.Empty, "Error Email Sent to " + ErrorEmailList, string.Empty);
                    }
                    catch { }
                }
            }
            FL.WriteLogFile("Receiver", FL.Event.EndThread, FL.MsgType.text, -1, string.Empty, "Stop received, shutting down thread", string.Empty);
        }

        private void RunSender()
        {
            FL.WriteLogFile("Sender", FL.Event.BeginStart, FL.MsgType.text, -1, string.Empty, String.Format("Sender Started, begin wait for MQ Connection."), string.Empty);

            //Wait for connection to be established
            try { MQConnector.Join(); }
            catch (ThreadInterruptedException) { }

            while (!StopSignalled)
            {
                try
                {
                    FL.WriteLogFile("Sender", FL.Event.BeginDBRead, FL.MsgType.text, -1, string.Empty, String.Format("Checking DB for pending outbound messages"), string.Empty);
                    TadpoleServiceIO tsIO = new TadpoleServiceIO();
                    List<ResponseEntity> responses = tsIO.GetResponses();

                    FL.WriteLogFile("Sender", FL.Event.BeginDBRead, FL.MsgType.text, -1, string.Empty, String.Format("Found {0} messages", responses.Count), string.Empty);

                    foreach (ResponseEntity r in responses)
                    {
                        try
                        {
                            string xmlString = tsIO.GetXMLForResponse(r);

                            DataTable dt = new DataTable();
                            DataSet ds = new DataSet();
                            DataRow dr;
                            TadpoleEventDB _SenderDB = new TadpoleEventDB();
                            ds = _SenderDB.getEncryptionTKey("ZA9");
                            dt = ds.Tables[0];
                            dr = dt.Rows[0];
                            string key = dr["KEY"].ToString();
                            string encryptedData = Encrypt(xmlString, key);
                            mqClient.MQPut(encryptedData);
                            FL.WriteLogFile("Sender", FL.Event.EndMQWrite, FL.MsgType.text, -1, string.Empty, "Response placed on MQ channel:"
                                + Environment.NewLine + xmlString, "");
                            tsIO.UpdateMessageStatus(r.MessageID, EmailSuccessStatus);
                        }
                        catch (Exception ex)
                        {
                            FL.WriteLogFile("Sender", FL.Event.Error, FL.MsgType.error, -1, string.Empty, "Sender Error for MessageID: " +
                                r.MessageID + Environment.NewLine + ex.ToString(), string.Empty);
                            tsIO.UpdateMessageStatus(r.MessageID, EmailErrorStatus);
                        }
                    }
                }
                catch (Exception ex)
                {
                    FL.WriteLogFile("Sender", FL.Event.Error, FL.MsgType.error, -1, string.Empty, "Sender Error - " + ex.ToString(), string.Empty);
                }

                try { Thread.Sleep(SenderSleepTime); }
                catch (ThreadInterruptedException) { }
            }
            FL.WriteLogFile("Sender", FL.Event.EndThread, FL.MsgType.text, -1, string.Empty, "Stop received, shutting down thread", string.Empty);
        }

        private RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private String Encrypt(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
        }

        private byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        private String Decrypt(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }
    }
}