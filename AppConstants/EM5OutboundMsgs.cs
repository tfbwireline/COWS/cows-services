﻿namespace AppConstants
{
    public enum EM5OutboundMsgs
    {
        CPEComplete = 1,
        MNSComplete = 2,
        MSSComplete = 3,
        CPERTS = 4,
        CPEPO = 5
    }
}