﻿using DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace COWSRecurringApptParser
{
    internal class RecurringApptParserDB : DBBase
    {
        public List<ApptRcurncData> lARData = new List<ApptRcurncData>();

        internal DataTable GetRecurringAppts()
        {
            using (COWS AppDBCntxt = new COWS(ConnStr))
            {
                AppDBCntxt.ObjectTrackingEnabled = false;

                var x = (from apt in AppDBCntxt.APPT
                         join apr in AppDBCntxt.APPT_RCURNC_TRGR on apt.APPT_ID equals apr.APPT_ID
                         where ((apt.RCURNC_DES_TXT != null) && (apt.RCURNC_CD == true) && (apt.ASN_TO_USER_ID_LIST_TXT != null) && (apr.STUS_ID == ((byte)EApptRecStus.Pending)))
                         select new
                         {
                             APPT_ID = apt.APPT_ID,
                             ResourceID = XmlElementExtensions.GetAttribute<List<string>>(XElement.Parse(apt.ASN_TO_USER_ID_LIST_TXT).Descendants("ResourceId"), "Value", new List<string>()),
                             StartDt = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "Start", apt.STRT_TMST.ToString()),
                             EndDt = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "End", apt.END_TMST.ToString()),
                             strtTmst = apt.STRT_TMST,
                             EndTmst = apt.END_TMST,
                             AType = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "Type", "0"),
                             WeekDays = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "WeekDays", "0"),
                             Periodicity = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "Periodicity", "0"),
                             WeekOfMonth = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "WeekOfMonth", "-1"),
                             DayNumber = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "DayNumber", "0"),
                             Month = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "Month", "0"),
                             Range = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "Range", "0"),
                             OccurrenceCount = XmlElementExtensions.GetAttribute<string>(XElement.Parse(apt.RCURNC_DES_TXT), "OccurrenceCount", "0"),
                             APPTTypeID = apt.APPT_TYPE_ID
                         }).OrderBy(ob => ob.APPT_ID);

                return LinqHelper.CopyToDataTable(x, null, null);
            }
        }

        internal DataTable GetNoEndRecurringAppts()
        {
            setCommand("[dbo].[getNoEndRecAppts]");
            return getDataTable();
        }

        internal void CommitApptRecData(int ApptID)
        {
            using (COWS AppDBCntxt = new COWS(ConnStr))
            {
                var oldRec = (from apd in AppDBCntxt.APPT_RCURNC_DATA
                              where apd.APPT_ID == ApptID
                              select apd);

                if (oldRec.Count() > 0)
                {
                    AppDBCntxt.APPT_RCURNC_DATA.DeleteAllOnSubmit(oldRec);
                    AppDBCntxt.SubmitChanges();
                }

                List<APPT_RCURNC_DATA> lARD = new List<APPT_RCURNC_DATA>();

                foreach (ApptRcurncData ar in lARData)
                {
                    APPT_RCURNC_DATA ard = new APPT_RCURNC_DATA();
                    ard.APPT_ID = ar.ApptID;
                    ard.APPT_TYPE_ID = ar.ApptTypeID;
                    ard.STRT_TMST = ar.StrtTmst;
                    ard.END_TMST = ar.EndTmst;
                    ard.ASN_USER_ID = ar.AsnUserID;
                    ard.CREAT_DT = ar.CreatDt;

                    lARD.Add(ard);
                }
                AppDBCntxt.APPT_RCURNC_DATA.InsertAllOnSubmit(lARD);
                AppDBCntxt.SubmitChanges();

                var tRecs = (from apt in AppDBCntxt.APPT_RCURNC_TRGR
                             where ((apt.APPT_ID == ApptID) && (apt.STUS_ID == ((byte)EApptRecStus.Processing)))
                             select apt);

                foreach (var tRec in tRecs)
                {
                    tRec.STUS_ID = ((byte)EApptRecStus.Success);
                    tRec.MODFD_DT = DateTime.Now;
                }

                AppDBCntxt.SubmitChanges();
            }
        }

        internal void UpdateTrigStatus(int iApptID, byte byStusID)
        {
            using (COWS AppDBCntxt = new COWS(ConnStr))
            {
                var tRecs = (from apt in AppDBCntxt.APPT_RCURNC_TRGR
                             where ((apt.APPT_ID == iApptID)
                                  && ((apt.STUS_ID == ((byte)EApptRecStus.Pending)) || (apt.STUS_ID == ((byte)EApptRecStus.Processing))))
                             select apt);

                foreach (var tRec in tRecs)
                {
                    tRec.STUS_ID = byStusID;
                    tRec.MODFD_DT = DateTime.Now;
                }

                AppDBCntxt.SubmitChanges();
            }
        }

        internal void InsertNoEndRecAppts(DataTable dtNoEnd)
        {
            using (COWS AppDBCntxt = new COWS(ConnStr))
            {
                foreach (DataRow dr in dtNoEnd.Rows)
                {
                    lock (this)
                    {
                        APPT_RCURNC_TRGR aptr = new APPT_RCURNC_TRGR();

                        aptr.APPT_ID = Int32.Parse(dr["APPT_ID"].ToString());
                        aptr.STUS_ID = ((byte)EApptRecStus.Pending);
                        aptr.CREAT_DT = DateTime.Now;

                        AppDBCntxt.APPT_RCURNC_TRGR.InsertOnSubmit(aptr);
                    }
                }
                AppDBCntxt.SubmitChanges();
            }
        }
    }
}