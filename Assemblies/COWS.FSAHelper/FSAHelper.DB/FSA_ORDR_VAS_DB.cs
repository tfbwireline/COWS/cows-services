﻿using DataManager;
using System;

namespace FSAHelper
{
    public class FSA_ORDR_VAS_DB : MSSqlBase
    {
        #region "Fields & Properties"

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private string _VAS_TYPE_CD;

        public string VAS_TYPE_CD
        {
            get { return _VAS_TYPE_CD; }
            set { _VAS_TYPE_CD = value; }
        }

        private string _SRVC_LVL_ID;

        public string SRVC_LVL_ID
        {
            get { return _SRVC_LVL_ID; }
            set { _SRVC_LVL_ID = value; }
        }

        private string _NW_ADR;

        public string NW_ADR
        {
            get { return _NW_ADR; }
            set { _NW_ADR = value; }
        }

        private Int16 _VAS_QTY;

        public Int16 VAS_QTY
        {
            get { return _VAS_QTY; }
            set { _VAS_QTY = value; }
        }

        #endregion "Fields & Properties"

        public FSA_ORDR_VAS_DB(int _OrderID)
        {
            ORDR_ID = _OrderID;
        }

        public int InsertFSAOrderVASInfo(BILL_LINE_ITEM_DB _Bill, bool _HasBillItem)
        {
            int VAS_ID = 0;

            setCommand("dbo.insertFSAOrderVASInfo");
            addIntParam("@ORDR_ID", _ORDR_ID);
            addStringParam("@VAS_TYPE_CD", _VAS_TYPE_CD);
            addStringParam("@SRVC_LVL_ID", _SRVC_LVL_ID);
            addStringParam("@NW_ADR", _NW_ADR);
            addSmallIntParam("@VAS_QTY", _VAS_QTY);

            addSmallIntParam("@BILL_ITEM_TYPE_ID", _Bill.BILL_ITEM_TYPE_ID);
            addStringParam("@BIC_CD", _Bill.BIC_CD);
            addStringParam("@LINE_ITEM_DES", _Bill.LINE_ITEM_DES);
            addSmallIntParam("@LINE_ITEM_QTY", _Bill.LINE_ITEM_QTY);
            addStringParam("@ITEM_CD", _Bill.ITEM_CD);
            addStringParam("@ITEM_DES", _Bill.ITEM_DES);
            addStringParam("@MRC_CHG_AMT", _Bill.MRC_CHG_AMT);
            addStringParam("@NRC_CHG_AMT", _Bill.NRC_CHG_AMT);
            addStringParam("@CHG_CD", _Bill.CHG_CD);
            addStringParam("@ACTN_CD", _Bill.ACTN_CD);
            addBooleanParam("@HasBill", _HasBillItem);
            addIntParam("@VAS_ID", VAS_ID, true);

            return execNoQueryOut("@VAS_ID", true);
        }
    }
}