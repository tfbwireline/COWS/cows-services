@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to report the status of the "peoplesoftbatch_svc" application 
REM component. An ERRORLEVEL of 0 will be returned if the component is running,
REM and a non-zero ERRORLEVEL will be returned if the component is not running
REM or if the status cannot be determined.
REM 
REM *******************************************************************************



REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%


REM
REM retrieve the startup mode and exit if it is set to 'Disabled'
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd PSOFT_BATCH_SVC_STARTUP_TYPE START_MODE
CALL "%APP_HOME%"\Scripts\_getconfig.cmd LOGFILE_HOME COWS_LOG_PATH
IF ERRORLEVEL 1 (
  ECHO ERROR: unable to retrieve PSOFT_BATCH_SVC_STARTUP_TYPE from configuration
  
)
IF "%START_MODE%" EQU "Disabled" (
  echo INFO: service start mode is set to 'Disabled'.  
)
CALL "%APP_HOME%\Scripts\_getconfig.cmd" SVC_ENV_PREFIX
SET SERVICE_NAME=%SVC_ENV_PREFIX%COWS.PeopleSoftBatchService


REM
REM invoke service status script to get status
REM
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_getServiceStatus.js %SERVICE_NAME%
