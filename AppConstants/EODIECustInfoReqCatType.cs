﻿namespace AppConstants
{
    public enum EODIECustInfoReqCatType
    {
        DataConverged = 1,
        VoiceConverged,
        DataVoiceConverged
    }
}