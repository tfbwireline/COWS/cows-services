﻿namespace AppConstants
{
    public enum ELocalAccessType
    {
        DS3,
        E1,
        Other,
        STM1,
        T1
    }
}