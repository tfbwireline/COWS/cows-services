﻿using DataManager;
using System.Data;

namespace COWS.PeopleSoftBatchService
{
    public class PeopleSoftBatchDB : MSSqlBase
    {
        public DataSet getPeopleSoftReqHdr()
        {
            setCommand("dbo.getPeopleSoftReqHdr");
            return getDataSet();
        }

        public DataSet getPeopleSoftLnItms(string recHdr)
        {
            setCommand("dbo.getPeopleSoftLnItms");
            addStringParam("@REQSTN_NBR", recHdr);

            return getDataSet();
        }

        public DataTable getPeopleSoftBatchSeq(string _type)
        {
            setCommand("dbo.getPeopleSoftBatchSeq");
            addStringParam("@Type", _type);

            return getDataTable();
        }

        public void updateHdrSent(int ORDR_ID, string REQSTN_NBR, int _seq)
        {
            setCommand("dbo.updatePS_REQ_HDR");
            addIntParam("@ORDR_ID", ORDR_ID);
            addStringParam("@REQSTN_NBR", REQSTN_NBR);
            addIntParam("@BATCH_SEQ", _seq);

            execNoQuery();
        }

        public void updateLineItem(int FSA_CPE_LINE_ITEM_ID, string REQSTN_NBR, int _lineNbr)
        {
            setCommand("dbo.updatePS_REQ_LINE_ITM");
            addIntParam("@FSA_CPE_LINE_ITEM_ID", FSA_CPE_LINE_ITEM_ID);
            addStringParam("@REQSTN_NBR", REQSTN_NBR);
            addIntParam("@REQ_LINE_NBR", _lineNbr);

            execNoQuery();
        }

        public DataSet getPeopleSoftPORcvd()
        {
            setCommand("dbo.getPeopleSoftPORcvd");
            return getDataSet();
        }

        public void updateRcvdSent(int PO_RCVD_ID, int _seq)
        {
            setCommand("dbo.updatePS_PO_RCVD");
            addIntParam("@PO_RCVD_ID", PO_RCVD_ID);
            addIntParam("@BATCH_SEQ", _seq);

            execNoQuery();
        }

        public void getPO_Nbrs()
        {
            setCommand("dbo.insertPO_Numbers");
            execNoQuery();
        }
    }
}