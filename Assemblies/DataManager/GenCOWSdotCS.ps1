param([string]$path, [Parameter(Mandatory=$false)] [string]$env)
try {
$path = $path -replace "~", " "
$envconfigpath = ($path -replace "Assemblies\\DataManager\\", "_build\env.config")
If ($env.length -le 0) {
Write-Host "Please enter a Valid Environment as argument/parameter."
}Else{
If (Test-Path $envconfigpath){
[xml]$envconf = Get-Content $envconfigpath
If ($envconf.ENV.$env.DB -le 0) {
Write-Host "Please make sure you have a valid DB value along with Port# for the environment you entered."
}
Else {
If (Test-Path 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.8 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.8 Tools\sqlmetal.exe'}
Else { If (Test-Path 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.7.2 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.7.2 Tools\sqlmetal.exe'}
Else { If (Test-Path 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.7 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.7 Tools\sqlmetal.exe'}
Else { If (Test-Path 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sqlmetal.exe'}
Else { If (Test-Path 'C:\Program Files\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sqlmetal.exe'}
Else  {If (Test-Path 'C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\sqlmetal.exe'}
Else  { If (Test-Path 'C:\Program Files\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\sqlmetal.exe')
{$prog = $finalprog = 'C:\Program Files\Microsoft SDKs\Windows\v8.1A\bin\NETFX 4.5.1 Tools\sqlmetal.exe'}}}}}}}
 
$AllArgs = ' /server:"'+$envconf.ENV.$env.DB+'" /database:COWS /code:"'+ $path + 'COWS.cs" /language:csharp /sprocs /views /functions'
If ((Test-Path $prog) -or (Test-Path $finalprog)){
  #Write-Host $AllArgs
  $process = Start-Process -NoNewWindow -PassThru -Wait $finalprog -ArgumentList $AllArgs
  #Write-Host $process.ExitCode
  if ($process.ExitCode -ne 0)
  {
	Write-Host "Please check your server name and port, also make sure the database exists with appropriate access."
  }Else{
  $FilePath = $path + 'COWS.cs'
  #Write-Host $FilePath
  (Get-Content $FilePath).replace('[global::System.Data.Linq.Mapping.ColumnAttribute(', '[global::System.Data.Linq.Mapping.ColumnAttribute(UpdateCheck=UpdateCheck.Never, ').replace(', UpdateCheck=UpdateCheck.Never)]',')]')  | Set-Content  ($Filepath)
  
  $textToAdd = '
  
	[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.deleteMDSSlotAvailibility")]
	[return: global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")]
	public int DeleteMDSSlotAvailibility([global::System.Data.Linq.Mapping.ParameterAttribute(Name="ASN_USER_ID", DbType="Int")] System.Nullable<int> aSN_USER_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EVENT_ID", DbType="Int")] System.Nullable<int> eVENT_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="OLD_STRT_TMST", DbType="DateTime")] System.Nullable<System.DateTime> oLD_STRT_TMST, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="OLD_TME_SLOT_ID", DbType="TinyInt")] System.Nullable<byte> oLD_TME_SLOT_ID)
	{
		IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), aSN_USER_ID, eVENT_ID, oLD_STRT_TMST, oLD_TME_SLOT_ID);
		return ((int)(result.ReturnValue));
	}

	[global::System.Data.Linq.Mapping.FunctionAttribute(Name="web.updateMDSFTSlots")]
	[return: global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")]
	public int Web_UpdateMDSFTSlots([global::System.Data.Linq.Mapping.ParameterAttribute(Name="EVENT_ID", DbType="Int")] System.Nullable<int> eVENT_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="MDS_FAST_TRK_TYPE_ID", DbType="CHAR(1)")] string mDS_FAST_TRK_TYPE_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EVENT_TITLE_TXT", DbType="VARCHAR(255)")] string eVENT_TITLE_TXT, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="STRT_TMST", DbType="DateTime")] System.Nullable<System.DateTime> sTRT_TMST, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="CNFRC_BRDG_NBR", DbType="VARCHAR(50)")] string cNFRC_BRDG_NBR, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="CNFRC_PIN_NBR", DbType="VARCHAR(20)")] string cNFRC_PIN_NBR, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="CREAT_BY_USER_ID", DbType="Int")] System.Nullable<int> cREAT_BY_USER_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="TME_SLOT_ID", DbType="CHAR(2)")] string tME_SLOT_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="WRKFLW_STUS_ID", DbType="TinyInt")] System.Nullable<byte> wRKFLW_STUS_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EVENT_STUS_ID", DbType="TinyInt")] System.Nullable<byte> eVENT_STUS_ID, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdateCd", DbType="BIT")] System.Nullable<bool> updateCd)
	{
		IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), eVENT_ID, mDS_FAST_TRK_TYPE_ID, eVENT_TITLE_TXT, sTRT_TMST, cNFRC_BRDG_NBR, cNFRC_PIN_NBR, cREAT_BY_USER_ID, tME_SLOT_ID, wRKFLW_STUS_ID, eVENT_STUS_ID, updateCd);
		return ((int)(result.ReturnValue));
	}
	
	'
$Match = [regex]::Escape('[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.deletePartialFSAOrder")]')
$Content = Get-Content $FilePath
$Index = ($Content | Select-String -Pattern $Match).LineNumber - 2
#Write-Host $Index
$Content[$Index] = $textToAdd
$Content | Set-Content $FilePath
  
  Write-Host "COWS.cs successfuly generated."}
}Else{
  Write-Host "You do not have sqlmetal.exe installed, it is part of visual studio 2013 professional. Please download visual studio and try again."
}# if sqlmetal
}# if env.DB
}
Else{
Write-Host "Please make sure _build\env.config file exists on your system."
}# if envconfig exists
}# if env length
} catch {
  throw $_
  Break
}