﻿namespace AppConstants
{
    public enum eODIEMsgTypes : int
    {
        ODIECustomerNameRequest = 1,
        ODIECustomerInfoRequest = 2,
        ODIEDesignComplete = 3,
        ODIEDiscoResponse = 4,
        ODIECPEInfo = 5,
        ODIECPEStus = 6,
        ODIEShortNameValidationRequest = 7
    }
}