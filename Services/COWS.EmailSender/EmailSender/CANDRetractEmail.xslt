﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align ="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(/EventMail/CalenderEntryURL, '?eid=', /EventMail/EventID, '&amp;mode=0')" />
                </xsl:attribute>
                Click here to open the event, find it in your outlook calendar and remove it
              </a>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(/EventMail/ViewEventURL, '?eid=', /EventMail/EventID, '&amp;email=1')" />
                </xsl:attribute>
                Click here to view Event
              </a>
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/EventID" />
            </td>
          </tr>
          <xsl:if test="/EventMail/SOWSEventID[.!='']">
            <tr>
              <td width="20%" style="text-align: left; font-weight: 600">SOWS Event ID : </td>
              <td width="80%">
                <xsl:value-of select="/EventMail/SOWSEventID" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event Title : </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/EventTitle" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event is retracted by </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/ModUserName" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>