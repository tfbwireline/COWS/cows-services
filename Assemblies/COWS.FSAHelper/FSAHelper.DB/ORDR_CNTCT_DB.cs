﻿using DataManager;
using System;

namespace FSAHelper
{
    public class ORDR_CNTCT_DB : MSSqlBase
    {
        #region "Fields and Properies"

        private int _ORDR_CNTCT_ID = 0;

        public int ORDR_CNTCT_ID
        {
            get { return _ORDR_CNTCT_ID; }
            set { _ORDR_CNTCT_ID = value; }
        }

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private Byte _CNTCT_TYPE_ID;

        public Byte CNTCT_TYPE_ID
        {
            get { return _CNTCT_TYPE_ID; }
            set { _CNTCT_TYPE_ID = value; }
        }

        private string _FRST_NME;

        public string FRST_NME
        {
            get { return _FRST_NME; }
            set { _FRST_NME = value; }
        }

        private string _LST_NME;

        public string LST_NME
        {
            get { return _LST_NME; }
            set { _LST_NME = value; }
        }

        private string _NME;

        public string NME
        {
            get { return _NME; }
            set { _NME = value; }
        }

        private string _PHN_NBR;

        public string PHN_NBR
        {
            get { return _PHN_NBR; }
            set { _PHN_NBR = value; }
        }

        private string _PHN_EXT_NBR;

        public string PHN_EXT_NBR
        {
            get { return _PHN_EXT_NBR; }
            set { _PHN_EXT_NBR = value; }
        }

        private string _EMAIL_ADR;

        public string EMAIL_ADR
        {
            get { return _EMAIL_ADR; }
            set { _EMAIL_ADR = value; }
        }

        private string _TME_ZONE_ID;

        public string TME_ZONE_ID
        {
            get { return _TME_ZONE_ID; }
            set { _TME_ZONE_ID = value; }
        }

        private bool _INTPRTR_CD = false;

        public bool INTPRTR_CD
        {
            get { return _INTPRTR_CD; }
            set { _INTPRTR_CD = value; }
        }

        private string _FAX_NBR;

        public string FAX_NBR
        {
            get { return _FAX_NBR; }
            set { _FAX_NBR = value; }
        }

        private Byte _ROLE_ID;

        public Byte ROLE_ID
        {
            get { return _ROLE_ID; }
            set { _ROLE_ID = value; }
        }

        private string _CIS_LVL_TYPE;

        public string CIS_LVL_TYPE
        {
            get { return _CIS_LVL_TYPE; }
            set { _CIS_LVL_TYPE = value; }
        }

        private string _FSA_MDUL_ID;

        public string FSA_MDUL_ID
        {
            get { return _FSA_MDUL_ID; }
            set { _FSA_MDUL_ID = value; }
        }

        private string _NPA;

        public string NPA
        {
            get { return _NPA; }
            set { _NPA = value; }
        }

        private string _NXX;

        public string NXX
        {
            get { return _NXX; }
            set { _NXX = value; }
        }

        private string _STN_NBR;

        public string STN_NBR
        {
            get { return _STN_NBR; }
            set { _STN_NBR = value; }
        }

        private string _CTY_CD;

        public string CTY_CD
        {
            get { return _CTY_CD; }
            set { _CTY_CD = value; }
        }

        private string _ISD_CD;

        public string ISD_CD
        {
            get { return _ISD_CD; }
            set { _ISD_CD = value; }
        }

        private string _CNTCT_HR_TXT;

        public string CNTCT_HR_TXT
        {
            get { return _CNTCT_HR_TXT; }
            set { _CNTCT_HR_TXT = value; }
        }

        private string _SUPPD_LANG_NME;

        public string SUPPD_LANG_NME
        {
            get { return _SUPPD_LANG_NME; }
            set { _SUPPD_LANG_NME = value; }
        }

        private string _FSA_TME_ZONE_CD;

        public string FSA_TME_ZONE_CD
        {
            get { return _FSA_TME_ZONE_CD; }
            set { _FSA_TME_ZONE_CD = value; }
        }

        #endregion "Fields and Properies"

        public ORDR_CNTCT_DB(int _OrderID, string _ModuleID)
        {
            ORDR_ID = _OrderID;
            FSA_MDUL_ID = _ModuleID;
        }

        public int InsertOrderContactInfo()
        {
            int _RetVal = -1;
            if (_CNTCT_TYPE_ID != 0)
            {
                setCommand("dbo.insertOrderContactInfo");
                addIntParam("@ORDR_CNTCT_ID", _ORDR_CNTCT_ID, true);
                addIntParam("@ORDR_ID", _ORDR_ID);
                addTinyIntParam("@CNTCT_TYPE_ID", _CNTCT_TYPE_ID);
                addStringParam("@FRST_NME", _FRST_NME);
                addStringParam("@LST_NME", _LST_NME);
                addStringParam("@NME", _NME);
                addStringParam("@PHN_NBR", _PHN_NBR);
                addStringParam("@EMAIL_ADR", _EMAIL_ADR);
                addStringParam("@TME_ZONE_ID", _TME_ZONE_ID);
                addBooleanParam("@INTPRTR_CD", _INTPRTR_CD);
                addIntParam("@CREAT_BY_USER_ID", 1);
                addStringParam("@FAX_NBR", _FAX_NBR);
                addTinyIntParam("@ROLE_ID", _ROLE_ID);
                addStringParam("@CIS_LVL_TYPE", _CIS_LVL_TYPE);
                addStringParam("@FSA_MDUL_ID", _FSA_MDUL_ID);
                addStringParam("@NPA", _NPA);
                addStringParam("@NXX", _NXX);
                addStringParam("@STN_NBR", _STN_NBR);
                addStringParam("@CTY_CD", _CTY_CD);
                addStringParam("@ISD_CD", _ISD_CD);
                addStringParam("@PHN_EXT_NBR", _PHN_EXT_NBR);
                addStringParam("@FSA_TME_ZONE_CD", _FSA_TME_ZONE_CD);
                addStringParam("@SUPPD_LANG_NME", _SUPPD_LANG_NME);
                addStringParam("@CNTCT_HR_TXT", _CNTCT_HR_TXT);

                _RetVal = execNoQuery();
            }
            return _RetVal;
        }
    }
}