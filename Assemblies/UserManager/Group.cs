﻿using System;

namespace UserManager
{
    [Serializable()]
    public class Group
    {
        private Role _GOM;

        public Role GOM
        {
            get { return _GOM; }
            set { _GOM = value; }
        }

        private Role _MDS;

        public Role MDS
        {
            get { return _MDS; }
            set { _MDS = value; }
        }

        private Role _CSC;

        public Role CSC
        {
            get { return _CSC; }
            set { _CSC = value; }
        }

        private Role _SalesSupport;

        public Role SalesSupport
        {
            get { return _SalesSupport; }
            set { _SalesSupport = value; }
        }

        private Role _xNCIAsia;

        public Role xNCIAsia
        {
            get { return _xNCIAsia; }
            set { _xNCIAsia = value; }
        }

        private Role _xNCIAm;

        public Role xNCIAm
        {
            get { return _xNCIAm; }
            set { _xNCIAm = value; }
        }

        private Role _xNCIEurope;

        public Role xNCIEurope
        {
            get { return _xNCIEurope; }
            set { _xNCIEurope = value; }
        }

        private Role _WBO;

        public Role WBO
        {
            get { return _WBO; }
            set { _WBO = value; }
        }

        private Role _ReadOnly;

        public Role ReadOnly
        {
            get { return _ReadOnly; }
            set { _ReadOnly = value; }
        }

        /*private Role _SalesEng;

        public Role SalesEng
        {
            get { return _SalesEng; }
            set { _SalesEng = value; }
        }*/

        private Role _CAND;

        public Role CAND
        {
            get { return _CAND; }
            set { _CAND = value; }
        }

        private Role _SIPTnUCaaS;

        public Role SIPTnUCaaS
        {
            get { return _SIPTnUCaaS; }
            set { _SIPTnUCaaS = value; }
        }

        private Role _DCPE;

        public Role DCPE
        {
            get { return _DCPE; }
            set { _DCPE = value; }
        }

        private Role _VCPE;

        public Role VCPE
        {
            get { return _VCPE; }
            set { _VCPE = value; }
        }

        private Role _DBB;

        private Role _CPETECH;

        public Role CPETECH
        {
            get { return _CPETECH; }
            set { _CPETECH = value; }
        }

        public Role DBB
        {
            get { return _DBB; }
            set { _DBB = value; }
        }

        private bool _bCAND = false;

        public bool bCAND
        {
            get { return _bCAND; }
            set { _bCAND = value; }
        }

        private bool _bReadOnly = false;

        public bool bReadOnly
        {
            get { return _bReadOnly; }
            set { _bReadOnly = value; }
        }

        private bool _bGOM = false;

        public bool bGOM
        {
            get { return _bGOM; }
            set { _bGOM = value; }
        }

        private bool _bMDS = false;

        public bool bMDS
        {
            get { return _bMDS; }
            set { _bMDS = value; }
        }

        private bool _bCSC = false;

        public bool bCSC
        {
            get { return _bCSC; }
            set { _bCSC = value; }
        }

        private bool _bSalesSupport = false;

        public bool bSalesSupport
        {
            get { return _bSalesSupport; }
            set { _bSalesSupport = value; }
        }

        private bool _bxNCIAsia = false;

        public bool bxNCIAsia
        {
            get { return _bxNCIAsia; }
            set { _bxNCIAsia = value; }
        }

        private bool _bxNCIAm = false;

        public bool bxNCIAm
        {
            get { return _bxNCIAm; }
            set { _bxNCIAm = value; }
        }

        private bool _bxNCIEurope = false;

        public bool bxNCIEurope
        {
            get { return _bxNCIEurope; }
            set { _bxNCIEurope = value; }
        }

        private bool _bWBO = false;

        public bool bWBO
        {
            get { return _bWBO; }
            set { _bWBO = value; }
        }

        private bool _bSalesEng = false;

        public bool bSalesEng
        {
            get { return _bSalesEng; }
            set { _bSalesEng = value; }
        }

        private bool _bSIPTnUCaaS = false;

        public bool bSIPTnUCaaS
        {
            get { return _bSIPTnUCaaS; }
            set { _bSIPTnUCaaS = value; }
        }

        private bool _bDCPE = false;

        public bool bDCPE
        {
            get { return _bDCPE; }
            set { _bDCPE = value; }
        }

        private bool _bVCPE = false;

        public bool bVCPE
        {
            get { return _bVCPE; }
            set { _bVCPE = value; }
        }

        private bool _bCPETECH = false;

        public bool bCPETECH
        {
            get { return _bCPETECH; }
            set { _bCPETECH = value; }
        }

        private bool _bDBB = false;

        public bool bDBB
        {
            get { return _bDBB; }
            set { _bDBB = value; }
        }

        public Group()
        {
            CAND = new Role();
            CSC = new Role();
            GOM = new Role();
            MDS = new Role();
            //SalesEng = new Role();
            SalesSupport = new Role();
            WBO = new Role();
            xNCIAm = new Role();
            xNCIAsia = new Role();
            xNCIEurope = new Role();
            SIPTnUCaaS = new Role();
            DCPE = new Role();
            VCPE = new Role();
            CPETECH = new Role();
            DBB = new Role();
        }
    }
}