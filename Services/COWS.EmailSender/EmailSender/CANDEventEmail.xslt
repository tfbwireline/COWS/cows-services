﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:include href="style.xslt" />
  <xsl:template match="/">
    <html>
      <body>
        <table width="920" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="5">
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/EventID" />
            </td>
          </tr>
          <xsl:if test="/EventMail/SOWSEventID[.!='']">
            <tr>
              <td width="20%" style="text-align: left; font-weight: 600">SOWS Event ID : </td>
              <td width="80%">
                <xsl:value-of select="/EventMail/SOWSEventID" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event Title : </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/EventTitle" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Event Type : </td>
            <td width="80%">
              <xsl:value-of select="/EventMail/EventType" />
            </td>
          </tr>
          <tr>
            <td width="20%" style="text-align: left; font-weight: 600">Scheduled Event Date &amp; Time : </td>
            <td width="80%">
              <xsl:value-of select="concat(/EventMail/StartDtTm, ' CT')" />
            </td>
          </tr>
          <xsl:if test="/EventMail/EndEStatusDes[.!='Rework']">
            <tr>
              <td width="20%" style="text-align: left; font-weight: 600">Assigned Activators : </td>
              <td width="80%">
                <xsl:value-of select="/EventMail/AssignAct" />
              </td>
            </tr>
            <xsl:if test="/EventMail/EndEStatusDes[.!='Completed']">
              <tr>
                <td colspan="2">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:value-of select="concat(/EventMail/CalenderEntryURL, '?eid=', /EventMail/EventID, '&amp;mode=2')" />
                    </xsl:attribute>
                    Click here to update your outlook calendar
                  </a>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <tr>
            <td colspan="2">
              <xsl:value-of select="concat('Event set to ', translate(/EventMail/EndEStatusDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Status from ', translate(/EventMail/StartEStatusDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Status using ', translate(/EventMail/CommandDes, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), ' Command by ', /EventMail/ModUserName ,' with the following comments')" /> :
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <xsl:value-of select="/EventMail/Comments" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(/EventMail/ViewEventURL, '?eid=', /EventMail/EventID, '&amp;email=1')" />
                </xsl:attribute>
                Click here to view Event
              </a>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>