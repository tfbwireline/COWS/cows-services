using System;
using System.Threading;
using System.Runtime;
using System.Runtime.Caching;

namespace CacheManager
{
    public class CacheObj
    {
        /// <summary>
        /// Cache Object
        /// </summary>
        public static ObjectCache Cache
        {
            get
            {
                return MemoryCache.Default;
            }
        }

        public static void AddCacheItem(string sKey, Object oVal, int iMinutes)
        {
            if (oVal != null)
            {
                if (Cache[sKey] != null)
                    Cache.Remove(sKey);
                Cache.Set(sKey, oVal, new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTime.Now.AddMinutes(iMinutes)
                });
            }
        }

        public static void RemoveCacheItem(string sKey)
        {
            if (Cache[sKey] != null)
                Cache.Remove(sKey);
        }
    }
}