﻿using DataManager;
using System;

namespace FSAHelper
{
    public class ORDR_NTE_DB : MSSqlBase
    {
        #region "Fields and Properties"

        private int _ORDR_ID = 0;

        public int ORDR_ID
        {
            get { return _ORDR_ID; }
            set { _ORDR_ID = value; }
        }

        private Byte _NTE_TYPE_ID;

        public Byte NTE_TYPE_ID
        {
            get { return _NTE_TYPE_ID; }
            set { _NTE_TYPE_ID = value; }
        }

        private string _NTE_TXT;

        public string NTE_TXT
        {
            get { return _NTE_TXT; }
            set { _NTE_TXT = value; }
        }

        #endregion "Fields and Properties"

        public ORDR_NTE_DB(int _OrderID)
        {
            ORDR_ID = _OrderID;
        }

        public int InsertOrderNotes()
        {
            setCommand("dbo.insertOrderNotes");
            addIntParam("@ORDR_ID", _ORDR_ID);
            addTinyIntParam("@NTE_TYPE_ID", _NTE_TYPE_ID);
            addStringParam("@NTE_TXT", _NTE_TXT);

            return execNoQuery();
        }
    }
}