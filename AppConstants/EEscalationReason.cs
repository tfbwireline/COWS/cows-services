﻿namespace AppConstants
{
    public enum EEscalationReason
    {
        NoResourceAvailableDueToTime = 1,
        NoQualifiedResourceAvailable,
        ShorterInterval,
        CustomerRequest,
        ManagementRequest,
        Other,
        OptingOutofFT
    }
}