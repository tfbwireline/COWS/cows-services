using EmailManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserManager;
using fl = LogManager.FileLogger;

namespace UIManager
{
    public class PageBase : System.Web.UI.Page
    {
        private ObjectStateFormatter _formatter = new ObjectStateFormatter();
        public string spath = string.Empty;

        public PageBase()
        {
        }

        public string GetUserIPAddress()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                if (ipList.Contains(","))
                    return ipList.Split(',')[0];
                else
                    return ipList;
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }

        public void SetUserInfo()
        {
            try
            {
                if (!(Request.Url.AbsoluteUri.Contains("email=1")))
                {
                    if (((Request.Url.AbsoluteUri.ToLower().Contains("default.aspx") && (!(Request.Url.AbsoluteUri.ToLower().Contains("?smn="))))
                        || (((Request.Url.AbsoluteUri.ToLower().Contains("?smn="))) && (this.Session["UserInfo"] == null)))
                            || (Request.Url.AbsoluteUri.ToLower().Contains("sdenew.aspx") && (this.Session["UserInfo"] == null)))
                    {
                        User UserInfo;
                        UserInfo = new User();
                        UserInfo = UserLibrary.CheckOutUser(1, this.Session.SessionID, GetUserIPAddress());
                        this.Session["UserInfo"] = UserInfo;
                        this.Session["UserName"] = UserInfo.UserName;
                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, Request.Url.AbsoluteUri + " : " + Request.Browser.Browser + " : " + Request.Browser.Version + " : " + Request.Browser.MajorVersion + " : " + Request.Browser.MinorVersion + " : " + Request.Browser.Platform + " : " + Request.Browser.Type + " : " + Request.Browser["JavaScriptVersion"] + " : " + this.Session.SessionID + " : " + UserInfo.UserName, string.Empty);
                    }
                    else if (this.Session["UserInfo"] == null)
                    {
                        //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, "In EndSession", string.Empty);
                        //redirects the user to the message
                        if (!(Request.Url.AbsoluteUri.ToLower().Contains("reports")))
                        {
                            Response.Redirect("~/EndSession.aspx?RefURL=" + Server.UrlEncode(Request.Url.AbsoluteUri), false);
                            Context.ApplicationInstance.CompleteRequest();
                        }

                        //Redirect calls a thread abort if you don't use the overload, so the code below is never hit.
                        //The suggested way to handle this would be to pass false as the overloaded method and then
                        //call the HttpApplication.CompleteRequest() instead of raising the abort as we are doing,
                        //but this allows the page methods to continue being executed.  Since the user session has timed
                        //out, we don't want the page to continue processing.

                        //User UserInfo;
                        //UserInfo = new User();
                        //UserInfo = UserLibrary.CheckOutUser(0);
                        //this.Session["UserInfo"] = UserInfo;
                        //this.Session["UserName"] = UserInfo.UserName;
                    }
                }
                else
                {
                    User UserInfo;
                    UserInfo = new User();
                    UserInfo = UserLibrary.CheckOutUser(1, this.Session.SessionID, GetUserIPAddress());
                    this.Session["UserInfo"] = UserInfo;
                    this.Session["UserName"] = UserInfo.UserName;
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, Request.Url.AbsoluteUri + " : " + Request.Browser.Browser + " : " + Request.Browser.Version + " : " + Request.Browser.MajorVersion + " : " + Request.Browser.MinorVersion + " : " + Request.Browser.Platform + " : " + Request.Browser.Type + " : " + Request.Browser["JavaScriptVersion"] + " : " + this.Session.SessionID + " : " + UserInfo.UserName, string.Empty);
                }
            }
            catch (ThreadAbortException)
            { }
            catch (Exception ex)
            {
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
            }
        }

        public User GetUserInfo()
        {
            return (User)this.Session["UserInfo"];
        }

        public void ClearSessionData()
        {
            Session["APPT_LKP_INFO"] = null;
            Session["dtCalendars"] = null;
            Session["EID"] = null;
            Session["UID"] = null;
            Session["MDSConferenceInfo"] = null;

            Session["ActiveOrderID"] = null;
            Session["ActiveTaskStatus"] = null;
            Session["CurrentNote"] = null;
            Session["bGOMExists"] = null;
            Session["GOMSpecificOrderData"] = null;
            Session["SelectedWGRow"] = null;
            Session["FTNList"] = null;
            Session["FTN"] = null;
            Session["SelectedEventDsplVwID"] = null;
            Session["IsCANDUser"] = null;
            Session["IsSIPTUser"] = null;
            Session["IsCANDAdmin"] = null;
        }

        public void chkPermission()
        {
            try
            {
                if (Request.Url.AbsolutePath.ToString().Contains("admin"))
                {
                    if (((User)this.Session["UserInfo"]).UserGrp.CAND.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.CSC.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.GOM.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.MDS.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.SIPTnUCaaS.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.SalesSupport.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.WBO.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.xNCIAm.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.xNCIAsia.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.xNCIEurope.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.DCPE.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.VCPE.Admin.Equals("1") ||
                        ((User)this.Session["UserInfo"]).UserGrp.CPETECH.Admin.Equals("1") ||
                        Request.Url.AbsolutePath.ToString().Contains("Preferences.aspx") ||
                        (Request.Url.AbsolutePath.ToString().Contains("Qualifications.aspx") && (
                                ((User)this.Session["UserInfo"]).UserGrp.CAND.ESActivator.Equals("1") || ((User)this.Session["UserInfo"]).UserGrp.SIPTnUCaaS.ESActivator.Equals("1")
                                || ((User)this.Session["UserInfo"]).UserGrp.CSC.ESActivator.Equals("1") || ((User)this.Session["UserInfo"]).UserGrp.GOM.ESActivator.Equals("1")
                                || ((User)this.Session["UserInfo"]).UserGrp.MDS.ESActivator.Equals("1") || ((User)this.Session["UserInfo"]).UserGrp.SalesSupport.ESActivator.Equals("1")
                                || ((User)this.Session["UserInfo"]).UserGrp.WBO.ESActivator.Equals("1") || ((User)this.Session["UserInfo"]).UserGrp.xNCIAm.ESActivator.Equals("1")
                                || ((User)this.Session["UserInfo"]).UserGrp.xNCIAsia.ESActivator.Equals("1") || ((User)this.Session["UserInfo"]).UserGrp.xNCIEurope.ESActivator.Equals("1")
                                || ((User)this.Session["UserInfo"]).UserGrp.MDS.MNSActivator.Equals("1")
                            )
                        )
                    )
                    {
                    }
                    else
                    {
                        if (!(Request.Url.AbsoluteUri.ToLower().Contains("reports")))
                        {
                            Session["SorryMsg"] = "We are sorry, you do not have access for this page. Please contact your supervisor or application admin.";
                            Response.Redirect("~\\Sorry.aspx", false);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, exp.StackTrace + " : " + exp.Message + Request.Url.AbsolutePath.ToString(), string.Empty);
            }
        }

        protected override void SavePageStateToPersistenceMedium(object viewState)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                _formatter.Serialize(ms, viewState);
                ClientScript.RegisterHiddenField("__COMPRESSEDVIEWSTATE", Convert.ToBase64String(CompressViewState.Compress(ms.ToArray())));
            }
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            try
            {
                if (Request.Form["__COMPRESSEDVIEWSTATE"] != null)
                {
                    return _formatter.Deserialize(Convert.ToBase64String(CompressViewState.Decompress(Convert.FromBase64String(Request.Form["__COMPRESSEDVIEWSTATE"].Replace(" ", "+")))));
                }
                else
                {
                    this.Session.Abandon();
                    return null;
                }
            }
            catch
            {
                this.Session.Abandon();

                return null;
            }
        }

        public void chkSession()
        {
            if ((this.Session["UserInfo"] == null) && (!(Request.Url.AbsoluteUri.ToLower().Contains("reports"))))
            {
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, "Session Timed Out", string.Empty);
                this.Session["SorryMsg"] = "Your Session has timed out. Please close your browser and log back in to COWS";
                Response.Redirect("~\\Sorry.aspx", false);
            }
        }

        public string formatPhone(string strPhone, Int16 noOfChars)
        {
            try
            {
                StringBuilder frmtPhone = new StringBuilder();

                if (noOfChars == 12)
                {
                    if (strPhone.Trim().IndexOf("-") >= 0)
                    {
                        return strPhone;
                    }
                    else
                    {
                        frmtPhone.Append(strPhone.Trim().Substring(0, 3));
                        frmtPhone.Append("-");
                        frmtPhone.Append(strPhone.Trim().Substring(3, 3));
                        frmtPhone.Append("-");
                        frmtPhone.Append(strPhone.Trim().Substring(6, 4));
                        return frmtPhone.ToString();
                    }
                }
                else if (noOfChars == 10)
                {
                    if (strPhone.Trim().IndexOf("-") < 0)
                    {
                        return strPhone;
                    }
                    else
                    {
                        return strPhone.ToString().Replace("-", string.Empty).Substring(0, 10);
                    }
                }
                else
                {
                    return strPhone;
                }
            }
            catch
            {
                return strPhone;
            }
        }

        protected override void OnError(EventArgs args)
        {
            try
            {
                //sendErrMail(Server.GetLastError());
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, Server.GetLastError().StackTrace + " : " + Server.GetLastError().Message, string.Empty);

                //Response.Redirect(@"~\\Sorry.aspx");
            }
            catch (Exception exp)
            {
                fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, exp.StackTrace + " : " + exp.Message, string.Empty);
                throw exp;
            }
        }

        protected void sendErrMail(Exception e)
        {
            string sMailTo = null;
            string sSubject = null;
            string sMailFrm = null;

            sMailFrm = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();
            sMailTo = ConfigurationManager.AppSettings["ErrorEmail"].ToString();
            sSubject = ConfigurationManager.AppSettings["COWSRegion"].ToString() + " : Error in the website";

            StringBuilder sBody = new StringBuilder("Message : " + e.Message.ToString() + "; StackTrace : " + e.StackTrace.ToString() + "; Source : " + e.Source.ToString());

            sBody.Insert(sBody.Length, "<br><br><br><font size=11 style=color:Red>DO NOT REPLY: This is an outbound email address only.</font>");

            EmailHelper._SendSingleEmail(true, MailPriority.High, sMailFrm, sMailTo, string.Empty, string.Empty, sSubject, sBody.ToString());
        }

        protected void sendErrMail(string e)
        {
            string sMailTo = null;
            string sSubject = null;
            string sMailFrm = null;

            sMailFrm = ConfigurationManager.AppSettings["DefaultFromEmail"].ToString();
            sMailTo = ConfigurationManager.AppSettings["ErrorEmail"].ToString();
            sSubject = ConfigurationManager.AppSettings["COWSRegion"].ToString() + " : Error in the website";

            StringBuilder sBody = new StringBuilder(e);

            sBody.Insert(sBody.Length, "<br><br><br><font size=11 style=color:Red>DO NOT REPLY: This is an outbound email address only.</font>");

            EmailHelper._SendSingleEmail(true, MailPriority.High, sMailFrm, sMailTo, string.Empty, string.Empty, sSubject, sBody.ToString());
        }

        /// <summary>
        /// loads deafult script. form must have id equals
        /// </summary>
        protected void RegisterOnCloseClientBlock()
        {
            HiddenField OnPageLeft = new HiddenField();
            OnPageLeft.ID = "OnPageLeft";
            Page.Form.Controls.Add(OnPageLeft);
            StringBuilder script = new StringBuilder();
            script.AppendLine("<script language='javascript' type='text/javascript'>");
            script.AppendLine("$(document).ready(function(){");
            script.AppendLine("$(window).unload(function() {");
            script.AppendLine(string.Format("$('#{0}').val('PageClosed');", OnPageLeft.ClientID));
            script.AppendLine("document.forms[0].submit();");
            script.AppendLine("});");
            script.AppendLine("});");
            script.AppendLine("</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OnPageLeavingScript", script.ToString());
        }

        /// <summary>
        /// loads deafult script. form must have id equals
        /// </summary>
        protected void RegisterOnCloseClientBlock(HiddenField OnPageLeaving)
        {
            StringBuilder script = new StringBuilder();
            script.AppendLine("<script language='javascript' type='text/javascript'>");
            script.AppendLine("$(document).ready(function(){");
            script.AppendLine("$(window).unload(function() {");
            script.AppendLine(string.Format("$('#{0}').val('PageClosed');", OnPageLeaving.ClientID));
            script.AppendLine("document.forms[0].submit();");
            script.AppendLine("});");
            script.AppendLine("});");
            script.AppendLine("</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OnPageLeavingScript", script.ToString());
        }

        /// <summary>
        /// loads deafult script. form must have id equals
        /// </summary>
        protected bool IsPageClosing()
        {
            HiddenField OnPageLeft = (HiddenField)Page.FindControl("OnPageLeft");
            return OnPageLeft.Value == "PageClosed";
        }

        /// <summary>
        /// loads deafult script. form must have id equals
        /// </summary>
        protected bool IsPageClosing(HiddenField OnPageLeaving)
        {
            return OnPageLeaving.Value == "PageClosed";
        }

        /// <summary>
        /// custom script
        /// </summary>
        /// <param name="script"></param>
        protected void RegisterOnCloseClientBlock(string script)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OnPageLeavingScript", script.ToString());
        }

        /// <summary>
        /// Show Alert Js Message
        /// </summary>
        /// <param name="message"></param>
        protected void ShowAlertMessage(string message)
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "alert-message", string.Format("<script type='text/javascript' language='javascript'>alert('{0}');</script>", message));
        }

        public void DisableControls(Control parent)
        {
            DisableControls(parent, null, null, null);
        }

        public void DisableControls(Control parent, List<string> enableIdList)
        {
            DisableControls(parent, enableIdList, null, null);
        }

        public void DisableControls(Control parent, List<string> enableIdList, List<string> readOnlyIdList)
        {
            DisableControls(parent, enableIdList, readOnlyIdList, null);
        }

        public void DisableControls(Control parent, List<string> enableIdList, List<string> readOnlyIdList, List<string> skipList)
        {
            if (readOnlyIdList == null)
                readOnlyIdList = new List<string>();
            if (enableIdList == null)
                enableIdList = new List<string>();
            if (enableIdList.Count <= 0)
                enableIdList.Add("btnAdvSearch");

            if (enableIdList.Find(a => a == "txtSearchValue") == null)
                enableIdList.Add("txtSearchValue");
            if (enableIdList.Find(a => a == "ddlSearchField") == null)
                enableIdList.Add("ddlSearchField");
            if (enableIdList.Find(a => a == "btnSearchOk") == null)
                enableIdList.Add("btnSearchOk");
            if (enableIdList.Find(a => a == "btnSOWGo") == null)
                enableIdList.Add("btnSOWGo");
            if (enableIdList.Find(a => a == "rbSrch") == null)
                enableIdList.Add("rbSrch");
            //if (enableIdList.Find(a => a == "txtItemReceiveDt") == null)
            //    enableIdList.Add("txtItemReceiveDt");
            //if (enableIdList.Find(a => a == "txtOrderShipDt") == null)
            //    enableIdList.Add("txtOrderShipDt");
            //if (enableIdList.Find(a => a == "txtHdwShipDt") == null)
            //    enableIdList.Add("txtHdwShipDt");
            //if (enableIdList.Find(a => a == "txtUSDistDt") == null)
            //    enableIdList.Add("txtUSDistDt");
            //if (enableIdList.Find(a => a == "txtCustDelDt") == null)
            //    enableIdList.Add("txtCustDelDt");

            //string s = "";
            foreach (Control control in parent.Controls)
            {
                if ((skipList == null) || ((skipList.Count > 0) && (skipList.FindAll(a => control.ClientID.Contains(a)).Count <= 0)))
                {
                    //if (control.ClientID.Contains("txtOrderShipDt"))
                    //s = "";
                    if ((control is TextBox) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        if (readOnlyIdList.FindAll(a => control.ClientID.Contains(a)).Count > 0)
                            ((TextBox)control).ReadOnly = true;
                        else
                            ((TextBox)control).Enabled = false;
                    }
                    else if ((control is DropDownList) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((DropDownList)control).Enabled = false;
                    }
                    else if ((control is Button) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((Button)control).Enabled = false;
                    }
                    else if ((control is LinkButton) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((LinkButton)control).Enabled = false;
                    }
                    else if ((control is ImageButton) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((ImageButton)control).Enabled = false;
                    }
                    else if ((control is CheckBox) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((CheckBox)control).Enabled = false;
                    }
                    else if ((control is RadioButton) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((RadioButton)control).Enabled = false;
                    }
                    else if ((control is CheckBoxList) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((CheckBoxList)control).Enabled = false;
                    }
                    else if ((control is RadioButtonList) && (enableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((RadioButtonList)control).Enabled = false;
                    }
                    else
                    {
                        //Response.Write();
                    }
                    DisableControls(control, enableIdList, readOnlyIdList);
                }
            }
        }

        public void EnableControls(Control parent)
        {
            EnableControls(parent, null, null, null);
        }

        public void EnableControls(Control parent, List<string> disableIdList, List<string> readOnlyIdList)
        {
            EnableControls(parent, disableIdList, readOnlyIdList, null);
        }

        public void EnableControls(Control parent, List<string> disableIdList, List<string> readOnlyIdList, List<string> skipList)
        {
            if (readOnlyIdList == null)
                readOnlyIdList = new List<string>();
            if (disableIdList == null)
                disableIdList = new List<string>();
            foreach (Control control in parent.Controls)
            {
                if ((skipList == null) || ((skipList.Count > 0) && (skipList.FindAll(a => control.ClientID.Contains(a)).Count <= 0)))
                {
                    if ((control is TextBox) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        if (readOnlyIdList.FindAll(a => control.ClientID.Contains(a)).Count > 0)
                            ((TextBox)control).ReadOnly = false;
                        else
                            ((TextBox)control).Enabled = true;
                    }
                    else if ((control is DropDownList) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((DropDownList)control).Enabled = true;
                    }
                    else if ((control is Button) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((Button)control).Enabled = true;
                    }
                    else if ((control is LinkButton) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((LinkButton)control).Enabled = true;
                    }
                    else if ((control is ImageButton) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((ImageButton)control).Enabled = true;
                    }
                    else if ((control is CheckBox) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((CheckBox)control).Enabled = true;
                    }
                    else if ((control is RadioButton) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((RadioButton)control).Enabled = true;
                    }
                    else if ((control is CheckBoxList) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((CheckBoxList)control).Enabled = true;
                    }
                    else if ((control is RadioButtonList) && (disableIdList.FindAll(a => control.ClientID.Contains(a)).Count <= 0))
                    {
                        ((RadioButtonList)control).Enabled = true;
                    }
                    else
                    {
                        //Response.Write();
                    }
                    EnableControls(control, disableIdList, readOnlyIdList);
                }
            }
        }
    }
}