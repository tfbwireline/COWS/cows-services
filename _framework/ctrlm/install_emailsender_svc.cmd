@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to install the "emailsender_svc" application component as a
REM Windows service. An ERRORLEVEL of 0 will be returned if the service is installed
REM successfully, and a non-zero ERRORLEVEL will be returned if there are any errors.
REM 
REM *******************************************************************************


REM
REM initialize some variables and do some validation
REM
SETLOCAL
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%
SET SERVICE_EXE_PATH=%APP_HOME%\Application\Services\COWS.EmailSender\COWS.EmailSender.exe


REM
REM retrieve and validate the startup mode for the service and the service env prefix
REM
CALL "%APP_HOME%\Scripts\_getconfig.cmd" EMAILSENDER_SVC_STARTUP_TYPE START_MODE
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" install_emailsender_svc.cmd %ERRORLEVEL% "ERROR: unable to retrieve EMAILSENDER_SVC_STARTUP_TYPE from configuration"
  EXIT /b %ERRORLEVEL%
)
IF "%START_MODE%" NEQ "Automatic" (
  IF "%START_MODE%" NEQ "Manual" (
    IF "%START_MODE%" NEQ "Disabled" (
      ECHO ERROR: service start mode [%START_MODE%] is not correct - should be Automatic, Manual, or Disabled
      CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" install_emailsender_svc.cmd 4 "ERROR: service start mode [%START_MODE%] is not correct - should be Automatic, Manual, or Disabled"
      EXIT /b 4
    )
  )
)
CALL "%APP_HOME%\Scripts\_getconfig.cmd" SVC_ENV_PREFIX
SET SERVICE_NAME=%SVC_ENV_PREFIX%COWS.EmailSender


REM
REM perform the host check
REM 
CALL "%APP_HOME%\Scripts\perform-host-check.cmd"
IF ERRORLEVEL 1 (
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" install_emailsender_svc.cmd 5 "ERROR: host validation failed"
  EXIT /b 5
)


REM
REM retrieve service startup username/password (if applicable)
REM
CALL %APP_HOME%\Scripts\_getconfig.cmd @USERVICE_ID@_STARTUP_CREDENTIALS START_NAME_PWD
IF "%START_NAME_PWD%" NEQ "" SET START_NAME_PWD=-USER=%START_NAME_PWD%


REM
REM invoke service install script
REM
"%windir%"\system32\cscript.exe /Nologo "%APP_HOME%"\Scripts\_installService.js %SERVICE_NAME% %START_MODE% "%SERVICE_EXE_PATH%" %START_NAME_PWD%
