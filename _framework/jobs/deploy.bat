@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework job to @PREFIX@deploy the application. Calls the @PREFIX@deploy.cmd script
REM in the scripts subdirectory.
REM 
REM *******************************************************************************


REM
REM initialize application home and call @PREFIX@deploy script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
Scripts\@PREFIX@deploy.cmd @CFGARG@
