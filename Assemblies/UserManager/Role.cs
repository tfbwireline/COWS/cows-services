﻿using System;

namespace UserManager
{
    [Serializable()]
    public class Role
    {
        private string _OReviewer = "0";

        public string OReviewer
        {
            get { return _OReviewer; }
            set { _OReviewer = value; }
        }

        private string _OUpdater = "0";

        public string OUpdater
        {
            get { return _OUpdater; }
            set { _OUpdater = value; }
        }

        private string _ORTS = "0";

        public string ORTS
        {
            get { return _ORTS; }
            set { _ORTS = value; }
        }

        private string _EMember = "0";

        public string ESMember
        {
            get { return _EMember; }
            set { _EMember = value; }
        }

        private string _EReviewer = "0";

        public string ESReviewer
        {
            get { return _EReviewer; }
            set { _EReviewer = value; }
        }

        private string _EActivator = "0";

        public string ESActivator
        {
            get { return _EActivator; }
            set { _EActivator = value; }
        }

        private string _Admin = "0";

        public string Admin
        {
            get { return _Admin; }
            set { _Admin = value; }
        }

        private string _WFMAssigner = "0";

        public string WFMAssigner
        {
            get { return _WFMAssigner; }
            set { _WFMAssigner = value; }
        }

        private string _MNSPreConfigEng = "0";

        public string MNSPreConfigEng
        {
            get { return _MNSPreConfigEng; }
            set { _MNSPreConfigEng = value; }
        }

        private string _MNSActivator = "0";

        public string MNSActivator
        {
            get { return _MNSActivator; }
            set { _MNSActivator = value; }
        }

        private string _LCTPM = "0";

        public string LCTPM
        {
            get { return _LCTPM; }
            set { _LCTPM = value; }
        }

        private string _MDSPM = "0";

        public string MDSPM
        {
            get { return _MDSPM; }
            set { _MDSPM = value; }
        }

        private string _CannedRpt = "0";

        public string CannedRpt
        {
            get { return _CannedRpt; }
            set { _CannedRpt = value; }
        }

        private string _OnDemandRpt = "0";

        public string OnDemandRpt
        {
            get { return _OnDemandRpt; }
            set { _OnDemandRpt = value; }
        }

        private string _AdhocRpt = "0";

        public string AdhocRpt
        {
            get { return _AdhocRpt; }
            set { _AdhocRpt = value; }
        }

        private string _SalesDesignEngineer = "0";

        public string SalesDesignEngineer
        {
            get { return _SalesDesignEngineer; }
            set { _SalesDesignEngineer = value; }
        }

        private string _NetworkTestEngineer = "0";

        public string NetworkTestEngineer
        {
            get { return _NetworkTestEngineer; }
            set { _NetworkTestEngineer = value; }
        }

        private string _SystemSecurityEngineer = "0";

        public string SystemSecurityEngineer
        {
            get { return _SystemSecurityEngineer; }
            set { _SystemSecurityEngineer = value; }
        }

        private string _NOS = "0";

        public string NOS
        {
            get { return _NOS; }
            set { _NOS = value; }
        }

        private string _Gatekeeper = "0";

        public string Gatekeeper
        {
            get { return _Gatekeeper; }
            set { _Gatekeeper = value; }
        }

        private string _Manager = "0";

        public string Manager
        {
            get { return _Manager; }
            set { _Manager = value; }
        }

        public Role()
        {
        }
    }
}