@echo off
REM *******************************************************************************
REM Copyright (c) 2007 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM UDM framework script to write message to job log.
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%


REM
REM get command line args and do some validation
REM
SET CALLER=%1
SET CODE=%2
set MSG=%~3
if "%MSG%" EQU "" (
  ECHO ERROR: _logJobMsg.cmd requires 3 arguments!
  EXIT /B 3
)


REM
REM leverage existing WSH script to write the message
REM
"%windir%\system32\cscript.exe" /Nologo "%APP_HOME%\Scripts\_logJobMsg.js" %CALLER% %CODE% "%MSG%"
