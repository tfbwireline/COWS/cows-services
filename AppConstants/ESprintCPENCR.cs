﻿namespace AppConstants
{
    public enum ESprintCPENCR
    {
        NCR = 1,
        SprintCPE = 2,
        CPENotRequired = 3,
        CustomerInstall = 4,
        InterCPE = 5,
        Orange = 6,
        TATA = 7,
        ChinaTelecom = 8,
        Velociti = 9,
        ARRIS = 10,
        Acuative = 11,
        CommScope = 12
    }
}