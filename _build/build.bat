@echo OFF
REM #############################################################################
REM 
REM Copyright (c) 2010 Sprint Communications Company
REM 
REM The content of this document is PROPRIETARY INFORMATION, and is the sole
REM and exclusive property of Sprint. Information provided in this document is
REM classified as SPRINT RESTRICTED. Originator must grant permission before
REM reproductions of this document can be made. If you need to dispose of any
REM hard copies of this document, please shred it.
REM
REM #############################################################################


SETLOCAL
SET RELATIVEPATH=%~dp0
ECHO.
ECHO.
PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command "& '%~dp0\build.ps1'" -path %~dp0
%RELATIVEPATH%NAnt\bin\NAnt.exe -logfile:build.log -t:net-4.0 -buildfile:%RELATIVEPATH%\_framework.build %1 %2 %3 %4 %5 %6 %7 %8 %9
PAUSE