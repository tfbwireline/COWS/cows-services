﻿using IOManager;
using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using CL = LogManager.FileLogger;

namespace COWS.Mach5SenderService
{
    public class Mach55SenderIO
    {
        public int _iSleepTime = 0;

        private DataRow DR;
        private Mach5SenderDB _SenderDB = new Mach5SenderDB();
        private ProvisioningServiceNM.ProvisioningService _m5ws = new ProvisioningServiceNM.ProvisioningService();

        //private RedesignChargeServiceNM.RedesignChargeService _m5rws = new RedesignChargeServiceNM.RedesignChargeService();
        private string CSG_LVL_ID = string.Empty;

        private string M5_TRAN_ID = string.Empty;

        public static bool IgnoreCertificateErrorHandler(object sender,
        X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public void GetMach5RedesignMsgs()
        {
            try
            {
                DataTable DT = new DataTable();
                string _m5Response = string.Empty;

                DT = _SenderDB.GetMach5RedesignMsgs();

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    int iCnt = DT.Rows.Count;
                    CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send Redesign Request(s) to Mach5", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];
                        //int iRsp = GenRedesignMsg(DR["TRAN_ID"].ToString(),
                        //    DR["REDSGN_NBR"].ToString(), DR["REDSGN_TYPE"].ToString(), DR["H6"].ToString(),
                        //    DR["ACTV_DT"].ToString(), DR["CHARGE"].ToString(), DR["SITE"].ToString(), DR["BEC"].ToString());

                        //_SenderDB.UpdateMach5RedesignMsgs(int.Parse(DR["TRAN_ID"].ToString()), iRsp);

                        string resp = GenRedesignMsg(DR["TRAN_ID"].ToString(),
                                DR["REDSGN_NBR"].ToString(), DR["REDSGN_TYPE"].ToString(), DR["H6"].ToString(),
                                DR["ACTV_DT"].ToString(), DR["CHARGE"].ToString(), DR["SITE"].ToString(), DR["BEC"].ToString());

                        if (resp.Length <= 0)
                            _SenderDB.UpdateMach5RedesignMsgs(int.Parse(DR["TRAN_ID"].ToString()), 21, resp);
                        else
                            _SenderDB.UpdateMach5RedesignMsgs(int.Parse(DR["TRAN_ID"].ToString()), 12, resp);
                    }
                }
                else
                    CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "No record(s) found to send Redesign Request(s) to Mach5", string.Empty);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error while sending Mach5 Redesign XML", ex.Message + "; " + ex.StackTrace);
            }
        }

        public void GetMach5BillDsptchMsgs()
        {
            try
            {
                DataTable DT = new DataTable();
                string _m5Response = string.Empty;

                DT = _SenderDB.GetMach5BillDsptchMsgs();

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    int iCnt = DT.Rows.Count;
                    CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send Billable Dispatch Request(s) to Mach5", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];

                        string resp = GenBillDsptchMsg(DR["M5_BILL_DSPTCH_MSG_ID"].ToString(),
                                DR["TICK_DESC"].ToString(), DR["TICK_NBR"].ToString(), DR["CUST_ID"].ToString(),
                                DR["CLS_DT"].ToString(), DR["CHARGE"].ToString(), DR["BEC"].ToString());

                        if (resp.Length <= 0)
                            _SenderDB.UpdateBillDsptchMsgs(int.Parse(DR["M5_BILL_DSPTCH_MSG_ID"].ToString()), int.Parse(DR["BILL_DSPTCH_ID"].ToString()), 21, resp);
                        else
                            _SenderDB.UpdateBillDsptchMsgs(int.Parse(DR["M5_BILL_DSPTCH_MSG_ID"].ToString()), int.Parse(DR["BILL_DSPTCH_ID"].ToString()), 12, resp);
                    }
                }
                else
                    CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "No record(s) found to send Billable Dispatch Request(s) to Mach5", string.Empty);
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error while sending Mach5 Billable Dispatch XML", ex.Message + "; " + ex.StackTrace);
            }
        }

        private string GenBillDsptchMsg(string _tranID, string _tktDesc, string _tktNbr, string _h6, string _clsDt, string _charge, string _bec)
        {
            string resp = string.Empty;
            RedesignChargeServiceReference.RedesignChargeClient soapClient = new RedesignChargeServiceReference.RedesignChargeClient();

            RedesignChargeServiceReference.redesignBillingChargeRequestType rbcrt = new RedesignChargeServiceReference.redesignBillingChargeRequestType();
            RedesignChargeServiceReference.CommonHeaderType cht = new RedesignChargeServiceReference.CommonHeaderType();
            RedesignChargeServiceReference.RedesignBillingCharge rbc = new RedesignChargeServiceReference.RedesignBillingCharge();
            RedesignChargeServiceReference.redesignBillingChargeResponseType response = new RedesignChargeServiceReference.redesignBillingChargeResponseType();

            cht.sendingApplicationId = "QDA";
            cht.sharedSecret = ConfigurationManager.AppSettings["redesignSharedSecret"].ToString();
            cht.transactionId = _tranID;
            rbcrt.commonHeader = cht;

            rbc.system = "QDA";
            rbc.redesignNumber = _tktNbr;
            rbc.redesignType = (_tktDesc.Length>40) ? _tktDesc.Substring(0,40) : _tktDesc;
            rbc.site = _h6;
            rbc.customerAccountNumber = _h6;
            rbc.activationDate = DateTime.Parse(_clsDt);
            rbc.charge = Decimal.Parse(_charge);
            rbc.billingEventCode = _bec;
            rbcrt.redesignBillingCharge = rbc;
            CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Request message sent to Mach5 for Billable Dispatch TRAN_ID " + _tranID, StringExtns.Serialize(rbcrt));
            try
            {
                if (ConfigurationManager.AppSettings["Url"].ToString().Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }

                response = soapClient.assessRedesignCharge(rbcrt);
                CL.WriteLogFile("Response", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Response received from Mach5-- " + StringExtns.Serialize(response) + "; Billable Dispatch TRAN_ID " + _tranID, string.Empty);
                resp = string.Empty;
            }
            catch (Exception error)
            {
                resp = error.Message;
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error received from Mach5 for Billable Dispatch TRAN_ID " + _tranID, error.Message);
            }

            return resp;
        }

        private string GenRedesignMsg(string _tranID, string _redsgnNbr, string _redsgnType, string _h6, string _actvDt, string _charge, string _site, string _bec)
        {
            string resp = string.Empty;
            RedesignChargeServiceReference.RedesignChargeClient soapClient = new RedesignChargeServiceReference.RedesignChargeClient();

            RedesignChargeServiceReference.redesignBillingChargeRequestType rbcrt = new RedesignChargeServiceReference.redesignBillingChargeRequestType();
            RedesignChargeServiceReference.CommonHeaderType cht = new RedesignChargeServiceReference.CommonHeaderType();
            RedesignChargeServiceReference.RedesignBillingCharge rbc = new RedesignChargeServiceReference.RedesignBillingCharge();
            RedesignChargeServiceReference.redesignBillingChargeResponseType response = new RedesignChargeServiceReference.redesignBillingChargeResponseType();

            cht.sendingApplicationId = "QDA";
            cht.sharedSecret = ConfigurationManager.AppSettings["redesignSharedSecret"].ToString();
            cht.transactionId = _tranID;
            rbcrt.commonHeader = cht;

            rbc.system = "QDA";
            rbc.redesignNumber = _redsgnNbr;
            rbc.redesignType = _redsgnType;
            rbc.site = _site;
            rbc.customerAccountNumber = _h6;
            rbc.activationDate = DateTime.Parse(_actvDt);
            rbc.charge = Decimal.Parse(_charge);
            rbc.billingEventCode = _bec;
            rbcrt.redesignBillingCharge = rbc;
            CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Request message sent to Mach5 for TRAN_ID " + _tranID, StringExtns.Serialize(rbcrt));
            try
            {
                if (ConfigurationManager.AppSettings["Url"].ToString().Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }

                response = soapClient.assessRedesignCharge(rbcrt);
                CL.WriteLogFile("Response", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Response received from Mach5-- " + StringExtns.Serialize(response), string.Empty);
                resp = string.Empty;
            }
            catch (Exception error)
            {
                resp = error.Message;
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, -1, string.Empty, "Error received from Mach5 for TRAN_ID " + _tranID, error.Message);
            }

            return resp;
        }

        //private int GenRedesignMsg(string _tranID, string _redsgnNbr, string _redsgnType, string _h6, string _actvDt, string _charge, string _site, string _bec)
        //{
        //    int iRsp = 11;
        //    try
        //    {
        //        RedesignChargeServiceNM.redesignBillingChargeRequestType _request = new RedesignChargeServiceNM.redesignBillingChargeRequestType();

        //        _request.commonHeader = new RedesignChargeServiceNM.CommonHeaderType();
        //        _request.redesignBillingCharge = new RedesignChargeServiceNM.RedesignBillingCharge();

        //        _request.commonHeader.sendingApplicationId = "QDA";
        //        _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
        //        _request.commonHeader.transactionId = _tranID;

        //        _request.redesignBillingCharge.system = "QDA";
        //        _request.redesignBillingCharge.redesignNumber = _redsgnNbr;
        //        _request.redesignBillingCharge.redesignType = _redsgnType;
        //        _request.redesignBillingCharge.site = _site;
        //        _request.redesignBillingCharge.customerAccountNumber = _h6;
        //        _request.redesignBillingCharge.activationDate = DateTime.Parse(_actvDt);
        //        _request.redesignBillingCharge.charge = Decimal.Parse(_charge);
        //        _request.redesignBillingCharge.billingEventCode = _bec;

        //        string XMLReqStr = StringExtns.Serialize(_request);

        //        if (XMLReqStr.Length > 0)
        //        {
        //            try
        //            {
        //                ServicePointManager.ServerCertificateValidationCallback =
        //                new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

        //                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        //                RedesignChargeServiceNM.redesignBillingChargeResponseType _response = new RedesignChargeServiceNM.redesignBillingChargeResponseType();
        //                CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, "Request message sent to Mach5 for TRAN_ID " + _tranID, XMLReqStr);

        //                _response = _m5rws.assessRedesignCharge(_request);

        //                CL.WriteLogFile("Response", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Response received from Mach5-- " + StringExtns.Serialize(_response), string.Empty);
        //            }
        //            catch (Exception e)
        //            {
        //                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error putting msg to Web Service -- " + XMLReqStr, e.Message.ToString());
        //                iRsp = 12;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        iRsp = 12;
        //        CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, "Error generating Mach5 XML for TRAN_ID " + _tranID, e.Message.ToString());
        //    }

        //    return iRsp;
        //}

        public void GetMach5Responses()
        {
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            DataTable DTDisc = new DataTable();
            string XMLReqStr = string.Empty;
            string errorMsg = string.Empty;
            try
            {
                DT = _SenderDB.GetMach5Responses();

                if ((DT != null) && (DT.Rows.Count > 0))
                {
                    int iCnt = DT.Rows.Count;
                    CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + " record(s) found to send Request(s) to Mach5", string.Empty);
                    for (int i = 0; i < iCnt; i++)
                    {
                        DR = DT.Rows[i];
                        M5_TRAN_ID = DR["M5_TRAN_ID"].ToString();
                        CSG_LVL_ID = DR["CSG_LVL_ID"].ToString();
                        ProvisioningServiceNM.ProvisioningEventMessageRequestType _msg = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();
                        int iStat = 11;

                        try
                        {
                            switch (Convert.ToInt32(DR["M5_MSG_ID"].ToString()))
                            {
                                case 1:
                                    _msg = GenComplCPEMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                                DR["NTE"].ToString(), DR["DEVICE"].ToString());
                                    break;

                                case 2:
                                    _msg = GenComplMNSMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(), DR["DEVICE"].ToString(), DR["CMPNT_ID"].ToString());
                                    break;

                                case 3:
                                    _msg = GenComplMSSMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(), DR["DEVICE"].ToString(), DR["CMPNT_ID"].ToString());
                                    break;

                                case 4:
                                    _msg = GenRTSCPEMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(), DR["DEVICE"].ToString());
                                    break;

                                case 5:
                                    _msg = GenCpePOMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(), DR["DEVICE"].ToString(),
                                                               DR["PRCH_ORDR_NBR"].ToString(),
                                                               DR["SUPPLIER"].ToString());
                                    break;

                                case 6:
                                    _msg = GenComplAccessMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(),
                                                               DR["CUST_ID"].ToString(),
                                                               DR["VNDR_CKT_ID"].ToString(),
                                                               DR["ORDR_SUB_TYPE_CD"].ToString());
                                    break;
                                case 7:
                                    _msg = GenComplAccessMsg(DR["M5_TRAN_ID"].ToString(), DR["FTN"].ToString(),
                                                               DR["NTE"].ToString(),
                                                               DR["CUST_ID"].ToString(),
                                                               DR["VNDR_CKT_ID"].ToString(),
                                                               DR["ORDR_SUB_TYPE_CD"].ToString(), "IMCMP");
                                    break;

                                default:
                                    break;
                            }
                            XMLReqStr = StringExtns.Serialize(_msg);

                            if (CSG_LVL_ID == "0")
                                CL.WriteLogFile("Response", CL.Event.GenericMessage, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Mach5 Request XML is: ", XMLReqStr);
                            else
                            {
                                try
                                {
                                    _SenderDB.InsertCustomerSecuredLog(Convert.ToInt32(M5_TRAN_ID), 104, XMLReqStr);
                                }
                                catch (Exception e)
                                {
                                    CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error inserting Log message into CUST_SCRD_LOG table for ORDR_ID " + DR["ORDR_ID"], e.Message.ToString());
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            iStat = 12;
                            CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error generating Mach5 XML for ORDR_ID " + DR["ORDR_ID"], e.Message.ToString());
                        }

                        if (XMLReqStr.Length > 0)
                        {
                            try
                            {
                                if (ConfigurationManager.AppSettings["Url"].ToString().Contains("https"))
                                {
                                    ServicePointManager.ServerCertificateValidationCallback =
                                    new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                }

                                ProvisioningServiceNM.ProvisioningEventResponseType _response = new ProvisioningServiceNM.ProvisioningEventResponseType();

                                _response = _m5ws.newProvisioningEventMessage(_msg);

                                CL.WriteLogFile("Response", CL.Event.EndMQWrite, CL.MsgType.text, -1, string.Empty, "Response received from Mach5-- " + StringExtns.Serialize(_response), string.Empty);
                            }
                            catch (Exception e)
                            {
                                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error putting msg to Web Service -- " + XMLReqStr, e.Message.ToString());
                                iStat = 12;
                                errorMsg = e.Message;
                            }
                        }

                        if (_SenderDB.UpdateMach5Response(Convert.ToInt32(DR["M5_TRAN_ID"]), Convert.ToInt32(DR["ORDR_ID"]), iStat, errorMsg) == 0)
                        {
                            CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.text, -1, string.Empty, iCnt.ToString() + "Error updating the status in the M5_ORDR_MSG table for TRAN_ID: " + DR["M5_TRAN_ID"], string.Empty);
                        }
                    }
                }
                else
                {
                    if (DateTime.Now.Second % 30 == 0)
                    {
                        CL.WriteLogFile("Response", CL.Event.EndDBRead, CL.MsgType.text, -1, string.Empty, "Zero (0) records found to send Requests to Mach5", string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                CL.WriteLogFile("Response", CL.Event.Error, CL.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
            }
        }

        #region "Private Methods"

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenComplCPEMsg(string _tranID, string _ordrId, string _note, string _device)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            //_request.commonHeader.sharedSecret = "lqsYkzHN82";
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            _request.provisioningEventMessage.msgType = "CCMPT";
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.cpeDeviceId = _device;

            return _request;
        }

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenComplMNSMsg(string _tranID, string _ordrId, string _note, string _device, string _cmpnt)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            _request.provisioningEventMessage.msgType = "MCMPT";
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.cpeDeviceId = _device;
            _request.provisioningEventMessage.orderComponents = new string[1] { _cmpnt };

            return _request;
        }

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenComplMSSMsg(string _tranID, string _ordrId, string _note, string _device, string _cmpnt)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            _request.provisioningEventMessage.msgType = "MSCMP";
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.cpeDeviceId = _device;
            _request.provisioningEventMessage.orderComponents = new string[1] { _cmpnt };

            return _request;
        }

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenRTSCPEMsg(string _tranID, string _ordrId, string _note, string _device)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            _request.provisioningEventMessage.msgType = "RTSCP";
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.cpeDeviceId = _device;

            return _request;
        }

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenCpePOMsg(string _tranID, string _ordrId, string _note, string _device, string _PO, string _supplier)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            _request.provisioningEventMessage.msgType = "CPEPO";
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.cpeDeviceId = _device;
            _request.provisioningEventMessage.purchaseOrderNumber = _PO;
            _request.provisioningEventMessage.cpeSupplier = _supplier;

            return _request;
        }

        private ProvisioningServiceNM.ProvisioningEventMessageRequestType GenComplAccessMsg(string _tranID, string _ordrId, string _note, string _h6CustId, string _circuitId, string _ordrSubTypeCd, string _msgType = null)
        {
            ProvisioningServiceNM.ProvisioningEventMessageRequestType _request = new ProvisioningServiceNM.ProvisioningEventMessageRequestType();

            _request.commonHeader = new ProvisioningServiceNM.CommonHeaderType();
            _request.provisioningEventMessage = new ProvisioningServiceNM.ProvisioningEventMessageType();

            _request.commonHeader.sendingApplicationId = "QDA";
            _request.commonHeader.sharedSecret = ConfigurationManager.AppSettings["sharedSecret"].ToString();
            _request.commonHeader.transactionId = _tranID;

            _request.provisioningEventMessage.orderNbr = _ordrId;
            if(_msgType == null)
            {
                _request.provisioningEventMessage.msgType = string.Equals(_ordrSubTypeCd, "AR", StringComparison.CurrentCultureIgnoreCase) ? "ARCMT" : (string.Equals(_ordrSubTypeCd, "AN", StringComparison.CurrentCultureIgnoreCase) ? "ARCMT" : "APCMP");
            } else
            {
                _request.provisioningEventMessage.msgType = _msgType;
            }
            _request.provisioningEventMessage.notes = _note;
            _request.provisioningEventMessage.eventDate = DateTime.Now;
            _request.provisioningEventMessage.h6CustId = _h6CustId;
            _request.provisioningEventMessage.circuitId = _circuitId;

            return _request;
        }

        private string FixNull(string input)
        {
            return (input == null || input == "QQQ" || input == "-1" || input == @"1/1/0001" || input == "ItemQQQ") ? "" : input;
        }

        #endregion "Private Methods"
    }
}