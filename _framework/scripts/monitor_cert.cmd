@echo off
REM *******************************************************************************
REM Copyright (c) 2012 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave 
REM
REM Script to Monitor the Certificate Experation date 
REM
REM
REM
REM
REM *******************************************************************************


REM
REM initialize some variables
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%

CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%

REM
REM validate %COMPUTERNAME% matches local config
REM 

CALL "%APP_HOME%\Scripts\perform-host-check.cmd"
IF ERRORLEVEL 1 (
  ECHO ERROR: host validation failed
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 3 "ERROR: host validation failed"
  EXIT /b 3
)



REM
REM initialize LOCATION from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd CERT_LOCATION LOCATION
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve CERT_LOCATION from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve CERT_LOCATION from Configuration"
  EXIT /b 2
)

REM
REM initialize DAYS_2_REMIND from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd CERT_DAYS_2_REMIND DAYS_2_REMIND
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve CERT_DAYS_2_REMIND from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve CERT_DAYS_2_REMIND from Configuration"
  EXIT /b 2
)

REM
REM initialize EMAIL_ADDRESS from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd CERT_EMAIL_ADDRESS EMAIL_ADDRESS
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve CERT_EMAIL_ADDRESS from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve CERT_EMAIL_ADDRESS from Configuration"
  EXIT /b 2
)

REM
REM initialize SUBJECT from configuration
REM
CALL "%APP_HOME%"\Scripts\_getconfig.cmd CERT_SUBJECT SUBJECT
IF ERRORLEVEL 1 (
  ECHO ERROR: Unable to retrieve CERT_SUBJECT from Configuration.
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.cmd 2 "ERROR: Unable to retrieve CERT_SUBJECT from Configuration"
  EXIT /b 2
)

%WINDIR%\System32\WindowsPowerShell\v1.0\powershell.exe "%APP_HOME%"\Scripts\MonitorCerts.ps1 -LOCATION '%LOCATION%' -DAYS_2_REMIND '%DAYS_2_REMIND%' -EMAIL_ADDRESS '%EMAIL_ADDRESS%' -SUBJECT '%SUBJECT%'

IF ERRORLEVEL 1 (
  ECHO ERROR: Emonitor_cert.ps1 failed with exit code %ERRORLEVEL%
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" monitor_cert.ps1 6 "ERROR: monitor_cert.ps1 failed"
  EXIT /b 6
) ELSE (
  ECHO INFO: Monitor_cert.ps1 Certificate Monitor Completed Successfully
  CALL "%APP_HOME%\Scripts\_logJobMsg.cmd" Monitor_cert.ps1 0 "INFO: Certificate Monitor Completed Successfully"
  EXIT /b 0
)