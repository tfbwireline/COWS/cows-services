﻿namespace AppConstants
{
    public enum EWebActyType
    {
        OrderSearch = 1,
        EventSearch = 2,
        RedesignSearch = 3,
        OrderDetails = 4,
        EventDetails = 5,
        RedesignDetails = 6
    }
}