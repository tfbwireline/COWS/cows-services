﻿using DataManager;
using System.Data;

namespace COWS.RulesetProcessor
{
    public class BPMRedesignDB : DBBase
    {
        public DataTable GetByRecStusId(int recStusId)
        {
            setCommandText("SELECT * FROM dbo.BPM_REDSN_TXN WHERE REC_STUS_ID = " + recStusId);
            return getDataTable();
        }

        public int Update(int id, int recStusId)
        {
            if (id > 0)
            {
                setCommandText("UPDATE dbo.BPM_REDSN_TXN SET REC_STUS_ID = " + recStusId + ", MODFD_BY_USER_ID = 1, MODFD_DT = GETDATE() WHERE ID = " + id);
                return execNoQuery();
            }

            return 0;
        }
    }
}