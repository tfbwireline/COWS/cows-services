@echo off
REM *******************************************************************************
REM Copyright (c) 2009 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Scott Musgrave - initial implementation
REM
REM Application job to Clean log files from the %APP_HOME%\Logs\Services\* 
REM directories. An ERRORLEVEL of 0 will be returned when logs over 120 days
REM are successfully removed, a non-zero ERRORLEVEL will be returned if there
REM are any errors.
REM 
REM *******************************************************************************


REM
REM initialize application home and call install script
REM
SETLOCAL
SET CMDARG=%~dp0
PUSHD "%CMDARG%"..
SET APP_HOME=%CD%
"%APP_HOME%\Scripts\log_cleaner.cmd"
