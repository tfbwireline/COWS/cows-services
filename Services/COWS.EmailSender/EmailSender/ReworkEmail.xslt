﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:key name="tabids" match="EventEmail/Tab[@TABID]" use="@TABID" />
	<xsl:template match="/">
		<html>
			<body>
				<table width="900" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
					<tr>
						<td colspan="2">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="concat(/EventEmail/MDSEvent/ViewEventURL, '?eid=', /EventEmail/MDSEvent/EVENT_ID, '&amp;email=1')" />
								</xsl:attribute>
								Click here to view Event
							</a>
						</td>
					</tr>
					<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.='FastTrack']">
						<tr>
							<td colspan="2">
								This event was submitted to the Fast Published queue and it has been returned to your rework queue as the stated requested implementation date has passed without initiation. Please select a valid date for work to be performed and resubmit. Thank you.
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.!='FastTrack']">
						<tr>
							<td colspan="2">
								This event was in 'Rework' with an assigned resource due to commence in less than 5 business days.  Please select a new valid date for work to be performed and resubmit. Thank you.
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event ID : </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/EVENT_ID" />
						</td>
					</tr>
					<xsl:if test="/EventEmail/MDSEvent/SOWS_EVENT_ID[.!='']">
						<tr>
							<td width="30%" style="text-align: left; font-weight: 600">SOWS Event ID : </td>
							<td width="70%">
								<xsl:value-of select="/EventEmail/MDSEvent/SOWS_EVENT_ID" />
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event Reviewer : </td>
						<td width="70%">
							<xsl:value-of select="concat(/EventEmail/MDSEvent/RevUserName, ' - ', /EventEmail/MDSEvent/REV_USR_ADID)" />
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Assigned Activators : </td>
						<td width="70%">
							<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.='FastTrack']">
								COWS ES Activators
							</xsl:if>
							<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.!='FastTrack']">
								<xsl:value-of select="/EventEmail/MDSEvent/AssignUsers" />
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Scheduled Event Date &amp; Time : </td>
						<td width="70%">
							<xsl:value-of select="concat(/EventEmail/MDSEvent/STRT_TMST, ' CT')" />
						</td>
					</tr>
					<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.='FastTrack']">
						<tr>
							<td width="30%" style="text-align: left; font-weight: 600">Fast Track TimeBlock : </td>
							<td width="70%">
								<xsl:value-of select="/EventEmail/MDSEvent/FT_TIME_BLOCK" />
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event Title : </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/EVENT_TITLE_TXT" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event Author : </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Event Author Contact #: </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_PHN" />
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Conference Bridge Number and Pin : </td>
						<td width="70%">
							<xsl:if test="/EventEmail/MDSEvent/CNFRC_BRDG_NBR[.!='']">
								<xsl:value-of select="concat(/EventEmail/MDSEvent/CNFRC_BRDG_NBR, ' / ', /EventEmail/MDSEvent/CNFRC_PIN_NBR)" />
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Online Meeting URL : </td>
						<td width="70%">
							<xsl:if test="/EventEmail/MDSEvent/OnlineMeetingURL[.!='']">
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="/EventEmail/MDSEvent/OnlineMeetingURL" />
									</xsl:attribute>
									<xsl:value-of select="/EventEmail/MDSEvent/OnlineMeetingURL" />
								</a>
							</xsl:if>
						</td>
					</tr>
					<xsl:if test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.='FastTrack']">
						<tr>
							<td width="30%" style="text-align: left; font-weight: 600">Fast Track Conference Bridge Number and Pin : </td>
							<td width="70%">
								Call 877-FAST-890 (877-327-8890) 15-20 minutes prior to event start to engage an engineering resource for the coordinated activity
							</td>
						</tr>
					</xsl:if>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Short Description : </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/SHRT_DES" />
						</td>
					</tr>
					<tr></tr>
					<xsl:for-each select="EventEmail/Tab[@TABID]">
						<tr></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td style="text-align: center; font-weight: 600" colspan="2">
								*********** Tab Info ***********
							</td>
						</tr>
						<tr></tr>
						<tr>
							<td style="text-align: center; font-weight: 600" colspan="2">
								====== TAB LEVEL Details ======
							</td>
						</tr>

						<xsl:for-each select="/EventEmail/Tab/MDSMISC">
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Tab Sequence : </td>
								<td width="70%">
									<xsl:value-of select="concat(/EventEmail/Tab/MDSMISC/TAB_SEQ_NBR,' : ',/EventEmail/Tab/MDSMISC/TAB_NME)" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Sprint CPE NCR DES : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SPRINT_CPE_NCR_DES" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Srvc Assurance Site Supplier : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SRVC_ASSRN_SITE_SUPP_DES" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">CPE Dispatch Email Addr : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/CPE_DSPCH_EMAIL_ADR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">CPE Dispatch Comments : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/CPE_DSPCH_CMNT_TXT" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Transport FTN : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/TRPT_FTN" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Is Wired Transport Required? : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/WIRED_DEV_TRNSPRT_REQR_CD" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Is Wireless Transport Required? : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/WRLS_TRNSPRT_REQR_CD" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Any Virtual Connections : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/VRTL_CNCTN_CD" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Install Site POC : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/INSTL_SITE_POC_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/INSTL_SITE_POC_PHN_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Install Site POC Phone : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/INSTL_SITE_POC_PHN_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Install Site POC Cell : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/INSTL_SITE_POC_CELL_PHN_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Assurance POC : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SRVC_ASSRN_POC_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Assurance POC Phone : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SRVC_ASSRN_POC_PHN_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Assurance POC Cell : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SRVC_ASSRN_POC_CELL_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Assurance POC Contact Hours : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/SA_POC_HR_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Assurance POC TimeZone : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSMISC/TME_ZONE_ID" />
								</td>
							</tr>
							<tr></tr>
						</xsl:for-each>

						<tr></tr>
						<tr></tr>

						<tr>
							<td  colspan="2" style="text-align: center; font-weight: 600">
								==== Order Entry Details =====
							</td>
						</tr>

						<xsl:for-each select="/EventEmail/Tab/MDSOE">
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600"> FTN : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSOE/MNS_OE_FTN" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Service Description : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSOE/MDS_SRVC_TIER_DES" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Enrollment Description : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSOE/MDS_ENTLMNT_DES" />
								</td>
							</tr>
							<tr></tr>
						</xsl:for-each>

						<tr>
							<td  colspan="2" style="text-align: center; font-weight: 600">
								====== LOC Details ======
							</td>
						</tr>

						<xsl:for-each select="/EventEmail/Tab/MDSLOC">
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">LOC Type : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSLOC/MDS_LOC_TYPE_DES" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">LOC Category : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSLOC/MDS_LOC_CAT_DES" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">LOC Number : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSLOC/MDS_LOC_NBR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">LOC RAS Date : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSLOC/MDS_LOC_RAS_DT" />
								</td>
							</tr>
							<tr></tr>
						</xsl:for-each>

						<tr></tr>

						<tr>
							<td  colspan="2" style="text-align: center; font-weight: 600">
								====== PVC Details ======
							</td>
						</tr>

						<xsl:for-each select="/EventEmail/Tab/MDSPVC">
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">PVC ID : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSPVC/PVC_ID" />
								</td>
							</tr>
							<tr></tr>
						</xsl:for-each>

						<tr></tr>

						<tr>
							<td  colspan="2" style="text-align: center; font-weight: 600">
								===== Site Address Details ======
							</td>
						</tr>

						<xsl:for-each select="/EventEmail/Tab/MDSAddr">
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Address : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/ADR" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Floor/Building Name : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/FLR_BLDG_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">City : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/CTY_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">State/Providence : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/STT_PRVN_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Country/Region : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/CTRY_RGN_NME" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">Zip : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/ZIP_CD" />
								</td>
							</tr>
							<tr>
								<td width="30%" style="text-align: left; font-weight: 600">US/International : </td>
								<td width="70%">
									<xsl:value-of select="/EventEmail/Tab/MDSAddr/US_INTL_ID" />
								</td>
							</tr>

							<tr></tr>
						</xsl:for-each>
					</xsl:for-each>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="30%" style="text-align: left; font-weight: 600">Redesign Number : </td>
						<td width="70%">
							<xsl:value-of select="/EventEmail/MDSEvent/RDSGN_NBR" />
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2">Here is a link to the Customer's SOW folder in Docshare. Click here to locate the redesign number listed above to access the Scope of Work for this activity :</td>
					</tr>
					<tr>
						<td width="100%" colspan="2">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="/EventEmail/MDSEvent/DSGN_DOC_LOC_TXT" />
								</xsl:attribute>
								Click Here
							</a>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="100%" colspan="2">Comments Log</td>
					</tr>
					<xsl:choose>
						<xsl:when test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.='FastTrack'] and /EventEmail/MDSEvent/EVENT_STUS_DES[.!='OnHold']">
							<tr>
								<td colspan="2">
									This event was submitted to the Fast Published queue and it has been returned to your rework queue as the stated requested implementation date has passed without initiation. Please select a valid date for work to be performed and resubmit. Thank you.
								</td>
							</tr>
						</xsl:when>
						<xsl:when test="/EventEmail/MDSEvent/MDS_FAST_TRK_CD[.!='FastTrack'] and /EventEmail/MDSEvent/EVENT_STUS_DES[.!='OnHold']">
							<tr>
								<td colspan="2">
									This event was in REWORK with an assigneed resource due to commence in less than 72 hours. Please select a new valid date for work to be performed and resubmit. Thank you.
								</td>
							</tr>
						</xsl:when>
						<xsl:when test="/EventEmail/MDSEvent/EVENT_STUS_DES[.='OnHold']">
							<tr>
								<td colspan="2">
									This event was left in the on-hold status in a Managed Services queue and it has been returned to your rework queue because the implementation date has passed. Please select another date for work to be performed and resubmit the event.
								</td>
							</tr>
						</xsl:when>
					</xsl:choose>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>