﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-ALL;" cellpadding="1">
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="concat(normalize-space(/EventEmail/MDSEvent/CalenderEntryURL), '?eid=', /EventEmail/MDSEvent/EVENT_ID, '&amp;mode=0')" />
                </xsl:attribute>
                Click here to open the event, find it in your outlook calendar and remove it
              </a>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a>
                <xsl:attribute name="href">
									<xsl:choose>
										<xsl:when test="contains(/EventEmail/MDSEvent/ViewEventURL,'cowsrtb1') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsrtb2') or contains(/EventEmail/MDSEvent/ViewEventURL,'cows.') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsbreakfix') or contains(/EventEmail/MDSEvent/ViewEventURL,'cowsdev2')">
											<xsl:value-of select="concat(normalize-space(/EventEmail/MDSEvent/ViewEventURL), 'mds/', /EventEmail/MDSEvent/EVENT_ID)" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="concat(/EventEmail/MDSEvent/ViewEventURL, '?eid=', /EventEmail/MDSEvent/EVENT_ID, '&amp;email=1')" />
										</xsl:otherwise>
									</xsl:choose>
                </xsl:attribute>
                Click here to view Event
              </a>
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 600">Event ID : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/MDSEvent/EVENT_ID" />
            </td>
          </tr>
          <xsl:if test="/EventEmail/MDSEvent/SOWS_EVENT_ID[.!='']">
            <tr>
              <td width="30%" style="text-align: left; font-weight: 600">SOWS Event ID : </td>
              <td width="70%">
                <xsl:value-of select="/EventEmail/MDSEvent/SOWS_EVENT_ID" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Status : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/MDSEvent/EVENT_STUS_DES" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Reviewer : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/MDSEvent/ModUserName" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Assigned MNS Resource : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/MDSEvent/AssignUsers" />
            </td>
          </tr>
          <tr>
            <td width="30%" style="text-align: left; font-weight: 200">Event Title : </td>
            <td width="70%">
              <xsl:value-of select="/EventEmail/MDSEvent/EVENT_TITLE_TXT" />
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>