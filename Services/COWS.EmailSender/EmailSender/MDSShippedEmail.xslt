﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template match="/">
    <html>
      <body>
        <table width="900" border="0" align="left" style="WORD-BREAK:BREAK-WORD;table-layout: fixed;" cellpadding="1">
          <tr>
            <td>

              <span>
				<xsl:if test="/EventEmail/Tab">
                Dear <xsl:value-of select="EventEmail/Tab[1]/MDSMISC[1]/INSTL_SITE_POC_NME" />,<br></br><br></br>

                The new equipment (Device Serial # : <xsl:value-of select="/EventEmail/MDSEvent/DEV_SERIAL_NBR" />) for
                <xsl:if test="EventEmail/Tab[1]/MDSAddr[1]/FLR_BLDG_NME[.!='']">
                  <xsl:value-of select="concat(EventEmail/Tab[1]/MDSAddr[1]/STREET_ADR, ', ',EventEmail/Tab[1]/MDSAddr[1]/FLR_BLDG_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/CTY_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/STT_PRVN_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/CTRY_RGN_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/US_INTL_ID, ', ',  EventEmail/Tab[1]/MDSAddr[1]/ZIP_CD)" />
                </xsl:if>
                <xsl:if test="EventEmail/Tab[1]/MDSAddr[1]/FLR_BLDG_NME[.='']">
                  <xsl:value-of select="concat(EventEmail/Tab[1]/MDSAddr[1]/STREET_ADR, ', ', EventEmail/Tab[1]/MDSAddr[1]/CTY_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/STT_PRVN_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/CTRY_RGN_NME, ', ', EventEmail/Tab[1]/MDSAddr[1]/US_INTL_ID, ', ',  EventEmail/Tab[1]/MDSAddr[1]/ZIP_CD)" />
                </xsl:if>
				</xsl:if>
				<xsl:if test="not(/EventEmail/Tab)">
				Dear <xsl:value-of select="EventEmail/MDSEvent/INSTL_SITE_POC_NME" />,<br></br><br></br>

                The new equipment (Device Serial # : <xsl:value-of select="/EventEmail/MDSEvent/DEV_SERIAL_NBR" />) for
                <xsl:if test="EventEmail/MDSEvent/FLR_BLDG_NME[.!='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/STREET_ADR, ', ',EventEmail/MDSEvent/FLR_BLDG_NME, ', ', EventEmail/MDSEvent/CTY_NME, ', ', EventEmail/MDSEvent/STT_PRVN_NME, ', ', EventEmail/MDSEvent/CTRY_RGN_NME, ', ', EventEmail/MDSEvent/US_INTL_ID, ', ',  EventEmail/MDSEvent/ZIP_CD)" />
                </xsl:if>
                <xsl:if test="EventEmail/MDSEvent/FLR_BLDG_NME[.='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/STREET_ADR, ', ', EventEmail/MDSEvent/CTY_NME, ', ', EventEmail/MDSEvent/STT_PRVN_NME, ', ', EventEmail/MDSEvent/CTRY_RGN_NME, ', ', EventEmail/MDSEvent/US_INTL_ID, ', ',  EventEmail/MDSEvent/ZIP_CD)" />
                </xsl:if>
				</xsl:if>
                has been shipped<xsl:if test="EventEmail/MDSEvent/ALT_SHIP_POC_NME[.!='']"> to <xsl:value-of select="/EventEmail/MDSEvent/ALT_SHIP_POC_NME" />, <xsl:if test="EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME[.!='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/ALT_SHIP_STREET_ADR, ', ',EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTY_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_STT_PRVN_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTRY_RGN_NME, ', ',  EventEmail/MDSEvent/ALT_SHIP_ZIP_CD)" />
                </xsl:if>
                <xsl:if test="EventEmail/MDSEvent/ALT_SHIP_FLR_BLDG_NME[.='']">
                  <xsl:value-of select="concat(EventEmail/MDSEvent/ALT_SHIP_STREET_ADR, ', ', EventEmail/MDSEvent/ALT_SHIP_CTY_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_STT_PRVN_NME, ', ', EventEmail/MDSEvent/ALT_SHIP_CTRY_RGN_NME, ', ',  EventEmail/MDSEvent/ALT_SHIP_ZIP_CD)" />
                </xsl:if></xsl:if>. Shipment Carrier Name is <xsl:value-of select="EventEmail/MDSEvent/SHIP_CXR_NME" />.  You may track this shipment via tracking number  <xsl:value-of select="EventEmail/MDSEvent/SHIP_TRK_REFR_NBR" />.<br></br><br></br>

                The scheduled turn up date for this Managed Equipment is <xsl:choose>
						<xsl:when test="EventEmail/MDSEvent/TME_SLOT_ID[.!='']"><xsl:value-of select="concat(substring-before(/EventEmail/MDSEvent/STRT_TMST, '@'), ' @ ', substring-before(/EventEmail/MDSEvent/TME_SLOT_ID, '-'))" /></xsl:when>
						<xsl:otherwise><xsl:value-of select="concat(/EventEmail/MDSEvent/STRT_TMST, ' CT')" /></xsl:otherwise>
						</xsl:choose>.&#160;Please join the conference bridge <xsl:if test="/EventEmail/MDSEvent/CNFRC_BRDG_NBR[.!='']">
                  <xsl:value-of select="concat(/EventEmail/MDSEvent/CNFRC_BRDG_NBR, ' / ', /EventEmail/MDSEvent/CNFRC_PIN_NBR)" />
                </xsl:if>
                at this time.&#160;If you do not join the bridge within 15 minutes, the bridge will be closed and the turn up will be rescheduled. Any changes to this date/time or conference bridge will be communicated to you by your Implementation Project Manager.<br></br><br></br>

                Please contact <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER" />
                at <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_PHN" />
                <xsl:if test="/EventEmail/MDSEvent/REQOR_USER_EMAIL[.!='']">
                  or <xsl:value-of select="/EventEmail/MDSEvent/REQOR_USER_EMAIL" />
                </xsl:if> should you have any questions or need to discuss an alternate date/time for the turn up of Managed Services.<br></br><br></br>

                Thank you,<br></br>
                T-Mobile Managed Services
              </span>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>