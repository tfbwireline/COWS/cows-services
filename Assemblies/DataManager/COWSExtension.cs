﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;
using System.Diagnostics;

public partial class COWS
{
    #region " Stored Procedure(s) "

    [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.linkFSAOrdersToH5Folder")]
    public void linkFSAOrdersToH5Folder()
    {
        this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
    }

    [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.InsertInitialTask")]
    [return: Parameter(DbType = "Int")]
    public int InsertInitialTask([Parameter(Name = "OrderID", DbType = "Int")] System.Nullable<int> OrderID,
                                [Parameter(Name = "CategoryID", DbType = "Int")] System.Nullable<int> CategoryID,
                                [Parameter(Name = "TaskID", DbType = "SmallInt")] System.Nullable<short> TaskID,
                                [Parameter(Name = "TaskStatus", DbType = "TinyInt")] System.Nullable<byte> TaskStatus)
    {
        IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), OrderID, CategoryID, TaskID, TaskStatus);
        return ((int)(result.ReturnValue));
    }

    #endregion " Stored Procedure(s) "
}
