@echo off
REM *******************************************************************************
REM Copyright (c) 2010 by Sprint Nextel
REM
REM All rights reserved. The content of this document is PROPRIETARY INFORMATION, 
REM and is the sole and exclusive property of the copyright owner. 
REM
REM Author:
REM   Woody Anderson - initial implementation
REM
REM AVT framework script to perform host validation. There is an optional config 
REM setting called HOST_CHECK that can be put in the config.properties.local file.
REM If this setting is present, then a check will be done against the environment
REM variable %COMPUTERNAME%. 
REM 
REM *******************************************************************************


REM
REM initialize some variables and do some validation
REM
CALL "%~dp0"_envsetup.cmd
IF ERRORLEVEL 1 EXIT /B %ERRORLEVEL%


REM
REM make sure the local (host) config file is available
REM
SET LOCAL_CFG_FILE=%APP_HOME%\Framework\etc\config.properties.local
IF NOT EXIST "%LOCAL_CFG_FILE%" (
  ECHO WARNING: local configuration file 'config.properties.local' not found, host check skipped
  EXIT /B 0
) 


REM
REM retrieve HOST_CHECK setting, exit if not existing
REM
CALL "%_SCRIPT_HOME%"\_getconfig.cmd HOST_CHECK
IF ERRORLEVEL 1 (
  ECHO INFO: setting HOST_CHECK not found, host validation skipped
  GOTO :environment_check
)


REM
REM now just compare against the env var %COMPUTERNAME%
REM
IF /I "%HOST_CHECK%" NEQ "%COMPUTERNAME%" (
  ECHO ERROR: host validation failed! [expected %HOST_CHECK%, found %COMPUTERNAME%]
  EXIT /B 10
) ELSE (
  ECHO INFO: host validation successful [host=%COMPUTERNAME%]
)


REM
REM ok, now let's do an environment check if we can
REM
:environment_check
SET ENVIRONMENT_CHECK=
CALL "%_SCRIPT_HOME%"\_getconfig.cmd ENVIRONMENT_CHECK
IF ERRORLEVEL 1 GOTO :environment_check_skipped
IF "%ENVIRONMENT_CHECK%" EQU "" GOTO :environment_check_skipped


REM
REM env check variable is set and not blank, so now we gotta validate env
REM  
SET ENV_NAME=
CALL "%_SCRIPT_HOME%"\_getconfig.cmd ENV_NAME
IF NOT ERRORLEVEL 1 (
  IF "%ENVIRONMENT_CHECK%" EQU "%ENV_NAME%" (
    ECHO INFO: environment validation successful [env=%ENV_NAME%]
    EXIT /B 0	  
  )
)
ECHO ERROR: environment validation failed! [expected %ENVIRONMENT_CHECK%, found %ENV_NAME%]
EXIT /B 11

	
REM
REM env check variable not found or set, environment check is skipped
REM  
:environment_check_skipped
ECHO WARNING: environment check setting not found or blank, environment validation skipped
EXIT /B 0
