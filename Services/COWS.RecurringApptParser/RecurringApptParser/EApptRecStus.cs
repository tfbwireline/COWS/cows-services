﻿namespace COWSRecurringApptParser
{
    public enum EApptRecStus
    {
        Pending = 0,
        Processing = 1,
        Success = 2,
        Failure = 3,
        OlderThan30days = 4
    }
}